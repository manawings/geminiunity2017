﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class TestFB : MonoBehaviour {
	
	public GUISkin MenuSkin;
	private static Dictionary<string, string> profile = null;
	public Rect LoginButtonRect; 
	string username;
	string result;
	string run;
	Texture userTexture;
	
	
	void Start () {
		enabled = false;                  
	    FB.Init(SetInit, OnHideUnity); 
	}
	
	private void SetInit()                                                                       
	{                            
		run = "SetInit";
	    Debug.Log("SetInit");                                                                  
	    enabled = true; // "enabled" is a property inherited from MonoBehaviour                  
	    if (FB.IsLoggedIn)                                                                       
	    {                                                                                        
	        Debug.Log("Already logged in");                                     
	        OnLoggedIn();                                                                        
	    }            
		//FB.Logout();
	}
	
	private void OnHideUnity(bool isGameShown)                                                   
	{                                                                                            
	    Debug.Log("OnHideUnity");                                                              
	    if (!isGameShown)                                                                        
	    {                                                                                        
	        // pause the game - we will need to hide                                             
	        Time.timeScale = 0;                                                                  
	    }                                                                                        
	    else                                                                                     
	    {                                                                                        
	        // start the game back up - we're getting focus again                                
	        Time.timeScale = 1;                                                                  
	    }                                                                                        
	}
	
	void LoginCallback(IResult result)                                                        
	{                                      
		run = result.RawResult;
	    Debug.Log("LoginCallback");                                                          
	
	    if (FB.IsLoggedIn)                                                                     
	    {                                                                                      
	        OnLoggedIn();                                                                      
	    }                                                                                      
	}
	
	void OnLoggedIn()
	{
		Debug.Log("Logged in. ID: " + AccessToken.CurrentAccessToken.UserId);
	    // Reqest player info and profile picture                                                                           
	    FB.API("/me?fields=id,first_name,friends.limit(1).fields(first_name,id)", HttpMethod.GET, APICallback);  
	    FB.API(Util.GetPictureURL("me", 128, 128), HttpMethod.GET, MyPictureCallback);    
	}
	
	void APICallback(IGraphResult result)                                                                                              
	{                                                                                                                              
	    Debug.Log("APICallback");                                                                                                
	    if (result.Error != null)                                                                                                  
	    {                                                                                                                          
			Debug.LogError(result.Error);                                                                                           
	        // Let's just try again                                                                                                
	        FB.API("/me?fields=id,first_name,friends.limit(100).fields(first_name,id)", HttpMethod.GET, APICallback);     
	        return;                                                                                                                
	    }                                                                                                                          
		this.result = result.RawResult; 
		profile = Util.DeserializeJSONProfile(result.RawResult);                                                                        
	    //GameStateManager.Username = profile["first_name"];                                                                         
	    //friends = Util.DeserializeJSONFriends(result.Text);                                                                        
	}                                                                                                                              
	
	void MyPictureCallback(IGraphResult result)                                                                                        
	{                                                                                                                              
		Debug.Log("MyPictureCallback");                                                                                          
	
	    if (result.Error != null)                                                                                                  
	    {                                                                                                                          
			Debug.LogError(result.Error);                                                                                           
	        // Let's just try again                                                                                                
	        FB.API(Util.GetPictureURL("me", 128, 128), HttpMethod.GET, MyPictureCallback);                                
	        return;                                                                                                                
	    }                                                                                                                          
	    userTexture = result.Texture;                                                                         
	}   	
	
	void OnGUI() {
		GUI.skin = MenuSkin;
		GUI.Label( (new Rect(150 , 400, 0, 0)), run, MenuSkin.GetStyle("text_only"));
		GUILayout.Box("", MenuSkin.GetStyle("panel_welcome"));
		if (!FB.IsLoggedIn)                                                                                              
		{                                                                                                                
		    GUI.Label( (new Rect(179 , 11, 287, 160)), "Login to Facebook", MenuSkin.GetStyle("text_only"));             
		    if (GUI.Button(LoginButtonRect, "", MenuSkin.GetStyle("button_login")))                                      
		    {             
				run = "Login";
		      //  FB.Login("email,publish_actions", LoginCallback);     
				var perms = new List<string>(){"user_friends"};
				FB.LogInWithReadPermissions(perms, LoginCallback);
				//FB.Login("user_friends", LoginCallback); 
		    }                                                                                                            
		} 
		
		if (FB.IsLoggedIn)
        {
            username = profile["first_name"];
			GUI.Label( (new Rect(200 , 0, 0, 0)), result, MenuSkin.GetStyle("text_only"));
			GUI.DrawTexture( (new Rect(8,10, 180, 180)), userTexture);
        }
		

	}
}
