using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;
using Facebook.Unity;

public class ConnectFacebook : MonoBehaviour {
	
	public string name;
	public Texture pic;
	public Dictionary<string, object> profile, temp;

	public bool isInit = false;
	public bool isSetProfile = false;
	public bool isSetFriendlist = false;

	public KJTime.TimeDelegate onComplete;

	public KJTime.TimeDelegate onLoginSuccess, onLoginFail;

	// Use this for initialization
	public void Connect (KJTime.TimeDelegate onLoginSuccess = null, KJTime.TimeDelegate onLoginFail = null) {   

		this.onLoginSuccess = onLoginSuccess;
		this.onLoginFail = onLoginFail;
		FB.Init(SetInit, OnHideUnity);

	}

	private void SetInit()         
	{ 

//		Debug.Log("Logged in. ID: " + FB.UserId);
//		Debug.Log("FB.IsLoggedIn: " + FB.IsLoggedIn);

		if (!FB.IsLoggedIn)                                                                    
		{          
//			Debug.Log("Begin FB Login");
			CallFBLogin();
		}

	}

	private IEnumerator WaitLogin()
	{
		while (true) 
		{
			if(isInit) { break; }
			yield return null; 
		}
	}

	private void CallFBLogin()
	{
		var perms = new List<string>(){"user_friends"};
		FB.LogInWithReadPermissions(perms, LoginCallback);
		//FB.Login("user_friends" , LoginCallback);
	}

	private void LoginCallback(ILoginResult result)                                                        
	{                                      
		Debug.Log("LoginCallback");

		isInit = true;

		if (FB.IsLoggedIn)                                                                   
		{          
			SetMyProfile();   
			//if (onLoginSuccess != null) onLoginSuccess();
		} else {
			if (onLoginFail != null) onLoginFail();
		}
	}
	
	public void RequestFacebookFriendList (KJTime.TimeDelegate onComplete = null) 
	{
		this.onComplete = onComplete;
//		FB.API("/fql?q=SELECT+uid,name,pic_square+FROM+user+WHERE+uid+IN(SELECT+uid2+FROM+friend+WHERE+uid1=me())+AND+is_app_user=1", HttpMethod.GET, FriendListCallback);
		FB.API("me?fields=id,first_name,friends.limit(100){first_name,id,picture}", HttpMethod.GET, FriendListCallback);
	}

	public void SetMyProfile () 
	{

		FB.API("/me/picture?g&width=128&height=128", HttpMethod.GET, PictureCallback);
		FB.API("me?fields=id,name,first_name,last_name", HttpMethod.GET, MyProfileCallback);  
	}

	private void FriendListCallback(IResult result)                                                                                        
	{                                                                                                                                                                                                               		
		if (result.Error != null)                                                                                                  
		{                                                                                                                          
			//Debug.LogError(result.Error);    
			Debug.LogError(result.Error);
			// Let's just try again                                                                                                
			                              
			return;                                                                                                                
		}           
		Debug.Log("FriendListCallback : " + result.RawResult);

		Dictionary<string, object> temp2;
		//temp = Json.Deserialize(result.) as Dictionary<string, object>;
		temp = (Dictionary<string, object>)result.ResultDictionary;
		foreach(KeyValuePair<string, object> entry in temp)
		{
			// do something with entry.Value or entry.Key
			Debug.Log("Key = " + entry.Key + "Value = " + entry.Value);
		}
		Dictionary<string, object> friendList = ((Dictionary<string, object>) temp["friends"]);
		foreach(KeyValuePair<string, object> entry in friendList)
		{
			// do something with entry.Value or entry.Key
			Debug.Log("Key = " + entry.Key + "Value = " + entry.Value);
		}
//		temp2 = ((Dictionary<string, object>)friendList[0]);
//		Debug.Log("uid = " + temp2["uid"]);
//		Debug.Log("data.count = " + friendList.Count);

		SocialController.allFacebookFriendList = new List<FriendItem>();
		List<object> friendData = (List<object>) friendList["data"];

//		for (int i = 0; i < friendData.Count ; i++) {
//			Debug.Log(friendData[i]);
//		}

		foreach(Dictionary<string, object> data in friendData) {
			foreach(KeyValuePair<string, object> entry in data)
			{
				// do something with entry.Value or entry.Key
				Debug.Log("Key = " + entry.Key + "Value = " + entry.Value);

			}
			FriendItem friendItem = new FriendItem();
			friendItem.realName = data["first_name"].ToString(); 
			friendItem.id = data["id"].ToString();
			Debug.Log(friendItem.realName);
			Debug.Log(friendItem.id);
			Dictionary<string, object> picture = (Dictionary<string, object>) data["picture"];
			Dictionary<string, object> data_picture = (Dictionary<string, object>) picture["data"];
			foreach(KeyValuePair<string, object> entry in data_picture)
			{
				// do something with entry.Value or entry.Key
				Debug.Log("Key = " + entry.Key + "Value = " + entry.Value);

			}

			friendItem.profilePicURL = data_picture["url"].ToString();
//			List<object> data_picture_url = 
//			friendItem.profilePicURL = ;

			// friendItem.id = (int) list["uid"];
			SocialController.allFacebookFriendList.Add(friendItem);
		}

//		foreach(KeyValuePair<string, object> entry in friendData) {
//			Debug.Log("Key = " + entry.Key + "Value = " + entry.Value);
//			FriendItem friendItem = new FriendItem();
////			friendItem.realName = (string) list["name"]; 
////			friendItem.id = list["uid"].ToString();
////			friendItem.profilePicURL = list["pic_square"].ToString();
//
////			friendItem.realName = (string) list["name"]; 
////			friendItem.id = list["id"].ToString();
////			List<object> data_picture = ((List<object>) list["friends"]);
//			//friendItem.profilePicURL = data_picture[0].ToString();
//			friendItem.profilePicURL = null;
//
//			// friendItem.id = (int) list["uid"];
//			SocialController.allFacebookFriendList.Add(friendItem);
//		}

		isSetFriendlist = true;
		if (onComplete != null) onComplete();
	}

	private void MyProfileCallback(IGraphResult result)                                                                                              
	{       

//		bool testError = true;

		if (result.Error != null ) //result.Error != null                                                                                                  
		{                                                                               
//			FbDebug.Error(result.Error);
//			Debug.LogError(result.Error);

			if (FB.IsLoggedIn) FB.LogOut();
			UIController.ShowError("ERROR", "Could not connect to Facebook: " + result.Error);

			return;                                                                                                                
		}

	//	profile = Json.Deserialize(result.RawResult) as Dictionary<string, object>; 
		profile = (Dictionary<string,object>)result.ResultDictionary;
		FacebookData.myProfile = profile;

		ProfileController.SocialName = (string) FacebookData.myProfile["name"];
		ProfileController.SocialId = (string) FacebookData.myProfile["id"];
		// TODO: In case we need to fix
//		Debug.Log( "ProfileController.SocialName " + ProfileController.SocialName);
		ProfileController.SocialUserName = "NAME_HOLDER"; //(string)FacebookData.myProfile["username"];
		//Debug.Log("SaveAllData Facebook Call");
		//DataController.SaveAllData();
		DataController.SaveSocialData();
		isSetProfile = true;
		if (onLoginSuccess != null) onLoginSuccess();
	}  
	
	private void PictureCallback(IGraphResult result)                                                                                        
	{                                                                                                                                                                                                               		
		if (result.Error != null)                                                                                                  
		{                                                                                                                          
			//Debug.LogError(result.Error);  
			Debug.LogError(result.Error);

			return;                                                                                                                
		}           
		FacebookData.myPicture = result.Texture;
	}

	private void OnHideUnity(bool isGameShown)                                                   
	{                                                                                            
		Debug.Log("OnHideUnity");                                                              
		if (!isGameShown)                                                                        
		{                                                                                        
			// pause the game - we will need to hide                                             
			Time.timeScale = 0;                                                                  
		}                                                                                        
		else                                                                                     
		{                                                                                        
			// start the game back up - we're getting focus again                                
			Time.timeScale = 1;                                                                  
		}                                                                                        
	}
}
