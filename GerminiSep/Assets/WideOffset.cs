﻿using UnityEngine;
using System.Collections;

public class WideOffset : MonoBehaviour {
	
	// public float offsetY = 0.0f;

	void Start () {
		
		float heightFactor = tk2dCamera.Instance.ScreenExtents.height / 480.0f;
		float newY = transform.localPosition.y * heightFactor;
		KJMath.SetY(transform, newY);
	}
}
