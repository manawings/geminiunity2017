﻿using UnityEngine;
using System.Collections;

public class SplashParticle : MonoBehaviour {

	public UILabel versionLabel;

	// Use this for initialization
	void Start () {
	
		versionLabel.text = "(c) 2014 ARMORGAMES INC v " + ProfileController.VersionID;
		KJEmitter emitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");

//		emitter.Deploy();
//		emitter.SetPower(100, 0, 0.5f);
//		emitter.SetEmissionRate(1000);
//		emitter.SetColor(new Color(1.0f, 0.5f, 0.2f, 1.0f), new Color(0.0f, 0.3f, 0.6f));
//		emitter.SetDecay(2.0f, 0, 0.7f);

		emitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		emitter.Deploy(new Vector3(-400, -1300, 0));
		emitter.SetRadius(500.0f);
		emitter.SetDecay(1.2f);
		emitter.SetScale(4.0f);
		emitter.SetDirection(KJMath.CIRCLE * 0.12f);
		emitter.SetColor(
			new Color (1.0f, 0.6f, 0.1f, 0.3f),
			new Color (1.0f, 0.0f, 0.0f, 0.0f));
		emitter.SetEmissionRate(2);
		emitter.SetGraphic(KJParticleGraphic.Type.Cloud);
		emitter.SetPower(1300.0f, 500.0f);
		emitter.SetChangeSize(false);
		emitter.SetSpawnBox(600, 200);
		emitter.SetOffset(Vector3.up * 900.0f);
		emitter.SetRandomRotation();
		
		emitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		emitter.Deploy(new Vector3(-100, -500, 0));
		emitter.SetRadius(500.0f);
		emitter.SetDecay(1.2f, 0.6f);
		emitter.SetScale(1.0f);
		emitter.SetDirection(KJMath.CIRCLE * 0.12f);
		emitter.SetColor(
			new Color (1.0f, 0.6f, 0.1f, 1.0f),
			new Color (1.0f, 0.0f, 0.0f, 1.0f));
		emitter.SetEmissionRate(60);
		emitter.SetOffset(new Vector3(0, -300, 0));
		emitter.SetGraphic(KJParticleGraphic.Type.Spark);
		emitter.SetPower(800.0f, 700.0f);
		emitter.SetDecceleration(0.95f, 0.05f);
		emitter.SetSpawnBox(900, 200);
		emitter.SetOffset(Vector3.up * 200.0f);

	}

}
