﻿using UnityEngine;
using System.Collections;

public class ScrollingBigBG : KJBehavior {

	float x;
	public float scrollSpeed = 5.0f;

	public float cameraFactor = 0.3f;
	private float cameraFactorInverse;
	
	// Use this for initialization
	void Start () {
		cameraTransform = Camera.main.transform;
		newPosition = transform.position;
		savedX = newPosition.x;
		AddTimer(Run);

		x = transform.position.x;

		cameraFactorInverse = 1.0f - cameraFactor;
		
	}
	
	Vector3 newPosition;
	Transform cameraTransform;
	float savedX;
	
	// Update is called once per frame
	void Run () {
		
		newPosition.y -= (scrollSpeed * DeltaTime) % (568.0f * 2.0f);
		// newPosition.x = cameraTransform.position.x * 0.5f + savedX;
		newPosition.x = cameraTransform.position.x * cameraFactorInverse + x * cameraFactor;
		transform.position = newPosition;

	}
}