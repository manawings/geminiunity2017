﻿using UnityEngine;
using System.Collections;

public class VictoryDisplayPanel : KJBehavior {

	public UISprite icon;
	public UILabel labelText;
	public UILabel labelNum;
	public KJBarGlow bar;

	float currentValue = 0.0f;
	float endValue = 0.0f;
	float maxValue = 0.0f;

	const float countTime = 2.0f;
	const float countGap = 0.05f;

	float numberIncreaser = 0;
	bool isPercent;
		
	public void Deploy (string iconName, string text, float endVal = 0, float maxVal = 0, bool isPercent = false)
	{

		if (iconName != "") {
			icon.spriteName = iconName;
			icon.MakePixelPerfect();
		}

		labelText.text = text;

		this.isPercent = isPercent;

		endValue = endVal;
		maxValue = maxVal;


		currentValue = 0;

		numberIncreaser =  endValue / (countTime / countGap);

		AddTimer(UpdatePercent, countGap);
		UpdatePercent();

		if (bar != null ) bar.SetProgress(0.0f, true);


	}
	
	void UpdatePercent ()
	{

		currentValue += numberIncreaser; 

		if (bar != null ) {
			bar.SetProgress(currentValue * 0.01f);
		}

		if( currentValue >= endValue ) {
			currentValue = endValue;
			RemoveTimerOrFlash(UpdatePercent);
		}

		if (isPercent) {
			labelNum.text = currentValue.ToString("F0") + "%";
		} else {
			labelNum.text = currentValue.ToString("F0");
		}

	}
}
