Shader "Krin/KrinVertexDarken" {
	Properties 
	{
	    _MainTex ("Base (RGB)", 2D) = "black" {}
	}
	
	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		ZWrite Off Blend SrcAlpha One Cull Off Fog { Mode Off }
		LOD 110

		CGPROGRAM
		#pragma surface surf Lambert alpha
		struct Input {
			float2 uv_MainTex;
			fixed4 color : COLOR;
		};
		
		sampler2D _MainTex;
		
		void surf(Input IN, inout SurfaceOutput o)
		{
		 	fixed4 originalColor = tex2D(_MainTex, IN.uv_MainTex);
		 	fixed4 inputColor = IN.color;
			fixed4 mainColor =  originalColor;
			
			mainColor.rgb = originalColor.rgb * inputColor.rgb;
			mainColor.a = originalColor.a * inputColor.a;
			
			o.Albedo = mainColor.rgb;
			o.Alpha = mainColor.a;
		}
		ENDCG		
	}
	
	SubShader 
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		ZWrite Off Blend SrcAlpha OneMinusSrcAlpha Cull Off Fog { Mode Off }
		LOD 100
	    Pass 
	    {
			Tags {"LightMode" = "Vertex"}
			
			ColorMaterial AmbientAndDiffuse
	        Lighting On
	        
	        SetTexture [_MainTex] 
	        {
	            Combine texture * primary double, texture * primary
	        }
	    }
	}

	Fallback "tk2d/BlendVertexColor", 1
}
