Shader "Krin/KrinVertexLighten" {

	Properties 
	{
	    _MainTex ("Base (RGB)", 2D) = "black" {}
	    _Color ("Main Color", Color) = (0, 0, 0, 1)
	}
	
	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Lighting Off
		ZWrite Off 
		Blend SrcAlpha OneMinusSrcAlpha 
		Cull Off 
		Fog { Mode Off }
		LOD 110

		CGPROGRAM
		#pragma surface surf Lambert alpha
		struct Input {
			float2 uv_MainTex;
			fixed4 color : COLOR;
		};
		
		sampler2D _MainTex;
		fixed4 _Color;
		
		void surf(Input IN, inout SurfaceOutput o)
		{
		 	fixed4 originalColor = tex2D(_MainTex, IN.uv_MainTex);
		 	fixed4 inputColor = IN.color;// _Color;
			fixed4 mainColor =  originalColor;
			
			mainColor.rgb = originalColor.rgb + (originalColor.rgb * inputColor.rgb * 5.0f) + inputColor.rgb;
			mainColor.a = originalColor.a * inputColor.a;
			
			o.Albedo = mainColor.rgb;
			o.Alpha = mainColor.a;
		}
		ENDCG		
	}
	
	SubShader 
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		ZWrite Off Blend SrcAlpha OneMinusSrcAlpha Cull Off Fog { Mode Off }
		LOD 100
	    Pass 
	    {
			Tags {"LightMode" = "Vertex"}
			
			ColorMaterial AmbientAndDiffuse
	        Lighting On
	        
	        SetTexture [_MainTex] 
	        {
	            Combine texture * primary double, texture * primary
	        }
	    }
	}

	Fallback "tk2d/BlendVertexColor", 1
}
