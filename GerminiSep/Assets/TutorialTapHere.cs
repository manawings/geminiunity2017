﻿using UnityEngine;
using System.Collections;

public class TutorialTapHere : KJBehavior {

	UISprite glowSprite;
	Color glowColor, originalColor;

	Hashtable panelTweenIn;
	Hashtable panelTweenOut;

	void Awake ()
	{
		panelTweenIn = new Hashtable();
		panelTweenIn.Add("from", 0.0f);
		panelTweenIn.Add("to", 1.0f);
		panelTweenIn.Add("time", 0.35f);
		panelTweenIn.Add("onupdate", "OnPanelTween");
		panelTweenIn.Add("oncomplete", "OnPanelCompleteIn");
		panelTweenIn.Add("easetype", iTween.EaseType.easeOutBack);
		
		panelTweenOut = new Hashtable();
		panelTweenOut.Add("from", 1.0f);
		panelTweenOut.Add("to", 0.0f);
		panelTweenOut.Add("time", 0.35f);
		panelTweenOut.Add("onupdate", "OnPanelTween");
		panelTweenOut.Add("oncomplete", "OnPanelCompleteOut");
		panelTweenOut.Add("easetype", iTween.EaseType.easeInBack);
	}

	void OnPanelTween (float val)
	{
		if (!hideTapHere) transform.localScale = new Vector2(99, 62) * val;
	}
	
	void OnPanelCompleteIn ()
	{
		// InputController.LockControlForPopUp();
	}
	
	void OnPanelCompleteOut ()
	{
		gameObject.SetActive(false);
	}

	float moveRange = 10.0f;
	bool hideTapHere = false;

	public void Deploy (GameObject targetObject, UISprite glowSprite, Color glowColor, float yOffset = 20.0f, float moveRange = 10.0f, bool hideTapHere = false) {

		gameObject.SetActive(true);
		transform.position = targetObject.transform.position;
		Vector3 newPosition =  transform.localPosition;
		newPosition.z = -120;
		transform.localPosition = newPosition;
		anchorY = transform.localPosition.y + yOffset;
		transform.localScale = Vector2.zero;
		this.moveRange = moveRange;



		this.glowSprite = glowSprite;
		this.glowColor = glowColor;

		iTween.ValueTo(gameObject, panelTweenIn);

		if (glowSprite != null) originalColor = glowSprite.color;

		this.hideTapHere = hideTapHere;

		Run ();
		AddTimer(Run);

	}

	float anchorY;

	void Run ()
	{
		float sinFactor = Mathf.Sin(Time.time * 3.0f);
		float colorFactor =  Mathf.Abs(Mathf.Sin(Time.time * 4.0f));
		float newY = anchorY + sinFactor * moveRange;

		if (glowSprite != null) glowSprite.color = Color.Lerp(glowColor, originalColor, colorFactor);

		KJMath.SetY(this, newY);
	}

	public void Hide () {

		InputController.UnlockControls();
		iTween.ValueTo(gameObject, panelTweenOut);
		RemoveTimerOrFlash(Run);
		if (glowSprite != null) glowSprite.color = originalColor;
		
	}
}
