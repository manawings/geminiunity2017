﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;
using UnityEngine.Purchasing;
using System;
using UnityEngine.Purchasing.Extension;

public class T_UnityService : MonoBehaviour, IStoreListener, IStore  {

	private IStoreController controller;
	private IExtensionProvider extensions;

	string[] comsumableID = {"HC1", "HC2", "HC3", "HC4", "HC5", "HC6"};
	string[] nonComsumableID = {"GDX"};

	// Use this for initialization
	void Start () {
		if (controller == null)
		{
			// Begin to configure our connection to Purchasing
			InitializePurchasing();
		}
	}

	void TestStats () {
		int totalPotions = 5;
		int totalCoins = 100;
		string weaponID = "Weapon_102";
		Analytics.CustomEvent("serviceTest", new Dictionary<string, object>
			{
				{ "potions", totalPotions },
				{ "coins", totalCoins },
				{ "activeWeapon", weaponID }
			});
	}

	/// <summary>
	/// Called when Unity IAP is ready to make purchases.
	/// </summary>
	public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
	{
		Debug.Log("OnInitialized: PASS");
		this.controller = controller;
		this.extensions = extensions;
		//BuyProductID("HC1");
		GetProductMeta();
	}

	public void InitializePurchasing () {

		if (IsInitialized())
		{
			return;
		}

		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
		for (int i = 0; i < comsumableID.Length; i++) {
			builder.AddProduct(comsumableID[i], ProductType.Consumable, new IDs{{comsumableID[i], MacAppStore.Name}});
		}
		for (int i = 0; i < nonComsumableID.Length; i++) {
			builder.AddProduct(nonComsumableID[i], ProductType.Consumable, new IDs{{nonComsumableID[i], MacAppStore.Name}});
		}
		UnityPurchasing.Initialize (this, builder);
	}

	private bool IsInitialized()
	{
		// Only say we are initialized if both the Purchasing references are set.
		return controller != null && extensions != null;
	}

	public void BuyConsumable(string productId)
	{
		// Buy the consumable product using its general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
		BuyProductID(productId);
	}

	void BuyProductID(string productId)
	{
		// If Purchasing has been initialized ...
		if (IsInitialized())
		{
			// ... look up the Product reference with the general product identifier and the Purchasing 
			// system's products collection.
			Product product = controller.products.WithID(productId);

			// If the look up found a product for this device's store and that product is ready to be sold ... 
			if (product != null && product.availableToPurchase)
			{
				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
				// asynchronously.
				controller.InitiatePurchase(product);
			}
			// Otherwise ...
			else
			{
				// ... report the product look-up failure situation  
				Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		// Otherwise ...
		else
		{
			// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
			// retrying initiailization.
			Debug.Log("BuyProductID FAIL. Not initialized.");
		}
	}

	public void RestorePurchases()
	{
		// If Purchasing has not yet been set up ...
		if (!IsInitialized())
		{
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer || 
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			// ... begin restoring purchases
			Debug.Log("RestorePurchases started ...");

			// Fetch the Apple store-specific subsystem.
			var apple = extensions.GetExtension<IAppleExtensions>();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
				// no purchases are available to be restored.
				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		// Otherwise ...
		else
		{
			// We are not running on an Apple device. No work is necessary to restore purchases.
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}

	/// <summary>
	/// Called when Unity IAP encounters an unrecoverable initialization error.
	///
	/// Note that this will not be called if Internet is unavailable; Unity IAP
	/// will attempt initialization until it becomes available.
	/// </summary>
	public void OnInitializeFailed (InitializationFailureReason error)
	{
	}

	/// <summary>
	/// Called when a purchase completes.
	///
	/// May be called at any time after OnInitialized().
	/// </summary>
	public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args)
	{
		// A consumable product has been purchased by this user.

		for (int i = 0; i < comsumableID.Length; i++) {
			if (String.Equals(args.purchasedProduct.definition.id, comsumableID[i], StringComparison.Ordinal))
			{
				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				// The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
				//ScoreManager.score += 100;
			}
		}

		for (int i = 0; i < nonComsumableID.Length; i++) {
			if (String.Equals(args.purchasedProduct.definition.id, nonComsumableID[i], StringComparison.Ordinal))
			{
				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			}
		}
			
		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		return PurchaseProcessingResult.Complete;
	}

	/// <summary>
	/// Called when a purchase fails.
	/// </summary>
	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
		// this reason with the user to guide their troubleshooting actions.
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}

	void GetProductMeta () {
		foreach (var product in controller.products.all) {
			Debug.Log (product.metadata.localizedTitle);
			Debug.Log (product.metadata.localizedDescription);
			Debug.Log (product.metadata.localizedPriceString);
		}
	}


	private IStoreCallback callback;
	public void Initialize (IStoreCallback callback)
	{
		this.callback = callback;   
	}

	public void RetrieveProducts (System.Collections.ObjectModel.ReadOnlyCollection<UnityEngine.Purchasing.ProductDefinition> products)
	{
		foreach (var product in products) {
			//product.
		}
		// Fetch product information and invoke callback.OnProductsRetrieved();
	}

	public void Purchase (UnityEngine.Purchasing.ProductDefinition product, string developerPayload)
	{
		// Start the purchase flow and call either callback.OnPurchaseSucceeded() or callback.OnPurchaseFailed()
	}

	public void FinishTransaction (UnityEngine.Purchasing.ProductDefinition product, string transactionId)
	{
		// Perform transaction related housekeeping 
	}

	void TestTransaction () {

		Analytics.Transaction("12345abcde", 0.99m, "USD", null, null);

	}

	void TestAttibutes () {
		Gender gender = Gender.Female;
		Analytics.SetUserGender(gender);

		int birthYear = 2014;
		Analytics.SetUserBirthYear(birthYear);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
