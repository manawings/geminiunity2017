﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif


[ExecuteInEditMode]

public class BlitzButton : MonoBehaviour {

	public GameObject actionTarget;
	public string methodName, buttonLabel;
	public int defaultPrice = 0;
	public string altPriceString = string.Empty;

	int newValue;
	public GameObject itemAlert;
	public UILabel infoLabel, priceLabel;
	public Color textColor, graphicColor, holdColor, flashColor, frameColor;
	public bool hasPrice = false;
	public bool sideLineLabel = false;
	public bool isSoftCurrency = false;
	public bool isCoreCurrency = false;
	public bool isFC = false;
	public bool isPopUp = false;
	public bool plusPrice = false;
	public bool sceneDelay = false;
	public ButtonFlashBehavior.SoundClip sound;
	public int fontSize = 28;
	public UISprite IconSprite;

	ButtonFlashBehavior _ButtonFlash;
	ButtonFlashBehavior ButtonFlash
	{
		get {
			if (_ButtonFlash == null) {
				_ButtonFlash = gameObject.GetComponentInChildren<ButtonFlashBehavior>();
			}
			return _ButtonFlash;
		}
	}

	public void SetBlueColor () {

		//  Set the Button to a blue color.

		graphicColor = new Color32 (0, 26, 56, 255);
		holdColor = new Color32 (0, 60, 125, 255);
		flashColor = new Color32 (0, 90, 235, 255);
		frameColor = new Color32 (0, 70, 130, 255);
	}

	public void SetYellowColor () {
		
		//  Set the Button to a yellow color.

		graphicColor = new Color32 (50, 30, 7, 255);
		holdColor = new Color32 (126, 70, 0, 255);
		flashColor = new Color32 (255, 215, 0, 255);
		frameColor = new Color32 (70, 40, 25, 255);
	}

	public void SetLightYellowColor () {
		
		//  Set the Button to a yellow color.
		
		graphicColor = new Color32 (58, 23, 3, 255);
		holdColor = new Color32 (126, 70, 0, 255);
		flashColor = new Color32 (236, 93, 0, 255);
		frameColor = new Color32 (130, 40, 25, 255);
	}

	public void SetTealColor () {
		
		//  Set the Button to a yellow color.
		
		graphicColor = new Color32 (0, 60, 40, 255);
		holdColor = new Color32 (0, 130, 60, 255);
		flashColor = new Color32 (0, 235, 160, 255);
		frameColor = new Color32 (0, 90, 100, 255);
	}

	public void SetOptions ()
	{
		ButtonFlash.originalColor = graphicColor;
		ButtonFlash.holdColor = holdColor;
		ButtonFlash.flashColor = flashColor;
		ButtonFlash.overrideSprite.color = graphicColor;
		ButtonFlash.frame.color = frameColor;
		ButtonFlash.buttonLabel.color = textColor;
		ButtonFlash.buttonLabel.text = buttonLabel.ToUpper();
		ButtonFlash.isPopUp = isPopUp;
		ButtonFlash.sceneDelay = sceneDelay;
		ButtonFlash.sound = sound;

		if (sideLineLabel) {
			ButtonFlash.buttonLabel.pivot = UIWidget.Pivot.Right;
			KJMath.SetX(ButtonFlash.buttonLabel, 80);
		} else {
			ButtonFlash.buttonLabel.pivot = UIWidget.Pivot.Center;
			KJMath.SetX(ButtonFlash.buttonLabel, 0);
		}
			

		if (hasPrice || isFC) {


			IconSprite.gameObject.SetActive(true);
			if (isSoftCurrency) {
				IconSprite.spriteName = "SCSmall_v3";
//				priceLabel.color = new Color32(133, 245, 255, 255);
				priceLabel.color = Color.white;

			} else if (isCoreCurrency) {

				IconSprite.spriteName = "XCSmall_v3";
				priceLabel.color = Color.white;

			} 
			else if (isFC) {

				IconSprite.spriteName = "FCSmall_v3";
				priceLabel.color = Color.white;

			} else {

				IconSprite.spriteName = "HCSmall_v3";
				priceLabel.color = new Color32(255, 232, 171, 255);
			}

			IconSprite.MakePixelPerfect();

			if (altPriceString != string.Empty) {

				priceLabel.text = altPriceString;
				priceLabel.MakePixelPerfect();

			} else {

				if (defaultPrice > 0 && !isFC) {
						priceLabel.text = defaultPrice.ToString();
					if (plusPrice) priceLabel.text =  priceLabel.text.Insert(0, "+");
					priceLabel.MakePixelPerfect();

				} else {
					priceLabel.text = string.Empty;
				}

			}



		} else {


			IconSprite.gameObject.SetActive(false);

			priceLabel.text = string.Empty;

		}

	}

	void Update ()
	{
#if UNITY_EDITOR
		if( EditorApplication.isPlaying ) return;
		SetOptions();
#endif
	}

	void OnTap ()
	{
		if (!string.IsNullOrEmpty(methodName) && actionTarget != null)
			actionTarget.SendMessage(methodName);
	}

	void Start ()
	{
		SetOptions();
		SetNewValue(0);
	}
	
	public void SetNewValue (int newValue = 0)
	{
		this.newValue = newValue;
		
		if (newValue == 0) {
			itemAlert.SetActive(false);
		} else {
			itemAlert.SetActive(true);
			infoLabel.text = newValue.ToString();
		}
	}
}
