using UnityEngine;
using System.Collections;

public class StatBar : MonoBehaviour {

	public UILabel nameLabel;
	public UILabel numLabel;
	public KJBarGlow bar;
	
	public void Deploy (string name, float statValue)
	{
		
		int newStatValue = (int) statValue;
		
		nameLabel.text = name;
		numLabel.text = newStatValue.ToString();
		
		float barFactor = statValue / 1000.0f ; // Stats.NGVForLevel(25.0f);
		bar.SetProgress(barFactor, true);
	}
}
