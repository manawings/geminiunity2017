﻿using UnityEngine;
using System.Collections;

public class MidBGCamera : KJBehavior {
	
	void Start () {
		
		newPosition = transform.position;
		cameraTransform = Camera.main.transform;
		AddTimer(Run);
	}
	
	Vector3 newPosition = new Vector3();
	float speed = 20.0f;
	Transform cameraTransform;
	
	void Run ()
	{
		
		newPosition.y += speed * DeltaTime;
		newPosition.x = cameraTransform.position.x * 0.3f;
		transform.position = newPosition;
		
	}
}
