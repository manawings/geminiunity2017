﻿using UnityEngine;
using System.Collections;

public class CommonPanel : MonoBehaviour {
	
	
	public UISprite playButton;
	public UISprite backButton;
	
	public GameObject midAnchor;
	
	public bool isBackMode;
	
	void Awake ()
	{
		midAnchor.SetActive(true);
		
		if (isBackMode)
		{
			playButton.gameObject.SetActive(false);
			backButton.gameObject.SetActive(true);
		} else {
			playButton.gameObject.SetActive(true);
			backButton.gameObject.SetActive(false);
		}
	}
	
	void OnAddHC ()
	{
		//IAPManager.RequestIAPData();
		// MenuController.LoadToScene(8);
		UIController.ShowWait();
		//UIController.ShowPopupIAP();
		IAPController.Singleton.InitializePurchasing();
		StartCoroutine(WaitForInit(10f));
//
//		if (ProfileController.Singleton.isOffline) {
//			if(!NetworkController.Singleton.CheckConnecttion) {
//				FailToConnectIAP();
//			} else {
//				UIController.ShowPopupIAP();
//			}
//			//UIController.ShowPopup("ERROR", "Social features are not available for Offline Mode. You must RESET the account from the options menu, and then sign in with an online account to access these features.");
//
//		}

	}


	IEnumerator WaitForInit(float timeOut){

		float startTime;
		startTime = Time.realtimeSinceStartup;

		while(!IAPController.Singleton.IsInitialized()){
			yield return null;
			if(Time.realtimeSinceStartup - startTime >= timeOut) break;
		}

		if(IAPController.Singleton.IsInitialized()){
			UIController.ShowPopupIAP();
		}
		else
			FailToConnectIAP();
	}

	void FailToConnectIAP ()
	{
		UIController.ShowPopUpSingleYes("Internet Problem", "Unable to connect to the internet. Please check your device's internet connection and try again.", "OK", UIController.ClosePopUp);
	}
	
	
}
