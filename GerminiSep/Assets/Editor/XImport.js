﻿ // name this say HappyScript.js (it's javascript) and drop in Editor/ folder
     
     class MyPostprocessor extends AssetPostprocessor {
 
         static function OnPostprocessAllAssets (
             importedAssets : String[],
             deletedAssets : String[],
             movedAssets : String[],
             movedFromAssetPaths : String[]) {
         
             for (var str in importedAssets){
                 Debug.Log("Reimported Asset: " + str);
                 
             }
 
             for (var str in deletedAssets)
                 Debug.Log("Deleted Asset: " + str);
 
             for (var i=0;i<movedAssets.Length;i++)
                 Debug.Log("Moved Asset: " + movedAssets[i] + " from: " + movedFromAssetPaths[i]);
             
         }
     }