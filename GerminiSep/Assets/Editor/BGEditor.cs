using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MainBGController))]

public class BGEditor : Editor {

	public override void OnInspectorGUI() {

		DrawDefaultInspector();
		MainBGController myScript = (MainBGController) target;

		if(GUILayout.Button("Copy BG Colors"))
			myScript.CopyDataToClipBoard();


	}
}