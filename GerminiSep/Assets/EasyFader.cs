﻿using UnityEngine;
using System.Collections;

public class EasyFader : MonoBehaviour {

	UILabel label;
	bool fadeOut = true;

	public Color startColor = Color.white, endColor = new Color (1.0f, 1.0f, 1.0f, 0.0f);
	float factor = 0.0f;

	void Start () {
		label = GetComponent<UILabel>();
		fadeOut = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (fadeOut) {
			factor -= Time.deltaTime * 0.7f;
			if (factor <= 0.0f) fadeOut = false;
		} else {
			factor += Time.deltaTime * 0.7f;
			if (factor >= 1.0f) fadeOut = true;
		}

		label.color = Color.Lerp(startColor, endColor, factor);
	}
}
