﻿using UnityEngine;
using System.Collections;
using System;

public class AdManager : KJSingleton<AdManager> {


	const string zoneID = "vzd02b23de0eec4748a5";

	const string tLastHC = "lastHC", tLastAd = "lastAd";
	static string lastHC = "0", lastAd = "0";
	static bool rewardHC = false;

	static int sessionShowCount = 0;
	static int sessionShowCountMax = 0; // (5) Shows every X Prompts.
	static KJTime.TimeDelegate onAdShown = null;

	protected override void OnInitialize ()
	{


//		// Assign any AdColony Delegates before calling Configure
//		AdColony.OnVideoFinished = OnAdFinished;
//		AdColony.OnVideoStarted = OnAdStart;
//
		lastHC = PlayerPrefs.GetString(tLastHC, "0");
		lastAd = PlayerPrefs.GetString(tLastAd, "0");

		rewardHC = false;

		sessionShowCount = sessionShowCountMax;
		

//		AdColony.Configure
//		(
//			"version:" +ProfileController.VersionID, // Arbitrary app version and Android app store declaration.
//			"app55d1541721884967b8",   // ADC App ID from adcolony.com
//			zoneID // A zone ID from adcolony.com
//		);

	}

	public const string AdTimeKey = "tAdTime";
	public const long MinuteGapPerAd = 1440; //1440 = 24 Hours
	string adTimeStampString = "0";
	long adTimeStamp = 0;

	public static bool CanAdBeShown
	{
		get {

			if (!CanShowAd) return false;

			Singleton.adTimeStampString = PlayerPrefs.GetString(AdTimeKey, "0");
			Singleton.adTimeStamp = long.Parse(Singleton.adTimeStampString);

			if ((Singleton.adTimeStamp + MinuteGapPerAd) < SocialController.serverTime) {
				return true;
			} else {
				return false;
			}
		}

		set {

			if (value == false) {

				Singleton.adTimeStamp = SocialController.serverTime;
				Singleton.adTimeStampString = Singleton.adTimeStamp.ToString();
				PlayerPrefs.SetString(AdTimeKey, Singleton.adTimeStampString);
				PlayerPrefs.Save();

			}

		}
	}

	public static void ToggleShowAdPopUp (KJTime.TimeDelegate _onAdShown = null) {

		onAdShown = _onAdShown;

		Debug.Log ("Can Show Ad: " + CanShowAd);
		Debug.Log ("Can Show HC: " + CanRewardHC);


		if(CanShowAd)
		{

			if (ProfileController.SectorsUnlocked > 2) {
				// Must be past at leat sector 1.
			
//				if (CanShowAd) {

//					Debug.Log ("Toggle Count: " + sessionShowCount);

					if (sessionShowCount <= 0) {

						// Last Step - The Ad hasn't reached it's timer limit.

						rewardHC = true; //CanRewardHC;
//						if (rewardHC) {
							UIController.ShowPopupViewAd();
//						} else {
//							UIController.ShowPopupViewAdSC();
//						}

						sessionShowCount = sessionShowCountMax;

					} else {

						sessionShowCount --;

					}

//				}

			}

		} else {

			Debug.Log("Video Not Available");

		}

	}

	public static void ShowAd () {
		PlayDefaultVideo ();
	}


	public static void PlayDefaultVideo () {
		PlayAd (zoneID);
	}

	static void PlayAd( string zoneID )
	{
//		Debug.Log ("AdColony Av: " + AdColony.IsVideoAvailable(zoneID));
//
//		if(AdColony.IsVideoAvailable(zoneID))
//			AdColony.ShowVideoAd(zoneID); 
	}

	public static void OnAdFinished (bool adShown)
	{
		// Ad has been shown. Give the player the promised reward.

		Debug.Log ("Ad is done! Shown: " + adShown);

		if (adShown) {

//			if (rewardHC) {
				ProfileController.Credits += 1;
				UIController.UpdateCurrency();
//			} else {
//				ProfileController.Gems += 50;
//				UIController.UpdateCurrency();
//			}

			if (onAdShown != null) {
				onAdShown();
				onAdShown = null;
			}

		}

		rewardHC = false;
//		UIController.FadeOut();

	}

	public static void OnAdStart ()
	{

		LogAdTime();
		if (rewardHC) {
			LogHCTime();
		}
			
		Debug.Log ("Ad starts!");
//		UIController.FadeIn();

	}

	static void LogAdTime () {
		lastAd =  TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalMinutes.ToString ();
		PlayerPrefs.SetString(tLastAd, lastAd);
		PlayerPrefs.Save();

//		Debug.Log ("Last Ad Logged " + lastAd);
	}

	static void LogHCTime () {
		lastHC =  TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalMinutes.ToString ();
		PlayerPrefs.SetString(tLastHC, lastHC);
		PlayerPrefs.Save();
	}

	static bool CanShowAd {
		get {
		//	return AdColony.IsVideoAvailable(zoneID);
			return false;
		}
	}

	static bool CanRewardHC {
		get {
			
			return TimeSinceEvent (lastHC, 24);
		}
	}

	static bool TimeSinceEvent (string markEvent, float timeGapInHours = 24.0f) {

		double markedTicks = 0;
		double.TryParse(markEvent, out markedTicks);

		double nowMinutes = TimeSpan.FromTicks(DateTime.UtcNow.Ticks).TotalMinutes;

//		Debug.Log ("Logged Tick: " +  markEvent + " // " + markedTicks);
//		Debug.Log ("Now Tick: " +  nowMinutes);

		double tickDifference = nowMinutes - markedTicks;
		double minutesGap = Mathf.RoundToInt ( timeGapInHours * 60);

//		Debug.Log ("Tick Difference: " +  tickDifference + " // " + minutesGap);

		if (tickDifference > minutesGap) {
			return true;
		}

		return false;
	}
}
