﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using System;

public class AdManagerUnity : MonoBehaviour
{

    void Start()
    {
		if(!isCombat)
		{
    	   ProfileController.Singleton.canShowAds = false;
     	   Singleton.ShowAds();
//			Debug.Log("Showads");
//			ShowAdSkip();

		}
    }

    private static AdManagerUnity _Singleton;
    public static AdManagerUnity Singleton
    {
        get
        {

            if (!_Singleton)
            {
                GameObject parentObject = new GameObject("AdManagerUnity");
                DontDestroyOnLoad(parentObject);
                _Singleton = parentObject.AddComponent<AdManagerUnity>();
                _Singleton.Initialize();
            }

            return _Singleton;
        }
    }

    public void Initialize()
    {
		Debug.Log("Init ads");

        if (!Advertisement.isInitialized)
            Advertisement.Initialize(ProfileController.adsUnityID, false);
    }

    void ShowAds()
    {
        try { 
            if (ProfileController.SectorId >= ProfileController.Singleton.minSectorAds)
            {
                System.DateTime timeNow = System.DateTime.Now;
                System.DateTime targetTime = System.Convert.ToDateTime(ProfileController.Singleton.targetOfferAdsTime);
                if ((targetTime - timeNow).TotalMinutes <= 0)
                {
                    //UIController.ShowPopupRevive();
					Debug.Log("Show popup ads");
                    UIController.ShowPopupViewAd();
				//	ShowAdSkip();

                }
                else if ((ProfileController.Singleton.countPlayForAds >= ProfileController.Singleton.minCountShowAds ||
					ProfileController.Singleton.currentIntervalTimer > ProfileController.Singleton.intervalAdstime)&& 
					!ProfileController.Singleton.disableSkipAds)
                {
                    ProfileController.Singleton.countPlayForAds = 0;
                    ProfileController.Singleton.currentIntervalTimer = 0;
                    ShowAdSkip();
					Debug.Log("Show  ads skip");

                }
                else
                {
                    MenuController.LoadToScene(2);
                }
                DataController.SaveAllData();
            }
            else
			{					
				MenuController.LoadToScene(2);
            }
        }
        catch (Exception e)
        {
            KJTime.Add(OnExit, 2, 1);
            print("Exception " + e);
        }
    }


	public bool isCombat = false;
	static KJTime.TimeDelegate callbackhandler = null;

	public void ShowAdHC(string zone = "",bool isCombat = false)
    {
    //    Debug.Log("ShowAd HC");
		this.isCombat = isCombat;

////		#if UNITY_EDITOR
////		StartCoroutine(WaitForAd());
////		#endif
//
//        if (string.Equals(zone, ""))
//            zone = null;
//
//        ShowOptions options = new ShowOptions();
//        options.resultCallback = AdCallbackhandler;
//		if(isCombat)
//			options.resultCallback = AdCallbackhandler3;
//		if (Advertisement.IsReady(zone))
//        {
//            Advertisement.Show("rewardedVideo", options);
//     //       Advertisement.Initialize(ProfileController.adsUnityID, false);
//        }
//        else
//        {
//			if(!isCombat)
//          	  OnExit();
//        }
//
//
//
//		if(isCombat)
//			callbackhandler = AdCallbackhandler;
//		else
//			callbackhandler = AdCallbackhandler3;

		StartCoroutine(AdConnect(60f,"rewardedVideo"));


    }

    public void ShowAdSkip(string zone = "")
    {
	//	callbackhandler = AdCallbackhandler2;

		StartCoroutine(AdConnect(30f,"video"));
    }

    void AdCallbackhandler2(ShowResult result)
    {
        Debug.Log("Call Back 2");
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("Ad Finished. Rewarding player...");
                ProfileController.Singleton.countPlayForAds = 0;
                ProfileController.Singleton.currentIntervalTimer = 0;
                break;
            case ShowResult.Skipped:
                Debug.Log("Ad skipped. Son, I am dissapointed in you");
                ProfileController.Singleton.countPlayForAds = 0;
                ProfileController.Singleton.currentIntervalTimer = 0;
                break;
            case ShowResult.Failed:
                Debug.Log("I swear this has never happened to me before");
                break;
            default:
                return;

        }

		OnExit();

    }

    void AdCallbackhandler(ShowResult result)
    {
        Debug.Log("Call Back");
        switch (result)
        {
            case ShowResult.Finished:
                ProfileController.Credits += 1;
                UIController.UpdateCurrency();
                ProfileController.Singleton.targetOfferAdsTime = System.DateTime.Now.AddHours(+1).ToString();
                ProfileController.Singleton.countPlayForAds = 0;
                ProfileController.Singleton.currentIntervalTimer = 0;
				UIController.ShowPopup("CREDIT","You have gained +1 Credit!");
                Debug.Log("Ad Finished. Rewarding player...");
                break;
            case ShowResult.Skipped:
                ProfileController.Singleton.targetOfferAdsTime = System.DateTime.Now.AddMinutes(+15).ToString();
                ProfileController.Singleton.countPlayForAds = 0;
                ProfileController.Singleton.currentIntervalTimer = 0;
                Debug.Log("Ad skipped. Son, I am dissapointed in you");
                break;
            case ShowResult.Failed:
                ProfileController.Singleton.targetOfferAdsTime = System.DateTime.Now.AddMinutes(+15).ToString();
                Debug.Log("I swear this has never happened to me before");
                break;
            default:
                OnExit();
                return;
        }

        OnExit();
    }


	void AdCallbackhandler3(ShowResult result)
	{
		Debug.Log("Call Back revive");
		switch (result)
		{
		case ShowResult.Finished:
			ProfileController.Singleton.adsReviveAvailable = false;
			GameController.Revive();
			Debug.Log("Ad Finished. Rewarding player...");
			break;
		case ShowResult.Skipped:
			ProfileController.Singleton.targetOfferAdsTime = System.DateTime.Now.AddMinutes(+15).ToString();
			ProfileController.Singleton.countPlayForAds = 0;
			ProfileController.Singleton.currentIntervalTimer = 0;
			Debug.Log("Ad skipped. Son, I am dissapointed in you");
			break;
		case ShowResult.Failed:
			ProfileController.Singleton.targetOfferAdsTime = System.DateTime.Now.AddMinutes(+15).ToString();
			Debug.Log("I swear this has never happened to me before");
			break;
		default:
			return;
		}
		//KJTime.Resume();
		UIController.popupPanel.ClosePanel();
		isCombat = false;
	}
		
	IEnumerator AdConnect(float timeOut,string adsID = ""){

		float startTime;
		startTime = Time.realtimeSinceStartup;

		while(!Advertisement.IsReady(adsID)){
			yield return null;
			if(Time.realtimeSinceStartup - startTime >= timeOut) break;
		}

		if(Advertisement.IsReady(adsID)){
			IsReadyCallback(adsID);
		}
		else
			FailedAds();
	}

	void IsReadyCallback(string adsID = ""){
		Debug.Log("ready!");
		ShowOptions options = new ShowOptions();
		options.resultCallback = AdCallbackhandler2;
		if(adsID == "rewardedVideo")
		{
			if(isCombat)
			{
				options.resultCallback = AdCallbackhandler3;
			}
			else
				options.resultCallback = AdCallbackhandler;
			
		}
	//	options.resultCallback = callbackhandler;
		Advertisement.Show(adsID, options);
	}

	void FailedAds(){
		OnExit();
	}
		

    public void OnExit()
    {
        print("OnExit");
        KJTime.Add(ExitScene, 0.5f, 1);
    }

    public void ExitScene()
    {
        print("ExitScene");
        MenuController.LoadToScene(ProfileController.Singleton.loadSectorWhenEndAds);
    }
}