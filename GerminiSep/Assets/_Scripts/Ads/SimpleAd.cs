﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class SimpleAd : MonoBehaviour 
{
	void Start () 
	{
		Debug.Log("Initialize");
		Advertisement.Initialize ("1226044", false);

		//StartCoroutine (ShowAdWhenReady());
	}

	IEnumerator ShowAdWhenReady()
	{
		while (!Advertisement.IsReady())
			yield return null;
		Advertisement.Show ();
	}
}