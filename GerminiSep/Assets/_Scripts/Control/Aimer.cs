using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Aimer : KJSpriteBehavior {

	// Use this for initialization
	
	Hashtable startTween;
	Hashtable endTween;
	tk2dSprite[] sprites;
	
	public GameObject aimerPartPrefab;
	public GameObject partsHolder;
	public tk2dSprite centerObject;
	
	List<AimerPart> aimerParts;
	
	float rotation = 0.0f;
	
	Color myColor;
	

	
	void Awake () {
		return;
		sprites = GetComponents<tk2dSprite>();
		myColor = new Color (1.0f, 0, 0, 1.0f);
		
		
		startTween = new Hashtable();
        startTween.Add("from", 0.0f);
        startTween.Add("to", 0.5f);
        startTween.Add("time", 0.25f);
        startTween.Add("onupdate", "OnUpdate");
		startTween.Add("easetype", iTween.EaseType.easeOutCirc);
		
		
		endTween = new Hashtable();
        endTween.Add("from", 0.5f);
        endTween.Add("to", 0.0f);
        endTween.Add("time", 0.25f);
        endTween.Add("onupdate", "OnUpdate");
		endTween.Add("easetype", iTween.EaseType.easeOutCirc);
		
        Deploy(3);
		
	}
	
	public void Deploy (int count)
	{
		return;
		Clear();
		
		aimerParts = new List<AimerPart>();
		float directionFactor = KJMath.CIRCLE / count;
		
		for (int i = 0 ; i < count ; i ++ )
		{
			
			AimerPart aimerPart = ((GameObject) Instantiate(aimerPartPrefab)).GetComponent<AimerPart>();
			aimerPart.direction = i * directionFactor;
			aimerParts.Add(aimerPart);
			aimerPart.transform.parent = partsHolder.transform;
			aimerPart.SetPosition();
			
		}
		
		centerObject.scale = Vector3.zero;
	}
	
	void Clear ()
	{
		return;
		partsHolder.transform.DetachChildren();
		
		if (aimerParts == null) return;
		
		for (int i = 0 ; i < aimerParts.Count ; i ++ )
		{
			Destroy(aimerParts[i].gameObject);
		}
	}
	
	public void ShowAimer ()
	{
		return;
		aimerParts.ForEach (p => p.SlideIn());
		iTween.Stop(gameObject);
		iTween.ValueTo(gameObject, startTween);
		
	}
	
	public void HideAimer ()
	{
		return;
		aimerParts.ForEach (p => p.SlideOut());
		iTween.Stop(gameObject);
		iTween.ValueTo(gameObject, endTween);
		
	}
	
	void OnUpdate (float val)
	{
		return;
		centerObject.scale = Vector3.one * val;
	}
}
