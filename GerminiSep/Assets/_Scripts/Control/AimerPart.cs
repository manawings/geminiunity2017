using UnityEngine;
using System.Collections;

public class AimerPart : KJSpriteBehavior
{
	
	private const float InDistance = 30.0f;
	private const float OutDistance = 60.0f;
	
	public float direction;
	public float distance;
	
	public float alpha = 1.0f;
	public float targetAlpha = 0.0f;
	
	Color targetColor;
	
	float targetDistance;
	
	public void SetPosition ()
	{
		targetColor = sprite.color;
		distance = targetDistance = OutDistance;
		RotationByRadians = direction;
		
		transform.localPosition = new Vector3 
			(Mathf.Sin ( direction) * distance,
			Mathf.Cos (direction) * distance,
			0.0f);
		
		targetColor.a = 0.0f;
		sprite.color = targetColor;
	}
	
	public void SlideOut ()
	{
		targetDistance = OutDistance;
		targetAlpha = 0.0f;
	}
	
	public void SlideIn ()
	{
		targetDistance = InDistance;
		targetAlpha = 1.0f;
	}
	
	void Update ()	
	{
		float distanceDifference = targetDistance - distance;
		
		float slideDistance = distanceDifference * 0.15f;
		distance += slideDistance;
		
		transform.Translate(Vector3.up * slideDistance, Space.Self);
		
		float colorDifference = targetAlpha - targetColor.a;
		targetColor.a += colorDifference * 0.15f;
		sprite.color = targetColor;
	}
	
}

