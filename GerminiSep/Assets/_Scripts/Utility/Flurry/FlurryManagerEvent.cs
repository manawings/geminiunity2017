﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlurryManagerEvent : MonoBehaviour {

	public static void startSession (string key) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.startSession(key);
//		#endif
//
//		#if UNITY_ANDROID
//		FlurryAndroid.onStartSession(key, false, false );
//		#endif
	}

	public static void logEvent (string eventName) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.logEvent(eventName, false);
//		#endif
//		
//		#if UNITY_ANDROID
//		FlurryAndroid.logEvent(eventName);
//		#endif
	}
	
	public static void logEvent (string eventName, bool isTimed) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.logEvent(eventName, isTimed );
//		#endif
//		
//		#if UNITY_ANDROID
//		FlurryAndroid.logEvent(eventName, isTimed);
//		#endif
	}

	public static void logEventWithParameters (string eventName, Dictionary<string,string> dict, bool isTimed) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.logEventWithParameters(eventName, dict, isTimed );
//		#endif
//		
//		#if UNITY_ANDROID
//		FlurryAndroid.logEvent(eventName, dict , isTimed);
//		#endif
	}

	public static void logEventWithParameters (string eventName, Dictionary<string,string> dict) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.logEventWithParameters(eventName, dict , false);
//		#endif
//		
//		#if UNITY_ANDROID
//		FlurryAndroid.logEvent(eventName, dict );
//		#endif
	}
	
	public static void endTimedEvent (string eventName ) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.endTimedEvent(eventName);
//		#endif
//		
//		#if UNITY_ANDROID
//		FlurryAndroid.endTimedEvent(eventName);
//		#endif
	}

	public static void endTimedEvent ( string eventName, Dictionary<string,string> dict ) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.endTimedEvent(eventName, dict);
//		#endif
//		
//		#if UNITY_ANDROID
//		FlurryAndroid.endTimedEvent(eventName, dict);
//		#endif
	}
	
	public static void setSessionReportsOnCloseEnabled (bool sendSessionReportsOnClose) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.setSessionReportsOnCloseEnabled( sendSessionReportsOnClose );
//		#endif
//		
//		#if UNITY_ANDROID
//
//		#endif
	}

	public static void setSessionReportsOnPauseEnabled (bool setSessionReportsOnPauseEnabled) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.setSessionReportsOnPauseEnabled( setSessionReportsOnPauseEnabled );
//		#endif
//		
//		#if UNITY_ANDROID
//
//		#endif
	}

	public static void setAge (int age) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.setAge(age);
//		#endif
//		
//		#if UNITY_ANDROID
//		FlurryAndroid.setAge(age);
//		#endif
	}

	public static void setGender (string gender) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.setGender( gender );
//		#endif
//		
//		#if UNITY_ANDROID
//
//		#endif
	}

	public static void setUserId (string id) {
//		#if UNITY_IPHONE
//		FlurryAnalytics.setUserId(id);
//		#endif
//		
//		#if UNITY_ANDROID
//		FlurryAndroid.setUserID(id);
//		#endif
	}

	public static void onEndSession () {
//		#if UNITY_ANDROID
//		FlurryAndroid.onEndSession();
//		#endif
	}
}
