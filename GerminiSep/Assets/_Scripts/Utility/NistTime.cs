﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Cache;
using System.IO;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;


public class NistTime {

	public static DateTime GetNistTime()
	{
		UIController.ShowWait();
		DateTime dateTime = DateTime.MinValue;
		HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://nist.time.gov/actualtime.cgi?lzbc=siqm9b");
		request.Method = "GET";
		request.Accept = "text/html, application/xhtml+xml, */*";
		request.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";
		request.ContentType = "application/x-www-form-urlencoded";
		request.CachePolicy = new RequestCachePolicy(RequestCacheLevel.NoCacheNoStore); //No caching
		HttpWebResponse response = (HttpWebResponse)request.GetResponse();
		if (response.StatusCode == HttpStatusCode.OK)
		{
			StreamReader stream = new StreamReader(response.GetResponseStream());
			string html = stream.ReadToEnd();//<timestamp time=\"1395772696469995\" delay=\"1395772696469995\"/>
			string time = Regex.Match(html, @"(?<=\btime="")[^""]*").Value;
			double milliseconds = Convert.ToInt64(time) / 1000.0;
			//dateTime = new DateTime(1970, 1, 1).AddMilliseconds(milliseconds).ToLocalTime();
			dateTime = new DateTime(1970, 1, 1).AddMilliseconds(milliseconds);
			//dateTime = dateTime.ToUniversalTime();
		}
		UIController.ClosePopUp();
		return dateTime;
	}

	public static string HourSession (int hour) {
		 
		DateTime dateTime = GetNistTime();
		DateTime dateTimeNow = new DateTime(dateTime.Ticks);
		int modifyTime = 0;
		if (hour != 1) { 
			modifyTime = dateTimeNow.Hour % hour;
			dateTime = dateTime.AddHours(-modifyTime);
		}
		return(Base64Encode(dateTime.Hour.ToString() + dateTime.Year.ToString()));

	}

	public static string DaySession (int day) {

		DateTime dateTime = GetNistTime();
		DateTime dateTimeNow = new DateTime(dateTime.Ticks);
		int modifyTime = 0;
		if (day != 1) {
			modifyTime = dateTimeNow.Day % day;
			dateTime = dateTime.AddDays(-modifyTime);
		}
		return(Base64Encode(dateTime.Day.ToString() + dateTime.Year.ToString()));

	}

	public static string Base64Encode(string plainText) {
		var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
		return System.Convert.ToBase64String(plainTextBytes);
	}

	DateTime FromUnixTime(long unixTime)
	{
		var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		return epoch.AddSeconds(unixTime);
	}

	long ToUnixTime(DateTime date)
	{
		var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		return Convert.ToInt64((date - epoch).TotalSeconds);
	}

}
