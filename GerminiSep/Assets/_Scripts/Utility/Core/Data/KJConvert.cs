﻿using System.Collections;
using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

public class KJConvert {

	public static List<T> EnumToList<T> () {
		return Enum.GetValues(typeof(T)).Cast<T>().ToList();
	}

	public static T[] EnumToArray<T> () {
		return Enum.GetValues(typeof(T)).Cast<T>().ToArray();
	}

	public static byte[] StringToByte(string str)
	{
		byte[] bytes = new byte[str.Length * sizeof(char)];
		System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
		return bytes;
	}
	
	public static string ByteToString(byte[] bytes)
	{
		char[] chars = new char[bytes.Length / sizeof(char)];
		System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
		return new string(chars);
	}

	static public string EncodeTo64(string toEncode)
	{
		byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
		string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
		return returnValue;
	}

	static public string DecodeFrom64(string encodedData)
	{
		byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
		string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
		return returnValue;
	}

}
