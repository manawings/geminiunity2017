﻿using UnityEngine;
using System.Collections;
using System.Linq.Expressions;
using System;

public class KJTool {
	
	public static string GetMemberName<T>(Expression<Func<T>> memberExpression)
	{
		MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
		return expressionBody.Member.Name;
	}

}
