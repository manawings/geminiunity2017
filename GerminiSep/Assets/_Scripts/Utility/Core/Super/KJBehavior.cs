using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KJBehavior : MonoBehaviour
{

	
	/** Extend this to Scripts that we want to set Active Object Delegates for.
	  * It's not required for an object to be in the pool, but
	  * it really helps with convienience. * */
	
	/** --- General Section --- */
	
	private const float Eul2Rad = -0.01745329237f;
	private const float Rad2Eul = -57.29578f;
	static Vector3 VectorRad2Eul = new Vector3(0, 0, Rad2Eul);
	public string KJName { get; private set; }
	
	public float RotationByRadians
	{
		get {
			return transform.localEulerAngles.z * Eul2Rad;
		}
		
		set {
			transform.localEulerAngles = VectorRad2Eul * value;
		}
	}
	
	/*
	Transform _transform;
	new public Transform transform {
		get {
			if (!_transform)
			{
				_transform = base.transform;	
			}
			return _transform;
		}
	}
	*/
	
	/** --- Active Object Section --- */

	bool objectFetched = false;
	KJActiveObject _activeObject = null;
	KJActiveObject activeObject {

		get {
			if (!objectFetched) {
				if (_activeObject == null) _activeObject = GetComponent<KJActiveObject>();
				objectFetched = true;
			}
			return _activeObject;
		}
	}
	
	public void InitializeDelegates ()
	{
		if (activeObject)
		{
			activeObject.AddDelegates(OnActivate, OnDeactivate);
		}
	}
	
	protected virtual void OnActivate ()
	{
		// Override this in the sub-class.
		delegateList.Clear();
	}
	
	protected virtual void OnDeactivate ()
	{
		// Override this in the sub-class.
		RemoveAllTimers();
		transform.parent = null;
	}
	
	protected void Deactivate ()
	{
		if (activeObject != null) 
		{
			activeObject.Deactivate();
		} else {
			OnDeactivate();	
			gameObject.SetActive(false);
		}
	}

	/** --- Timer Section --- */
	
	KJTime.SpaceType _TimeSpaceType;
	protected KJTime.SpaceType TimeSpaceType
	{
		get {
			return _TimeSpaceType;
		}
		
		set {
			_TimeSpaceType = value;
			_TimeSpace = KJTime.GetTimeSpaceOf(_TimeSpaceType);
		}
	}
	
	KJTime.TimeSpace _TimeSpace;
	KJTime.TimeSpace TimeSpace
	{
		get {
			if (_TimeSpace == null)
			{
				TimeSpaceType = KJTime.SpaceType.Game;
			}
			
			return _TimeSpace;
		}
	}

	List<KJTime.TimeDelegate> delegateList = new List<KJTime.TimeDelegate>();
	
	protected void AddTimer (KJTime.TimeDelegate timeDelegate, float gapInSeconds = 0.0f, int repeatCount = 0)
	{
		KJTime.Add(TimeSpaceType, timeDelegate, gameObject, gapInSeconds, repeatCount);
		delegateList.Add(timeDelegate);
	}
	
	protected void AddFlash (KJTime.TimeDelegate flashOn, KJTime.TimeDelegate flashOff, float gap = 0.15f, int timesToFlash = 3, KJTime.TimeDelegate onComplete = null)
	{
		KJTime.Flash(TimeSpaceType, flashOn, flashOff, gameObject, gap, timesToFlash, onComplete);
		delegateList.Add(flashOn);
	}
	
	protected void RemoveTimerOrFlash (KJTime.TimeDelegate timeDelegate)
	{
		/** Can also be used to be remove Flashes. */
		KJTime.Remove(timeDelegate);
		delegateList.Remove(timeDelegate);
	}
	
	protected void RemoveAllTimers ()
	{
		delegateList.ForEach( p => KJTime.Remove(p));
		delegateList.Clear();
	}
	
	protected bool IsPaused 
	{
		get {
			return TimeSpace.IsPaused;
		}
	}
	
	protected float DeltaTime
	{
		get {
			return TimeSpace.DeltaTime;
		} 
	}
}

