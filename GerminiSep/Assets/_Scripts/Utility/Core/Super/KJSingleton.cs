﻿using UnityEngine;
using System.Collections;

public abstract class KJSingleton<T>
	where T : KJSingleton<T>, new()
{
	private static T _Singleton;
	public static T Singleton
	{
		get	
		{         
			if (_Singleton == null) 
			{
				_Singleton = new T();
				_Singleton.InternalInit();
			}

			return _Singleton;
		}   
	}

	static bool hasBeenInit = false;

	public static void Initialize ()
	{
		Singleton.InternalInit();
	}

	void InternalInit ()
	{
		if (hasBeenInit) return;
		hasBeenInit = true;

		OnInitialize();
	}

	protected virtual void OnInitialize ()
	{
		// Override this!
	}

}
