﻿using UnityEngine;
using System.Collections;

public class KJSpriteBehavior : KJBehavior {

	tk2dSprite _sprite;
	protected tk2dSprite sprite {
		get {
			if (_sprite == null)
			{
				_sprite = GetComponent<tk2dSprite>();	
				if (_sprite == null) {
					_sprite = gameObject.AddComponent<tk2dSprite>();
//					Debug.Log ("NEW SPRITE: " + _sprite);
				}

			}
			return _sprite;
		}
	}
	
	tk2dSpriteAnimator _animator;
	protected tk2dSpriteAnimator animator {
		get {
			if (_animator == null)
			{
				_animator = GetComponent<tk2dSpriteAnimator>();
				if (_animator == null )_animator = gameObject.AddComponent<tk2dSpriteAnimator>();
			}
			return _animator;
		}
	}
	
	protected float FlatScale
	{
		get {
			return sprite.scale.x;
		}
		
		set {
			sprite.scale = Vector3.one * value;
		}
	}
	
	
	
	public Color flashColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	
	public void Flash (int timesToFlash = 3)
	{
		AddFlash(FlashOn, FlashOff, 0.05f, timesToFlash);
	}
	
	void FlashOn ()
	{
		sprite.color = flashColor;
	}
	
	void FlashOff ()
	{
		sprite.color = Color.black;
	}
}
