﻿
using UnityEngine;
using UnityEditor;
using System.IO;



[InitializeOnLoad]

public class KJVersion
	
{
	
	static KJVersion()    {

		EditorApplication.update += RunOnce;
		
	}
	
	
	
	static void RunOnce()    {
		
		EditorApplication.update -= RunOnce;
		ReadVersionAndIncrement();
		
	}
	
	
	
	static void ReadVersionAndIncrement()    {

		return;
		//the file name and path.  No path is the base of the Unity project directory (same level as Assets folder)

		string versionText = PlayerSettings.bundleVersion;
		if (string.IsNullOrEmpty(versionText)) versionText = "0.0.0";
		if (versionText.Split('.').Length != 3) versionText = "0.0.0";
		

			
		versionText = versionText.Trim();
		string[] lines = versionText.Split('.');

			
			
		int MajorVersion = int.Parse(lines[0]);
		int MinorVersion = int.Parse(lines[1]);
		int SubMinorVersion = int.Parse(lines[2]) + 1; //increment here
			
			
			
//		Debug.Log("Major, Minor, SubMinor: " + MajorVersion + " " + MinorVersion + " " + SubMinorVersion);
			
			
			
		versionText = 
			MajorVersion.ToString() + "." +
			MinorVersion.ToString() + "." +
			SubMinorVersion.ToString();
			
			
			Debug.Log("Version Incremented " + versionText);

		PlayerSettings.bundleVersion = versionText;
		
	}
	
}