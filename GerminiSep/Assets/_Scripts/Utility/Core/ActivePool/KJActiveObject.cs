using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/** ActiveObject is added to any GameObject that is
  * activated from the ActivePool. It has the key function of
  * SetActive() and of self-Deactivating. */

public class KJActiveObject : MonoBehaviour {
	
	public delegate void ActiveObjectDelegate ();
	
	private event ActiveObjectDelegate OnActivateEvent;
	private event ActiveObjectDelegate OnDeactivateEvent;
	
	KJActiveList activeList;

	bool isActive = false;
	
	public void AddDelegates (ActiveObjectDelegate onActivate, ActiveObjectDelegate onDeactivate)
	{
		
		/** Set the delegates for call backs. */
		
		OnActivateEvent += onActivate;
		OnDeactivateEvent += onDeactivate;
		
	}
	
	public void SetActiveList(KJActiveList newActiveList)
	{
		/** Set the ActiveList Reference.
		  * This MUST be done for the ActiveObject to work.
		  * Since this is usually done when the object is added to the list
		  * also set Active to false. */
		
		gameObject.SetActive(false);
		activeList = newActiveList;	
	}
	
	public void Activate () 
	{
		/** Prefab is activated.
		  * Enable the object and add it to the stage. */

		isActive = true;
		gameObject.SetActive(true);
		OnActivateEvent();
		
	}
	
	public void Deactivate () 
	{
		/** Active Object has been deactivated.
		  * Remove from the scene, and run the remove function
		  * from the ActiveList too. */
		if (isActive) {
			Recycle();
			activeList.Deactivate(gameObject);
			isActive = false;
		}
	}
	
	public void Recycle () 
	{
		/** Almost the same as deactivation - except does not add it back
		  * into the Free Object list. */
		
		OnDeactivateEvent();
		gameObject.SetActive(false);
		
	}
}
