using System;
using System.Collections.Generic;
using UnityEngine;

/** ActivePool is the Singleton class used to group all
  * the ActiveList. This must be attached using the EDITOR.
  * Types and amount of Prefabs can be specified using the INSPECTOR. */

// TODO: Consider making the singleton independant, as the KJTime class.

public class KJActivePool : MonoBehaviour
{
	
	/** Singleton Access */
	
	public static KJActivePool Singleton { get; private set; }
	
	/** Inspector / Public Members */
	
	#region member
	
	[Serializable]
    public class ActivePoolEntry
	{
		[SerializeField]
		public GameObject modelPrefab;
     
		[SerializeField]
		public int maxAmount;
		
		[SerializeField]
		public bool isHardCapped;
	}
	
	#endregion
	
	public ActivePoolEntry[] activePoolEntries;
	
	/** Private Members */
	
	Dictionary<string, KJActiveList> activeListDictionary;

	void Start ()
	{
		Singleton = this;
		activeListDictionary = new Dictionary<string, KJActiveList>();
		CreateAllLists();
	}
	
	public void CreateAllLists ()
	{
		
		/** Run through the Entries and create all the lists. */
		
		for ( int i = 0 ; i < activePoolEntries.Length ; i++ ) {
			CreateActiveList(activePoolEntries[i].modelPrefab.name);
		}
	}
	
	public void CreateActiveList (string prefabName) {
			
		/** Create and initialize the list for a type of
		  * active object. Put it into the dictionary for future reference. */
		
		KJActiveList activeList = new KJActiveList(prefabName);
		
		for ( int i = 0 ; i < activePoolEntries.Length ; i++ ) {
			
			if ( activePoolEntries[i].modelPrefab.name == prefabName) {
				
				ActivePoolEntry activeEntry = activePoolEntries[i];
				activeList.Initialize(activeEntry.modelPrefab, activeEntry.maxAmount, activeEntry.isHardCapped);				
				activeListDictionary.Add(prefabName, activeList);
			}
		}	
	}
	
	public static GameObject GetNew (string prefabName)
	{
		return Singleton._GetNew(prefabName);
	}
	
	public static T GetNewOf<T>(string prefabName) where T : Component
	{
		GameObject gameObject = GetNew(prefabName);
		return gameObject.GetComponent<T>();
	}
	
	GameObject _GetNew (string prefabName) {
		
		/** Find the relevant list and return a new object
		  * of the prefab type. */
		
		if (!activeListDictionary.ContainsKey(prefabName))
		{
			CreateActiveList(prefabName);
		}
		
		KJActiveList activeList = activeListDictionary[prefabName];
		return activeList.GetNew();
	}
	
	// TODO: Add Method to destroy specific or all lists for memory.
	
	public void DeactivateAll ()
	{
		foreach( KJActiveList activeList in activeListDictionary.Values)
		{
		 	activeList.DeactivateAll();
		}
	}
}
