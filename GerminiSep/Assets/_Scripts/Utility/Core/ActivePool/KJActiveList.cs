using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/** ActiveList is used by ActivePool to manage
  * all free and active objects. Pool serves as a reference point
  * and a library, but the List is where all the heavy lifting takes place. */

public class KJActiveList
{
	
	/** FREE and ACTIVE Lists. */
	
	List<GameObject> freeObjects;
	List<GameObject> activeObjects;
	
	/** Entry Members. */
	
	GameObject modelPrefab;
	int maxAmount;
	bool isHardCapped = false;
	string name;

	int objectCount = 0;

	public KJActiveList (string name)
	{
		this.name = name;
	}
	
	public void Initialize (GameObject modelPrefab, int maxAmount, bool isHardCapped){
		
		/** Must be called before the list is used.
		  * Sets all the important variables needed to function. */
		
		this.modelPrefab = modelPrefab;
		this.maxAmount = maxAmount;
		this.isHardCapped = isHardCapped;
		
		CreateAllObjects();
	}

	public void CreateAllObjects ()
	{
		/** Create all lists + objects. And populate the two lists. */
		
		freeObjects = new List<GameObject>();
		activeObjects = new List<GameObject>();
		
		for ( int i = 0 ; i < maxAmount ; i ++ ) {
			
			/** Create the GameObject. */
			CreateNewActiveObject();
		}
	}
	
	public GameObject GetNew ()
	{
		
		/** Try to get a NEW object from the FREE list.
		  * If none are FREE, then return a new one if not hard-capped.
		  * Otherwise - return the oldest ACTIVE object (after reset, of course). */
		
		GameObject proxyObject = null; // Reference Game Object

		if ( freeObjects.Count > 0 )
		{
			/** An object is FREE. */

			int objectIndex = 0; //freeObjects.Count - 1;
			proxyObject = freeObjects[objectIndex];
			freeObjects.RemoveAt(objectIndex);
	
		} else if (!isHardCapped) {
			/** Create a new object. */

			proxyObject = CreateNewActiveObject();
			
		} else {
			/** Fetch the oldest ACTIVE object. */
			proxyObject = RecycleActiveObject();
		}

		activeObjects.Add(proxyObject);
		proxyObject.GetComponent<KJActiveObject>().Activate();
		
		return proxyObject;
	}
	
	public void Deactivate (GameObject gameObjectToDeactivate)
	{
		/** Remove the target object from the active list and add it to the FREE object list. */

		activeObjects.Remove(gameObjectToDeactivate);
		freeObjects.Add(gameObjectToDeactivate);

	}
	
	public void DeactivateAll ()
	{
		for (int i = activeObjects.Count - 1 ; i > -1 ; i --)
		{
			activeObjects[i].GetComponent<KJActiveObject>().Deactivate();
		}
		activeObjects.Clear();
	}
	
	GameObject CreateNewActiveObject ()
	{
		/** Internal function to create new game objects. */
			
		GameObject proxyObject = (GameObject) GameObject.Instantiate(modelPrefab);
		 proxyObject.name += "_" + objectCount;
		objectCount ++;
		
		KJActiveObject activeObject = proxyObject.AddComponent<KJActiveObject>(); // This is nessescary to run the object.
		activeObject.SetActiveList(this);
		
		KJBehavior[] kjBehaviors = proxyObject.GetComponents<KJBehavior>();
		foreach (KJBehavior kjBehavior in kjBehaviors)
		{
			kjBehavior.InitializeDelegates();	
		}
		
		freeObjects.Add(proxyObject);
			
		return proxyObject;
	}
	
	GameObject RecycleActiveObject ()
	{
		/** Deactivate the oldest member of the ACTIVE list and return it after deactivating. */
		
		GameObject proxyObject = activeObjects[0];
		activeObjects.RemoveAt(0);
		proxyObject.GetComponent<KJActiveObject>().Recycle();
		return proxyObject;
	}
}

