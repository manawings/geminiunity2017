﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KCSoundData : MonoBehaviour {

	public KCSoundEffectClip[] clips;
	Dictionary<string, KCSoundEffectClip> soundDictionary;
	AudioSource[] audioSources;
	int currentSourceIndex = 0;
	
	[System.Serializable]
	public class KCSoundEffectClip {
		
		public string name;
		public AudioClip[] audioClip;
		[HideInInspector] public int currentIndex = 0;
		[HideInInspector] public bool hasBeenPlayed = false;
	}


	public void Initialize ()
	{
		// Create a look-up dictionary for easy reference.
		soundDictionary = new Dictionary<string, KCSoundEffectClip>();
		foreach (KCSoundEffectClip soundEffect in clips) {
			soundDictionary.Add (soundEffect.name, soundEffect);
		}

		int audioSourceCount = 35;
		audioSources = new AudioSource[audioSourceCount];

		for( int i = 0 ; i < audioSourceCount ; i ++ )
		{
			GameObject newSoundSource = new GameObject("Sound Effect Source " + i);
			audioSources[i] = newSoundSource.AddComponent<AudioSource>();
			newSoundSource.transform.parent = this.transform;
			audioSources[i].dopplerLevel = 0.0f;
			audioSources[i].maxDistance = 800.0f;
			audioSources[i].spatialBlend = 0.5f;
			audioSources[i].rolloffMode = AudioRolloffMode.Linear;
		}
	}


	AudioSource CurrentAudioSource {
		get {
			return audioSources[currentSourceIndex];
		}
	}

	public AudioSource PlaySoundById (string soundName, Vector2 position, bool randomPitch = false)
	{
//		KCSoundEffect soundEffect;
//		if (soundDictionary.TryGetValue(soundName, soundEffect)) {
//			.
//		}
		KCSoundEffectClip soundEffect = soundDictionary[soundName];
		if (soundEffect.hasBeenPlayed) return null;

		soundEffect.hasBeenPlayed = true;

		AudioSource audioSource = CurrentAudioSource;
		audioSource.volume = 1.0f;
		audioSource.clip = soundEffect.audioClip[soundEffect.currentIndex];
		if (randomPitch) audioSource.pitch = Random.value * 0.5f + 0.75f;
		else audioSource.pitch = 1.0f;

		audioSource.Play();
		audioSource.transform.position = new Vector3(position.x, position.y, Camera.main.transform.position.z);
//		soundEffect.hasBeenPlayed = true;

		if (++soundEffect.currentIndex == soundEffect.audioClip.Length) soundEffect.currentIndex = 0;
		if (++currentSourceIndex == audioSources.Length) currentSourceIndex = 0;

		return audioSource;
	}

	public void ResetPlayStatus ()
	{
		foreach (KCSoundEffectClip soundEffect in clips) {
			soundEffect.hasBeenPlayed = false;
		}
	}


}
