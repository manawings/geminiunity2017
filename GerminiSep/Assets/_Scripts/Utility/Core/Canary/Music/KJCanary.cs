﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class KJCanary : MonoBehaviour {

	
	[HideInInspector] public KCSet currentSet;

	public enum TrackType {
		
		Ruby,
		Topaz,
		Emerald,
		Sapphire,
		Ivory,
		Obsidian
		
	}
	
	public enum SetType {
		
		Set1,
		Set2,
		Set3,
		Set4,
		Set5,
		Set6,
		Set7,
		Set8,
		Set9,
		Set10,
		Set11,
		Set12
		
	}

	const string tSound = "tSound", tMusic = "tMusic";

	public static bool IsSoundEnabled;
	public static bool IsMusicEnabled;

	public List<AudioSource> fadeOutList = new List<AudioSource>();

	KCSoundData _SoundEffectData;
	public KCSoundData SoundEffectData
	{
		get {
			if (_SoundEffectData == null)
			{
				_SoundEffectData = GetComponent<KCSoundData>();
				_SoundEffectData.Initialize();
			}
			
			return _SoundEffectData;
		}
	}

	KCSet[] _Sets;
	public KCSet[] Sets
	{
		get {
			if (_Sets == null)
			{
				_Sets = GetComponents<KCSet>();
				foreach (KCSet set in _Sets) {
					if (set.lengthInSeconds == 0) {
						set.lengthInSeconds = defaultSetLength;
					}
				}
			}

			return _Sets;
		}
	}

	List<KCTrack> _Tracks;
	public List<KCTrack> Tracks
	{
		get {
			if (_Tracks == null)
			{
				_Tracks = new List<KCTrack>();
				foreach (KJCanary.TrackType trackType in (KJCanary.TrackType[]) Enum.GetValues(typeof(KJCanary.TrackType)))
				{
					KCTrack newTrack = new KCTrack(trackType, gameObject);
					_Tracks.Add(newTrack);
				}
			}
			
			return _Tracks;
		}
	}

	public float defaultSetLength = 25.6f;

	public KCSet SetOf (KJCanary.SetType type)
	{
		foreach (KCSet set in Sets) {
			if (set.type == type) return set;
		}
		return null;
	}
	
	public KCTrack TrackOf (KJCanary.TrackType type)
	{
		foreach (KCTrack track in Tracks) {
			if (track.type == type) return track;
		}
		return null;
	}


	static KJCanary _Singleton;
	public static KJCanary Singleton {
		get {
			if (_Singleton == null) {
				GameObject gameObject = (GameObject) Instantiate(Resources.Load("Canary"));
				gameObject.name = "Canary";
				_Singleton = gameObject.GetComponent<KJCanary>();
				_Singleton.Initialize();
			}

			return _Singleton;
		}
	}

	void Initialize ()
	{
		IsMusicEnabled = PlayerPrefs.GetInt(tMusic, 1) == 1 ? true : false;
		IsSoundEnabled = PlayerPrefs.GetInt(tSound, 1) == 1 ? true : false;

		DontDestroyOnLoad(gameObject);
		RenderedTrack = new KCTrack(KJCanary.TrackType.Emerald, gameObject);
		currentSet = KJMath.RandomMemberOf<KCSet>(Sets);

		fadeOutList.Clear ();
		Mute();

		// SetNextBar(0.0f);
		ExecuteNextBar();
		SetPosition();
	}

	public void SetPosition ()
	{
		transform.position = Camera.main.transform.position;
	}

	public void Mute ()
	{
		Tracks.ForEach(p => p.SetVolumeTarget(0, 0.0f));
		RenderedTrack.SetVolumeTarget(0, 0);
	}

	public static AudioSource PlaySoundEffect(string soundEffect, bool randomPitch = false)
	{
		return Singleton._PlaySoundEffect(soundEffect, Camera.main.transform.position, randomPitch);
	}

	public static AudioSource PlaySoundEffect(string soundEffect, Vector2 position, bool randomPitch = false)
	{
		if (position == null) position = Vector2.zero;
		return Singleton._PlaySoundEffect(soundEffect, position, randomPitch);
	}

	public AudioSource _PlaySoundEffect(string soundEffect, Vector2 position, bool randomPitch = false)
	{
//		return;
		if (IsSoundEnabled) {

			return SoundEffectData.PlaySoundById(soundEffect, position, randomPitch);
		}

		return null;
	}

	public static void FadeOut (AudioSource audioSource)
	{
		if (IsSoundEnabled) 
			Singleton.fadeOutList.Add (audioSource);
	}

	public void StartMusic ()
	{
		Tracks.ForEach(p => p.SetVolumeTarget(1, 5.0f));
	}

	public void StopMusic ()
	{
		Tracks.ForEach(p => p.SetVolumeTarget(0, 5.0f));
	}

	public void SwitchToCombat ()
	{
		if (!IsMusicEnabled) return;
		TrackOf(KJCanary.TrackType.Ruby).SetVolumeTarget(0.3f, 20);
		TrackOf(KJCanary.TrackType.Obsidian).SetVolumeTarget(1, 20);
		RenderedTrack.SetVolumeTarget(0, 3);
	}

	public void SwitchToAmbience ()
	{
		if (!IsMusicEnabled) return;
		TrackOf(KJCanary.TrackType.Ruby).SetVolumeTarget(1, 8);
		TrackOf(KJCanary.TrackType.Obsidian).SetVolumeTarget(0, 8);
		RenderedTrack.SetVolumeTarget(0, 3);
	}

	public static void DisableSoundEffects ()
	{
		PlayerPrefs.SetInt(tSound, 0);
		PlayerPrefs.Save();
		IsSoundEnabled = false;
	}

	public static void EnableSoundEffects ()
	{
		PlayerPrefs.SetInt(tSound, 1);
		PlayerPrefs.Save();
		IsSoundEnabled = true;
	}

	public static void DisableMusic ()
	{
		PlayerPrefs.SetInt(tMusic, 0);
		PlayerPrefs.Save();
		Singleton.Mute();
		IsMusicEnabled = false;
	}

	public static void EnableMusic ()
	{
		PlayerPrefs.SetInt(tMusic, 1);
		PlayerPrefs.Save();
		Singleton.StartMusic();
		IsMusicEnabled = true;
	}

	KCTrack RenderedTrack;
	public AudioClip bossClip1, bossClip2;

	AudioClip BossClip {

		get {

			int modLevel = ProfileController.ActualSector.id % 2;
			if (modLevel == 0) return bossClip2;
			else return bossClip1;
		}
	}

	public void SwitchToRendered ()
	{
		if (!IsMusicEnabled) return;
		StopMusic();
		RenderedTrack.StopAll();
		RenderedTrack.SetAndPlayClip(BossClip, true);
		RenderedTrack.SetVolumeTarget(1, 5);
	}
	
	void Deploy ()
	{

	}

	void Update ()
	{
		if (fadeOutList.Count > 0) {
			for (int i = fadeOutList.Count - 1 ; i > - 1 ; i -- )
			{
				AudioSource audioSource = fadeOutList[i];
				audioSource.volume -= Time.deltaTime * 1.0f;
				if (audioSource.volume <= 0) {
					audioSource.volume = 0;
					fadeOutList.RemoveAt(i);
				}
			}
		}

		SoundEffectData.ResetPlayStatus();
	}

	float nextExecute, nextPreExecute;
	
	public void RecalibrateTimers ()
	{
		float newDelay = currentSet.lengthInSeconds - Tracks[0].CurrentAudioSource.time;
		if (newDelay > 0) {
			KJTime.Add(KJTime.SpaceType.System, ExecuteNextBar, newDelay);
		}
	}

	void ExecuteNextBar ()
	{

		AutoChangeToNewSet();

		foreach (KCTrack track in Tracks) {
			KCSet.KCMiniClip miniClip = currentSet.RandomClipForTrack(track.type);
			if ( miniClip != null ) track.SetAndPlayClip(miniClip.audioClip, false, 0.0f);
		}

		KJTime.Add(KJTime.SpaceType.System, ExecuteNextBar, currentSet.lengthInSeconds);
	}

	void AutoChangeToNewSet ()
	{
		List<KCSet> possibleSets = new List<KCSet>();

		if (currentSet.canGoToAllSets) {

			foreach (KJCanary.SetType setType in (KJCanary.SetType[]) Enum.GetValues(typeof(KJCanary.SetType))) {
				possibleSets.Add(SetOf(setType));
			}

		} else {

			foreach (KJCanary.SetType setType in currentSet.setsToGoTo) {
				possibleSets.Add(SetOf(setType));
			}
		}

		currentSet = KJMath.RandomMemberOf<KCSet>(possibleSets);
	}
}
