﻿using UnityEngine;
using System.Collections;

public class KCTrack {

	AudioSource audioSource1, audioSource2, _currentAudioSource;
	public AudioSource CurrentAudioSource
	{
		get {
			if (_currentAudioSource == null) {
				_currentAudioSource = audioSource1;
			}

			return _currentAudioSource;
		}
	}


	public KJCanary.TrackType type;

	float volumeTarget, volumeChangePerSecond;
	KJTime.TimeDelegate onChangeComplete;

	float _Volume = 1.0f;
	public float Volume
	{
		get {
			return _Volume;
		}

		set {
			_Volume = value;
			if (_Volume > 1) _Volume = 1;
			if (_Volume < 0) _Volume = 0;
			audioSource1.volume = _Volume;
			audioSource2.volume = _Volume;
		}
	}

	float _Pan = 0.0f;
	public float Pan 
	{
		get {
			return _Pan;
		}
		
		set {
			_Pan = value;
			if (_Pan > 1) _Pan = 1;
			if (_Pan < -1) _Pan = -1;
			audioSource1.panStereo = _Pan;
			audioSource2.panStereo = _Pan;
		}
	}

	public KCTrack (KJCanary.TrackType type, GameObject gameObject) {

		this.type = type;
		audioSource1 = gameObject.AddComponent<AudioSource>();
		audioSource2 = gameObject.AddComponent<AudioSource>();
	}



	public void SetAndPlayClip (AudioClip audioClip, bool loop = false, float delay = 0.0f) {

		SetClip(audioClip, loop);
		if (CurrentAudioSource.clip != null) {
			if (delay == 0) {
				CurrentAudioSource.Play ();
			} else {
				CurrentAudioSource.PlayDelayed(delay);
			}
		}
	}

	public void SetClip (AudioClip audioClip, bool loop = false)
	{
		SwitchAudioSource();
		CurrentAudioSource.Stop();
		CurrentAudioSource.loop = loop;
		CurrentAudioSource.clip = audioClip;
	}

	public void Play ()
	{
		if (CurrentAudioSource.clip != null)
		CurrentAudioSource.Play();
	}

	public void StopAll ()
	{
		audioSource1.Stop();
		audioSource2.Stop();
	}

	void SwitchAudioSource ()
	{
		_currentAudioSource = _currentAudioSource == audioSource2 ? audioSource1 : audioSource2;
	}

	void RunVolumeChange ()
	{
//		Debug.Log (type.ToString()+" "+Volume);
		Volume += volumeChangePerSecond * Time.deltaTime;
	}

	void EndVolumeChange ()
	{
		Volume = volumeTarget;
		if (onChangeComplete != null) onChangeComplete();
		KJTime.Remove (RunVolumeChange);
	}


	public void SetVolumeTarget (float volumeTarget, float timeToChange = 5.0f, KJTime.TimeDelegate onChangeComplete = null)
	{
		this.volumeTarget = volumeTarget;
		this.onChangeComplete = onChangeComplete;

		if (timeToChange == 0.0f) {

			Volume = volumeTarget;

			KJTime.Remove (RunVolumeChange);
			KJTime.Remove (EndVolumeChange);

		} else {

			float volumeDifference = volumeTarget - Volume;
			volumeChangePerSecond = volumeDifference / timeToChange;
			KJTime.Add (KJTime.SpaceType.System, RunVolumeChange);
			KJTime.Add (KJTime.SpaceType.System, EndVolumeChange, timeToChange, 1);

		}
	}
}
