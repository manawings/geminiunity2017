﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class KCSet : MonoBehaviour {

	public string name;
	public KJCanary.SetType type;
	public KJCanary.SetType[] setsToGoTo;
	public bool canGoToAllSets = false;

	public float lengthInSeconds = 0.0f;

	public KCMiniClip[] clips;

	[HideInInspector] public KCMiniClip currentClip;

	[System.Serializable]
	public class KCMiniClip {

		public KJCanary.TrackType track;
		public AudioClip audioClip;

	}

	public KCMiniClip RandomClipForTrack (KJCanary.TrackType trackType)
	{
		List<KCMiniClip> eligableClips = new List<KCMiniClip>();

		foreach (KCMiniClip clip in clips) {
			if (clip.track == trackType) {
				eligableClips.Add (clip);
			}
		}

		if (eligableClips.Count > 0) return KJMath.RandomMemberOf<KCMiniClip>(eligableClips);
		else return null;
	}

	public void Deploy ()
	{

	}

}
