using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KJMath {
		
	public const float GOLDEN_RATIO = 1.6180339f;
	public const float GOLDEN_RATIO_INVERSE = 0.6180339f; // Both GOLDEN_RATIO - 1 and Inverse ( 1 / GOLDEN_RATIO)
		
	public const float CIRCLE = 6.28318530718f;// 2 PI
	public const float RIGHT_ANGLE = 1.57079632679f; // 0.5 PI

	public const float ANGLE_45 = 0.78539816339f;// 0.25 PI
	public const float INVERSE_PI = 0.31830988618f;// 1/ PI
	
	public static float ConvergeToZero ( float originalValue, float convergeFactor = 0.8f, float cutoffLimit = 0.1f )
	{
		/** Covnverge the given value towards zero. */
		if (originalValue == 0) return 0;
		if (Mathf.Abs(originalValue) < cutoffLimit) return 0;
		return originalValue * convergeFactor;
	}
		
	public static float ConvergeToTarget ( float originalValue, float targetValue, float convergeFactor = 0.3f, float cutoffLimit = 0.1f )
	{
		/** Covnverge the given value towards a given target value. */
		if ( originalValue == targetValue ) return targetValue;
		float valueDifference = targetValue - originalValue;
		if ( Mathf.Abs(valueDifference) < cutoffLimit) return targetValue;
		return originalValue + valueDifference * convergeFactor;
	}
	
	public static float Cap (float targetValue, float min = 0.0f, float max = 1.0f) 
	{
		if (targetValue < min) return min;
		if (targetValue > max) return max;
		return targetValue;
	}
	
	public static int Cap (int targetValue, int min = 0, int max = 100) 
	{
		if (targetValue < min) return min;
		if (targetValue > max) return max;
		return targetValue;
	}
	
	public static T RandomMemberOf<T>(IList list, bool removeAfterCheck = false)
	{
		int index = RandomIndexOf(list);
		T returnType = (T) list[index];
		if (removeAfterCheck) list.RemoveAt(index);
		return returnType;
	}
	
	public static int RandomIndexOf(IList list)
	{
		// Return a number that is a random index of the List's length.
		if (list.Count == 1) return 0;
		return Random.Range(0, list.Count);
	}
	
	public static float DirectionFromVector(float x, float y)
	{
		/** Finds an angle from the top co-ordinate to this vector. */
		return DirectionFromPosition(new Vector2(), new Vector2(x, y));
		//return RIGHT_ANGLE + Mathf.Atan2(-y, x);
	}
	
	public static float DirectionFromVector(Vector2 vector)
	{
		/** Finds an angle from the top co-ordinate to this vector. */
		return DirectionFromPosition(new Vector2(), vector);
		//return RIGHT_ANGLE + Mathf.Atan2(-vector.y, vector.x);
	}
	
	public static float DistanceSquared2d (Vector2 p1, Vector2 p2)
	{
		/** Squared distance between two points. Used for radial collision detection. */
		return (p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y);
	}
	
	public static float DistanceSquared2d (GameObject p1, GameObject p2)
	{
		/** Squared distance between two points. Used for radial collision detection. */
		return (p2.transform.position.x - p1.transform.position.x) * (p2.transform.position.x - p1.transform.position.x) + (p2.transform.position.y - p1.transform.position.y) * (p2.transform.position.y - p1.transform.position.y);
	}

	public static float LocalDistance (GameObject p1, GameObject p2)
	{
		/** Squared distance between two points. Used for radial collision detection. */
		return Mathf.Sqrt((p2.transform.localPosition.x - p1.transform.localPosition.x) * (p2.transform.localPosition.x - p1.transform.localPosition.x) + (p2.transform.localPosition.y - p1.transform.localPosition.y) * (p2.transform.localPosition.y - p1.transform.localPosition.y));
	}
	
	public static float DirectionFromPosition(Vector2 origin, Vector2 target)
	{
		// Finds an angle from the top co-ordinate to this vector using atan2.
		return  TreatAngle(Mathf.Atan2((origin.y - target.y), (target.x - origin.x)) + RIGHT_ANGLE);
	}
	
	public static float DirectionFromPosition(Vector3 origin, Vector3 target)
	{
		// Finds an angle from the top co-ordinate to this vector using atan2.
		return  TreatAngle(Mathf.Atan2((origin.y - target.y), (target.x - origin.x)) + RIGHT_ANGLE);
	}
	
	public static float TreatAngle(float angle)
	{
		// Treats and angle so that it returns as a radian between 0 and 2 PI.

		while ( angle >= CIRCLE )
		{
			angle -= CIRCLE;
		}
		while ( angle < 0 )
		{
			angle += CIRCLE;
		}
	
		return angle;
	}

	public static Vector2 GetClosestPointToSegment2D (Vector2 segmentStart, Vector2 segmentEnd, Vector2 targetPoint)
	{
		
		Vector2 a = segmentStart;
		Vector2 b = segmentEnd;
		Vector2 c = targetPoint;
		Vector2 ab;
		Vector2 ac;
		Vector2 returnPoint;
		
		ab.x = b.x - a.x;
		ab.y = b.y - a.y;
		ac.x = c.x - a.x;
		ac.y = c.y - a.y;
			
		float crossFactor = Vector2.Dot(ac, ab) / Vector2.Dot(ab, ab);
			
		if (crossFactor < 0) crossFactor = 0;
		if (crossFactor > 1) crossFactor = 1;
			
		returnPoint.x = a.x + crossFactor * ab.x;
		returnPoint.y = a.y + crossFactor * ab.y;
			
		return returnPoint;
	}
	
	
	
	public static void SetY (Transform transform, float val)
	{
		Vector3 newPosition = transform.localPosition;
		newPosition.y = val;
		transform.localPosition = newPosition;
	}
	
	public static void SetX (Transform transform, float val)
	{
		Vector3 newPosition = transform.localPosition;
		newPosition.x = val;
		transform.localPosition = newPosition;
	}
	
	public static void AddY (Transform transform, float val)
	{
		Vector3 newPosition = transform.localPosition;
		newPosition.y += val;
		transform.localPosition = newPosition;
	}
	
	public static void AddX (Transform transform, float val)
	{
		Vector3 newPosition = transform.localPosition;
		newPosition.x += val;
		transform.localPosition = newPosition;
	}
	
	public static void SetY (MonoBehaviour behavior, float val)
	{
		SetY (behavior.transform, val);	
	}
	
	public static void SetX (MonoBehaviour behavior, float val)
	{
		SetX (behavior.transform, val);	
	}
	
	public static void AddY (MonoBehaviour behavior, float val)
	{
		AddY (behavior.transform, val);	
	}
	
	public static void AddX (MonoBehaviour behavior, float val)
	{
		AddX (behavior.transform, val);	
	}
}
