﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KJDice {
	
	/** Math Class specially made to deal with random behavior. */

	public static int index = 0;
	public static int seed = 0;
	public static List<float> seedList = new List<float>();

	public static float NextValue { 
		get {

			if (seedList.Count == 0) SetListForSeed(99);

			index++;
			if (index == seedList.Count) index = 0;

			return seedList[index];
		}
	}

	public static void ResetUnitySeed ()
	{
		Random.seed = (10000 * System.DateTime.UtcNow.Minute) + (100 * System.DateTime.UtcNow.Second) + (System.DateTime.UtcNow.Millisecond);
	}

	public static bool SeededRoll (float probability = 0.5f)
	{
		return NextValue < probability ? true : false;
	}
	
	public static bool SeededRoll (int probability = 50)
	{
		return (NextValue * 100) < probability ? true : false;
	}

	public static bool Roll (float probability = 0.5f)
	{
		ResetUnitySeed();
		return Random.value < probability ? true : false;
	}
	
	public static bool Roll (int probability = 50)
	{
		ResetUnitySeed();
		return Random.Range(0, 100) < probability ? true : false;
	}

	public static void SetListForSeed (int _seed, int poolSize = 100) {


//		Debug.Log ("--- SET LIST FOR SEED: " + _seed);

		seed = _seed;
//		Debug.Log(_seed);
		Random.seed = seed;

		index = 0;
		seedList.Clear();
		for (int i = 0; i < poolSize; i++) {

			seedList.Add(UnityEngine.Random.value);
		}

		ResetUnitySeed();
	}

	public static int SeededRange (int startIndex, int endIndex) {

		int difference = endIndex - startIndex;
		if (difference < 1) return startIndex;

		int returnIndex =  startIndex + (int) Mathf.Floor ( NextValue * ((float) difference - 0.001f ));
		return returnIndex;
	}

	public static T RandomMemberOf<T>(IList list, bool removeAfterCheck = false)
	{
		int proxyIndex = RandomIndexOf(list);
		T returnType = (T) list[proxyIndex];
		if (removeAfterCheck) list.RemoveAt(proxyIndex);
		return returnType;
	}

	public static T RandomMeberFromEnum<T> () {
		List<T> TypeList = KJConvert.EnumToList<T>();
		T t;
		t = KJDice.RandomMemberOf<T>(TypeList);
		return t;
	}

	public static int RandomIndexOf(IList list)
	{
		// Return a number that is a random index of the List's length.
		if (list.Count == 1) return 0;
		int returnIndex = SeededRange(0, list.Count);
		return returnIndex;
	}

	public static List<T> RandomSequenceFromList<T> (List<T> model, int outputCount)
	{
		List<T> proxyList, outputList = new List<T>();
		proxyList = new List<T>(model);
		for (int i = 0 ; i < outputCount ; i++) {

			int proxyIndex = RandomIndexOf(proxyList);
			T returnType = (T) proxyList[proxyIndex];
			proxyList.RemoveAt(proxyIndex);
			if (proxyList.Count == 0) proxyList = new List<T>(model);
			outputList.Add(returnType);

		}
		return outputList;
	}

}
