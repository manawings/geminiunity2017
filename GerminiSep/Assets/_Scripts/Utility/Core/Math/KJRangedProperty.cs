﻿using UnityEngine;
using System.Collections;

public class KJRangedProperty {

	float _value;
	bool capAtZero = false;
	bool hasRange = false;
	
	float range;
	float percentRange;
		
	float min;
	float max;
	
	
		
	public KJRangedProperty (float baseValue = 1.0f, float range = 0, float percentRange = 0, bool capAtZero = false) 
	{
		SetValue (baseValue, range, percentRange, capAtZero);
	}
		
	public void CopyDataFrom(KJRangedProperty model)
	{
		/** Copy data from another KJRangedProperty. */
		SetValue(model._value, model.range, model.percentRange, model.capAtZero);
	}
		
	public void SetValue (float baseValue = 1.0f, float range = 0, float percentRange = 0, bool capAtZero = false)
	{
		_value = baseValue;

		this.range = range;
		this.percentRange = percentRange;
		this.capAtZero = capAtZero;
		
		if (range == 0 && percentRange == 0) {
			hasRange = false;
		} else {
			hasRange = true;
		}
			
		if (hasRange) {
			GetMinMax ();
		}
	}
		
	void GetMinMax ()
	{
			
		min = _value - (_value * percentRange) - range;
		max = _value + (_value * percentRange) + range;
				
		if (capAtZero) {
			if (min < 0 && _value > 0) min = 0;
			if (min > 0 && _value < 0) min = 0;
		}
				
		if (min > max) {
			float tempMin = min;
			min = max;
			max = tempMin;
		}
	}
	
	public float Value
	{
		get {
			if (hasRange) {
				return Random.Range(min, max);
			} else {
				return _value;
			}
		}
	}
}
