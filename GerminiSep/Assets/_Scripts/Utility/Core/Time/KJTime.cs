using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class KJTime : MonoBehaviour {
	
	/** Time Delegate */
	
	public delegate void TimeDelegate ();
	
	/** Time Spaces */
	
	Dictionary<SpaceType, TimeSpace> timeSpaces;
	
	/** A list and a dictionary to manage all the timers. */
	
	List<TimerPair> timeList = new List<TimerPair>();
	Dictionary<TimeDelegate, TimerPair> timeDictionary = new Dictionary<TimeDelegate, TimerPair>();
	
	List<FlashPair> flashList = new List<FlashPair>();
	Dictionary<TimeDelegate, FlashPair> flashDictionary = new Dictionary<TimeDelegate, FlashPair>();
	
	/** Reference to the GameObject. */
	
	private static GameObject parentObject;
	
	/** Time Space Types */
	
	public enum SpaceType {
		Game,
		UI,
		System
	}
	
	/** Singleton Getter */
	
	private static KJTime _Singleton;
	public static KJTime Singleton {
		get {
			if (!_Singleton)
			{
				parentObject = new GameObject("Time Controller");
				DontDestroyOnLoad(parentObject);
				_Singleton = parentObject.AddComponent<KJTime>();
				_Singleton.Initialize();
			}
			return _Singleton;
		}
	}

	private static bool mustCalibrate = false;
	
	void Update ()
	{

		/** Update the DeltaTime */
		foreach(TimeSpace proxyTimeSpace in timeSpaces.Values) 
		{
			proxyTimeSpace.Update();
		}

//		foreach (TimerPair timerPair in timeList)
//		{
//			timerPair.Update();
//		}

		timeList.ForEach ( p => p.Update());
		flashList.ForEach ( p => p.Update());
		
		if (!IsPausedFor(SpaceType.Game))
			 KJParticleController.Update(timeSpaces[SpaceType.Game].DeltaTime);

		if (mustCalibrate) CalibrateList();
	}
	
	void CalibrateList ()
	{
		timeList = new List<TimerPair>(Singleton.timeDictionary.Values);
		flashList = new List<FlashPair>(Singleton.flashDictionary.Values);
	}
	
	void Initialize ()
	{
		/** First time set-up of the timer class. */
		
		/** Set up the Time Spaces. */
		timeSpaces = new Dictionary<SpaceType, TimeSpace>();
		foreach(SpaceType proxyTimeSpace in Enum.GetValues(typeof(SpaceType)))
		{
			timeSpaces.Add (proxyTimeSpace, new TimeSpace());
		}
		
		/** Set up the Repeating Calibrations. */
		// InvokeRepeating("CalibrateList", 2.0f, 2.0f);
		Update(); // Run first update to iron out problems.
		
	}
	
	public static void SetTimeScale(SpaceType timeSpaceType, float targetScale = 1.0f, float scaleChangeFactor = 15.0f)
	{
		TimeSpace timeSpace = Singleton.timeSpaces[timeSpaceType];
		timeSpace.SetTimeScaleTarget(targetScale, scaleChangeFactor);
	}
	
	public static void Pause (SpaceType timeSpaceType)
	{
		TimeSpace timeSpace = Singleton.timeSpaces[timeSpaceType];
		timeSpace.Pause();
		
		 if (timeSpaceType == SpaceType.Game) tk2dSpriteAnimator.g_Paused = true;
	}
	
	public static void Pause ()
	{
		/** Pause all except UI and System layers. */
		Pause (SpaceType.Game);
	}
	
	public static void PauseAll ()
	{
		/** Pause all layers. */
		foreach(TimeSpace proxyTimeSpace in Singleton.timeSpaces.Values) 
		{
			proxyTimeSpace.Pause();
		}
		
		 tk2dSpriteAnimator.g_Paused = true;
	}
	
	public static void Resume (SpaceType timeSpaceType)
	{
		TimeSpace timeSpace = Singleton.timeSpaces[timeSpaceType];
		timeSpace.Resume();
		
		 if (timeSpaceType == SpaceType.Game) tk2dSpriteAnimator.g_Paused = false;
	}
	
	public static void Resume ()
	{
		Resume (SpaceType.Game);
	}
	
	public static void ResumeAll ()
	{
		/** Resume all layers. */
		foreach(TimeSpace proxyTimeSpace in Singleton.timeSpaces.Values) 
		{
			proxyTimeSpace.Resume();
		}
		
		 tk2dSpriteAnimator.g_Paused = false;
	}
	
	public static bool IsPausedFor (SpaceType timeSpaceType)
	{
		return Singleton.timeSpaces[timeSpaceType].IsPaused;
	}
	
	public static float DeltaTimeFor (SpaceType timeSpaceType)
	{
		return Singleton.timeSpaces[timeSpaceType].DeltaTime;
	}
	
	public static TimeSpace GetTimeSpaceOf (SpaceType timeSpaceType)
	{
		return Singleton.timeSpaces[timeSpaceType];
	}
	
	// ---------------------------------------------------------------------------
	
	public static void Add (SpaceType timeSpaceType, TimeDelegate timeDelegate, GameObject originObject, float gapInSeconds = 0.0f, int repeatCount = 0)
	{
		/** AddTimer a timer for a specific gameObject. */
		
		TimerPair timerPair;
			
		if (Singleton.timeDictionary.ContainsKey(timeDelegate))
		{	
			
			/** Timer already exists. */
			timerPair = Singleton.timeDictionary[timeDelegate];
		} else {
			
			/** Timer does not exist yet. */
			timerPair = new TimerPair();
			Singleton.timeDictionary.Add(timeDelegate, timerPair);
			mustCalibrate = true;
			
		}
		
		timerPair.Initialize(GetTimeSpaceOf(timeSpaceType), timeDelegate, originObject, gapInSeconds, repeatCount);
	}
	
	public static void Add (TimeDelegate timeDelegate, float gapInSeconds = 0.0f, int repeatCount = 0)
	{
		/** AddTimer a timer to the Game layer. */
		Add (SpaceType.Game, timeDelegate, Singleton.gameObject, gapInSeconds, repeatCount);
	}
	
	public static void Add (SpaceType timeSpaceType, TimeDelegate timeDelegate, float gapInSeconds = 0.0f, int repeatCount = 0)
	{
		/** AddTimer a timer to a specific layer. */
		Add (timeSpaceType, timeDelegate, Singleton.gameObject, gapInSeconds, repeatCount);
	}
	
	public static void Flash (SpaceType timeSpaceType, KJTime.TimeDelegate flashOn, KJTime.TimeDelegate flashOff, GameObject originObject, float gap = 0.15f, int timesToFlash = 3, KJTime.TimeDelegate onComplete = null)
	{
		FlashPair flashPair;
			
		if (Singleton.flashDictionary.ContainsKey(flashOn))
		{	
			
			/** Timer already exists. */
			flashPair = Singleton.flashDictionary[flashOn];
		} else {
			
			/** Timer does not exist yet. */
			flashPair = new FlashPair();
			// Singleton.flashList.Add(flashPair);
			mustCalibrate = true;
			Singleton.flashDictionary.Add(flashOn, flashPair);
			
		}
		
		flashPair.Initialize(GetTimeSpaceOf(timeSpaceType), flashOn, flashOff, originObject, gap, timesToFlash, onComplete);
		
	}
	
	public static void Flash (KJTime.TimeDelegate flashOn, KJTime.TimeDelegate flashOff, float gap = 0.15f, int timesToFlash = 3, KJTime.TimeDelegate onComplete = null)
	{
		Flash (SpaceType.Game, flashOn, flashOff, Singleton.gameObject, gap, timesToFlash, onComplete);
	}
	
	public static void Flash (SpaceType timeSpaceType, KJTime.TimeDelegate flashOn, KJTime.TimeDelegate flashOff, float gap = 0.15f, int timesToFlash = 3, KJTime.TimeDelegate onComplete = null)
	{
		Flash (timeSpaceType, flashOn, flashOff, Singleton.gameObject, gap, timesToFlash, onComplete);
	}
	
	public static void Remove (TimeDelegate timerDelegate)
	{
		if (Singleton.timeDictionary.ContainsKey(timerDelegate)) 
		{
			Singleton.timeDictionary[timerDelegate].isActive = false;
			Singleton.timeDictionary.Remove(timerDelegate);
		} 
		
		else if (Singleton.flashDictionary.ContainsKey(timerDelegate))
		{
			Singleton.flashDictionary[timerDelegate].isActive = false;
			Singleton.flashDictionary[timerDelegate].FlashOff();
			Singleton.flashDictionary.Remove(timerDelegate);
		}

		mustCalibrate = true;
	}
	
	static void RemoveTimerDirect (TimeDelegate timerDelegate)
	{
		Singleton.timeDictionary[timerDelegate].isActive = false;
		Singleton.timeDictionary.Remove(timerDelegate);

		mustCalibrate = true;
	}
	
	static void RemoveFlashDirect (TimeDelegate timerDelegate)
	{
		Singleton.flashDictionary[timerDelegate].isActive = false;
		Singleton.flashDictionary[timerDelegate].FlashOff();
		Singleton.flashDictionary.Remove(timerDelegate);

		mustCalibrate = true;
	}
	
	public static void RemoveAllFrom (GameObject referenceObject)
	{
		Singleton.timeList.ForEach (p => p.CheckAndRemove(referenceObject)) ;
		Singleton.flashList.ForEach (p => p.CheckAndRemove(referenceObject)) ;

		mustCalibrate = true;
	}

	public static void RemoveGameTimers ()
	{
		TimeSpace gameTimeSpace = Singleton.timeSpaces[SpaceType.Game];

		Singleton.timeList.ForEach (p => p.CheckAndRemoveIfTimeSpace(gameTimeSpace));
		Singleton.flashList.ForEach (p => p.CheckAndRemoveIfTimeSpace(gameTimeSpace));

		Singleton.CalibrateList();
	}

	public static void RemoveGameAndUITimers ()
	{
		TimeSpace gameTimeSpace = Singleton.timeSpaces[SpaceType.Game];
		TimeSpace uiTimeSpace = Singleton.timeSpaces[SpaceType.UI];
		
		Singleton.timeList.ForEach (p => p.CheckAndRemoveIfTimeSpace(gameTimeSpace));
		Singleton.flashList.ForEach (p => p.CheckAndRemoveIfTimeSpace(gameTimeSpace));
		Singleton.timeList.ForEach (p => p.CheckAndRemoveIfTimeSpace(uiTimeSpace));
		Singleton.flashList.ForEach (p => p.CheckAndRemoveIfTimeSpace(uiTimeSpace));

		Singleton.CalibrateList();
	}
	
	public static void RemoveAll ()
	{
		
		Singleton.timeList.ForEach (p => p.isActive = false) ;
		Singleton.flashList.ForEach (p => p.isActive = false) ;
		
		Singleton.timeDictionary.Clear();
		Singleton.timeList.Clear();
		Singleton.flashList.Clear();
		Singleton.flashDictionary.Clear();
	}
	
	public class TimeSpace
	{
		float currentScale = 1.0f, targetScale = 1.0f;
		float scaleChangeFactor = 2.0f;
		
		public bool IsPaused { get; private set; }
		public float DeltaTime { get; private set; }
		
		public void Update ()
		{
			/** Adjust the Time Scale. */
			if (currentScale != targetScale)
			{
				float scaleDifference = targetScale - currentScale;
				if (Mathf.Abs(scaleDifference) < 0.01f)
				{
					currentScale = targetScale;
				} else {
					currentScale += scaleDifference * scaleChangeFactor * Time.deltaTime;
				}
			}
			
			/** Cache the current DeltaTime */
			DeltaTime = currentScale * Time.deltaTime;
		}

		
		public void SetTimeScaleTarget (float targetScale = 1.0f, float scaleChangeFactor = 15.0f)
		{
			this.targetScale = targetScale;
			this.scaleChangeFactor = scaleChangeFactor;
		}
		
		public void Pause () { IsPaused = true; }
		public void Resume () { IsPaused = false; }
		
	}
	
	class TimerPair
	{
		/** References */
		GameObject originObject;
		KJTime.TimeDelegate timeDelegate;
		TimeSpace timeSpace;
		
		/** Functional Variables */
		int maxRepeatCount = 0, currentRepeatCount = 0;
		float maxGapInSeconds = 1.0f, currentGapInSeconds = 0.0f;
		
		/** Functional Bools */
		bool isFrameUpdate = false;
		public bool isActive = false;
		
			
		public void Initialize (TimeSpace timeSpace, KJTime.TimeDelegate timeDelegate, GameObject originObject, float gapInSeconds, int repeatCount)
		{
			this.timeSpace = timeSpace;
			this.timeDelegate = timeDelegate;
			this.originObject = originObject;
			this.maxGapInSeconds = gapInSeconds;
			this.maxRepeatCount = repeatCount;
			
			isActive = true;
			isFrameUpdate = maxGapInSeconds == 0.0f ? true : false;
				
			currentRepeatCount = 0;
			currentGapInSeconds = 0.0f;
		}
		
		public void CheckAndRemove (GameObject referenceObject)
		{
			if (originObject == referenceObject && isActive) KJTime.RemoveTimerDirect(timeDelegate);
		}

		public void CheckAndRemoveIfTimeSpace (TimeSpace timeSpace)
		{
			if (this.timeSpace == timeSpace && isActive) KJTime.RemoveTimerDirect(timeDelegate);
		}
			
		public void Update ()
		{	
			if (!isActive || timeSpace.IsPaused) return;
			
			if (isFrameUpdate) {
					
				/** Is a per-frame update. */
				timeDelegate ();
					
			} else {
					
				/** Is a timed update. */
				currentGapInSeconds += timeSpace.DeltaTime;
				
				/** Timer filled. Execute. */
				if (currentGapInSeconds >= maxGapInSeconds) {
					
					timeDelegate ();
					currentGapInSeconds = 0.0f;
					currentRepeatCount ++;
						
					/** If the repeats have been filled and the timer is still active. */
					if (currentRepeatCount == maxRepeatCount && isActive) KJTime.RemoveTimerDirect(timeDelegate);
				}	
			}
		}
	}
	
	class FlashPair {
		
		
		/** References */
		TimeSpace timeSpace;
		GameObject originObject;
		KJTime.TimeDelegate flashOn;
		KJTime.TimeDelegate flashOff;
		KJTime.TimeDelegate onComplete;
		
		/** Functional Variables */
		int timesToFlash = 0, timesFlashed = 0;
		float maxGapInSeconds = 1.0f, currentGapInSeconds = 0.0f;
		
		/** Functional Bools */
		bool hasFlashedOn = false;
		public bool isActive = false;

		
		public void Initialize (TimeSpace timeSpace, KJTime.TimeDelegate flashOn, KJTime.TimeDelegate flashOff, GameObject originObject, float gap = 0.15f, int timesToFlash = 3, KJTime.TimeDelegate onComplete = null)
		{
			
			this.timeSpace = timeSpace;
			this.flashOn = flashOn;
			this.flashOff = flashOff;
			this.onComplete = onComplete;
			this.originObject = originObject;
				
			this.timesToFlash = timesToFlash;
			timesFlashed = 0;
			maxGapInSeconds = gap;
			
			hasFlashedOn = false;
			isActive = true;
	
			Flash ();
		}
		
		public void CheckAndRemove (GameObject referenceObject)
		{
			if (originObject == referenceObject && isActive) KJTime.RemoveFlashDirect(flashOn);
		}

		public void CheckAndRemoveIfTimeSpace (TimeSpace timeSpace)
		{
			if (this.timeSpace == timeSpace && isActive) KJTime.RemoveFlashDirect(flashOn);
		}

		
		public void Update ()
		{	
			if (!isActive || timeSpace.IsPaused) return;
			
			currentGapInSeconds += timeSpace.DeltaTime;
			if (currentGapInSeconds >= maxGapInSeconds) 
			{
				currentGapInSeconds = 0.0f;
				Flash ();
			}
		}
		
		public void FlashOff ()
		{
			flashOff();
		}
			
		void Flash ()
		{
			if (!hasFlashedOn)
			{
					
				flashOn();
				hasFlashedOn = true;
				timesFlashed ++;
					
			} else {
					
				flashOff();
				hasFlashedOn = false;
					
				if (timesFlashed == timesToFlash)
				{
					if (onComplete != null) onComplete();
					if (isActive) KJTime.RemoveFlashDirect(flashOn);
				}
			}
		}		
	}
}
