using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KJParticleController
{
	/** Manages all the particles. */
	/** Manages the system to draw all particles. */
	
	private static int maxParticleCount = 400;
	
	private static List<KJParticle> activeList = new List<KJParticle>();
	private static List<KJParticle> freeList = new List<KJParticle>();
	private static List<KJParticle> removeList = new List<KJParticle>(); 
	
	public static tk2dBufferSprite bufferSprite;
	
	private static bool hasBeenInit = false;
	
	static public void Initialize (tk2dSpriteCollectionData collection)
	{
		
		if (hasBeenInit) return;
		
		hasBeenInit = true;
		
		GameObject bufferSpriteObject = new GameObject("Buffer Sprite");
		bufferSprite = bufferSpriteObject.AddComponent<tk2dBufferSprite>();
		bufferSprite.SetSprite(collection, "pRound");
		
		Object.DontDestroyOnLoad(bufferSprite);
		
		activeList.Clear();
		freeList.Clear();
		
		while (freeList.Count < maxParticleCount)
		{
			freeList.Add(new KJParticle());
		}
	}
	
	static public void Update (float DeltaTime)
	{
		if (!hasBeenInit) return;
		
//		removeList.Clear();
////		activeList.ForEach(p => );

		for (int pi = activeList.Count - 1 ; pi > -1 ; pi --)
		{
			if (activeList[pi].Run(DeltaTime)) {
				freeList.Add (activeList[pi]);
				activeList.RemoveAt(pi);
			}
		}
//			RemoveDirect(pi);

//		removeList.ForEach(p => RemoveDirect(p));
		bufferSprite.Render(activeList);
	}
	
	static public tk2dSpriteDefinition GetSpriteDefinitionFor (int spriteId)
	{
		return bufferSprite.GetSpriteFor(spriteId);	
	}
	
	static public KJParticle NewParticle 
	{
		get
		{
			
			KJParticle proxyParticle;
			
			if ( freeList.Count > 0 )
			{
				/** An object is FREE. */
				
				proxyParticle = freeList[0];
				freeList.RemoveAt(0);
				
			} else {
			
				/** Fetch the oldest ACTIVE object. */
				proxyParticle = RecycleParticle();
			}
			
			activeList.Add(proxyParticle);
			
			return proxyParticle;
		}
	}	
	
	static public KJParticle RecycleParticle ()
	{
		KJParticle proxyObject = activeList[0];
		activeList.RemoveAt(0);
		proxyObject.OnDeactivate();
		return proxyObject;
	}
	
	static public void Deactivate (KJParticle particle)
	{
		removeList.Add(particle);
	}

	static public void DeactivateAll ()
	{
		for (int pi = activeList.Count - 1 ; pi > -1 ; pi --)
		{
				activeList[pi].Deactivate();
				freeList.Add (activeList[pi]);

		}
		activeList.Clear();
	}
	
//	static private void RemoveDirect (int removeIndex)
//	{
//		freeList.Add (activeList[removeIndex]);
//		activeList.RemoveAt(removeIndex);
//
//	}
}

