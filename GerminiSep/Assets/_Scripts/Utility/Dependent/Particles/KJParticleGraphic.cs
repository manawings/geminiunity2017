using UnityEngine;
using System.Collections;

public class KJParticleGraphic
{

	public enum Type {
		Round,
		Laser,
		Glow,
		Spark,
		Pixel,
		Streak,
		Shock,
		Stun,
		Burn,
		Sonar,
		Smoke,
		Wave,
		BigGlow,
		Hex,
		Cloud
		
	}
	
	public static int GetSpriteIdFor (Type type)
	{
		switch (type)
		{
			case Type.Round: return 1;
			case Type.Laser: return 2;
			case Type.Glow: return 3;
			case Type.Pixel: return 4;
			case Type.Spark: return 5;	
			case Type.Streak: return 6;
			case Type.Sonar: return 43;
			case Type.Hex: return 44;
			case Type.BigGlow: return 45;
			case Type.Wave: return 46;
			case Type.Cloud: return 48;
			
			case Type.Shock: return Random.Range(24, 30);
			case Type.Stun: return Random.Range(37, 40);
			case Type.Burn: return Random.Range(31, 36);
			case Type.Smoke: return Random.Range(41, 43);
			
		}
		
		return 0;
	}
}

