using UnityEngine;
using System.Collections;

public class KJEmitter : KJBehavior {

	
	// Emitter Organizer.
	
	enum MotionType {
		Normal,
		Converge,
		Diverge
	}
		
	// Physical Space Emission.
	/*
	public static var MOTIONMODE_CONVERGE:String = "MOTIONMODE_CONVERGE";
	public static var MOTIONMODE_DIVERGE:String = "MOTIONMODE_DIVERGE";
	public static var MOTIONMODE_NONE:String = "MOTIONMODE_NONE";
	*/

	public Vector3 emitterPosition; // x y z to be used to spawn particles.
	private float depthZ = -5.0f;
	
	private bool isToBeDeactivated = false;

	// Emitter's Own Motion.
	private Vector3 emitterOffset = new Vector3();
	
	private Vector3 emitterVelocity = new Vector3();
	private float velocityChangeFactor = 0.0f;
		
	private float particleScale = 1.0f;
	private float scaleChangeFactor = 0.0f; // Scale changes by X% per second.
		
		// Time of Emission.
	private float emissionsPerSecond = 0;
	private float emissionsDue = 0;
		
	private float explosionTimeMax = 0.0f;
	private float explosionTime = 0.0f;
	private float explosionAmount = 1.0f;
		
	// Spawning Area
	
	float spawnRadius = 1.0f;
	bool boxSpawn = false;
	int spawnBoxX = 100, spawnBoxY = 100;
		
	// Particle Motion
	
	private bool isOrbit = false;
	private int orbitCount = 3; // How many particles to make orbit around.
	private float orbitSpeed = 0.0f;
	
	// Vars for Calculation.
	private float currentOrbitDirection = 0.0f;
	private float orbitAngleFactor = 0.0f; // Angle between each orbit particle.
	
	// private Vector3 particleAcceleration = new Vector3();
		
	// Advanced Motion Control
	public bool changeSize = true;
	private bool changeSizeInverted = false;
	private bool directional = false;
	private bool stretch = false;
	private float effectDelay = 0;
	private bool fadeAfterDelay = false;
	private bool spawnInDirection = false;
	
	// Advanced Motion Control Step II: Orbiting
	
	// Advanced Motion Control Step III: Converge / Diverge
	private float motionAcceleration = 0;
	private MotionType motionType = MotionType.Normal;
		
	// Connected Mode
	private bool connectedMode = false;
	private float minimumGap = 1.5f;
	private Vector2 currentPoint = new Vector3(); // For Calculation.
	private Vector2 oldPoint = new Vector3(); // For Calculation.
	
	// Emitter Parameters
	private KJRangedProperty direction = new KJRangedProperty(0.0f);
	private KJRangedProperty power = new KJRangedProperty(0.0f);
	private KJRangedProperty decay = new KJRangedProperty(0.5f);
	private KJRangedProperty decceleration = new KJRangedProperty(0.0f);
		
	// Color and Blending
	Color startColor = Color.white;
	Color endColor = Color.white;
	
	// Particle Initializer
	private KJParticle myParticle;

	// Particle Graphics
	private KJParticleGraphic.Type particleGraphic = KJParticleGraphic.Type.Glow;
	private float randomSize = 0;
	private bool randomRotation = false;
		
	// Emitter Attachment
	private GameObject emitContainer; // Container to emit to. If null, uses the default camera setting.
	
	private bool isContained = false; /** If the emitter is contained, particles will spawn instde the emitter's transform instead. */
	public GameObject linkedObject = null; /** Emitter's transform will always be set to follow the object. */
	public Transform linkedTransform = null; /** Emitter's transform will always be set to follow the object. */
	private bool firstUpdate = true;
	
	public void CopyFromModel (KJEmitter model)
	{
		emitterPosition = model.emitterPosition;
		depthZ = model.depthZ;

		emitterOffset = model.emitterOffset;
		emitterVelocity = model.emitterVelocity;
		velocityChangeFactor = model.velocityChangeFactor;
		
		spawnInDirection = model.spawnInDirection;
			
		particleScale = model.particleScale;
		scaleChangeFactor = model.scaleChangeFactor;
		
		emissionsPerSecond = model.emissionsPerSecond;
		
		explosionTimeMax = model.explosionTimeMax;
		explosionTime = model.explosionTime;
		explosionAmount = model.explosionAmount;
		
		spawnRadius = model.spawnRadius;
		
		connectedMode =  model.connectedMode;
		minimumGap =  model.minimumGap;
		
		direction.CopyDataFrom(model.direction);
		power.CopyDataFrom(model.power);
		decay.CopyDataFrom(model.decay);
		decceleration.CopyDataFrom(model.decceleration);
		
		startColor = model.startColor;
		endColor = model.endColor;
		
		particleGraphic = model.particleGraphic;
		randomSize = model.randomSize;
		randomRotation = model.randomRotation;
		
		isContained = model.isContained;
		
		// Advanced Motion Control III
		motionType = model.motionType;
		motionAcceleration = model.motionAcceleration;
		
		// Advanced Motion Control
		changeSize = model.changeSize;
		changeSizeInverted = model.changeSizeInverted;
		
		// Orbit Control
		isOrbit = model.isOrbit;
		orbitCount = model.orbitCount;
		orbitSpeed = model.orbitSpeed;
		orbitAngleFactor = model.orbitAngleFactor;
		
		// Area Spawning
		boxSpawn = model.boxSpawn;
		spawnBoxX = model.spawnBoxX;
		spawnBoxY = model.spawnBoxY;

		//effect delay
		effectDelay = model.effectDelay;
		fadeAfterDelay = model.fadeAfterDelay;

		/*
	

		// Particle Motion
		private bool motion3D = true;
		private bool motionDiverge = false;
		private bool motionConverge = false;
		private float motionAcceleration = 0;
		private Vector3 particleAcceleration = new Vector3();
			
		
		private bool directional = false;
		private bool stretch = false;
		private float effectDelay = 0;
		private bool fadeAfterDelay = false;
	
		private bool isContained = false; /** If the emitter is contained, particles will spawn instde the emitter's transform instead.
		
		*/
	}
	
	public void Deploy ()
	{
		Deploy (Vector3.zero);
	}
	
	public void Terminate ()
	{
		KillTimers();
		Deactivate();	
	}
	
	public void Deploy (Vector3 position)
	{

		// Instance reset and initialization here.
		// Work out how many emissions needed per frame.
			
		// isActive = true;
			
		// TODO: this.copyFromModel(MODEL_EMITTER);
		
//		Debug.Log("Deploy Emitter: " + gameObject.name);

		emitterPosition = position;
		transform.position = position;
		
		emissionsDue = 0.0f;
		
		isToBeDeactivated = false;
		firstUpdate = true;

		AddTimer (Run);
		
		// TEMP: ---
		//isLinked = true;
		
	}
	
	void SetConnectedPositions ()
	{
		currentPoint = ProjectedSpawnPosition;
		oldPoint = ProjectedSpawnPosition;
	}
		
	void Run ()
	{
		MoveEmitter();
		
		/** Check for post explosion deactivation. */
		
		if (isToBeDeactivated)
		{
			if (transform.childCount == 0) Deactivate();
			
			/** If the emitter is marked to be deactivated, do not run any further. */
			return;
		}
		
		/* Spawn the Connnected Mode */
		SpawnConnectedMode();
		
		/* Increase the emission. */
		emissionsDue += emissionsPerSecond * DeltaTime;
		
		/* Run the orbit mode */
		currentOrbitDirection += orbitSpeed;
		
		/* Spawn the regular particle. */
		SpawnParticles();
		
		/** Run the explosion. */
		if (explosionTime > 0) {
			explosionTime -= DeltaTime;
			if (explosionTime <= 0) isToBeDeactivated = true;
		}
	}
	
	void SpawnConnectedMode ()
	{
		if (connectedMode)
		{
			// Spawn Particles in a line from the previous point to the new one.
			
			currentPoint = ProjectedSpawnPosition;
				
			float distanceGap = Vector3.Distance(currentPoint, oldPoint);
			
			if (distanceGap > minimumGap) {
				// It is a wider gap, so time to spawn.
				
				float spawnCountMax = distanceGap / minimumGap;
				float spawnCount = spawnCountMax;
					
				while (spawnCount > 0) {
	
					Vector3 newSpawnPoint = Vector3.Lerp (currentPoint, oldPoint, spawnCount / spawnCountMax);
					EmitSingleParticle(newSpawnPoint, 1.0f);
					spawnCount --;
				}
			}
				
			oldPoint = currentPoint;
		}
	}
	
	void MoveEmitter ()
	{
		// Move the Emitter
			
		if (linkedTransform != null) {
			emitterPosition = transform.position = linkedTransform.position;
		} else {
			emitterPosition = transform.position;
		}

		emitterOffset += emitterVelocity * DeltaTime;
		
		float finalVelocityFactor = 1.0f + velocityChangeFactor * DeltaTime;
		float finalScaleFactor = 1.0f + scaleChangeFactor * DeltaTime;
		
		emitterVelocity *= finalVelocityFactor;
		particleScale *= finalScaleFactor;
		
		if (firstUpdate) 
		{
			SetConnectedPositions();
			firstUpdate = false;
		}
		
	}
	
	public void Explode (Vector3 emitterVelocity, Vector3 position)
	{
		// Release a burst of particles
		Deploy(position);
		
		this.emitterVelocity = emitterVelocity;
		emitterOffset = Vector3.zero;
		explosionTime = explosionTimeMax;
		
		if (explosionTime == 0) {
			emissionsDue = explosionAmount;
			SpawnParticles();
			Deactivate();
		} else {
			emissionsPerSecond = explosionAmount / explosionTimeMax;
		}	
	}
	
	public void SpawnLine (Vector2 fromPosition, Vector2 toPosition, int particlesPer100px = 20, float endDecayFactor = 0.0f)
	{
		// Spawn Particles in a line from point A to B.
				
		float distanceGap = Vector3.Distance(fromPosition, toPosition);
		int particleSpawnCount = (int) (distanceGap * 0.01f /* Divide by 100 */) * particlesPer100px;
		float spawnPositionFactor = 1.0f;
			
		while (particleSpawnCount > 0) {
	
			particleSpawnCount --;
			spawnPositionFactor = Random.value;
				
			Vector3 newSpawnPoint = Vector3.Lerp (fromPosition, toPosition, spawnPositionFactor);
			
			float decayFactor = 1.0f + spawnPositionFactor * endDecayFactor;
			EmitSingleParticle(newSpawnPoint, decayFactor);
			particleSpawnCount --;
		}
	}
	
	void SpawnParticles ()
	{
		while (emissionsDue > 0) {
			
			
			if (isOrbit)
			{
				int spawnCount = orbitCount;
				while (spawnCount > 0)
				{
					EmitSingleParticle(Vector3.zero, 1.0f, spawnCount);	
					spawnCount --;
				}
			} else {
				EmitSingleParticle();
			}
			
				
			// Manage the emissionCount
			emissionsDue--;
		}
	}
	
	void EmitSingleParticle ()
	{
		EmitSingleParticle(Vector3.zero, 1.0f);
	}
	
	Vector3 ProjectedSpawnPosition {
		get
		{
			Vector3 projectedSpawnPosition = new Vector3();
			if (!isContained) projectedSpawnPosition += emitterPosition;
			projectedSpawnPosition += emitterOffset;
			return projectedSpawnPosition;
		}
	}
	
	void EmitSingleParticle (Vector3 position, float decayMultiplier = 1.0f, int orbitSequence = 0)
	{
		
		// Emit a Particle!
		
		
		float useScaler = particleScale;//  * (1.0f + Random.Range(-randomSize, randomSize));
		
		myParticle = KJParticleController.NewParticle;
		myParticle.Deploy(useScaler);
		
		if (isContained) {
			myParticle.LinkedTransform = transform;
			myParticle.SetOffset(emitterOffset);
		}
		
		Vector3 spawnPosition = new Vector3();
			
		if (position == Vector3.zero)
		{
			spawnPosition = ProjectedSpawnPosition;
		} else {
			spawnPosition = position;	
		}
		
		
		float spawnAngle = 0.0f;
		float spawnDistance = 0.0f;
		
		if (spawnRadius != 0)
		{
			if (isOrbit)
			{
				spawnAngle = currentOrbitDirection + orbitSequence * orbitAngleFactor;
				spawnDistance = spawnRadius;
			} else {
				spawnAngle = Random.Range(0, KJMath.CIRCLE);
				spawnDistance = Random.Range(0, spawnRadius * particleScale);
				if (spawnDistance < 1.0f) spawnDistance = 0.0f;
			}
		}

		// Set the Particle's Motion
		
		float usePower = power.Value;
		float useDecay = decay.Value * decayMultiplier;
		float useDirection = direction.Value;
		
		/// MOTION
		/// Warning: Does not calculate for Decceleration. Decceleration should be set to ZERO.
		if (motionType == MotionType.Normal) {
		
			if (boxSpawn)
			{
			
				spawnPosition.x += Random.Range(-spawnBoxX, spawnBoxX);
				spawnPosition.y += Random.Range(-spawnBoxY, spawnBoxY);
				
			} else {
				
				spawnPosition.x += Mathf.Sin(spawnAngle) * spawnDistance;
				spawnPosition.y += Mathf.Cos(spawnAngle) * spawnDistance;
				
			}
			
			
		} else {
			
			useDirection = spawnAngle;
			
			if (motionType == MotionType.Converge) {
			
				// Factor in movement, so that the particle ends its motion within the spawn radius.
				// Does not take static acceleration into account.

				float projectedDistance = spawnRadius + (usePower * useDecay) + (motionAcceleration * useDecay * useDecay) / 2.0f;
				
				spawnPosition.x -= Mathf.Sin(spawnAngle) * projectedDistance;
				spawnPosition.y -= Mathf.Cos(spawnAngle) * projectedDistance;
				
			} else {
				
				spawnPosition.x += Mathf.Sin(spawnAngle) * spawnDistance;
				spawnPosition.y += Mathf.Cos(spawnAngle) * spawnDistance;
				
			}
		} 	
		
		
		/// END MOTION

		/** Transfer the data to the particle. */
		
		spawnPosition.z = -5.0f;
		myParticle.SetXYZ(spawnPosition);
		
		myParticle.SetParticleGraphic(particleGraphic);
		myParticle.SetDirectionInfo(useDirection, 0.0f, usePower, motionAcceleration, decceleration.Value);
		// myParticle.ModifyAcceleration(particleAcceleration);
		myParticle.SetDecayInfo(useDecay, effectDelay, changeSize, changeSizeInverted, directional, stretch, fadeAfterDelay);
		myParticle.SetColorInfo(startColor, endColor);

		if (spawnInDirection) {
			myParticle.SetRotation(useDirection);
		} else if (randomRotation) {
			myParticle.SetRotation( Random.Range(0, KJMath.CIRCLE) );
		} else {
			myParticle.SetRotation ( direction.Value );
		}

		
	}
	
	/** Setter Functions */
	
	public void SetChangeSize (bool changeSize = true, bool inverted = false)
	{
		this.changeSize = changeSize;
		this.changeSizeInverted = inverted;
	}
	
	public void SetEmissionRate (int emissionsPerSecond)
	{
		this.emissionsPerSecond = emissionsPerSecond;
	}
	
	public void SetColor (Color startColor, Color endColor)
	{
		/** Set starting and ending colors for the particles. */
		this.startColor = startColor;
		this.endColor = endColor;
	}
	
	public void SetRadius (float spawnRadius)
	{
		/** Set spawning radius for the particles from the center. */
		this.spawnRadius = spawnRadius;
	}
	
	public void SetScale (float particleScale)
	{
		this.particleScale = particleScale;	
	}
	
	public void SetExplosion (int explosionAmount = 0, float explosionTimeMax = 0.0f, float velocityChangeFactor = 0.0f, float scaleChangeFactor = 0.0f)
	{
		/** Set generic explosion parameters. */
		this.explosionAmount = explosionAmount;
		this.explosionTimeMax = explosionTimeMax;
		this.velocityChangeFactor = velocityChangeFactor;
		this.scaleChangeFactor = scaleChangeFactor;
	}

	public void SetRandomRotation ()
	{
		randomRotation = true;
	}
		
	public void SetDirection (float baseValue = 0.0f, float range = 0.0f, float percentRange = 0.0f)
	{
		direction.SetValue(baseValue, range, percentRange, false);
	}
	
	public void SetPower (float baseValue = 0.0f, float range = 0.0f, float percentRange = 0.0f)
	{
		power.SetValue(baseValue, range, percentRange, true);
	}
	
	public void SetDecay (float baseValue = 1.0f, float range = 0.0f, float percentRange = 0.0f)
	{
		decay.SetValue(baseValue, range, percentRange, true);
	}
	
	public void SetDecceleration (float baseValue = 1.0f, float range = 0.0f, float percentRange = 0.0f)
	{
		decceleration.SetValue(baseValue, range, percentRange, true);
	}
	
	public void SetObjectLink (GameObject linkedObject)
	{
		//Debug.Log(gameObject.name + " Set Object Link To: " + linkedObject.name);
		this.linkedObject = linkedObject;
		linkedTransform = linkedObject.transform;
	}
	
	public void SetIsContained (bool isContained)
	{
		this.isContained = isContained;
	}
	
	public void SetGraphic (KJParticleGraphic.Type type)
	{
		particleGraphic = type;	
	}
	
	public void SetConnectedMode (float minimumGap)
	{
		if (minimumGap > 0.0f) {
			connectedMode = true;	
		} else {
			connectedMode = false;	
		}
		
		this.minimumGap = minimumGap;
	}
	
	public void SetOffset (Vector3 offset)
	{
		emitterOffset = offset;	
	}
	
	public void SetSpawnInDirection (bool spawnInDirection)
	{
		// Whether particles spawn in the original direction of the emitter.
		this.spawnInDirection = spawnInDirection;
	}
	
	public void SetMotionConverge (float acceleration = 0.0f)
	{
		motionAcceleration = acceleration;
		motionType = MotionType.Converge;	
		decceleration.SetValue(0.0f);
	}
	
	public void SetMotionDiverge (float acceleration = 0.0f)
	{
		motionAcceleration = acceleration;
		motionType = MotionType.Diverge;
		
	}

	public void SetEffectDelay (float delay = 0.0f)
	{
		fadeAfterDelay = true;
		effectDelay = delay;
	}
	
	public void SetOrbit (float orbitSpeed = 0.15f, int orbitCount = 3)
	{
		isOrbit = true;
		this.orbitSpeed = orbitSpeed;
		this.orbitCount = orbitCount;
		orbitAngleFactor = KJMath.CIRCLE / orbitCount;
	}
	
	
	public void SetSpawnBox (int spawnX = 100, int spawnY = 100)
	{
		
		// Set a rectangular area for particles to spawn in.
		
		boxSpawn = true;
		spawnBoxX = spawnX / 2;
		spawnBoxY = spawnY / 2;
		spawnRadius = 0.0f;
	}
	
	protected override void OnDeactivate ()
	{
		linkedObject = null;
		linkedTransform = null;
		base.OnDeactivate();	
	}
	
	public void KillTimers ()
	{
		RemoveTimerOrFlash (Run);
	}
	
	
	
	/*

		
	public function setSpawnScale(scale:Number = 1):void
	{
		spawnScaler = scale;
	}
		
	public function spawnBetweenPoints(p1:Point, p2:Point, count:int = 10):void
	{

		var anchorRatio:Number = 1;
		var anchorRatioInverse:Number = 0;
		var anchorGap:Number = 1 / count;
		var interpolationFactor:Number = 0;
		var newPoint:Point;
		var anchorPoint:Number = KJMath.randomFloatInRange(0, 0.3);
			
		while (count > 0)
		{
			interpolationFactor = KJMath.randomFloatInRange(0, 1) * anchorRatioInverse + anchorPoint * anchorRatio;
			newPoint = Point.interpolate(p2, p1, interpolationFactor);
				
			anchorRatio -= anchorGap;
			anchorRatioInverse += anchorGap;
				
			emitSingleParticle(newPoint.x, newPoint.y, 0, anchorRatio);
			count --;
		}
	}
		
	// Methods for setting the Particle Emitter's Parameters.
	
	// Method 1
	public function setEmissionRate(emissionsPerSecond:int = 0):void
	{
		// Set the regular emission rate for the particles.
		this.emissionsPerSecond = emissionsPerSecond;
		emissionsPerFrame = emissionsPerSecond / KJDisplay.frameRate;
		emissionsDue = 0;
	}
		
	// Method 2
	public function setExplosionParameters(explosionAmount:int = 0, explosionTimeInSeconds:Number = 0):void
	{
		// Set the paramters for an explosion.
		this.explosionAmount = explosionAmount;
		explosionTime = explosionTimeInSeconds;
		explosionRemainingFrames =  int(explosionTime * KJDisplay.frameRate);
	}
		
	// Method 3
	public function setSpawnRadius(spawnRadiusInPixels:uint = 0):void
	{
		spawnRadius = spawnRadiusInPixels;
	}
		
	// Method 4
	public function setConnectedMode(isActive:Boolean = false, gapInPixels:Number = 1.5):void
	{
		// Whether the Emitter draws a 'line'.
		connectedMode = isActive;
		minimumGap = gapInPixels;
	}
		
	// Method 5
	public function setDirection(value:Number = 1, range:Number = 0, percentRange:Number = 0, capAtZero:Boolean = false):void
	{
		direction.setValue(value, range, percentRange, capAtZero);
	}
		
	// Method 6
	public function setPower(value:Number = 1, range:Number = 0, percentRange:Number = 0, capAtZero:Boolean = false):void
	{
		power.setValue(value, range, percentRange, capAtZero);
	}
		
	// Method 7
	public function setDecceleration(value:Number = 1, range:Number = 0, percentRange:Number = 0, capAtZero:Boolean = false):void
	{
		decceleration.setValue(value, range, percentRange, capAtZero);
	}
		
	// Method 8
	public function setDecay(value:Number = 1, range:Number = 0, percentRange:Number = 0, capAtZero:Boolean = false):void
	{
		decay.setValue(value, range, percentRange, capAtZero);
	}
		
	public function setEffectDelay(delayInSeconds:Number = 0):void
	{
		effectDelay = delayInSeconds;
	}
		
	// Method 9
	public function setParticleColorMode(startColor:int = 0, endColor:int = 0, addMode:Boolean = false, switchColor:Boolean = false):void
	{
		// Set the paramters to do with the particle's color and blending.
		particleStartColor = startColor;
		particleEndColor = endColor;
		this.addMode = addMode;// Whether to use ADD blending.
		particleSwitchColor = switchColor;// Whether the color will switch.
			
		gradientId = KJLinearColorGradient.getGradientName(startColor, endColor);
			
		if (!KJLinearColorGradient.gradientExists(gradientId))
		{
			KJLinearColorGradient.registerNewGradient(gradientId, startColor, endColor);
		}
	}
		
	public function setBasicMode(basicMode:Boolean = false):void
	{
		this.basicMode = basicMode;
	}
		
	// Method 10
	public function setMotionParameters(motion3D:Boolean = true, motionAcceleration:Number = 0, motionMode:String = "MOTIONMODE_NONE"):void
	{
		this.motion3D = motion3D;
		this.motionAcceleration = motionAcceleration;
			
		switch (motionMode) {
			case MOTIONMODE_CONVERGE:
				motionConverge = true;
				motionDiverge = false;
				setDecceleration(0); // Convergence Math does not take deceleration into account.
				break;
					
			case MOTIONMODE_DIVERGE:
				motionConverge = false;
				motionDiverge = true;
				break;
					
			default:
				motionConverge = false;
				motionDiverge = false;
				break;
		}
	}
		
	public function setParticleBehavior(changeAlpha:Boolean = true, changeAlphaInverted:Boolean = false, changeSize:Boolean = true, changeSizeInverted:Boolean = false, directional:Boolean = false, stretch:Boolean = false):void
	{
		this.changeAlpha = changeAlpha;
		this.changeSize = changeSize;
		this.changeAlphaInverted = changeAlphaInverted;
		this.changeSizeInverted = changeSizeInverted;
		this.directional = directional;// Whether this rotates to match the direction.
		this.stretch = stretch;// Whether this particle stretches on its launch.
	}
		
	public function setStaticAcceleration(accelX:Number = 0, accelY:Number = 0, accelZ:Number = 0):void
	{
		staticAcceleration.x = accelX;
		staticAcceleration.y = accelY;
		staticAcceleration.z = accelZ;
	}
		
	public function setGraphics(graphicsCode:String = "GRAPHICS_SMOKE", randomRotation:Boolean = true, randomSize:Number = 0.3):void
	{
		graphics = graphicsCode;
		this.randomRotation = randomRotation;
		this.randomSize = randomSize;
	}
		
	public function setLinkedObject(object:* = null):void
	{
		linkedObject = object;
	}
		
	public function setEmitContainer(newEmitContainer:Sprite = null):void
	{
		emitContainer = newEmitContainer;
	}
		
	public function setFadeAfterDelay(fadeAfterDelay:Boolean = false):void
	{
		this.fadeAfterDelay = fadeAfterDelay;
	}
		
	public void SetOffset (x:Number, y:Number, z:Number)
	{
		emitterOffset.setTo(x, y, z);
	}
	*/
}
