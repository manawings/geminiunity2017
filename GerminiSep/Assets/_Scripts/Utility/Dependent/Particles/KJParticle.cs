﻿using UnityEngine;
using System.Collections;

public class KJParticle {
	
	// Movement Vectors
	
	public Vector3 position;
	Vector3 speed;
	Vector3 acceleration;
	Vector3 offset;
	
	public Transform LinkedTransform
	{
		get {
			return _linkedTransform;
		}
		set {
			if (value == null) 
				hasLinkedTransform = false;
			else 
				hasLinkedTransform = true;
			_linkedTransform = value;
		}
	} 

	Transform _linkedTransform;
	bool hasLinkedTransform = false;
	
	// Particle Physics
	
	private float lifeMax, life, lifeMaxFromDelay;
	private float originalScale = 1.0f;
	private float decceleration = 0.95f;
	private float inverseDecceleration;
	private float useDecceleration;
		
	// Particle Behavior
	
	private bool changeSize = true;
	private bool changeSizeInverted = false;
	private bool directional = false;// Whether this rotates to match the direction.
	private bool stretch = false;// Whether this particle stretches on its launch.
		
	// Decay
	
	private float decayProgress = 0;
	private float decayProgressInverse = 0;
	private float effectDelay = 0;// How long until the deceleration kicks in.
	private bool fadeAfterDelay = false;
		
	// Particle Effects
	
	private Color32 targetColor = new Color32(1, 1, 1, 1);
	private Color32 originalColor = new Color32(1, 1, 1, 1);
	public Color32 currentColor = new Color32(0, 0, 0, 0);
	
	// tk2d Sprite Reference
	
	public tk2dSpriteDefinition spriteDefinition;
	public Vector3[] spritePositions;
	
	private const float Eul2Rad = -0.01745329237f;
	private const float Rad2Eul = -57.29578f;
	
	public void Deploy (float scale = 1.0f)
	{
		// In Basic Mode - A lot of functions are switched off to run faster.
		
		decayProgress = 1;
		decayProgressInverse = 0;
//		RotationByRadians = 0;
		
		originalScale = scale;
		// TODO FlatScale = scale;
	}
	
	public void SetXYZ (Vector3 position)
	{
		this.position = position;
		
		// TODO transform.localPosition = position;
	}
	
	public void SetOffset (Vector3 offset)
	{
		this.offset = offset;
	}
	
	// METHODS to set all the other parameters.
	public void SetColorInfo (Color startColor, Color endColor)
	{
		originalColor = startColor;
		targetColor = endColor;
		currentColor = originalColor;
		
	}
		
	public void SetDirectionInfo (float direction = 0, float zDirectionFactor = 0, float power = 0, float acceleration = 0, float decceleration = 0)
	{	
		
		float speedX = Mathf.Sin(direction) * power;
		float speedY = Mathf.Cos(direction) * power;
		float speedZ = zDirectionFactor * power;
		this.speed = new Vector3(speedX, speedY, speedZ);
			
		float accelerationX =  Mathf.Sin(direction) * acceleration;
		float accelerationY =  Mathf.Cos(direction) * acceleration;
		float accelerationZ = zDirectionFactor * acceleration;
		this.acceleration = new Vector3(accelerationX, accelerationY, accelerationZ);
			
		this.decceleration = decceleration;
		inverseDecceleration = (1.0f - decceleration) * 60.0f;// Target Frame Rate
	}
	
	public void SetDecayInfo(float decay = 1.0f, float effectDelay = 0, bool changeSize = true, bool changeSizeInverted = false, bool directional = false, bool stretch = false, bool fadeAfterDelay = false)
	{	
		this.changeSize = changeSize;
		this.changeSizeInverted = changeSizeInverted;
			
		this.fadeAfterDelay = fadeAfterDelay;
			
		life = lifeMax = decay;
		this.effectDelay = effectDelay;
		lifeMaxFromDelay = lifeMax - effectDelay;
			
		this.directional = directional;
		this.stretch = stretch;
		
		if (changeSizeInverted) { FlatScale = 0.01f ; } else { FlatScale = originalScale; }
	}

	Quaternion rotator;

	public void SetRotation (float direction) 
	{
		RotationByRadians = direction;
		rotator = Quaternion.AngleAxis(direction * Rad2Eul, Vector3.forward);
		spritePositions = new Vector3[4];

		for (int i = 0 ; i < 4 ; i ++ )
		{
			spritePositions[i] = rotator * spriteDefinition.positions[i];
		}
	}
	
	public void SetParticleGraphic (KJParticleGraphic.Type type)
	{
		spriteDefinition = KJParticleController.GetSpriteDefinitionFor(KJParticleGraphic.GetSpriteIdFor(type));
		spritePositions = spriteDefinition.positions;

	}
		
	public void ModifyAcceleration(Vector3 particleAcceleration)
	{
		// Run after direction acceleration is applied.
		acceleration = particleAcceleration;
	}
	
	public bool Run (float DeltaTime)
	{
		if (life != 0)
		{
			life -= DeltaTime;
			if (life < 0) life = 0;
				
			decayProgress = life / lifeMax;
			decayProgressInverse = 1.0f - decayProgress;

			if (!fadeAfterDelay) {
				
				if (changeSize) {
					if (changeSizeInverted) {
						FlatScale = originalScale * decayProgressInverse;
					} else {
						FlatScale = originalScale * decayProgress;
					}
				}	
			}

			if (decceleration != 0) {
				if (effectDelay == 0) { // A general delay before effects kick in.
							
					useDecceleration = 1.0f - inverseDecceleration * DeltaTime;
					speed *= useDecceleration;
								
					if (fadeAfterDelay) {
								
						decayProgress = life / lifeMaxFromDelay;
						decayProgressInverse = 1.0f - decayProgress;

						if (changeSize) {
							if (changeSizeInverted) {
								FlatScale = originalScale * decayProgressInverse;
							} else {
								FlatScale = originalScale * decayProgress;
							}
						}	
					}
							
				} else {
						
					effectDelay -= DeltaTime;
					if (effectDelay <= 0) effectDelay = 0;	
							
				}	
			}
					
			speed += acceleration * DeltaTime;
			position += speed * DeltaTime;		
			currentColor = Color32.Lerp(originalColor, targetColor, decayProgressInverse);

			// Do not deactivate.
			return false;

		} else {


			Deactivate();
			return true; // Remove me
			
		}	
	}
	
	public Vector3 CalibratedPosition 
	{
		get 
		{
			if (hasLinkedTransform)
			{
				return LinkedTransform.position + position;
			} else {
				return position;
			}
		}
	}
	
	public float RotationByRadians
	{
		get;
		
		set;
	}
	
	public float FlatScale
	{
		get;
		
		set;
	}
	
	public void Deactivate ()
	{
		OnDeactivate();
//		KJParticleController.Deactivate(this);
	}
	
	public void OnDeactivate ()
	{
		LinkedTransform = null;
		offset = Vector3.zero;
	}
}
