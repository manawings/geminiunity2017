using UnityEngine;
using System.Collections;

public class KJNGUIButtonFunction : KJBehavior {
	
	public bool isPopUp = false;
	protected bool isPressed = false;
	public bool onPress = true;
	protected bool isHovering = false;

	public UISprite overrideSprite;
	public UIWidget overrideWidget;


	protected void Start () {

		UIEventListener.Get(gameObject).onPress += MyPressFunction;
	//	UIEventListener.Get(gameObject).onDrop += MyDropFunction;
		UIEventListener.Get(gameObject).onClick += MyClickFunction;
		UIEventListener.Get(gameObject).onHover += MyHoverFunction;

		TimeSpaceType = KJTime.SpaceType.UI;
	}

	public TutorialTapHere tapHere;
	
	protected bool IsControlLocked
	{
		get {
			
			if (InputController.IsControlLocked)
			{
				if (isPopUp && !InputController.IsPopUpLocked)
				{
					return false;	
				}

				if (InputController.unlockedButton == this) {
					return false;
				}
				
				return true;
			}
			
			return false;
		}
	}

	void MyDropFunction (GameObject gameObject = null, GameObject dragObject = null)
	{

	}

	void MyHoverFunction (GameObject gameObject = null, bool state = false)
	{
		isHovering = state;
	}

	void MyClickFunction (GameObject gameObject = null)
	{
		// Debug.Log ("isClicked");
	}

	protected bool hasBeenPressed = false;

	void MyPressFunction (GameObject gameObject, bool state)
	{
		if (hasBeenPressed) return;
		if (state == true) {

			if (IsControlLocked) return;
			isPressed = true;
			KJ_OnPress();
			ReleaseTutorial();

		} else {

			if (!isPressed) return;
			ReleaseHold();
			if (!IsControlLocked && !onPress) {
				ReleaseTutorial();
				if (sceneDelay) {
					KJTime.Add(KJTime.SpaceType.UI, ExecuteMethod, 0.15f, 1);
					hasBeenPressed = true;
				} else {
					ExecuteMethod();
				}
			}	
		}
	}

	public bool sceneDelay = false;


	void ReleaseTutorial ()
	{
		if (tapHere != null) {
			tapHere.Hide();
			tapHere = null;
		}
	}

	protected virtual void ExecuteMethod ()
	{
		
	}
	
	protected virtual void KJ_OnPress ()
	{
		
	}

	protected virtual void KJ_OnClick ()
	{
		
	}	

	protected virtual void StartHold ()
	{
		
	}
	
	protected virtual void ReleaseHold ()
	{
		
	}	


	
	UISprite _Sprite;
	protected UISprite Sprite
	{
		get {
			if (_Sprite == null) {
				if (overrideSprite != null) {
					_Sprite = overrideSprite;
				} else {
					_Sprite = GetComponent<UISprite>();
				}
			}
			return _Sprite;
		}
	}

	UIWidget _Widget;
	protected UIWidget Widget
	{
		get {

			if (Sprite != null) return Sprite;

			if (_Widget == null) {
				if (overrideWidget != null) {
					_Widget = overrideWidget;
				} else {
					_Widget = GetComponent<UIWidget>();
				}
			}
			return _Widget;
		}
	}
}
