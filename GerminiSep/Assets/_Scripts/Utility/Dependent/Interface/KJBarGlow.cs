using UnityEngine;
using System.Collections;

[ExecuteInEditMode]

public class KJBarGlow : KJBehavior {
	
	public UISprite mainBar;
	public UISprite flashBar;

	public UISprite borderTop;
	public UISprite borderBot;
	public UISprite borderLeft;
	public UISprite borderRight;

	public Color mainBarColor = Color.white;
	public Color frameColor = Color.white;

	// Border
	public float padding = 5;
	public float borderThickness = 1;
	
	float progress = 1.0f;
	
	// For Animation
	float flashProgress = 1.0f;
	float mainProgress = 1.0f;

	void Start () {

		CalibrateBarSize();
		CalibrateBarColor();

	}
	
	void Update () {
		
		#if UNITY_EDITOR
		CalibrateBarSize();
		CalibrateBarColor();
		#endif
		
	}

	void ChangeBar ()
	{
		SetProgress(Random.Range(0.2f, 0.8f));
	}

	void CalibrateBarColor ()
	{
		mainBar.color = mainBarColor;
		borderTop.color = frameColor;
		borderBot.color = frameColor;
		borderLeft.color = frameColor;
		borderRight.color = frameColor;
	}
	
	void CalibrateBarSize ()
	{
		
		float borderScaleX = (Mathf.Abs(transform.localScale.x) + (padding + borderThickness) * 2) / Mathf.Abs (transform.localScale.x);
		float borderScaleY = (Mathf.Abs(transform.localScale.y) + padding * 2) / Mathf.Abs(transform.localScale.y);
		float borderSizeScaledX = borderThickness / Mathf.Abs (transform.localScale.x);
		float borderSizeScaledY = borderThickness /  Mathf.Abs ( transform.localScale.y);


		borderTop.transform.localScale = new Vector3(borderScaleX, borderSizeScaledY, 1.0f);
		borderBot.transform.localScale = new Vector3(borderScaleX, borderSizeScaledY, 1.0f);

		borderLeft.transform.localScale = new Vector3(borderSizeScaledX, borderScaleY, 1.0f);
		borderRight.transform.localScale = new Vector3(borderSizeScaledX, borderScaleY, 1.0f);

		//borderBar.transform.localScale = new Vector3(borderScaleX, borderScaleY, 1.0f);
		float scaleExcessX = (borderScaleX - 1.0f) * 0.5f;
		float scaleExcessXRight = (Mathf.Abs(transform.localScale.x) + (padding)) / Mathf.Abs (transform.localScale.x);
		float scaleExcessY = (Mathf.Abs(transform.localScale.y * 0.5f) + padding + borderThickness * 0.5f) / Mathf.Abs(transform.localScale.y);

		borderTop.transform.localPosition = new Vector3(- scaleExcessX, scaleExcessY, 0.0f);
		borderBot.transform.localPosition = new Vector3(- scaleExcessX, -scaleExcessY, 0.0f);

		borderLeft.transform.localPosition = new Vector3(- scaleExcessX, 0, 0.0f);
		borderRight.transform.localPosition = new Vector3(scaleExcessXRight, 0, 0.0f);
	}
	
	public void SetProgress (float progress, bool instant = false)
	{

		flashBar.gameObject.SetActive(true);

		if (progress < 0.0f) progress = 0.0f;
		if (progress > 1.0f) progress = 1.0f;
		
		this.progress = progress;


		
		if (instant)
		{
			
			flashProgress = progress;
			mainProgress = progress;
			
			SetBarSizeToProgress();
			
		} else {

			if (flashProgress < mainProgress) flashProgress = mainProgress;
			AddTimer (Run);
			
		}
	}
	
	
	void Run ()
	{
		
		float progressDifference = 0.0f;
		bool isBarDone = true;
		
		if (progress < mainProgress) {
			
			// Target is under main.
			mainProgress = progress;
			
		} else if (progress > mainProgress) {
			
			progressDifference = progress - mainProgress;
			
			if (progressDifference < 0.01f) {
				mainProgress = progress;
			} else {
				mainProgress += progressDifference * 0.08f;
				isBarDone = false;
			}	
		}
		
		if (progress < flashProgress) {
			
			// Target is under flash.
			progressDifference = progress - flashProgress;
			
			if (progressDifference > -0.01f) {
				flashProgress = progress;
			} else {
				flashProgress += progressDifference * 0.08f;
				isBarDone = false;
			}
			
		} else if (progress > flashProgress) {
			
			// flashProgress = progress;
		}
		
		SetBarSizeToProgress();
		
		if (isBarDone) {
			RemoveAllTimers();
		}
	}
	
	void SetBarSizeToProgress ()
	{
		mainBar.transform.localScale = new Vector3(mainProgress, 1.0f, 1.0f);

		float actualFlashProgress = flashProgress - mainProgress;
		if (actualFlashProgress < 0) actualFlashProgress = 0;

		flashBar.transform.localScale = new Vector3(actualFlashProgress, 1.0f, 1.0f);	
		flashBar.transform.localPosition = new Vector3(mainProgress, 0, 0.0f);
	}
}
