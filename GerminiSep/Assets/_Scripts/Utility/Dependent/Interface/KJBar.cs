﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]

public class KJBar : KJBehavior {
	
	public UISprite mainBar;
	public UISprite borderBar;
	public UISprite backBar;
	public UISprite flashBar;
	
	public int borderSize = 4;
	public int segments = 1;
	
	float progress = 1.0f;
	
	// For Animation
	float flashProgress = 1.0f;
	float mainProgress = 1.0f;
	
	void Update () {
		
		#if UNITY_EDITOR
	    CalibrateBarSize();
	    #endif
		
	}
	
	void ChangeBar ()
	{
		SetProgress(Random.Range(0.2f, 0.8f));
	}
	
	void CalibrateBarSize ()
	{
		
		float borderScaleX = (Mathf.Abs(transform.localScale.x) + borderSize * 2) / Mathf.Abs (transform.localScale.x);
		float borderScaleY = (transform.localScale.y + borderSize * 2) / transform.localScale.y;
		
		borderBar.transform.localScale = new Vector3(borderScaleX, borderScaleY, 1.0f);
		float scaleExcess = (borderScaleX - 1.0f) * 0.5f;
		borderBar.transform.localPosition = new Vector3(- scaleExcess, 0.0f, 0.0f);
	}
	
	public void SetProgress (float progress, bool instant = false)
	{
		if (progress < 0.0f) progress = 0.0f;
		if (progress > 1.0f) progress = 1.0f;
		
		this.progress = progress;
		
		if (instant)
		{
			
			flashProgress = progress;
			mainProgress = progress;
			
			SetBarSizeToProgress();
			
		} else {
			
			AddTimer (Run);
			
		}
	}
	
	
	void Run ()
	{
		
		float progressDifference = 0.0f;
		bool isBarDone = true;
		
		if (progress < mainProgress) {
			
			// Target is under main.
			mainProgress = progress;
			
		} else if (progress > mainProgress) {
			
			progressDifference = progress - mainProgress;
			
			if (progressDifference < 0.01f) {
				mainProgress = progress;
			} else {
				mainProgress += progressDifference * 0.08f;
				isBarDone = false;
			}	
		}
		
		if (progress < flashProgress) {
			
			// Target is under flash.
			progressDifference = progress - flashProgress;
			
			if (progressDifference > -0.01f) {
				flashProgress = progress;
			} else {
				flashProgress += progressDifference * 0.08f;
				isBarDone = false;
			}
			
		} else if (progress > flashProgress) {
			
			flashProgress = progress;
		}
		
		SetBarSizeToProgress();
		
		if (isBarDone) {
			RemoveAllTimers();
		}
	}
	
	void SetBarSizeToProgress ()
	{
		mainBar.transform.localScale = new Vector3(mainProgress, 1.0f, 1.0f);
		flashBar.transform.localScale = new Vector3(flashProgress, 1.0f, 1.0f);	
	}
}
