using UnityEngine;
using System.Collections;

public class KJCamera : KJBehavior
{
	
	public static KJCamera Singleton { get; private set; }
	
	public float x = 0.0f, y = 0.0f;
	private GameObject shakeObject;
	
	// Reference to Camera
	Camera _camera;
	new Camera camera {
		get {
			if (_camera == null) _camera = GetComponent<Camera>();
			return _camera;
		}	
	}

	void Awake ()
	{
		Singleton = this;
		
		cameraPosition = Vector3.zero;
		shakePosition = Vector3.zero;
		targetPosition = Vector3.zero;
		finalPosition = Vector3.zero;

		cameraPosition = new Vector3(0, 0, -100);
//		camera.transform.position = new Vector3(0, 0, -1000);
		AddTimer(Run);
	}
	
//	void Update ()
//	{
//		float newX = x + shakeObject.transform.position.x;
//		float newY = y + shakeObject.transform.position.y;
//		camera.transform.position = new Vector3(newX, newY, camera.transform.position.z);
//
//	}
//	
//	public void Shake (float magnitude)
//	{
//		iTween.ShakePosition(Singleton.shakeObject, new Vector3 (magnitude, magnitude, 0.0f), 0.65f);	
//	}



	// _____

	
	public Vector3 cameraPosition, shakePosition, targetPosition, finalPosition;
	public Vector2 autoPanTarget, jitterPosition, jitterTarget;
	public float FocalLength = 100.0f, cameraSpeed = 0.10f, autoPanSpeed = 0.25f;
	bool overshooting = false;
	

	
	void Deploy ()
	{
		AddTimer(Run);
	}
	
	float sinAdder = 0.0f;
	float sinAdder2 = 0.0f;
	
	void Run ()
	{
		if (isShaking) RunShake();
		
		finalPosition = cameraPosition + shakePosition;
		camera.transform.position = finalPosition + shakePosition;
	}
	
	
	// CAMERA MOVEMENT AND TARGETING -----------------------

	
	// CAMERA SHAKE FUNCTIONS -----------------------
	
	bool isShaking = false;
	float shakeMagnitude = 0.0f, shakeDecay = 0.90f, shakeSpeed = 0.85f, shakeSin = 0.0f, shakeRotationFactor = 0.1f;
	ShakeAxis shakeAxis;
	public enum ShakeAxis { X, Y, Z };
	
	void RunShake ()
	{
		
		// Run the SIN Function
		
		shakeSin += shakeSpeed;
		float shakeValue = Mathf.Sin (shakeSin) * shakeMagnitude;
		//		camera.transform.localEulerAngles = Vector3.forward * shakeValue * shakeRotationFactor;
		
		// Show the Shake
		
		shakePosition = Vector3.zero;
		
		switch (shakeAxis) {
			
		case ShakeAxis.X:
			shakePosition.x = shakeValue;
			break;
			
		case ShakeAxis.Y:
			shakePosition.y = shakeValue;
			break;
			
		case ShakeAxis.Z:
			shakePosition.z = shakeValue;
			break;
			
		}
		
		// Decay the Shake Magnitude
		
		shakeMagnitude *= shakeDecay;
		if (shakeMagnitude < 0.1f) {
			isShaking = false;
			shakePosition = Vector3.zero;
			camera.transform.localEulerAngles = Vector3.zero;
		}
	}
	
	public static void Shake (float magnitude = 50.0f, ShakeAxis axis = ShakeAxis.Y, float speed = 0.9f, float decay = 0.90f) { Singleton._Shake(magnitude, axis, speed, decay); }
	void _Shake (float magnitude, ShakeAxis axis, float speed, float decay)
	{
		isShaking = true;
		shakeSin = -1.0f;
		shakeMagnitude = magnitude;
		shakeSpeed = speed;
		shakeDecay = decay;
		shakeAxis = axis;
	}
}

