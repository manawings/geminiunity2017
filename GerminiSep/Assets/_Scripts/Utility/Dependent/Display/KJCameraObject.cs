using UnityEngine;
using System.Collections;

public class KJCameraObject : MonoBehaviour
{
	
	private float x, y;
	
	public float cameraFactor = 0.3f;
	private float cameraFactorInverse;
	
	Transform cameraTransform;
	Vector3 finalPosition;
	
	// Use this for initialization
	void Start ()
	{
		x = transform.position.x;
		y = transform.position.y;
		cameraFactorInverse = 1.0f - cameraFactor;
		
		cameraTransform = Camera.main.transform;
		finalPosition = new Vector3();
		
		KJTime.Add(Run);
	}
	
	// Update is called once per frame
	void Run ()
	{
		finalPosition.x = cameraTransform.position.x * cameraFactorInverse + x * cameraFactor;
		// finalPosition.y = cameraTransform.position.y * cameraFactorInverse + y * cameraFactor;
		transform.localPosition = finalPosition;
	}
}

