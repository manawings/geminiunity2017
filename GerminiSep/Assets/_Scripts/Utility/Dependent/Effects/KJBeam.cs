using UnityEngine;
using System.Collections;

public class KJBeam : KJBehavior {
	
	public tk2dTiledSprite innerBeamSprite;
	public tk2dTiledSprite outerBeamSprite;
	
	protected Vector2 beamStartPoint;
	protected Vector2 beamTargetPoint;
	
	// Emitter System
	protected KJEmitter chargeEmitter;
	protected KJEmitter trailEmitter;
	protected KJEmitter electroEmitter;

	protected bool isPlayerBeam = false;

	bool beamTrailMade = false;

	// Use this for initialization
	void Awake () {
		
		beamStartPoint = new Vector2();
		beamTargetPoint = new Vector2(0.0f, 500.0f);
		
		// Trail Emitter
		
		trailEmitter = gameObject.AddComponent<KJEmitter>();
		trailEmitter.SetDecay(0.6f, 0.6f);
		trailEmitter.SetEmissionRate(0);
		trailEmitter.SetGraphic(KJParticleGraphic.Type.Laser);
		trailEmitter.SetPower(100.0f, 100.0f);

		// Trail Emitter
		
		electroEmitter = gameObject.AddComponent<KJEmitter>();
		electroEmitter.SetDecay(1.0f, 0.5f);
		electroEmitter.SetScale(0.5f);
		electroEmitter.SetIsContained(true);
		// electroEmitter.SetChangeSize(false, false);
		electroEmitter.SetEmissionRate(15);
		electroEmitter.SetGraphic(KJParticleGraphic.Type.Stun);
		electroEmitter.SetPower(900.0f, 400.0f);
		electroEmitter.SetSpawnInDirection(true);
		
		// Charge
		
		chargeEmitter = gameObject.AddComponent<KJEmitter>();
		chargeEmitter.SetScale(1.5f);
		chargeEmitter.SetDecay(0.4f, 0.2f);
		chargeEmitter.SetEmissionRate(20);
		chargeEmitter.SetGraphic(KJParticleGraphic.Type.Spark);
		chargeEmitter.SetMotionConverge(400.0f);
		chargeEmitter.SetPower(0.0f, 20.0f);
		chargeEmitter.SetIsContained(true);
		chargeEmitter.SetDecceleration (0.0f);
		chargeEmitter.SetChangeSize(true, true);
		chargeEmitter.SetSpawnInDirection(true);
		
		// Shine
		
		/*
		shineEmitter = gameObject.AddComponent<KJEmitter>();
		shineEmitter.SetColor(
			new Color (1.0f, 0.4f, 0.8f, 1.0f),
			new Color (0.4f, 0.0f, 0.5f, 0.5f));
		shineEmitter.SetRadius(1.0f);
		shineEmitter.SetDecay(0.8f, 0.3f);
		shineEmitter.SetScale(2.5f);
		shineEmitter.SetEmissionRate(5);
		shineEmitter.SetGraphic(KJParticleGraphic.Type.Spark);
		shineEmitter.SetIsContained(true);
		*/
		

		
	}
	
	float sizeX;
	float sizeBase = 1.0f;
	float deltaCapper = 0.0f;
	float sizeVary = 0.10f;
	float sizeScale = 1.0f;
	
	void Run () {
		
		
		sizeX = sizeScale * (sizeBase + Mathf.Sin(deltaCapper) * sizeVary);
		deltaCapper += 0.25f;
			
		// Make sure the beam is always a positive size.
		float innerSizeX = sizeX - 0.35f;
		if (innerSizeX < 0) innerSizeX = - innerSizeX;

		innerBeamSprite.scale = new Vector3(innerSizeX, 1.0f, 1.0f);
		outerBeamSprite.scale = new Vector3(sizeX + 0.35f, 1.0f, 1.0f);
		
		SetBeamPosition();
		
	}

	public virtual void SetBeamPosition ()
	{

		transform.position = new Vector3(beamStartPoint.x, beamStartPoint.y, transform.position.z);
		chargeEmitter.emitterPosition = transform.position;
		if (isPlayerBeam) electroEmitter.emitterPosition = transform.position;
		
		float beamDirection = KJMath.DirectionFromPosition(beamStartPoint, beamTargetPoint);
		float newAngle = - Mathf.Rad2Deg * beamDirection;
		transform.eulerAngles = new Vector3(0.0f, 0.0f, newAngle);
		
		float beamLength = Vector2.Distance(beamStartPoint, beamTargetPoint);
		
		innerBeamSprite.dimensions = new Vector2(innerBeamSprite.dimensions.x, beamLength);
		outerBeamSprite.dimensions = new Vector2(innerBeamSprite.dimensions.x, beamLength);
	}
	
	public void StartBeam ()
	{
		
		chargeEmitter.Deploy();

		beamTrailMade = false;
		isFlickingOn = true;
		sizeScale = 0.15f;

		SetBeamPosition();
		flickTimeCurrent = flickTimeMax;

		FlickOn();
		AddTimer(Run);
	}
	
	public void CreatePostBeamTrail ()
	{
		if (beamTrailMade) return;
		float beamDirection = KJMath.DirectionFromPosition(beamStartPoint, beamTargetPoint);
		trailEmitter.SetDirection(beamDirection);
		trailEmitter.SpawnLine(beamStartPoint, beamTargetPoint, 12, 5.0f);	
		beamTrailMade = true;
	}
	
	public void EndBeam ()
	{
		
		chargeEmitter.KillTimers();
		if (isPlayerBeam) electroEmitter.KillTimers();

		DisengageBeam();
		CreatePostBeamTrail();
		
		iTween.Stop (gameObject);
		sizeScale = 0.15f;
		isFlickingOn = false;
		flickCountCurrent = 3;
		flickTimeCurrent = flickCutOff;
		
		FlickOff();
	}
	
	public void TerminateBeam ()
	{
		// Instantly Terminate the Beam.

		CreatePostBeamTrail();
		Deactivate();
	}
	
	protected override void OnDeactivate ()
	{
		iTween.Stop (gameObject);
		base.OnDeactivate();
	}
	
	protected virtual void EngageBeam ()
	{
		if (isPlayerBeam) electroEmitter.Deploy();
	}
	
	protected virtual void DisengageBeam ()
	{
		
	}
	
	float flickTimeMax = 0.60f;
	float flickTimeCurrent;
	float flickCutOff = 0.05f;
	float flickFactor = 0.55f;
	int flickCountCurrent = 0;
	
	bool isFlickingOn = true;
	
	void FlickOn ()
	{
		

		innerBeamSprite.GetComponent<Renderer>().enabled = true;
		outerBeamSprite.GetComponent<Renderer>().enabled = true;
		
		if (isFlickingOn && flickTimeCurrent <= flickCutOff)
		{
			// Beam is Officially On

			Hashtable param = new Hashtable();
            param.Add("from", 3.0f);
            param.Add("to", 1.0f);
            param.Add("time", 0.5f);
            param.Add("onupdate", "OnTweenBeamScale");
			param.Add("easetype", iTween.EaseType.easeOutCirc);
            iTween.ValueTo(gameObject, param);
			
			EngageBeam();
			
		} else {
			AddTimer (FlickOff, 0.05f, 1);
		}

		SetBeamPosition();
	}
	
	void OnTweenBeamScale (float newValue)
	{
		sizeScale = newValue;
	}
	
	void FlickOff ()
	{
		
		innerBeamSprite.GetComponent<Renderer>().enabled = false;
		outerBeamSprite.GetComponent<Renderer>().enabled = false;
		
		if (isFlickingOn)
		{
			flickTimeCurrent *= flickFactor;
		} else {
			flickTimeCurrent = 0.05f;
			flickCountCurrent --;
		}
		
		if (!isFlickingOn && flickCountCurrent == 0) {
			
			// Beam is Officially Off
			Deactivate();
			
		} else {

			AddTimer (FlickOn, flickTimeCurrent, 1);	
		}
	}

}
