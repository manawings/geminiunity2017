﻿using UnityEngine;
using System.Collections;

public class DatabaseManager : MonoBehaviour {

	private static GameObject parentObject;
	private static DatabaseManager _Singleton;
	public static DatabaseManager Singleton {
		get {
			if (!_Singleton)
			{
				parentObject = new GameObject("DatabaseManager");
				DontDestroyOnLoad(parentObject);
				_Singleton = parentObject.AddComponent<DatabaseManager>();
				//_Singleton.Init();
			}
			return _Singleton;
		}
	}

	public delegate void SocialDelegate ();
	SocialDelegate loginSuccessDelegate;
	SocialDelegate loginFailDelegate;

	public enum AccessCode {

		RequestLogin = 0,

		RequestBigData = 1,
		RequestSmallData = 2,

		SaveBigData = 3,
		SaveSmallData = 4,

	}

	public void RequestLogin ()
	{
		Debug.Log ("Call DB for Login");
		DB.Singleton.CallDatabase ((AccessCode.RequestLogin).ToString(), OnLoginSuccess);
	}

	public void BeginLogin (string myId, SocialDelegate onLoginSucess, SocialDelegate onLoginFail)
	{
		//		Debug.Log ("Begin DB Login");

		SocialController.isLoggedIn = false;
		SocialController.id = myId;
		//friendId = friendIdInput;
		loginSuccessDelegate = onLoginSucess;
		loginFailDelegate = onLoginFail;

		RequestLogin();
		// OnLoginSuccess();
	}

	void OnLoginSuccess ()
	{
		//		Debug.Log ("DB Login Success");
		//		Debug.Log ("Login With: " + SocialController.id);
		SocialController.isLoggedIn = true;
		if (loginSuccessDelegate != null) loginSuccessDelegate();
	}

	public void SavePostGame ()
	{
		//SendData(AccessCode.RequestBigData, id);
		if (SocialController.isLoggedIn) {
			//			Debug.Log ("Saving Post Game Data...");
			DB.Singleton.CallDatabase ("SavePostGame", UIController.ClosePopUp);
		}
	}

	public void RequestLoadBigData ()
	{
		//SendData(AccessCode.RequestBigData, id);
		//		Debug.Log ("Request Load Big Data");
		DB.Singleton.CallDatabase ((AccessCode.RequestBigData).ToString());
	}

	public void RequestSaveBigData (KJTime.TimeDelegate onSaveComplete = null, KJTime.TimeDelegate onSaveFail = null)
	{
		DB.Singleton.CallDatabase ((AccessCode.SaveBigData).ToString(), onSaveComplete, onSaveFail);
		//	SendData(AccessCode.SendBigData, id, DataController.EncodedBigData);
	}

	public void BeginChallenge ()
	{
		DB.Singleton.CallDatabase ((AccessCode.RequestSmallData).ToString());
	}

	public static void CheckServerTime (KJTime.TimeDelegate OnCheckTimeComplete = null)
	{
//		webService.ConnectWebService("Check_Server_Time", OnCheckTimeComplete);
	}
}
