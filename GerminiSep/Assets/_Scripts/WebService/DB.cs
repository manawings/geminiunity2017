﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Security.Cryptography;
using MiniJSON;
using CodeStage.AntiCheat.ObscuredTypes;

public class DB : MonoBehaviour {

	public enum AccessCode {

		RequestLogin = 0,

		RequestBigData = 1,
		RequestSmallData = 2,

		SaveBigData = 3,
		SaveSmallData = 4,

	}

	private ObscuredString secretKey = "yCKg3asgEgA6+XfVEXMJBagaEsgaEU11eDrsMYi5jt8/hC803SNd";
	private ObscuredString secretKey2 = "VvAI47LtdzduIoKm+dIgJCFbdplurThAfXg5Yf7gxAAD";
	private ObscuredString key = "+HIp3NddJk7CUWfdwuG8+agKJIhyvU3y5/6azdT3EgGi6dfOdUffQ=";
	private string id = "10";
	private string data;
	private string IV;
	private ObscuredString url = "http://aws.amazon.com/biltz";
	private ObscuredString url2 = "http://aws.amazon.com/biltz2"; //http://gemini-services.sys.armorgames.net/
	private string result;
	private byte [] bKey = {15,43,0,72,0,73,0,112,0,51,0,78,0,74,0,107,0,55,0,67,0,85,0,87,0,102,0,119,0,117,0,71,0,56,0,43,0,97,0,75,0,74,0,73,0,121,0,118,0,85,0,51,0,121,0,53,0,47,0,54,0,122,0,100,0,84,0,51,0,69,0,71,0,105,0,54,0,100,0,79,0,100,0,85,0,81,0,61,0,58};
	private byte [] bSecretKey = {11,121,0,67,0,75,0,103,0,51,0,65,0,54,0,43,0,88,0,102,0,86,0,69,0,88,0,77,0,74,0,66,0,69,0,85,0,49,0,49,0,101,0,68,0,114,0,115,0,77,0,89,0,105,0,53,0,106,0,116,0,56,0,47,0,104,0,67,0,56,0,48,0,51,0,83,0,78,0,100,0,58};
	private byte [] bSecretKey2 = {1,86,0,118,0,65,0,73,0,52,0,55,0,76,0,116,0,100,0,122,0,117,0,73,0,111,0,75,0,109,0,43,0,100,0,73,0,74,0,67,0,70,0,98,0,112,0,108,0,117,0,114,0,84,0,104,0,65,0,88,0,103,0,53,0,89,0,102,0,55,0,103,0,120,0,65,0,65,0,68,0,5};
//	private byte [] bUrl = {104,0,116,0,116,0,112,0,115,0,58,0,47,0,47,0,103,0,101,0,109,0,105,0,110,0,105,0,45,0,115,0,101,0,114,0,118,0,105,0,99,0,101,0,115,0,46,0,115,0,121,0,115,0,46,0,97,0,114,0,109,0,111,0,114,0,103,0,97,0,109,0,101,0,115,0,46,0,110,0,101,0,116,0,47,0};
	private byte [] bUrl = {104,0,116,0,116,0,112,0,58,0,47,0,47,0,103,0,101,0,109,0,105,0,110,0,105,0,45,0,115,0,101,0,114,0,118,0,105,0,99,0,101,0,115,0,46,0,115,0,121,0,115,0,46,0,97,0,114,0,109,0,111,0,114,0,103,0,97,0,109,0,101,0,115,0,46,0,110,0,101,0,116,0,47,0};
//	private byte [] bUrl = {104,0,116,0,116,0,112,0,58,0,47,0,47,0,107,0,114,0,105,0,110,0,115,0,116,0,117,0,100,0,105,0,111,0,46,0,99,0,111,0,109,0,47,0,98,0,108,0,105,0,116,0,122,0,47,0,97,0,119,0,115,0,47,0,98,0,108,0,105,0,116,0,122,0,46,0,112,0,104,0,112,0};
	private byte [] bUrl2 = {104,0,116,0,116,0,112,0,58,0,47,0,47,0,107,0,114,0,105,0,110,0,115,0,116,0,117,0,100,0,105,0,111,0,46,0,99,0,111,0,109,0,47,0,98,0,108,0,105,0,116,0,122,0,47,0,97,0,119,0,115,0,47,0,115,0,116,0,97,0,114,0,95,0,115,0,116,0,114,0,105,0,107,0,101,0,46,0,112,0,104,0,112,0};
//	private byte [] bUrl2 = {104,0,116,0,116,0,112,0,58,0,47,0,47,0,105,0,110,0,115,0,116,0,97,0,110,0,116,0,112,0,97,0,121,0,100,0,97,0,121,0,108,0,111,0,97,0,110,0,115,0,52,0,121,0,111,0,117,0,46,0,99,0,111,0,109,0,47,0,121,0,111,0,115,0,105,0,109,0,97,0,47,0,103,0,97,0,109,0,101,0,47,0,97,0,119,0,115,0,47,0,98,0,108,0,105,0,116,0,122,0,46,0,112,0,104,0,112,0};
//	private byte [] bUrl3 = {101,0,99,0,50,0,45,0,53,0,52,0,45,0,56,0,49,0,45,0,49,0,55,0,55,0,45,0,49,0,57,0,48,0,46,0,99,0,111,0,109,0,112,0,117,0,116,0,101,0,45,0,49,0,46,0,97,0,109,0,97,0,122,0,111,0,110,0,97,0,119,0,115,0,46,0,99,0,111,0,109,0};
	private byte [] bUrl4 = {104,0,116,0,116,0,112,0,58,0,47,0,47,0,103,0,101,0,109,0,105,0,110,0,105,0,45,0,49,0,48,0,53,0,55,0,57,0,52,0,52,0,53,0,50,0,54,0,46,0,117,0,115,0,45,0,101,0,97,0,115,0,116,0,45,0,49,0,46,0,101,0,108,0,98,0,46,0,97,0,109,0,97,0,122,0,111,0,110,0,97,0,119,0,115,0,46,0,99,0,111,0,109,0,47,0};

	private static GameObject parentObject;
	private static DB _Singleton;
	public static DB Singleton {
		get {
			if (!_Singleton)
			{
				parentObject = new GameObject("Database");
				DontDestroyOnLoad(parentObject);
				_Singleton = parentObject.AddComponent<DB>();
				_Singleton.Init();
			}
			return _Singleton;
		}
	}

	List<SocialData> socialDataList;
	RijndaelEncrypted AES = new RijndaelEncrypted ();

	public void Init () {
		
//		bKey = RijndaelEncrypted.RemoveAt(bKey,0);
//		bKey = RijndaelEncrypted.RemoveAt(bKey,bKey.Length - 1);
//		bSecretKey = RijndaelEncrypted.RemoveAt(bSecretKey,0);
//		bSecretKey = RijndaelEncrypted.RemoveAt(bSecretKey,bSecretKey.Length - 1);
//		bSecretKey2 = RijndaelEncrypted.RemoveAt(bSecretKey2,0);
//		bSecretKey2 = RijndaelEncrypted.RemoveAt(bSecretKey2,bSecretKey2.Length - 1);
//		key = AES.GetString(bKey);
//		secretKey = AES.GetString(bSecretKey);
//		secretKey2 = AES.GetString(bSecretKey2);
//		url = AES.GetString(bUrl);
//		url2 = AES.GetString(bUrl2);

	}

	void Awake () {
//		AES.ByteLog("https://test/");
		bKey = RijndaelEncrypted.RemoveAt(bKey,0);
		bKey = RijndaelEncrypted.RemoveAt(bKey,bKey.Length - 1);
		bSecretKey = RijndaelEncrypted.RemoveAt(bSecretKey,0);
		bSecretKey = RijndaelEncrypted.RemoveAt(bSecretKey,bSecretKey.Length - 1);
		bSecretKey2 = RijndaelEncrypted.RemoveAt(bSecretKey2,0);
		bSecretKey2 = RijndaelEncrypted.RemoveAt(bSecretKey2,bSecretKey2.Length - 1);
		key = AES.GetString(bKey);
		secretKey = AES.GetString(bSecretKey);
		secretKey2 = AES.GetString(bSecretKey2);
		url = AES.GetString(bUrl);
		url2 = AES.GetString(bUrl2);

	}

	public KJTime.TimeDelegate onCallSuccess, onCallFail;
	public string action, overrideId;
	public int tValue;
	public bool hasPopup = false;

	public void CheckConnection () {
		Debug.Log("CheckConnection");
		if (!NetworkController.Singleton.CheckConnecttion) {

			if (OnConnectFail != null) {
				Debug.Log("OnConnectFail");
				OnConnectFail();
			} else {
				UIController.ShowPopUpSingleYes("NET ERROR", "Unable to connect to the internet. Please check your device's internet connection and try again.", "OK", CheckConnection);
				hasPopup = true;
			}
		} else {

			if (hasPopup) { 
				hasPopup = false;
				UIController.ClosePopUp();
			}

			//if (ProfileController.Singleton.isOffline && !ProfileController.Singleton.isLoadData) return;

			SelectCase(action, onCallSuccess, onCallFail, overrideId, tValue);
		}
	}

	int endTime, startTime;
	public void CheckServer () {
		WWW www;
		WWWForm form = new WWWForm();
		//form.AddField("action", action);
		form.AddField("check", "check");
		www = new WWW(url, form);
		startTime = DateTime.UtcNow.Millisecond;
		StartCoroutine (WaitForCheck (www));
	}
	
	IEnumerator WaitForCheck(WWW www)
	{
		yield return www;
		if (www.error == null)
		{
//			Debug.Log("OK");
		} else {
//			Debug.Log("Time Out");
		}
		endTime = DateTime.UtcNow.Millisecond;
//		Debug.Log(startTime - endTime + "ms");
	}

	public KJTime.TimeDelegate OnConnectFail = null;

	public void CallDatabase (string action, KJTime.TimeDelegate onCallSuccess = null, KJTime.TimeDelegate onCallFail = null, string overrideId = "NULL", int tValue = 0) {
		this.onCallSuccess = onCallSuccess;
		this.onCallFail = onCallFail;
		this.action = action;
		this.overrideId = overrideId;
		this.tValue = tValue;
//		Debug.Log ("Calling Database...");
		CheckConnection();
		OnConnectFail = null;
		if (SocialController.id == "0") SocialController.LoadSocialGuestID();
	
	}

	public void SelectCase (string action, KJTime.TimeDelegate onCallSuccess = null, KJTime.TimeDelegate onCallFail = null, string overrideStr = "NULL", int tValue = 0) {
		Debug.Log("SelectCase : " + action + "url : " + url);
		this.onCallSuccess = onCallSuccess;
		this.onCallFail = onCallFail;
		
		WWW www;
		switch (action) {
			
		case "RequestLogin" :
//			print ("Requesting Login");
			www = new WWW(url, EncryptedForm(JsonEncodedAll(action)));
			StartCoroutine (WaitForRequestBigData (www));
			break;
			
		case "RequestSmallData" :
			www = new WWW(url, EncryptedForm(JsonEncodedAll(action)));
			StartCoroutine (WaitForRequestSmallData (www));
			break;
			
		case "RequestCompactProfile" :
			www = new WWW(url, EncryptedForm(JsonEncodedAll(action, overrideStr)));
			StartCoroutine (WaitForRequestSmallData (www));
			break;
			
		case "RequestBigData" :
			www = new WWW(url, EncryptedForm(JsonEncodedID(action)));
			StartCoroutine (WaitForRequestBigData (www));
			break;
			
		case "SaveBigData" :
			www = new WWW(url, EncryptedForm(JsonEncodedAll(action)));
			StartCoroutine (WaitForData (www));
			break;
			
		case "SavePostGame" :
			www = new WWW(url, EncryptedForm(JsonEncodedAll(action)));
			StartCoroutine (WaitForData (www));
			break;
			
		case "AddFriend" :
			www = new WWW(url, EncryptedForm(JsonEncodedFriendID(action)));
			StartCoroutine (WaitForData (www));
			break;
			
		case "RequestFriendList" :
			www = new WWW(url, EncryptedForm(JsonEncodedID(action)));
			StartCoroutine (WaitForRequestFriendList (www));
			break;
			
		case "AcceptFriend" :
			www = new WWW(url, EncryptedForm(JsonEncodedFriendID(action)));
			StartCoroutine (WaitForData (www));
			break;
			
		case "DeleteFriend" :
			www = new WWW(url, EncryptedForm(JsonEncodedFriendID(action)));
			StartCoroutine (WaitForData (www));
			break;
			
		case "RandomPlayer" :
			www = new WWW(url, EncryptedForm(JsonEncodedID(action)));
			StartCoroutine (WaitForRandomPlayer (www));
			break;
			
		case "CollectReward" :
			www = new WWW(url, EncryptedForm(JsonEncodedSimple(action, tValue)));
			StartCoroutine (WaitForData (www));
			break;
			
		case "CollectGold" :
			www = new WWW(url, EncryptedForm(JsonEncodedSimple(action, tValue)));
			StartCoroutine (WaitForData (www));
			break;
			
		case "DeclineFriend" :
			www = new WWW(url, EncryptedForm(JsonEncodedFriendID(action)));
			StartCoroutine (WaitForData (www));
			break;
			
		case "RequestDelete":
			www = new WWW(url, EncryptedForm(JsonEncodedSimple(action, tValue)));
			StartCoroutine (WaitForData (www));
			break;
			
		case "CheckAccount":
			www = new WWW(url, EncryptedForm(JsonEncodedSimple(action, tValue)));
			StartCoroutine (WaitForData (www));
			break;

		case "CheckInbox":
			www = new WWW(url, EncryptedForm(JsonEncodedSimple(action, tValue)));
			StartCoroutine (WaitForData (www));
			break;

		case "CheckVersion":
			www = new WWW(url, EncryptedForm(JsonEncodedAction(action)));
			StartCoroutine (WaitForVersion (www));
			break;

		case "IAP_HC":
			www = new WWW(url, EncryptedForm(JsonEncodedIAP(action, overrideStr)));
			StartCoroutine (WaitForIAP (www));
			break;

		case "HC_Used":
			www = new WWW(url, EncryptedForm(JsonEncodedHC(action, tValue)));
			StartCoroutine (WaitForHC (www));
			break;

		case "Open_Cargo":
			www = new WWW(url, EncryptedForm(JsonEncodedCargo(action, overrideStr)));
			StartCoroutine (WaitForCargo (www));
			break;

		case "Check_Server_Time":
			www = new WWW(url, EncryptedForm(JsonEncodedAction(action)));
			StartCoroutine (WaitForTimeServer (www));
			break;
			
		}
		
		UIController.AddCoroutineToTimeout(this);
	}

	IEnumerator WaitForRequestBigData(WWW www)
	{
		yield return www;

		Debug.Log("Got Big Data ---");
			
			// check for errors
		if (www.error == null)
		{
//			Debug.Log(www.text);
			result = www.text;

//			Debug.Log("Encrypt = " + www.text);
			string[] data;
			data = www.text.Split(new string[] { "__blite__" }, StringSplitOptions.None);
			result = data[0];
			data = data[1].Split(new string[] { "__blite2__" }, StringSplitOptions.None);
			string hash = data[0];
			string IV = data[1];
			if (Md5Sum(IV + result + secretKey2) == hash) {
				result = AES.DecryptStringFromBytes(Convert.FromBase64String(result), Convert.FromBase64String(key), Convert.FromBase64String(IV));
				Debug.Log("Result = " + (result));
				DataController.DecodeBigData(JsonDecoded(result));

				if (onCallSuccess != null) onCallSuccess();
			}
			
		} else {
//			Debug.Log("WWW Error: "+ www.error);
			if (onCallFail != null) onCallFail();
		}
	} 

	IEnumerator WaitForRequestSmallData(WWW www)
	{
		yield return www;
		
		// check for errors
		if (www.error == null)
		{
			Debug.Log(www.text);
			//result = www.text;
			
//			Debug.Log("Encrypt = " + www.text);
			string[] data;
			data = www.text.Split(new string[] { "__blite__" }, StringSplitOptions.None);
			result = data[0];
			data = data[1].Split(new string[] { "__blite2__" }, StringSplitOptions.None);
			string hash = data[0];
			string IV = data[1];
			if (Md5Sum(IV + result + secretKey2) == hash) {
				result = AES.DecryptStringFromBytes(Convert.FromBase64String(result), Convert.FromBase64String(key), Convert.FromBase64String(IV));
//				Debug.Log("Result = " + (result));
				DataController.DecodeRivalData(JsonDecoded(result));
				if (onCallSuccess != null) onCallSuccess();
			}
		} else {
//			Debug.Log("WWW Error: "+ www.error);
			if (onCallFail != null) onCallFail();
		}
	} 

	IEnumerator WaitForRandomPlayer(WWW www)
	{
		yield return www;
		
		// check for errors
		if (www.error == null)
		{
			//Debug.Log(www.text);
			//result = www.text;
			
//			Debug.Log("Encrypt = " + www.text);
			string[] data;
			data = www.text.Split(new string[] { "__blite__" }, StringSplitOptions.None);
			result = data[0];
			data = data[1].Split(new string[] { "__blite2__" }, StringSplitOptions.None);
			string hash = data[0];
			string IV = data[1];
			if (Md5Sum(IV + result + secretKey2) == hash) {

				result = AES.DecryptStringFromBytes(Convert.FromBase64String(result), Convert.FromBase64String(key), Convert.FromBase64String(IV));
//				Debug.Log("Result = " + (result));
				if (DataController.DecodeRivalData(JsonDecoded(result))) {
					ProfileController.allyStandby = true;
				} else {
					ProfileController.allyStandby = false;
				}

				if (onCallSuccess != null) onCallSuccess();
			}
		} else {
//			Debug.Log("WWW Error: "+ www.error);
			if (onCallFail != null) onCallFail();
		}
	} 

	IEnumerator WaitForData(WWW www)
	{
		yield return www;
		Debug.Log("Error" + www.error);
		// check for errors
		if (www.error == null)
		{

			Debug.Log("Result = " + www.text);

			if (www.text != null && www.text != "") {
				Dictionary<string, object> returnDict = JsonDecoded(www.text);

				if (returnDict.ContainsKey("error"))
				{
					int errorCode = int.Parse (returnDict["code"].ToString());

					if (errorCode == 51) {
						UIController.ShowError("Cannot Add", "You cannot add any more friends because your friend list is already full.");
						SocialController.friendRequestStatus = false;
					}

					if (errorCode == 52) {
						UIController.ShowError("Cannot Add", "You cannot add this player as a friend because his or her friend list is already full.");
						SocialController.friendRequestStatus = false;
					}

					if (errorCode == 53) {
						UIController.ShowError("Cannot Add", "You cannot add any more friends because you've reached the maximum amount of requests you are able to send.");
						SocialController.friendRequestStatus = false;
					}

					if (errorCode == 54) {
						UIController.ShowError("Cannot Add", "You cannot add this player as a friend because his or her request list is already full.");
						SocialController.friendRequestStatus = false;
					}

					if (errorCode == 205) {
						UIController.ShowError("Cannot Add", "Sorry, you cannot add this player as a friend because he or she does not have an active profile.");
						SocialController.friendRequestStatus = false;
					}

					if (errorCode == 3001) {
						UIController.ShowPopUpSingleYes("Session Error", "You have been logged in from another device. Please exit the game and log in again.", "EXIT", LogOutToMenu);
//						Debug.Log("Session Error");

					}

					else if (errorCode == 3002) {
						UIController.ShowPopUpSingleYes("Version Error", "Your app version is out of date. Please update to the latest version of the game to continue playing.", "EXIT", ShowAppUpdate);
//						Debug.Log("Version Error");

					}
				}

				if (returnDict.ContainsKey("response"))
				{
					string responseCode = returnDict["response"].ToString();

					if (responseCode == "true") {
						if (onCallSuccess != null) onCallSuccess();
					}
					else if (responseCode == "false") {
						if (onCallFail != null) onCallFail();
					}
				}
			}

			if (onCallSuccess != null) onCallSuccess();
		} else {
//			Debug.Log("WWW Error: "+ www.error);
			if (onCallFail != null) onCallFail();
		}

	} 

	void ShowAppUpdate ()
	{
		Application.OpenURL("http://itunes.apple.com/app/id797701494");
		LogOutToMenu();
	}

	void LogOutToMenu ()
	{
		UIController.ClosePopUp();
		MenuController.LoadToScene(0);
	}


	IEnumerator WaitForRequestFriendList(WWW www)
	{
		yield return www;
		
		// check for errors
		if (www.error == null)
		{
			//Debug.Log(www.text);
			//result = www.text;
			
//			Debug.Log("Encrypt = " + www.text);
			string[] data;
			data = www.text.Split(new string[] { "__blite__" }, StringSplitOptions.None);
			result = data[0];
			data = data[1].Split(new string[] { "__blite2__" }, StringSplitOptions.None);
			string hash = data[0];
			string IV = data[1];
			if (Md5Sum(IV + result + secretKey2) == hash) {
				result = AES.DecryptStringFromBytes(Convert.FromBase64String(result), Convert.FromBase64String(key), Convert.FromBase64String(IV));
				//DataController.DecodeBigData(JsonDecoded(result));
//				Debug.Log("Result = " + (result));
				CalibrateFriendListData(result);
				if (onCallSuccess != null) onCallSuccess();
			}
		} else {
//			Debug.Log("WWW Error: "+ www.error);
			if (onCallFail != null) onCallFail();
		}
	}

	IEnumerator WaitForVersion(WWW www)
	{
		yield return www;
		if (www.error == null)
		{
			if (www.text != null && www.text != "") {
				Dictionary<string, object> returnDict = JsonDecoded(www.text);
				if (returnDict.ContainsKey("version"))
				{
					string responseCode = returnDict["version"].ToString();
				}
			}
			if (onCallSuccess != null) onCallSuccess();
		} else {
			if (onCallFail != null) onCallFail();
		}
	}
	
	IEnumerator WaitForTimeServer(WWW www)
	{
		yield return www;
		if (www.error == null)
		{
			if (www.text != null && www.text != "") {
				Dictionary<string, object> returnDict = JsonDecoded(www.text);
				if (returnDict.ContainsKey("server_time"))
				{
					string serverTime = returnDict["server_time"].ToString();
				}
			}
			
			if (onCallSuccess != null) onCallSuccess();
		} else {
			if (onCallFail != null) onCallFail();
		}
	}

	IEnumerator WaitForIAP(WWW www)
	{
		yield return www;
		if (www.error == null)
		{
			//Debug.Log(www.text);
			if (www.text != null && www.text != "") {
				Dictionary<string, object> returnDict = JsonDecoded(www.text);

				if (returnDict != null && returnDict.Count != 0) {


					if (returnDict.ContainsKey("credit"))
					{
						string credit = returnDict["credit"].ToString();
//						Debug.Log("Credit Returned = " + credit);

						// Add the Credits to the Profile
						ProfileController.Credits = int.Parse(credit);
						UIController.UpdateCurrency();
					}

					if (onCallSuccess != null) onCallSuccess();

				}
			}
			

		} else {

			if (onCallFail != null) onCallFail();

		}
	}
	
	IEnumerator WaitForHC(WWW www)
	{
		yield return www;
		if (www.error == null)
		{
			Debug.Log(www.text);
			if (www.text != null && www.text != "") {

			}
			
			if (onCallSuccess != null) onCallSuccess();
		} else {
			if (onCallFail != null) onCallFail();
		}
	}

	IEnumerator WaitForCargo(WWW www)
	{
		yield return www;
		if (www.error == null)
		{
			Debug.Log(www.text);
			if (www.text != null && www.text != "") {
				
			}
			
			if (onCallSuccess != null) onCallSuccess();
		} else {
			if (onCallFail != null) onCallFail();
		}
	}

	private void CalibrateFriendListData(string result)                                                                                        
	{                                                                                                                                                                                                               		      
//		Debug.Log("Calibrating Friend List Data: " + result);
		
		Dictionary<string, object> temp;
		temp = Json.Deserialize(result) as Dictionary<string, object>;
		List<object> friendList = ((List<object>) temp["friend_list"]);
		
		SocialController.allAmazonFriendList = new List<FriendItem>();
		
		foreach(Dictionary<string, object> list in friendList) {
			foreach(KeyValuePair<string, object> entry in list)
			{
				// do something with entry.Value or entry.Key
//				Debug.Log("Key = " + entry.Key + "Value = " + entry.Value);
			}
			
			FriendItem friendItem = new FriendItem();
			friendItem.name = list["DisplayName"].ToString(); // "Testing";
			friendItem.realName = list["DisplayName"].ToString(); // "Testing";
			friendItem.displayName = list["DisplayName"].ToString(); // "Testing";
			friendItem.status = (FriendItem.FriendStatus) FriendItem.FriendStatus.Parse(typeof(FriendItem.FriendStatus), list["status"].ToString()); 
			friendItem.id = list["id"].ToString();
			friendItem.shipId = int.Parse(list["tShipId"].ToString());
			// friendItem.id = (int) list["uid"];
			SocialController.allAmazonFriendList.Add(friendItem);
		}

		// MenuController.LoadToScene(10);
	}

	public string Md5Sum(string strToEncrypt)
	{
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes(strToEncrypt);

		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash(bytes);

		string hashString = "";
		
		for (int i = 0; i < hashBytes.Length; i++)
		{
			hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
		}
		
		return hashString.PadLeft(32, '0');
	}

	private WWWForm EncryptedForm (String jsonData) 
	{
		Dictionary<string, object> dict = Json.Deserialize(jsonData) as Dictionary<string,object>;
		TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
		long secondsSinceEpoch = (long)t.TotalMilliseconds;
		
		dict.Add("session_time",secondsSinceEpoch);
		
		jsonData = Json.Serialize(dict);
		//jsonData = JsonEncodedAll(action);
		Debug.Log ("Data Json =" + jsonData);
		jsonData = Encrypted (jsonData);
		
		//	Debug.Log ("Data =" + jsonData);
		//Debug.Log ("IV = " + IV);
		//Debug.Log(AES.DecryptStringFromBytes(Convert.FromBase64String(jsonData),Convert.FromBase64String(key),Convert.FromBase64String(IV)));
		
		string hash = Md5Sum(IV + jsonData + secretKey);
		WWWForm form = new WWWForm();
		//form.AddField("action", action);
		form.AddField("jsonData", jsonData);
		//		form.AddField("jsonData2", IV);
		//		form.AddField("jsonData3", hash);
		form.AddField("IV", IV);
		form.AddField("hash", hash);
		return form;

	}

	private string Encrypted (string data)
	{
		try
		{
			string original = data;
			using (RijndaelManaged myRijndael = new RijndaelManaged())
			{
				myRijndael.Key = Convert.FromBase64String(key);
				myRijndael.GenerateIV();
				//myRijndael.IV = Convert.FromBase64String("Lhdrft6hS/rhgJyK2sOH/A==");
				IV = Convert.ToBase64String(myRijndael.IV);
				byte[] encrypted = AES.EncryptStringToBytes(original, myRijndael.Key, myRijndael.IV);
				return Convert.ToBase64String(encrypted);
			}
			
		}
		catch (Exception e)
		{
//			Debug.Log("Error: " + e.Message);
			return null;
		}
		return null;
	}

	public string JsonEncodedAll (string action, string overrideId = "NULL"){
		string jsonData;
		jsonData = "{ ";

		if (action == "RequestSmallData") {
			socialDataList = DataController.EncodedSmallData(action, overrideId);
		} else {
			socialDataList = DataController.EncodedBigData(action, overrideId);
		}

		for (int i = 0 ; i < socialDataList.Count; i++)

		{
			SocialData socialData = socialDataList[i];
			switch(socialData.dataType)
			{

			case SocialData.DataType.Int:
				jsonData += "\""+socialData.key+"\": "+socialData.intValue+"";
				break;
					
			case SocialData.DataType.Float:
				jsonData += "\""+socialData.key+"\": "+socialData.floatValue+"";
				break;
					
			case SocialData.DataType.String:
				jsonData += "\""+socialData.key+"\": \""+socialData.stringValue+"\"";
				break;
					
			}
			if (i < socialDataList.Count - 1) jsonData += ", "; 
		}

		jsonData += " }";


//		Debug.Log ("Encode: " + jsonData);
		return jsonData;
	}

	private string JsonEncodedID (string action){
		string jsonData;
		jsonData = "{ ";
		
		jsonData += "\"tID\": \""+SocialController.id+"\",";
		jsonData += "\"tIsGuest\": "+ ProfileController.Singleton.isGuest +",";
		jsonData += "\"versionID\": "+ ProfileController.VersionID +",";
		jsonData += "\"action\": \""+action+"\"";

		
		jsonData += " }";
		return jsonData;
	}
	private string JsonEncodedIAP (string action, string code){
		string jsonData;
		jsonData = "{ ";
		
		jsonData += "\"tID\": \""+SocialController.id+"\",";
		jsonData += "\"IAP_HC\": \""+code+"\",";
		jsonData += "\"tIsGuest\": "+ ProfileController.Singleton.isGuest +",";
		jsonData += "\"action\": \""+action+"\"";

		jsonData += " }";
		
		return jsonData;
	}

	private string JsonEncodedHC (string action, int value){
		string jsonData;
		jsonData = "{ ";
		
		jsonData += "\"tID\": \""+SocialController.id+"\",";
		jsonData += "\"HC_Used\": \""+value+"\",";
		jsonData += "\"tIsGuest\": "+ ProfileController.Singleton.isGuest +",";
		jsonData += "\"action\": \""+action+"\"";
		
		jsonData += " }";
		
		return jsonData;
	}

	private string JsonEncodedCargo (string action, string value){
		string jsonData;
		jsonData = "{ ";
		
		jsonData += "\"tID\": \""+SocialController.id+"\",";
		jsonData += "\"Cargo_Type\": \""+value+"\",";
		jsonData += "\"tIsGuest\": "+ ProfileController.Singleton.isGuest +",";
		jsonData += "\"action\": \""+action+"\"";
		
		jsonData += " }";
		
		return jsonData;
	}

	private string JsonEncodedFriendID (string action){
		string jsonData;
		jsonData = "{ ";
		
		jsonData += "\"tID\": \""+SocialController.id+"\",";
		jsonData += "\"tIsGuest\": "+ ProfileController.Singleton.isGuest +",";
		jsonData += "\"FriendID\": "+SocialController.friendId+",";
		jsonData += "\"action\": \""+action+"\"";
		//jsonData += "\"Status\": \"Request\"";
		
		jsonData += " }";
		
		return jsonData;
	}

	private string JsonEncodedAction (string action){

		string jsonData;
		jsonData = "{ ";
		jsonData += "\"action\": \""+action+"\"";
		jsonData += " }";
		
		return jsonData;
	}

	private string JsonEncodedSimple (string action, int value = 0){
		string jsonData;
		jsonData = "{ ";
		
		jsonData += "\"tID\": \""+SocialController.id+"\",";
		jsonData += "\"tIsGuest\": "+ ProfileController.Singleton.isGuest +",";
		jsonData += "\"action\": \""+action+"\",";
		jsonData += "\"tValue\": \""+value+"\"";
		//jsonData += "\"Status\": \"Request\"";
		
		jsonData += " }";
		
		return jsonData;
	}

	public Dictionary<string, object> JsonDecoded (string jsonData) {

		Dictionary<string, object> dict = new Dictionary<string, object>();

		try {
			dict = Json.Deserialize(jsonData) as Dictionary<string,object>;
			foreach(KeyValuePair<string, object> entry in dict)
			{
				// do something with entry.Value or entry.Key
				//Debug.Log("Key = " + entry.Key + "Value = " + entry.Value);
			}
		} catch (Exception e) {

			if (onCallFail != null ) {
				onCallFail();
			} else {
				UIController.ShowPopup("ERROR", "Unknown Error Encountered.");
			}

			Debug.Log("FAIL");
		}

		return dict;
	}

	void JsonTest (string jsonData) {

		string jsonString = jsonData;
		
		Dictionary<string, object> dict = Json.Deserialize(jsonString) as Dictionary<string,object>;

		foreach(KeyValuePair<string, object> entry in dict)
		{
			// do something with entry.Value or entry.Key
			//Debug.Log("Key = " + entry.Key + "Value = " + entry.Value);
		}
		
		string str = Json.Serialize(dict);
		
		//Debug.Log("serialized: " + str);

	}
}
