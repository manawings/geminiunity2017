﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Security.Cryptography;

public class RijndaelEncrypted {
	public string AES_Encrypted (string data)
	{
		try
		{
			string original = data;
			
			// Create a new instance of the RijndaelManaged 
			// class.  This generates a new key and initialization  
			// vector (IV). 
			using (RijndaelManaged myRijndael = new RijndaelManaged())
			{
				//myRijndael.GenerateKey();
			//	myRijndael.GenerateIV();
			//Debug.Log("Key:" + Convert.ToBase64String(myRijndael.Key));
				myRijndael.Key = Convert.FromBase64String("+HIp3NJk7CUWfwuG8+aKJIyvU3y5/6zdT3EGi6dOdUQ=");
				myRijndael.IV = Convert.FromBase64String("Lhdrft6hS/rhgJyK2sOH/A==");

				// Encrypt the string to an array of bytes. 
				byte[] encrypted = EncryptStringToBytes(original, myRijndael.Key, myRijndael.IV);
				
				// Decrypt the bytes to a string. 
				//string roundtrip = DecryptStringFromBytes(encrypted, myRijndael.Key, myRijndael.IV);
				
				return Convert.ToBase64String(encrypted);
			}
			
		}
		catch (Exception e)
		{
//		Debug.Log("Error: " + e.Message);
		return null;
		}
		return null;
	}

	public byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
	{
		// Check arguments. 
		if (plainText == null || plainText.Length <= 0)
			throw new ArgumentNullException("plainText");
		if (Key == null || Key.Length <= 0)
			throw new ArgumentNullException("Key");
		if (IV == null || IV.Length <= 0)
			throw new ArgumentNullException("Key");
		byte[] encrypted;
		// Create an RijndaelManaged object 
		// with the specified key and IV. 
		using (RijndaelManaged rijAlg = new RijndaelManaged())
		{
			//rijAlg.Mode = CipherMode.ECB;
			rijAlg.Key = Key;
			rijAlg.IV = IV;
			
			// Create a decrytor to perform the stream transform.
			ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);
			
			// Create the streams used for encryption. 
			using (MemoryStream msEncrypt = new MemoryStream())
			{
				using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
				{
					using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
					{
						
						//Write all data to the stream.
						swEncrypt.Write(plainText);
					}
					encrypted = msEncrypt.ToArray();
				}
			}
		}
		
		
		// Return the encrypted bytes from the memory stream. 
		return encrypted;
		
	}
	
	public string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
	{
		// Check arguments. 
		if (cipherText == null || cipherText.Length <= 0)
			throw new ArgumentNullException("cipherText");
		if (Key == null || Key.Length <= 0)
			throw new ArgumentNullException("Key");
		if (IV == null || IV.Length <= 0)
			throw new ArgumentNullException("Key");
		
		// Declare the string used to hold 
		// the decrypted text. 
		string plaintext = null;
		
		// Create an RijndaelManaged object 
		// with the specified key and IV. 
		using (RijndaelManaged rijAlg = new RijndaelManaged())
		{
			//rijAlg.Mode = CipherMode.ECB;
			rijAlg.Key = Key;
			rijAlg.IV = IV;
			rijAlg.Padding = PaddingMode.None;
			
			// Create a decrytor to perform the stream transform.
			ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
			
			// Create the streams used for decryption. 
			using (MemoryStream msDecrypt = new MemoryStream(cipherText))
			{
				using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
				{
					using (StreamReader srDecrypt = new StreamReader(csDecrypt))
					{
						
						// Read the decrypted bytes from the decrypting stream 
						// and place them in a string.
						plaintext = srDecrypt.ReadToEnd();
					}
				}
			}
			
		}
		
		return plaintext;
		
	}

	public byte[] GetBytes(string str)
	{
		byte[] bytes = new byte[str.Length * sizeof(char)];
		System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
		return bytes;
	}
	
	public string GetString(byte[] bytes)
	{
		char[] chars = new char[bytes.Length / sizeof(char)];
		System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
		return new string(chars);
	}

	public static T[] RemoveAt<T>(T[] source, int index)
	{
		T[] dest = new T[source.Length - 1];
		if( index > 0 )
			Array.Copy(source, 0, dest, 0, index);
		
		if( index < source.Length - 1 )
			Array.Copy(source, index + 1, dest, index, source.Length - index - 1);
		
		return dest;
	}

	public void ByteLog (string SB) {
		byte [] bKey = {0};
		string outputString = string.Empty;

		bKey = GetBytes(SB);
		foreach (byte s in bKey) {
			string exeFolder = System.IO.Path.GetDirectoryName(Application.dataPath);
			using(StreamWriter writer = new StreamWriter(exeFolder + "\\Log\\"+"byte.txt", true))
			{
				outputString += s + ",";
				writer.WriteLine(s + ",");
			}
		}

		Debug.Log (outputString);
	}
}
