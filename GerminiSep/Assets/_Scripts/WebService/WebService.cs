﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Security.Cryptography;
using MiniJSON;
using CodeStage.AntiCheat.ObscuredTypes;

public class WebService : MonoBehaviour {

	private static GameObject parentObject;
	private static WebService _Singleton;
	public static WebService Singleton {
		get {
			if (!_Singleton)
			{
				parentObject = new GameObject("WebService");
				DontDestroyOnLoad(parentObject);
				_Singleton = parentObject.AddComponent<WebService>();
				//_Singleton.Init();
			}
			return _Singleton;
		}
	}

	private ObscuredString url = "http://aws.amazon.com/biltz";
	private ObscuredString url2 = "http://aws.amazon.com/biltz2";

	private byte [] bUrl = {104,0,116,0,116,0,112,0,58,0,47,0,47,0,103,0,101,0,109,0,105,0,110,0,105,0,45,0,115,0,101,0,114,0,118,0,105,0,99,0,101,0,115,0,46,0,115,0,121,0,115,0,46,0,97,0,114,0,109,0,111,0,114,0,103,0,97,0,109,0,101,0,115,0,46,0,110,0,101,0,116,0,47,0};
	private byte [] bUrl2 = {104,0,116,0,116,0,112,0,58,0,47,0,47,0,107,0,114,0,105,0,110,0,115,0,116,0,117,0,100,0,105,0,111,0,46,0,99,0,111,0,109,0,47,0,98,0,108,0,105,0,116,0,122,0,47,0,97,0,119,0,115,0,47,0,115,0,116,0,97,0,114,0,95,0,115,0,116,0,114,0,105,0,107,0,101,0,46,0,112,0,104,0,112,0};

	RijndaelEncrypted AES = new RijndaelEncrypted ();

	public KJTime.TimeDelegate onCallSuccess, onCallFail;

	void Awake () {
		url = AES.GetString(bUrl2);
		url2 = AES.GetString(bUrl2);
	}

	public void ConnectWebService (string action, KJTime.TimeDelegate onCallSuccess = null, KJTime.TimeDelegate onCallFail = null) {
		WWW www;
		www = new WWW(url2, EncryptedForm(action));
		StartCoroutine (WaitForServerTime (www));
		this.onCallSuccess = onCallSuccess;
		this.onCallFail = onCallFail;
	}

	private WWWForm EncryptedForm (string action) 
	{
		WWWForm form = new WWWForm();
		form.AddField("action", action);
		return form;
	}

	IEnumerator WaitForServerTime(WWW www)
	{
		yield return www;
		if (www.error == null)
		{
			if (www.text != null && www.text != "") {
				Dictionary<string, object> returnDict = JsonDecoded(www.text);
				if (returnDict.ContainsKey("server_time"))
				{
					string response = returnDict["server_time"].ToString();
					SocialController.serverTime = long.Parse(response);
				}
			}
			if (onCallSuccess != null) onCallSuccess();
		} else {
			if (onCallFail != null) onCallFail();
		}
	}

	public Dictionary<string, object> JsonDecoded (string jsonData) {
		
		Dictionary<string, object> dict = new Dictionary<string, object>();
		
		try {
			dict = Json.Deserialize(jsonData) as Dictionary<string,object>;
			foreach(KeyValuePair<string, object> entry in dict)
			{
				// do something with entry.Value or entry.Key
				//Debug.Log("Key = " + entry.Key + "Value = " + entry.Value);
			}
		} catch (Exception e) {
			
			if (onCallFail != null ) {
				onCallFail();
			} else {
				//UIController.ShowPopup("ERROR", "Unknown Error Encountered.");
			}
			
			//Debug.Log("FAIL");
		}
		
		return dict;
	}
}
