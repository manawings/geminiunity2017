using UnityEngine;
using System.Collections;

public class HangarController : KJBehavior {
	
	public GameObject hangarPanel;
	public GameObject itemPanel;
	public GameObject previewPanel;
	public GameObject inventoryPanel;
	public GameObject mapPanel;
	public GameObject shipPanel;
	public GameObject shipPreviewPanel;
	
	public GameObject playButton;
	public GameObject backButton;
	
	GameObject currentPanel;
	GameObject targetPanel;
	
	public PreviewShip previewShip;
	public Aimer previewAimer;
	
	public KJBar progressBar;
	public UILabel sectorLabel;
	public UILabel progressLabel;
	
	public enum PanelType {
		Hangar,
		Item,
		Preview,
		Inventory,
		Back,
		Map,
		Ship,
		ShipPreview
	}
	
	[HideInInspector] public ItemBasic.Type currentItemType;
	
	public static HangarController Singleton { get; private set; }
	
	Hashtable tweenHash;
	ItemBasic previewItem;
	ItemShip previewItemShip;

	void Start () {
		
		Singleton = this;
		
		NetworkController.Singleton.CheckAndTest();
		
		AddTimer(Deploy, 0.05f, 1);
		outPosition = tk2dCamera.Instance.ScreenExtents.width;
		
		currentPanel = hangarPanel;
		
		tweenHash = new Hashtable();
        tweenHash.Add("from", 0.0f);
        tweenHash.Add("to", 1.0f);
        tweenHash.Add("time", 0.5f);
        tweenHash.Add("onupdate", "Run");
		tweenHash.Add("oncomplete", "OnComplete");
		tweenHash.Add("easetype", iTween.EaseType.easeInOutQuad);
		
		CalibrateHangarPanel();
		
		
		
		backButton.SetActive (false);
		
	}

	
	void Run (float val)
	{
		KJMath.SetX(targetPanel.transform, (1.0f - val) * positionForTarget);
		KJMath.SetX(currentPanel.transform, val * positionForCurrent);
		
		if (currentPanel == previewPanel || targetPanel == previewPanel)
		{
			previewPanel.GetComponent<PreviewPanel>().RunPreviewFollow();
		}
	}
	
	void OnComplete ()
	{
		
		
		
		if (targetPanel == hangarPanel)
		{
			backButton.SetActive(false);
			// playButton.SetActive(true);
		}
		/*
		if (targetPanel == previewPanel) {
			
			if (previewItem.type == ItemBasic.Type.Ship) {
				previewShip.StartEmitters();
			} else if (previewItem.type == ItemBasic.Type.Weapon) {
				previewAimer.ShowAimer();
			}
		}
		
		if (currentPanel == previewPanel) {
			
			if (previewItem.type == ItemBasic.Type.Ship) {
				previewShip.Hide();
			} else if (previewItem.type == ItemBasic.Type.Weapon) {
				previewAimer.HideAimer();
				previewAimer.gameObject.SetActive(false);
			}
		}
		*/
		currentPanel.SetActive(false);
		currentPanel = targetPanel;
		
		InputController.UnlockControls();
	}
	
	void Deploy ()
	{
		UIController.UpdateCurrency();
	}
	
	float outPosition;
	
	float positionForCurrent, positionForTarget;
	
	GameObject GetBackPanel ()
	{

		if (currentPanel == previewPanel) return inventoryPanel;
		if (currentPanel == shipPreviewPanel) return shipPanel;
		
		return hangarPanel;
	}
	
	void CalibrateHangarPanel ()
	{
		HangarButton[] hangarButtons = hangarPanel.GetComponentsInChildren<HangarButton>();
		foreach ( HangarButton hangarButton in hangarButtons)
		{
			hangarButton.Calibrate();	
		}
		
		float sectorMax = 5.0f;
		float sectorNow = (float) ProfileController.SubSector;
		float progress = (sectorNow - 1) / sectorMax;
		progressBar.SetProgress(progress, true);
		
		
		sectorLabel.text = "Sector " + ProfileController.SectorId;
		progressLabel.text = "Level " + ProfileController.SubSector + " of " + 5;
	}
	
	public void SwapTo (PanelType panelType, bool backwards)
	{
		
		if (panelType == PanelType.Item) targetPanel = itemPanel;
		if (panelType == PanelType.Hangar) targetPanel = hangarPanel;
		if (panelType == PanelType.Preview) targetPanel = previewPanel;
		if (panelType == PanelType.Inventory) targetPanel = inventoryPanel;
		if (panelType == PanelType.Map) targetPanel = mapPanel;
		if (panelType == PanelType.Ship) targetPanel = shipPanel;
		if (panelType == PanelType.ShipPreview) targetPanel = shipPreviewPanel;
		if (panelType == PanelType.Back) targetPanel = GetBackPanel();	
		
		if (backwards) {
			
			positionForCurrent = outPosition;
			positionForTarget = -outPosition;
			KJMath.SetX(targetPanel.transform, -outPosition);
			
		} else {
			
			positionForCurrent = -outPosition;
			positionForTarget = outPosition;
			KJMath.SetX(targetPanel.transform, outPosition);
			
		}
		
		targetPanel.SetActive(true);

		iTween.ValueTo(gameObject, tweenHash);
		
		if (targetPanel != hangarPanel)
		{
			backButton.SetActive(true);
		} else {
			CalibrateHangarPanel();
		}
		
		if (targetPanel == itemPanel) itemPanel.GetComponent<ItemPanel>().Refresh();
		if (targetPanel == shipPanel) shipPanel.GetComponent<ShipPanel>().Refresh();
		
		if (targetPanel == inventoryPanel) {
			inventoryPanel.GetComponent<InventoryPanel>().Deploy();
			inventoryPanel.GetComponentInChildren<ItemGrid>().Refresh();
		}
		
		if (targetPanel == mapPanel) mapPanel.GetComponent<MapPanel>().Refresh();
		
		
		if (targetPanel == previewPanel)
		{
			// previewPanel.GetComponent<PreviewPanel>().RunPreviewFollow();
		}
		
		if (currentPanel == previewPanel)
		{
			previewShip.EndEmitters();
		//	if (previewItem.type == ItemBasic.Type.Weapon) previewAimer.HideAimer();
		}
		
		if (targetPanel == hangarPanel)
		{
			CalibrateHangarPanel();	
		}
	}

	public void CalibratePreviewForItem(ItemBasic item)
	{
		previewItem = item;
		previewPanel.GetComponent<PreviewPanel>().Calibrate(item);
		
		/*
		if (item.type == ItemBasic.Type.Ship) {
			previewShip.Deploy(item);
		} else if (item.type == ItemBasic.Type.Weapon) {
			previewAimer.gameObject.SetActive(true);
			previewAimer.Deploy(3);
		}
		*/
	}
	
	public void CalibratePreviewForShip(ItemShip ship)
	{
		
		previewItemShip = ship;
		shipPreviewPanel.GetComponent<ShipPreviewPanel>().Calibrate(previewItemShip);
		
		//if (previewItemShip.type == ItemBasic.Type.Ship) {
			//previewShip.Deploy(previewItemShip);
		//} else if (item.type == ItemBasic.Type.Weapon) {
			//previewAimer.gameObject.SetActive(true);
			//previewAimer.Deploy(3);
		//}
	}
	
	void ShowPeers ()
	{
		/*
		#if UNITY_IPHONE
		Debug.Log("Show Peers");
		GameKitBinding.showPeerPicker(true);
		#endif
		*/
	}
	
	
}
