using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

public class CargoController : MonoBehaviour {

	public enum CargoMode {
		Normal,
		BlackMarket
	}

	public CargoMode cargoMode = CargoMode.Normal;


	public GameObject[] firstPanels;
	public GameObject[] lastPanels;

	int firstPanelCount = 0;
	int lastPanelCount = 0;

	public UISprite chestFull;

	public CargoChestPart chestPartTop;
	public CargoChestPart chestPartLeft;
	public CargoChestPart chestPartRight;

	public UISprite itemGlow;
	public Color chestGlowColor;
	public ItemSlot itemSlot;
	public UILabel itemLabel;
	public UILabel rarityLabel;
	public UILabel itemDescLabel;

	public UILabel cargoTitle;
	public UILabel cargoDesc;

	int creditCost = 5;

	public tk2dSpriteCollectionData spriteCollection;


	KJEmitter sparkEmitter;
	KJEmitter sparkEmitter2;
	KJEmitter sparkEmitter3;

	KJEmitter sparkEmitterB1;
	KJEmitter sparkEmitterB2;
	KJEmitter sparkEmitterB3;

	bool isSparkSplit = false;
	float itemYSave = 0;
	UISprite[] itemSlotSprites;

	public GameObject sparkObject;
	public GameObject sparkObjectB;

	public BlitzButton cargoBuyButton;

	ObscuredInt cargoPrice = 100;
	ObscuredInt payPrice = 0;

	ItemBasic newItem;

	public enum CargoType {
		normal,
		blackMarket,
		quest
	}

	public CargoType cargoType;

	// Use this for initialization
	void Start () {
		KJTime.Add(Deploy, 0.05f, 1);
		KJParticleController.Initialize(spriteCollection);

		foreach (GameObject gameObject in firstPanels)
		{
			gameObject.SetActive(true);
		}

		foreach (GameObject gameObject in lastPanels)
		{
			gameObject.SetActive(false);
		}

		chestFull.gameObject.SetActive (true);
		itemSlot.gameObject.SetActive(false);
		itemGlow.gameObject.SetActive(false);

		chestPartTop.gameObject.SetActive(false);
		chestPartLeft.gameObject.SetActive(false);
		chestPartRight.gameObject.SetActive(false);

		if (cargoBuyButton != null) 
		{

		}

		if (cargoTitle != null) {

			if (ProfileController.secretMission) {
				cargoTitle.text = "SECRET CARGO BOX";
				cargoDesc.text = "Contains rare cargo, and can be unlocked at a lower cost.";
			} else {
				cargoTitle.text = "CLASSIFIED CARGO BOX";
				cargoDesc.text = "Unlock the box to find a rare and powerful item.";
			}

		}



		if (ProfileController.secretMission) {
			creditCost = ItemBasic.SecretCargoCost;
		} else {
			creditCost = ItemBasic.BlackMarketCost;
		}

		if (cargoBuyButton != null) {

			if (cargoMode == CargoMode.Normal) {

				cargoPrice = Stats.BoxCostForLevel(ProfileController.SectorLevel);
				cargoBuyButton.defaultPrice = cargoPrice;
//				Debug.Log ("Credit Price: " + cargoPrice);



			} else {

				cargoBuyButton.defaultPrice = creditCost;
//				Debug.Log ("Credit Price: " + creditCost);

			}

			cargoBuyButton.SetOptions();

		}


	}
	
	void Deploy ()
	{
		if (cargoMode == CargoMode.Normal) {
			FlurryController.NormalCargoEncountered();
		}

		if (ProfileController.secretMission) {
			cargoType = CargoType.quest;
		}
		UIController.UpdateCurrency();
	}
	
	void BuyCargo ()
	{


		payPrice = cargoPrice;

		if (ProfileController.Gems >= cargoPrice)
		{

			ProfileController.Gems -= cargoPrice;
			newItem  = ProfileController.GainCargoItem();

			DataController.SaveAllData(false, BuyCargoActual, OnCargoBuyFail);
			
			if (SocialController.isLoggedIn)
				UIController.ShowWait(15, null, BuyCargo, OnCargoBuyFail);

		} else {

			UIController.ShowPopupNoGems();
		}
	}
	

	void BuyBlackMarket ()
	{

		payPrice = creditCost;

		if (ProfileController.Credits >= payPrice)
		{
			ProfileController.Credits -= payPrice;
			newItem  = ProfileController.GainBlackMarketItem();

			DataController.SaveAllData(false, BuyCargoActual);
			
			if (SocialController.isLoggedIn)
				UIController.ShowWait(15, null, BuyBlackMarket, OnCargoBuyFail);

		} else {
			UIController.ShowPopupNoCredits();
		}
	}

	void OnCargoBuyFail ()
	{
		// Undo the additions before the saving.
		ProfileController.Singleton.inventoryList.Remove(newItem);

		if (cargoMode == CargoMode.BlackMarket) {

			ProfileController.Credits += payPrice;

		} else {

			ProfileController.Gems += payPrice;

		}

		UIController.UpdateCurrency();
	}

	void BuyCargoActual ()
	{

		if (cargoMode == CargoMode.Normal) {
			
			FlurryController.NormalCargoOpened();
			
		} else {
			
			if (cargoType == CargoType.blackMarket) {

				FlurryController.CargoBlackmarket();

			} else if (cargoType == CargoType.quest){

				FlurryController.CargoQuest();

			}
			
		}

		UIController.ClosePopUp();
		
		KJTime.Add(MoveNextFirstPanelOut, 0.15f);
		MoveNextFirstPanelOut();
		
		UIController.UpdateCurrency();
	}

	void MoveNextFirstPanelOut ()
	{
		if (firstPanelCount != firstPanels.Length) {

			GameObject moveObject = firstPanels[firstPanelCount];

			Hashtable param = new Hashtable();
			param.Add("time", 0.50f);
			param.Add("amount", Vector3.down * 1.00f);
			param.Add("easetype", iTween.EaseType.easeInBack);
			iTween.MoveBy(moveObject, param);

			firstPanelCount ++;

		} else {

			KJTime.Remove(MoveNextFirstPanelOut);
			KJTime.Add(MoveChestDown, 0.15f, 1);
		}
	}

	void MoveChestDown ()
	{

		Hashtable param = new Hashtable();
		param.Add("time", 0.50f);
		param.Add("y", 0.0f);
		param.Add("easetype", iTween.EaseType.easeInOutCirc);
		iTween.MoveTo(chestFull.gameObject, param);



		// sparkEmitter.transform.

		KJTime.Add(BeginSpark, 0.5f, 1);


	}

	void BeginSpark ()
	{

		KJCanary.PlaySoundEffect("BoxOpening");

		sparkObject.transform.localPosition = Vector3.down * 80.0f;

		sparkEmitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		sparkEmitter.CopyFromModel(EffectController.Singleton.blueFlare);
		sparkEmitter.SetColor(
			new Color (1.0f, 0.8f, 0.3f, 1.0f),
			new Color (0.8f, 0.0f, 0.0f, 1.0f));
		sparkEmitter.Deploy(sparkObject.transform.localPosition);
		sparkEmitter.SetObjectLink(sparkObject);

		sparkEmitter2 = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		sparkEmitter2.CopyFromModel(EffectController.Singleton.boxFlare);
		sparkEmitter2.Deploy(sparkObject.transform.localPosition);
		sparkEmitter2.SetObjectLink(sparkObject);

		sparkEmitter3 = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		sparkEmitter3.CopyFromModel(EffectController.Singleton.boxFireTrail);
		sparkEmitter3.Deploy(sparkObject.transform.localPosition);
		sparkEmitter3.SetObjectLink(sparkObject);


		KJTime.Add(RunSpark);
		iTween.ShakePosition(chestFull.gameObject, Vector3.one * 0.025f, 2.5f);
		KJTime.Add(SplitSpark, 1.0f, 1);
		KJTime.Add(ShowItem, 2.0f, 1);

	}

	void SplitSpark ()
	{
		sparkObjectB.transform.localPosition = Vector3.zero;
		sparkObject.transform.localPosition = Vector3.zero;
		
		sparkEmitterB1 = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		sparkEmitterB1.CopyFromModel(EffectController.Singleton.blueFlare);
		sparkEmitterB1.SetColor(
			new Color (1.0f, 0.8f, 0.3f, 1.0f),
			new Color (0.8f, 0.0f, 0.0f, 1.0f));
		sparkEmitterB1.Deploy(sparkObjectB.transform.localPosition);
		sparkEmitterB1.SetObjectLink(sparkObjectB);
		
		sparkEmitterB2 = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		sparkEmitterB2.CopyFromModel(EffectController.Singleton.boxFlare);
		sparkEmitterB2.Deploy(sparkObjectB.transform.localPosition);
		sparkEmitterB2.SetObjectLink(sparkObjectB);
		
		sparkEmitterB3 = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		sparkEmitterB3.CopyFromModel(EffectController.Singleton.boxFireTrail);
		sparkEmitterB3.Deploy(sparkObjectB.transform.localPosition);
		sparkEmitterB3.SetObjectLink(sparkObjectB);

		isSparkSplit = true;
	}

	void RunSpark ()
	{
		if (isSparkSplit) {
			sparkObject.transform.localPosition = sparkObject.transform.localPosition + new Vector3(-0.8f, 0.45f, 0.0f) * 1.5f;
			sparkObjectB.transform.localPosition = sparkObjectB.transform.localPosition + new Vector3(0.8f, 0.45f, 0.0f) * 1.5f;
			chestFull.color = Color.Lerp(chestFull.color, chestGlowColor, 0.01f);
		} else {
			sparkObject.transform.localPosition = sparkObject.transform.localPosition + Vector3.up * 1.4f;
			chestFull.color = Color.Lerp(chestFull.color, chestGlowColor, 0.003f);
		}



	}

	void ShowItem ()
	{
		KJCanary.PlaySoundEffect("BoxOpen");
		KJTime.Remove(RunSpark);

		sparkEmitter.Terminate();
		sparkEmitter2.Terminate();
		sparkEmitter3.Terminate();

		sparkEmitterB1.Terminate();
		sparkEmitterB2.Terminate();
		sparkEmitterB3.Terminate();

		chestFull.gameObject.SetActive(false);
		itemSlot.gameObject.SetActive(true);

		chestPartTop.gameObject.SetActive(true);
		chestPartLeft.gameObject.SetActive(true);
		chestPartRight.gameObject.SetActive(true);

		chestPartTop.Deploy(chestFull.color, 56.0f, 0.0f, 0.2f);
		chestPartLeft.Deploy(chestFull.color, -2.9f, -42.0f, 0.7f);
		chestPartRight.Deploy(chestFull.color, -2.9f, 42.0f, -0.7f);

		itemYSave = itemSlot.transform.position.y;
		itemSlot.transform.position = Vector3.zero;

		itemSlot.Deploy(newItem, true);
		itemSlotSprites = itemSlot.GetComponentsInChildren<UISprite>();
		foreach (UISprite sprite in itemSlotSprites)
		{
			sprite.color = newItem.TextColor;
		}

		itemGlow.gameObject.SetActive(true);
		itemGlow.transform.parent = itemSlot.transform;
		itemGlow.transform.localPosition = Vector3.forward * 20;
		itemGlow.MakePixelPerfect();

		itemSlot.transform.localScale = Vector3.zero;

		Hashtable param = new Hashtable();
		param.Add("time", 0.35f);
		param.Add("x", 1.00f);
		param.Add("y", 1.00f);
		param.Add("z", 1.00f);
		param.Add("easetype", iTween.EaseType.easeOutBack);
		iTween.ScaleTo(itemSlot.gameObject, param);

		itemLabel.text = newItem.Name;
		itemLabel.color = newItem.TextColor;
		itemGlow.color = newItem.TextColor;

		KJEmitter newEmitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		newEmitter.SetColor(
			new Color (1.0f, 0.8f, 0.3f, 1.0f),
			new Color (0.8f, 0.0f, 0.0f, 0.0f));
		newEmitter.SetRadius(0.0f);
		newEmitter.SetPower(1200.0f, 0.0f, 0.85f);
		newEmitter.SetDirection(0.0f, Mathf.PI);
		newEmitter.SetDecay(0.3f, 1.5f);
		newEmitter.SetScale(1.0f);
		newEmitter.SetScale(1.0f);
		newEmitter.SetGraphic(KJParticleGraphic.Type.Laser);
		newEmitter.SetExplosion(60, 0.15f, 0, 0.85f);
		newEmitter.SetSpawnInDirection(true);
		newEmitter.SetDecceleration(0.85f);
		newEmitter.Explode(Vector3.zero, Vector3.zero);

		SetItemDesc(newItem);

		rarityLabel.text = ItemBasic.RarityNameOf(newItem.rarity).ToUpper() + " ITEM";
		rarityLabel.color = newItem.TextColor;

		KJTime.Add(RunGlow);
		KJTime.Add(SpinGlow);
		KJTime.Add(MoveEndPanelsIn, 2.5f, 1);


	}

	void MoveEndPanelsIn ()
	{

		Hashtable param = new Hashtable();
		param.Add("time", 0.50f);
		param.Add("y", itemYSave);
		param.Add("easetype", iTween.EaseType.easeOutCirc);
		iTween.MoveTo(itemSlot.gameObject, param);

		KJTime.Add(MoveNextLastPanelIn, 0.15f);
	}

	void MoveNextLastPanelIn ()
	{
		if (lastPanelCount != lastPanels.Length) {
			
			GameObject moveObject = lastPanels[lastPanelCount];


			Hashtable param = new Hashtable();
			param.Add("time", 0.50f);
			param.Add("y", moveObject.transform.position.y);
			param.Add("easetype", iTween.EaseType.easeOutCirc);

			moveObject.transform.position = moveObject.transform.position + Vector3.down * 0.85f;
			moveObject.SetActive(true);
			iTween.MoveTo(moveObject, param);
			
			lastPanelCount ++;
			
		} else {
			
			KJTime.Remove(MoveNextLastPanelIn);
		}
	}

	void SetItemDesc (ItemBasic item)
	{
		string descText = string.Empty;
		int lines = 0;
		bool hasDesc = false;
		
		// descText += "Level +" + item.level.ToString() + "\n";
		if (item.LifeFinal > 0.0f) {
			descText += "+ " + item.LifeFinal.ToString() + " Life"; lines++;
		}
		if (item.DamageFinal > 0.0f) {
			if (lines > 0) descText += "\n";
			descText += "+ " + item.DamageFinal.ToString() + " Damage"; lines++;
		}
		if (item.ArmorFinal > 0.0f) {
			if (lines > 0) descText += "\n";
			descText += "+ " + item.ArmorFinal.ToString() + " Armor"; lines++;
		}
		
		if (item.module != null ) {
			
			descText += "\n\n";
			descText += "[D8FFB8]" + item.module.name + ": [-]"; // item.module.name
			descText += "[89FF52]" + item.module.Description + "[-]"; 
			hasDesc = true;


		}

		itemDescLabel.text = descText;
	}

	void SpinGlow ()
	{
		itemGlow.transform.eulerAngles += Vector3.forward * 1.0f;
	}

	void RunGlow ()
	{

		foreach (UISprite sprite in itemSlotSprites)
		{
			sprite.color = Color.Lerp(sprite.color, Color.black, 0.05f);
		}
	}

	void ExitScene ()
	{
//		KJTime.RemoveGameTimers();
		MenuController.LoadToScene(2);
	}

	void GoToInventory ()
	{
//		KJTime.RemoveGameTimers();
		MenuController.LoadToScene(5);
	}

}
