﻿using UnityEngine;
using System.Collections;
using System.Net;
using System;
using System.IO;

public class NetworkController : KJBehavior {
	
	private static NetworkController _Singleton;
	
	public static NetworkController Singleton
	{
		get {
			if (_Singleton == null)
			{
				GameObject gameObject = new GameObject("NetworkController");
				DontDestroyOnLoad(gameObject);
				_Singleton = gameObject.AddComponent<NetworkController>();
				_Singleton.Initialize ();
			}
			
			return _Singleton;
		}
	}
	
	public bool isMultiplayer = false;
	
	void Initialize ()
	{

		// listen to all events for illustration purposes
		/*
		#if UNITY_IPHONE
		GameKitManager.peerConnected += onPeerConnected;
		GameKitManager.peerDisconnected += onPeerDisconnect;
		#endif
		*/

	}
	
	public void onPeerConnected( string peerId )
	{
		#if UNITY_IPHONE
		hasRolled = false;
		hasGottenRoll = false;
		
		Debug.Log( "PeerConnected event fired with peerId: " + peerId );
		
		// Start the game.
		SendRollForControl();
		#endif
	}
	
	
	public void onPeerDisconnect( string peerId )
	{
		
	}
	
	public void CheckAndTest ()
	{
		
	}
	
	public bool isInGame = false;

	public string GetHtmlFromUri(string resource)
	{
		string html = string.Empty;
		HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
		try
		{
			using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
			{
				bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
				if (isSuccess)
				{
					using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
					{
						//We are limiting the array to 80 so we don't have
						//to parse the entire html document feel free to 
						//adjust (probably stay under 300)
						char[] cs = new char[80];
						reader.Read(cs, 0, cs.Length);
						foreach(char ch in cs)
						{
							html +=ch;
						}
					}
				}
			}
		}
		catch
		{
			return "";
		}
		return html;
	}

	public bool CheckConnecttion
	{
		get {
			string HtmlText = GetHtmlFromUri("http://google.com");
			if(HtmlText == "")
			{
				return false;
			}
			else if(!HtmlText.Contains("schema.org/WebPage"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
//		get {
//			bool isConnectedToInternet = false;
////			Debug.Log(Network.player.ipAddress.ToString());
//			#if UNITY_EDITOR
//			if (Network.player.ipAddress.ToString() != "127.0.0.1" && Network.player.ipAddress.ToString() != "0.0.0.0")
//			{
//				isConnectedToInternet = true;       
//			}
//			#endif
//			#if UNITY_IPHONE
//			if (Application.internetReachability != NetworkReachability.NotReachable)
//			{
//				isConnectedToInternet = true;
//			}
//			#endif
//			#if UNITY_ANDROID
//			if (Application.internetReachability != NetworkReachability.NotReachable)	
//			{	
//				isConnectedToInternet = true;
//			}
//			#endif
//			#if (!UNITY_IPHONE && !UNITY_ANDROID)
//			if (Network.player.ipAddress.ToString() != "127.0.0.1" && Network.player.ipAddress.ToString() != "0.0.0.0")	
//			{	
//				isConnectedToInternet = true;
//			}
//			#endif
////			Debug.Log(isConnectedToInternet);
//			return isConnectedToInternet;
//		}
	}

	// Multiplayer Region
	
	public int allyItemId = 0;
	public int currentRandomSeed = 0;
	public int currentRoll = 0;
	int allyRoll = 0;
	bool hasRolled = false;
	bool hasGottenRoll = false;
	
	public void SendRollForControl ()
	{
		#if UNITY_IPHONE
		//if (!GameKitManager.isConnected()) return;
		
		hasRolled = true;
		
		currentRoll = UnityEngine.Random.Range(int.MinValue, int.MaxValue);
		string sendData = currentRoll.ToString();	
		//GameKitBinding.sendData("NetworkController", "RecRollForControl", sendData, true);
		
		if (hasGottenRoll) DetermineRollWinner();
		#endif
	}
	
	public void RecRollForControl (string param)
	{
		#if UNITY_IPHONE
		allyRoll = int.Parse(param);	
		hasGottenRoll = true;
		
		if (hasRolled) DetermineRollWinner();
		#endif
	}
	
	void DetermineRollWinner ()
	{
		#if UNITY_IPHONE
		if (currentRoll > allyRoll)
		{
			KJTime.Add(SendRandomSeed, 1.5f);
			Debug.Log("Is Rolling: true");
		} else {
			Debug.Log("Is Rolling: false");	
		}
		
		SendShipInfo();
		#endif
	}
	
	public void SendRandomSeed ()
	{
		#if UNITY_IPHONE
		//if (!GameKitManager.isConnected()) return;
		
		currentRandomSeed = UnityEngine.Random.Range(int.MinValue, int.MaxValue);
		currentRandomSeed = currentRandomSeed;
		string sendData = currentRandomSeed.ToString();	
		//GameKitBinding.sendData("NetworkController", "RecRandomSeed", sendData, true);
		#endif
	}
	
	public void RecRandomSeed (string param)
	{
		#if UNITY_IPHONE
		currentRandomSeed = int.Parse(param);	
		#endif
	}
	
	public void SendShipInfo ()
	{
		#if UNITY_IPHONE
		//if (!GameKitManager.isConnected()) return;
		
		string sendData = ProfileController.Singleton.shipId.ToString();	
		//GameKitBinding.sendData("NetworkController", "RecShipInfo", sendData, true);
		#endif
	}
	
	public void RecShipInfo (string param)
	{
		#if UNITY_IPHONE
		allyItemId = int.Parse(param);
		isMultiplayer = true;
		MenuController.LoadToScene(1);
		#endif
		
	}
	
	public void SendDie ()
	{
		#if UNITY_IPHONE
		//if (!GameKitManager.isConnected()) return;
		//GameKitBinding.sendData("NetworkController", "RecDie", string.Empty, true);
		#endif
	}
	
	public void RecDie (string param)
	{
		#if UNITY_IPHONE
		GameController.Singleton.AllyDie();
		#endif
	}
	
	public void SendPosition(float x, float y)
	{
		#if UNITY_IPHONE
		//if ( GameKitManager.isConnected() )
		{
			string touchData = (int) x + "," + (int) y;
			//GameKitBinding.sendData("NetworkController", "RecPosition", touchData, false);
		}
		#endif
	}
	
	
	
	public void RecPosition (string param)
	{
		#if UNITY_IPHONE
		if (!isInGame) return;
		string[] parts = param.Split( ',' );
		if( parts.Length == 2 )
		{
			float xPos = float.Parse( parts[0] );
			float yPos = float.Parse( parts[1] );
			GameController.Singleton.SetAllyTarget(xPos, yPos);
		}
		#endif
	}
	
	public void SendReactor (int reactorId)
	{
		#if UNITY_IPHONE
		//if ( GameKitManager.isConnected() )
		{
			string reactorData = reactorId.ToString();
			//GameKitBinding.sendData("NetworkController", "RecReactor", reactorData, false);
		}
		#endif
	}
	
	public void RecReactor (string param)
	{
		#if UNITY_IPHONE
		if (!isInGame) return;
		
		int reactorId = int.Parse( param );
		GameController.Singleton.FireAllyReactor(reactorId);
		
		#endif
	}
	
	public void Disconnect ()
	{
		#if UNITY_IPHONE
		hasRolled = false;
		hasGottenRoll = false;
		KJTime.Remove(SendRandomSeed);
		//GameKitBinding.invalidateSession();
		#endif
	}

}
