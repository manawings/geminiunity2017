using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class AppStartController : MonoBehaviour
{
	
	private static bool hasBeenInit = false;
	public tk2dSpriteCollectionData particleAtlas;

	
	void Awake ()
	{
		//PlayerPrefs.DeleteAll();
		Application.targetFrameRate = 60;

		if (hasBeenInit) return;
		hasBeenInit = true;
		CentralController.Singleton.CheckAndStart();
		KJParticleController.Initialize(particleAtlas);

		BGGraphics.Initialize();
		SectorPattern.Initialize();
		BossPattern.Initialize();
		StoryCharacter.Initialize();
		StoryPair.Initialize();
		TutorialData.Initialize();
		WeaponPattern.Initialize();
		ItemPattern.Initialize();
		SquadManeuver.Initialize();
		EnemyPattern.Initialize();

		PlatformPattern.Initialize();
		Achievement.Initialize();

		AchievementManager.Initialize();

		FlurryController.Singleton.Initialize();

		IAPManager.DisableListeners();
		//AdManager.Initialize();
		//AdManagerUnity.Initialize();

//		RijndaelEncrypted r = new RijndaelEncrypted();
//		r.ByteLog("X");

//		Debug.Log("Init AppStart");

		// Initialize the Saved Control Schemes
		ProfileController.controlMode = PlayerPrefs.GetInt(DataController.tControlMode, 0);
		ProfileController.controlSensitivity = PlayerPrefs.GetFloat(DataController.tSensitivity, 1.0f);
	}

	void Start ()
	{
//
//		PlayerPrefs.DeleteKey("isOffline");
//		PlayerPrefs.DeleteKey("isGuest");

//		RijndaelEncrypted r = new RijndaelEncrypted();
//		r.ByteLog ("ec2-54-81-177-190.compute-1.amazonaws.com");
		KJCanary.Singleton.SwitchToAmbience();
	}

	void CheckAndLoad()
	{
//		PlayerPrefs.DeleteKey("isGuest"); PlayerPrefs.DeleteKey("isOffline");
		#if UNITY_IPHONE
		if (UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch4Gen) {

			UIController.ShowSingleClose("INCOMPATIBLE", "Sorry! This game is not compatible with this device due to performance issues.");
			return;

		}
		#endif

		//Only offline mode
		ProfileController.Singleton.isOffline = true;
		SocialController.isLoggedIn = false;
		PlayerPrefs.SetInt("isOffline", 1);
		PlayerPrefs.SetInt("isGuest", 0);
		bool isLoad = DataController.LoadAllData();
		if (!isLoad) { 
			GoToEnterNameScene();
		} else {
			if (ProfileController.IsLoadData) {
				MenuController.LoadToScene(2);
			} else {
				#if UNITY_IPHONE
			//	MenuController.LoadToScene(15);
				MenuController.LoadToScene(2);
				#else
				MenuController.LoadToScene(2);
				#endif
			}

		}

//		DataController.LoadAllData();
//		MenuController.LoadToScene(2);
		//end offline mode

		//CheckOnlineMode(); use for old version online facebook and guest


	}

	void CheckOnlineMode () {

		if(!PlayerPrefs.HasKey("isGuest") && !PlayerPrefs.HasKey("isOffline")){

			PlayerPrefs.SetInt("isOffline", 0);
			PlayerPrefs.SetInt("isGuest", 0);

			// Go to Login Screen
			if (ProfileController.ForcedOfflineMode) {

				ProfileController.Singleton.isOffline = true;
				SocialController.isLoggedIn = false;
				MenuController.LoadToScene(12);

			} else {

				MenuController.LoadToScene(15);

			}


		} else if (!ProfileController.ForcedOfflineMode && (PlayerPrefs.GetInt("isOffline") == 0 && PlayerPrefs.GetInt("isGuest") == 0)){

			Debug.Log("Online FB");
			FlurryController.LoginEvent("Facebook");
			UIController.ShowWait();

			CheckLogin();


		} else if (!ProfileController.ForcedOfflineMode && (PlayerPrefs.GetInt("isOffline") == 0 && PlayerPrefs.GetInt("isGuest") == 1)){

			FlurryController.LoginEvent("Guest");
			UIController.ShowWait();
			SocialController.LoadSocialGuestID();
			ProfileController.SocialId = SocialController.id;
			ProfileController.Singleton.isGuest = 1;
			PlayerPrefs.SetInt("isGuest", 1);

			// If the ID's are different, do the first check.
			if (SocialController.oldID == SocialController.id) {
				// IDs are the same - do as normal
				DB.Singleton.CallDatabase("CheckAccount", OnCheckAccountSuccess, OnCheckAccountFail);
			} else {
				// IDs are the different - check the new one first.
				DB.Singleton.CallDatabase("CheckAccount", OnCheckAccountSuccess, OnFirstCheckAccountFail);
			}

		} else {

			//			Debug.Log("Offline");
			ProfileController.Singleton.isOffline = true;
			SocialController.isLoggedIn = false;
			DataController.LoadAllData();
			MenuController.LoadToScene(2);
		}
	}

	ConnectFacebook connectFB = new ConnectFacebook();
	void CheckLogin ()
	{

//		Debug.Log ("AppStart: CheckLogin");

		if (FB.IsLoggedIn) {
			
			// Already Logged In. Skip FB and connect to the DB.
			// Facebook Login Successful, now login to the main AWS DB.
//			Debug.Log ("AppStart: FB.IsLoggedIn TRUE");

			connectFB.Connect(OnFacebookLoginSuccess, OnFacebookLoginFail);

		} else {

//			Debug.Log ("AppStart: FB.IsLoggedIn FALSE");

			// Go to Splash.
			MenuController.LoadToScene(2);
			UIController.ClosePopUp();
			
		} 
		
	}

	void OnFacebookLoginSuccess () {
		SocialController.id = AccessToken.CurrentAccessToken.UserId;
		ProfileController.Singleton.isGuest = 0;
		PlayerPrefs.SetInt("isGuest", 0);
		PlayerPrefs.SetInt("isOffline", 0);
		DB.Singleton.CallDatabase("CheckAccount", OnCheckAccountSuccess, OnCheckAccountFail);
	}

	void OnFacebookLoginFail () {
		MenuController.LoadToScene(2);
		UIController.ClosePopUp();
	}


	void OnCheckAccountSuccess ()
	{
//		Debug.Log ("OnCheckAccountSuccess");
		SocialController.BeginLogin(SocialController.id, OnDBLoginSuccess, OnDBLoginFail);
	}

	void OnFirstCheckAccountFail ()
	{
		// The Vendor ID Account Failed. Switch to Guest Account and Check.
//		Debug.Log ("OnFirstCheckAccountFail");
		UIController.RefreshWaitDuration();
		SocialController.id = SocialController.oldID; // Set to the old ID.
		DB.Singleton.CallDatabase("CheckAccount", OnSecondCheckAccountSuccess, OnSecondCheckAccountFail);
	}

	void OnSecondCheckAccountSuccess ()
	{
//		Debug.Log ("OnSecondCheckAccountSuccess");
		// The Vendor ID Account Failed. Switch to Guest Account and Check.
		SocialController.BeginLogin(SocialController.id, OnSecondDBLoginSuccess, OnDBLoginFail);
	}

	void OnSecondDBLoginSuccess ()
	{
//		Debug.Log ("OnSecondDBLoginSuccess");
		// Load the OLD Friend List
		DB.Singleton.CallDatabase("RequestFriendList", SecondCalibrateForUser, null);
	}

	void SecondCalibrateForUser ()
	{
//		Debug.Log ("SecondCalibrateForUser");
		// Logged into the old account. Load everything as normal, but change ID to the new account right away.
		SocialController.MergeFriendLists();
		UIController.ClosePopUp();
		DataController.LoadSocialData();
		MenuController.LoadToScene(2);

		SocialController.id = SocialController.vendorId;
		
	}

	void OnSecondCheckAccountFail ()
	{
//		Debug.Log ("OnSecondCheckAccountFail");
		// No OLD account either - this user is new, so login using the vendor ID right away.
		SocialController.id = SocialController.vendorId;
		GoToEnterNameScene();
	}
	
	void OnCheckAccountFail ()
	{
//		Debug.Log ("OnCheckAccountFail");
		GoToEnterNameScene();
	}



	
	void GoToEnterNameScene ()
	{
		MenuController.LoadToScene(12);
	}

	void OnDBLoginFail ()
	{
		// Database Login Failed
		SocialController.MergeFriendLists();
		UIController.ClosePopUp();
		
	}
	
	void CalibrateForUser ()
	{
		// User has successfully logged in.
//		Debug.Log ("CalibrateForUser - Ready to Go!");
		SocialController.MergeFriendLists();
		UIController.ClosePopUp();
		DataController.LoadSocialData();
		MenuController.LoadToScene(2);
		
	}
	
	void OnDBLoginSuccess ()
	{
		// First step of loggin in successful.
		//if (ProfileController.Singleton.isGuest != 1)
		DB.Singleton.CallDatabase("RequestFriendList", CalibrateForUser, null);
	}

	void Update()
	{
		ProfileController.Singleton.currentIntervalTimer += Time.deltaTime;
		//ProfileController.Singleton.currentOfferTimer += Time.deltaTime;
	}
}

