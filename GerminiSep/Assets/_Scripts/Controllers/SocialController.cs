using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using System;

public class SocialController : MonoBehaviour {


	public enum AccessCode {

		RequestLogin = 0,

		RequestBigData = 1,
		RequestSmallData = 2,

		SaveBigData = 3,
		SaveSmallData = 4,

	}

	public static bool shouldLoadInfo = false;

	public static DB db
	{
		get {
			return Singleton._db;
		}
	}

	public static WebService webService
	{
		get {
			return Singleton._webService;
		}
	}

	public static ListType FriendListType = ListType.Friends;

	public enum ListType
	{
		Facebook,
		Friends,
		Inbox,
		Sent
	}

	public DB _db;
	public WebService _webService;

	public static FriendItem friendItem;
	public static List<FriendItem> masterFriendList;
	public static List<FriendItem> allFacebookFriendList;
	public static List<FriendItem> allAmazonFriendList;
	public static List<FriendItem> friendListRequest;
	public static List<FriendItem> friendListConfirmed;
	public static List<FriendItem> friendListSent;
	//	public static List<FriendItem> facebookFriendList;
	//	public static List<FriendItem> facebookFriendList;

	public static bool friendListCalibrated = false;

	//web server timer

	public const string tServerTime = "tServerTime";
	public const string tLastTimeCheck = "tLastTimeCheck";

	public static long serverTime {
		get {
			long _serverTime = long.Parse(PlayerPrefs.GetString(tServerTime, "0"));
			return _serverTime;
		}

		set {
			PlayerPrefs.SetString(tServerTime, value.ToString());
			PlayerPrefs.Save();
		}
	}

	public static long lastTimeCheck {
		get {
			long _lastTimeCheck = long.Parse(PlayerPrefs.GetString(tLastTimeCheck, "0"));
			return _lastTimeCheck;
		}

		set {
			PlayerPrefs.SetString(tLastTimeCheck, value.ToString());
			PlayerPrefs.Save();
		}
	}


	public const int refreshTime = 3;// How long to wait before refreshing again.

	ObscuredBool _isLoggedIn = false;
	public static ObscuredBool isLoggedIn { get { return Singleton._isLoggedIn; } set { Singleton._isLoggedIn = value; } }

	//int _id = 0;
	//public static int id { get { return Singleton._id; } set { Singleton._id = value; } }

	ObscuredString _id = "0";
	public static ObscuredString id { get { return Singleton._id; } set { Singleton._id = value; } }

	ObscuredString _vendorId = "0";
	public static ObscuredString vendorId { get { return Singleton._vendorId; } set { Singleton._vendorId = value; } }

	ObscuredString _oldID = "0";
	public static ObscuredString oldID { get { return Singleton._oldID; } set { Singleton._oldID = value; } }

	string _friendId = "0";
	public static string friendId { get { return Singleton._friendId; } set { Singleton._friendId = value; } }

	ObscuredString _sessionID = "0";
	public static ObscuredString sessionID { get { return Singleton._sessionID; } set { Singleton._sessionID = value; } }

//	WWWForm _wwwForm = new WWWForm();
//	public static WWWForm wwwForm { get { return Singleton._wwwForm; } set { Singleton._wwwForm = value; } }

	public delegate void SocialDelegate ();
	SocialDelegate loginSuccessDelegate;
	SocialDelegate loginFailDelegate;

	/** Reference to the GameObject. */

	private static GameObject parentObject;

	private static SocialController _Singleton;
	public static SocialController Singleton {
		get {
			if (!_Singleton)
			{
				parentObject = new GameObject("Social Controller");
				DontDestroyOnLoad(parentObject);
				_Singleton = parentObject.AddComponent<SocialController>();
				_Singleton.Initialize();
			}
			return _Singleton;
		}
	}

	void Initialize ()
	{
		_db = gameObject.AddComponent<DB> ();
		_webService = gameObject.AddComponent<WebService> ();
	}

	public static bool friendRequestStatus = false;

	//public static void BeginLogin (int myId, int friendIdInput, SocialDelegate onLoginSucess, SocialDelegate onLoginFail)
	public static void BeginLogin (string myId, SocialDelegate onLoginSucess, SocialDelegate onLoginFail)
	{
		//		Debug.Log ("Begin DB Login");

		isLoggedIn = false;
		id = myId;
		//friendId = friendIdInput;
		Singleton.loginSuccessDelegate = onLoginSucess;
		Singleton.loginFailDelegate = onLoginFail;

		RequestLogin();
		// OnLoginSuccess();
	}

	public static void LoadSocialGuestID ()
	{

		id = SystemInfo.deviceUniqueIdentifier.ToString();
		oldID = SystemInfo.deviceUniqueIdentifier.ToString();
		#if UNITY_IPHONE
		vendorId = UnityEngine.iOS.Device.vendorIdentifier;
		#endif
		#if UNITY_ANDROID
		vendorId = SystemInfo.deviceUniqueIdentifier.ToString();
		#endif
		if (oldID != vendorId) {
			if (!string.IsNullOrEmpty(vendorId)) {
				Debug.Log ("ID MISMATCH");
				id = vendorId;
			}
		}

		// Otherwise, use the ID we've saved with Player Prefs as a priority.
		if (!string.IsNullOrEmpty(DataController.GuestKey) && DataController.GuestKey.Length > 5) {
			SocialController.id = DataController.GuestKey;
		} else {
			DataController.GuestKey = SocialController.id;
		}

		//		Debug.Log ("oldId: " + oldID);
		//		Debug.Log ("newVendorId: " + vendorId);

		//		iPhone.vendorIdentifier;

		SocialController.id = id;
	}

	public static void RequestLogin ()
	{
		//		Debug.Log ("Call DB for Login");
		db.CallDatabase ((AccessCode.RequestLogin).ToString(), OnLoginSuccess);
	}

	static void OnLoginSuccess ()
	{
		//		Debug.Log ("DB Login Success");
		//		Debug.Log ("Login With: " + SocialController.id);
		isLoggedIn = true;
		if (Singleton.loginSuccessDelegate != null) Singleton.loginSuccessDelegate();
	}

	public static void OnLoginFail ()
	{
		isLoggedIn = false;
		Singleton.loginFailDelegate();
	}

	public static void LogOut ()
	{
		isLoggedIn = false;
	}

	// Database Interaction Functions

	public static void SavePostGame ()
	{
		//SendData(AccessCode.RequestBigData, id);
		if (isLoggedIn) {
			//			Debug.Log ("Saving Post Game Data...");
			db.CallDatabase ("SavePostGame", UIController.ClosePopUp);
		}
	}

	public static void RequestLoadBigData ()
	{
		//SendData(AccessCode.RequestBigData, id);
		//		Debug.Log ("Request Load Big Data");
		db.CallDatabase ((AccessCode.RequestBigData).ToString());
	}

	public static void RequestSaveBigData (KJTime.TimeDelegate onSaveComplete = null, KJTime.TimeDelegate onSaveFail = null)
	{
		db.CallDatabase ((AccessCode.SaveBigData).ToString(), onSaveComplete, onSaveFail);
		//	SendData(AccessCode.SendBigData, id, DataController.EncodedBigData);
	}

	public static void BeginChallenge ()
	{
		db.CallDatabase ((AccessCode.RequestSmallData).ToString());
	}

	public static void CheckServerTime (KJTime.TimeDelegate OnCheckTimeComplete = null)
	{
		webService.ConnectWebService("Check_Server_Time", OnCheckTimeComplete);
	}

	public static void CheckTime () {
		if (NextTimeCheck(refreshTime) > lastTimeCheck) {
			CheckServerTime(OnCheckTimeComplete);
		}
	}

	static void OnCheckTimeComplete () {

		lastTimeCheck = EpochTime;

		// The ServerTime and LastTimeCheck Property already saves the Info.

		//		Debug.Log ("Time Check Complete: " + serverTime);

		//		PlayerPrefs.SetString(tServerTime, serverTime.ToString());
		//		PlayerPrefs.SetString(tLastTimeCheck, lastTimeCheck.ToString());
		//
		//		PlayerPrefs.Save();

	}

	public static long NextTimeCheck (int minutes){
		TimeSpan t = DateTime.UtcNow.AddMinutes(-minutes) - new DateTime(1970, 1, 1);
		long secondsSinceEpoch = (long)t.TotalMilliseconds;
		return secondsSinceEpoch;
	}

	public static long EpochTime {
		get {
			TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
			long secondsSinceEpoch = (long)t.TotalMilliseconds;
			return secondsSinceEpoch;
		}
	}

//	public static void SendData (AccessCode accessCode, int id, List<SocialData> socialDataList = null)
//	{
//		wwwForm = new WWWForm();
//
//		foreach (SocialData socialData in socialDataList)
//		{
//
//			switch(socialData.dataType)
//			{
//
//			case SocialData.DataType.Int:
//				wwwForm.AddField(socialData.key, socialData.intValue.ToString());
//				break;
//
//			case SocialData.DataType.Float:
//				wwwForm.AddField(socialData.key, socialData.floatValue.ToString());
//				break;
//
//			case SocialData.DataType.String:
//				wwwForm.AddField(socialData.key, socialData.stringValue);
//				break;
//
//			}
//
//		}
//
//		//		Debug.Log ("Form: " + wwwForm);
//	}

	public static void OnReceiveData ()
	{

	}

	public static void MergeFriendLists ()
	{
		masterFriendList = new List<FriendItem>();
		friendListRequest = new List<FriendItem>();
		friendListConfirmed = new List<FriendItem>();
		friendListSent = new List<FriendItem>();

		// Calibrate the Main Friend List.
		foreach (FriendItem friendItem in allAmazonFriendList)
		{
			masterFriendList.Add(friendItem);
		}

		// Calibrate the FB Friend List.
		if (allFacebookFriendList != null && allFacebookFriendList.Count > 0) {
			for (int i = allFacebookFriendList.Count - 1 ; i > -1 ; i --)
			{
				FriendItem facebookFriend = allFacebookFriendList[i];

				foreach (FriendItem masterFriend in masterFriendList)
				{
					if (masterFriend.id == facebookFriend.id) {
						masterFriend.realName = facebookFriend.realName;
						masterFriend.profilePicURL = facebookFriend.profilePicURL;
						masterFriend.fbTexture = facebookFriend.fbTexture;
						allFacebookFriendList.RemoveAt(i);
						allFacebookFriendList.Insert(i, masterFriend);
						goto endOfLoop;
					}
				}

				// Friend was not present in master list - add to master list.
				masterFriendList.Add(facebookFriend);
				endOfLoop: {}
			}
		}

		// Calibrate the Request Friend List.
		// Calibrate the Confirmed Friend List.
		foreach (FriendItem friendItem in masterFriendList)
		{
			if (friendItem.status == FriendItem.FriendStatus.Pending) {
				friendListRequest.Add(friendItem);
			}

			if (friendItem.status == FriendItem.FriendStatus.Friend) {
				friendListConfirmed.Add(friendItem);
			}
		}

		// Calibrate the Sent Friend List.
		foreach (FriendItem friendItem in masterFriendList)
		{
			if (friendItem.status == FriendItem.FriendStatus.Requested) {
				friendListSent.Add(friendItem);
			}
		}

		//		Debug.Log("Friend List Calibration Complete: " + masterFriendList.Count);
		friendListCalibrated = true;
	}

}
