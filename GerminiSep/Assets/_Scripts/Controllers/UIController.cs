using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIController : KJBehavior {
	
	public UILabel hcLabel;
	public UILabel gemLabel;
	public UILabel shipPartsLabel;
	public UISprite topPanel;
	public UISprite XCIcon;

	public CombatHeader combatHeader;
	public UIPanel uiPanel;
	
	public ReactorButton reactorButton;
	float xOut = - 60.0f, xIn = 0.0f;
	
	Hashtable reactorTweenIn;
	Hashtable reactorTweenOut;
	
	public static PopupPanel popupPanel;

	// Life Bars
	public KJBarGlow enemyBar;
	public UILabel enemyLifeLabel;
	public GameObject enemyBarHolder;
	float enemyBarAnchorTarget = 0.0f, enemyBarAnchorCurrent = 0.0f;
	const float enemyBarIn = 0.0f, enemyBarOut = 40.0f;

	public static KJTime.TimeDelegate failDelegate = null;
	public static KJTime.TimeDelegate addFailDelegate = null;
	public static KJTime.TimeDelegate tryAgainDelegate = null;
	public static bool forceTryAgain = false;

	public const string DefaultErrorHeader = "Connection Error";
	public const string DefaultErrorBody = "Connection Error Body";

	public static Color HCColor, SCColor;
	
	public static UIPanel MyUIPanel {
		get {
			return Singleton.uiPanel;	
		}
	}
	
	public static UIController Singleton
	{
		get; private set;
	}
	
	void Start () {

		HCColor = new Color32(255, 232, 171, 255);
		SCColor = new Color32(133, 245, 255, 255);
		
		Singleton = this;
		
		// Hastable For Reactor Button Tweens

		reactorTweenIn = new Hashtable();
		reactorTweenIn.Add("name", "reactorTween");
        reactorTweenIn.Add("from", xOut);
        reactorTweenIn.Add("to", xIn);
        reactorTweenIn.Add("time", 0.35f);
        reactorTweenIn.Add("onupdate", "OnReactorButtonUpdate");
        reactorTweenIn.Add("oncomplete", "OnReactorTweenComplete");
		reactorTweenIn.Add("easetype", iTween.EaseType.easeOutCirc);
		
		reactorTweenOut = new Hashtable();
		reactorTweenOut.Add("name", "reactorTween");
        reactorTweenOut.Add("from", xIn);
        reactorTweenOut.Add("to", xOut);
        reactorTweenOut.Add("time", 0.35f);
        reactorTweenOut.Add("onupdate", "OnReactorButtonUpdate");
		reactorTweenOut.Add("easetype", iTween.EaseType.easeOutCirc);

		
		enemyBarAnchorCurrent = enemyBarAnchorTarget = enemyBarOut;
		SetEnemyBarPosition();

		if (gemLabel != null && hcLabel != null) {
			UpdateCurrency();
		}
		
	}
	
	public static void UpdateCurrency ()
	{
		if( Singleton.hcLabel != null) {
			Singleton.hcLabel.text =  ProfileController.Credits.ToString();
			Singleton.gemLabel.text = ProfileController.Gems.ToString();
		}

		if (ProfileController.ShipParts == 0) {

			if (Singleton.XCIcon != null)
				Singleton.XCIcon.gameObject.SetActive(false);

			if (Singleton.topPanel != null) 
				Singleton.topPanel.spriteName = "TopBar_v3";

			if (Singleton.shipPartsLabel != null) {
				Singleton.shipPartsLabel.text = string.Empty;
			}
		} else {

			if (Singleton.XCIcon != null)
				Singleton.XCIcon.gameObject.SetActive(true);

			if (Singleton.topPanel != null) 
				Singleton.topPanel.spriteName = "TopBar2_v3";

			if (Singleton.shipPartsLabel != null) {

				Singleton.shipPartsLabel.text = ProfileController.ShipParts.ToString() + " CORE";
				if (ProfileController.ShipParts > 1)
					Singleton.shipPartsLabel.text += "S";

			}
		}


	}
	
	public static void ShowReactorButton (Collectable.Type type, bool slideIn = true)
	{
		if (Singleton.reactorButton.transform.localPosition.x != Singleton.xIn) {
			iTween.StopByName(Singleton.gameObject, "reactorTween");
			iTween.ValueTo(Singleton.gameObject, Singleton.reactorTweenIn);
		} else {
			Singleton.reactorButton.isReady = true;
		}
		
		Singleton.reactorButton.CollectReactorGraphic(type);
	}
	
	public static void HideReactorButton (bool instant = false)
	{
		if (instant)
		{
			KJMath.SetX(Singleton.reactorButton, Singleton.xOut);
		} else {
			iTween.StopByName(Singleton.gameObject, "reactorTween");
			iTween.ValueTo(Singleton.gameObject, Singleton.reactorTweenOut);
		}
		
	}
	
	void OnReactorButtonUpdate (float val)
	{
		KJMath.SetX(reactorButton, val);
	}

	void OnReactorTweenComplete ()
	{
		reactorButton.isReady = true;
	}



	private static void OnFail ()
	{


		if (addFailDelegate != null) addFailDelegate();

		if (failDelegate == null) {
			DefaultWaitFail();
		} else {
			failDelegate();
		}

		ClearTimeOutDelegates();
	}


	public static void DefaultWaitFail ()
	{
		UIController.ShowError("TIMEOUT ERROR", "Failed to connect to the server. The server may be down. Please check your internet connection and try again!");
	}

	public static void RefreshWaitDuration (float timeout = 15.0f)
	{
		KJTime.Add(KJTime.SpaceType.System, OnFail, timeout, 1);
	}

	public static void ShowWait (float timeout = 15.0f, KJTime.TimeDelegate _failDelegate = null, KJTime.TimeDelegate _tryAgainDelegate = null, KJTime.TimeDelegate _addFailDelegate = null, bool _forceTryAgain = false)
	{
		Debug.Log("ShowWait");
		failDelegate = _failDelegate;
		addFailDelegate = _addFailDelegate;
		tryAgainDelegate = _tryAgainDelegate;
		forceTryAgain = _forceTryAgain;
		if (timeout > 0) KJTime.Add(KJTime.SpaceType.System, OnFail, timeout, 1);
		popupPanel.Deploy(PopupPanel.Type.Waiting);
	}


	public static void ClosePopUp ()
	{
		ClearTimeOutDelegates();
		popupPanel.ClosePanel();
	}

	public static void FadeIn () {
		popupPanel.FadeIn();
	}

	public static void FadeOut () {
		popupPanel.FadeOut();
	}

	public static void ShowError (string header, string body)
	{
		KJCanary.PlaySoundEffect("NegativeButton");
		popupPanel.DeployError(header, body);
	}

	public static void ShowComplexError (string header, string body, KJTime.TimeDelegate forceTryDelegate = null, bool _forceTryAgain = false)
	{
		tryAgainDelegate = forceTryDelegate;
		forceTryAgain = _forceTryAgain;
		
		ShowError(header, body);
	}

	public static void ShowComplexError (KJTime.TimeDelegate forceTryDelegate = null, bool _forceTryAgain = false)
	{
		tryAgainDelegate = forceTryDelegate;
		forceTryAgain = _forceTryAgain;
		
		ShowComplexError(DefaultErrorHeader, DefaultErrorBody, forceTryDelegate, _forceTryAgain);
	}
	
	public static void ShowPopup (string header = "POP UP", string body = "Body Text Here!")
	{
		popupPanel.Deploy(PopupPanel.Type.Empty, header, body);
	}

	public static void ShowPause ()
	{
		popupPanel.Deploy(PopupPanel.Type.Pause);
	}

	public static void ShowSingleClose (string header = "POP UP", string body = "Body Text Here!")
	{
		popupPanel.Deploy(PopupPanel.Type.SingleClose, header, body);
	}

	public static void ShowPopUpSingleYes (string header = "POP UP", string body = "Body Text Here!", string yesText = "YES", KJTime.TimeDelegate OnYesDelegate = null)
	{
		popupPanel.OnYesDelegate = OnYesDelegate;
		popupPanel.Deploy(PopupPanel.Type.SingleYes, header, body, yesText);
	}

	public static void ShowPopupYesOrNo (string header = "POP UP", string body = "Body Text Here!", KJTime.TimeDelegate OnYesDelegate = null)
	{
		popupPanel.OnYesDelegate = OnYesDelegate;
		popupPanel.Deploy(PopupPanel.Type.YesOrNo, header, body);
	}

	public static void ShowPopupYesOrNo (string header = "POP UP", string body = "Body Text Here!", string yesText = "YES", string noText = "NO", KJTime.TimeDelegate OnYesDelegate = null)
	{
		popupPanel.OnYesDelegate = OnYesDelegate;
		popupPanel.Deploy(PopupPanel.Type.YesOrNo, header, body, yesText, noText);
	}

	public static void ShowPopupNoGems ()
	{
		KJCanary.PlaySoundEffect("NegativeButton");
		popupPanel.Deploy(PopupPanel.Type.NotEnoughGems);
	}

	public static void ShowPopupNoCredits ()
	{
		KJCanary.PlaySoundEffect("NegativeButton");
		popupPanel.Deploy(PopupPanel.Type.NotEnoughCredits);
	}

	public static void ShowPopupNoCores ()
	{
		KJCanary.PlaySoundEffect("NegativeButton");
		popupPanel.Deploy(PopupPanel.Type.NotEnoughCores);
	}

	public static void ShowPopupIAP ()
	{
		popupPanel.Deploy(PopupPanel.Type.IAP);
	}




	public static void ShowPopupHeart ()
	{
		KJCanary.PlaySoundEffect("NegativeButton");
		popupPanel.Deploy(PopupPanel.Type.NoHearts);
	}

	public static void ShowPopupBuyHeart ()
	{
		popupPanel.Deploy(PopupPanel.Type.BuyHearts);
	}

	public static void ShowPopupRevive ()
	{
		popupPanel.Deploy(PopupPanel.Type.Revive);
	}

	
	public static void ShowPopupSell (ItemBasic item)
	{
		popupPanel.SetItem(item);
		popupPanel.Deploy(PopupPanel.Type.Sell);
	}

	public static void ShowPopupPreview (ItemBasic item)
	{
		popupPanel.SetItem(item);
		popupPanel.Deploy(PopupPanel.Type.Preview);
	}
	
	public static void ShowPopupEquip (ItemBasic item)
	{
		popupPanel.SetItem(item);
		popupPanel.Deploy(PopupPanel.Type.Equip);
	}
	
	public static void ShowPopupUpgrade (ItemBasic item)
	{
		popupPanel.SetItem(item);
		popupPanel.Deploy(PopupPanel.Type.Upgrade);
	}
	
	public static void ShowPopupUnlockShip (ItemBasic item)
	{
		popupPanel.SetItem(item);
		popupPanel.Deploy(PopupPanel.Type.UnlockShip);
	}

	public static void ShowPopupViewAd ()
	{
		popupPanel.Deploy(PopupPanel.Type.ViewAd);
	}

	public static void ShowPopupViewAdSC ()
	{
		popupPanel.Deploy(PopupPanel.Type.ViewAdSC);
	}

	public static void ShowStory ( int storyId ) {
		ShowStory(StoryPair.At(storyId));
	}

	public static void ShowStory ( Story story ) {
		popupPanel.DeployStory(story);
	}

	public static void UpdateBossBar (float bossLifePercent, string bossLife)
	{
		Singleton.enemyBar.SetProgress(bossLifePercent);
		Singleton.enemyLifeLabel.text = bossLife + " HP";
	}
	
	public static void ShowEnemyBar ()
	{
		Singleton.enemyBarAnchorTarget = enemyBarIn;
		KJTime.Add(Singleton.RunEnemyBar);
	}
	
	public static void HideEnemyBar ()
	{
		Singleton.enemyBarAnchorTarget = enemyBarOut;
		KJTime.Add(Singleton.RunEnemyBar);
	}

	// Pop Up and Timeout System

	public static List<MonoBehaviour> coroutineList;
	
	public static void AddCoroutineToTimeout (MonoBehaviour monoBehavior)
	{
		if (coroutineList == null ) coroutineList = new List<MonoBehaviour>();
		coroutineList.Add(monoBehavior);
	}
	
	public static void ClearCoroutines ()
	{
		if (coroutineList == null ) return;

		coroutineList.ForEach( p => p.StopAllCoroutines());
		coroutineList.Clear();
	}
	
	public static void ClearTimeOutDelegates ()
	{
//		addFailDelegate = null;
//		tryAgainDelegate = null;

//		Debug.Log ("Clear Time Out Delegates");

		ClearCoroutines();
//		IAPManager.DisableListeners();
		KJTime.Remove(OnFail);
	}

	// -------------------------


	
	void RunEnemyBar ()
	{
		
		float barPositionDifference = enemyBarAnchorTarget - enemyBarAnchorCurrent;
		
		if (Mathf.Abs(barPositionDifference) < 0.1f) {
			enemyBarAnchorCurrent = enemyBarAnchorTarget;
			KJTime.Remove(RunEnemyBar);
		} else {
			enemyBarAnchorCurrent += barPositionDifference * 0.15f;
		}
		
		SetEnemyBarPosition();
	}
	
	void SetEnemyBarPosition ()
	{
		if (enemyBarHolder != null) {
			Vector3 localPosition = enemyBarHolder.transform.localPosition;
			enemyBarHolder.transform.localPosition = new Vector3(localPosition.x, enemyBarAnchorCurrent, localPosition.z);
		}
	}
	
	public static bool ShowHeader (string labelText, string subText, KJTime.TimeDelegate headerDelegate = null, bool isBoss = false, CombatHeader.ColorType headerColor = CombatHeader.ColorType.Green)
	{

		if (isBoss)
			KJCanary.PlaySoundEffect("BossWarning");
		else
			KJCanary.PlaySoundEffect("CombatHeader");
			                      
		if (Singleton.combatHeader != null) {
			return Singleton.combatHeader.ShowHeader(labelText, subText, headerDelegate, headerColor);
		}

		return false;
	}
}
