﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RewardController {

	private static SquadFormation formation;
	private static int minorGroups = 2;
	private static int gemCountPerGroup = 7;
	private static int gemGap = 50;
		
	private static float spawnPoint = 0.0f;
	private static float spawnPointAddMajor;
	private static float spawnPointAddMinor;
		
	private static bool spawnAsCircle = true;
	// private static var collectable:Collectable;
	private static GameObject collectableObject;
	private static Collectable collectable;
	
	private static Type type;
	
	private enum Type
	{
		Medic,
		Weapon,
		Gem,
		Item
	}

	private static Collectable.Type collectableType;
	
	public static void SpawnRewards(float intensity = 0, bool miniWave = false, int amount = 2, float offsetValue = 0)
	{
			
		if (spawnPointAddMajor == 0.0f)
		{
			spawnPointAddMajor = - gemGap * 2.0f * 0.85f;
			spawnPointAddMinor = - gemGap * 0.85f;
				
		}
		
		int minorGroupLeft;
		int majorGroupLeft;
		float xOffset = 0;
			
		// Find the type of reward best suited for the player.
		if (miniWave) {
			spawnAsCircle = false;
		} else {
			DetermineRewardType();
		}
			
		if (spawnAsCircle)
		{
			majorGroupLeft = 1;
			spawnPoint = -700;
				
			// Settings based on intensity.
			minorGroups = 2 + (int) KJMath.Cap(intensity * WaveController.Level / 5.0f, 0.0f, 4.0f);
			gemCountPerGroup = 4 + (int) (3.0 * intensity + KJMath.Cap(WaveController.Level / 5.0f, 0.0f, 5.0f));
				
			// Spawn the center piece.
			collectableObject = KJActivePool.GetNew("Collectable");
			collectable = collectableObject.GetComponent<Collectable>();
			collectable.Deploy(collectableType, 0.0f, spawnPoint); 
			// collectable.FixMovement();
				
		} else {
				
			// Settings based on intensity.
				
			if (miniWave) 
			{
				majorGroupLeft = 1;
				minorGroups = amount; 
				gemCountPerGroup = amount;
				xOffset = Random.Range( -offsetValue, offsetValue);
				spawnPoint = 500;
					
			} else {
					
				majorGroupLeft = 1;// + (int) KJMath.Cap(intensity * WaveController.Level / 10.0f, 0.0f, 2.0f);
				minorGroups = 2;
				gemCountPerGroup = 1 + (int) (KJMath.Cap(WaveController.Level / 30.0f, 0.0f, 2.0f));
				spawnPoint = 500;
			}
				
		}

		while (majorGroupLeft > 0)
		{
				
			minorGroupLeft = minorGroups;
				
			while (minorGroupLeft > 0)
			{
				// Spawn the Minor Group
					
				if (spawnAsCircle) {
					formation = new SquadFormation(gemCountPerGroup, SquadFormation.Type.Entourage, gemGap * minorGroupLeft);
				} else {
					formation = new SquadFormation(gemCountPerGroup, SquadFormation.Type.Line, gemGap);
				}
					
				for (int i = 0 ; i < formation.slots.Count ; i ++ )
				{
					SquadSlot slot = formation.slots[i];
					
					collectableObject = KJActivePool.GetNew("Collectable");
					collectable = collectableObject.GetComponent<Collectable>();
					collectable.Deploy(Collectable.Type.Gem, slot.x + xOffset, slot.y + spawnPoint); 
					// collectable.FixMovement();
						
				}
					
				if (!spawnAsCircle) spawnPoint += spawnPointAddMinor;
				minorGroupLeft--;
			}
				
			if (!spawnAsCircle) spawnPoint += spawnPointAddMajor;
			majorGroupLeft--;
		}
	}

	public static int gainGemReward (int allEnemy, int allEnemyDie) {
		float gainGem;
		gainGem = ((float) allEnemyDie / (float) allEnemy) * Stats.BoxCostForLevel(ProfileController.SectorLevel) * 0.17f;
		if (gainGem < 0 || System.Single.IsNaN(gainGem)) { 
			gainGem = Stats.BoxCostForLevel(ProfileController.SectorLevel) * 0.5f;
		}
//		Debug.Log ("allEnemyDie  = " + allEnemyDie); 
//		Debug.Log ("allEnemy  = " + allEnemy); 
//		Debug.Log ("allEnemyDie / allEnemy = " + (float)allEnemyDie / allEnemy); 
//		Debug.Log ("Stats.NGVForLevel(ProfileController.SectorLevel = " + Stats.NGVForLevel(ProfileController.SectorLevel) * 0.5f); 
//		WaveController.allEnemy = 0;
//		WaveController.allEnemyDie = 0;
		if (ProfileController.GemDouble) gainGem *= 2;

		return (int) Mathf.Round(gainGem);
	}

	private static void DetermineRewardType ()
	{
		List<Type> rewardRoll = new List<Type>();
		float lifePercent = GameController.PlayerUnit.LifePercent;
		float energyPercent = GameController.PlayerUnit.EnergyPercent;
			
			/*
			if (GameController.canSpawnItem) {
				rewardRoll.push(ITEM);
				rewardRoll.push(ITEM);
			}
			*/	
		
		
		if (lifePercent > 0.5f)
		{
			// 85%+ Life. We don't spawn Med Kits.
			rewardRoll.Add(Type.Gem);
			rewardRoll.Add(Type.Weapon);

				
		} else {
				
			// Life is between 20% and 85%. Chance to spawn med kit.
			rewardRoll.Add(Type.Medic);
			rewardRoll.Add(Type.Weapon);
			rewardRoll.Add(Type.Gem);
					
		}
		
		type = KJMath.RandomMemberOf<Type>(rewardRoll);
		
		if (type == Type.Gem)
		{
			spawnAsCircle = false;
		} else {
			spawnAsCircle = true;
		}
			
		switch (type)
		{
			case Type.Medic:
				collectableType = Collectable.Type.Medic;
				break;
					
			case Type.Item:
				// GameController.canSpawnItem = false;
				collectableType = Collectable.Type.Yellow;
				break;
					
			case Type.Weapon:
				collectableType = Collectable.Type.Green;
				break;
		}
	}
}
