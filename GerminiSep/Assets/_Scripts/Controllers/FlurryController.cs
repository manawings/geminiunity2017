﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlurryController : MonoBehaviour {

	public static string key = "9QF6QXQXK2WPJDZBQY76";

	private static GameObject parentObject;
	private static FlurryController _Singleton;
	public static FlurryController Singleton {
		get {
			if (!_Singleton)
			{
				parentObject = new GameObject("FlurryController");
				DontDestroyOnLoad(parentObject);
				_Singleton = parentObject.AddComponent<FlurryController>();
				//_Singleton.Initialize();
			}
			return _Singleton;
		}
	}

	public void Initialize ()
	{
//		startSession();
//		#if UNITY_IPHONE
//		gameObject.AddComponent<FlurryEventListener> ();
//		#endif
//
//		#if UNITY_ANDROID
//		gameObject.AddComponent<FlurryEventListener> ();
//		#endif

	}

	public static void startSession() {
		FlurryManagerEvent.startSession(key);
	}

	public static void NormalCargoEncountered () {
//		Debug.Log("Normal Cargo Encountered");
		FlurryManagerEvent.logEvent("Normal Cargo Encountered");
	}

	public static void NormalCargoOpened () {
//		Debug.Log("Normal Cargo Opened");
		FlurryManagerEvent.logEvent("Normal Cargo Opened");
	}

	public static void CargoBlackmarket () {
		var dict = new Dictionary<string,string>();
		dict.Add("Type", "BlackMarket" );
//		Debug.Log("HC Cargo Opened Blackmarket");
		FlurryManagerEvent.logEventWithParameters("HC Cargo Opened", dict);
	}

	public static void CargoQuest () {
		var dict = new Dictionary<string,string>();
		dict.Add( "Type", "Quest" );
//		Debug.Log("HC Cargo Opened Quest");
		FlurryManagerEvent.logEventWithParameters("HC Cargo Opened", dict);
	}

	public static void WaveTypeTime(){
		var dict = new Dictionary<string,string>();
		dict.Add( "WaveType", WaveController.type.ToString());
		FlurryManagerEvent.logEventWithParameters("Wave Type Time", dict, true);
	}

	public static void EndWaveTypeTime() {
		var dict = new Dictionary<string,string>();
		dict.Add( "WaveType", WaveController.type.ToString());
		FlurryManagerEvent.endTimedEvent("Wave Type Time", dict);
	}

	public static void PlayerDefeated () {
		var dict = new Dictionary<string,string>();
		dict.Add( "WaveType", WaveController.type.ToString());
//		Debug.Log("Player Defeated " + WaveController.type.ToString());
		FlurryManagerEvent.logEventWithParameters("Player Defeated", dict);
	}

	public static void LockedOut () {
//		Debug.Log("A Player is locked-out");
		FlurryManagerEvent.logEvent("Player Lock Out");
	}

	public static void SectorRepeated () {
//		Debug.Log("A Sector is repeated");
		FlurryManagerEvent.logEvent("Sector Repeated");
	}

	public static void SectorCleared() {
//		Debug.Log("A Sector is cleared.");
		var dict = new Dictionary<string,string>();
		dict.Add( "Sector ID", ProfileController.SectorId.ToString());

		FlurryManagerEvent.logEventWithParameters("Sector Cleared", dict);
	}

	public static void LoginEvent (string s) {
//		Debug.Log("LoginEvent");
		var dict = new Dictionary<string,string>();
		dict.Add( "LoginType", s);
		FlurryManagerEvent.logEventWithParameters("Login Event", dict);
	}

	public static void ShipBuying (string shipName, string shipClass) {
//		Debug.Log("ShipBuying");
		var dict = new Dictionary<string,string>();
		dict.Add( "Name", shipName);
		dict.Add( "Class", shipClass);
		FlurryManagerEvent.logEventWithParameters("Ship Purhcased", dict);
	}

	public static void WaveType(string type){
		var dict = new Dictionary<string,string>();
		dict.Add( "WaveType", type);
		FlurryManagerEvent.logEventWithParameters("WaveType", dict);
	}
}
