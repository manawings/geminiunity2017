using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine.Advertisements;

public class ProfileController : MonoBehaviour
{
	
	// DEBUG FUNCTION 

	public const bool ForcedOfflineMode = false;

	// If we no longer wish to use the servers, activate this mode.

	public static string keyDaySession = "keyDaySession";

	public static int InboxCount {
		get {

			int inboxCount = 0;

			inboxCount += SocialController.friendListRequest.Count;

			int totalHelepd = ProfileController.HelpedFriends + ProfileController.HelpedStrangers;
			if (totalHelepd > 0) inboxCount ++;

			if (ProfileController.Singleton.hasGoldReward) inboxCount ++;

			return inboxCount;
		}
	}

	public static string VersionID {
		get {
			return "1.4.8";
		}
	}

    //Ads Unity
    public int minSectorAds = 3; //3
    public bool canShowAds = false;
    public int countPlayForAds = 0;
    public int minCountShowAds = 3; //3

	public bool adsReviveAvailable = true;
  //  public string adsUnityID = "1213372";
	public static string adsUnityID = "1216009";
    public float currentIntervalTimer = 0;
    public float intervalAdstime = 300; //300
    public int loadSectorWhenEndAds = 2;

    void SetDebugParams ()
	{
        //countPlayForAds = 3;
	    //currentIntervalTimer = 300;
	    //minSectorAds = 0;
        
	//	SetDebugLevel(20,1);
	//	DoAllTutorials();
	//	SetDebugItems(33, 33, 3, 33, 3, 3, 3, 3);

//		tutorialStatus[22] = false;
//		tutorialStatus[23] = false;
//		tutorialStatus[24] = false;
//		tutorialStatus[25] = false;
//
//		ShipId = 25;

//		inventoryList.Add(ItemBasic.NewItemRarity(0, -1, 2));

//		Gems = 1500;
//		Credits = 1000;
//		ShipParts = 3;

//		RaidProgress = 3;
	}


	// Additional Debug Options

	public const bool OverrideCurrency = false;
	public const bool OverrideTutorial = false;

	public bool IsSecretMissionReady {

		get {
			return CheckQuestTime();
		}

	}

	public bool IsSecretBossReady {
		
		get {
			return CheckQuestBossTime();
		}
	}

	public bool IsRaidReady {
		
		get {
			return CheckRaidTime ();
		}
	}


	public bool needNewSecretSector = true;

	public void SetSecretSector () {

//		Debug.Log ("Setting Secret Sectors");

		if (needNewSecretSector || secretSector == -1)
		{
			secretSector = -1;
			
			List<int> eligiableSectors = EligableSectors;

			if (eligiableSectors.Count > 0) {
				secretSector = KJMath.RandomMemberOf<int>(eligiableSectors);
			}

//			Debug.Log ("elig: " + eligiableSectors);
//			Debug.Log ("secretSector: " + secretSector);
//			Debug.Log ("---" );

		}

		if (needNewSecretSector || secretBossSector == -1) {


			secretBossSector = -1;
			
			List<int> eligiableSectors = EligableSectors;
			
			if (eligiableSectors.Count > 0) {
				secretBossSector = KJMath.RandomMemberOf<int>(eligiableSectors);
			}

//			Debug.Log ("elig: " + eligiableSectors);
//			Debug.Log ("secretBossSector: " + secretBossSector);
//			Debug.Log ("---" );

		}

		if (ProfileController.SectorsUnlocked > 4) {

			if (needNewSecretSector || secretRaidSector == -1) {
				
				
				secretRaidSector = -1;
				
				List<int> eligiableSectors = EligableSectors;
				
				if (eligiableSectors.Count > 0) {
					secretRaidSector = KJMath.RandomMemberOf<int>(eligiableSectors);
				}

//				Debug.Log ("elig: " + eligiableSectors);
//				Debug.Log ("secretRaidSector: " + secretRaidSector);
//				Debug.Log ("---" );
			}
		}

		if (ProfileController.SectorsUnlocked > 3) {
			
			if (needNewSecretSector || secretBroadcastSector == -1) {
				
				
				secretBroadcastSector = -1;
				
				List<int> eligiableSectors = EligableSectors;
				
				if (eligiableSectors.Count > 0) {
					secretBroadcastSector = KJMath.RandomMemberOf<int>(eligiableSectors);
				}

//				Debug.Log ("elig: " + eligiableSectors);
//				Debug.Log ("secretBroadcastSector: " + secretBroadcastSector);
//				Debug.Log ("---" );
			}
		}

		needNewSecretSector = false;

		// currentSecretSector is either set to the map ID or to -1 (means it is not accessable).

	}

	List<int> EligableSectors {
		get {

			List<int> eligiableSectors = new List<int>();
			
			// Any Sectors +/3 can be the secret mission.
			
//			eligiableSectors.Add (SectorsUnlocked + 4);
//			eligiableSectors.Add (SectorsUnlocked + 3);
//			eligiableSectors.Add (SectorsUnlocked + 2);
//			eligiableSectors.Add (SectorsUnlocked + 1);
			
			eligiableSectors.Add (SectorsUnlocked - 1);
			eligiableSectors.Add (SectorsUnlocked - 2);
			eligiableSectors.Add (SectorsUnlocked - 3);
			eligiableSectors.Add (SectorsUnlocked - 4);
			eligiableSectors.Add (SectorsUnlocked - 5);
			eligiableSectors.Add (SectorsUnlocked - 6);
			
			// Cull the List
			
			for (int i = eligiableSectors.Count - 1 ; i > - 1 ; i -- )
			{
				if (eligiableSectors[i] > (SectorsUnlocked - 1) || eligiableSectors[i] < 1 || eligiableSectors[i] == secretSector || eligiableSectors[i] == secretRaidSector || eligiableSectors[i] == secretBossSector )
				{
					eligiableSectors.RemoveAt(i);
				}
			}

			return eligiableSectors;
		}
	}

	public bool IsSecretMission (int checkId) {

		if (!IsSecretMissionReady) return false;

		if (checkId == secretSector) {
			return true;
		} else {
			return false;
		}
	}

	public bool IsSecretBossMission (int checkId) {
		
		if (!IsSecretBossReady) return false;
		
		if (checkId == secretBossSector) {
			return true;
		} else {
			return false;
		}
	}

	public bool IsRaidMission (int checkId) {
		
		if (!IsRaidReady) return false;
		
		if (checkId == secretRaidSector) {
			return true;
		} else {
			return false;
		}
	}

	public bool IsBroadcastMission (int checkId) {

		if (!AdManager.CanAdBeShown) return false;
		
		if (checkId == secretBroadcastSector) {
			return true;
		} else {
			return false;
		}
	}

	public void DoAllTutorials ()
	{
		for (int i = 0 ; i < tutorialStatus.Count ; i ++ )
		{
			tutorialStatus[i] = true;
		}
	}
	
	// Constants
	
	public const int MaxHearts = 4;
	public static bool forceMap = false;
	
//	const int MinutesPerRefill = 15;
	const int MinutesPerRefill = 10;

	
	public static long TicksPerRefill {
		get {
			return new TimeSpan(0, MinutesPerRefill, 0).Ticks;
		}
	}
	
	public static float SecondsPerRefill {
		get {
			return (float) MinutesPerRefill * 60.0f;
		}
	}
	
	private static ProfileController _Singleton;
	public static ProfileController Singleton {
		get {
			
			if (!_Singleton)
			{
				GameObject parentObject = new GameObject("Profile Controller");
				DontDestroyOnLoad(parentObject);
				_Singleton = parentObject.AddComponent<ProfileController>();
				_Singleton.Initialize();
			}
			
			return _Singleton;
		}
	}
	
	/// Round Based Values
	
	public int isGuest = 1;
	public int roundGems = 0;
	float roundRank = 0f;
	public bool lowGraphic = false;


//	public float currentOfferTimer = 0;
	public float forceOfferAdsTime = 3600;
	public string targetOfferAdsTime = "";
	/// Persistant Values
	
	ObscuredInt _Credits = 0; // Hard Currency
	ObscuredInt _Gems = 0; // Soft Currency

	// System to Support Raid Event
	ObscuredInt _ShipParts = 0; // Gained from Raid Bosses
	ObscuredInt _RaidProgress = 0; // Progress on the Current Raid Boss.
	ObscuredInt _RaidLevel = 0; // Current Level of the Raid (How many times the raid was beaten?)

	// Raid Difficulty Factors
	// This is how we calculate the difficulty of the raid level.
	public const float raidBaseLevel = 5.0f;
	public const float raidLevelFactor = 7.0f;
	public const float raidProgressFactor = 1.5f;
	public const float raidLevelCap = 10;

	// Preview Mode
	public static bool previewMode = false;
	public static int previewShip = 0;

	bool _GemDouble = false; // Gains Double Gems
	public bool disableSkipAds = false;

	long _RefillMarkedTime = 0; // Ticks of when the Heart should be refilled.
	int _Hearts = MaxHearts; // Energy System
	float _Rank = 0f;
	int _SectorId = 0;
	int _SectorsUnlocked = 0;
	int _SubSector = 0;
	SectorPattern _Sector = null;
	
	int _helpedFriends = 0, _helpedStrangers = 0;
	public static FriendItem rewardItem;
	
	public static bool allyStandby = false;

	public static bool secretMission = false; // Currently playing on a secret mission?
	public static bool secretBoss = false; // Currently playing on a boss mission?
	public static bool secretRaid = false; // Currently playing on a boss mission?

	public static int controlMode = 0;
	public static float controlSensitivity = 1.0f;

	public static bool isDie = false;
	public static bool isFirstWinBoss = false;

	public static int secretSector = 0;
	public static int secretBossSector = 0;
	public static int secretRaidSector = 0;
	public static int secretBroadcastSector = 0;

	public static bool GemDouble {
		get { return Singleton._GemDouble; }	
		set { Singleton._GemDouble = value; }
	}


	public static bool IsLoadData {
		get { return Singleton.isLoadData; }	
		set { Singleton.isLoadData = value; }
	}

	bool isLoadData = false;
	public static int isLoadDataAsInt {
		get { return Singleton.isLoadData == true ? 1 : 0; }	
		set { Singleton.isLoadData = value == 1 ? true : false; }
	}

	public static int GemDoubleAsInt {
		get { return GemDouble == true ? 1 : 0; }	
		set { GemDouble = value == 1 ? true : false; }
	}
	
	public static int HelpedFriends {
		get { return Singleton._helpedFriends; }	
		set { Singleton._helpedFriends = value; }
	}
	public static int HelpedStrangers {
		get { return Singleton._helpedStrangers; }	
		set { Singleton._helpedStrangers = value; }
	}
	
	string _displayName = "Castor";
	public static string DisplayName { get { return Singleton._displayName; } set { Singleton._displayName = value; } }
	
	public string _socialName = "Unnamed", _socialUserName = "SCUnamed";
	public static string SocialName {
		get { return Singleton._socialName; }	
		set { Singleton._socialName = value; }
	}
	
	public static string SocialUserName {
		get { return Singleton._socialUserName; }	
		set { Singleton._socialUserName = value; }
	}
	
	public string _socialId = string.Empty;
	public static string SocialId {
		get { return Singleton._socialId; }	
		set { Singleton._socialId = value; }
	}

	/// Items
	
	/// New System
	
	public List<ItemBasic> equipList = new List<ItemBasic>();
	public List<ItemBasic> inventoryList = new List<ItemBasic>();
	
	// public List<ItemBasic> shipList = new List<ItemBasic>();
	
	/// Ship System
	
	public static bool hasCollectedCargo = false;
	
	public bool[] shipUnlockStatus;
	public int[] shipUpgradeLevel;
	public int shipId = 0;
	
	public Stats coreStats;
	public Stats equipStats;
	public Stats finalStats;
	
	public bool isFightingRival = false;
	public RivalData rivalData = null;
	
	public bool hasGoldReward = false;
	public bool isOffline = true;
	
	public int narrativeProgress = -1;
	public List<bool> tutorialStatus;
	public const int tutorialAmount = 40;
	public const int MaxSubSectors = 6;
	
	public static bool HasInventorySpace {
		get {
			
			if (Singleton.inventoryList.Count < 24)
				return true;
			else
				return false;
		}
	}
	
	public static RivalData Rival {
		get {
			if (Singleton.rivalData == null)
			{
//				print ("Creating New Rival!");
				Singleton.rivalData = new RivalData(); // DataController.DecodeRivalData(DB.Singleton.JsonDecoded(DB.Singleton.JsonEncodedAll("RequestBigData")));
			}
			
			return Singleton.rivalData;
		}
		
		set {
			Singleton.rivalData = value;
		}
	}
	
	
	/// Current Ship Stats
	
	public static float StatDamage {
		get {
			return 10.0f;
		}
	}
	
	public static float StatArmor {
		get {
			return 10.0f;
		}
	}
	
	public static float StatLife {
		get {
			return 10.0f;
		}
	}
	
	public static float StatEnergy {
		get {
			return 10.0f;
		}
	}
	
	public static float StatSpeed {
		get {
			return 10.0f;
		}
	}
	
	/*
	public static ItemBasic Ship {
		get {
			
			// TEMP: Add a default ship.
			
			if (Singleton.shipList.Count == 0)
			{
				Singleton.shipList.Add(ItemBasic.NewRandomItem());
				Singleton.shipList.Add(ItemBasic.NewRandomItem());
				Singleton.shipList.Add(ItemBasic.NewRandomItem());
			}
			
			return Singleton.shipList[Singleton.shipId];
			// return ItemPattern.ShipList[Singleton.shipId];
		}
	}
	*/
	
	public static ObscuredInt Credits {
		get { return Singleton._Credits; }	
		set { Singleton._Credits = value; }
	}

	public static ObscuredInt Gems {
		get { return Singleton._Gems; }	
		set { Singleton._Gems = value; }
	}

	// Raid System

	public static ObscuredInt ShipParts {
		get { return Singleton._ShipParts; }	
		set { Singleton._ShipParts = value; }
	}
	
	public static ObscuredInt RaidProgress {
		get { return Singleton._RaidProgress; }	
		set { Singleton._RaidProgress = value; }
	}

	public static ObscuredInt RaidLevel {
		get { return Singleton._RaidLevel; }	
		set { Singleton._RaidLevel = value; }
	}

	// ------
	
	public static int ShipId {
		get { return Singleton.shipId; }	
		set { Singleton.shipId = value; }
	}
	
	public static ItemShip Ship {
		get { return ItemPattern.ShipList[Singleton.shipId]; }
	}
	
	public static int Hearts {
		get { return Singleton._Hearts; }	
		set { Singleton._Hearts = value; }
	}
	
	public static long RefillMarkedTime {
		get { return Singleton._RefillMarkedTime; }	
		set { Singleton._RefillMarkedTime = value; }
	}

	long _MarkQuestTime = 0;
	public static long MarkQuestTime {
		get { return Singleton._MarkQuestTime; }	
		set { Singleton._MarkQuestTime = value; }
	}

	long _RenewQuestTime = 0;
	public static long RenewQuestTime {
		get { return Singleton._RenewQuestTime; }	
		set { Singleton._RenewQuestTime = value; }
	}

	long _MarkQuestBossTime = 0;
	public static long MarkQuestBossTime {
		get { return Singleton._MarkQuestBossTime; }	
		set { Singleton._MarkQuestBossTime = value; }
	}
	
//	long _RenewQuestBossTime = 0;
//	public static long RenewQuestBossTime {
//		get { return Singleton._RenewQuestBossTime; }	
//		set { Singleton._RenewQuestBossTime = value; }
//	}

	long _MarkRaidTime = 0;
	public static long MarkRaidTime {
		get { return Singleton._MarkRaidTime; }	
		set { Singleton._MarkRaidTime = value; }
	}
	
//	long _RenewRaidTime = 0;
//	public static long RenewRaidTime {
//		get { return Singleton._RenewRaidTime; }	
//		set { Singleton._RenewRaidTime = value; }
//	}

	public static int SectorsUnlocked {
		get { return Singleton._SectorsUnlocked; }	
		set { Singleton._SectorsUnlocked = value; }
	}
	
	public static SectorPattern Sector {
		get { 
			if (Singleton._Sector == null) {
				SetSector(SectorId);
			}
			return Singleton._Sector; 
		}	
		set { Singleton._Sector = value; }
	}

	public static SectorPattern ActualSector {
		get { 
			if (secretMission) {
				return SectorPattern.At(secretSector - 1);
			} else if (secretBoss) {
				return SectorPattern.At(secretBossSector - 1);
			} else if (secretRaid) {
				return SectorPattern.At(secretRaidSector - 1);
			}
			return Sector;
		}
	}
	
	public static int SectorId {
		get { return Singleton._SectorId; }	
		set { SetSector(value);}
	}
	
	public static int SubSector {
		get { return Singleton._SubSector; }	
		set { Singleton._SubSector = value; }
	}
	
	public static float Rank {
		get { return Singleton._Rank; }
	}

	public static void SetSector (int id) {
		Singleton._SectorId = id;
		Sector = SectorPattern.At(id - 1);
		Singleton.needNewSecretSector = true;
		Singleton.SetSecretSector();
//		Debug.Log ("id1: " + id + " // id2: " + Sector.id + " // Secret Sector: " + secretSector);
	}
	
	public static int NewItems
	{
		get {
			int newItems = 0;
			foreach (ItemBasic item in Singleton.inventoryList)
			{
				if (item.isNew) newItems ++;
			}
			
			return newItems;
		}
	}
	
	public static float SectorLevel {
		get {
//			return WaveController.Level;
			return (float) SectorId + (float) SubSector / (float) MaxSubSectors;
		}
	}
	
	public static bool IsShipUnlocked (int shipId)
	{
		if (shipId < Singleton.shipUnlockStatus.Length) {
			
			if (Singleton.shipUnlockStatus[shipId]) return true;
		}
		
		return false;
	}
	
	public static int GetShipUpgrade (int shipId)
	{
		if (shipId < Singleton.shipUpgradeLevel.Length) {
			
			return Singleton.shipUpgradeLevel[shipId];
		}
		
		return 0;
	}
	
	public static void UnlockShip (int shipId)
	{
		if (shipId < Singleton.shipUnlockStatus.Length) {
			
			Singleton.shipUnlockStatus[shipId] = true;


		}
	}

	public static void LockShip (int shipId)
	{
		if (shipId < Singleton.shipUnlockStatus.Length) {
			
			Singleton.shipUnlockStatus[shipId] = false;
			
			
		}
	}
	
	public static void UpgradeShip (int shipId)
	{
		if (shipId < Singleton.shipUpgradeLevel.Length) {
			
			Singleton.shipUpgradeLevel[shipId] ++ ;
		}
	}

	
	public static void Reset ()
	{
		Singleton._Reset();
	}
	
	void _Reset ()
	{
        //DoAllTutorials()
	   // SetDebugParams();

        //Ini Ads
        Advertisement.Initialize(ProfileController.adsUnityID, false);

        /// Round Based Values
        countPlayForAds = 0;
		currentIntervalTimer = 0;
		isLoadData = false;
//		currentOfferTimer = 3600;
		targetOfferAdsTime = System.DateTime.Now.AddHours(-1).ToString();

		roundGems = 0;
		roundRank = 0f;

		_GemDouble = false;

		secretSector = -1;
		narrativeProgress = -1;
		
		/// Persistant Values
		
		Gems = 200;
		Credits = 3;
		RaidProgress = 0;
//		Rank = 0f;
		SectorId = 1;
		SectorsUnlocked = _SectorId;
		SubSector = 1;
		SetSector(_SectorId);
		isFirstWinBoss = false;

		// Preview Mode
		previewMode = false;
		previewShip = 0;
		
		/// Initialize Item System
		
		equipList = new List<ItemBasic>(4);
		inventoryList = new List<ItemBasic>();
		
		/// Initialize Stats
		
		//__Test boost speed time
		//KJTime.SetTimeScale(KJTime.SpaceType.Game,3.0f,3.0f);
		//KJTime.SetTimeScale(KJTime.SpaceType.System,3.0f,3.0f);
		//		KJTime.SetTimeScale(KJTime.SpaceType.UI,3.0f,3.0f);
		//__Test
		
		coreStats = new Stats(25.0f, 25.0f, 25.0f, 25.0f);
//		coreStats = new Stats(250000.0f, 25000.0f, 25.0f, 25.0f);
		equipStats = new Stats();
		finalStats = new Stats();
		
		// Ship Unlock
		
		shipId = 0;
		int shipCount = ItemPattern.ShipList.Count;
		shipUnlockStatus = new bool[shipCount];
		shipUpgradeLevel = new int[shipCount];
		
		for (int i = 0 ; i < shipCount ; i ++ )
		{
			shipUnlockStatus[i] = false;
			shipUpgradeLevel[i] = 1;
		}
		
		tutorialStatus = new List<bool>();
		for (int i = 0 ; i < tutorialAmount ; i ++ )
		{
			tutorialStatus.Add (false);
		}
		
		shipUnlockStatus[0] = true;
		
		// TODO: Temp filling inventory space with some random stuff.
		
		//		inventoryList.Add(ItemBasic.NewRandomItem(1));
		
		SetDebugParams();
		SetSecretSector();
		
	}

	public bool CheckShowAds () {
       
		Debug.Log("CheckShowAds");
		if (ProfileController.SectorId >= ProfileController.Singleton.minSectorAds) {
			System.DateTime timeNow = System.DateTime.Now;
			System.DateTime targetTime = System.Convert.ToDateTime(ProfileController.Singleton.targetOfferAdsTime);
			if ((targetTime - timeNow).TotalMinutes <= 0) {
				canShowAds = true;
			} else if (ProfileController.Singleton.countPlayForAds >= ProfileController.Singleton.minCountShowAds || ProfileController.Singleton.currentIntervalTimer > ProfileController.Singleton.intervalAdstime) {
				canShowAds = true;
			}
		}
        print("CheckShowAds" + canShowAds);
        if (!Advertisement.IsReady())
        {
            Advertisement.Initialize(ProfileController.adsUnityID, false);
            canShowAds = false;
        }
        return canShowAds;
	}

	void SetDebugLevel (int debugLevel = 15, int subSector = 1)
	{
		SetSector(debugLevel);
		_SectorsUnlocked = debugLevel;
		_SubSector = subSector;
	}
	
	public void SetDebugItems (int itemModule1 = 0, int itemModule2 = 0, int itemModule3 = 0, int itemModule4 = 0, int itemRarity1 = 0, int itemRarity2 = 0, int itemRarity3 = 0, int itemRarity4 = 0)
	{
		//		inventoryList.Add(ItemBasic.NewItemRarity(0, itemModule1, itemRarity1));
		//		inventoryList.Add(ItemBasic.NewItemRarity(0, itemModule2, itemRarity2));
		//		inventoryList.Add(ItemBasic.NewItemRarity(0, itemModule3, itemRarity3));
		//		inventoryList.Add(ItemBasic.NewItemRarity(0, itemModule4, itemRarity4));
		if (_SectorId >= 1) {
			int itemLevel = 50;
			equipList.Add(ItemBasic.NewItemRarity(itemLevel, itemModule1, itemRarity1));
			equipList.Add(ItemBasic.NewItemRarity(itemLevel, itemModule2, itemRarity2));
			equipList.Add(ItemBasic.NewItemRarity(itemLevel, itemModule3, itemRarity3));
			equipList.Add(ItemBasic.NewItemRarity(itemLevel, itemModule4, itemRarity4));
		}
	}
	
	void Initialize ()
	{
		ObscuredInt.SetNewCryptoKey(1150);
		/// Create the list of items that have already been unlocked by the player.
		_Reset ();
		
	}

//	GameObject adsUnity;
//	void AddAdsUnity () {
//		adsUnity = new GameObject("Ads Unity");
//		adsUnity.AddComponent<AdManagerUnity>();
//		adsUnity.transform.SetParent(gameObject.transform);
//	}

	public static void CalibrateStats ()
	{
		// Calibrate Stats with Equipment and Final.
		Singleton.equipStats = new Stats();
		Singleton.finalStats = new Stats();
		
		foreach (ItemBasic item in Singleton.equipList)
		{
			Singleton.AddEquipStats(item);
		}
		
		Singleton.finalStats.Add(Singleton.coreStats);
		Singleton.finalStats.Add(Singleton.equipStats);
		
	}
	
	void AddEquipStats (ItemBasic item)
	{
		// Add the stats of the given item to the equipped stats.
		item.Calibrate();
		equipStats.Add (item.stats);
	}
	
	public static bool HasItem (ItemBasic item)
	{
		return true;
	}
	
	public static void GainItem ()
	{
		
	}
	
	public static bool HasEquipSpace
	{
		get {
			if (Singleton.equipList.Count < 4) return true;
			
			for (int i = 0 ; i < 4 ; i ++ )
			{
				if (Singleton.equipList[i] == null) return true;	
			}
			
			return false;
		}
	}
	
	public static void EquipShip (ItemShip ship)
	{
		Singleton.shipId = ship.id;
	}
	
	public static void EquipItem (ItemBasic item)
	{
		if (Singleton.inventoryList.Contains(item))
		{
			Singleton.inventoryList.Remove(item);	
		}
		
		Singleton.equipList.Add(item);
		
		CalibrateStats();
	}
	
	public static void UnequipItem (ItemBasic item)
	{
		if (Singleton.equipList.Contains(item))
		{
			//Singleton.equipList[Singleton.equipList.IndexOf(item)] = null;
			Singleton.equipList.Remove(item);
			Singleton.inventoryList.Add(item);
		}
		
		CalibrateStats();
	}
	
	public static bool IsItemEquipped (ItemBasic item)
	{
		
		// Check to see if given item is currently in the equip list.
		return Singleton.equipList.Contains(item);
		
	}
	
	public static ItemBasic TempGainItem ()
	{
		ItemBasic newItem = ItemBasic.NewRandomItem(SectorLevel);
		Singleton.inventoryList.Add(newItem);
//		DataController.SaveAllData(); //nun
		return newItem;
	}
	
	public static ItemBasic GainCargoItem ()
	{
		
		int itemRarity = 1;
		
		if (KJDice.Roll(5)) {
			itemRarity = 2;
		}
		
		ItemBasic newItem = ItemBasic.NewItemRarity(SectorLevel, -1, itemRarity);
		Singleton.inventoryList.Add(newItem);
		return newItem;
	}
	
	public static ItemBasic GainBlackMarketItem ()
	{
		
		int itemRarity = 2;
		
		if (KJDice.Roll(20)) {
			itemRarity = 3;
			
			if (KJDice.Roll(10)) {
				itemRarity = 4;
			}
		}

		if (itemRarity == 3) {
			Achievement.AchievementComplete(Achievement.AchievementType.A7FindRareItem);
		}

		if (itemRarity == 4) {
			Achievement.AchievementComplete(Achievement.AchievementType.A8FindSecretItem);
		}
		
		ItemBasic newItem = ItemBasic.NewItemRarity(SectorsUnlocked + 2, -1, itemRarity);
		Singleton.inventoryList.Add(newItem);
		return newItem;
	}
	
	public static void SetGemsAndRank (int gems, float rank)
	{
		Singleton._Gems = gems;
		Singleton._Rank = rank;
	}
	
	public static void CalibrateRoundWithProfile ()
	{
		Singleton._CalibrateRoundWithProfile();
		// DataController.SaveAllData();
	}
	
	void _CalibrateRoundWithProfile ()
	{
		float factor = 1.0f + Stats.FactorForLevel(SectorLevel);
		roundGems = (int)(roundGems * factor * 0.75f);
//		Debug.Log("roundGems" + roundGems);
		_Gems += roundGems;
		if( roundRank > _Rank) _Rank = roundRank;
	}
	
	public static void ResetForNewRound ()
	{
		Collectable.CargoSpawned = false;
		hasCollectedCargo = false;
		Singleton.roundGems = 0;
		Singleton.roundRank = 0;
		
		WaveController.allEnemy = 0;
		WaveController.allEnemyDie = 0;
		
		// UIController.SetGems(Singleton.roundGems);
		
		// DataController.SaveAllData();
	}
	
	public static void GainRoundGem (int amount)
	{
		Singleton.roundGems += amount;
		// UIController.SetGems(Singleton.roundGems);
	}

	public static void CalibratePreGame ()
	{
//		LoseHeart(false);

	}
	
	public static void LoseHeart (KJTime.TimeDelegate onSaveComplete = null)
	{

		if (Hearts > 0) {
			
			if (Hearts == MaxHearts) {
				RefillMarkedTime = System.DateTime.UtcNow.Ticks + TicksPerRefill;
			} else {
				RefillMarkedTime += TicksPerRefill;
			}
			
			Hearts --;
			
			//if (RefillMarkedTime == 0)
			//{
			
			//RefillMarkedTime += System.DateTime.UtcNow.Ticks + TicksPerRefill;
			//}
		}
		// Debug.Log(Hearts);


		DataController.SaveTime();
		DataController.SaveAllData(false, onSaveComplete);


	}
	
	public static void RestoreHeart ()
	{
		
		Hearts = MaxHearts;
		RefillMarkedTime = 0;

		DataController.SaveTime();
//		DataController.SaveAllData();

		
	}

	public const int QuestGap = 4;
	public const int QuestBossGap = 16;
	public const int RaidGap = 12;
	public const int RaidProgressMax = 4;

	public static void SetMarkQuestTime() {

		DateTime nextTime = DateTime.UtcNow.AddHours(QuestGap);
		MarkQuestTime = nextTime.Ticks;
		DataController.SaveTimeQuest();
		Singleton.needNewSecretSector = true;

		#if UNITY_IOS
//		EtceteraTwoBinding.cancelAllLocalNotifications();
//		EtceteraTwoBinding.scheduleLocalNotification(((int) (ProfileController.QuestGap * 60 * 60)), "A new secret mission is now available!", "Play");
//		// int secondsUntilTomorrow = 60 * 60 * 24;
		// if (!ProfileController.Singleton.hasGoldReward) EtceteraTwoBinding.scheduleLocalNotification(secondsUntilTomorrow, "You have been awarded a Gold Credit. Come and collect it!", "Play", 1, string.Empty, string.Empty);
		#endif
		//Debug.Log(new DateTime(MarkQuestTime));
	}

	public static void SetRenewQuestTime() {

		DateTime nextTime = DateTime.UtcNow.AddHours(QuestGap);
		RenewQuestTime = nextTime.Ticks;
		Singleton.needNewSecretSector = true;
		Singleton.SetSecretSector();

	}

	public static bool CheckQuestTime () {
		if (DateTime.UtcNow.Ticks > MarkQuestTime) {
			return true;
		} else {
			return false;
		}
	}

	public static bool CheckQuestRenew () {
//		Debug.Log ("Renew Time: " + MarkQuestTime);
		if (DateTime.UtcNow.Ticks > RenewQuestTime) {
			SetRenewQuestTime();
			return true;
		} else {
			return false;
		}
	}

	public static void SetMarkQuestBossTime() {
		
		DateTime nextTime = DateTime.UtcNow.AddHours(QuestBossGap);
		MarkQuestBossTime = nextTime.Ticks;
		DataController.SaveTimeQuestBoss();

	}

	public static bool CheckRaidTime () {

		if (DateTime.UtcNow.Ticks > MarkRaidTime) {
			return true;
		} else {
			return false;
		}

	}

	public static void SetMarkRaidTime() {
		
		DateTime nextTime = DateTime.UtcNow.AddHours(RaidGap);
		MarkRaidTime = nextTime.Ticks;
		DataController.SaveTimeRaid();
		
	}

	public static void OnRaidWin () {
		// Increase the raid progress.
		RaidProgress ++ ;
	}

	public static void OnRaidComplete () {
		// Raid is completed. Reset the progress.
		RaidProgress = 0;
		RaidLevel ++;
	}

	public static void OnRaidFail () {
		// Reset the raid progress.
		RaidProgress = 0;
	}
	
	public static bool CheckQuestBossTime () {
		
		if (DateTime.UtcNow.Ticks > MarkQuestBossTime) {
			return true;
		} else {
			return false;
		}
		
	}

	public static long markInboxTime = 0;
	public static bool CheckInBoxTime () {
		if (DateTime.UtcNow.Ticks > markInboxTime) {
			return true;
		} else {
			return false;
		}
	}

	public static void AddRefillMarkedTime () {

		if (RefillMarkedTime != 0) {
			long addTime = new DateTime(RefillMarkedTime).AddMinutes(-15).Ticks;
			RefillMarkedTime = addTime;
			DataController.SaveTime();
		}
	}

	public static void GainHeart ()
	{
		Hearts ++;
		if (Hearts >= MaxHearts)
		{
			// All refill has been done.
			Hearts = MaxHearts;
			RefillMarkedTime = 0;
		} else {
			// Next step refill.	
			// RefillMarkedTime += TicksPerRefill;
			
			//			long timeDifferenceInTicks = ProfileController.RefillMarkedTime - System.DateTime.UtcNow.Ticks;
			//			
			//			// Check next inverval, and insta-refill.
			//			if (timeDifferenceInTicks <= 0) 
			//			{
			//				GainHeart();
			//			}
		}
		
		DataController.SaveTime();
		
	}
	
	public static long HeartTimeAdjuster {
		get {
			return TicksPerRefill * ((MaxHearts - Hearts) - 1);
		}
	}
}

