﻿using UnityEngine;
using System.Collections;

public class MenuController {

	static private Shutter currentShutter;
	static public bool isShutterClosed = false;
	static private int sceneToLoad = 0;

	public static bool HasTutorialPrompt = false;
	
	public static void SetShutterAsCurrent (Shutter newShutter)
	{
		currentShutter = newShutter;
	}
	
	public static void LoadToScene (int sceneId)
	{
		InputController.LockControls();
		sceneToLoad = sceneId;
		currentShutter.CloseShutters();
	}
	
	public static void OnShutterClosed ()
	{

		HasTutorialPrompt = false;
		isShutterClosed = true;
		if (sceneToLoad != 1) NetworkController.Singleton.isInGame = false;
		KJTime.RemoveGameAndUITimers();
		KJParticleController.DeactivateAll();
		Application.LoadLevel(sceneToLoad);	
	}
}
