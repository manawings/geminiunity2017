using UnityEngine;
using System.Collections;

public class CentralController : MonoBehaviour
{
	private static CentralController _Singleton;
	public static CentralController Singleton {
		get {
			if (!_Singleton)
			{
				GameObject parentObject = new GameObject("Central Controller");
				DontDestroyOnLoad(parentObject);
				_Singleton = parentObject.AddComponent<CentralController>();
				_Singleton.Initialize();
			}
			return _Singleton;
		}
	}
	
	public void CheckAndStart ()
	{
		// Blank Function used to Initilaize the Singleton.
	}
	
	void Initialize ()
	{
		
	}
}

