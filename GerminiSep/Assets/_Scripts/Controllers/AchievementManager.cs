﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AchievementManager : KJSingleton<AchievementManager> {

//	/// <summary>
//	/// (persistent) Indicates whether the message box was shown on very 1st start.
//	/// </summary>
//	bool initialMessageBoxShown = false;
//	/// <summary>
//	/// (persistent) Indicates whether GameCenterBinding.authenticateLocalPlayer should be called.
//	/// </summary>
//	bool autoConnect = false;
//	/// <summary>
//	/// Set temporily when Game Center app is opened to force in-game Game Center auhentication.
//	/// </summary>
//	bool reconnect = false;
//	
//	void Awake () {
//		initialMessageBoxShown = false; // Read this from your config
//		autoConnect = false; // Read this from your config
//	}
//	
//	
//	// some useful ivars to hold information retrieved from GameCenter. These will make it easier
//	// to test this code with your GameCenter enabled application because they allow the sample
//	// to not hardcode any values for leaderboards and achievements.
//
//	#if UNITY_IOS
//	private List<GameCenterLeaderboard> _leaderboards;
//	private List<GameCenterAchievementMetadata> _achievementMetadata;
//	private List<GameCenterPlayer> _friends;
//	#endif
//	
//	// bools used to hide/show different buttons based on what is loaded
//	private bool _hasFriends;
//	private bool _hasLeaderboardData;
//	private bool _hasAchievementData;
//
//	public void AuthenticateLocalPlayer () {
//
//		if (!initialMessageBoxShown) {
//			#if UNITY_IOS
//			EtceteraManager.alertButtonClickedEvent += GameCenterMessageBoxCallback;
//			string [] buttons = {"No", "Yes"};
//			EtceteraBinding.showAlertWithTitleMessageAndButtons ("Game Center", "Do you wish to use Game Center for this App?", buttons);
//			#endif
//		}
//
//		if (!autoConnect) {
//			return;
//		}
//
//	}
//	
//	public void GameCenterMessageBoxCallback (string button) {
//
//		Debug.Log ("Callback: " + button);
//
//		if (button == "Yes") {
//			PlayerPrefs.SetInt("GameCenter", 1);
//		}
//
//		if (button == "No") {
//			PlayerPrefs.SetInt("GameCenter", 0);
//		}
//
//		PlayerPrefs.Save();
//
//
//		bool autoConnect = button == "Yes";
//		initialMessageBoxShown = true;
//
//		if (autoConnect) {
//			#if UNITY_IOS
//			GameCenterBinding.authenticateLocalPlayer ();
//			#endif
//		}
//	}
//
//
//	public void ResetShowAuth () {
//
//		initialMessageBoxShown = false; // Read this from your config
//		autoConnect = false; // Read this from your config
//		AuthenticateLocalPlayer ();
//
//	}
//	
	protected override void OnInitialize ()
	{


		#if UNITY_IOS


		if(GeminiGameCenter.Singleton != null)
			Debug.Log("Succes to Authen");
		else
			Debug.Log("Fail to connect to gamecenter");

//		GameCenterManager.achievementMetadataLoaded += delegate( List<GameCenterAchievementMetadata> achievementMetadata )
//		{
//			_achievementMetadata = achievementMetadata;
//			_hasAchievementData = _achievementMetadata != null && _achievementMetadata.Count > 0;
//		};
//		
//
//		GameCenterManager.playerAuthenticated += () =>
//		{
//			LoadAchievementMetadata();
//			GameCenterBinding.showCompletionBannerForAchievements();
//
//		};
//		
//
//		if (PlayerPrefs.HasKey("GameCenter")) {
//
//		} else {
//			AuthenticateLocalPlayer();
//		}

		#endif
	}
//	
//	private void loadFriends()
//	{
//		#if UNITY_IOS
//		GameCenterManager.playerDataLoaded += friends =>
//		{
//			_friends = friends;
//			_hasFriends = _friends != null && _friends.Count > 0;
//		};
//		
//		Debug.Log( "player is authenticated so we are loading friends" );
//		GameCenterBinding.retrieveFriends( true, true );
//		#endif
//	}
//	
//	
//	public static void ShowGCAchievements()
//	{
//		#if UNITY_IOS
//		GameCenterBinding.showGameCenterViewController( GameCenterViewControllerState.Achievements );
//		#endif
//	}
//
//	public static void ResetAchievements()
//	{
//		#if UNITY_IOS
//		GameCenterBinding.resetAchievements();
//		#endif
//		
//	}
//	
//	public static void LoadAchievementMetadata()
//	{
//		#if UNITY_IOS
//		GameCenterBinding.retrieveAchievementMetadata();
//		#endif
//
//	}
//
	public static void PostAchievement (string achId)
	{
		#if UNITY_IOS

//		if (!GameCenterBinding.isPlayerAuthenticated()) return;

//		if( !Singleton._hasAchievementData )
//		{
//			Debug.Log ("Achievement Data Has Not Been Loaded");
//			return;
//		}
//
//		bool hasFound = false;
//		for (int i = 0 ; i < Singleton._achievementMetadata.Count ; i ++ )
//		{
//			if (Singleton._achievementMetadata[i].identifier == achId)
//				hasFound = true;
//
//		}
//
//		if (!hasFound) {
//			Debug.Log ("Achievement Not Found: " +  achId);
//			return;
//		}
//
//		GameCenterBinding.showCompletionBannerForAchievements();
//		GameCenterBinding.reportAchievement( achId, 100 );
		GeminiGameCenter.Singleton.ReportAchievement(achId,100f);

		#endif
	}
//	
//

}
