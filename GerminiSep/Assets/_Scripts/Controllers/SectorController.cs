﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SectorController {

	public static List<float> masterSeedList;

	public static int GetSeedForLevelSector (int sectorId, int subSector, int overWrite = 1) {
		return ((sectorId) * 1000)  +  (subSector * 1000) + overWrite + sectorId + subSector;
	}
	
}
