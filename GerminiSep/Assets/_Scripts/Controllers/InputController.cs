using UnityEngine;
using System.Collections;

public class InputController
{




	// Re-Tap Controls
	const float ReTapRadius = 50.0f;
	const float ReTapTime = 0.45f;
	private static Vector2 retapPosition = Vector2.zero, relativeAnchor = Vector2.zero;
	private static int checkForRetap = 0;

	private static InputController _Singleton;
	public static InputController Singleton { 
		get
		{
			if ( _Singleton == null )
			{
				_Singleton = new InputController();
				_Singleton.Start();
				
			}
			return _Singleton;
		}
	}
	
	
	bool isShooting = false;
	
	public static bool IsControlLocked { get; private set; }
	public static bool IsPopUpLocked { get; private set; }
	public static bool IsRelativeTouch { get; private set; }
	public static float TouchFactor { get; private set; }
	
	public static void Clear ()
	{
		Singleton._Clear();
	}
	
	void _Clear ()
	{
		IsRelativeTouch = ProfileController.controlMode == 1 ? true : false;
		if (IsRelativeTouch) {
			TouchFactor = ProfileController.controlSensitivity;
		} else {
			TouchFactor = 1.0f;
		}

		checkForRetap = 0;
		isShooting = false;
		KJTime.Add(Run);
		EndMark();
	}
	
	
	// Selective Input Lock System.


	// For Calculating the Interpolation
	static float offsetFactor = 1.0f;
	static float safeBound;
	static float distanceFromSafeBound;
	static float differenceCap = 1.0f / 80.0f;

	static float expandFactorX, expandFactorY;


	void Start ()
	{

		widthFactor = 1.0f / Screen.width;
		heightFactor = 1.0f / Screen.height;
		
		cachedGameWidth = tk2dCamera.Instance.ScreenExtents.width;
		cachedGameHeight = tk2dCamera.Instance.ScreenExtents.height;

		expandFactorX = Screen.width / cachedGameWidth;
		expandFactorY = Screen.height / cachedGameHeight;

		cachedVirtualWidth = GameController.gameStageWidth;

		safeBound = - 0.35f * cachedGameHeight;
		
		/** Calculate the bounds. */
		
		float boundSize = 70.0f;
		boundY = tk2dCamera.Instance.ScreenExtents.yMin + boundSize;
		boundLeft = tk2dCamera.Instance.ScreenExtents.xMin + boundSize;
		boundRight = tk2dCamera.Instance.ScreenExtents.xMax - boundSize;
		
		Clear();
	}
	
	float pointFactorX, pointFactorY, projectedX, projectedY, aimerX, aimerY;
	float cachedGameWidth, cachedGameHeight, cachedVirtualWidth;	
	float widthFactor, heightFactor;

	bool isOutOfBounds = false;
	
	/** Bounds for the Blind Spot (where the buttons are). */
	
	float boundY, boundLeft, boundRight;
	
	bool isPressedFromLastUpdate = false;
//	bool isOutOfBounds = false;

	float originalProjectedX = 0.0f;
	float originalProjectedY = 0.0f;
	
	void RunPointerAt (float rawPointerX, float rawPointerY)
	{



		rawPointerX *= TouchFactor;
		rawPointerY *= TouchFactor;

		if (IsRelativeTouch) {

			originalProjectedX = (rawPointerX * widthFactor - 0.5f) * cachedGameWidth;
			originalProjectedY = (rawPointerY * heightFactor - 0.5f) * cachedGameHeight;

			rawPointerX += relativeAnchor.x;
			rawPointerY += relativeAnchor.y;

		}

		pointFactorX = rawPointerX * widthFactor - 0.5f;
		pointFactorY = rawPointerY * heightFactor - 0.5f;
		
		projectedX = pointFactorX * cachedGameWidth;
		projectedY = pointFactorY * cachedGameHeight;
	
		
		aimerX = pointFactorX * cachedVirtualWidth;
		aimerY = projectedY; // + 180.0f;

//		if (IsRelativeTouch) {
//			aimerX += aimDifference;
//		}


		// Restrict x-axis on Platform levels
		if (WaveController.type == WaveController.Type.Platform) {
			if (aimerX > 140) {
				aimerX = 140;
				if (IsRelativeTouch) relativeAnchor.x = RelativeXAnchor;
			}
			if (aimerX < -140) {
				aimerX = -140;
				if (IsRelativeTouch) relativeAnchor.x = RelativeXAnchor;
			}
		} else {
			if (aimerX > 300) {
				aimerX = 300;
				if (IsRelativeTouch) relativeAnchor.x = RelativeXAnchor;
			}
			if (aimerX < -300) {
				aimerX = -300;
				if (IsRelativeTouch) relativeAnchor.x = RelativeXAnchor;
			}
		}

		// Do not let it go up too high.
		if (aimerY > 200) {
			aimerY = 200;
			if (IsRelativeTouch) relativeAnchor.y = RelativeYAnchor;
		}
		if (aimerY < -300) {
			aimerY = -300;
//			if (IsRelativeTouch) relativeAnchor.y = GameController.PlayerUnit.transform.position.y - originalProjectedY;
		}	

		if (IsPositionOpen(projectedX, projectedY))
		{
			isOutOfBounds = false;	
		}
	}

	bool IsPositionOpen (float positionX, float positionY)
	{
		
		if (positionY < boundY)
		{
			if (positionX < boundLeft || positionX > boundRight)
			{
				return false;	
			}
		}
		
		return true;
	}


	float oldAimX, newAimX, aimDifference = 0.0f;
	float offsetRelativeFactor = 1.18f; //1.18f;

	float RelativeXAnchor {
		get {

			float adjustedShipPosition = (GameController.PlayerUnit.transform.position.x - Camera.main.transform.position.x) * offsetRelativeFactor;
			float adjustedProjectedX = originalProjectedX;
			float finalAnchor = (adjustedShipPosition - adjustedProjectedX) * expandFactorX;

			return finalAnchor;
		}
	}

	float RelativeYAnchor {
		get {
			
			float adjustedShipPosition = GameController.PlayerUnit.transform.position.y;
			float adjustedProjectedY = originalProjectedY;
			float finalAnchor = (adjustedShipPosition - adjustedProjectedY) * expandFactorY;
			
			return finalAnchor;
		}
	}
	
	void Run () {

		if (Input.GetButton("Fire1")) {


			// Finger Pressed

			Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);


			if (Input.touchCount > 0) {
				Touch touch = Input.touches[0];
				mousePosition.x = touch.position.x;
				mousePosition.y = touch.position.y;
			}

			RunPointerAt(mousePosition.x, mousePosition.y);


			Vector2 newPosition = new Vector2(mousePosition.x, mousePosition.y);

			if (checkForRetap == 2) {
				if (Vector2.Distance(retapPosition, newPosition) < ReTapRadius) {
					OnReTap();
				}
			}

			retapPosition = new Vector2(mousePosition.x, mousePosition.y);

			if (!isPressedFromLastUpdate)
			{

				if (IsRelativeTouch) {

					relativeAnchor = new Vector2(RelativeXAnchor,  RelativeYAnchor);
					RunPointerAt(mousePosition.x, mousePosition.y);


				}

				if (!isPressedFromLastUpdate)
				{
					if (IsPositionOpen(projectedX, projectedY))
					{
						isOutOfBounds = false;	
					} else {
						isOutOfBounds = true;	
					}
					
					isPressedFromLastUpdate = true;
				}

				isPressedFromLastUpdate = true;
			}

			if (!IsRelativeTouch) {
				distanceFromSafeBound = - ( aimerY - safeBound );
				offsetFactor = 1.0f - distanceFromSafeBound * differenceCap;
				if (offsetFactor > 1.0f) offsetFactor = 1.0f;
				if (offsetFactor < 0.0f) offsetFactor = 0.0f;
			}

			if (!isOutOfBounds) {
				GameController.Singleton.SetAimerTarget(aimerX, aimerY, offsetFactor);
				StartShooting();
			}


		} else {
			
			if (isPressedFromLastUpdate)
			{
				// Finger Released

				relativeAnchor = Vector2.zero;
				StopShooting();
				isPressedFromLastUpdate = false;
				checkForRetap++;
				KJTime.Add(RemoveReTapCheck, ReTapTime, 1);

			}	
		}

//		if (Input.GetButtonDown("Fire2")) {
//			UIController.Singleton.reactorButton.Fire();
//		}

	}

	void OnReTap ()
	{
		UIController.Singleton.reactorButton.Fire();
	}

	void RemoveReTapCheck ()
	{
		checkForRetap = 0;
		KJTime.Remove(RemoveReTapCheck);
	}
	
	void StartShooting ()
	{
		if (WaveController.type == WaveController.Type.BrutalMine 
		    || WaveController.type == WaveController.Type.BrutalRock 
		    || WaveController.type == WaveController.Type.Platform)
		return;

		if (!isShooting) {
			if (!GameController.PlayerUnit.IsStunned) {
				isShooting = true;
				if (GameController.Singleton.playerShip.GetComponent<PlayerUnit>().IsAlive)
				{
					GameController.Singleton.playerShip.GetComponent<PlayerUnit>().StartShooting();
				}
			}
		}
	}
	
	public void StopShooting ()
	{
		if (isShooting) {
			isShooting = false;
			GameController.Singleton.playerShip.GetComponent<PlayerUnit>().StopShooting();
		}
	}
	
	public static void LockControls ()
	{
//		Debug.Log("Lock All");
		IsControlLocked = true;
		IsPopUpLocked = true;
	}
	
	public static void LockControlForPopUp ()
	{
//		Debug.Log("Lock Pop");
		IsControlLocked = true;
		IsPopUpLocked = false;
	}

	public static KJNGUIButtonFunction unlockedButton;

	public static void LockControlsExceptFor (KJNGUIButtonFunction button)
	{
//		Debug.Log("Lock Tut");
		IsControlLocked = true;
		IsPopUpLocked = true;
		unlockedButton = button;
	}
	
	public static void UnlockControls ()
	{
//		Debug.Log("Unlock");
		IsControlLocked = false;
		IsPopUpLocked = false;
		unlockedButton = null;
	}


	// MOUSE MARKING

	// Mark the Mouse position and furthest distance. Useful for tracking drags, etc.

	private static Vector2 originPosition, currentPosition;
	private static float largestDistanceSq;

	public static void BeginMark ()
	{
		currentPosition = Vector2.zero;
		originPosition = Vector2.zero;
		KJTime.Add(KJTime.SpaceType.UI, RunMark);
		originPosition = Input.mousePosition;
		largestDistanceSq = 0;
	}

	static void RunMark ()
	{
		currentPosition = Input.mousePosition;
		float xDist = originPosition.x - currentPosition.x;
		float yDist = originPosition.y - currentPosition.y;
		float distanceSq = xDist * xDist + yDist * yDist;

		if (distanceSq > largestDistanceSq) largestDistanceSq = distanceSq;

	}

	public static void EndMark ()
	{
		KJTime.Remove(RunMark);
	}

	public static float LargestMarkedDistance {

		get {
			// Auto End the Mark.
			EndMark();
			return Mathf.Sqrt(largestDistanceSq);
		}
	}
}

