using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
	
	public static GameController Singleton { get; private set; }

	// Linked Objects

	public GameObject playerShipPrefab;
	public GameObject rivalShipPrefab;
	public SectorBoss sectorBoss;
	
	public GameObject slotPointerPrefab;

	public MainBGController bgController;

	// Player Life Bar

	public KJBarGlow lifeBar;
	public UILabel lifeLabel;

	public tk2dSpriteCollectionData spriteCollection;
	
	public GameObject playerShip { get; private set; }
	private PlayerUnit _playerUnit;
	public static PlayerUnit PlayerUnit {
		get {
			if (Singleton._playerUnit == null)
			{
				Singleton._playerUnit = Singleton.playerShip.GetComponent<PlayerUnit>();
			}
			
			return Singleton._playerUnit;
		}
	}
	
	public GameObject allyShip { get; private set; }
	private PlayerUnit _AllyUnit;
	public static PlayerUnit AllyUnit {
		get {
			if (Singleton._AllyUnit == null)
			{
				if (Singleton.allyShip != null) {
					Singleton._AllyUnit = Singleton.allyShip.GetComponent<PlayerUnit>();
				}
			}
			
			return Singleton._AllyUnit;
		}
	}
	
	RivalUnit _CurrentRivalBoss;
	public static RivalUnit CurrentRivalBoss {
		get {
			return Singleton._CurrentRivalBoss;
		}
		
		set {
			Singleton._CurrentRivalBoss = value;
		}
	}

	SectorBoss _CurrentSectorBoss;
	public static SectorBoss CurrentSectorBoss {
		get {
			return Singleton._CurrentSectorBoss;
		}
		
		set {
			Singleton._CurrentSectorBoss = value;
		}
	}

	void ShowStory ()
	{
	
		UIController.ShowStory(0);
	}
		
	// Game Dimension Control
	public static float gameStageWidth = 600.0f;
	public float halfGameStageWidth { get; private set; }
	public float cameraRatio { get; private set; }
	public bool puaseForRevive = false;
	[HideInInspector] public float physicalPadding = 10, extraBottomPadding = 60, physicalPlaneWidth = 30, physicalPlaneHeight = 45;
	[HideInInspector] public float leftBound = 0, rightBound = 0, upBound = 0, downBound = 0;
	[HideInInspector] public float aimBound = 150;
	
	// Test emitter for BG Environment
	KJEmitter cloudEmitter, starEmitter;

	void TestAds () {
		//UIController.ShowPopupViewAd();
		//adsUnity.GetComponent<AdManagerUnity>().ShowAd();
	//	UIController.ShowPopupRevive();
	//	EndGame(false);
	//	MenuController.LoadToScene(19);

	}

	// Use this for initialization
	void Start () {

        KJTime.Add(TestAds, 2, 1);
        //ProfileController.Singleton.DoAllTutorials();

        Singleton = this;
		// Clear Boss References
		CurrentRivalBoss = null;
		CurrentSectorBoss = null;

		Initialize();
		//AddAdsUnity();

		//Debug.Log(ProfileController.Singleton.countPlayForAds);
		ProfileController.Singleton.countPlayForAds++;
		// KJParticleController.Initialize(spriteCollection);
//		WeaponPattern.Initialize();

		InputController.Clear();
		StartScene();
		KJTime.Add(ShowStartHeader, 1.5f, 1);

		bgController.Deploy();
		BGGraphics graphics = ProfileController.ActualSector.SectorGraphic;
		DeployAtmosphere(graphics.particleColorStart, graphics.particleColorEnd);

		KJCanary.Singleton.SwitchToAmbience();
		KJCanary.Singleton.RecalibrateTimers();

		KJTime.Add(SwitchSongToCombat, 30.0f, 1);
		KJTime.Add (Unit.ResetExplodeCount, 0.1f);
//		KJTime.Add(ShowStory, 12.0f, 1);

		Achievement.noTakeDamage = true;

		WaveController.Calibrate();
		#if UNITY_IPHONE
		if (UnityEngine.iOS.Device.generation != UnityEngine.iOS.DeviceGeneration.iPhone4) {
//			if (Everyplay.IsSupported()) {
//				Everyplay.StartRecording();
//			}
		}
		#endif
		ProfileController.Singleton.adsReviveAvailable = true;
		puaseForRevive =  false;
	}

	void SwitchSongToCombat ()
	{
		if (_CurrentSectorBoss == null)
		KJCanary.Singleton.SwitchToCombat();
	}
	
	void Initialize ()
	{
		halfGameStageWidth = gameStageWidth * 0.5f;
		
		float sHeight = tk2dCamera.Instance.ScreenExtents.height;
		float sWidth = tk2dCamera.Instance.ScreenExtents.width;
		float sHalfHeight = sHeight / 2.0f;

		leftBound = (physicalPadding + physicalPlaneWidth / 2.0f) - halfGameStageWidth;
		rightBound = halfGameStageWidth - (physicalPadding + physicalPlaneWidth / 2.0f);	
		upBound = (physicalPadding + physicalPlaneHeight / 2.0f) - sHalfHeight;
		downBound = sHalfHeight - (physicalPadding + physicalPlaneHeight / 2.0f);

		float halfDeltaGap = (gameStageWidth - sWidth) / 2.0f;
		float maxCameraWidth = halfDeltaGap + physicalPadding + physicalPlaneWidth / 2.0f;
			
		cameraRatio = maxCameraWidth / halfGameStageWidth;
		
	}

	public static float fingerGap = 56.0f;
	
	public void SetAimerTarget (float projectedX, float projectedY, float offsetFactor = 1.0f)
	{

		if (InputController.IsRelativeTouch) {
			PlayerUnit.SetTargetPoint(new Vector3(projectedX, projectedY, 0.0f));
		} else {
			PlayerUnit.SetTargetPoint(new Vector3(projectedX, projectedY + fingerGap * offsetFactor, 0.0f));
		}

	}
	
	public void SetAllyTarget (float projectedX, float projectedY)
	{
		AllyUnit.SetTargetPoint(new Vector3(projectedX, projectedY, 0.0f));
	}
	
	public void FireAllyReactor (int reactorId)
	{
		AllyUnit.FireReactor(reactorId);
	}

	public Vector2 GetShipPoint ()
	{
		return new Vector2 (playerShip.transform.position.x, playerShip.transform.position.y);
	}

	Color CopiedColor (Color from, float alpha = 1.0f )
	{
		return new Color(from.r, from.g, from.b, alpha);
	}

	public void DeployAtmosphere (Color startColor, Color endColor)
	{

//		if (ProfileController.Singleton.lowGraphic) return;

		cloudEmitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		cloudEmitter.CopyFromModel(EffectController.Singleton.bgCloud);

		starEmitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		starEmitter.CopyFromModel(EffectController.Singleton.bgStar);

		cloudEmitter.SetColor(
			CopiedColor(startColor, 0.15f),
			CopiedColor(endColor, 0.15f));

		starEmitter.SetColor(
			startColor,
			endColor);

//		cloudEmitter.SetGraphic(KJParticleGraphic.Type.Cloud);

		cloudEmitter.Deploy();
		starEmitter.Deploy();
	}
	
	void StartScene ()
	{

		// BG Star Emitter
		ProfileController.ResetForNewRound();
		Projectile.SetBulletLimits();
		hasGameEnded = false;

		allyShip = null;
		_AllyUnit = null;

		if (NetworkController.Singleton.isMultiplayer)
		{

		}
		
		_playerUnit = null;
		playerShip = (GameObject) Instantiate(playerShipPrefab);
		int activeShip = ProfileController.Singleton.shipId;
		if (ProfileController.previewMode) activeShip = ProfileController.previewShip;
		PlayerUnit.Initialize( activeShip , true );
		UIController.HideReactorButton(true);

		NetworkController.Singleton.isInGame = true;

	}

	public void SpawnAlly ()
	{

		UIController.ShowHeader(ProfileController.Rival.displayName, "HAS JOINED THE BATTLE", null, false, CombatHeader.ColorType.Blue);

		allyShip = (GameObject) Instantiate(playerShipPrefab);
		AllyUnit.InitializeAsAlly();
		AllyUnit.StartShooting();
		AllyUnit.StartAI();
	}
	
	void ShowStartHeader ()
	{
		if (ProfileController.Singleton.tutorialStatus[1] == false) {
//			Debug.Log ("Show Start 1");
			KJTime.Add( DeployTutorial, 2.0f, 1 );
			KJTime.Add( ShowStartHeaderActual, 4.0f, 1);
		} else if (ProfileController.Sector.story_FirstSector != -1) {
//			Debug.Log ("Show Start 2");
			KJTime.Add( DeploySectorStory, 1.0f, 1 );
			KJTime.Add( ShowStartHeaderActual, 2.0f, 1);
		} else {
			ShowStartHeaderActual();
		}
	}

	void DeploySectorStory ()
	{

		ProfileController.Sector.DeployFirstSectorStory();
	}

	void ShowStartHeaderActual ()
	{
		// Debug.Log ("Show Start GO");


		if (ProfileController.secretMission || ProfileController.secretBoss) {

			UIController.ShowHeader("SECRET MISSION", "PREPARE TO ENGAGE", StartGame, false, CombatHeader.ColorType.Yellow);

		} else if (ProfileController.secretRaid) {

			UIController.ShowHeader("RAID MISSION", "PREPARE TO ENGAGE", StartGame, false, CombatHeader.ColorType.Yellow);

		} else if (ProfileController.previewMode) {

			UIController.ShowHeader("SHIP PREVIEW", "SIMULATION BEGIN", StartGame, false, CombatHeader.ColorType.Blue);

		} else if (WaveController.type == WaveController.Type.Platform) {

			UIController.ShowHeader("BLOCKADE MISSION", "SURVIVE THE ENCOUNTER!", StartGame, false, CombatHeader.ColorType.Yellow);

		} else if (WaveController.type == WaveController.Type.BrutalRock) {

			UIController.ShowHeader("ASTEROID MISSION", "SURVIVE THE ENCOUNTER!", StartGame, false, CombatHeader.ColorType.Yellow);

		} else if (WaveController.type == WaveController.Type.BrutalMine) {

			UIController.ShowHeader("MINE MISSION", "SURVIVE THE ENCOUNTER!", StartGame, false, CombatHeader.ColorType.Yellow);


		} else {
			UIController.ShowHeader("SECTOR " + ProfileController.SectorId + "." + ProfileController.SubSector, "PREPARE TO ENGAGE", StartGame);
		}

	}

	void DeployTutorial ()
	{
		TutorialData.Deploy(1);
	}

	static float timeAtStart;

	public void StartGame ()
	{

		timeAtStart = Time.time;
		WaveController.ResetAndStart();
	}
	
	public void AllyDie ()
	{
		if (AllyUnit.IsAlive) AllyUnit.ExternalDie();	
	}
	
	public void EndGame (bool hasWon)
	{

		#if UNITY_IPHONE
		if (UnityEngine.iOS.Device.generation != UnityEngine.iOS.DeviceGeneration.iPhone4) {
//			Everyplay.StopRecording();
//			Everyplay.SetMetadata("sector", ProfileController.SectorId + "." + ProfileController.SubSector);
//			Everyplay.SetMetadata("chapter", ProfileController.Sector.name);
//			Everyplay.SetMetadata("pilot", ProfileController.DisplayName);
		}
		#endif
		//Show Ads
//		if (ProfileController.Singleton.countPlayForAds > ProfileController.Singleton.minCountShowAds) {
//			if (ProfileController.SectorLevel > 3) {
//				//adsUnity.GetComponent<AdManagerUnity>().ShowAd();
//				ProfileController.Singleton.countPlayForAds = 0;
//			}
//		}

		iTween.Stop(Singleton.gameObject);

		NetworkController.Singleton.Disconnect();

		KJActivePool.Singleton.DeactivateAll();

		KJTime.RemoveGameTimers();


		NetworkController.Singleton.isMultiplayer = false;

		FlurryController.EndWaveTypeTime();

		if (ProfileController.previewMode) {

			MenuController.LoadToScene (6);

		} else {
		
			if (hasWon)
			{
				ProfileController.AddRefillMarkedTime(); // like add Hearts
				ProfileController.CalibrateRoundWithProfile();

				MenuController.LoadToScene(3);

			} else {

				FlurryController.PlayerDefeated();
				if (ProfileController.Hearts == 0) {
					FlurryController.LockedOut();
				}
				if (ProfileController.Singleton.CheckShowAds()) {
                    ProfileController.Singleton.loadSectorWhenEndAds = 2;
                    MenuController.LoadToScene(19);
				} else {
					MenuController.LoadToScene(2);
				}

			}

		}
		
	}

	//__TEST
	public void AutoScene1 (){
//		MenuController.LoadToScene(2);
	}
	public void AutoScene2 (){
//		MenuController.LoadToScene(1);
	}
	//


	public bool hasGameEnded = false;

	public static void WinGame ()
	{
		if (Singleton.hasGameEnded) return;

		if (ProfileController.Sector.story_PreVictory != -1 && ProfileController.SubSector == ProfileController.MaxSubSectors) {
			ProfileController.Sector.DeployPreVictoryStory();
			KJTime.Add(WinStepActual, 2.0f, 1);

		} else if (ProfileController.secretRaid) {
			TutorialData.Deploy(22);
			KJTime.Add(WinStepActual, 2.0f, 1);
		} else {
			KJTime.Add(WinStepActual, 2.0f, 1);
		}

		Singleton.hasGameEnded = true;

	}

	static void WinStepActual ()
	{
		if (ProfileController.previewMode) {
			EndSimulation () ;
		} else {
			UIController.ShowHeader("VICTORY","SECTOR CLEARED", GameController.WinGameActual);
		}

	}

	static void EndSimulation () {
		UIController.ShowHeader("SHIP PREVIEW","SIMULATION ENDED", GameController.WinGameActual, false, CombatHeader.ColorType.Blue);
	}
		

	public static void OnDie ()
	{

		if (Singleton.hasGameEnded) return;

		//UIController.ShowPopupRevive();

		KJTime.Add ( LoseGame, 0.5f, 10 );
		// Can revive in Secret Mission, in boss fights, and in any Sector greater than 1, after time played is > 45 sec.

		if (!ProfileController.previewMode) {
			if (ProfileController.secretMission || ProfileController.secretBoss || ProfileController.secretRaid) {
				UIController.ShowPopupRevive();

			} else if (ProfileController.SubSector == ProfileController.MaxSubSectors) {
				UIController.ShowPopupRevive();

			} else if (WaveController.type == WaveController.Type.Platform || WaveController.type == WaveController.Type.BrutalRock || WaveController.type == WaveController.Type.BrutalMine) {

				UIController.ShowPopupRevive();

			} else {

				if ((ProfileController.SectorId > 1) && (Time.time - timeAtStart > 25) ) {

					UIController.ShowPopupRevive();

				}
			}
		}
//
//		if(Singleton.puaseForRevive)
//			Singleton.puaseForRevive = false;
//		else
//			KJTime.Add ( LoseGame, 0.5f, 10 );

	}

	public static void Revive ()
	{
		KJTime.Remove ( LoseGame );
		PlayerUnit.Revive(ProfileController.Singleton.shipId);
	}
	
	public static void LoseGame ()
	{


		if (Singleton.hasGameEnded) return;

		if (WaveController.type == WaveController.Type.Platform) {
			WaveController.hasPlatformList = false;
		}

		if (ProfileController.previewMode) {

			EndSimulation () ;

		} else {

			Singleton.hasGameEnded = UIController.ShowHeader("DEFEATED", "MISSION FAILED", GameController.LoseGameActual);

		}

		KJTime.Remove(LoseGame);

	}
	
	private static void WinGameActual ()
	{
		Singleton.EndGame(true);
	}

	private static void SimulationEnd ()
	{
		Singleton.EndGame(true);
	}
	
	public static void LoseGameActual ()
	{
		ProfileController.isDie = true;

		Singleton.EndGame(false);
	}
	
	void SpawnEnemy ()
	{
		SetAllyTarget(Random.Range(-gameStageWidth/2, gameStageWidth/2), Random.Range(-250, 50));
	}
	
	void Reset ()
	{
		
	}
	
	public void UpdateLifeBar ()
	{
		lifeBar.SetProgress(PlayerUnit.stats.LifePercent);
		lifeLabel.text = PlayerUnit.stats.lifePoints.ToString("F0") + " HP";
	}
	
	public void ShakeScreen ()
	{
		KJCamera.Shake(4.0f);
		// iTween.ShakePosition(Camera.main.camera.gameObject, new Vector3 (5.0f, 5.0f, 0.0f), 0.65f);	
	}
	
	public void BigShakeScreen ()
	{
		KJCamera.Shake(8.0f);
		// iTween.ShakePosition(Camera.main.camera.gameObject, new Vector3 (15.0f, 25.0f, 0.0f), 0.65f);	
	}

	public void MegaShakeScreen ()
	{
		KJCamera.Shake(12.0f, KJCamera.ShakeAxis.Y, 1.5f, 0.93f);
		// iTween.ShakePosition(Camera.main.camera.gameObject, new Vector3 (15.0f, 25.0f, 0.0f), 0.65f);	
	}
	
	public void DeploySectorBoss ()
	{
		KJDice.index = 0;
		BossPattern.Initialize();
//		sectorBoss.Deploy( BossPattern.OMEGA_T0);	
		sectorBoss.Deploy(BossPattern.GetBossPatternForLevel(WaveController.Level));	
		UIController.ShowEnemyBar();
	}

	public void DeployRivalBoss ()
	{
		CurrentRivalBoss = ((GameObject)Instantiate(rivalShipPrefab)).GetComponent<RivalUnit>();
		CurrentRivalBoss.Deploy(0);
		UIController.ShowEnemyBar();
	}
}
