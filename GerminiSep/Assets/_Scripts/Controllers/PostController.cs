using UnityEngine;
using System.Collections;
using CodeStage.AntiCheat.ObscuredTypes;

public class PostController : MonoBehaviour
{

	public GameObject viewAllyButton;
	public GameObject continueButton;
	public GameObject creditReward;

	public GameObject[] detailPanel;

	int detailPanelCount = 0;
	ObscuredInt gemRewardValue;
	bool hasCreditReward = false;
	int numberOfTweenDisplay;
	float progress ;	
	float sectorMax;
	float sectorNow;
	// Use this for initialization

	public GameObject everyplayButton;

	bool sectorCleared = false;

	int postTutorialId = 0;

	void DeployPostTutorial (int id = 0)
	{
		postTutorialId = id;
		KJTime.Add(DeployPostTutorialActual, 0.75f, 1);

	}

	void DeployPostTutorialActual () {
		TutorialData.Deploy(postTutorialId);
	}


	void Start ()
	{
		
//		if (iPhone.generation == iPhoneGeneration.iPhone4) {
			everyplayButton.SetActive(false);
//		}

		if (ProfileController.isFirstWinBoss && TutorialData.CheckTutorial(38)) {
			InputController.LockControls();
			DeployPostTutorial(38);
		}

		for(int i = 0 ; i <  detailPanel.Length ; i ++)
		{
			detailPanel[i].SetActive(false);
		}

		sectorCleared = false;

		KJCanary.Singleton.SwitchToAmbience();

		// Only increase the game progress if it's not a secret mission.
		if (!ProfileController.secretMission && !ProfileController.secretBoss && !ProfileController.secretRaid) {
			ProfileController.SubSector ++;
//			ProfileController.Singleton.adsReviveAvailable = true;
		}

		sectorMax = (float) ProfileController.MaxSubSectors;
		sectorNow = (float) ProfileController.SubSector;
		progress = (sectorNow - 1.0f) / sectorMax;

		gemRewardValue = RewardController.gainGemReward(WaveController.allEnemy, WaveController.allEnemyDie);

		if (WaveController.type == WaveController.Type.Quest_Bonus) {
			float factor = 1.0f + Stats.FactorForLevel(ProfileController.SectorLevel);
			gemRewardValue = (int)(gemRewardValue * factor * 2.0f);
		}

		if (WaveController.type == WaveController.Type.Quest_Boss) {
			float factor = 1.0f + Stats.FactorForLevel(ProfileController.SectorLevel);
			gemRewardValue = (int)(gemRewardValue * factor * 3f);
		}

		ProfileController.Gems += gemRewardValue;
		
		UIController.UpdateCurrency();

		if (sectorNow > sectorMax)
		{
			ProfileController.SubSector = 1;
			int newSector = ProfileController.SectorId;
			if (ProfileController.SectorId < 100) {
				newSector += 1;
			} else {
				TutorialData.Deploy(31);
			}

			ProfileController.SetSector(newSector);
			
			if (ProfileController.SectorId > ProfileController.SectorsUnlocked)
			{
				//ProfileController.Singleton.adsReviveAvailable = true;
				ProfileController.SectorsUnlocked = ProfileController.SectorId;
				ProfileController.Credits ++;
				detailPanel =  AddToArray(detailPanel, creditReward);
				detailPanel[detailPanel.Length - 1].GetComponent<VictoryDisplayPanel>().Deploy("","Credit Bounty", 1);
				sectorCleared = true;

				FlurryController.SectorCleared();

			}
		
		}

		// Secret Boss Defeated. Reward 2 Credits
		if (ProfileController.secretBoss) {
			ProfileController.Credits += 2;
			detailPanel =  AddToArray(detailPanel, creditReward);
			detailPanel[detailPanel.Length - 1].GetComponent<VictoryDisplayPanel>().Deploy("","Credit Bounty", 2);
		}

		// Raid Boss Defeated. Reward 1 Core.
		if (ProfileController.secretRaid) {

			DeployPostTutorial(24);
			ProfileController.OnRaidWin();

			if (ProfileController.RaidProgress == ProfileController.RaidProgressMax) {

				// Current Raid is Completed
				DeployPostTutorial(25);

				int coreAmount = 1;
				ProfileController.ShipParts += coreAmount;
				detailPanel =  AddToArray(detailPanel, creditReward);
				VictoryDisplayPanel victoryPanel = detailPanel[detailPanel.Length - 1].GetComponent<VictoryDisplayPanel>();
				victoryPanel.labelNum.color = new Color32 (96, 255, 189, 255);
				victoryPanel.labelText.color = new Color32 (96, 255, 189, 255);
				victoryPanel.Deploy("XCSmall_v3", "Fusion Core", coreAmount);
				UIController.UpdateCurrency();

			}
		}


		creditReward.SetActive(false);
		if (viewAllyButton != null)	viewAllyButton.SetActive(false);
		continueButton.SetActive(false);

		if (ProfileController.allyStandby) {

			ProfileController.allyStandby = false;
			detailPanel =  AddToArray(detailPanel, viewAllyButton);

		} else {

			KJMath.SetY(continueButton.transform, -190.0f);

		}

		detailPanel =  AddToArray(detailPanel, continueButton);

		KJTime.Add (SavePostData, 0.15f, 1);
		BeginShow();

		CheckAchievement();
	}



	void OnEveryplayShare ()
	{
//		Debug.Log ("Share");
		//Everyplay.ShowSharingModal();
	}

	void OnEveryplayShow ()
	{
	///	Everyplay.Show();
	}

	void CheckAchievement () {
		
		if (ProfileController.Gems >= 1000)
			Achievement.AchievementComplete(Achievement.AchievementType.A0Collect1000Gems);
		
		if (WaveController.type == WaveController.Type.Quest_Bonus)
			Achievement.AchievementComplete(Achievement.AchievementType.A2SecretMission);
		
		if (WaveController.type == WaveController.Type.Quest_Boss)
			Achievement.AchievementComplete(Achievement.AchievementType.A1BountyMission);
		
		if (WaveController.type == WaveController.Type.Platform && Achievement.noTakeDamage == true)
			Achievement.AchievementComplete(Achievement.AchievementType.A11ClearPlatform);
		
		if (WaveController.type == WaveController.Type.BrutalRock && Achievement.noTakeDamage == true)
			Achievement.AchievementComplete(Achievement.AchievementType.A12ClearRock);
		
		if (WaveController.type == WaveController.Type.BrutalMine && Achievement.noTakeDamage == true)
			Achievement.AchievementComplete(Achievement.AchievementType.A13ClearMines);
		
		if (ProfileController.SectorId > 5)
			Achievement.AchievementComplete(Achievement.AchievementType.A3ReachSector5);
		
		if (ProfileController.SectorId > 10)
			Achievement.AchievementComplete(Achievement.AchievementType.A4ReachSector10);
		
		if (ProfileController.SectorId > 15)
			Achievement.AchievementComplete(Achievement.AchievementType.A5ReacSector15);
		
		if (ProfileController.SectorId > 20)
			Achievement.AchievementComplete(Achievement.AchievementType.A6ReachSector20);
	}

	GameObject[] AddToArray(GameObject[] array, GameObject newValue){
		int newLength = array.Length + 1;


		
		GameObject[] result = new  GameObject[newLength];
		for(int i = 0 ; i < array.Length; i++)
		{
			result[i] = array[i];
		}
		result[newLength -1] = newValue;
		
		return result;
	}

	void BeginShow () {

		if (WaveController.allEnemy == 0) WaveController.allEnemy = 1000;
		float killPercent = (100 * ((float) WaveController.allEnemyDie / (float) WaveController.allEnemy));
		if (killPercent > 100) killPercent = 100;
		float sectorProgress =(100 * progress);

		if (ProfileController.secretMission || ProfileController.secretBoss) sectorProgress = 100;


		// Raid Progress to Reflect the Boss' current HP progress.
		if (ProfileController.secretRaid) {
			
			sectorProgress = 100.0f * ((float) ProfileController.RaidProgress/ (float) ProfileController.RaidProgressMax);
			
			if (ProfileController.RaidProgress == ProfileController.RaidProgressMax)
				ProfileController.OnRaidComplete();

		}

		detailPanel[1].GetComponent<VictoryDisplayPanel>().Deploy("","Sector Progress", sectorProgress, 100, true);
		detailPanel[2].GetComponent<VictoryDisplayPanel>().Deploy("","Enemies Destroyed", WaveController.allEnemyDie);
		detailPanel[3].GetComponent<VictoryDisplayPanel>().Deploy("","Gem Bounty", gemRewardValue + ProfileController.Singleton.roundGems, 0);

		KJTime.Add(MoveNextLastPanelIn, 0.15f);

	}
	

	void MoveNextLastPanelIn ()
	{
		if (detailPanelCount != detailPanel.Length) {
			
			GameObject moveObject = detailPanel[detailPanelCount];

			Hashtable param = new Hashtable();
			param.Add("time", 0.50f);
			param.Add("y", moveObject.transform.position.y);
			param.Add("easetype", iTween.EaseType.easeOutCirc);
			
			moveObject.transform.position = moveObject.transform.position + Vector3.down * 0.85f;
			moveObject.SetActive(true);
			iTween.MoveTo(moveObject, param);
			
			detailPanelCount ++;
			
		} else {
			
			KJTime.Remove(MoveNextLastPanelIn);
		}
	}


	void OnViewAlly ()
	{
		SocialController.friendItem = null;
		GoToFriendProfile(ProfileController.Rival.facebook_id);
	}

	string friendId;

	public void GoToFriendProfile (string friendId)
	{
		this.friendId = friendId;
		UIController.ShowWait();
		if (SocialController.friendListCalibrated)
			RequestCompactProfile();
		else
			DB.Singleton.CallDatabase("RequestFriendList", RequestCompactProfile);
		
	}

	void RequestCompactProfile ()
	{
		SocialController.MergeFriendLists();
		DB.Singleton.CallDatabase("RequestCompactProfile", OnSmallDataSuccess, null, friendId);
		UIController.RefreshWaitDuration();
	}
	
	void OnSmallDataSuccess ()
	{
		// Go to friend profile.
		UIController.ClosePopUp();
		FriendProfileController.isPostBattle = true;
		MenuController.LoadToScene(11);
	}

	void SavePostData ()
	{
		DataController.SaveAllData(true);
	}

	void OnExit ()
	{
		KJTime.Remove(MoveNextLastPanelIn);
		KJTime.RemoveGameTimers();
	
		if (ProfileController.hasCollectedCargo) {

			ProfileController.hasCollectedCargo = false;

			if (ProfileController.secretMission) {
				MenuController.LoadToScene(13);
			} else {
				MenuController.LoadToScene(4);
			}

		} else {
			//MenuController.LoadToScene(2);

			// If the Sector is Cleared then Go to the Map Screen. 

			if (sectorCleared) {
				ProfileController.forceMap = true;
                if (ProfileController.Singleton.CheckShowAds())
                {
                    ProfileController.Singleton.loadSectorWhenEndAds = 7;
                    MenuController.LoadToScene(19);
                }
                else
                {
                    MenuController.LoadToScene(7);
                }
            } else {
				if (ProfileController.Singleton.CheckShowAds()) {
                    ProfileController.Singleton.loadSectorWhenEndAds = 2;
                    print("Application LoadLevel");
                    MenuController.LoadToScene(19);
				} else {
					MenuController.LoadToScene(2);
				}
			}

		}
	}
}

