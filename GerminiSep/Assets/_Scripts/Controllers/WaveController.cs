using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaveController
{
	// Static class to control the flow and difficulty of gameplay.

	public enum Type {
		Balanced,
		BulletHell,
		AoE,
		Mines,
		BeamHell,
		Homing,
		// Fleet,
		Elite,
		Quest_Bonus,
		Quest_Boss,
		BrutalRock,
		BrutalMine,
		Platform,
		Boss,
		Raid
	}


	public static bool TestEnemy = false;
	public static EnemyPattern TestEnemyPattern = EnemyPattern.SwirlGreen;
	public static bool TestElite = false;
	public static EnemyPattern TestElitePattern = EnemyPattern.Elite_RedLaser2;
	public static bool isBossTest = false;
	
	public static Type type;

	// Wave and Spawn Counters
	private static int waveCount;
	private static int squadCount;
	private static List<WaveData.Mode> wavePattern = new List<WaveData.Mode>();
	public static float Level { get; private set; }

	// Approach Variations
	private static int approachDir; // -1, 0, 1
	private static bool approachSwitch = false; // Whether to switch up the direction of approach.
		
	// Wave Shape Controller
	private static WaveData waveData = new WaveData();
	private static WaveData waveDataSingle = new WaveData();
	private static float intensity; // Will be between 0 and 1.
	private static float intensityGain;
	private static WaveData.Mode waveMode = WaveData.Mode.LIGHT;
	private static float waveMod;
	
	private static float standardWaveGap;
	private static float singleSquadGap;
	private static float singleMineGap;
	private static float singleRockGap;
	private static bool standardTimerHasChanged = false;

	// Elite Squad Control
	private static int eliteLimitMin = 0, eliteLimitMax = 0, eliteLimitCount = 0; // Min Max and Current Count of elite units.
	private static bool eliteDualSquad = false; // Whether to include a dual squad.
	public static bool hasCargoSpawned { get; private set; }
	public static bool DoubleElite { get; private set; } // Whether the elite squad is running two units.
	public static int allEnemy = 0;
	public static int allEnemyDie = 0;
	
	private static bool spawnGems = true;
	private static int maxWaves = 0;

	public static bool waveIsRunning = false;

	static bool IsBossSector {

		get {
			return ProfileController.SubSector == ProfileController.MaxSubSectors ? true : false;
		}
	}

	public static void SpawnSingleUnitSolo ()
	{
		EnemyUnit enemyUnit = KJActivePool.GetNewOf<EnemyUnit>("EnemyUnit");

		Vector2 startingPosition = new Vector2();
		startingPosition.x = Random.Range(-200, 200);
		startingPosition.y = 300;

		enemyUnit.DeployAsNormal(startingPosition, EnemyPattern.Standard);
		
		enemyUnit.SetTargetDirection(Mathf.PI, true);
		enemyUnit.squad = null;
	}

	private static void SpawnSocialRival ()
	{
		EnemyUnit enemyUnit = KJActivePool.GetNewOf<EnemyUnit>("EnemyUnit");
		
		enemyUnit.DeployAsRival();
		enemyUnit.SetTargetDirection(Mathf.PI, true);
		enemyUnit.squad = null;
	}

	private static void SpawnAlly ()
	{
		TutorialData.Deploy(29);
		GameController.Singleton.SpawnAlly();
	}

	public static void CheckTutorial(){
		switch (type){


			case Type.BulletHell : TutorialData.Deploy(21);
				break;

			case Type.Elite : TutorialData.Deploy(26);
				break;

			case Type.Quest_Bonus : TutorialData.Deploy(27);
				break;

			case Type.Quest_Boss : TutorialData.Deploy(32);
				break;

			case Type.BrutalRock : TutorialData.Deploy(33);
				break;

			case Type.BrutalMine : TutorialData.Deploy(34);
				break;

			case Type.Platform : TutorialData.Deploy(35);
				break;

			case Type.Raid : TutorialData.Deploy(23);
				break;
		}
	}

	public static void Calibrate ()
	{
		if (ProfileController.secretMission || ProfileController.secretBoss) {
			
			Level = ProfileController.SectorsUnlocked + 1.5f;
			if (Level < 5) Level = 5;
			
		} else if (ProfileController.secretRaid) {

			Level = ProfileController.SectorsUnlocked + 2.5f;
			if (Level < 8) Level = 8;

		} else {
			
			Level = ProfileController.SectorId + ( 0.35f * ProfileController.SubSector );
		}

		KJDice.SetListForSeed(SectorController.GetSeedForLevelSector(ProfileController.SectorId, ProfileController.SubSector, ProfileController.Sector.overWrite));

		if (ProfileController.SubSector == 3) {
			
//			List<Type> specialList = new List<Type>();
//			specialList.Add(Type.BrutalRock);
//			specialList.Add(Type.BrutalMine);
//			specialList.Add(Type.Platform);
//			type = KJDice.RandomMemberOf<Type>(specialList);
			if (ProfileController.SectorId % 3 == 1) {
				type = Type.BrutalMine;
			}
			if (ProfileController.SectorId % 3 == 0) {
				type = Type.BrutalRock;
			}
			if (ProfileController.SectorId % 3 == 2) {
				type = Type.Platform;
			}
			
		} else if (ProfileController.SubSector % 2 == 1) {
			
			type = Type.Balanced;
			
		} else {
			
			List<Type> TypeList = KJConvert.EnumToList<Type>();
			TypeList.Remove(Type.Boss);
			TypeList.Remove(Type.Balanced);
			TypeList.Remove(Type.Quest_Bonus);
			TypeList.Remove(Type.Quest_Boss);
			TypeList.Remove(Type.Raid);
			TypeList.Remove(Type.BrutalRock);
			TypeList.Remove(Type.BrutalMine);
			TypeList.Remove(Type.Platform);
			if(ProfileController.SectorLevel < 8) TypeList.Remove(Type.AoE);
			type = KJDice.RandomMemberOf<Type>(TypeList);
			
		}

		if (ProfileController.SectorId == 1) {
			type = Type.Balanced;
		}

		if (IsBossSector) type = Type.Boss;
		
		if (ProfileController.secretMission) 
			type = Type.Quest_Bonus;
		else if (ProfileController.secretBoss)
			type = Type.Quest_Boss;
		else if (ProfileController.secretRaid)
			type = Type.Raid;

		if (ProfileController.previewMode) 
			type = Type.Balanced;

		//type = Type.Platform; //Test


		platformWall = null;

		if (type == Type.Platform) {

			// Burst Spawn Walls
			int wallCount = 0;
			while (wallCount < 6) {
				SpawnWall();
				wallCount ++;
			}
		}
	}

	public static void ResetAndStart ()
	{


		if (type == Type.BrutalRock && type == Type.BrutalMine && type == Type.Platform) {
			ProfileController.allyStandby = false;
		}

		if (ProfileController.allyStandby) {
				KJTime.Add(SpawnAlly, 5.0f, 1);
		}

		waveIsRunning = true;

//		SpawnCargo();

//		KJTime.Add(EndGame, 10.0f, 1);
//		return;

//		UIController.ShowEnemyBar();

//		KJTime.Add (SpawnSocialRival, 3.0f, 1);
//		return;

//		KJTime.Add(GameController.Singleton.DeployRivalBoss, 1.5f, 1);

		//ProfileController.secretRaid = true;
		if (isBossTest) {
		  	KJTime.Add(SpawnSectorBoss, 1.0f, 1);
			return;
		}

		// KJTime.Add(SpawnCargo, 2.0f);
		//return;

//		KJTime.Add (GameController.WinGame, 3.0f, 1);
//		return;

		if (ProfileController.Singleton.isFightingRival) {
			ProfileController.Singleton.isFightingRival = false;
			UIController.ShowEnemyBar();
			KJTime.Add (SpawnSocialRival, 3.0f, 1);
			return;
		}

//		//__TEST Log
//		List<Type> IType = new List<Type>();
//		List<Type> WaveSequence = new List<Type>();
//		IType = KJConvert.EnumToList<Type>();
//		WaveSequence = KJDice.RandomSequenceFromList(IType, 100);
//		//WaveSequence.ForEach(i => Debug.Log(i));
//		string exeFolder = System.IO.Path.GetDirectoryName(Application.dataPath);
//		using(System.IO.StreamWriter writer = new System.IO.StreamWriter(exeFolder + "\\Log\\" + "AllType.txt", true))
//		{
//			WaveSequence.ForEach(i => writer.WriteLine(i));
//		}
//		//__TEST



		FlurryController.WaveType(type.ToString());
//		Debug.Log("Wave Type: " + type.ToString());
		KJTime.Add(CheckTutorial, 1.2f, 1);

		SetEliteCountForType();
		SetMaxWavesForType();
		SetWaveCount();
		
		intensity = 0;
		squadCount = 0;
		spawnGems = false; // No gems on first spawn.

		//Mine
		offsetMine = 0.0f;
		caseMine = 3;
		lastPositionMineLeft = new Vector3(-280.0f, 0);
		lastPositionMineRight = new Vector3(280.0f, 0);
		directMineCount = 0;
		iniMineCount = 12;

		//Platform
		hasPlatformList = false;

		SetSpawnTimers();
	}

	public static void ResumeWhenElitesCleared ()
	{
		int eliteCount = 0;
		foreach (Unit unit in EnemyUnit.allActive)
		{
			if (unit.isElite) eliteCount ++;
		}
		if (eliteCount == 0) {
			UIController.HideEnemyBar();
			ResumeWaves();
			KJTime.Remove(ResumeWhenElitesCleared);
		}

//		KJCanary.Singleton.SwitchToAmbience();

	}

	private static void ClearSpawnTimers (bool clearMines = true)
	{
		KJTime.Remove(Run);
		KJTime.Remove(SpawnSingleUnit);
		//if (clearMines) KJTime.Remove(SpawnSingleMine);
		//if (clearMines) KJTime.Remove(SpawnLineMines);
	}
	
	public static void CalibrateNextWave ()
	{
		
		if (maxWaves == 0)
		{
			if ( EnemyUnit.allActive.Count == 0)
			{
				if (IsBossSector && type == Type.Boss)
				{

					KJTime.Remove(Run);
					KJTime.Remove(SpawnSingleUnit);
					SpawnSectorBoss();

				} else if (type == Type.Quest_Boss){

					KJTime.Remove(Run);
					KJTime.Remove(SpawnSingleUnit);
					SpawnSectorBoss();

				} else if (type == Type.Raid) {

					KJTime.Remove(Run);
					KJTime.Remove(SpawnSingleUnit);
					SpawnSectorBoss();

				} else {

					waveIsRunning = false;

					KJCanary.Singleton.SwitchToAmbience();
					ClearSpawnTimers();
					GameController.WinGame();
				}
			}

		} else {
			
			intensity = 0;
			squadCount = 0;
			spawnGems = true;

			SetWaveCount();
			SetSpawnTimers();
			
			maxWaves--;
		}
	}
	
	public static void GenerateSubWaves (int maxWaveCount)
	{
		int randomWaveCount;
		int finalWaveCount = 0;
		List<WaveData.Mode> newWavePattern;
		wavePattern.Clear();
		
		if (NetworkController.Singleton.isMultiplayer) Random.seed = NetworkController.Singleton.currentRandomSeed;
		
		while (maxWaveCount > 0)
		{
			randomWaveCount = KJDice.SeededRange(2, KJMath.Cap( maxWaveCount, 2, 4));
			finalWaveCount += randomWaveCount;
			newWavePattern = waveData.GetWavePatternForCount(randomWaveCount);
			maxWaveCount -= randomWaveCount;
			
			for (int i = 0 ; i < newWavePattern.Count ; i ++ )
			{
				wavePattern.Add(newWavePattern[i]);
			}
		}
		
		wavePattern.Reverse();
		waveCount = finalWaveCount;
		intensityGain = 1.0f / (float) finalWaveCount;
		
	}
	
	public static void Run ()
	{
		if (squadCount > 0)
		{
			
			// Spawn a new squad for this wave.
			//SpawnSquad(true); // for test elite
			SpawnSquad(IsEliteSquad);

			
		} else {
			
			if (waveCount > 0)
			{
				waveCount --;
				waveMode = wavePattern[waveCount];
				GetWavePatternModifier();
				intensity += intensityGain;

				squadCount = 1;
					
				waveData = new WaveData();
				waveData.Calibrate(waveMod, IsEliteSquad);
				approachDir = KJDice.Roll(50) ? 1 : -1;
				approachSwitch = KJDice.Roll(50) ? true : false;
					
				// Small Break
					
				if (spawnGems)
				{
					BurstSpawnGems();
					spawnGems = false;
				}
				
			} else {
				
				// Current Wave is Over.
				CalibrateNextWave();
			}
		}
	}

	public static void ResumeWaves ()
	{
		SetSpawnTimers();
	}

	static void EliteTutorial ()
	{
		TutorialData.Deploy(10);
	}

	private static void SpawnSquad (bool isElite = false)
	{
		// Spawn a new squad for this wave.
		// GameObject squadObject = new GameObject();
		Squad squad = KJActivePool.GetNewOf<Squad>("Squad");
		bool isQuestBoss = false;
		bool isRival = false;
		// Manage Elite Units
		if (isElite) {

			if (TutorialData.CheckTutorial(10)) KJTime.Add(EliteTutorial, 3.0f, 1);
			UIController.ShowEnemyBar();

			if (eliteDualSquad && eliteLimitCount == 1)
			{
				waveData.size = 2;
				DoubleElite = true;
			} else {

				if (!ProfileController.secretBoss && !ProfileController.secretMission) {
					if (CanSpawnRival) {
						if (KJDice.SeededRoll(0.5f)) {
							isRival = true;
						}
					}
				}

				waveData.size = 1;
				DoubleElite = false;
			}

			if (eliteLimitCount == 1 && type == Type.Quest_Bonus) {
				KJTime.Remove(Run);
				KJTime.Remove(SpawnSingleUnit);
				isQuestBoss = true;
				if (maxWaves != 0) {
					maxWaves = 0 ;
				}
			}

			eliteLimitCount -- ;
		}

		//isRival = true; //##TEST Rival

		// Deploy the Squad
		squad.Deploy(waveData.maneuver, waveData.formation, waveData.size, waveData.unitPatterns, approachDir, isElite, isQuestBoss, isRival);
		squadCount --;

		
		if (approachSwitch) approachDir = approachDir == 1 ? -1 : 1; // Switch up the approach direction.
		if (isElite) {
			KJCanary.Singleton.SwitchToCombat();
			ClearSpawnTimers(); // Pause Spawning until the elites are defeated.
		}

	}
	
	public static void SpawnSingleUnit ()
	{
		/*
		if (singleTimerHasChanged)
		{
			// TODO: KJTime.Change
			singleTimerHasChanged = false;
		}
		*/
		
		if (waveCount > 0)
		{
			int oppositeApproach = approachDir == 1 ? -1 : 1;
			waveDataSingle.Calibrate(waveMod, false, false);
			waveDataSingle.maneuver = SquadManeuver.LONE;
			waveDataSingle.size = 1;
			
			GameObject squadObject = new GameObject();
			Squad squad = squadObject.AddComponent<Squad>();
			squad.Deploy(waveDataSingle.maneuver, waveDataSingle.formation, waveDataSingle.size, waveDataSingle.unitPatterns, oppositeApproach);
		}
	}
	
	private static void SpawnSingleMine ()
	{
		Mine mine = KJActivePool.GetNewOf<Mine>("Mine");
		mine.Deploy( new Vector3(Random.Range(-250, 250), 300.0f, 0.0f), Random.Range(-20, 20), Random.Range(-30, -60));
	}

	private static void SpawnMoveMine ()
	{
		Mine mine = KJActivePool.GetNewOf<Mine>("Mine");
		mine.Deploy( new Vector3(Random.Range(-250, 250), 300.0f, 0.0f), Random.Range(-200, 200), Random.Range(-200, -300), 0.5f);
	}

	private static PlatformWall platformWall = null;

	private static void SpawnWall ()
	{

		float insertY = -300.0f;

		if (platformWall != null) insertY = platformWall.transform.position.y;

		platformWall = KJActivePool.GetNewOf<PlatformWall>("PlatformWall");
		platformWall.Deploy(1, insertY);

		platformWall = KJActivePool.GetNewOf<PlatformWall>("PlatformWall");
		platformWall.Deploy(-1, insertY);




	}
	public static List<PlatformPattern> platformPatterns = new List<PlatformPattern>();
	public static List<PlatformPattern> platformList = new List<PlatformPattern>();
	public static bool hasPlatformList = false;
	public static int platformIndex = 0;

	private static void SpawnPlatform ()
	{
		if (!hasPlatformList) { 
			platformIndex = 0;
			platformPatterns = new List<PlatformPattern>(PlatformPattern.allPatterns);
			int count = platformPatterns.Count;
			for (int i = 0; i < count; i++) {
				platformList.Add(KJDice.RandomMemberOf<PlatformPattern>(platformPatterns, true));
			}
			hasPlatformList = true;
		}

		PlatformPattern pattern = platformList[platformIndex];

		//pattern = PlatformPattern.slow5_2; //Test

		foreach(PlatformData platformData in pattern.platformData) {
			Platform platform = KJActivePool.GetNewOf<Platform>("Platform");
			platform.Deploy(platformData);
		}
		platformIndex++;
	}

	static int rockCount = 0;
	private static void SpawnRock ()
	{



		Rock rock = KJActivePool.GetNewOf<Rock>("Rock");
		if (type != Type.BrutalRock) {
			rock.Deploy( new Vector3(Random.Range(-275, 275), 300.0f, 0.0f), Random.Range(-150, 150), Random.Range(-220, -300), 0.5f, false);
		} else {

			if (rockCount % 4 != 0) {
				rock.Deploy( new Vector3(Random.Range(-400, 400), 300.0f, 0.0f), Random.Range(-200, 200), Random.Range(-350, -570), 0.5f);
			} else {
				rock.Deploy( new Vector3(Random.Range(-300, 300), 300.0f, 0.0f), Random.Range(-15, 15), Random.Range(-30, -50), 0.5f, false);
			}

			if (rockCount == 4) {
				Rock rock2 = KJActivePool.GetNewOf<Rock>("Rock");
				Rock rock3 = KJActivePool.GetNewOf<Rock>("Rock");
				rock2.Deploy( new Vector3(Random.Range(200, 300), 300.0f, 0.0f), Random.Range(0, 100), Random.Range(-450, -450), 0.5f);
				rock3.Deploy( new Vector3(Random.Range(-200, -300), 300.0f, 0.0f), Random.Range(0, -100), Random.Range(-450, -450), 0.5f);
				rockCount = 0;
			}
			rockCount++;
		}
	}

	static float offsetMine = 0.0f;
	static int caseMine = 3;
	static Vector3 lastPositionMineLeft = new Vector3(-280.0f, 0);
	static Vector3 lastPositionMineRight = new Vector3(280.0f, 0);
	static int directMineCount = 0;
	static int iniMineCount = 12;
	private static void SpawnBrutalMineTime ()
	{
		switch (caseMine) { 
		case 1 :
		{
			Mine mineLeft = KJActivePool.GetNewOf<Mine>("Mine");
			Mine mineRight = KJActivePool.GetNewOf<Mine>("Mine");
			
			Vector3 positionLeft = new Vector3((Mathf.Sin(offsetMine) * 280.0f) - 100.0f, 360.0f);
			mineLeft.Deploy(positionLeft, 0.0f, -275.0f);
			
			Vector3 positionRight = new Vector3((Mathf.Sin(offsetMine) * 280.0f) + 100.0f, 360.0f);
			mineRight.Deploy(positionRight, 0.0f, -275.0f);

			offsetMine += 0.15f;
			offsetMine += KJDice.NextValue * 0.03f;
			//Debug.Log(offsetMine);

			if (KJDice.Roll(2)) {
				lastPositionMineLeft = positionLeft;
				lastPositionMineRight = positionRight;
				caseMine = 2;
				directMineCount = (int)((KJDice.NextValue * 15.0f) + 10.0f);
			}
		}
		break;
		case 2: 
		{
			Mine mineLeft = KJActivePool.GetNewOf<Mine>("Mine");
			Mine mineRight = KJActivePool.GetNewOf<Mine>("Mine");
			Vector3 positionLeft = new Vector3(lastPositionMineLeft.x + 40.0f, lastPositionMineLeft.y);
			mineLeft.Deploy(positionLeft, 0.0f, -275.0f);
			Vector3 positionRight = new Vector3(lastPositionMineRight.x - 40.0f, lastPositionMineRight.y);
			mineRight.Deploy(positionRight, 0.0f, -275.0f);
			directMineCount--;
			if (directMineCount == 0) {
				caseMine = 1;
			}
		}
		break;
		case 3:
		{
			Mine mineLeft = KJActivePool.GetNewOf<Mine>("Mine");
			Mine mineRight = KJActivePool.GetNewOf<Mine>("Mine");
			Vector3 positionLeft = new Vector3(lastPositionMineLeft.x, 360.0f);
			mineLeft.Deploy(positionLeft, 0.0f, -275.0f);
			Vector3 positionRight = new Vector3(lastPositionMineRight.x, 360.0f);
			mineRight.Deploy(positionRight, 0.0f, -275.0f);

			lastPositionMineLeft.x += 17.5f;
			lastPositionMineRight.x -= 17.5f;

			iniMineCount--;
			if (iniMineCount == 0) {
				lastPositionMineLeft = positionLeft;
				lastPositionMineRight = positionRight;
				directMineCount = (int)((KJDice.NextValue * 15.0f) + 10.0f);
				caseMine = 2;
			}
		}
		break;


		}
	}

	private static void SpawnBrutalMine() {
		KJTime.Add(SpawnBrutalMineTime, 0.10f);
	}

	private static void SpawnCargo ()
	{
		if (ProfileController.SubSector != ProfileController.MaxSubSectors) {
			Cargo cargo = KJActivePool.GetNewOf<Cargo>("Cargo");
			cargo.Deploy();
		}
	}


	private static void SpawnLineMines ()
	{
		Vector3 startPosition = new Vector3(- 360.0f, 700.0f);
		Vector3 endPosition = new Vector3(360.0f, 300.0f);
		float gap = 80.0f;
		int maxAmount = (int) (Vector3.Distance(startPosition, endPosition) / gap);
		int amount = maxAmount;

		switch (Random.Range(1,4)) {
		case 1: 
			startPosition = new Vector3(- 360.0f, 700.0f);
			endPosition = new Vector3(360.0f, 300.0f);
			break;

		case 2: 
			startPosition = new Vector3(- 360.0f, 300.0f);
			endPosition = new Vector3(360.0f, 700.0f);
			break;

		case 3: 
			startPosition = new Vector3(- 360.0f, 700.0f);
			endPosition = new Vector3(360.0f, 700.0f);
			break;
		
		default:
			startPosition = new Vector3(0.0f, 700.0f);
			endPosition = new Vector3(0.0f, 250.0f);
			gap = 60.0f;
			break;

		}

		while (amount > 0)
		{
			Mine mine = KJActivePool.GetNewOf<Mine>("Mine");
			Vector3 newPosition = Vector3.Lerp(startPosition, endPosition, amount / (float) maxAmount);
			mine.Deploy( newPosition, 0.0f, -40.0f);
			amount --;
		}
	}

	public static void BurstSpawnSubSquad (int approachDir)
	{

		if (!waveIsRunning) return;

		int oppositeApproach = approachDir;
		waveDataSingle.Calibrate(0.0f, false, false);
		waveDataSingle.maneuver = SquadManeuver.SUPPORT;
		waveDataSingle.size = 2;
		waveDataSingle.OverridePatterns(EnemyPattern.Support);
		
		GameObject squadObject = new GameObject();
		Squad squad = squadObject.AddComponent<Squad>();
		squad.Deploy(waveDataSingle.maneuver, SquadFormation.Type.Line, waveDataSingle.size, waveDataSingle.unitPatterns, oppositeApproach);
	}

	public static void BurstSpawnBossSquad (int approachDir)
	{
		
		if (!waveIsRunning) return;
		
		int oppositeApproach = approachDir;
		waveDataSingle.Calibrate(0.0f, false, false, true);
		waveDataSingle.maneuver = SquadManeuver.SUPPORT;
		waveDataSingle.size = 3;
//		waveDataSingle.OverridePatterns(EnemyPattern.);
		
		GameObject squadObject = new GameObject();
		Squad squad = squadObject.AddComponent<Squad>();
		squad.Deploy(waveDataSingle.maneuver, SquadFormation.Type.Wedge, waveDataSingle.size, waveDataSingle.unitPatterns, oppositeApproach);
	}
	
	private static void BurstSpawnGems ()	
	{
		int amount = (int) (KJMath.Cap((Level + 100.0f) / 50.0f, 2.0f, 3.0f));// Start getting 1 gem. Soon, get 4 gems, then quickly after get 9. 
		float gemOffsetValue = 100.0f;
		RewardController.SpawnRewards(0.0f, true, amount, gemOffsetValue);
		//lastSpawnWasGem = true;
	}
	
	private static void SpawnSectorBoss ()
	{
		if (ProfileController.Sector.story_PreBoss != -1) {
			ProfileController.Sector.DeployPreBossStory();
			KJTime.Add(DeploySectorBossActual, 1.0f, 1);
		} else {
			KJTime.Add(DeploySectorBossActual, 5.0f, 1);
		}
	}

	private static void DeploySectorBossActual () {
		UIController.ShowHeader("WARNING!", "BOSS INCOMING", GameController.Singleton.DeploySectorBoss, false, CombatHeader.ColorType.Red);
		KJCanary.Singleton.SwitchToRendered();
	}
	
	private static void GetWavePatternModifier ()
	{
		
		switch (waveMode)
		{
			
			case WaveData.Mode.REWARD:
				waveMod = 0.0f;
				break;
			
			case WaveData.Mode.LIGHT:
				waveMod = 1.0f;
				break;
			
			case WaveData.Mode.MEDIUM:
				waveMod = 2.0f;
				break;
			
			case WaveData.Mode.HEAVY:
				waveMod = 3.0f;
				break;
		}
	}

	private static void SetEliteCountForType ()
	{
		eliteDualSquad = false;

		switch (type)
		{

		case Type.Balanced:
			eliteLimitMin = 1;
			eliteLimitMax = 2;
			break;

		case Type.BulletHell:
			eliteLimitMin = 0;
			eliteLimitMax = 1;
			break;

		case Type.AoE:
			eliteLimitMin = 0;
			eliteLimitMax = 1;
			break;

		case Type.Mines:
			eliteLimitMin = 0;
			eliteLimitMax = 1;
			break;

		case Type.BeamHell:
			eliteLimitMin = 0;
			eliteLimitMax = 1;
			break;

		case Type.Homing:
			eliteLimitMin = 0;
			eliteLimitMax = 1;
			break;
			
//		    case Type.Fleet:
//			eliteLimitMin = 0;
//			eliteLimitMax = 1;
//			break;

		case Type.Elite:
			eliteLimitMin = 4;
			eliteLimitMax = 4;
			eliteDualSquad = true;
			break;
		
		case Type.Quest_Bonus:
			eliteLimitMin = 4;//elite test
			eliteLimitMax = 4;
			break;

		case Type.Quest_Boss:
			eliteLimitMin = 2;
			eliteLimitMax = 2;
			break;

		case Type.Raid:
			eliteLimitMin = 0;
			eliteLimitMax = 0;
			break;
			
		case Type.Boss:
			eliteLimitMin = 0;
			eliteLimitMax = 0;
			break;

		default :
			eliteLimitMin = 0;
			eliteLimitMax = 1;
			break;
		}

		if (eliteLimitMin == eliteLimitMax) {
			eliteLimitCount = eliteLimitMax;
		} else {
			eliteLimitCount = KJDice.SeededRange(eliteLimitMin, eliteLimitMax + 1);
		}

		// No Elites in the first two stages.
		if (ProfileController.SectorId == 1 && ProfileController.SubSector < 3)
			eliteLimitCount = 0;
	}

	private static bool IsEliteSquad
	{
		get {

			if (waveCount != 0) return false;
			if (eliteLimitCount == 0) return false;

			int totalWaveCount = maxWaves;
			if (eliteLimitCount >= totalWaveCount)
			{
				return true;
			} else {
				int spawnChance = (int) (100.0f * (eliteLimitCount / totalWaveCount));
				return KJDice.Roll(spawnChance);
			}

		}
	}

	private static void SetMaxWavesForType ()
	{

		maxWaves = 2 + (int) (ProfileController.SubSector / 2);
		if (maxWaves > 4) maxWaves = 4;

		switch (type)
		{

		case Type.BulletHell:
			maxWaves += 1;
			break;
		
		case Type.BeamHell:
			maxWaves += 1;
			break;
		
		case Type.AoE:
			maxWaves += 1;
			break;
		
		case Type.Homing:
			maxWaves += 1;
			break;
		
		case Type.Mines:
			maxWaves += 1;
			break;

		case Type.Quest_Bonus:
			maxWaves += 1;
			break;

		case Type.Quest_Boss:
			maxWaves = 4;//elite test
			break;

		case Type.Boss:
			maxWaves = 2;
			break;

		case Type.Raid:
			maxWaves = 0;
			break;

		default:
			break;

		}
	}

	
	public static void SetWaveCount ()
	{
		// How many squads are there in a wave?
		
		switch (type)
		{
			
		case Type.Elite:
			waveCount = 4;
			break;

		case Type.BulletHell:
			waveCount = 5;
			break;
		
		case Type.BeamHell:
			waveCount = 5;
			break;
		
		case Type.Homing:
			waveCount = 5;
			break;

		case Type.AoE:
			waveCount = 4;
			break;
		
		case Type.Mines:
			waveCount = 5;
			break;	

		case Type.Quest_Bonus:
			waveCount = 5;//elite test
			break;	

		case Type.Raid:
			waveCount = 0;
			break;	


		default:
			waveCount = 4;
			break;
			
		}

		GenerateSubWaves (waveCount);
	}

	public static void SetSpawnTimers (bool burstExecute = true)
	{
		standardWaveGap = 1.00f;
		singleSquadGap = standardWaveGap * 3.7f;
		singleMineGap = standardWaveGap * 3.7f;

		float burstMineGap = 0.0f;

		switch (type)
		{
		
		case Type.Balanced:
			singleRockGap = 2.5f;
			break;

		case Type.BulletHell:
			standardWaveGap = 1.2f;
			singleMineGap = 0.0f;
			singleRockGap = 2.25f;
			break;

		case Type.BeamHell:
			standardWaveGap = 0.9f;
			singleMineGap = 0.0f;
			singleRockGap = 1.5f;
			break;
			
		case Type.Mines:
			singleMineGap = 0.0f;
			burstMineGap = 1.5f;
			singleRockGap = 1.0f;
			break;

		case Type.Quest_Bonus :
			singleRockGap = 0.0f;
			singleMineGap = 0.0f;
			//singleMineGap = 1.5f;
			break;

		case Type.Quest_Boss:
			singleRockGap = 0.0f;
			singleMineGap = 0.0f;
			break;

		case Type.Raid:
			singleRockGap = 0.0f;
			singleMineGap = 0.0f;
			break;
			
		case Type.Boss:
			singleRockGap = 0.0f;
			singleMineGap = 0.0f;
			break;

		default:
			standardWaveGap = 1.0f;
			singleRockGap = 2.25f;
			break;
			
		}

		if (Level < 3) {

			singleMineGap = 0.0f;
			standardWaveGap *= 1.35f;
			singleRockGap *= 5.0f;
			singleSquadGap = standardWaveGap * 3.7f;

		} else if (Level < 7) {

			singleMineGap = 0.0f;
			singleRockGap *= 3.0f;

		} else if (Level < 12) {

			singleMineGap *= 1.75f;
			singleRockGap *= 1.75f;

		} else if (Level < 20) {

			singleMineGap *= 1.25f;
			singleRockGap *= 1.25f;

		}

		if (NetworkController.Singleton.isMultiplayer) {
			standardWaveGap -= 0.15f;
		}

		if (type != Type.BrutalRock && type != Type.BrutalMine && type != Type.Platform) 
		KJTime.Add(Run, standardWaveGap);


		if (type == Type.BrutalRock) {
			KJTime.Add(EndGame, 60.0f, 1);
			if (Level < 5) {
				singleRockGap = 2.0f;
			} else if (Level < 10) {
				singleRockGap = 1.6f;
			} else if (Level < 15) {
				singleRockGap = 1.3f;
			} else {
				singleRockGap = 1.00f;
			}
			singleRockGap *= 0.1f;
			singleMineGap = 0.0f;
		}

		if (type == Type.BrutalMine) {
			KJTime.Add(EndGame, 60.0f, 1);
			singleRockGap = 0.0f;
			singleMineGap = 0.0f;
			SpawnBrutalMine();
		}

		if (type == Type.Platform) {
			singleRockGap = 0.0f;
			singleMineGap = 0.0f;
			KJTime.Add(EndPlatform, 66.0f, 1);
			SpawnPlatform();
			KJTime.Add(SpawnPlatform, 11.0f);
			KJTime.Add(SpawnWall, 4.5f);
		}

		if (KJDice.Roll(15) && type != Type.BrutalMine && type != Type.BrutalRock && type != Type.Platform && type != Type.Quest_Bonus && type != Type.Quest_Boss && type != Type.Raid)
		{
			if (ProfileController.SubSector > 1 || ProfileController.SectorId > 1)
				SpawnCargo();
		}

		if (ProfileController.SectorId > 1 || ProfileController.SubSector > 1) {
			// Do not spawn rocks til 2nd SubSector.
			if (singleRockGap != 0.0f) KJTime.Add(SpawnRock, singleRockGap);
		}

		if (singleMineGap != 0.0f) KJTime.Add(SpawnSingleMine, singleMineGap);

		if (burstMineGap != 0.0f) {
			KJTime.Add (SpawnLineMines, burstMineGap, 1);
			//KJTime.Add (SpawnMoveMine, 1.0f);
		}

		if (burstExecute) Run ();
	}

	// Whether we can spawn the Rival instead of the elite.
	public static bool CanSpawnRival {

		get {
			int minRivalSector = 81;
			int maxRivalSector = 90;
			if (ProfileController.SectorsUnlocked > minRivalSector && ProfileController.SectorsUnlocked < maxRivalSector)
				return true;
			else
				return false;
		}
	}

	public static void EndPlatform() {
		hasPlatformList = false;
		KJTime.Remove(SpawnPlatform);
		KJTime.Add(EndGame, 10.0f, 1);
	}

	public static void EndGame() {
		waveIsRunning = false;
		KJTime.Remove(SpawnBrutalMineTime);
		KJTime.Remove(SpawnRock);
		KJCanary.Singleton.SwitchToAmbience();
		ClearSpawnTimers();
		GameController.WinGame();
	}
}

