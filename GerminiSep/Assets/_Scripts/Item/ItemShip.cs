using UnityEngine;
using System.Collections;

public class ItemShip : ItemBasic
{

	public ItemShip (string name, ItemBasic.Rarity rarity, int cost) :
		base (1, 0, 0, rarity, cost, -1, true, name)
	{
		weapon = WeaponPattern.StandardYellow;
		id = currentId;
		currentId ++;
		moduleId = 0;
	}

	public Weapon weapon;
	public string shipDescription = "Ship Description Here";
	public string specialDescription = "";


	private static int currentId = 0;

	
	/// Pro-Gen Section: Methods to help with the random generation of items.
	
	public static void ResetGlobalId () { currentId = 0; }
}

