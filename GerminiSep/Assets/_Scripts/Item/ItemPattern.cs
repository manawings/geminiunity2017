using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemPattern
{
	
	private static List<ItemShip> _ShipList;
	public static List<ItemShip> ShipList
	{
		get {
			if (!hasBeenInit)
			{
				Initialize();	
			}
			
			return _ShipList;
		}
	}

	private static ItemShip newShip;
	
	private static bool hasBeenInit = false;
	
	public static void Initialize ()
	{
		
		hasBeenInit = true;

		Color startBlue = new Color (0.2f, 0.8f, 1.0f, 1.0f);
		Color endBlue = new Color (0.0f, 0.1f, 0.7f, 1.0f);

		Color startOrange = new Color (1.0f, 0.8f, 0.0f, 1.0f);
		Color endOrange = new Color (1.0f, 0.0f, 0.0f, 1.0f);

		Color startPink = new Color (0.7f, 0.2f, 1.0f, 1.0f);
		Color endPink = new Color (0.3f, 0.3f, 0.8f, 1.0f);

		Color startGreen= new Color (0.7f, 1.0f, 0.2f, 1.0f);
		Color endGreen = new Color (0.0f, 0.6f, 0.8f, 1.0f);

		Color startRed= new Color (1.0f, 0.8f, 0.6f, 1.0f);
		Color endRed = new Color (1.0f, 0.2f, 0.5f, 1.0f);

		_ShipList = new List<ItemShip>();

		AddShip("Strike Eagle", "Fighter", ItemBasic.Rarity.Common, 5, "ps_Standard", 0, startBlue, endBlue, WeaponPattern.StandardYellow);
		newShip.shipDescription = "Due to its combat efficiency and cheap production costs, the Strike Eagle has earned its place as the mainstay Fighter of the Alliance fleet. \n\n It is equipped with dual blaster cannons which fire in alternating frequency.";


		AddShip("Hunt Viper", "Fighter", ItemBasic.Rarity.Common, 7, "ps_StandardFett", 304, startBlue, endBlue, WeaponPattern.StandardYellow); //missile green 5s
		newShip.shipDescription = "This modded version of the Strike Eagle features increased offensive capabilities, and is a popular choice amongst seasoned pilots.";
		newShip.specialDescription = "Fires Hunter Missiles every 5 seconds. Also grants 5% bonus to damage.";


		AddShip("Flame Tiger", "Fighter", ItemBasic.Rarity.Uncommon, 15, "ps_StandardEmber", 305, startBlue, endBlue, WeaponPattern.StandardYellow); //missile burn 5s
		newShip.shipDescription = "With the addition of Pyro-Foam missiles, the Ash Tiger was designed to burn down elite and evasive targets.";
		newShip.specialDescription = "Fires Pyro-Foam Missiles every 5 seconds. Also grants 5% bonus to damage and HP.";


		AddShip("Shock Phantom", "Fighter", ItemBasic.Rarity.Rare, 25, "ps_StandardSteam", 306, startBlue, endBlue, WeaponPattern.StandardYellow); //missile twin stun 5s
		newShip.shipDescription = "This is the most advanced unit in its class. It is equipped with EMP missiles to lock down the weapon systems of its targets.";
		newShip.specialDescription = "Fires EMP Missiles every 4 seconds. Also grants 15% bonus to damage and HP.";


		AddShip("Shizuka", "Sakura", ItemBasic.Rarity.Common, 7, "ps_SakuraPink", 307, startPink, endPink, WeaponPattern.StandardGreen); //standard fast atk / damage 70% //sonar*5 8s 
		newShip.shipDescription = "A fragile ship that packs a deadly punch. The key to piloting a Sakura is the timing of its powerful Sonar Blast.";
		newShip.specialDescription = "Fires Sonic Blast every 8 seconds. Also grants 5% bonus to damage.";


		AddShip("Banshou", "Sakura", ItemBasic.Rarity.Uncommon, 15, "ps_SakuraEmber", 308, startPink, endPink, WeaponPattern.StandardGreen); //fast fire 70% //sonar*5 6s 
		newShip.shipDescription = "This customized version of the Sakura is commonly used by pirate raiders because of its powerful weapons and manly red color.";
		newShip.specialDescription =  "Fires Sonic Blast every 6 seconds. Also grants 15% bonus to damage.";


		AddShip("Raikou", "Sakura", ItemBasic.Rarity.Rare, 25, "ps_SakuraJade", 309, startPink, endPink, WeaponPattern.StandardGreen); //fast fire 70% //sonar*5 4s 
		newShip.shipDescription = "The original Sakura prototype is equipped with the fastest Sonar Blaster. It is capable of the highest sustained damage output of all the known ships.";
		newShip.specialDescription =  "Fires Sonic Blast every 4 seconds. Also grants 30% bonus to damage.";


		AddShip("Azura", "Artemis", ItemBasic.Rarity.Common, 7, "ps_ArtemisBlue", 310, startOrange, endOrange, WeaponPattern.StandardBlue); //scatter green
		newShip.shipDescription = "An older Alliance model equipped with a scatter weapon designed to combat enemies flying in formation.";
		newShip.specialDescription = "Fires Scatter Shot every 5 seconds. Also grants 5% bonus to HP.";


		AddShip("Verdira", "Artemis",ItemBasic.Rarity.Common, 7, "ps_ArtemisGreen", 311, startOrange, endOrange, WeaponPattern.StandardBlue); //scatter green
		newShip.shipDescription = "A variant of the Azura, favouring thicker armor over the the former's superior life support system.";
		newShip.specialDescription = "Fires Scatter Shot every 5 seconds. Also grants 5% bonus to armor.";


		AddShip("Emporia", "Artemis",ItemBasic.Rarity.Uncommon, 15, "ps_ArtemisOrange", 312, startOrange, endOrange, WeaponPattern.StandardBlue); //scatter twin green 
		newShip.shipDescription = "An advanced Artemis vessel, combining the strengths of both the Azura and Verdira.";
		newShip.specialDescription = "Fires Scatter Shot every 5 seconds. Also grants 5% bonus to armor and HP.";


		AddShip("Evira", "Artemis", ItemBasic.Rarity.Rare, 25, "ps_ArtemisEva", 313, startOrange, endOrange, WeaponPattern.StandardBlue); //scatter twin green
		newShip.shipDescription = "The Evira was the original Artemis prototype, built in repsonse to the Third Great Disaster.";
		newShip.specialDescription = "Fires Scatter Shot every 4 seconds. Also grants 15% bonus to armor and HP.";


		AddShip("Hannover", "Storm", ItemBasic.Rarity.Common, 7, "ps_CloudGreen", 314, startGreen, endGreen, WeaponPattern.StandardRound); //standard round slow fire //chain slow color
		newShip.shipDescription = "The Storms were originally industrial ships, but have been modified for combat.";
		newShip.specialDescription = "Fires Electro Chain. Also grants 5% bonus to armor.";


		AddShip("Leipzig", "Storm", ItemBasic.Rarity.Common, 7, "ps_CloudOrange", 315, startGreen, endGreen, WeaponPattern.StandardRound); //standard round slow fire //chain slow color
		newShip.shipDescription = "The Storms were originally industrial ships, but have been modified for combat.";
		newShip.specialDescription = "Fires Electro Chain. Also grants 5% bonus to HP.";


		AddShip("Ravenstein", "Storm", ItemBasic.Rarity.Common, 7, "ps_CloudRed", 316, startGreen, endGreen, WeaponPattern.StandardRound); //standard round slow fire //chain slow color
		newShip.shipDescription = "The Storms were originally industrial ships, but have been modified for combat.";
		newShip.specialDescription = "Fires Electro Chain. Also grants 5% bonus to damage";


		AddShip("Siegfried", "Storm", ItemBasic.Rarity.Uncommon, 15, "ps_CloudGrey", 317, startGreen, endGreen, WeaponPattern.StandardRound); //twin chainnewShip.shipDescription = "Chain Weapon";
		newShip.shipDescription = "This pirate variant of the Leipzig is fitted with an additional Chainer and thicker armor.";
		newShip.specialDescription = "Fires Electro Chain. Also grants 5% bonus to armor and HP";



		AddShip("Fellbach", "Storm", ItemBasic.Rarity.Rare, 25, "ps_CloudSilver", 318, startGreen, endGreen, WeaponPattern.StandardRound); //twin chain
		newShip.shipDescription = "The Fellbach is the newest generation of Storm class. It is fitted with two Chainers instead of one, and features vast hull improvements.";
		newShip.specialDescription = "Fires Dual Electro Chain. Also grants 10% bonus to all stats.";



		AddShip("Sapphire Sting", "Corsair", ItemBasic.Rarity.Uncommon, 15, "ps_CorsairBlue", 319, startBlue, endBlue, WeaponPattern.StandardSonar_1); //standard sonar stun slow atk high damage //Block 1 hit 
		newShip.shipDescription = "The design of the Corsair is based on the Imperial Striker. Its weapon is slower, but more powerful.";
		newShip.specialDescription = "Gains Auto-Hex Shield, absorbing one hit every 20 seconds. Also grants 15% bonus to HP.";


		AddShip("Golden Claw", "Corsair",ItemBasic.Rarity.Uncommon, 15, "ps_CorsairYellow", 320, startBlue, endBlue, WeaponPattern.StandardSonar_2); //standard sonar fast atk normal damage //Block 1 hit
		newShip.shipDescription = "The design of the Corsair is based on the Imperial Striker. Its weapon is slower, but more powerful.";
		newShip.specialDescription = "Gains Auto-Hex Shield, absorbing one hit every 20 seconds. Also grants 15% bonus to damage.";



		AddShip("Silver Fang", "Corsair", ItemBasic.Rarity.Uncommon, 15, "ps_CorsairSilver", 321, startBlue, endBlue, WeaponPattern.StandardSonar_3); //standard sonar pink pierce slow high damage //Block 1 hit
		newShip.shipDescription = "The design of the Corsair is based on the Imperial Striker. Its weapon is slower, but more powerful.";
		newShip.specialDescription = "Gains Auto-Hex Shield, absorbing one hit every 20 seconds. Also grants 15% bonus to armor.";



		AddShip("Caspar", "Eidolon", ItemBasic.Rarity.Uncommon, 20, "ps_EidolonGreen", 322, startOrange, endOrange, WeaponPattern.StandardSweep); //standard round scatter//Laser Charge high pierce damage*2
		newShip.shipDescription = "An Alliance ship designed for fleet combat. It is optimized for fighting groups of smaller enemies, but may struggle against single targets.";
		newShip.specialDescription = "Fires a Laser Blast every 4 seconds. Also grants 10% bonus to armor and damage.";



		AddShip("Balthazar", "Eidolon",ItemBasic.Rarity.Uncommon, 20, "ps_EidolonOrange", 323, startOrange, endOrange, WeaponPattern.StandardSweep);
		newShip.shipDescription = "An Alliance ship designed for fleet combat. It is optimized for fighting groups of smaller enemies, but may struggle against single targets.";
		newShip.specialDescription = "Fires a Laser Blast every 4 seconds. Also grants 10% bonus to armor and HP.";



		AddShip("Melchior", "Eidolon",ItemBasic.Rarity.Uncommon, 20, "ps_EidolonSilver", 324, startOrange, endOrange, WeaponPattern.StandardSweep);
		newShip.shipDescription = "An Alliance ship designed for fleet combat. It is optimized for fighting groups of smaller enemies, but may struggle against single targets.";
		newShip.specialDescription = "Fires a Laser Blast every 4 seconds. Also grants 10% bonus to HP and damage.";



		AddShip("Bjorn", "Odin", ItemBasic.Rarity.Uncommon, 15, "ps_MaelstromSilver", 325, startPink, endPink, WeaponPattern.StandardShock); //standard chain homing // shield 5sec
		newShip.shipDescription = "The Odin is a result of a peacetime collaboration between the Alliance and the Empire.";
		newShip.specialDescription = "When HP drops below 15%, the Odin Shield is activated and blocks all damage for 6 seconds. Also grants 15% bonus to HP and damage.";

		AddShip("Anvindr", "Odin", ItemBasic.Rarity.Rare, 35, "ps_MaelstromPurple", 326, startPink, endPink, WeaponPattern.StandardShock); //standard chain homing // shield 10sec
		newShip.shipDescription = "The Odin is a result of a peacetime collaboration between the Alliance and the Empire.";
		newShip.specialDescription = "When HP drops below 15%, the Odin Shield is activated and blocks all damage for 12 seconds. Also grants 20% bonus to HP and damage.";



		//index 25

		AddShip("Mantis", "Slayer", ItemBasic.Rarity.Rare, 30, "ps_Mantis", 328, startOrange, endOrange, WeaponPattern.StandardRocket);
		newShip.shipDescription = "The Slayer class is equipped exclusively with missile weapons. Slow to fire, but extremely devastating.";
		newShip.specialDescription = "Fires twin Plasma missiles every 6 seconds. Also grants bonus 10% to armor and 25% to damage.";

		AddShip("Stingray", "Guardian", ItemBasic.Rarity.Rare, 30, "ps_Sting", 331, startGreen, endGreen, WeaponPattern.StandardZarall);
		newShip.shipDescription = "A defensive ship with a shot-gun style weapon that helps it deal with bigger swarms of enemies.";
		newShip.specialDescription = "Automatically repairs 5% HP every 7 seconds. Also grants 20% bonus to HP and armor.";

		AddShip("Violet Wolf", "Fighter", ItemBasic.Rarity.Rare, 30, "ps_StandardPurple", 329, startPink, endBlue, WeaponPattern.StandardPink);
		newShip.shipDescription = "An experimental Fighter infused with Plasma energy. It overloads its energy to damage nearby enemy ships.";
		newShip.specialDescription = "Automatically fires twin Plasma chains every 5 seconds. Also grants bonus 10% to armor and 25% to damage.";

		AddShip("Sangara", "Artemis", ItemBasic.Rarity.Rare, 30, "ps_ArtemisExtra", 332, startOrange, endOrange, WeaponPattern.StandardPink);
		newShip.shipDescription = "The Sangara is a noble and powerful vessel that is strong in both attack and defense.";
		newShip.specialDescription = "Automatically repairs 6% HP every 8 seconds. Also grants 20% bonus to HP and armor.";

		// Special Ships

		AddShip("Cruel Angel", "Fighter", ItemBasic.Rarity.Rare, 3, "ps_Energy", 335, startOrange, endOrange, WeaponPattern.StandardEnergy);
		newShip.isSpecialShip = true;
		newShip.shipDescription = "An Alliance Fighter infused with CORE energy - the most efficient energy source in the galaxy.";
		newShip.specialDescription = "When HP drops below 15%, an Energy Shield is activated and blocks all damage for 12 seconds. Also grants 20% bonus to HP and damage.";

		AddShip("Akama", "Sakura", ItemBasic.Rarity.Rare, 3, "ps_Energy2", 327, startGreen, endGreen, WeaponPattern.SakuraEnergy);
		newShip.isSpecialShip = true;
		newShip.shipDescription = "The added core-energy shields give the Sakura its much needed survivability.";
		newShip.specialDescription = "When HP drops below 15%, an Energy Shield is activated and blocks all damage for 12 seconds. Also grants bonus 10% to HP and 25% to damage.";
		
		AddShip("Diamond Crown", "Corsair", ItemBasic.Rarity.Rare, 3, "ps_Energy3", 333, startOrange, endOrange, WeaponPattern.StandardEnergy);
		newShip.isSpecialShip = true;
		newShip.shipDescription = "A core-powered Corsair, modded with improved fighter weapons and seeker missiles.";
		newShip.specialDescription = "Fires twin Plasma missiles every 6 seconds. Also grants bonus 15% to damage and 20% to armor.";

		AddShip("Anubis", "Eidolon", ItemBasic.Rarity.Rare, 3, "ps_Energy4", 334, startPink, endPink, WeaponPattern.EidolonEnergy);
		newShip.isSpecialShip = true;
		newShip.shipDescription = "A core-powered Eidolon, modded with improved Artemis weapons and scattering lasers.";
		newShip.specialDescription = "Fires Omega lasers every 6 seconds. Also grants bonus 15% to damage and 20% to armor.";
		
//		AddShip("Z.V. Striker", ItemBasic.Rarity.Common, 1, "p_striker", 305, new Color(1.0f, 0.8f, 0.0f, 1.0f), new Color(7.0f, 0.0f, 0.0f, 1.0f), WeaponPattern.p_striker);
//		
//		AddShip("Red Baron", ItemBasic.Rarity.Common, 1, "p_lancer", 304, new Color(1.0f, 0.8f, 0.0f, 1.0f), new Color(7.0f, 0.0f, 0.0f, 1.0f), WeaponPattern.p_lancer);
//		
//		AddShip("Sky Crystal", ItemBasic.Rarity.Common, 1, "p_crystal", 314, new Color(1.0f, 0.8f, 0.0f, 1.0f), new Color(7.0f, 0.0f, 0.0f, 1.0f), WeaponPattern.p_crystal);
//		
//		AddShip("Imperial", ItemBasic.Rarity.Rare, 3, "p_white", 306, new Color(1.0f, 0.8f, 0.0f, 1.0f), new Color(7.0f, 0.0f, 0.0f, 1.0f), WeaponPattern.StandardBlue);
//		
//		AddShip("Void Tracer", ItemBasic.Rarity.Rare, 5, "p_void", 307, new Color(0.7f, 0.2f, 1.0f, 1.0f), new Color(0.3f, 0.0f, 0.8f, 1.0f), WeaponPattern.StandardPink);
//	
//		AddShip("Classified", ItemBasic.Rarity.Common, 5, "p_gold", 308, new Color(1.0f, 0.8f, 0.0f, 1.0f), new Color(7.0f, 0.0f, 0.0f, 1.0f), WeaponPattern.p_gold);
//
//		AddShip("Classified 2", ItemBasic.Rarity.Common, 5, "p_gold", 308, new Color(1.0f, 0.8f, 0.0f, 1.0f), new Color(7.0f, 0.0f, 0.0f, 1.0f), WeaponPattern.StandardYellow);
//
//		AddShip("Classified 3", ItemBasic.Rarity.Common, 5, "p_gold", 308, new Color(1.0f, 0.8f, 0.0f, 1.0f), new Color(7.0f, 0.0f, 0.0f, 1.0f), WeaponPattern.StandardYellow);

		
	}
	
	private static void AddShip (string name, string shipType, ItemBasic.Rarity rarity, int cost, string shipClip, int moduleId, Color startColor, Color endColor, Weapon weapon = null)
	{
		newShip = new ItemShip(name, rarity, cost);
		if (weapon != null) newShip.weapon = weapon;
		newShip.shipType = shipType;
		newShip.moduleId = moduleId;
		newShip.SetThings(shipClip, startColor, endColor);
		_ShipList.Add(newShip);
	}
}

