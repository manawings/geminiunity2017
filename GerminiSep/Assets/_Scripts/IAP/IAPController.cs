﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;
using UnityEngine.Purchasing;
using System;
using UnityEngine.Purchasing.Extension;

public class IAPController : MonoBehaviour, IStoreListener, IStore {

	static List<StoreObject> _StoreList;
	static string dummyId = "";
	static bool restoring = false;
	static StoreObject currentStoreObject;
	string[] comsumableIDApple = {"HC1", "HC2", "HC3", "HC4", "HC5", "HC6"};
	string[] nonComsumableIDApple = {"GDX"};
	string[] comsumableIDGoogle = {"hc1", "hc2", "hc3", "hc4", "hc5", "hc6"};
	string[] nonComsumableIDGoogle = {"gdx"};
	private IStoreController controller;
	private IExtensionProvider extensions;

	private static GameObject parentObject;

	private static IAPController _Singleton;
	public static IAPController Singleton {
		get {
			if (!_Singleton)
			{
				parentObject = new GameObject("IAPController");
				DontDestroyOnLoad(parentObject);
				_Singleton = parentObject.AddComponent<IAPController>();
				//_Singleton.Init();
			}
			return _Singleton;
		}
	}

	void Start () {
		if (controller == null)
		{
			// Begin to configure our connection to Purchasing
			InitializePurchasing();
		}
	}

	public static List<StoreObject> StoreList {
		get {
			if (_StoreList == null) {
				_StoreList = new List<StoreObject>();
				#if UNITY_IOS
				_StoreList.Add ( new StoreObject("5 Credits", "HC2", "$2.99", false, 5)); // $3 -- 0.60    0
				_StoreList.Add ( new StoreObject("15 Credits", "HC6", "$7.99", false, 15)); // $8 -- 0.53    1
				_StoreList.Add ( new StoreObject("25 Credits", "HC3", "$12.99", false, 25)); // $12 -- 0.52   2
				_StoreList.Add ( new StoreObject("50 Credits", "HC4", "$19.99", false, 50)); // $20 -- 0.40   3
				_StoreList.Add ( new StoreObject("100 Credits", "HC5", "$34.99", false, 100)); // $35 -- 0.35   4
				_StoreList.Add ( new StoreObject("Gem Doubler", "GDX", "$4.99", true, 1)); // $5    5
				#endif
				#if UNITY_ANDROID
				_StoreList.Add ( new StoreObject("5 Credits", "hc2", "$2.99", false, 5)); // $3 -- 0.60    0
				_StoreList.Add ( new StoreObject("15 Credits", "hc6", "$7.99", false, 15)); // $8 -- 0.53    1
				_StoreList.Add ( new StoreObject("25 Credits", "hc3", "$12.99", false, 25)); // $12 -- 0.52   2
				_StoreList.Add ( new StoreObject("50 Credits", "hc4", "$19.99", false, 50)); // $20 -- 0.40   3
				_StoreList.Add ( new StoreObject("100 Credits", "hc5", "$34.99", false, 100)); // $35 -- 0.35   4
				_StoreList.Add ( new StoreObject("Gem Doubler", "gdx", "$4.99", true, 1)); // $5    5
				#endif

			}
			return _StoreList;
		}
	}
		
	public void RequestBuyItem (int id) {

//		if (!IsInitialized())
//		{
////			UIController.ShowError("IAP ERROR", "Purchase failed to process. Please check your internet connection and try again.");
////			InitializePurchasing();
//			ConnectInitializePurchasing(60);
//			//return;
//		}

		StoreObject storeObject = StoreList[id];
		currentStoreObject = storeObject;


		#if UNITY_IOS
		dummyId = storeObject.appleId;
		restoring = false;
		BuyProductID(currentStoreObject.appleId);
		//RequestBuyIAPApple();
		//		KJTime.Add(KJTime.SpaceType.System, DummyRequestBuyFunction, 1.0f, 1);
		#endif
		#if UNITY_ANDROID
		dummyId = storeObject.andriodId;
		restoring = false;
		BuyProductID(currentStoreObject.andriodId);
		//RequestBuyIAPApple();
		//		KJTime.Add(KJTime.SpaceType.System, DummyRequestBuyFunction, 1.0f, 1);
		//		KJTime.Add(KJTime.SpaceType.System, DummyRequestBuyFunction, 1.0f, 1);
		#endif

		//UIController.ShowWait(180);
	}

	public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
	{
		Debug.Log("OnInitialized: PASS");
		this.controller = controller;
		this.extensions = extensions;
		//callBackInitialize();
		//BuyProductID("HC1");
		//GetProductMeta();
	}
//	delegate void delegateString();
//	delegateString callBackInitialize;



	public IEnumerator ConnectInitializePurchasing(float timeOut){

		float startTime;
		startTime = Time.realtimeSinceStartup;
		InitializePurchasing () ;

		while(!IsInitialized()){
			yield return null;
			if(Time.realtimeSinceStartup - startTime >= timeOut) break;
		}

		if (IsInitialized())
		{
			Debug.Log("Success");
		}
		else
			UIController.ShowError("IAP ERROR", "Purchase cancelled. Please check your internet connection and try again.");
	//	return false;

	}


	public void InitializePurchasing () {

		if (IsInitialized())
		{
			return;
		}

		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
		#if UNITY_IOS
		for (int i = 0; i < comsumableIDApple.Length; i++) {
			builder.AddProduct(comsumableIDApple[i], ProductType.Consumable, new IDs{{comsumableIDApple[i], MacAppStore.Name}});
		}
		for (int i = 0; i < nonComsumableIDApple.Length; i++) {
			builder.AddProduct(nonComsumableIDApple[i], ProductType.Consumable, new IDs{{nonComsumableIDApple[i], MacAppStore.Name}});
		}
		#endif
		#if UNITY_ANDROID
		for (int i = 0; i < comsumableIDGoogle.Length; i++) {
			builder.AddProduct(comsumableIDGoogle[i], ProductType.Consumable, new IDs{{comsumableIDGoogle[i], MacAppStore.Name}});
		}
		for (int i = 0; i < nonComsumableIDGoogle.Length; i++) {
			builder.AddProduct(nonComsumableIDGoogle[i], ProductType.Consumable, new IDs{{nonComsumableIDGoogle[i], MacAppStore.Name}});
		}
		#endif
		UnityPurchasing.Initialize (this, builder);
	}

	public bool IsInitialized()
	{
		// Only say we are initialized if both the Purchasing references are set.
		return controller != null && extensions != null;
	}

	public void BuyConsumable(string productId)
	{
		// Buy the consumable product using its general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
		BuyProductID(productId);
	}

	void BuyProductID(string productId)
	{
		restoring = false;
		UIController.ShowWait();

		if(!IsInitialized())
			ConnectInitializePurchasing(60);
		// If Purchasing has been initialized ...
//		if (IsInitialized())
//		{
			// ... look up the Product reference with the general product identifier and the Purchasing 
			// system's products collection.
			Product product = controller.products.WithID(productId);

			// If the look up found a product for this device's store and that product is ready to be sold ... 
			if (product != null && product.availableToPurchase)
			{
				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
				// asynchronously.

				controller.InitiatePurchase(product);
			}
			// Otherwise ...
			else
			{
				// ... report the product look-up failure situation  
				Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
//		}
//		// Otherwise ...
//		else
//		{
//			// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
//			// retrying initiailization.
//			Debug.Log("BuyProductID FAIL. Not initialized.");
//		}
	}

	public void RestorePurchases()
	{
		restoring = true;
		// If Purchasing has not yet been set up ...
		if (!IsInitialized())
		{
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		if (restoring)
		{
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer || 
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			// ... begin restoring purchases
			Debug.Log("RestorePurchases started ...");

			// Fetch the Apple store-specific subsystem.
			var apple = extensions.GetExtension<IAppleExtensions>();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
				// no purchases are available to be restored.
				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		// Otherwise ...
		else
		{
			// We are not running on an Apple device. No work is necessary to restore purchases.
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}

		UIController.ClosePopUp();
	}

	/// <summary>
	/// Called when Unity IAP encounters an unrecoverable initialization error.
	///
	/// Note that this will not be called if Internet is unavailable; Unity IAP
	/// will attempt initialization until it becomes available.
	/// </summary>
	public void OnInitializeFailed (InitializationFailureReason error)
	{
	}

	/// <summary>
	/// Called when a purchase completes.
	///
	/// May be called at any time after OnInitialized().
	/// </summary>
	public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args)
	{
		// A consumable product has been purchased by this user.

		#if UNITY_IOS
		for (int i = 0; i < comsumableIDApple.Length; i++) {
			if (String.Equals(args.purchasedProduct.definition.id, comsumableIDApple[i], StringComparison.Ordinal)) {
				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				// The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
				//ScoreManager.score += 100;
				//KJTime.Add(UIController.ClosePopUp, 5, 1);

				ProfileController.Credits += currentStoreObject.units;
				UIController.UpdateCurrency();
				if (!restoring) {
					ProfileController.Singleton.disableSkipAds = true;
					IAPManager.SaveData();
					UIController.ShowPopUpSingleYes("SUCCESS", "The Credits have now been added!", "DONE", UIController.ClosePopUp);
				}
				//IAPManager.SaveData();
			}
		}
		for (int i = 0; i < nonComsumableIDApple.Length; i++) {
			if (String.Equals(args.purchasedProduct.definition.id, nonComsumableIDApple[i], StringComparison.Ordinal)) {
				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

				ProfileController.GemDouble = true;
				if (!restoring) {
					IAPManager.SaveData();
					UIController.ShowPopUpSingleYes("SUCCESS", "The Credits have now been added!", "DONE", UIController.ClosePopUp);
				}
			}
		}
		#endif

		#if UNITY_ANDROID
		for (int i = 0; i < comsumableIDGoogle.Length; i++) {
			if (String.Equals(args.purchasedProduct.definition.id, comsumableIDGoogle[i], StringComparison.Ordinal)) {
				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				// The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
				//ScoreManager.score += 100;
				//KJTime.Add(UIController.ClosePopUp, 5, 1);

				ProfileController.Credits += currentStoreObject.units;
				UIController.UpdateCurrency();
				if (!restoring) {
					IAPManager.SaveData();
					UIController.ShowPopUpSingleYes("SUCCESS", "The Credits have now been added!", "DONE", UIController.ClosePopUp);
				}

			}
		}
		for (int i = 0; i < nonComsumableIDGoogle.Length; i++) {
			if (String.Equals(args.purchasedProduct.definition.id, nonComsumableIDGoogle[i], StringComparison.Ordinal)) {
				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

				ProfileController.GemDouble = true;
				if (!restoring) {
					IAPManager.SaveData();
					UIController.ShowPopUpSingleYes("SUCCESS", "The Credits have now been added!", "DONE", UIController.ClosePopUp);
				}
			}
		}
		#endif

		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		return PurchaseProcessingResult.Complete;
	}

	/// <summary>
	/// Called when a purchase fails.
	/// </summary>
	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		UIController.ShowError("IAP ERROR", "Purchase cancelled. Please check your internet connection and try again.");
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
		// this reason with the user to guide their troubleshooting actions.
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}
		
	void GetProductMeta () {
		foreach (var product in controller.products.all) {
			Debug.Log (product.metadata.localizedTitle);
			Debug.Log (product.metadata.localizedDescription);
			Debug.Log (product.metadata.localizedPriceString);
		}
	}


	private IStoreCallback callback;
	public void Initialize (IStoreCallback callback)
	{
		this.callback = callback;   
	}

	public void RetrieveProducts (System.Collections.ObjectModel.ReadOnlyCollection<UnityEngine.Purchasing.ProductDefinition> products)
	{
		foreach (var product in products) {
			//product.
		}
		// Fetch product information and invoke callback.OnProductsRetrieved();
	}

	public void Purchase (UnityEngine.Purchasing.ProductDefinition product, string developerPayload)
	{
		// Start the purchase flow and call either callback.OnPurchaseSucceeded() or callback.OnPurchaseFailed()
	}

	public void FinishTransaction (UnityEngine.Purchasing.ProductDefinition product, string transactionId)
	{
		// Perform transaction related housekeeping 
	}
		
}
