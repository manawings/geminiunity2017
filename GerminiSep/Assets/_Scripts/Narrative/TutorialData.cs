﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialData {

	private static bool hasBeenInit = false;
	private static List<TutorialData> tutorialList;
	
	public static void Initialize ()
	{
		if (hasBeenInit) return;
		hasBeenInit = true;

		tutorialList = new List<TutorialData>();

		// 0 Learn How to Start
		CreateTutorial(0);

		// 1 Learn How to Move
		CreateTutorial(1);

		// 2 Learn How to Shoot Reactor
		CreateTutorial(2);

		// 3 How to open SC Box
		CreateTutorial(3);

		// 4 Learn How to HC Box
		CreateTutorial(4, ConditionType.PreBossSector);

		// 5
		CreateTutorial(-1, ConditionType.PreviousIsDone);

		// 6 Learn How to Equip an Item
		CreateTutorial(5, ConditionType.HasItem);

		// 7 Press Item
		CreateTutorial(-1, ConditionType.PreviousIsDone);

		// 8 Press Equip
		CreateTutorial(-1, ConditionType.PreviousIsDone);

		// 9 Post Equip
		CreateTutorial(6, ConditionType.PreviousIsDone);

		// 10 First Elite
		CreateTutorial(8);

		// 11 Mines
		CreateTutorial(9);

		// 12 Asteroids
		CreateTutorial(10);

		// 13 Upgrade Item
		CreateTutorial(20, ConditionType.HasEquip);

		// 14 Upgrade Item
		CreateTutorial(-1, ConditionType.PreviousIsDone);

		// 15 Upgrade Item
		CreateTutorial(-1, ConditionType.PreviousIsDone);

		// 16 Upgrade Complete
		CreateTutorial(21, ConditionType.PreviousIsDone);

		// 17 Cargo
		CreateTutorial(22);

		// 18 Reserved
		CreateTutorial(22);

		// 19 Map
		CreateTutorial(25);

		// 20 Ship Button
		CreateTutorial(23, ConditionType.ReadyForShips);

		// 21 BulletHell
		CreateTutorial(27);



		// 22 Raid Mission Fall Back
		CreateTutorial(28);

		// 23 Raid Mission Start
		CreateTutorial(29);

		// 24 Raid Mission End
		CreateTutorial(30);

		// 25 Raid Mission Success
		CreateTutorial(31);




		// 26 Elite
		CreateTutorial(32);

		// 27 Quest_Bonus
		CreateTutorial(33);

		// 28 Black Hole
		CreateTutorial(34);

		// 29 Ally Help
		CreateTutorial(35);

		// 30 Stun
		CreateTutorial(36);

		// 31 End Level 100
		CreateTutorial(37);

		// 32 Quest Boss
		CreateTutorial(38);

		// 33 BrutalRock
		CreateTutorial(39);

		// 34 BrutalMine
		CreateTutorial(40);

		// 35 Platform
		CreateTutorial(41);

		// 36 First Die
		CreateTutorial(42);

		// 37 Empty Heart
		CreateTutorial(43);

		// 38 First Kill Boss
		CreateTutorial(44);

		// 39 Cargo > 100 Gems
		CreateTutorial(45, ConditionType.GemCargo);


		
	}

	public int id, storyId = -1;
	public bool isActive = true;
	public ConditionType conditionType;
	public TutorialPrompter tutorialPrompter;
	public int preId = -1;

	public static void CreateTutorial (int _storyId, ConditionType condition = ConditionType.None, int preId = -1)
	{
		TutorialData newTutorial = new TutorialData();
		newTutorial.storyId = _storyId;
		newTutorial.preId = preId;
		newTutorial.id = tutorialList.Count;
		newTutorial.conditionType = condition;
		tutorialList.Add (newTutorial);
	}

	public enum ConditionType {

		None,
		PreviousIsDone,
		HasItem,
		PreBossSector,
		ReadyForShips,
		AllySector,
		HasEquip,
		GemCargo
	}

	public static void Deploy ( int tutorialId , TutorialPrompter tutorialPrompter = null) 
	{
		TutorialData tutorial = tutorialList[tutorialId];

		//Debug.Log ("Ordered To Deploy: " + tutorialId + " // " + tutorial.CheckCondition(tutorial));

		if (tutorial.CheckCondition(tutorial)) {

			//Debug.Log ("Deploying: " + tutorialId);

			tutorial.isActive = false;
			tutorial.tutorialPrompter = tutorialPrompter;
			ProfileController.Singleton.tutorialStatus[tutorial.id] = true;

			if (tutorial.storyId != -1) {
				StoryPair storyPair = StoryPair.At(tutorial.storyId).list[0];
				storyPair.tutorialId = tutorial.id;
				UIController.ShowStory(tutorial.storyId);
			} else {
				PostDeploy(tutorial.id);
			}
		}
	}

	public static bool CheckTutorialState (int tutorialId) {

		if (CheckTutorial(tutorialId) || HasCompleted(tutorialId)) {
			return true;
		} else {
			return false;
		}
	}

	public static bool CheckTutorial ( int tutorialId ) 
	{
		TutorialData tutorial = tutorialList[tutorialId];
		return tutorial.CheckCondition();

	}

	public static bool HasCompleted ( int tutorialId ) 
	{
		return ProfileController.Singleton.tutorialStatus[tutorialId];
		
	}

	public bool CheckCondition (TutorialData tutorial = null) {

		// Check if the tutorial has already been done.
		if (ProfileController.Singleton.tutorialStatus[id]) {
			return false;
		}
//		Debug.Log("Check Condition: "+ conditionType.ToString());

		switch (conditionType) {

		case ConditionType.None:
			return true;
			break;

		case ConditionType.PreviousIsDone:

			if (preId == -1) {
				if (ProfileController.Singleton.tutorialStatus[id - 1]) return true;
			} else {
				if (ProfileController.Singleton.tutorialStatus[preId]) return true;
			}

			break;

		case ConditionType.HasItem:
			if (ProfileController.Singleton.inventoryList.Count > 0) {
				if (ProfileController.SubSector != ProfileController.MaxSubSectors || ProfileController.Singleton.tutorialStatus[4]) {
					// HC Chest is done, or not in subsector.
					return true;
				}
			}
			break;

		case ConditionType.PreBossSector:
			if (ProfileController.SubSector == ProfileController.MaxSubSectors) return true;
			break;

		case ConditionType.ReadyForShips:
			if ((ProfileController.SubSector > 4 && ProfileController.SectorId > 1) || ProfileController.SectorId > 2) return true;
			break;

		case ConditionType.AllySector:
			if (ProfileController.SectorId > 1) return true;
			break;
		
		case ConditionType.HasEquip:
			if (ProfileController.Singleton.equipList.Count > 0) {
				return true;
			}
			break;

		case ConditionType.GemCargo:

//			Debug.Log ("Box Cost: " + Stats.BoxCostForLevel(ProfileController.SectorLevel) + "  // SECT: " + ProfileController.SectorLevel);

			if (Stats.BoxCostForLevel(ProfileController.SectorLevel) > 100) {
				return true;
			}

			break;

		}


		return false;
	}

	public static void PostDeploy ( int tutorialId ) 
	{
		TutorialData tutorial = tutorialList[tutorialId];
		KJTime.Add( tutorial.PostExecute, 0.05f, 1);
	}

	public void PostExecute ()
	{
		if (tutorialPrompter != null) {
			tutorialPrompter.PostExecute();
		}
	}

}
