﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

public class StoryPair {

	public string contentLine;
	public StoryCharacter character;
	public int tutorialId = -1, creditGain = 0, id = 0;
	public ObscuredInt gemGain = 0;

	public StoryPair (StoryCharacter character, string contentLine, int creditGain = 0, int gemGain = 0)
	{
		this.contentLine = contentLine;
		this.character = character;
		this.creditGain = creditGain;
		this.gemGain = gemGain;
	}



	private static bool hasBeenInit = false;
	public static List<Story> storyList;
	public static Story story;

	public static void Initialize ()
	{
		if (hasBeenInit) return;
		hasBeenInit = true;

		storyList = new List<Story>();

		CreateStory(); // 0: Learn How to Start
		AddSub(StoryCharacter.COMPUTER, ">> CRYOGENIC STASIS LOG");
		AddSub(StoryCharacter.COMPUTER, ">> DURATION: 6 WEEKS");
		AddSub(StoryCharacter.COMPUTER, ">> STATUS: COMPLETE");
		AddSub(StoryCharacter.COMPUTER, ">> GOOD MORNING, GEMINI SOLDIER @NN");

		AddSub(StoryCharacter.PILOT, "Mmmm... Mornin', computer. Whatcha got for me?");

		AddSub(StoryCharacter.COMPUTER, ">> INCOMING COMMS LINK WITH ALLIANCE REPUBLIC HQ");

		AddSub(StoryCharacter.BRIGHTMAN, "Well, punch me in the gut. @N, it's good to see you.");

		AddSub(StoryCharacter.PILOT, "Major Brightman, you mangled old lion. It's an honor to be working with you again.");
		AddSub(StoryCharacter.BRIGHTMAN, "Your sixth consecutive tour is coming up, @N. You know what that means.");
		AddSub(StoryCharacter.PILOT, "All I think about, Major. Blowing the living hell out of the Royal Empire's fleet of space dorks.");

		AddSub(StoryCharacter.BRIGHTMAN, "Hah! That's the Alliance spirit! OORAH!");
		AddSub(StoryCharacter.PILOT, "OORAH! Alright, get me out there.");


		CreateStory(); // 1: Learn How to Move
		AddSub(StoryCharacter.BRIGHTMAN, "@N, say hello to your new virtual engineer for the tour.");
		AddSub(StoryCharacter.ENGINEER, "Hold your finger on your display to fire and move your ship.");
		AddSub(StoryCharacter.ENGINEER, "Try not to lift your finger from the display.");
		AddSub(StoryCharacter.PILOT, "Um, thanks... I know. And hello, by the way.");
		AddSub(StoryCharacter.ENGINEER, "Oh, yeah hello! Hah, sorry @N. I'm Ren.");
		AddSub(StoryCharacter.ENGINEER, "It's my first day. I'm still feeling a little nervous.");
		AddSub(StoryCharacter.PILOT, "I have that effect on people.");
		AddSub(StoryCharacter.ENGINEER, "In line with Project Gemini's annual servicing, I have to take you through basic operations.");
		AddSub(StoryCharacter.PILOT, "So I get to keep receiving messages from you, telling me how to do things I've been doing for years?");
		AddSub(StoryCharacter.ENGINEER, "Um, well... Yeah.");
		AddSub(StoryCharacter.PILOT, "Goodie.");

		CreateStory(); // 2: Use Reactor
		AddSub(StoryCharacter.ENGINEER, "Hey @N, that glowing thing you just picked up was a Reactor.");
		AddSub(StoryCharacter.ENGINEER, "See how it appears in the bottom left corner of your screen?");
		AddSub(StoryCharacter.PILOT, "Hmm, this is new. What does it do?");
		AddSub(StoryCharacter.ENGINEER, "You can DOUBLE TAP the screen to unleash it as a weapon.");
		AddSub(StoryCharacter.ENGINEER, "Different types of Reactors will produce a different effect.");
		AddSub(StoryCharacter.ENGINEER, "But it's gone after one use, so use it well!");

		CreateStory(); // 3: Find a SC Cargo
		AddSub(StoryCharacter.PILOT, "Look at that, Ren. We found a box.", 0, 100);
		AddSub(StoryCharacter.ENGINEER, "The cargo ship you shot apart earlier must have dropped it.");
		AddSub(StoryCharacter.PILOT, "What do you suppose is inside?");
		AddSub(StoryCharacter.ENGINEER, "Typically they contain an item. Perhaps a new weapon or armor that you can equip to your ship.");
		AddSub(StoryCharacter.PILOT, "Ha! Then what are we waiting for? Let's get it open.");

		CreateStory(); // 4: Find a HC Cargo
		AddSub(StoryCharacter.ENGINEER, "Oh, crap! CRAP! Uh... I screwed up.", 3);

		AddSub(StoryCharacter.PILOT, "What did you do?");

		AddSub(StoryCharacter.ENGINEER, "I was ... I was just trying to make an investment for the ship.");

		AddSub(StoryCharacter.PILOT, "Investment?");

		AddSub(StoryCharacter.ENGINEER, "I kinda hacked into a pirate COMMS frequency...");
		AddSub(StoryCharacter.ENGINEER, "There was an exiled prince promising a 200% return on investments in just three weeks.");

		AddSub(StoryCharacter.PILOT, "So, let me guess. You sent them our $C allowance. And suddenly we're $200C lighter.");

		AddSub(StoryCharacter.ENGINEER, "I..");

		AddSub(StoryCharacter.PILOT, "Hahaha! Beat me sideways, that is hilarious. You're an idiot.");

		AddSub(StoryCharacter.ENGINEER, "Please, @N, you can't tell Major Brightman. He'll freaking kill me. I'll get fired!");

		AddSub(StoryCharacter.PILOT, "Relax. So we're strapped for cash. Big deal.");
		AddSub(StoryCharacter.PILOT, "We'll just find a way to use the pirate COMMS channel to our advantage, since we have that now.");

		AddSub(StoryCharacter.ENGINEER, "Well, the pirates do operate a Black Market where they trade really good gear for just a few $C.");

		AddSub(StoryCharacter.PILOT, "That's it! Open access to the Black Market.");
		AddSub(StoryCharacter.PILOT, "We'll gear up through there instead, and Major Brightman won't hear a thing of this. Done deal?");

		AddSub(StoryCharacter.ENGINEER, "A-Alright. Let's try it.");


		CreateStory(); // 5: Inventory Screen
		AddSub(StoryCharacter.ENGINEER, "@N, since you're now the proud owner of a new item, we should run over equipping protocol.");
		AddSub(StoryCharacter.ENGINEER, "Won't take long!");
		CreateStory(); // 6: Finished Equipping
		AddSub(StoryCharacter.ENGINEER, "That's it. Wasn't so bad, right?");
		AddSub(StoryCharacter.PILOT, "Reminded me of getting a booster at the Doctor's as a kid.");
		AddSub(StoryCharacter.ENGINEER, "Oh yeah? In what way?");
		AddSub(StoryCharacter.PILOT, "Learning how to deal with a little prick.");
		AddSub(StoryCharacter.ENGINEER, "Man. Low blow, @N. Low blow.");


		CreateStory(); // 7: First Boss

		CreateStory(); // 8: First Elite
		AddSub(StoryCharacter.ENGINEER, "Watch out! That's an elite unit you're fighting. They're stronger than the smaller ships.");
		AddSub(StoryCharacter.ENGINEER, "It's gonna take a lot more hits to take down.");
		AddSub(StoryCharacter.PILOT, "Don't worry Ren, I know these guys well. The bigger they are, the bigger the explosions!");

		CreateStory(); // 9: Mines
		AddSub(StoryCharacter.ENGINEER, "Hmm... That's strange.");
		AddSub(StoryCharacter.ENGINEER, "I'm detecting proximity mines in this area. We haven't seen these in a while.");
		AddSub(StoryCharacter.PILOT, "Mines?");
		AddSub(StoryCharacter.ENGINEER, "If you fly too close to them, they will blow you right up.");
		AddSub(StoryCharacter.PILOT, "No problem. I'll just shoot them down.");
		AddSub(StoryCharacter.ENGINEER, "That won't work. They're too small and have no heat signature. Waste of time.");
		AddSub(StoryCharacter.ENGINEER, "You'll just have to be careful not to fly into them.");

		CreateStory(); // 10: Asteroids
		AddSub(StoryCharacter.ENGINEER, "@N... I'm detecting asteroids in this area.");
		AddSub (StoryCharacter.ENGINEER, "You know the rules. No asteroid is to be destroyed in combat in accordance with the Galactical Geological Responsibility During Wartime Efforts Act of X20X.");
		AddSub (StoryCharacter.PILOT, "If I ever get the chance to meet the politician who made that law, I'll ram an asteroid so far up their...");
		AddSub(StoryCharacter.ENGINEER, "WELL, like it or not, that's how it is. It's just a rock, anyway. You can handle a rock, can't you?");
		AddSub(StoryCharacter.PILOT, "Pff. Rock's got nothin' on me.");


		// Enemy Tutorials
		
		CreateStory(); // 11: Noob Enemy 1
		AddSub(StoryCharacter.ENGINEER, "This is the 11th Story. This will play when you enter the sector.");
		AddSub(StoryCharacter.ENGINEER, "This is the 11th Story. This will play when you enter the sector.");
		
		CreateStory(); // 12: Noob Enemy 2
		
		CreateStory(); // 13: Striker

		CreateStory(); // 14: Stunner
		CreateStory(); // 15: Beamer
		CreateStory(); // 16: Special Ops
		CreateStory(); // 17: Starwars
		CreateStory(); // 18: Elites
		CreateStory(); // 19: Hard Elites

		// Reserved

		CreateStory(); // 20: Upgrade Item
		AddSub(StoryCharacter.ENGINEER, "Good. Now your ship is a little bit stronger! But while we're here...", 0, 100);
		AddSub(StoryCharacter.ENGINEER, "There's just one more thing I need to show you.");
		AddSub(StoryCharacter.ENGINEER, "As you progress through the sector, the Imperial fleet will be upgrading their ships.");
		AddSub(StoryCharacter.ENGINEER, "To keep up, you'll have to upgrade your ship's gear as well. Here's how...");

		CreateStory(); // 21: Upgrade Complete
		AddSub(StoryCharacter.ENGINEER, "All done! There, that's it. Wasn't so bad, right?");
		AddSub(StoryCharacter.PILOT, "Reminded me of getting a booster at the Doctor's as a kid.");
		AddSub(StoryCharacter.ENGINEER, "Oh yeah? In what way?");
		AddSub(StoryCharacter.PILOT, "Learning how to deal with a little prick.");
		AddSub(StoryCharacter.ENGINEER, "Man. Low blow, @N. Low blow.");

		CreateStory(); // 22: Cargo
		AddSub(StoryCharacter.ENGINEER, "Keep an eye out for a slow, orange-looking ship in your area.");
		AddSub(StoryCharacter.PILOT, "Diplomatic?");
		AddSub(StoryCharacter.ENGINEER, "No, it's an Imperial cargo ship. They have thick armor and are hard to take down.");
		AddSub(StoryCharacter.ENGINEER, "But if you can manage it, it'll drop some mighty fine loot.");

		CreateStory(); // 23: 
		AddSub(StoryCharacter.ENGINEER, "Hey, I have some news.");
		AddSub(StoryCharacter.PILOT, "Good news, I hope?");
		AddSub(StoryCharacter.ENGINEER, "Sure is. You now have direct access to Project Gemini's 'ship hangar' protocol.");
		AddSub(StoryCharacter.ENGINEER, "You'll gain access to better ships over time, and you can switch to the ship you want to use.");
		AddSub(StoryCharacter.PILOT, "So what? The Alliance may be the greatest nation in all Universes, but our ships are pretty samey.");
		AddSub(StoryCharacter.ENGINEER, "That's the thing, @N. It's not just Alliance fighters we have access to.");
		AddSub(StoryCharacter.ENGINEER, "Take a look...");

		CreateStory(); // 24: 

		AddSub(StoryCharacter.ENGINEER, "!!! TODO");

		CreateStory(); // 25:

		AddSub(StoryCharacter.ENGINEER, "Ta-da! Welcome to the StarMap! Here you can see your progress through the sectors.");
		AddSub(StoryCharacter.ENGINEER, "Normally, once you clear a sector, you'd typically want to move on to the next one.");
		AddSub(StoryCharacter.ENGINEER, "But you can also go back to previous Sectors whenever you want.");
		AddSub(StoryCharacter.PILOT, "Um, I know how maps work Ren. But thanks anyway.");
		AddSub(StoryCharacter.ENGINEER, "Oh, there is one more thing... Remember how we linked up with the pirate COMMS?");
		AddSub(StoryCharacter.PILOT, "Mm hmm?");
		AddSub(StoryCharacter.ENGINEER, "Occasionally... Say, every few hours, they broadcast jobs on the COMMS.");
		AddSub(StoryCharacter.ENGINEER, "That will show up here on the StarMap as well.");
		AddSub(StoryCharacter.PILOT, "Jobs? Pirates have jobs?");

		AddSub(StoryCharacter.ENGINEER, "Yeah, well, I guess they're more like 'secret pirate missions.'");
		AddSub(StoryCharacter.ENGINEER, "Stuff like bounty hunting or intercepting a rare cargo ship.");
		AddSub(StoryCharacter.ENGINEER, "Lots of rewards to be gained from completing one.");
		AddSub(StoryCharacter.PILOT, "But where there's reward, there's always risk...");

		AddSub(StoryCharacter.ENGINEER, "That's right. If you attempt a secret mission and fail it, it'll be removed from the StarMap.");
		AddSub(StoryCharacter.ENGINEER, "You won't get a second chance. You'll have to wait until a new mission is broadcast to try again.");
		AddSub(StoryCharacter.PILOT, "Sounds fun. I'll keep an eye out for it.");

		CreateStory(); // 26: 

		CreateStory(); // 27: 

		AddSub(StoryCharacter.IMPERIALPILOT, "At last! I've been waiting...");
		AddSub(StoryCharacter.PILOT, "What is this? Who are you?");
		AddSub(StoryCharacter.IMPERIALPILOT, "I've prepared a little surprise for you, my Alliance friend.");
		AddSub(StoryCharacter.IMPERIALPILOT, "Our entire squad has been equipped with plasma weapons. This is the end for you!");
		AddSub(StoryCharacter.PILOT, "Ren? What is he talking about?");
		AddSub(StoryCharacter.ENGINEER, "Plasma weapons are the purple ones, @N. They are extremely dangerous.");
		AddSub(StoryCharacter.ENGINEER, "It's got the highest damage output that we know of.");
		AddSub(StoryCharacter.IMPERIALPILOT, "That's right. And if you get hit even once, you're finished! Bahaha!");
		AddSub(StoryCharacter.PILOT, "IF I get hit? Well then, I guess I'll have to NOT get hit.");
		AddSub(StoryCharacter.IMPERIALPILOT, "WAHAHA!");


		CreateStory(); // 28: 
//		AddSub(StoryCharacter.ENGINEER, "AoE!");
		AddSub(StoryCharacter.IMPERIALPILOT, "That's enough for now. Fall back!");
		AddSub(StoryCharacter.IMPERIALPILOT, "This isn't over, Gemini Pilot. We'll meet again.");


		CreateStory(); // 29: 
		AddSub(StoryCharacter.PILOT, "I'm detecting a single massive energy signal up ahead. I've never seen-");
		AddSub(StoryCharacter.IMPERIALPILOT, "Gemini Pilot! Behold the mighty, Core-powered Dreadnought.");
		AddSub(StoryCharacter.PILOT, "Core-powered what?");
		AddSub(StoryCharacter.IMPERIALPILOT, "You see, Imperial science is the best in the galaxy. We will destroy you!");
		AddSub(StoryCharacter.ENGINEER, "Core-powered... So they really managed to build one. @N, perhaps if we can defeat it, we can salvage their energy module.");
		AddSub(StoryCharacter.PILOT, "I'll try. But looks like it'll take a while to burn through all that armor.");
		AddSub(StoryCharacter.IMPERIALPILOT, "That's right, little pilot. You'll never beat us!");
		AddSub(StoryCharacter.ENGINEER, "Be careful, @N. If you are defeated here, they'll get away and we'll lose their signal.");
		AddSub(StoryCharacter.PILOT, "No pressure.");

		CreateStory(); // 30: Tutorial for First Raid Mission
		AddSub(StoryCharacter.ENGINEER, "I see... They've retreated to improve their weapons and tactics against you.");
		AddSub(StoryCharacter.ENGINEER, "To take them down, you'll have to win multiple confrontations against them without losing even once.");
		AddSub(StoryCharacter.ENGINEER, "If you can manage that, then perhaps we can savage their Core energy module for our own use.");

		CreateStory(); // 31: Tutorial for End of First Raid Boss Defeated
		AddSub(StoryCharacter.ENGINEER, "You've done it, @N! You defeated the Dreadnought. I've retrieved one of its Cores. It is a truly powerful piece of technology...");
		AddSub(StoryCharacter.ENGINEER, "If you manage to find a few more, we'll have enough to build our own energy Fighters.");


		CreateStory(); // 32: 
		AddSub(StoryCharacter.ENGINEER, "Wait! What's this? I'm detecting two - no, THREE elite units approaching you.");
		AddSub(StoryCharacter.PILOT, "Starting to think these guys don't like me much.");
		AddSub(StoryCharacter.IMPERIALPILOT, "Don't expect mercy, pilot of the Alliance. You may be able to fight us in isolation...");
		AddSub(StoryCharacter.IMPERIALPILOT, "But can you defeat three of us in a single battle?");
		AddSub(StoryCharacter.PILOT, "You really want me to answer that?");

		CreateStory(); // 33: 
		AddSub(StoryCharacter.ENGINEER, "Looks like we're on our first 'cargo route' mission.");
		AddSub(StoryCharacter.PILOT, "There's a first time for everything. What do I need to know?");
		AddSub(StoryCharacter.ENGINEER, "Let's see... A high value cargo box is being transported by a Royal Elite.");
		AddSub(StoryCharacter.ENGINEER, "Also, all the ships on this route are shipping $G. Surplus amounts of $G.");
		AddSub(StoryCharacter.PILOT, "Sweet. This'll be like taking candy from a baby.");
		AddSub(StoryCharacter.ENGINEER, "Yeah... a baby with an enormous plasma cannon.");
		AddSub(StoryCharacter.ENGINEER, "Don't underestimate them, @N! The Royal Elites are heavily armed and guarded.");
		AddSub(StoryCharacter.ENGINEER, "Plus, if you fail the mission, it'll be gone from your StarMap until a new one appears.");
		AddSub(StoryCharacter.PILOT, "Chill, Ren. I got this.");

		CreateStory(); // 34: 
		AddSub(StoryCharacter.ENGINEER, "Uh oh...");
		AddSub(StoryCharacter.PILOT, "What? What's the matter?");
		AddSub(StoryCharacter.ENGINEER, "There are enemies in this area equipped with VOID bombs. Also known as black hole bombs!");
		AddSub(StoryCharacter.PILOT, "That sounds bad. Is it bad?");
		AddSub(StoryCharacter.ENGINEER, "They detonate and leave a black hole in the area for a couple of seconds.");
		AddSub(StoryCharacter.ENGINEER, "Anything nearby will be sucked in and damaged. I don't recommend you fly anywhere near those things.");

		CreateStory(); // 35: Ally comes to help
		AddSub(StoryCharacter.BRIGHTMAN, "@N, I've sent an Alliance pilot in to assist you.");
		AddSub(StoryCharacter.PILOT, "Uh, thanks Major, but I don't need any help. Remember?");
		AddSub(StoryCharacter.BRIGHTMAN, "I know, soldier. But the forces have an image to present to the media.");
		AddSub(StoryCharacter.BRIGHTMAN, "Plus, it'll be good for the headlines.");
		AddSub(StoryCharacter.PILOT, "Right...");

		CreateStory(); // 36: EMP Intro
		AddSub(StoryCharacter.ENGINEER, "@N, some of the enemies here are equipped with EMP weapons.");
		AddSub(StoryCharacter.ENGINEER, "If you see bullets or missiles that are blue, that means it's an EMP.");
		AddSub(StoryCharacter.PILOT, "What sort of damage are we talking about?");
		AddSub(StoryCharacter.ENGINEER, "If you get hit, your systems will go offline. You'll move slow, and your guns will be disabled.");
		AddSub(StoryCharacter.PILOT, "Hmmm...");
		AddSub(StoryCharacter.ENGINEER, "Don't worry though, the stun only lasts for a second.");
		AddSub(StoryCharacter.PILOT, "That might be a second too long...");

		CreateStory(); // 37: END GAME ENDING
		AddSub(StoryCharacter.COMPUTER, ">> PROJECT LOG UPDATE - MISSION COMPLETED. CONFIRMED ROYAL EMPIRE IDENTITY EMPRESS IS DESTROYED.");
		AddSub(StoryCharacter.COMPUTER, ">> PLEASE TELL ME YOUR ORDERS, GEMINI PILOT @NN.");
		AddSub(StoryCharacter.PILOT, "This is it... It's over.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.PILOT, "Computer... initiate termination protocol.");
		AddSub(StoryCharacter.COMPUTER, ">> TERMINATION PROTOCOL WILL DESTROY ALL PROJECT GEMINI CLONES. CONTINUE?");
		AddSub(StoryCharacter.PILOT, "Continue.");
		AddSub(StoryCharacter.COMPUTER, ">> PROTOCOL PROTECTED BY PASSWORD. PROVIDE PASSWORD TO CONTINUE.");
		AddSub(StoryCharacter.PILOT, "Password is...");
		AddSub(StoryCharacter.PILOT, "M-O-T-H-E-R");
		AddSub(StoryCharacter.COMPUTER, ">> PASSWORD ACCEPTED. INTIIATING TERMINATION PROTOCOL.");
		AddSub(StoryCharacter.COMPUTER, ">> ...");
		AddSub(StoryCharacter.COMPUTER, ">> CONFIRMED DESTRUCTION OF REMAINING 13,446 GEMINI CLONES.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.COMPUTER, ">> CONFIRMED SHUT DOWN AND DESTRUCTION OF FACILITY MAINFRAMES.");
		AddSub(StoryCharacter.COMPUTER, ">> FINAL PROCESS: DESTRUCTION OF REMAINING CLONE, INITIALIZED.");
		AddSub(StoryCharacter.PILOT, "Wait...");
		AddSub(StoryCharacter.COMPUTER, ">> REMOVAL OF MILITARY TITLE, SUCCESSFUL. PILOT NOW KNOWN AS 'PILOT @NN'.");
		AddSub(StoryCharacter.COMPUTER, ">> CONTINUING PROCESS...");
		AddSub(StoryCharacter.PILOT, "Stop!");
		AddSub(StoryCharacter.COMPUTER, ">> ACKNOWLEDGED.");
		AddSub(StoryCharacter.COMPUTER, ">> ALL SYSTEMS TERMINATED EXCEPT FOR FINAL PROCESS. HALTED BY MANUAL OVERRIDE.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.PILOT, "Not yet... I might just be a clone of a long lost soldier.");
		AddSub(StoryCharacter.PILOT, "But at least I'm the only one.");
		AddSub(StoryCharacter.PILOT, "Computer... re-engage StarNAV and COMM");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.PILOT, "Time to look up some old friends.");


		CreateStory(); // 38: SECRET BOUNTY
		AddSub(StoryCharacter.ENGINEER, "So, you've decided to do a bounty hunting mission.");
		AddSub(StoryCharacter.PILOT, "Just trying out new experiences.");
		AddSub(StoryCharacter.ENGINEER, "Alright, well, let me explain how this works.");
		AddSub(StoryCharacter.ENGINEER, "The pirates have tracked a BIG ship to this area. There's a bounty on this ship of $2C.");
		AddSub(StoryCharacter.PILOT, "So if I take it down, I'll be $2C richer?");
		AddSub(StoryCharacter.ENGINEER, "That's correct. But let me remind you one more time...");
		AddSub(StoryCharacter.ENGINEER, "If you fail the mission, it will be gone from the StarMap.");
		AddSub(StoryCharacter.ENGINEER, "You'll have to wait some hours until it is broadcast again to have another shot.");
		AddSub(StoryCharacter.PILOT, "Alright, got it. Time to focus.");


		CreateStory(); // 39: ROCK LEVEL
		AddSub(StoryCharacter.COMPUTER, ">> WARNING!");
		AddSub(StoryCharacter.COMPUTER, ">> ASTEROID STORM DETECTED.");
		AddSub(StoryCharacter.PILOT, "REN! Out of all the possible routes, why am I flying through an asteroid storm?");
		AddSub(StoryCharacter.ENGINEER, "Ah, sorry! It's my fault. I'm not very good at navigation.");
		AddSub(StoryCharacter.PILOT, "Damn it, Ren! How am I supposed to get out of this mess?");
		AddSub(StoryCharacter.ENGINEER, "Just keep flying in that direction. If you can survive for about a minute, you'll be clear of the storm.");
		AddSub(StoryCharacter.PILOT, "'JUST' keep flying towards an asteroid storm. Oh, okay, cool. Thanks.");


		CreateStory(); // 40: MINE LEVEL
		AddSub(StoryCharacter.BRIGHTMAN, "@N! It's that time again. Time for our broadcast of Super Space Acrobats. Are you ready?");
		AddSub(StoryCharacter.PILOT, "Sorry, Major. It's been a while since we've done this. Can you remind me how it goes again?");
		AddSub(StoryCharacter.BRIGHTMAN, "As you know, people love seeing awe inspiring footage of Alliance pilots.");
		AddSub(StoryCharacter.BRIGHTMAN, "So we broadcast Super Space Acrobats weekly, for their pilot pleasure! And you're the star.");
		AddSub(StoryCharacter.PILOT, "Great! What are my lines?");
		AddSub(StoryCharacter.BRIGHTMAN, "No lines. Just stunts.");
		AddSub(StoryCharacter.PILOT, "Stunts, huh?");
		AddSub(StoryCharacter.BRIGHTMAN, "We've laid out an obstacle course for you with live mines.");
		AddSub(StoryCharacter.ENGINEER, "Uhh...");
		AddSub(StoryCharacter.BRIGHTMAN, "All you have to do is fly through the course without blowing yourself up.");
		AddSub(StoryCharacter.ENGINEER, "LIVE mines?!");
		AddSub(StoryCharacter.BRIGHTMAN, "And BOOM! Action packed footage to inspire the folks back at home. Hahaha!");
		AddSub(StoryCharacter.PILOT, "Take it easy, Ren. I get second chances when it comes to stuff blowing up, remember?");


		CreateStory(); // 41: BLOCKADE
		AddSub(StoryCharacter.IMPERIALPILOT, "We've got him in our trap. Activate the jammers!");
		AddSub(StoryCharacter.PILOT, "What's this? I can't use my weapons!");
		AddSub(StoryCharacter.IMPERIALPILOT, "That's right fools, witness the glory of Imperial Engineering. The best in the galaxy!");
		AddSub(StoryCharacter.PILOT, "What's going on, Ren? What have they done?");
		AddSub(StoryCharacter.ENGINEER, "I detect zero enemy ships in the area. Don't tell me...");
		AddSub(StoryCharacter.ENGINEER, "The jamming technology also shut off their own ships.");
		AddSub(StoryCharacter.PILOT, "Huh? What a bunch of morons! I can just fly right through the sector.");
		AddSub(StoryCharacter.ENGINEER, "Hold on, that's not all. They've set up a blockade up ahead with fully functional weapons.");
		AddSub(StoryCharacter.PILOT, "Ah. They're getting more creative.");
		AddSub(StoryCharacter.IMPERIALPILOT, "Bahaha! This will be your final flight, @N. You'll never make it through.");
		AddSub(StoryCharacter.PILOT, "Looks like somebody didn't get the clone message.");

		CreateStory(); // 42: First Die
		AddSub(StoryCharacter.MEDIC, "Hello, @N. I'm Doctor Grace. How are you feeling?");
		AddSub(StoryCharacter.PILOT, "I... I'm fine. What happened? Did I die again?");
		AddSub(StoryCharacter.MEDIC, "Well. Your predecessor was just killed in battle. Or at least we've lost contact.");
		AddSub(StoryCharacter.PILOT, "Ah crap. I'm making me look bad...");
		AddSub(StoryCharacter.MEDIC, "As you know, the final memories of your late self are transcribed into your current self.");
		AddSub(StoryCharacter.MEDIC, "This way, you can learn from your mistakes. That is the essence of our operation.");
		AddSub(StoryCharacter.PILOT, "Of course. And you said there are no side effects, right?");
		AddSub(StoryCharacter.MEDIC, "No. Probably not. I guess we'll find out! But for now, your readings look normal.");
		AddSub(StoryCharacter.PILOT, "Comforting.");
		AddSub(StoryCharacter.MEDIC, "You may carry on, @N. I'll be keeping an eye on you.");

		CreateStory(); // 43: Empty Heart
		AddSub(StoryCharacter.MEDIC, "Sorry Major, we can't deploy right now.");
		AddSub(StoryCharacter.BRIGHTMAN, "What's the matter? We almost had them!");
		AddSub(StoryCharacter.MEDIC, "All copies of the pilot, @N, have been killed. The replacements are still in transit to this theatre.");
		AddSub(StoryCharacter.MEDIC, "In other words, we must wait a few minutes for them to be ready.");
		AddSub(StoryCharacter.BRIGHTMAN, "Very well, we'll wait for the new pilots to arrive.");

		CreateStory(); // 44: First Kill Boss
		AddSub(StoryCharacter.ENGINEER, "Wow! Good job, @N. That was amazing! It's the first time I've ever seen an Imperial Destroyer get taken out by a single pilot.");
		AddSub(StoryCharacter.PILOT, "See? I told you. Piece of cake. Did you see how many $G that thing dropped?");
		AddSub(StoryCharacter.ENGINEER, "Oh! There's another important thing I need to tell you. The first time you defeat a boss, you also get $1C.");
		AddSub(StoryCharacter.PILOT, "Hmm... $1C?");
		AddSub(StoryCharacter.ENGINEER, "$C is the prime crypto-currency of the trade networks, places like the Black Market.");
		AddSub(StoryCharacter.ENGINEER, "We use it now for ship hangar unlocks, too. And to deploy counter attacks against the enemy.");
		AddSub(StoryCharacter.PILOT, "Nobody else thinks it's weird that I, the soldier in an army, have to pay for stuff like... better equipment?");
		AddSub(StoryCharacter.ENGINEER, "$C are extremely valuable, so spend them wisely.");
		AddSub(StoryCharacter.PILOT, "'Spend them wisely?' So now I'm receiving financial advice from you? Of all people...");
		AddSub(StoryCharacter.PILOT, "YOU, who just now 'invested' our entire $C balance into the blackest hole of the universe?");
		AddSub(StoryCharacter.ENGINEER, "Oh. Haha, yeah. Sorry about that...");

		CreateStory(); // 45: Cargo > 100 Gems
		AddSub(StoryCharacter.PILOT, "Hey Ren, tell me something...");
		AddSub(StoryCharacter.ENGINEER, "What's that?");
		AddSub(StoryCharacter.PILOT, "How come this box is more expensive to open than the first one we found? It's the same, isn't it?");
		AddSub(StoryCharacter.ENGINEER, "Hmm. It seems that the alloy is stronger here. It'll take more effort to breach.");
		AddSub(StoryCharacter.PILOT, "Extra security. That means the stuff inside is a bit more valueble too, doesn't it?");
		AddSub(StoryCharacter.ENGINEER, "Yes... Yes, I guess it does.");

		CreateStory(); // 46: 

		CreateStory(); // 47: 

		CreateStory(); // 48: 

		CreateStory(); // 49: 

		CreateStory(); // 50: 


		// Narrative

		// AWAKENING 

		CreateStory(); // 51 AWAKENING - ONMIDSECTOR
		AddSub(StoryCharacter.BRIGHTMAN, "Looking good out there, @N. Considering you just spent six weeks on vacation, you're on top form.");
		AddSub(StoryCharacter.ENGINEER, "Oh, you were on vacation, @N? Where'd you go?");
		AddSub(StoryCharacter.PILOT, "Hehe...");
		AddSub(StoryCharacter.BRIGHTMAN, "Wahaha!");
		AddSub(StoryCharacter.ENGINEER, "Uh... what? What'd I say?");
		AddSub(StoryCharacter.PILOT, "I was in cryo-stasis for 6 weeks, Ren. It's procedure before the start of a new tour. Just a bit of relaxation, y'know?");
		AddSub(StoryCharacter.ENGINEER, "Oh. So you didn't go far?");
		AddSub(StoryCharacter.PILOT, "No. I was in my ship. In stasis.");
		AddSub(StoryCharacter.ENGINEER, "Oh. Sorry.");
		AddSub(StoryCharacter.BRIGHTMAN, "I think you two are going to get along just fine.");


		CreateStory(); // 52 AWAKENING - ONPREBOSSFIGHT
		AddSub(StoryCharacter.ENGINEER, "@N, you're approaching a BOSS unit of the Royal Empire.");
		AddSub(StoryCharacter.ENGINEER, "They're bigger and badder than the fleet ships you've met so far.");
		AddSub(StoryCharacter.ENGINEER, "But even big ships have weak spots. Shoot their weak spots, @N. That's how we'll win!");
		AddSub(StoryCharacter.PILOT, "Thanks, Ren, for telling me more stuff I already know.");
		AddSub(StoryCharacter.ENGINEER, "Hey, I'm just doing my job, @N. Tour anniversary procedures.");
		AddSub(StoryCharacter.PILOT, "Don't take it to heart. I always give my engineers a hard time. ");
		AddSub(StoryCharacter.PILOT, "Now, speaking of jobs... Watch a pro do theirs!");



		CreateStory(); // 53
		AddSub(StoryCharacter.ENGINEER, "This is the 13 Story. This will play mid sector.");
		AddSub(StoryCharacter.ENGINEER, "This is the 13 Story. This will play mid sector.");



		CreateStory(); // 54
		AddSub(StoryCharacter.ENGINEER, "This is the 14 Story. This will play boss sector");
		AddSub(StoryCharacter.ENGINEER, "This is the 14 Story. This will play boss sector");

		// ABOVE THE FOLD

		CreateStory(); // 55 ABOVE THE FOLD - ONENTERSECTOR
		AddSub(StoryCharacter.BRIGHTMAN, "The media are going NUTS over Project Gemini, @N. This is looking to be our best year to date.");
		AddSub(StoryCharacter.PILOT, "Glad to hear it. Just make sure my name's in big, BIG print.");
		AddSub(StoryCharacter.BRIGHTMAN, "I've got a lot of stuff to sort out before your annual address, so I'll be quiet on the COMMS for a while.");
		AddSub(StoryCharacter.BRIGHTMAN, "Kill some time with some routine clearance and I'll be in touch.");



		CreateStory(); // 56 ABOVE THE FOLD - ONMIDSECTOR
		AddSub(StoryCharacter.ENGINEER, "Hey @N. I've got a question.");
		AddSub(StoryCharacter.PILOT, "Okay. What is it?");
		AddSub(StoryCharacter.ENGINEER, "Um. Well, you've been involved in Project Gemini for six years now.");
		AddSub(StoryCharacter.ENGINEER, "And I'm just curious... What's it like?");
		AddSub(StoryCharacter.PILOT, "What's what like?");
		AddSub(StoryCharacter.ENGINEER, "Come on. Being a... You know, being a...");
		AddSub(StoryCharacter.PILOT, "Clone. You can say it.");
		AddSub(StoryCharacter.ENGINEER, "Sorry.");
		AddSub(StoryCharacter.PILOT, "A clone is exactly the same as a normal person. That's why they call it a clone.");
		AddSub(StoryCharacter.ENGINEER, "Yeah, I KNOW that, but...");
		AddSub(StoryCharacter.PILOT, "Truth is, I love being part of Project Gemini.");
		AddSub(StoryCharacter.PILOT, "The real me is currently sunbathing on a tropical shoreline with a cushty severance pay package.");
		AddSub(StoryCharacter.PILOT, "Meanwhile, an infinite amount of me's are protecting the free world from oppressors without the fear of death.");
		AddSub(StoryCharacter.PILOT, "What's not to love about that?");
		AddSub(StoryCharacter.ENGINEER, "When you put it that way, it does sound... fun.");
		AddSub(StoryCharacter.PILOT, "Daaaaamn RIGHT it's fun!");



		CreateStory(); // 57 ABOVE THE FOLD - ONPREPAREFORBOSS
		AddSub(StoryCharacter.BRIGHTMAN, "@N, I need a quote from you.");
		AddSub(StoryCharacter.PILOT, "Alright. What'll it be? Something inspiring? Something to tug the heart strings?");
		AddSub(StoryCharacter.BRIGHTMAN, "No, I need one for the suit sponsor, WARGEAR. About the comfort of your suit.");
		AddSub(StoryCharacter.PILOT, "Hmm. How about... \"Vanquishing Oppressors has never been so comfortable, thanks to my WARGEAR suit!\"");
		AddSub(StoryCharacter.BRIGHTMAN, "Awesome! That's just what I needed. I'll be back in touch later.");



		CreateStory(); // 58 ONPREBOSSFIGHT
		AddSub(StoryCharacter.ENGINEER, "Hey, @N... Your ship has been receiving some encrypted audio messages.");
		AddSub(StoryCharacter.PILOT, "Oh yeah? Can you decrypt them?");
		AddSub(StoryCharacter.ENGINEER, "I think--");
		AddSub(StoryCharacter.BRIGHTMAN, "Sorry Ren, that'll have to wait. @N, need another quote from ya.");
		AddSub(StoryCharacter.ENGINEER, "...");
		AddSub(StoryCharacter.PILOT, "Whatcha need, Major?");
		AddSub(StoryCharacter.BRIGHTMAN, "Something patriotic. Maybe a little sad.");
		AddSub(StoryCharacter.PILOT, "'No matter how far from the Alliance Republic I am...'");
		AddSub(StoryCharacter.PILOT, "'...I still see the glow of the city lights and hear the songs of my fellow free people.'");
		AddSub(StoryCharacter.BRIGHTMAN, "Perfection!");



		CreateStory(); // 59 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.ENGINEER, "So... About these encrypted messages.");
		AddSub(StoryCharacter.BRIGHTMAN, "That'll have to wait, Ren. @N needs some beauty sleep. Tomorrow is a big day.");
		AddSub(StoryCharacter.ENGINEER, "Right, of course...");
		AddSub(StoryCharacter.PILOT, "Roger that, Major. I need to practise my speech, anyway. See you guys in the morning. @N out.");



		CreateStory(); // 60 ONENTERCHAPTER
		AddSub(StoryCharacter.BRIGHTMAN, "Alright @N, ready for your annual broadcast?");
		AddSub(StoryCharacter.PILOT, "Ready, Major.");
		AddSub(StoryCharacter.BRIGHTMAN, "I'll count you down from 3.");
		AddSub(StoryCharacter.BRIGHTMAN, "3.. 2.. 1..");
		AddSub(StoryCharacter.COMPUTER, ">>  A/V RECORDING START");
		AddSub(StoryCharacter.PILOT, "Best day and all power to you, one and all! Gemini Pilot @N here once again, with my annual ‘Words of Heroism'.");
		AddSub(StoryCharacter.PILOT, "I'm not going to lie to you, free people of The Alliance Republic.");
		AddSub(StoryCharacter.PILOT, "Out here in this void of hostility, in this ultimate war against oppressors, there is little to smile about.");
		AddSub(StoryCharacter.PILOT, "That might make all of you at home wonder...");
		AddSub(StoryCharacter.PILOT, "'How is this hero of the Alliance still alive? How do you keep on going, no matter what?'");
		AddSub(StoryCharacter.PILOT, "Well, that's easy.");
		AddSub(StoryCharacter.PILOT, "I remember that I'm a FREE CHILD of the ALLIANCE!");
		AddSub(StoryCharacter.PILOT, "I remember that NO oppressor is too powerful to withstand the upholders of peace, beauty and success!");
		AddSub(StoryCharacter.PILOT, "Now, my friends, I gotta run. I have to get back to keeping the dark away from the light.");
		AddSub(StoryCharacter.PILOT, "Gemini Pilot @N, signing off.");
		AddSub(StoryCharacter.COMPUTER, ">>  A/V RECORDING END");
		AddSub(StoryCharacter.BRIGHTMAN, "Great job, @N. I especially loved the 'how am I still alive?' stuff. Moved me to tears.");
		AddSub(StoryCharacter.PILOT, "What can I say, Major? I've got a way with words.");
		AddSub(StoryCharacter.BRIGHTMAN, "Well, now I need you to have a way with battle. Back to your routine clearing, soldier.");
		AddSub(StoryCharacter.PILOT, "Roger that. I'll get strapped in.");



		CreateStory(); // 61 ONENTERSECTOR
		AddSub(StoryCharacter.ENGINEER, "Happy tour anniversary, @N.");
		AddSub(StoryCharacter.PILOT, "Thanks, Ren.");
		AddSub(StoryCharacter.ENGINEER, "So hey, if you're not too busy thinking up more quotes, I decrypted those messages you received.");
		AddSub(StoryCharacter.PILOT, "Do I detect a little hostility from my new engineer?");
		AddSub(StoryCharacter.ENGINEER, "Sorry. I just don't get off on the showbiz stuff.");
		AddSub(StoryCharacter.PILOT, "Need to keep the people's spirits high, Ren.");
		AddSub(StoryCharacter.ENGINEER, "I guess. Anyway, here are the messages.");
		AddSub(StoryCharacter.COMPUTER, ">> 'The soldier is just...'");
		AddSub(StoryCharacter.COMPUTER, ">> 'A branded, plastic, child's toy.'");
		AddSub(StoryCharacter.COMPUTER, ">> 'Under heat, it melts.'");
		AddSub(StoryCharacter.PILOT, "Ooookay...");
		AddSub(StoryCharacter.ENGINEER, "At first, I thought it was just a message received in error. But then there's was this...");
		AddSub(StoryCharacter.COMPUTER, ">> 'It is my anniversary too, dear friend.'");
		AddSub(StoryCharacter.PILOT, "Sounds like someone wants to play. Inform the Major and see if you can get a trace on the sender.");
		AddSub(StoryCharacter.PILOT, "I'll have a think. And blow some stuff up.");
		AddSub(StoryCharacter.ENGINEER, "Copy that.");



		CreateStory(); // 62 ONMIDSECTOR
		AddSub(StoryCharacter.ENGINEER, "@N, we compared the voice in the messages with known Oppressors. We have a match.");
		AddSub(StoryCharacter.PILOT, "So, who is this new friend I've made?");
		AddSub(StoryCharacter.ENGINEER, "This isn't just a run of the mill Royal Empire goon. You've got the attention of Viceroy Francois.");
		AddSub(StoryCharacter.PILOT, "Wow, I'm honored.");
		AddSub(StoryCharacter.BRIGHTMAN, "This is big news, @N. The Upper Hierarchy have noticed you. The Viceroy is only the beginning.");
		AddSub(StoryCharacter.BRIGHTMAN, "Take him out, and you could be trading shots with the Royal Family soon.");



		CreateStory(); // 63 ONPREBOSSFIGHT
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING FOREIGN COMMS TRANSMISSION");
		AddSub(StoryCharacter.VICEROY, "'Little toy soldier \nLaden with the fingerprints \nOf its child owner.'");
		AddSub(StoryCharacter.PILOT, "Is that... Is that a poem? That's appalling.");
		AddSub(StoryCharacter.VICEROY, "Of course, I do not expect a member of the Alliance Republic to appreciate my work.");
		AddSub(StoryCharacter.PILOT, "So, I've blown up a few thousand of your friends and now I've got your attention. Wanna wrestle?");
		AddSub(StoryCharacter.VICEROY, "No, toy soldier. I just want to know you. Such a curious yet terrifying creation...");



		CreateStory(); // 64 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.BRIGHTMAN, "@N. We've figured out why the Viceroy referred to today as his anniversary.");
		AddSub(StoryCharacter.PILOT, "Is today the day he tried on his first dress?");
		AddSub(StoryCharacter.BRIGHTMAN, "You remember the attack last year on the Triton Colony?");
		AddSub(StoryCharacter.BRIGHTMAN, "Sixteen thousand civilians killed by a tidal wave.");
		AddSub(StoryCharacter.PILOT, "That... that was HIM?");
		AddSub(StoryCharacter.BRIGHTMAN, "I'm afraid so.");
		AddSub(StoryCharacter.PILOT, "Son of a...");
		AddSub(StoryCharacter.BRIGHTMAN, "He might seem like a joke, but he's deadly serious. You need to stop him @N!");
		AddSub(StoryCharacter.PILOT, "...I'm on it, Major.");


		// RETRIBUTION

		CreateStory(); // 65 ONENTERCHAPTER
		AddSub(StoryCharacter.COMPUTER, ">> GOOD MORNING, GEMINI PILOT @NN.");
		AddSub(StoryCharacter.PILOT, "Mmm... Morning, computer.");
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING AUDIO MESSAGE FROM MOTHER");
		AddSub(StoryCharacter.PILOT, "M-Mother?");
		AddSub(StoryCharacter.MOTHER, "Hello sweetie! I haven't heard from you in a while so I thought I'd send a quick 'hi' and a kiss. Mwah!");
		AddSub(StoryCharacter.MOTHER, "Ok, I'll stop embarrassing you now and let you get back to saving the galaxy. Love you!");
		AddSub(StoryCharacter.COMPUTER, ">> END AUDIO MESSAGE");
		AddSub(StoryCharacter.PILOT, "Mother... How long has it been since I replied to her, I wonder?");



		CreateStory(); // 66 ONENTERSECTOR
		AddSub(StoryCharacter.ENGINEER, "Morning, @N.");
		AddSub(StoryCharacter.PILOT, "Hey Ren.");
		AddSub(StoryCharacter.ENGINEER, "The Viceroy has been trying to comms link with you for the past few hours.");
		AddSub(StoryCharacter.ENGINEER, "We figured we'd keep him blocked until you were up and about.");
		AddSub(StoryCharacter.PILOT, "Go ahead and patch him in.");
		AddSub(StoryCharacter.ENGINEER, "Alright. And hey...");
		AddSub(StoryCharacter.ENGINEER, "About that attack on Triton Colony. I had... I had some fam-");
		AddSub(StoryCharacter.PILOT, "Some... What?");
		AddSub(StoryCharacter.ENGINEER, "Ah, nevermind. It's not important anyway. I'll patch him in now.");
		AddSub(StoryCharacter.VICEROY, "'The mass-produced toy...'");
		AddSub(StoryCharacter.PILOT, "Oh boy.");
		AddSub(StoryCharacter.VICEROY, "'For some reason, still needs sleep.'");
		AddSub(StoryCharacter.PILOT, "Good morning to you, too.");
		AddSub(StoryCharacter.VICEROY, "'Irony unseen.'");
		AddSub(StoryCharacter.PILOT, "That wasn't your best. You're starting to get sloppy.");
		AddSub(StoryCharacter.VICEROY, "I agree. Indeed, my best work of art was on the Triton Colony.");
		AddSub(StoryCharacter.VICEROY, "But I don't suppose you Alliance cretins would appreciate such majesty.");
		AddSub(StoryCharacter.PILOT, "You think the mass murder of innocent people is majesty?");
		AddSub(StoryCharacter.PILOT, "You're demented. I'm coming for you, Viceroy Francois.");
		AddSub(StoryCharacter.VICEROY, "And I wait for you, Gemini Pilot @N.");


		CreateStory(); // 67 ONMIDSECTOR
		AddSub(StoryCharacter.VICEROY, "I can see why a small minded individual like yourself may not UNDERSTAND my work.");
		AddSub(StoryCharacter.PILOT, "You know what they say about people with small minds. Big lasers.");
		AddSub(StoryCharacter.VICEROY, "Ha ha ha! Very witty. Perhaps we can collaborate on some comical prose.");
		AddSub(StoryCharacter.PILOT, "Perhaps I can leave your corpse in a comical pose.");



		CreateStory(); // 68 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.VICEROY, "Very good, little toy soldier. You are making your way closer to me.");
		AddSub(StoryCharacter.VICEROY, "I wonder, perhaps, if I might provide you with some entertainment during your trip.");
		AddSub(StoryCharacter.PILOT, "You already entertain me enough, Viceroy.");
		AddSub(StoryCharacter.VICEROY, "Oh, but I do believe you will LOVE what I have planned next.");


		// FIRST, FIVE

		CreateStory(); // 69: ONENTERSECTOR
		AddSub(StoryCharacter.BRIGHTMAN, "Just wanted to say, @N... your banter with the Viceroy is top-notch.");
		AddSub(StoryCharacter.PILOT, "Thanks, Major.");
		AddSub(StoryCharacter.BRIGHTMAN, "We're broadcasting a little memorial show about the Triton Colony.");
		AddSub(StoryCharacter.BRIGHTMAN, "We want people to see who the Viceroy is, so they know who to hate.");
		AddSub(StoryCharacter.PILOT, "Good idea.");
		AddSub(StoryCharacter.BRIGHTMAN, "Also, to show that our top pilot is able to deflect his Empire evangelism with cutting-edge humor!");
		AddSub(StoryCharacter.PILOT, "Great idea. In that case, I'll bring it up a gear.");

		CreateStory(); // 70: ONMIDSECTOR
		AddSub(StoryCharacter.VICEROY, "'Above, the sky is lit. \nBy vessels that rain down fire. \nThose great stars of war.'");
		AddSub(StoryCharacter.PILOT, "So THAT'S what the light is! Here I was thinking it was your night-light for keeping big bad Gemini Pilots away.");

		CreateStory(); // 71 ONPREBOSSFIGHT
		AddSub(StoryCharacter.VICEROY, "'They sleep peacefully. \nUnaware, that soon they will... \nSlumber evermore.'");
		AddSub(StoryCharacter.PILOT, "You DO know I'm a clone, right? You can kill me 'for evermore', but I'll just keep waking up and coming at you.");
		AddSub(StoryCharacter.VICEROY, "'Ten seeds are planted. \nFor every tree cut down. \nBut growing needs time.'");

		CreateStory(); // 72 POSTBOSSFIGHT
		AddSub(StoryCharacter.ENGINEER, "You know, those poems the Viceroy are reciting are kinda freaking me out.");
		AddSub(StoryCharacter.PILOT, "That's what he wants. He thinks he can scare us with his fancy cryptic poems.");
		AddSub(StoryCharacter.ENGINEER, "No, it's something else... Like, I dunno.");
		AddSub(StoryCharacter.ENGINEER, " As if he's talking about something else.");
		AddSub(StoryCharacter.PILOT, "Don't read into them too hard, Ren. Let him waste his breath.");
		AddSub(StoryCharacter.PILOT, "I'll catch up with him soon enough and shut him up for good.");



		// THEN, SEVEN


		CreateStory(); // 73 ONENTERSECTOR
		AddSub(StoryCharacter.VICEROY, "'Smiles for cameras \nAre the only willing gifts \nFrom the self-obsessed.'");
		AddSub(StoryCharacter.PILOT, "Hey, that's not true. I've not plenty of other gifts for you.");
		AddSub(StoryCharacter.PILOT, "They're really big and shiny. Why don't you come get 'em?");
		AddSub(StoryCharacter.BRIGHTMAN, "Hah! Stick it to him, @N!");


		CreateStory(); // 74 ONMIDSECTOR
		AddSub(StoryCharacter.VICEROY, "'With quick sleight of hand \nThe illusionist distracts \nHis grinning public.'");
		AddSub(StoryCharacter.PILOT, "I can do magic, too. See those Royal Empire ships coming at me? I'm about to make 'em disappear.");
		AddSub(StoryCharacter.BRIGHTMAN, "Bahaha! @N, he can't even keep up!");


		CreateStory(); // 75 ONPREBOSSFIGHT
		AddSub(StoryCharacter.PILOT, "Hey Viceroy, I've got one for you.");
		AddSub(StoryCharacter.PILOT, "'I am going to \nShoot you in the face with a \nReal big gun. Ha ha.'");
		AddSub(StoryCharacter.PILOT, "Whaddya think?");
		AddSub(StoryCharacter.PILOT, "Hello? Viceroy? What's wrong? Run out of poems?");
		AddSub(StoryCharacter.BRIGHTMAN, "Hah! He's dumbstruck!");
		AddSub(StoryCharacter.PILOT, "He knows I'm getting closer. He can feel the pressure.");
		AddSub(StoryCharacter.ENGINEER, "...I don't think he's interested in your replies.");
		AddSub(StoryCharacter.BRIGHTMAN, "Come on, Ren. The Viceroy's obviously upset by @N's crushing retort.");


		CreateStory(); // 76 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.VICEROY, "Trembling, is the web, \nAs the fly is wrapped within. \nPleased, is the spider.");
		AddSub(StoryCharacter.PILOT, "Oh, there you are! I was starting to worry about you.");



		// SECTOR 7: AND FINALLY, FIVE



		CreateStory(); // 77 ONENTERSECTOR
		AddSub(StoryCharacter.BRIGHTMAN, "So we're calling this Viceroy debacle 'The Warrior VS The Clown.'");
		AddSub(StoryCharacter.BRIGHTMAN, "I've got quotes up to my eyeballs, @N, but a few more can't hurt.");
		AddSub(StoryCharacter.BRIGHTMAN, "See if you can break him out of this dumb poem spree he's on.");
		AddSub(StoryCharacter.ENGINEER, "We've picked up on a direct trace from his comms link, @N. As far as we can tell, he's in your sector.");
		AddSub(StoryCharacter.PILOT, "Got it. Watch out, Viceroy. Here I come.");
		
		
		CreateStory(); // 78 ONMIDSECTOR
		AddSub(StoryCharacter.VICEROY, "'Pleased is the hero. \nAs he walks the bloody path, \nTo the Hall of Fame.'");
		AddSub(StoryCharacter.PILOT, "Unlike you, I don't wear pretty frills and lace. So I'm not worried about a little blood on my clothes.");
		AddSub(StoryCharacter.VICEROY, "Ah... but it depends whose blood it is, does it not?");
		AddSub(StoryCharacter.PILOT, "Whoa! He speaks at last! Does this mean you've finished your great book of poems?");
		AddSub(StoryCharacter.VICEROY, "I have one more for you, toy soldier.");
		AddSub(StoryCharacter.PILOT, "I'm dying to hear it.");
		AddSub(StoryCharacter.VICEROY, "Come to me and I will recite it to you in person.");
		
		CreateStory(); // 79 ONPREPAREFORBOSS
		AddSub(StoryCharacter.ENGINEER, "According to the trace analysis, the incoming enemy is the source of the COMMS link.");
		AddSub(StoryCharacter.ENGINEER, "So you're about to go head-to-head with the Viceroy, @N. Better get prepped.");
		
		
		CreateStory(); // 80 ONPREBOSSFIGHT
		AddSub(StoryCharacter.ENGINEER, "Wait a sec... Something's wrong here.");
		AddSub(StoryCharacter.PILOT, "What is it, Ren?");
		AddSub(StoryCharacter.ENGINEER, "The trace analysis... It's coming up different. I don't understand. ");
		AddSub(StoryCharacter.ENGINEER, "A minute ago, the ship you're about to encounter was matching the Viceroy's COMMS link.");
		AddSub(StoryCharacter.PILOT, "So... what, this isn't the Viceroy?");
		AddSub(StoryCharacter.ENGINEER, "No...");
		AddSub(StoryCharacter.VICEROY, "'Now their eyes widen, \nFor too long, they were focused, \nOn the wrong target.");
		AddSub(StoryCharacter.ENGINEER, "Oh no.");
		AddSub(StoryCharacter.BRIGHTMAN, "What? What is it, Ren?!");
		AddSub(StoryCharacter.ENGINEER, "We've just received a video link. It's of Stardawn, one of our outer colonies.");
		AddSub(StoryCharacter.PILOT, "What?!");
		AddSub(StoryCharacter.BRIGHTMAN, "You better start talking sense, Engineer Ren. What the hell is going on?");
		AddSub(StoryCharacter.ENGINEER, "It's... on fire. The whole place is lit up. I KNEW it!");
		AddSub(StoryCharacter.ENGINEER, "He's been distracting us with all the theatrics this whole time, transmitting from a different ship!");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.ENGINEER, "While you've been chasing him around and joking about his poems, he's attacked a vulnerable colony!");
		AddSub(StoryCharacter.VICEROY, "And thus the final page of the book is turned.");
		AddSub(StoryCharacter.BRIGHTMAN, "DAMNIT! I have to sort this mess out back here... @N, find him! Find him now!");
		AddSub(StoryCharacter.PILOT, "Yes sir.");

		CreateStory(); // 81 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.PILOT, "...How many, Ren?");
		AddSub(StoryCharacter.ENGINEER, "...Just under six thousand confirmed dead. Many more missing.");
		AddSub(StoryCharacter.PILOT, "Damn it!");
		AddSub(StoryCharacter.ENGINEER, "He planned it all along, @N. And we walked right into it.");
		AddSub(StoryCharacter.PILOT, "Well now it's time to walk out the other side. Put all your efforts into finding him, Ren. ");
		AddSub(StoryCharacter.PILOT, "Those people deserve vengeance.");

		// SECTOR 8: DRILL TO THE BRAIN

		CreateStory(); // 82 ONENTERCHAPTER
		AddSub(StoryCharacter.PILOT, "Ah!");
		AddSub(StoryCharacter.COMPUTER, ">> GOOD MORNING, GEMINI PILOT @NN.");
		AddSub(StoryCharacter.PILOT, "Huh.. Ah... Nngghh!");
		AddSub(StoryCharacter.PILOT, "My... my head. Ow.");
		AddSub(StoryCharacter.PILOT, "Ren, you there?");
		AddSub(StoryCharacter.ENGINEER, "What's up, @N?");
		AddSub(StoryCharacter.PILOT, "I've got this.. pain in my head. It's like...");
		AddSub(StoryCharacter.ENGINEER, "What, like a headache? What does it feel like?");
		AddSub(StoryCharacter.PILOT, "Like someone's drilling into my head. I don't know how else to describe it.");
		AddSub(StoryCharacter.ENGINEER, "Alright, sit still. Let me run a vitals analysis.");
		AddSub(StoryCharacter.ENGINEER, "...");
		AddSub(StoryCharacter.ENGINEER, "Hm... Nothing coming up on the report.");
		AddSub(StoryCharacter.PILOT, "Nevermind, it's faded now. That was weird.");
		AddSub(StoryCharacter.ENGINEER, "You've been through a lot in the last few days, @N.");
		AddSub(StoryCharacter.ENGINEER, "The news about the Stardawn Colony hit us all pretty hard. It's called stress.");
		AddSub(StoryCharacter.PILOT, "Pfff. Stress, shmess. It's probably my brain telling me to get the hell up and find the Viceroy.");
		
		
		CreateStory(); // 83 ONENTERSECTOR
		AddSub(StoryCharacter.BRIGHTMAN, "This whole Stardawn Colony mess has crippled us, @N.");
		AddSub(StoryCharacter.BRIGHTMAN, "It's going to take a lot of work to spin this the right way back home.");
		AddSub(StoryCharacter.PILOT, "'Spin this the right way?' What do you mean?");
		AddSub(StoryCharacter.BRIGHTMAN, "Don't worry about it. That's my job. You need to do yours, soldier.");
		AddSub(StoryCharacter.BRIGHTMAN, "The big wigs aren't impressed by your distraction.");
		AddSub(StoryCharacter.BRIGHTMAN, "You need to show them you haven't lost us the chance to start infiltrating their Upper Hierarchy.");
		AddSub(StoryCharacter.PILOT, "I haven't lost anything, Major. You'll see.");


		// SECTOR 9: DESCTRUCTIVE FEEDBACK


		CreateStory(); // 84 ONENTERSECTOR
		AddSub(StoryCharacter.PILOT, "Alright, no screwing around now, Ren. The Viceroy goes down. TODAY.");
		AddSub(StoryCharacter.ENGINEER, "Copy that, @N. I've got your back. I'll working on finding him.");
		
		
		CreateStory(); // 85 ONENTERSUBSECTOR 9.2
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING FOREIGN COMMS TRANSMISSION");
		AddSub(StoryCharacter.HIDDENIRIS, "HEY! Get the hell out of here!");
		AddSub(StoryCharacter.PILOT, "Who's gonna make me?");
		AddSub(StoryCharacter.HIDDENIRIS, "Go eat a black hole, ya freakin' fashion model.");
		AddSub(StoryCharacter.PILOT, "Who the hell is this?! Ren, do you have a trace?");
		AddSub(StoryCharacter.ENGINEER, "I do, but there's no pilot or ship info. I think... I think it's a pirate!");
		AddSub(StoryCharacter.HIDDENIRIS, "It's a pirate! It's a PIIII-RAAATE! WAAHH! Go back to Glossyland, jerk-offs.");
		AddSub(StoryCharacter.COMPUTER, ">> COMMS TRANSMISSION TERMINATED");
		AddSub(StoryCharacter.ENGINEER, "What was HER problem?");
		AddSub(StoryCharacter.PILOT, "She could be in the Viceroy's pocket. Trying to distract me.");
		AddSub(StoryCharacter.PILOT, "Keep an eye out for her signal. I don't want anymore surprises.");


		CreateStory(); // 86 ONMIDSECTOR
		AddSub(StoryCharacter.VICEROY, "You have not spoken to me for some time, dear toy soldier. I worry my work has offended you.");
		AddSub(StoryCharacter.PILOT, "Offended me? Nah. Your words don't mean a damn thing. That's the problem with you Royal Empire lame-o's.");
		AddSub(StoryCharacter.PILOT, "You suck at entertaining. You're like the worst ever audition on 'S-Factor' back home.");
		AddSub(StoryCharacter.VICEROY, "AH! Do not say that disgusting title to me, Alliance SCUM. The humble subjects Royal Empire are masters of prose.");
		AddSub(StoryCharacter.VICEROY, "Of REAL literature and art. Unlike your perverted, brain-washing TRASH you force-feed to the people!");
		AddSub(StoryCharacter.PILOT, "BZZZZZT! It's a no from me, Viceroy, sorry. You just don't have the 'S-Factor'.");
		
		
		CreateStory(); // 87 ONPREPAREFORBOSS
		AddSub(StoryCharacter.PILOT, "Now who's being quiet? Still pissed off that I said you suck?");
		AddSub(StoryCharacter.VICEROY, "You seem so keen to forget about the thousands of minds I liberated from your botoxed regime.");
		AddSub(StoryCharacter.VICEROY, "So eager are you to crack jokes and not think of your failure.");


		CreateStory(); // 88 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.VICEROY, "I sense you grow closer, little fly. Please, continue to shake my web.");
		AddSub(StoryCharacter.PILOT, "You'll be sensing a whole world of hurt, soon. That's a promise.");




		// SECTOR 10: The Final Syllable
		
		
		CreateStory(); // 89 ONENTERSECTOR
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING AUDIO MESSAGE FROM MOTHER");
		AddSub(StoryCharacter.MOTHER, "Heeeeellloooo! Good morning, sweetheart! Or good afternoon... or night!");
		AddSub(StoryCharacter.MOTHER, "I'm not sure what time these silly things actually arrive...");
		AddSub(StoryCharacter.PILOT, "Nnggh.. ..mm...");
		AddSub(StoryCharacter.MOTHER, "Anyway, I hope you're having fun out there, saving the world!");
		AddSub(StoryCharacter.MOTHER, "I'm cooking spaghetti tonight, so I thought of you! My little spaghetti goblin!");
		AddSub(StoryCharacter.PILOT, "...mm? Huh?");
		AddSub(StoryCharacter.MOTHER, "Well, better get back to chopping onions! Love you, sweetie!");
		AddSub(StoryCharacter.COMPUTER, ">> END AUDIO MESSAGE FROM MOTHER");
		AddSub(StoryCharacter.PILOT, "Jeez... Mom... I'm trying to concentrate here.");
		AddSub(StoryCharacter.PILOT, "Spaghetti goblin. For crying out loud.");

		
		CreateStory(); // 90 ONMIDSECTOR
		AddSub(StoryCharacter.VICEROY, "'Little toy soldier, where did you go?'");
		AddSub(StoryCharacter.PILOT, "Oh, great. Now he's written a song.");
		AddSub(StoryCharacter.VICEROY, "'I once saw you regularly, now never again.'");
		AddSub(StoryCharacter.VICEROY, "'We are enemies, toy soldier, but also truly friends.'");
		AddSub(StoryCharacter.PILOT, "If you say so.");
		AddSub(StoryCharacter.VICEROY, "'Like light and dark, yin and yang, we are tied.'");
		AddSub(StoryCharacter.PILOT, "I'd rather be tied to a shark.");
		
		
		CreateStory(); // 91 ONPREPAREFORBOSS
		AddSub(StoryCharacter.VICEROY, "'But forces greater than us dictate our dusty path.'");
		AddSub(StoryCharacter.PILOT, "Speaking of great forces... Computer, arm the missiles.");
		AddSub(StoryCharacter.VICEROY, "The end is nigh. My time has come.");
		AddSub(StoryCharacter.PILOT, "'My time has come?' So you admit I'm about to kick your sorry ass?");
		AddSub(StoryCharacter.PILOT, "Wow, this has reached a whole new level of sad.");
		AddSub(StoryCharacter.VICEROY, "Of course I will fall, one day, to the little toy soldier.");
		AddSub(StoryCharacter.VICEROY, "For the children have purchased so many of them.");

		
		CreateStory(); // 92 ONPREBOSSFIGHT
		AddSub(StoryCharacter.ENGINEER, "@N! This is it. His signal is coming out loud and clear.");
		AddSub(StoryCharacter.ENGINEER, "That's HIS ship approaching you. Get ready!");
		AddSub(StoryCharacter.PILOT, "Viceroy. At last.");
		AddSub(StoryCharacter.VICEROY, "Generations from now, my people will recite the tale of 'The Viceroy and the Toy Soldier.'");
		AddSub(StoryCharacter.VICEROY, "Then, like you, I will live forever.");
		AddSub(StoryCharacter.PILOT, "Not if we win, Viceroy.");
		AddSub(StoryCharacter.PILOT, "Then the only tale you'll be in is 'Little Turdy Viceroy and His Lame Book of Poems.'");
		AddSub(StoryCharacter.PILOT, "It'll be kid's bestseller.");
		AddSub(StoryCharacter.VICEROY, "Enough of your comedy!");
		
		
		CreateStory(); // 93 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.VICEROY, "Do... do I really not have the 'S-Factor'...?");
		AddSub(StoryCharacter.VICEROY, "UUUAAAAAGH!");
		AddSub(StoryCharacter.BRIGHTMAN, "You did it, @N! The Viceroy is stardust.");
		AddSub(StoryCharacter.PILOT, "Was there ever any doubt, Major?");
		AddSub(StoryCharacter.BRIGHTMAN, "You've done us proud, soldier. I want you to get cleaned up and take the rest of the day off. ");
		AddSub(StoryCharacter.BRIGHTMAN, "Tomorrow morning, I need a fresh quote on how you're feeling. ");
		AddSub(StoryCharacter.BRIGHTMAN, "Good work turning this back in our favour, @N. Brightman out.");
		AddSub(StoryCharacter.ENGINEER, "Good job, @N. I never doubted you for a second.");
		AddSub(StoryCharacter.ENGINEER, "You've avenged our fallen brothers and sisters on Stardawn and Triton.");
		AddSub(StoryCharacter.PILOT, "Thanks, Ren. You get some rest, too.");
		AddSub(StoryCharacter.PILOT, "This event is going to cause some ripples in the Royal Empire's Upper Hierarchy.");
		AddSub(StoryCharacter.PILOT, "We need to be ready for them.");


		// SECTOR 11: RIPPLES


		CreateStory(); // 94 ONENTERSECTOR
		AddSub(StoryCharacter.BRIGHTMAN, "The media are having a field day with your work on the Viceroy, @N.");
		AddSub(StoryCharacter.BRIGHTMAN, "Which means the bigwigs are happy. Which means I'm happy.");
		AddSub(StoryCharacter.PILOT, "I live to make the Alliance Republic happy, Major.");
		AddSub(StoryCharacter.BRIGHTMAN, "Apart from the increase of ships in nearby sectors...");
		AddSub(StoryCharacter.BRIGHTMAN, "...there hasn't been any official response yet from the Royal Empire since you took out the Viceroy.");
		AddSub(StoryCharacter.PILOT, "Oh, it'll come. Don't worry, I'm patient.");


		CreateStory(); // 95 ONMIDSECTOR
		AddSub(StoryCharacter.HIDDENIRIS, "You have GOT to be kidding me.");
		AddSub(StoryCharacter.ENGINEER, "@N, it's that irate pirate again.");
		AddSub(StoryCharacter.PILOT, "This is Gemini Pilot @N of the Alliance Republic.");
		AddSub(StoryCharacter.PILOT, "State your name and intention before continuing COMMS with this ship.");
		AddSub(StoryCharacter.HIDDENIRIS, "Name and intention!? NAME AND INTENTION?!");
		AddSub(StoryCharacter.PILOT, "That's right.");
		AddSub(StoryCharacter.HIDDENIRIS, "YOU TOOK MY FARM, YOU MASSIVE ASS!");
		AddSub(StoryCharacter.PILOT, "Your... Your farm?");
		AddSub(StoryCharacter.HIDDENIRIS, "And if HOGGING my loot wasn't enough, now you've pissed off the Royal Dumbpire.");
		AddSub(StoryCharacter.HIDDENIRIS, "This sector used to be an easy way for a pirate to make an honest living.");
		AddSub(StoryCharacter.HIDDENIRIS, "Now it's crawling with those goons and YOU.");
		AddSub(StoryCharacter.PILOT, "Honest Living...? Pirate...?");
		AddSub(StoryCharacter.PILOT, "Are you a part-time comedienne, too?");
		AddSub(StoryCharacter.HIDDENIRIS, "OH YOU COMPLETE--");
		AddSub(StoryCharacter.COMPUTER, ">> AUTO-CENSOR ENGAGED");
		AddSub(StoryCharacter.HIDDENIRIS, "******* ******* PIECE OF ***** LICKING ****POT.");
		AddSub(StoryCharacter.PILOT, "Thanks for the auto-censor, Ren.");
		AddSub(StoryCharacter.ENGINEER, "I knew she'd be a potty mouth.");
		AddSub(StoryCharacter.HIDDENIRIS, "This ain't over, *****.");

		CreateStory(); // 96 ONPREBOSSFIGHT
		AddSub(StoryCharacter.BRIGHTMAN, "I hear you've been getting harassed by a pirate, @N.");
		AddSub(StoryCharacter.PILOT, "Hardly harassed, sir. She just shows up, shouts a lot, then flies away.");
		AddSub(StoryCharacter.BRIGHTMAN, "Well, don't feel like you need to be friendly with pirates. They can't be trusted.");
		AddSub(StoryCharacter.BRIGHTMAN, "If she continues to show up, you have my permission to take her down.");
		AddSub(StoryCharacter.PILOT, "Good to know, Major. Thanks.");


		CreateStory(); // 97 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.ENGINEER, "Hey @N, you've received an e-mail.");
		AddSub(StoryCharacter.PILOT, "An e-mail?! Jeez. What year are we in?");
		AddSub(StoryCharacter.ENGINEER, "It says the sender is... ");
		AddSub(StoryCharacter.ENGINEER, "Crown Prosecutor Julian Trebon of the All Encompassing, All Loving Royal Empire.");
		AddSub(StoryCharacter.PILOT, "What are you waiting for, Engineer Ren? Read it.");
		AddSub(StoryCharacter.ENGINEER, "Ah... yes sir!");
		AddSub(StoryCharacter.ENGINEER, "'The high-honorable Julian Trebon, Crown Prosecutor of our regal Royal Empire... '");
		AddSub(StoryCharacter.ENGINEER, "'...has hereby opened an official investigation into the murder of the most honourable Viceroy Francis.'");
		AddSub(StoryCharacter.ENGINEER, "'The accused, Gemini Pilot @N of the terrorist organisation known as the Alliance Republic...'");
		AddSub(StoryCharacter.ENGINEER, "'...is required to attend trial and face a righteous verdict by the Crown Prosecutor.'");
		AddSub(StoryCharacter.ENGINEER, "'You will be allowed safe boarding on to my vessel, where the trial shall be held.'");
		AddSub(StoryCharacter.ENGINEER, "'We await your reply to RSVP attendance to the trial.'");
		AddSub(StoryCharacter.ENGINEER, "'In the name of the noble and blessed family of the Royal Empire.'");
		AddSub(StoryCharacter.ENGINEER, "'Yours faithfully, Crown Prosecutor Julian Trebon.'");
		AddSub(StoryCharacter.PILOT, "They're taking me to COURT? Via E-MAIL?!");
		AddSub(StoryCharacter.BRIGHTMAN, "Ren, send them a reply. State that Gemini Pilot @N refuses to take part in a trial held by Oppressors.");
		AddSub(StoryCharacter.BRIGHTMAN, "Tell them they'll have to come arrest @N if they want their trial.");
		AddSub(StoryCharacter.PILOT, "Nice thinking, sir. Bring him to me.");


		// SECTOR 12: NOTHING TO WRITE HOME ABOUT


		CreateStory(); // 98 ONENTERCHAPTER
		AddSub(StoryCharacter.PILOT, "But yeah, anyway... things are going okay here. Blowing stuff up. ");
		AddSub(StoryCharacter.PILOT, "Fighting against oppression. You know, the usual. Ermm...");
		AddSub(StoryCharacter.PILOT, "But I... miss yo- Ah, no. Damnit. Computer, delete message.");
		AddSub(StoryCharacter.COMPUTER, ">> MESSAGE TO 'MOTHER' DELETED");
		AddSub(StoryCharacter.PILOT, "I'll do this later. My head's not in it.");
		
		
		CreateStory(); // 99 ONENTERSECTOR
		AddSub(StoryCharacter.ENGINEER, "Morning @N. The Major's reply to the e-mail worked a treat.");
		AddSub(StoryCharacter.ENGINEER, "You've been receiving a COMMS request from an unknown source while you were in bed. It's got to be them.");
		AddSub(StoryCharacter.PILOT, "Great. Patch it through, Ren.");
		AddSub(StoryCharacter.PILOT, "This is Gemini Pilot @N, representative of brave Alliance Republic.");
		AddSub(StoryCharacter.CROWN, "Good day to you, Gemini Pilot @N. I am Crown Prosecutor Julian Trebon.");
		AddSub(StoryCharacter.CROWN, "Thank you for your reply to my electronic mail.");
		AddSub(StoryCharacter.PILOT, "Good day? Thank you? You feeling okay there, mister Royal Empire man?");
		AddSub(StoryCharacter.CROWN, "It is required of the Crown Prosecutor to maintain the highest standards of formality and polite behaviour.");
		AddSub(StoryCharacter.CROWN, "Even when dealing with the enemy.");
		AddSub(StoryCharacter.PILOT, "Riiiight.. Look, can I call you back? I'm kinda in the middle of blowing up your friends.");
		
		CreateStory(); // 100 ONMMIDSECTOR
		AddSub(StoryCharacter.CROWN, "You have chosen to decline the invitation to attend trial...");
		AddSub(StoryCharacter.CROWN, "To determine your fate against the charges of the murder of our Viceroy.");
		AddSub(StoryCharacter.PILOT, "Oh, hi. Yep. Yep, I decline.");
		AddSub(StoryCharacter.CROWN, "It is nonetheless my duty to continue the investigation into the charges made and ultimately conclude with a verdict.");
		AddSub(StoryCharacter.CROWN, "The trial shall commence remotely. I will continue to contact you to acquire information for use in the trial.");
		AddSub(StoryCharacter.CROWN, "Your complete cooperation will be appreciated, and will aid in reaching a trial date in a shorter period of time. ");
		AddSub(StoryCharacter.PILOT, "This guy likes to talk, huh?");
		AddSub(StoryCharacter.BRIGHTMAN, "We can't lose the attention of the Upper Hierarchy, @N.");
		AddSub(StoryCharacter.BRIGHTMAN, "Play along with his game so you have a chance of coming head to head with him.");
		AddSub(StoryCharacter.PILOT, "Alright, Crown Prosecutor. I'll answer your questions. Just try and keep them snappy. Got it?");
		
		
		CreateStory(); // 101 ONPREBOSSFIGHT
		AddSub(StoryCharacter.CROWN, "You have destroyed many Royal Empire vessels since your arrival. ");
		AddSub(StoryCharacter.CROWN, "You continue to do so, still. Is this your mission, Gemini Pilot?");
		AddSub(StoryCharacter.PILOT, "Clear sectors from Oppressor activity. Yep, you got it. That's what I do.");
		AddSub(StoryCharacter.CROWN, "Are you aware of the number of vessels you have destroyed to date?");
		AddSub(StoryCharacter.PILOT, "I can't count that high, your honor.");


		CreateStory(); // 102 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.CROWN, "So then... the Gemini Pilot has a complete disregard for the number of murders committed.");
		AddSub(StoryCharacter.PILOT, "I could say the same about the Viceroy and other Royal Empire servants.");
		AddSub(StoryCharacter.CROWN, "The loss of innocent lives in strategic military attacks is, sometimes, inevitable.");
		AddSub(StoryCharacter.PILOT, "Strategic military attacks?! He set the Stardawn colony on FIRE! It was ONLY innocent lives!");
		AddSub(StoryCharacter.CROWN, "I see the ignorance of the Gemini Pilot is deeply rooted.");
		AddSub(StoryCharacter.PILOT, "Say what?");
		AddSub(StoryCharacter.CROWN, "That will do for today. Thank you, Gemini Pilot @N.");
		AddSub(StoryCharacter.PILOT, "...");



		// SECTOR 13: AHOY MATEY


		CreateStory(); // 103 ONENTERSECTOR
		AddSub(StoryCharacter.ENGINEER, "That Crown Prosecutor is a whole new level of crazy.");
		AddSub(StoryCharacter.ENGINEER, "Overly polite and gathering information for a 'trial'?");
		AddSub(StoryCharacter.PILOT, "When you build a nation based on frills, lace and big words it leads to a nation of crazy people.");
		AddSub(StoryCharacter.ENGINEER, "Why are they even holding a trial? I mean, you're their enemy...");
		AddSub(StoryCharacter.ENGINEER, "So of course they'll find you guilty, right?");
		AddSub(StoryCharacter.PILOT, "Dunno, Ren. To be honest, don't wanna know. Just wanna blow them up.");
		
		
		CreateStory(); // 104 ONMIDSECTOR
		AddSub(StoryCharacter.HIDDENIRIS, "You're weird, you know that?");
		AddSub(StoryCharacter.PILOT, "Oh! It's you again. Hello.");
		AddSub(StoryCharacter.HIDDENIRIS, "I've been watching you.");
		AddSub(StoryCharacter.PILOT, "And I'M the weird one?");
		AddSub(StoryCharacter.HIDDENIRIS, "Sometimes you take all the loot. Sometimes you leave some behind.");
		AddSub(StoryCharacter.PILOT, "Uh, yeah, well... money isn't everything, right?");
		AddSub(StoryCharacter.HIDDENIRIS, "Hahaha! Good one.");
		AddSub(StoryCharacter.PILOT, "Glad you find me funny.");
		AddSub(StoryCharacter.HIDDENIRIS, "So... you're not here to take all the loot?");
		AddSub(StoryCharacter.PILOT, "No. I'm here to destroy the Oppressors known as the Royal Empire.");
		AddSub(StoryCharacter.HIDDENIRIS, "Yeah, whatever. Mind if I pick up any loot you leave behind?");
		AddSub(StoryCharacter.PILOT, "Sure. Just don't get in my way.");
		AddSub(StoryCharacter.ENGINEER, "Are you sure it's safe to let a pirate hang around you, @N?");
		AddSub(StoryCharacter.HIDDENIRIS, "It isn't.");
		
		CreateStory(); // 105 ONPREBOSSFIGHT
		AddSub(StoryCharacter.PILOT, "How's all that spare loot hunting going, my pirate pal?");
		AddSub(StoryCharacter.HIDDENIRIS, "Meh. You're kinda slow at blowing stuff up and you do miss things. But it's alright.");
		AddSub(StoryCharacter.PILOT, "Glad to help. Look, if you're going to be following me everywhere like a vulture...");
		AddSub(StoryCharacter.PILOT, "You could at least tell me your name.");
		AddSub(StoryCharacter.IRIS, "I'm Iris. Richest, sexiest and most bad ass pirate in the sector.");
		AddSub(StoryCharacter.PILOT, "I'm @N. Most badass - well, the only - Gemini Pilot of the Alliance Republic.");
		AddSub(StoryCharacter.IRIS, "Oooohhh, you're that clone thing?");
		AddSub(StoryCharacter.PILOT, "Gemini Pilot.");
		AddSub(StoryCharacter.IRIS, "Whatever. I've heard about you. ");
		AddSub(StoryCharacter.IRIS, "They say you're invincible. Of course, that ain't true, is it? You're just mass-produced.");
		AddSub(StoryCharacter.IRIS, "You can suck hard at flying and fighting, and it doesn't matter. You just keep coming back.");
		AddSub(StoryCharacter.PILOT, "For the record, I DON'T suck hard.");
		AddSub(StoryCharacter.IRIS, "Hah, yeah, sure.");
		
		
		CreateStory(); // 106 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.IRIS, "Alright, so maybe you're not as big a pain in my ass as I thought.");
		AddSub(StoryCharacter.PILOT, "Thanks. I kinda like you, too.");
		AddSub(StoryCharacter.IRIS, "Hey, I never said I like you. I just know a cash cow when I see one.");
		AddSub(StoryCharacter.IRIS, "I'll see you around, clone.");


		// SECTOR 14 - INVESTIGATION

		CreateStory(); // 107 ONENTERSECTOR
		AddSub(StoryCharacter.CROWN, "Good morning, @N. My task today is to clarify the details around your existence as a Gemini Pilot.");
		AddSub(StoryCharacter.PILOT, "Yeah, well my task today is to spread the entrails of your friends around space.");
		
		
		CreateStory(); // 108 ONMIDSECTOR
		AddSub(StoryCharacter.CROWN, "Captain @N is a highly experienced fighter pilot in the Alliance Republic space fleet.");
		AddSub(StoryCharacter.CROWN, "Originally from the Aurora Colony, @N has worked hard to progress through the ranks and become a respected soldier.");
		AddSub(StoryCharacter.PILOT, "If you're going to write my biography...");
		AddSub(StoryCharacter.PILOT, "...then you better have someone ready to step in when you get to the part where I kill you.");
		AddSub(StoryCharacter.CROWN, "Six years ago, Captain @N was approached by Major Alex Brightman, who proposed that @N become...");
		AddSub(StoryCharacter.CROWN, "...the face of a newly created project seeking to gain the upper hand in the war against the Royal Empire.");
		AddSub(StoryCharacter.PILOT, "We've always had the upper hand. We just like to try new things.");
		AddSub(StoryCharacter.CROWN, "The idea was simple. The Gemini Pilot would be part of a new system of cloning.");
		AddSub(StoryCharacter.CROWN, "Copies of the pilot would be made, and the process would be continuously carried out.");
		AddSub(StoryCharacter.CROWN, "A clone is sent into the battlefield, carrying with it the expertise of the original @N.");


		CreateStory(); // 109 ONPREBOSSFIGHT
		AddSub(StoryCharacter.CROWN, "When death would inevitably come, a new clone is activated and sent to continue the work.");
		AddSub(StoryCharacter.CROWN, "Memories are stored through complex algorithms, which are passed on and activated by the new clone.");
		AddSub(StoryCharacter.CROWN, "The time it takes for the new clone to reach the old clone's last location is removed from memory algorithms...");
		AddSub(StoryCharacter.CROWN, "...preventing the new clone from experiencing their activation and journey to the battlefield.");
		AddSub(StoryCharacter.CROWN, "They simply wake up, continuing with the memories of their predecessor.");
		
		
		CreateStory(); // 110 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.CROWN, "All the while, the original Gemini Pilot @N is kept safe at Alliance Republic HQ...");
		AddSub(StoryCharacter.CROWN, "Enjoying an extended break away from service.");
		AddSub(StoryCharacter.CROWN, "When the war ends, Gemini Pilot @N will be released from duty and allowed to retire from service.");
		AddSub(StoryCharacter.CROWN, "It is truly a perfect plan.");
		AddSub(StoryCharacter.PILOT, "Aw, you think I'm perfect. That's real sweet.");
		AddSub(StoryCharacter.CROWN, "On the contrary, I think you are far from perfect. You are merely... an obedient pet.");
		AddSub(StoryCharacter.PILOT, "Careful. Sometimes pets bite.");
		AddSub(StoryCharacter.CROWN, "I must continue to prepare my notes for the trial. Goodbye for now.");
		AddSub(StoryCharacter.ENGINEER, "How's he know so much about Project Gemini?");
		AddSub(StoryCharacter.BRIGHTMAN, "Hate to admit it, but the Royal Empire know what they're doing when it comes to spying and gathering info on us.");
		AddSub(StoryCharacter.BRIGHTMAN, "They must have been collecting the dirt on @N for some time...");
		AddSub(StoryCharacter.BRIGHTMAN, "And now that we've got the attention of the Upper Hierarchy they're trying to scare us by revealing what they know.");
		AddSub(StoryCharacter.PILOT, "Well, I'm not scared. So it ain't working.");


		// SECTOR 15 ONE MORTAL, BRAVE HERO


		CreateStory(); // 111 ONENTERSECTOR
		AddSub(StoryCharacter.ENGINEER, "Morning, @N.");
		AddSub(StoryCharacter.PILOT, "Hey Ren. What's new?");
		AddSub(StoryCharacter.ENGINEER, "Erm... not much. Just wanted to... ah... well, just wanted to see how you're doing.");
		AddSub(StoryCharacter.PILOT, "Um... I'm fine, Ren.");
		AddSub(StoryCharacter.ENGINEER, "Yeah. I know. It's just uh... well, I'll be honest.");
		AddSub(StoryCharacter.ENGINEER, "I'm kind of weirded out by how much the Royal Empire know about Project Gemini.");
		AddSub(StoryCharacter.PILOT, "Don't let it bug you, Ren. It doesn't make a difference.");
		AddSub(StoryCharacter.ENGINEER, "The general public don't know the truth about Project Gemini, but the Royal Empire do.");
		AddSub(StoryCharacter.ENGINEER, "You don't think that's weird?");
		AddSub(StoryCharacter.PILOT, "I think that what the public don't know, can't hurt them.");
		AddSub(StoryCharacter.PILOT, "They need to believe we're going to win this war, and that takes a hero.");
		AddSub(StoryCharacter.PILOT, "They think I'm just one, mortal, brave human being like them. Single-handedly taking down the Royal Empire.");
		AddSub(StoryCharacter.PILOT, "It's inspiring.");
		AddSub(StoryCharacter.ENGINEER, "...I guess.");
		
		
		CreateStory(); // 112 ONMIDSECTOR
		AddSub(StoryCharacter.IRIS, "Jeez, you really pissed off the Royal Empire, huh?");
		AddSub(StoryCharacter.PILOT, "What makes you say that?");
		AddSub(StoryCharacter.IRIS, "I've been shooting down Imperial goons for the past three hours.");
		AddSub(StoryCharacter.IRIS, "They carry around a lot of $C, though, so it's all good.");
		AddSub(StoryCharacter.PILOT, "I knew you were on our side.");
		AddSub(StoryCharacter.IRIS, "Please. Back when there were Alliance Republic fleets out here and not just one clone taking on the entire Empire...");
		AddSub(StoryCharacter.IRIS, "I was taking down equal measures of your friends and theirs.");
		AddSub(StoryCharacter.PILOT, "I shouldn't even talk to you, if you're responsible for killing my fellow free people.");
		AddSub(StoryCharacter.IRIS, "'Free people.' I hate to piss on your fire, but you're not free, @N.");

		
		CreateStory(); // 113 ONPREBOSSFIGHT
		AddSub(StoryCharacter.PILOT, "So I guess if I was a pirate, I'd be free, huh?");
		AddSub(StoryCharacter.IRIS, "It ain't perfect, but it's sure as hell a lot more free than being an Alliance Republic screen slave.");
		AddSub(StoryCharacter.PILOT, "Screen slave?");
		AddSub(StoryCharacter.IRIS, "That's what us pirates call civvies out on the Republic colonies.");
		AddSub(StoryCharacter.IRIS, "All you people do is sit in front of TV screens, watch garbage and get told what to buy.");
		AddSub(StoryCharacter.IRIS, "Screen slaves!");
		
		
		CreateStory(); // 114 ONPOSTBOSSFIGHT
		AddSub(StoryCharacter.BRIGHTMAN, "Gemini Pilot @N, atten-TION!");
		AddSub(StoryCharacter.PILOT, "Yes sir, Major sir!");
		AddSub(StoryCharacter.BRIGHTMAN, "What is this garbage I'm hearing about you getting friendly with a pirate?");
		AddSub(StoryCharacter.PILOT, "I'm just trying to get local knowledge out of her, Major. She could be useful.");
		AddSub(StoryCharacter.BRIGHTMAN, "Pirates are untrustworthy, @N. The minute you let your guard down, she'll turn on you.");
		AddSub(StoryCharacter.BRIGHTMAN, "And the fact she's a human makes her a God damn traitor.");
		AddSub(StoryCharacter.BRIGHTMAN, "You are not to continue communication with the pirate, is that understood?");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.BRIGHTMAN, "I said IS that UNDERSTOOD, soldier?");
		AddSub(StoryCharacter.PILOT, "Yes sir.");

		// SECTOR 16 THE WRONG BOOKS

		CreateStory(); // 115 ONENTERCHAPTER
		AddSub(StoryCharacter.CROWN, "Report.");
		AddSub(StoryCharacter.ROYALENGINEER, "Fleet-wide upgrades have been completed, sir. All ships now have greater shield power, weaponry and control.");
		AddSub(StoryCharacter.CROWN, "Excellent. Let us see how this affects the progress of the Gemini Clone.");

		CreateStory(); // 116 ONENTERSECTOR
		AddSub(StoryCharacter.CROWN, "Fine morning to you, Gemini Pilot @N.");
		AddSub(StoryCharacter.PILOT, "What do you want?");
		AddSub(StoryCharacter.CROWN, "We are nearing your trial date. Evidence collection is coming together nicely.");
		AddSub(StoryCharacter.CROWN, "Today, I would like to address the murder of Viceroy Francois.");
		AddSub(StoryCharacter.PILOT, "Murder. Right.");
		AddSub(StoryCharacter.CROWN, "Do you admit to destroying my most honourable brother in arms?");
		AddSub(StoryCharacter.PILOT, "Yep. I blew him up. It was real pretty.");


		CreateStory(); // 117 ONMIDSECTOR
		AddSub(StoryCharacter.CROWN, "And do you have any sort of reason for your actions?");
		AddSub(StoryCharacter.PILOT, "Are you kidding? You and I are at WAR. He was at war with me. He murdered thousands of innocent civilians on our colonies.");
		AddSub(StoryCharacter.PILOT, "I didn't just murder the Viceroy. I put him on trail and EXECUTED him.");
		AddSub(StoryCharacter.CROWN, "It seems we have a fundamental difference in opinion. Viceroy Francois did indeed plan attacks on your colonies.");
		AddSub(StoryCharacter.CROWN, "But these attacks were no more murderous than those your kind have inflicted upon the Royal Empire over the years.");
		AddSub(StoryCharacter.PILOT, "Yeah, right. We've been fighting off your oppression since before I was a kid.");
		AddSub(StoryCharacter.PILOT, "I've read the history books, Crown Prosecutor.");
		AddSub(StoryCharacter.PILOT, "The Royal Empire has wanted our free thinking nation dead since the moment we crossed paths.");
		AddSub(StoryCharacter.CROWN, "Hmm. The Gemini Pilot is completely unaware of the history of the war predating childhood. Very interesting.");

		CreateStory(); // 118 
		AddSub(StoryCharacter.PILOT, "What do you mean 'completely unaware of the history?' I said I've READ the books.");
		AddSub(StoryCharacter.CROWN, "Indeed, you have read books. Just... the wrong ones.");
		AddSub(StoryCharacter.PILOT, "It's a shame you won't get the chance to read the book that explains how I single-handedly tore you and your oppressive kind apart.");

		CreateStory(); // 119
		AddSub(StoryCharacter.ENGINEER, "So uh. What's all that about the history books being wrong?");
		AddSub(StoryCharacter.BRIGHTMAN, "More scare tactics, Engineer Ren. He's trying to sow seeds of doubt into @N.");
		AddSub(StoryCharacter.PILOT, "Yeah. Don't pay attention to his propaganda, Ren. He's trying to make us sloppy.");

		// SECTOR 17

		CreateStory(); // 120
		AddSub(StoryCharacter.PILOT, "Nnnggh...");
		AddSub(StoryCharacter.PILOT, "Aahh... AHH!");
		AddSub(StoryCharacter.PILOT, "My head! That.. drilling again.");
		AddSub(StoryCharacter.ENGINEER, "@N? You okay, @N?");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.PILOT, "Ugh... yeah. Yeah, I'm ok.");
		AddSub(StoryCharacter.ENGINEER, "Was it that headache again?");
		AddSub(StoryCharacter.PILOT, "Yeah. I've never felt anything like it. Feels like my head's going to explode. Then suddenly, it's gone.");
		AddSub(StoryCharacter.ENGINEER, "Things are harder than they've ever been, @N. Stress manifests in all sorts of ways.");
		AddSub(StoryCharacter.ENGINEER, "Unfortunately for you, it's come in the form of a really bad migraine.");
		AddSub(StoryCharacter.PILOT, "Migraine. Yeah...");


		CreateStory(); // 121
		AddSub(StoryCharacter.IRIS, "Gooooood morning! I just found the haul of the YEAR. It's so Goddamn shiny, I might just explode.");
		AddSub(StoryCharacter.IRIS, "How about you back me up and we split the takings?");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.IRIS, "Hee-lloooo?");
		AddSub(StoryCharacter.PILOT, "I am under orders not to communicate further with you. Do not contact this ship again, pirate.");
		AddSub(StoryCharacter.IRIS, "Oh, I get it. The bossman's scared I'm going to taint your tiny little brain so now he wants you to cut ties.");
		AddSub(StoryCharacter.IRIS, "Fine with me, you're just another boring screen slave anyway.");
		AddSub(StoryCharacter.IRIS, "Have fun dying then coming back to life over and over for all eternity, ****face.");
		AddSub(StoryCharacter.PILOT, "...");


		CreateStory(); // 122
		AddSub(StoryCharacter.CROWN, "Greetings, Gemini Pilot @N. Today, I would like to get to know a bit more about you.");
		AddSub(StoryCharacter.PILOT, "I thought you guys were the best at gathering info on us lowly Alliance Republic infidels.");
		AddSub(StoryCharacter.PILOT, "What could I possibly tell you that you don't already know?");
		AddSub(StoryCharacter.CROWN, "Indeed, I have information. I just wish to cross-examine it with your own words.");
		AddSub(StoryCharacter.PILOT, "Alright, fine. You wanna know about me? I'm awesome, that's all you really need to know.");
		AddSub(StoryCharacter.CROWN, "I am curious. Have you lost childhood memories as a result of the Gemini Project?");
		AddSub(StoryCharacter.PILOT, "Pffff! Forget childhood memories? Come on. Let's see here.... I was born on the Aurora Colony to a beautiful couple.");
		AddSub(StoryCharacter.PILOT, "My father was an engineer. My mother a cook. We weren't rich, but we didn't want for much.");
		AddSub(StoryCharacter.CROWN, "Go on...");


		CreateStory(); // 123
		AddSub(StoryCharacter.PILOT, "At the academy I was the best at everything. Any sport, any subject. I just knew how to give it my all.");
		AddSub(StoryCharacter.PILOT, "As I got older, I started to pay more attention to the war going on out there.");
		AddSub(StoryCharacter.PILOT, "I hated hearing about colonies getting attacked by the Royal Empire. I knew pretty early on that I wanted to become a pilot.");


		CreateStory(); // 124
		AddSub(StoryCharacter.PILOT, "I enlisted. I trained. I excelled amongst my brothers and sisters in arms. I started jumping up the ranks.");
		AddSub(StoryCharacter.PILOT, "And now here I am. The face of the project that is going to win us the war against oppression.");


		CreateStory(); // 125
		AddSub(StoryCharacter.PILOT, "There you have it. Detailed enough for you?");
		AddSub(StoryCharacter.CROWN, "Thank you for the... 'detailed' recollection of your past, Gemini Pilot @N.");
		AddSub(StoryCharacter.PILOT, "No problem. Don't forget to pick up a postcard at the gift shop on your way out.");

		// SECTOR 18: ONE OF THOSE DAYS

		CreateStory(); // 126
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING AUDIO MESSAGE FROM MOTHER");
		AddSub(StoryCharacter.MOTHER, "*Sniff*");
		AddSub(StoryCharacter.MOTHER, "...");
		AddSub(StoryCharacter.MOTHER, "Today's been one of those days, sweetie. I.. I just felt like seeing your face.");
		AddSub(StoryCharacter.MOTHER, "...");
		AddSub(StoryCharacter.MOTHER, "But I know you're busy... I'm sorry. *sniff* ...I don't mean to bother you when you're in the middle of saving us all.");
		AddSub(StoryCharacter.MOTHER, "Love you...");
		AddSub(StoryCharacter.COMPUTER, ">> END OF AUDIO MESSAGE");
		AddSub(StoryCharacter.PILOT, "Computer, begin audio recording.");
		AddSub(StoryCharacter.PILOT, "Hey Momma. I'm sorry I haven't replied to you sooner. It's just... it's just crazy out here, and-");
		AddSub(StoryCharacter.COMPUTER, ">> RED ALERT. BATTLE STATIONS REQUIRED.");
		AddSub(StoryCharacter.PILOT, "What?! Damnit, delete audio message!");

		CreateStory(); // 127
		AddSub(StoryCharacter.PILOT, "What's going on, Ren?");
		AddSub(StoryCharacter.ENGINEER, "There's a Imperial fleet headed for you, @N.");
		AddSub(StoryCharacter.CROWN, "Good morning, Gemini Pilot @N.");
		AddSub(StoryCharacter.PILOT, "Trying to catch me with my guard down, Crown Prosecutor?");
		AddSub(StoryCharacter.CROWN, "Indeed, I am aware this meeting with my fleet is a bit unexpected. You are an animal of routine and habit.");
		AddSub(StoryCharacter.CROWN, "You wake up, you hunt, you go to bed with your stomach full. I do apologise for bringing the fight to you.");
		AddSub(StoryCharacter.PILOT, "No need to apologise. I'm happy to kill your soldiers any time.");

		CreateStory(); // 128
		AddSub(StoryCharacter.CROWN, "This is, I suppose, what we might call the final assessment before your upcoming trial.");
		AddSub(StoryCharacter.PILOT, "And what exactly are you assessing?");
		AddSub(StoryCharacter.CROWN, "You're very good at attacking our fleet. But how do you respond to the fleet attacking you and interrupting your routine?");
		AddSub(StoryCharacter.PILOT, "Makes no difference to me.");
		AddSub(StoryCharacter.CROWN, "Indeed, I can see this.");

		CreateStory(); // 129
		AddSub(StoryCharacter.CROWN, "Gemini Pilot @N, I have completed my investigation.");
		AddSub(StoryCharacter.PILOT, "Hey, good job. What do you want - a medal?");
		AddSub(StoryCharacter.CROWN, "I, high honourable Crown Prosecutor Julian Trebon...");
		AddSub(StoryCharacter.CROWN, "...call Gemini Pilot @N of the terrorist faction 'Alliance Republic' to the judicial order of the mighty Royal Empire.");
		AddSub(StoryCharacter.CROWN, "The trial will commence on the sixth day of Solaris, in the age X374X.");
		AddSub(StoryCharacter.PILOT, "Think I might be busy that day.");
		AddSub(StoryCharacter.CROWN, "We shall speak again at the trial. Enjoy your freedom until then, Gemini Pilot @N.");


		// SECTOR 19: Hangman's Noose
		
		CreateStory(); // 130
		AddSub(StoryCharacter.BRIGHTMAN, "@N, the chiefs and I just wanted to thank you for putting up with this Crown Prosecutor nonsense for so long.");
		AddSub(StoryCharacter.PILOT, "I'm just a humble soldier, sir. Doing my job the only way I know how.");
		AddSub(StoryCharacter.BRIGHTMAN, "The media have been going nuts over the upcoming trial. Really showing the folks back just how crazy the Royal Empire is.");
		AddSub(StoryCharacter.BRIGHTMAN, "AR-TV did a national poll and the people LOVE you. 67% think you're innocent.");
		AddSub(StoryCharacter.PILOT, "Hah, cool, tha-- wait. 67%?");
		AddSub(StoryCharacter.BRIGHTMAN, "That's right.");
		AddSub(StoryCharacter.PILOT, "Uh... not, like, 100%?");
		AddSub(StoryCharacter.BRIGHTMAN, "It's TV, @N. Don't take it to heart. People like to think they have an opinion. Really, everyone's on your side.");
		AddSub(StoryCharacter.PILOT, "Right...");
		AddSub(StoryCharacter.BRIGHTMAN, "Broadcasts about you are the single most watched bit of entertainment of the century, @N. ");
		AddSub(StoryCharacter.BRIGHTMAN, "Don't think for a second that the whole nation isn't on your side.");
		AddSub(StoryCharacter.PILOT, "...Entertainment?");


		CreateStory(); // 131
		AddSub(StoryCharacter.ENGINEER, "Hey @N, just wanted to wish your luck for the trial tomorrow.");
		AddSub(StoryCharacter.PILOT, "Luck?! Ren, it's a ****ing sham. I'm being tried by a nutcase.");
		AddSub(StoryCharacter.PILOT, "I don't need LUCK, there's obviously only going to be ONE verdict.");
		AddSub(StoryCharacter.ENGINEER, "Yeah. Well I dunno, I just thought...");
		AddSub(StoryCharacter.PILOT, "How about you keep your THOUGHTS to yourself, Engineer?");
		AddSub(StoryCharacter.ENGINEER, "...");
		AddSub(StoryCharacter.PILOT, "Sorry, Ren. Just feeling a little tense. I guess the trial thing creeps me out more than I want to let on.");
		AddSub(StoryCharacter.ENGINEER, "Well, you're right about it, @N. It is a sham.");
		AddSub(StoryCharacter.ENGINEER, "But it's a necessary sham to put up with so you can get closer to the Crown Prosecutor and take him down.");
		AddSub(StoryCharacter.PILOT, "Yeah. You're right. And that's exactly what I'll do.");


		CreateStory(); // 132
		AddSub(StoryCharacter.BRIGHTMAN, "We're going to go quiet on the COMMS tomorrow, @N. We don't want the Crown Prosecutor to potentially get distracted by us.");
		AddSub(StoryCharacter.BRIGHTMAN, "Keep him focused on this bogus trial and strike when you see the opportune moment.");
		AddSub(StoryCharacter.PILOT, "Yes sir.");
		AddSub(StoryCharacter.BRIGHTMAN, "See you on the other side, soldier.");
		AddSub(StoryCharacter.PILOT, "Or dangling from the hangman's noose.");


		// SECTOR 20: FINAL VERDICT 
		
		CreateStory(); // 133
		AddSub(StoryCharacter.CROWN, "On this, the sixth day of Solaris in the age X374X...");
		AddSub(StoryCharacter.CROWN, "I bring to the righteous courts of the Royal Empire the trial of Gemini Pilot @N of the terrorist faction 'Alliance Republic.'");
		AddSub(StoryCharacter.CROWN, "The defendant stands accused of the murder of the most excellent and noble Viceroy Francois, Defender of the Crown.");
		AddSub(StoryCharacter.CROWN, "As well as the murder of thousands of brave Royal Empire soldiers, committed in atrocious war crimes against the throne.");
		AddSub(StoryCharacter.CROWN, "The defendant pleads non-guilty, and today this court will reach a verdict.");
		AddSub(StoryCharacter.CROWN, "It must also be noted that the defendant has refused to board our vessel and be remanded in custody during proceedings.");
		AddSub(StoryCharacter.CROWN, "As such, it is likely the defendant is travelling to the court's location with the intention of launching an attack.");
		AddSub(StoryCharacter.CROWN, "The court is fully prepared for such a scenario.");


		CreateStory(); // 134
		AddSub(StoryCharacter.CROWN, "I, the most honourable Crown Prosecutor Julian Trebon, have been conducting an investigation into the accused for some time.");
		AddSub(StoryCharacter.PILOT, "Oh and it's been so, SO much fun hanging out with you by the way.");
		AddSub(StoryCharacter.CROWN, "Let is be known that the accused is part of a tactical war project known as 'Project Gemini'.");
		AddSub(StoryCharacter.CROWN, "The accused is a clone of a human known as Captain @N of the Aurora Colony.");
		AddSub(StoryCharacter.CROWN, "This makes this trial a very unique experience; one that should be treated carefully.");
		AddSub(StoryCharacter.CROWN, "From what I can gather, the personality resemblance of the clone to the original human being is strikingly similar.");
		AddSub(StoryCharacter.CROWN, "It would seem as though the cloning process is near on perfect. Each clone is an exact physical and emotional copy of the previous.");
		AddSub(StoryCharacter.CROWN, "By using an ingenious algorithm, each time a clone is destroyed all information is instantly transcribed into the next clone.");
		AddSub(StoryCharacter.CROWN, "A wonderfully subtle removal of the ability to dwell - or over think - the concept of death and reincarnation.");
		AddSub(StoryCharacter.CROWN, "Each time a clone dies, the new clone is completely unaware of the previous death. The clone continues on as normal.");
		AddSub(StoryCharacter.CROWN, "Any mention of time between the destruction of the old and the delivery of the new is avoided...");
		AddSub(StoryCharacter.CROWN, "The clone simply does not think about how many times it has 'died' during its war campaign.");


		CreateStory(); // 135
		AddSub(StoryCharacter.CROWN, "Let it be known that the accused has now reached close quarters to the court.");
		AddSub(StoryCharacter.CROWN, "We will no doubt engage in combat very soon. Thus, we will attempt a hasty verdict.");
		AddSub(StoryCharacter.CROWN, "Before a verdict is reached, we will allow the accused to be defended by a representative of their choice.");
		AddSub(StoryCharacter.CROWN, "They may provide character testimony to better inform us on our decision.");
		AddSub(StoryCharacter.CROWN, "Do you have someone you wish to speak for your defence, @N?");
		AddSub(StoryCharacter.PILOT, "Nah. All the Alliance Republic folk are off COMMS for this nebula-puke.");
		AddSub(StoryCharacter.CROWN, "No one wishes to defend your name?");
		AddSub(StoryCharacter.PILOT, "I... well, no.");
		AddSub(StoryCharacter.IRIS, "Hey, morons! I'll say something.");
		AddSub(StoryCharacter.PILOT, "Iris?");
		AddSub(StoryCharacter.IRIS, "@N's just as much a murderer as all of us here.");
		AddSub(StoryCharacter.IRIS, "Everyone from the Alliance is a freedomless screen slave, whether they want to admit it or not.");
		AddSub(StoryCharacter.IRIS, "But what @N has to go through, that's a whoooolle 'nother slavery ball game.");
		AddSub(StoryCharacter.IRIS, "So instead of persecuting the fool, you should show pity.");
		AddSub(StoryCharacter.PILOT, "Huh?");
		AddSub(StoryCharacter.IRIS, "Just something to think about, Mister Prosecutor.");



		CreateStory(); // 136
		AddSub(StoryCharacter.PILOT, "There you are. Nice ship. Looks like being a Crown Prosecutor's got its perks.");
		AddSub(StoryCharacter.CROWN, "The court has reached a verdict.");
		AddSub(StoryCharacter.PILOT, "Alright. Let's hear it.");
		AddSub(StoryCharacter.CROWN, "The court has decided that this case is a very unique matter. After careful consideration...");
		AddSub(StoryCharacter.CROWN, "I have decided that the defendant - the Gemini Pilot @N which currently flies before my ship - is...");
		AddSub(StoryCharacter.CROWN, "...innocent of the offences accused.");
		AddSub(StoryCharacter.PILOT, "Yeah, I knew it, I'm gu-- wait... innocent?");
		AddSub(StoryCharacter.CROWN, "It seems unreasonable to trial this clone for offences committed by what would have been a previous clone...");
		AddSub(StoryCharacter.CROWN, "Or indeed *clones* - sometime before the creation of the one before us.");
		AddSub(StoryCharacter.PILOT, "What the ****?");
		AddSub(StoryCharacter.CROWN, "Instead, I declare the human being Captain @N of the Aurora Colony GUILTY of atrocities committed against the crown.");
		AddSub(StoryCharacter.PILOT, "But.. that IS me, you idiot!");
		AddSub(StoryCharacter.CROWN, "I am inclined to disagree. Now, Gemini Pilot @N. You may turn your ship around and know that you are free of my persecution.");
		AddSub(StoryCharacter.CROWN, "However, should you stay I will have no choice but to engage with you in combat.");
		AddSub(StoryCharacter.PILOT, "Damn your righteousness! You're just trying to confuse me, aren't you?​");



		CreateStory(); // 137
		AddSub(StoryCharacter.CROWN, "This... court is... adjourned.");
		AddSub(StoryCharacter.BRIGHTMAN, "Great work, @N! You did it!");
		AddSub(StoryCharacter.ENGINEER, "Awesome stuff, @N!");
		AddSub(StoryCharacter.PILOT, "Yeah. And hey, thanks for the help back there guys.");
		AddSub(StoryCharacter.PILOT, "Oh wait! No, you didn't testify to my innocence. A PIRATE who barely knows me did.");
		AddSub(StoryCharacter.BRIGHTMAN, "Don't get smart with me, soldier. That character testimony meant as much as the court case itself. Absolutely nothing.");
		AddSub(StoryCharacter.BRIGHTMAN, "Either way, it would have had to end like this anyway.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.BRIGHTMAN, "I've got to go prepare for a press conference. You're tired. Rightly so.");
		AddSub(StoryCharacter.BRIGHTMAN, "Get some rest and calm down. I'll speak to you soon.");
		AddSub(StoryCharacter.BRIGHTMAN, "And @N. I mean it. Well done, soldier.");
		AddSub(StoryCharacter.PILOT, "Yeah, alright. Thanks, Major.");
		AddSub(StoryCharacter.ENGINEER, "Hey @N... I wanted to testify for you, just so you know. The Major wouldn't let me.");
		AddSub(StoryCharacter.PILOT, "Thanks Ren. And hey... re-open COMMS with Iris for me.");
		AddSub(StoryCharacter.PILOT, "But do me a favour and keep it out of earshot from the Major.");
		AddSub(StoryCharacter.ENGINEER, "You got it.");

		// SECTOR 22: PAROLE
		
		CreateStory(); // 138
		AddSub(StoryCharacter.ENGINEER, "...So I said 'yes sir' and flipped him the bird.");
		AddSub(StoryCharacter.PILOT, "Really?");
		AddSub(StoryCharacter.ENGINEER, "Yeah. But he didn't see me do it.");
		AddSub(StoryCharacter.PILOT, "You little rebel.");
		AddSub(StoryCharacter.ENGINEER, "It's not our fault the Upper Hierarchy haven't been in touch. Maybe the Crown prosecutor wasn't as big a fish as we thought.");
		AddSub(StoryCharacter.PILOT, "Or maybe I'm on parole, and they're just waiting to see if I change my ways.");
		AddSub(StoryCharacter.ENGINEER, "Do you think it's over? Have they stopped trying to fight back against Project Gemini?");
		AddSub(StoryCharacter.PILOT, "All I know is in a few months time, my tour is up. All clones will be put into stasis.");
		AddSub(StoryCharacter.PILOT, "I'll be living ONE life, and it'll be someone else's problem. I'm tired, I'm done.");
		AddSub(StoryCharacter.ENGINEER, "I'm looking forward to partying with you when it's all over, @N.");
		AddSub(StoryCharacter.PILOT, "The real me might not know you yet, but I have a feeling we'll get on just fine.");


		// SECTOR 23: SCAN

		CreateStory(); // 139
		AddSub(StoryCharacter.MEDIC, "Good morning, @N.");
		AddSub(StoryCharacter.PILOT, "What's up, Doc?");
		AddSub(StoryCharacter.MEDIC, "Very funny. I heard you've been having some problems? Headaches is it?");
		AddSub(StoryCharacter.PILOT, "Yeah. Not much to say. They freakin' hurt, they strike at random and I want them to stop.");
		AddSub(StoryCharacter.MEDIC, "I see. Well, sit back for a moment and I will run some scans while you work.");


		CreateStory(); // 140
		AddSub(StoryCharacter.MEDIC, "Hmm...");
		AddSub(StoryCharacter.PILOT, "What is it, Doc?");
		AddSub(StoryCharacter.MEDIC, "Nothing particularly suspicious comes up in your MRI. Your brain activity is functioning normally, tissue matter seems healthy.");
		AddSub(StoryCharacter.MEDIC, "It will be difficult to figure out what the problem is for the moment, if scans are coming back normal.");
		AddSub(StoryCharacter.MEDIC, "I suggest we meet every now and again for checkups and monitor the problem.");
		AddSub(StoryCharacter.PILOT, "Alright, sounds good.");


		// SECTOR 24: ACTORS


		CreateStory(); // 141
		AddSub(StoryCharacter.IRIS, "Well if it ain't clone number 147. Or is it 247? I've lost count.");
		AddSub (StoryCharacter.PILOT, "That is SO funny, I'm going to save my laugh for later so I can really enjoy it.");
		AddSub (StoryCharacter.PILOT, "How's your treasure hunt going?");
		AddSub(StoryCharacter.IRIS, "I just pried the location of a pretty sexy looking haul from the fingers of a very dead ex-Alliance Republic pilot.");
		AddSub(StoryCharacter.PILOT, "You killed one of my comrades?");
		AddSub(StoryCharacter.IRIS, "I said EX. He had the sense to bail out of the Alliance years ago.");
		AddSub(StoryCharacter.IRIS, "Anyway, I'll split the loot with ya if you back me up. You in?");
		AddSub(StoryCharacter.PILOT, "Alright, fine.");
		AddSub(StoryCharacter.IRIS, "Great, follow me clone number six billion three hundr-");
		AddSub(StoryCharacter.PILOT, "Shut up.");
		AddSub(StoryCharacter.IRIS, "Pahaha!");


		CreateStory(); // 142
		AddSub(StoryCharacter.IRIS, "It's not far from here. Few clicks away.");
		AddSub(StoryCharacter.PILOT, "Roger that. So... the guy you killed for these coordinates. He got out of the Alliance?");
		AddSub(StoryCharacter.IRIS, "Yeah. Left it years ago. He never really fit in, though, cuz he had half a brain.");
		AddSub(StoryCharacter.PILOT, "Ho ho, ha ha.");
		AddSub(StoryCharacter.IRIS, "Been playing him for a while. Not bad 'company'. Slow trigger finger, though.");
		AddSub(StoryCharacter.PILOT, "Too much info.");
		AddSub(StoryCharacter.IRIS, "Playa gon' play.");
		AddSub(StoryCharacter.IRIS, "Anyway, he thought I was privy to some sweet hauls and thought HE was playing ME. Takes a real pirate to play this game.");


		CreateStory(); // 143
		AddSub(StoryCharacter.IRIS, "Alright, we're here. It's going to be pretty heavily guarded. Nothing you haven't dealt with before, though.");
		AddSub(StoryCharacter.PILOT, "You're making it sound like I'm about to do this alone.");
		AddSub(StoryCharacter.IRIS, "You are. See ya!");
		AddSub(StoryCharacter.PILOT, "Son of a...");


		CreateStory(); // 144
		AddSub(StoryCharacter.IRIS, "Boy, I sure do love having a clone friend.");
		AddSub(StoryCharacter.PILOT, "You're using me for my immortality?");
		AddSub(StoryCharacter.IRIS, "When I die, I'm dead for good. When you die, another ugly little @N pops up and acts like nothing's the matter.");
		AddSub(StoryCharacter.IRIS, "What kind of idiot wouldn't use that to their advantage?");
		AddSub(StoryCharacter.PILOT, "I take it back. You're no comedian. You're low life scum, through and through.");
		AddSub(StoryCharacter.IRIS, "Aw, thanks, that's sweet. I'll wire over your share of the $G. Ciao for now.");


		// SECTOR 26: BIRTHDAY
		
		CreateStory(); // 145
		AddSub(StoryCharacter.ENGINEER, "Happy birthday, @N!");
		AddSub(StoryCharacter.PILOT, "Thanks, Ren.");
		AddSub(StoryCharacter.ENGINEER, "How's it feel to be thirty four?");
		AddSub(StoryCharacter.PILOT, "Twenty four? I'm thirty three, Ren.");
		AddSub(StoryCharacter.ENGINEER, "Really? Weird, my records say you're twenty four.");
		AddSub(StoryCharacter.PILOT, "Wait... oh... right, yeah. I am twenty four. Yeah. Hah, whoops.");
		AddSub(StoryCharacter.ENGINEER, "Haha! That's the Alzheimer's kicking in!");
		AddSub(StoryCharacter.PILOT, "Yeah, funny.");
		AddSub(StoryCharacter.BRIGHTMAN, "@N. What's the latest on the Upper Hierarchy?");
		AddSub(StoryCharacter.ENGINEER, "It's @N's birthday today, Major.");
		AddSub(StoryCharacter.BRIGHTMAN, "Get back to work, Engineer Ren.");
		AddSub(StoryCharacter.ENGINEER, "...yes sir.");
		AddSub(StoryCharacter.PILOT, "Nothing to report, Major. Still no contact.");
		AddSub(StoryCharacter.BRIGHTMAN, "The big wigs are on my ass, @N. They're starting to call this a big waste of time. Make contact, you hear me?");
		AddSub(StoryCharacter.PILOT, "Trying my best, Major.");
		AddSub(StoryCharacter.BRIGHTMAN, "Not good enough, soldier. Try harder. Brightman out.");
		AddSub(StoryCharacter.PILOT, "Sheesh.");


		CreateStory(); // 146
		AddSub(StoryCharacter.IRIS, "A little birdie told me it's your birthday.");
		AddSub(StoryCharacter.PILOT, "Yep. Twenty thr-- eh, four years young.");
		AddSub(StoryCharacter.IRIS, "Guess you'll be wanting a present.");
		AddSub(StoryCharacter.PILOT, "From you? Oh boy, what's it going to be? A knife in the back?");
		AddSub(StoryCharacter.IRIS, "Oh! Well I guess I'll just take it back, then.");
		AddSub(StoryCharacter.PILOT, "Hey! Come on, I was kidding!");


		CreateStory(); // 147
		AddSub(StoryCharacter.PILOT, "Come on, where's my present?");
		AddSub(StoryCharacter.IRIS, "Not sure you deserve it after that remark.");
		AddSub(StoryCharacter.PILOT, "Now, now. Be honest. You've stabbed more people in the back than you've bought birthday presents.");
		AddSub(StoryCharacter.IRIS, "Nonsense. Never heard the phrase 'honor amongst thieves?'");


		CreateStory(); // 148
		AddSub(StoryCharacter.IRIS, "Alright, alright. I can't hold it any longer. You ready?");
		AddSub(StoryCharacter.PILOT, "Should I close my eyes?");
		AddSub(StoryCharacter.IRIS, "It ain't a kiss, dork. It's a COMMS channel.");
		AddSub(StoryCharacter.PILOT, "Huh?");
		AddSub(StoryCharacter.IRIS, "I'm sending you a private COMMS invite. DON'T tell your Alliance buddies, alright?");
		AddSub(StoryCharacter.PILOT, "Okay...");
		AddSub(StoryCharacter.IRIS, "Alright. You patched in?");
		AddSub(StoryCharacter.PILOT, "I'm in.");
		AddSub(StoryCharacter.IRIS, "Good. Gemini Pilot @N, say hello to Prince Cyril Enfant of the Royal Empire.");
		AddSub(StoryCharacter.PRINCE, "Happy Birthday, @N.");
		AddSub(StoryCharacter.PILOT, "Wait... What?!");




		// SECTOR 27: PERPLEXION
		
		CreateStory(); // 149
		AddSub(StoryCharacter.PRINCE, "It's a pleasure to meet you, @N.");
		AddSub(StoryCharacter.PILOT, "What the hell's going on?");
		AddSub(StoryCharacter.IRIS, "I'll leave you both to talk shop. Have fun!");
		AddSub(StoryCharacter.PILOT, "Iris! God damn it, what is this? Are you with them?!");
		AddSub(StoryCharacter.PRINCE, "No, I can assure you she's not. Iris is the most exclusively neutral person I've ever met. She's a true pirate.");
		AddSub(StoryCharacter.PILOT, "How do I know you're really the Prince?");
		AddSub(StoryCharacter.PRINCE, "I suppose you'll have to take my word for it.");


		CreateStory(); // 150
		AddSub(StoryCharacter.PILOT, "So are you here to try and dissuade me from blowing you and your family up for good?");
		AddSub(StoryCharacter.PRINCE, "Well, yes and no.");
		AddSub(StoryCharacter.PILOT, "What?");
		AddSub(StoryCharacter.PRINCE, "Iris told me she was in contact with you. We spoke privately and struck a deal.");
		AddSub(StoryCharacter.PRINCE, "She'd open a private COMMS with you. She said you would listen.");
		AddSub(StoryCharacter.PILOT, "Oh, she did, did she?");
		AddSub(StoryCharacter.PRINCE, "You're going to find the following a bit hard to believe.");
		AddSub(StoryCharacter.PILOT, "Alright.");
		AddSub(StoryCharacter.PRINCE, "My 'family' - the Upper Hierarchy of the Royal Empire. We're not as close as you might think.");
		AddSub(StoryCharacter.PRINCE, "People of the Royal Empire are encouraged to think freely. We're educated in the Universes.");
		AddSub(StoryCharacter.PRINCE, "We're not told what the government thinks we need to know and kept in the dark about the rest, like the people of the Alliance.");
		AddSub(StoryCharacter.PILOT, "I knew it. You're just going to try and brainwash me into thinking the AR's an oppressive nation and that YOU'RE the free one.");
		AddSub(StoryCharacter.PRINCE, "No, I apologise. I'm just trying to tell you the whole truth. Our nation has its dark side, just like yours.");


		CreateStory(); // 151
		AddSub(StoryCharacter.PILOT, "Ok. So you're all educated and free thinking. So what?");
		AddSub(StoryCharacter.PRINCE, "The influence of the Upper Hierarchy is a very powerful tool. It's similar to the Alliance's media propaganda tactics.");
		AddSub(StoryCharacter.PRINCE, "The people of the Royal Empire choose to stay, to be loyal to the crown, despite knowing there are better worlds to be habited and lives to be lived.");
		AddSub(StoryCharacter.PRINCE, "But I'm not like the people of the Empire. I'm tired. I can't keep fighting this war.");
		AddSub(StoryCharacter.PRINCE, "I'm not a soldier. I was born into this position. I never made the choice, like you, to fight for my nation.");
		AddSub(StoryCharacter.PILOT, "...");


		CreateStory(); // 152
		AddSub(StoryCharacter.PRINCE, "I wanted to speak with you and try to explain my position. To ask you to spare me.");
		AddSub(StoryCharacter.PRINCE, "To consider a scenario you never thought would be true.");
		AddSub(StoryCharacter.PILOT, "Never thought, never will. I don't believe you for a second.");
		AddSub(StoryCharacter.PRINCE, "I had a feeling that would be the case. I understand. All I ask is that you take some time to think on it.");
		AddSub(StoryCharacter.PRINCE, "I will leave you for now. Farewell, @N.");


		CreateStory(); // 153
		AddSub(StoryCharacter.IRIS, "How'd you like your present, @N?");
		AddSub(StoryCharacter.PILOT, "A pair of socks would've sufficed. Jeez. How long have you been best buddies with my arch nemesis?");
		AddSub(StoryCharacter.IRIS, "Nah, it ain't like that. Sure, I've known the Prince for a while. A lot of pirates do. He asks us about our 'adventures.'");
		AddSub(StoryCharacter.IRIS, "It's kinda endearing, and we DO love talking about the crazy crap we get up to.");
		AddSub(StoryCharacter.IRIS, "He kept sayin' he wants out of the war cuz he's - well, he's a pussycat.");
		AddSub(StoryCharacter.IRIS, "After a while I got to thinkin' I could make a few $C if I hooked him up with you, let him tell you his story.");
		AddSub(StoryCharacter.PILOT, "You actually believe him?");
		AddSub(StoryCharacter.IRIS, "Why not? I believe the stuff YOU say, and you're not even the same person you were last week.");
		AddSub(StoryCharacter.PILOT, "...");



		// SECTOR 28: SUBSISTENCE
		
		CreateStory(); // 154
		AddSub(StoryCharacter.MEDIC, "When did you last experience a migraine, @N?");
		AddSub(StoryCharacter.PILOT, "Few weeks ago. Just after my birthday.");
		AddSub(StoryCharacter.MEDIC, "Well, scans are coming back clear as usual.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.MEDIC, "What clone number are you?");
		AddSub(StoryCharacter.PILOT, "Uh... dunno. I 'll ask the computer.");
		AddSub(StoryCharacter.PILOT, "Computer. What clone number is operating this ship?");
		AddSub(StoryCharacter.COMPUTER, ">> CLONE 1439 OPERATES THIS SHIP");
		AddSub(StoryCharacter.PILOT, "1439...");
		AddSub(StoryCharacter.MEDIC, "Hmm. Alright. I think we're wasting time on remote tests. I should run some tests on the real you.");
		AddSub(StoryCharacter.MEDIC, "I'll speak with the Major about getting access to Captain @N.");
		AddSub(StoryCharacter.PILOT, "I'm on a tropical beach somewhere, enjoying a vacation. Shouldn't be too hard to find me.");
		AddSub(StoryCharacter.MEDIC, "Alright. I'll let you know how I get on.");
		AddSub(StoryCharacter.PILOT, "Sure. Say hi to me for me.");


		// SECTOR 30: THINK OF ME
		
		CreateStory(); // 155
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING AUDIO MESSAGE FROM MOTHER");
		AddSub(StoryCharacter.MOTHER, "Remember that lovely little song your father and I would sing you before bed time?");
		AddSub(StoryCharacter.MOTHER, "I was thinking about it today. How did it go again?");
		AddSub(StoryCharacter.MOTHER, "'If one day, I can't be heard.\nYou look back, but the picture's blurred...'");
		AddSub(StoryCharacter.MOTHER, "'Just think of me, and that will do.\nAnd know I'll always think of you.'");


		// SECTOR 32: ROMANS
		
		CreateStory(); // 156
		AddSub(StoryCharacter.PRINCE, "Hi, @N.");
		AddSub(StoryCharacter.PILOT, "Oh, great. Now I've got you messaging me whenever you want.");
		AddSub(StoryCharacter.PRINCE, "Sorry. You're right. I should have asked if you mind me messaging you directly.");
		AddSub(StoryCharacter.PILOT, "It's not so much THAT as... my sole purpose in life, right now, is to kill you and your entire family.");
		AddSub(StoryCharacter.PRINCE, "Yeah. I guess that's a bit awkward.");
		AddSub(StoryCharacter.PILOT, "Hah. Well at least you have a sense of humour.");


		CreateStory(); // 157
		AddSub(StoryCharacter.PRINCE, "For the record, I've never actually killed any of your people myself. Never even given orders TO kill.");
		AddSub(StoryCharacter.PRINCE, "I just sit in a ship and get treated like a Roman Emperor.");
		AddSub(StoryCharacter.PILOT, "'Roman Emperor?' What's that?");
		AddSub(StoryCharacter.PRINCE, "Ah.. really old human thing. We're talking trillions of years ago.");
		AddSub(StoryCharacter.PILOT, "Right. You a human historian or something?");
		AddSub(StoryCharacter.PRINCE, "Haha! Well, I've always fancied myself as a bit of a scholar. But I'm no expert.");
		AddSub(StoryCharacter.PRINCE, "I just like to read about lots of things. History especially. Often it's more bizarre than fiction.");


		CreateStory(); // 158
		AddSub(StoryCharacter.PRINCE, "Look, @N... I know you don't trust me. If I was you, I wouldn't.");
		AddSub(StoryCharacter.PRINCE, "If you have to tell your Major Brightman that I've been in touch, so be it.");
		AddSub(StoryCharacter.PRINCE, "But just know that not everyone in the Royal Empire is like the Viceroy, or the Crown Prosecutor.");
		AddSub(StoryCharacter.PRINCE, "They were men of war. Defenders of our long lasting Empire. I'm just a guy born into the wrong destiny.");
		AddSub(StoryCharacter.PILOT, "Don't think I'm going to fall for your sob story, Prince.");
		AddSub(StoryCharacter.PILOT, "I'm just taking my time seeing how far you're prepared to go to lead me into whatever trap you've got planned.");




		// SECTOR 33: Guillotine
		
		CreateStory(); // 159
		AddSub(StoryCharacter.PILOT, "AAAAGGGHHHH!");
		AddSub(StoryCharacter.ENGINEER, "@N! @N, you okay?");
		AddSub(StoryCharacter.PILOT, "NNNGHH!");
		AddSub(StoryCharacter.ENGINEER, "Ah, jeez! I'll call for the doctor.");
		AddSub(StoryCharacter.MEDIC, "@N, I'm here. Try and stay calm. Talk me through the pain.");
		AddSub(StoryCharacter.PILOT, "Fierce... Ahh.. It's tearing my skull apart");
		AddSub(StoryCharacter.MEDIC, "It's not, @N. Don't worry. It will fade, just like it always does. Sit back and let's run a scan.");
		AddSub(StoryCharacter.MEDIC, "...");
		AddSub(StoryCharacter.MEDIC, "Alright... yes, the scan is showing inflammation across the entire cerebral cortex.");
		AddSub(StoryCharacter.PILOT, "Just give me someone else's head already!");
		AddSub(StoryCharacter.MEDIC, "I'm going to transmit some electrode frequencies through the computer. It should relax the inflammation.");
		AddSub(StoryCharacter.COMPUTER, ">> MEDICAL ELECTRODE FREQUENCY TRANSFER COMPLETE");
		AddSub(StoryCharacter.MEDIC, "Alright... how's that?");
		AddSub(StoryCharacter.PILOT, "Yeah... yeah, it's gone.");


		CreateStory(); // 160
		AddSub(StoryCharacter.PILOT, "Thanks for the meds earlier, doc.");
		AddSub(StoryCharacter.MEDIC, "No problem, @N. By the way, any idea which tropical island your real self is currently vacationing on?");
		AddSub(StoryCharacter.PILOT, "Uhh. I... I dunno. Why?");
		AddSub(StoryCharacter.MEDIC, "Just having some difficulty tracking you down.");
		AddSub(StoryCharacter.PILOT, "Maybe speak with the Major. He ought to know.");
		AddSub(StoryCharacter.MEDIC, "Alright, will do. Try to take it easy out there, @N.");
		AddSub(StoryCharacter.PILOT, "Thanks, Doc. I'll try.");


		CreateStory(); // 161
		AddSub(StoryCharacter.PRINCE, "Sounds like those headaches you get are pretty nasty.");
		AddSub(StoryCharacter.PILOT, "Now you're listening in to my conversations? Well, that'll be the last straw there.");
		AddSub(StoryCharacter.PRINCE, "I know, it's rude of me. But I am fascinated by you and your condition.");
		AddSub(StoryCharacter.PILOT, "How about I let you have a close look just before I blow you to smithereens?");
		AddSub(StoryCharacter.PRINCE, "Don't you think it's a little weird that each clone copy of you is suffering from these extreme migraines?");
		AddSub(StoryCharacter.PRINCE, "Once or twice, sure, I could understand that. But it seems as though every clone has experienced it.");
		AddSub(StoryCharacter.PILOT, "Nobody's perfect. I'm just real, real close.");
		AddSub(StoryCharacter.PRINCE, "Each clone's memories are transferred to the next, right?");
		AddSub(StoryCharacter.PRINCE, "The migraines are probably due to extreme stress levels, inherited and made worse by each clone's experience.");
		AddSub(StoryCharacter.PILOT, "Everybody's an expert on me, it seems.");


		CreateStory(); // 162
		AddSub(StoryCharacter.PRINCE, "I've never experienced a migraine. Let alone something as painful as what I have heard you go through.");
		AddSub(StoryCharacter.PRINCE, "You have my sympathy.");
		AddSub(StoryCharacter.PILOT, "Gee whiz, thanks, buddy. I've always wanted my enemy's sympathy.");


		// SECTOR 34: THE DEAL
		
		CreateStory(); // 163
		AddSub(StoryCharacter.IRIS, "So, how's it going with your new boyfriend?");
		AddSub(StoryCharacter.PILOT, "I'm going to report it to Brightman. I've wasted enough time hearing the Prince beg for his life.");
		AddSub(StoryCharacter.IRIS, "Fair enough. At least you tried.");
		AddSub(StoryCharacter.PILOT, "Yep. And you're a few thousand $C richer. So everyone's happy.");


		CreateStory(); // 164
		AddSub(StoryCharacter.IRIS, "Tough break with @N, Prince. There I was thinking you guys would hold hands and live happily ever after.");
		AddSub(StoryCharacter.PRINCE, "Thank you for putting us in touch. I appreciate it. And I do not hold hard feelings, @N.");
		AddSub(StoryCharacter.PRINCE, "I have explained myself as best I can, and I understand that you have obligations as a soldier.");
		AddSub(StoryCharacter.PILOT, "Let's pretend for a second that I believe you want out of the Royal Empire. Isn't that treason?");
		AddSub(StoryCharacter.PILOT, "What about the Princess, King, Queen, or whoever else is in the Upper Hierarchy? ");
		AddSub(StoryCharacter.PRINCE, "Well, there is the Empress. All commands come from her.");
		AddSub(StoryCharacter.PRINCE, "She reigns supreme... And I'm sure she wouldn't approve of my decisions.");
		AddSub(StoryCharacter.PILOT, "So, the Empress leads the Royal Empire. And where do I find her?");
		AddSub(StoryCharacter.PRINCE, "Our Empire spans more galaxies than the Alliance Republic have even travelled, @N.");
		AddSub(StoryCharacter.PRINCE, "It will take you hundreds of years to find her.");
		AddSub(StoryCharacter.PILOT, "That's why Project Gemini exists. I don't care if I don't find her.");
		AddSub(StoryCharacter.PILOT, "I'm just clearing the way for the next Gemini Pilot once my tour is up.");


		CreateStory(); // 165
		AddSub(StoryCharacter.PRINCE, "If you spare me, I'll give you her coordinates.");
		AddSub(StoryCharacter.PILOT, "Wow, that sure doesn't sound like a trap in the slightest.");
		AddSub(StoryCharacter.PRINCE, "It isn't. Take the time to consider. The offer remains on the table.");
		AddSub(StoryCharacter.PILOT, "You're a true patriot.");


		// SECTOR 35: ABSENCE


		CreateStory(); // 166
		AddSub(StoryCharacter.PILOT, "Hey Ren. Wasn't Doctor Grace supposed to get in touch today?");
		AddSub(StoryCharacter.ENGINEER, "Yeah, I've got the appointment written down in the diary. Have you not heard from her?");
		AddSub(StoryCharacter.PILOT, "Nope.");
		AddSub(StoryCharacter.ENGINEER, "Alright. Let me see if I can get a hold of her.");



		CreateStory(); // 167
		AddSub(StoryCharacter.ENGINEER, "Hey, got some bad news...");
		AddSub(StoryCharacter.PILOT, "What is it?");
		AddSub(StoryCharacter.ENGINEER, "Looks like the doc's called in sick.");
		AddSub(StoryCharacter.PILOT, "Crap. What's wrong with her?");
		AddSub(StoryCharacter.ENGINEER, "Her secretary just said she's not feeling well. Says she needs some time off. We don't know when she'll be back.");
		AddSub(StoryCharacter.PILOT, "What a selfish *****.");
		AddSub(StoryCharacter.ENGINEER, "I'll see if I can get in touch with her at home. Guess you'll have to just keep fighting through those migraines for now.");
		AddSub(StoryCharacter.PILOT, "Yeah...");



		// SECTOR 36: LIMITED
		
		CreateStory(); // 168
		AddSub(StoryCharacter.PILOT, "Alright.. enough's enough. Time to report to Brightman.");
		AddSub(StoryCharacter.PILOT, "@N to Major Brightman. You there, sir?");
		AddSub(StoryCharacter.BRIGHTMAN, "I'm here, @N. Report.");
		AddSub(StoryCharacter.PILOT, "I've... just... been contacted by the Prince of the Royal Empire.");
		AddSub(StoryCharacter.BRIGHTMAN, "Finally! And?!");
		AddSub(StoryCharacter.PILOT, "He's as crazy as the rest of 'em, that's for sure. Ren's locked on to his coordinates. We know where he is.");
		AddSub(StoryCharacter.BRIGHTMAN, "Excellent work, soldier. You know what to do.");
		AddSub(StoryCharacter.PILOT, "Uh, yeah, about that...");
		AddSub(StoryCharacter.BRIGHTMAN, "What is it?");
		AddSub(StoryCharacter.PILOT, "Well, sir... the end of my tour is coming up. One week to go, actually. ");
		AddSub(StoryCharacter.BRIGHTMAN, "And?");
		AddSub(StoryCharacter.PILOT, "And well. There's no way I can reach the Prince and eliminate him in a week.");
		AddSub(StoryCharacter.PILOT, "It's going to take time. Then after that, it's the Empress.");
		AddSub(StoryCharacter.BRIGHTMAN, "So take your time. We'll extend your tour.");
		AddSub(StoryCharacter.PILOT, "No, you don't understand, sir. What I'm saying is... I want to go home. I'm done, Major.");
		AddSub(StoryCharacter.BRIGHTMAN, "...");
		AddSub(StoryCharacter.PILOT, "Major?");


		CreateStory(); // 169
		AddSub(StoryCharacter.PILOT, "Major, are you there?");
		AddSub(StoryCharacter.BRIGHTMAN, "You're 'done?'");
		AddSub(StoryCharacter.PILOT, "Yes sir. I think it's time for the next Gemini Pilot to be activated.");
		AddSub(StoryCharacter.BRIGHTMAN, "You insubordinate piece of...");
		AddSub(StoryCharacter.PILOT, "Sir?");
		AddSub(StoryCharacter.BRIGHTMAN, "You've been floating around space, frolicking with pirates, having a good old laugh at the Alliance's expense.");
		AddSub(StoryCharacter.BRIGHTMAN, "Absolutely NO progress finding the remaining Upper Hierarchy members. And now that THEY do the job for you, you want OUT?!");
		AddSub(StoryCharacter.PILOT, "I signed up for a set tour, Major. You want me to stick out my last week? Fine. But I won't take down the Prince. That just can't happen.");
		AddSub(StoryCharacter.PILOT, "There's not enough time. So all I'm saying is...");
		AddSub(StoryCharacter.PILOT, "Activate the next Gemini pilot. And when my tour's up, send me home as promised. I want normality again.");
		AddSub(StoryCharacter.BRIGHTMAN, "We're fighting a war here. You're a soldier. There's no such thing as normality for soldiers.");
		AddSub(StoryCharacter.PILOT, "What the hell is your problem? I've single handedly FOUGHT this war for the past EIGHT years!");
		AddSub(StoryCharacter.BRIGHTMAN, "Gemini pilot @N, listen very carefully. You are hereby under detention for the foreseeable future.");
		AddSub(StoryCharacter.BRIGHTMAN, "The real Captain @N will be detained in Alliance Republic HQ until you have completed your mission.");
		AddSub(StoryCharacter.PILOT, "WHAT?");


		CreateStory(); // 170
		AddSub(StoryCharacter.PILOT, "You can't do this, Major.");
		AddSub(StoryCharacter.BRIGHTMAN, "That's where you're wrong, soldier. I can, and I will. Your traitorous actions have cost you a longer tour.");
		AddSub(StoryCharacter.BRIGHTMAN, "FIND the Prince. DESTROY him. THEN we'll talk about your tour being up.");
		AddSub(StoryCharacter.PILOT, "What? **** you!");
		AddSub(StoryCharacter.BRIGHTMAN, "I'm going to cut COMMS between you and HQ for a while to let you calm down.");
		AddSub(StoryCharacter.BRIGHTMAN, "Now, if I was you - I'd do my job so I can come home. Brightman out.");


		CreateStory(); // 171
		AddSub(StoryCharacter.IRIS, "Ouch. That didn't work out as you'd hoped, huh?");
		AddSub(StoryCharacter.PILOT, "Shut up, Iris. Not now.");



		// SECTOR 37: BETRAYAL
		
		CreateStory(); // 172
		AddSub(StoryCharacter.IRIS, "Hi there, gloomy guts.");
		AddSub(StoryCharacter.PILOT, "What do you want?");
		AddSub(StoryCharacter.IRIS, "Are you going to just sit there and do nothing?");
		AddSub(StoryCharacter.PILOT, "Dunno. Maybe.");
		AddSub(StoryCharacter.IRIS, "So you've been backstabbed by your boss. It's hurt your feelings. I get it.");
		AddSub(StoryCharacter.IRIS, "But there's no point in drifting out in space with your face in your hands.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.IRIS, "Why don't you go clear some sectors, huh? Kill a few Royal Empire losers, let me pick up some of the spare $C...");
		AddSub(StoryCharacter.PILOT, "Hmph... Yeah, I guess I could.");
		AddSub(StoryCharacter.IRIS, "THAT'S the @N I know! Now hurry up. Momma needs a new pair of shoes.");


		CreateStory(); // 173
		AddSub(StoryCharacter.PILOT, "Just... Can't believe this has happened. I've served my time. Damnit!");
		AddSub(StoryCharacter.IRIS, "He's a no-brained army man, getting ordered around by bigger no-brained army men. What do you expect?");


		CreateStory(); // 174
		AddSub(StoryCharacter.PILOT, "After everything I've gone through, Iris... I'm done with this crap.");
		AddSub(StoryCharacter.PILOT, "The Alliance Republic. Project Gemini. The Royal Empire. The migraines. All of it.");
		AddSub(StoryCharacter.PILOT, "I just want to be ONE person again, enjoying retirement.");
		AddSub(StoryCharacter.IRIS, "Don't kid yourself. That ain't gonna happen. Not unless you deliver what he wants.");
		AddSub(StoryCharacter.PILOT, "So I gotta kill the Prince. Which means tracking him down. Fighting him. That all takes time.");


		CreateStory(); // 175
		AddSub(StoryCharacter.IRIS, "That does take time. But it'd take less time if you were to... I dunno. NOT kill the Prince.");
		AddSub(StoryCharacter.PILOT, "And exactly how would NOT killing the Prince get me back in Brightman's good books?");
		AddSub(StoryCharacter.IRIS, "Try to keep up, doofus. I'm suggesting you give the Prince and Brightman what they both want. We FAKE the Prince's death.");
		AddSub(StoryCharacter.IRIS, "Brightman thinks the Prince is dead, lets you finish your tour. The Prince gets to pussy out of the war like he's always dreamt.");
		AddSub(StoryCharacter.IRIS, "You pay me a few $C for thinking up this master plan. Everyone's a winner.");
		AddSub(StoryCharacter.PILOT, "Hmm...");


		CreateStory(); // 176
		AddSub(StoryCharacter.IRIS, "Look, you're pissed off with your home boys. So live a little and stick it to 'em for once in your boring multiple lives.");
		AddSub(StoryCharacter.PILOT, "Maybe you're right. I don't know. I need to think about it.");
		AddSub(StoryCharacter.IRIS, "Take your time. You've got, probably, forever.");


		// SECTOR 38:  CONTEMPLATION
		
		CreateStory(); // 177
		AddSub(StoryCharacter.PILOT, "Could... Could this really work?");


		// SECTOR 39: MEMORIES
		
		CreateStory(); // 178
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING MESSAGE FROM MOTHER");
		AddSub(StoryCharacter.MOTHER, "I was watching back some old recordings of you at your school sports day.");
		AddSub(StoryCharacter.MOTHER, "You were such a cute little runner, with your spindly little legs!");
		AddSub(StoryCharacter.MOTHER, "Do you remember helping that little boy who sprained his ankle? You helped him finish the race and got 9th place because of it.");
		AddSub(StoryCharacter.MOTHER, "You've always been so caring about others.");
		AddSub(StoryCharacter.PILOT, "...");


		// SECTOR 40: DECISION
		
		CreateStory(); // 179
		AddSub(StoryCharacter.PILOT, "Oh, screw it. What have I got to lose? Brightman wants to mess with me? So be it.");



		// SECTOR 41: NOT BY BLOOD
		
		CreateStory(); // 180
		AddSub(StoryCharacter.PILOT, "Iris. Prince. Come in. It's @N.");
		AddSub(StoryCharacter.IRIS, "Sup?");
		AddSub(StoryCharacter.PRINCE, "Hello, @N.");
		AddSub(StoryCharacter.PILOT, "I've thought long and hard about this, and... I'm going to let you go free, Prince.");
		AddSub(StoryCharacter.PRINCE, "Really?! Thank you, @N... I... thank you so much!");
		AddSub(StoryCharacter.PILOT, "We need Brightman to think that you're dead. Hell, we need the Royal Empire to think you're dead, too.");
		AddSub(StoryCharacter.PILOT, "Otherwise no one's going to believe this. ONLY the three of us can know. Understood?");
		AddSub(StoryCharacter.PRINCE, "Of course. I understand.");
		AddSub(StoryCharacter.IRIS, "I'll keep my mouth shut.");
		AddSub(StoryCharacter.PILOT, "Good. Turn off all COMMS back home, Prince, and come find me. Iris, I want you to record the whole thing.");


		CreateStory(); // 181
		AddSub(StoryCharacter.PILOT, "We have to keep things looking as normal as possible afterwards.");
		AddSub(StoryCharacter.PILOT, "I've got to keep clearing sectors, make it look like I'm after the Empress.");
		AddSub(StoryCharacter.PILOT, "That'll give you enough time to get a million universes away from here, Prince.");
		AddSub(StoryCharacter.PRINCE, "Good thinking. And as promised, I will give you the Empress' coordinates.");
		AddSub(StoryCharacter.IRIS, "You're really just going to hand over your own mom, just like that?");
		AddSub(StoryCharacter.PRINCE, "She's not my mother. The Empress is a being that has lived for centuries. She is the only thing as old as our Empire.");
		AddSub(StoryCharacter.PRINCE, "Her royal family are born anew every few years. I'm only the Prince by chance, not by blood.");
		AddSub(StoryCharacter.IRIS, "Jeez. Ain't that a bit cold...");


		CreateStory(); // 182
		AddSub(StoryCharacter.PRINCE, "@N... Thank you again. I really mean it. I've wanted out of this war for so long. Finally, I can explore other worlds.");
		AddSub(StoryCharacter.PRINCE, "I can be free of the Royal Empire's wrongdoing, and the judgement of the Alliance Republic.");
		AddSub(StoryCharacter.PILOT, "I know that feeling more and more each day. Don't mention it. And whatever you do... don't make me regret this.");
		AddSub(StoryCharacter.PILOT, "If this is all some sort of elaborate trap...");
		AddSub(StoryCharacter.PRINCE, "It isn't. I promise you.");
		AddSub(StoryCharacter.PILOT, "Alright. Are you within range?");
		AddSub(StoryCharacter.PRINCE, "Only a few clicks away. Keep coming. We will meet soon.");



		CreateStory(); // 183
		AddSub(StoryCharacter.PRINCE, "I can see your ship! This is it... it's finally happening.");



		CreateStory(); // 184
		AddSub(StoryCharacter.PRINCE, "Everything is ready, @N. I'm in the escape pod. The ship is on auto defence.");
		AddSub(StoryCharacter.PILOT, "Make sure that auto defence knows how to fight. This has got to look real.");
		AddSub(StoryCharacter.PRINCE, "Don't worry, it's a sophisticated machine. It fights a damn sight better than I do. Here I come!");


		CreateStory(); // 185
		AddSub(StoryCharacter.IRIS, "Holy crap, that's got to be the biggest explosion I've ever seen.");
		AddSub(StoryCharacter.PILOT, "Prince! Prince, are you out?");
		AddSub(StoryCharacter.IRIS, "A lot of static on our channel...");
		AddSub(StoryCharacter.PILOT, "Prince! Come in, Prince!");
		AddSub(StoryCharacter.PRINCE, "I'm here! I ... you... we did it! I'm free!");
		AddSub(StoryCharacter.PILOT, "Didn't think you got out in time, then.");
		AddSub(StoryCharacter.PRINCE, "It was close. I thought I was a goner, too. Thank you, @N... thank you, Iris. I don't know what to say.");
		AddSub(StoryCharacter.PILOT, "Then don't speak. Just get out of here before your people start poking around.");
		AddSub(StoryCharacter.PRINCE, "I have sent you the Empress' coordinates. You are both the greatest friends I have ever had, truly.");
		AddSub(StoryCharacter.PRINCE, "Goodbye, both of you. May we be re-united as star dust one day to come.");
		AddSub(StoryCharacter.IRIS, "Ugh. Gag factor. Leave it to the Royal Empire dude to get all sentimental.");
		AddSub(StoryCharacter.PILOT, "Alright, let's send the footage to Alliance HQ with a distress signal.");
		AddSub(StoryCharacter.PILOT, "That ought to get Brightman's attention.");


		// SECTOR 42: NO HARD FEELINGS


		CreateStory(); // 186
		AddSub(StoryCharacter.BRIGHTMAN, "@N. Come in, @N.");
		AddSub(StoryCharacter.PILOT, "Major Brightman?");
		AddSub(StoryCharacter.BRIGHTMAN, "Yes, it's me. We received a distress signal and video footage of a Royal Empire ship going down. Was it who I think it is?");
		AddSub(StoryCharacter.PILOT, "You got it. Prince Cyril Enfant of the Royal Empire.");
		AddSub(StoryCharacter.BRIGHTMAN, "HAH! I knew you could do it! All you needed was a firm shove in the right direction. No hard feelings about before, old friend.");
		AddSub(StoryCharacter.PILOT, "None taken, sir.");
		AddSub(StoryCharacter.BRIGHTMAN, "According to our intelligence, The Empress is the head member of the Upper Hierarchy. She IS the Royal Empire.");
		AddSub(StoryCharacter.PILOT, "I had a heated exchange with the Prince before I tore a hole in his hull.");
		AddSub(StoryCharacter.PILOT, "Had him begging for his life. Guess what I convinced him to give me?");
		AddSub(StoryCharacter.BRIGHTMAN, "What? What?!");
		AddSub(StoryCharacter.PILOT, "The Empress' coordinates.");
		AddSub(StoryCharacter.BRIGHTMAN, "HAHA! SPANK MY ASS, @NN, THAT'S INCREDIBLE!");
		AddSub(StoryCharacter.PILOT, "Uh, yeah.");
		AddSub(StoryCharacter.BRIGHTMAN, "You're going to be the biggest hero this nation has ever had, @N. The media are going to go NUTS over this.");
		AddSub(StoryCharacter.BRIGHTMAN, "I'm going to go let the bigwigs know. Great work, soldier!");


		CreateStory(); // 187
		AddSub(StoryCharacter.ENGINEER, "Hey @N.");
		AddSub(StoryCharacter.PILOT, "Ren! Long time no speak. Did you miss me?");
		AddSub(StoryCharacter.ENGINEER, "Missed getting teased all the time? Not really.");
		AddSub(StoryCharacter.PILOT, "Haha!");
		AddSub(StoryCharacter.ENGINEER, "Look, I'm going to COMMS you privately, okay? There's something you need to hear.");
		AddSub(StoryCharacter.PILOT, "Uh... alright.");


		CreateStory(); // 188
		AddSub(StoryCharacter.ENGINEER, "Okay, I'm here.");
		AddSub(StoryCharacter.PILOT, "What's up? You seem a little on edge.");
		AddSub(StoryCharacter.ENGINEER, "@N, while COMMS were blocked I was told to take some time off. So I did. Kinda. I spent that time trying to get hold of the Doctor.");
		AddSub(StoryCharacter.PILOT, "Oh, right. Any luck? How is she?");
		AddSub(StoryCharacter.ENGINEER, "She's dead, @N.");
		AddSub(StoryCharacter.PILOT, "What, really? Jeez, she must have been really sick.");
		AddSub(StoryCharacter.ENGINEER, "I don't think she was sick.");
		AddSub(StoryCharacter.PILOT, "What...?");
		AddSub(StoryCharacter.ENGINEER, "I think she was murdered.");


		CreateStory(); // 189
		AddSub(StoryCharacter.ENGINEER, "She kept an audio log, which gets sent securely to her secretary. I went round her office to meet with him.");
		AddSub(StoryCharacter.ENGINEER, "He said she was acting strangely before she died. He mentioned the audio log, so I asked to listen to it.");
		AddSub(StoryCharacter.PILOT, "And?");
		AddSub(StoryCharacter.ENGINEER, "She asked Brightman for your location. The real you. Brightman kept coming up with excuses, refusing to tell her.");
		AddSub(StoryCharacter.ENGINEER, "The Doc got impatient, started accusing Brightman of wrongdoing.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.ENGINEER, "In her last audio log, she talks through her investigation into all this. You're not going to like what she found.");


		CreateStory(); // 190
		AddSub(StoryCharacter.ENGINEER, "She hacked into Project Gemini's data bank and apparently, you're not on a beach somewhere, enjoying retirement.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.ENGINEER, "She thinks you're being kept locked up in Project Gemini's research lab, kept in a permanent coma... being cloned endlessly.");
		AddSub(StoryCharacter.PILOT, "...");

		// SECTOR 43: DIVIDE
		
		CreateStory(); // 191
		AddSub(StoryCharacter.PILOT, "Those bastards, using me as a goddamn cloning machine. I might as well be dead!");
		AddSub(StoryCharacter.ENGINEER, "@N, I'm so sorry. I mean, if I'd known... I can't believe they'd do this. Project Gemini is pure evil!");
		AddSub(StoryCharacter.IRIS, "Why are you two acting so surprised?");
		AddSub(StoryCharacter.PILOT, "Oh, like you saw this coming.");
		AddSub(StoryCharacter.IRIS, "Maybe not THIS, but jeez.. SOMETHING weird had to be going on. Cloning a soldier millions of time to fight a war for you?");
		AddSub(StoryCharacter.IRIS, "LYING to the general public, even to that person's family, making everyone think that it's just ONE person...");
		AddSub(StoryCharacter.IRIS, "To mislead them into thinking they're the greatest soldier that ever lived?");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.IRIS, "You believed it too, didn't you? Thought you were the best. All this time I've been flying next to you.");
		AddSub(StoryCharacter.IRIS, "Me with my ONE life. You with millions. You know how I've survived this long, @N? RUNNING.");
		AddSub(StoryCharacter.IRIS, "It ain't about fighting all the time. It's about knowing which battles to fight, and which ones to run away from.");
		AddSub(StoryCharacter.IRIS, "THAT'S how you stay alive.");


		CreateStory(); // 192
		AddSub(StoryCharacter.ENGINEER, "We can't just sit back and let this happen, @N.");
		AddSub(StoryCharacter.PILOT, "What can I do? I can't go back and rescue myself.");
		AddSub(StoryCharacter.ENGINEER, "No... but I can.");
		AddSub(StoryCharacter.PILOT, "What?!");


		CreateStory(); // 193
		AddSub(StoryCharacter.ENGINEER, "I can sneak into the Project Gemini lab. I'll get you out of the cloning machine and fly you out.");
		AddSub(StoryCharacter.PILOT, "... You'd do that for me, Ren?");
		AddSub(StoryCharacter.IRIS, "Are you kidding me? He couldn't sneak his way into an un-fenced field.");
		AddSub(StoryCharacter.ENGINEER, "I have to try!");
		AddSub(StoryCharacter.IRIS, "You'll get yourself killed, going at it alone. I'll go with you.");
		AddSub(StoryCharacter.PILOT, "What? Iris, this is no time for your bad jokes.");
		AddSub(StoryCharacter.IRIS, "I ain't jokin', clone. You want freedom? Alright, fine.");
		AddSub(StoryCharacter.IRIS, "You've greased my palm a fair bit out here and made things a little less boring.");
		AddSub(StoryCharacter.IRIS, "I'll give Ren a hand getting you out of the land of the 'free.'");


		CreateStory(); // 194
		AddSub(StoryCharacter.PILOT, "I can't let you guys do this. This is my fight, my problem. Not yours.");
		AddSub(StoryCharacter.IRIS, "What're you going to do? Fly back and steal yourself? They'll shut down the clones and blow your brains out. Then what?");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.IRIS, "I've started my jump towards AR space. I should be there within a week if I keep up the pace.");
		AddSub(StoryCharacter.IRIS, "Ren, you get everything ready. I'm going to need a new assault rifle. Preferably something in pink.");
		AddSub(StoryCharacter.ENGINEER, "You got it.");
		AddSub(StoryCharacter.IRIS, "@N, you carry on towards The Empress as normal.");
		AddSub(StoryCharacter.IRIS, "Brightman can't think there's something going on, or this whole thing goes down the crapper.");
		AddSub(StoryCharacter.PILOT, "Thanks, Iris...");
		AddSub(StoryCharacter.IRIS, "Shut up. I'm only doing this because you'll be financially indebted to me for the rest of your life.");


		// SECTOR 44: JAILBREAK
		
		CreateStory(); // 195
		AddSub(StoryCharacter.PILOT, "Computer, begin audio recording.");
		AddSub(StoryCharacter.COMPUTER, ">> AUDIO RECORDING INITIATED");
		AddSub(StoryCharacter.PILOT, "Hey Mom. Sorry it's been so long. Look, I've got to be quick... something's up. Something bad.");
		AddSub(StoryCharacter.PILOT, "I can't go into it now, but I just want you to know that-");
		AddSub(StoryCharacter.ENGINEER, "@N, we're ready.");
		AddSub(StoryCharacter.PILOT, "... Alright, I'm coming to the bridge.");
		AddSub(StoryCharacter.PILOT, "Computer, delete audio message.");
		AddSub(StoryCharacter.COMPUTER, ">> AUDIO MESSAGE DELETED");


		CreateStory(); // 196
		AddSub(StoryCharacter.IRIS, "Alright, this is it. Ready to rock.");
		AddSub(StoryCharacter.ENGINEER, "We have the location of where we think you're being kept, @N. It's late, so there should be minimal security at the lab.");
		AddSub(StoryCharacter.PILOT, "Be careful, you two. Keep me updated as you go.");
		AddSub(StoryCharacter.IRIS, "Aw, how sweet. You're all worried! Chill, @N. We'll be in and out, no problem.");


		CreateStory(); // 197
		AddSub(StoryCharacter.ENGINEER, "We're in the facility. First set of security have been taken out. So far so good.");
		AddSub(StoryCharacter.IRIS, "Still think you should have let me kill them. Saves them the trouble of waking back up.");
		AddSub(StoryCharacter.ENGINEER, "Alright, we're approaching Sector 6. This should lead to a corridor that branches out into the main clinical labs.");
		AddSub(StoryCharacter.ENGINEER, "@N's being kept in room 68C.");
		AddSub(StoryCharacter.IRIS, "Ssh. What was that?");
		AddSub(StoryCharacter.PILOT, "What? What is it?");
		AddSub(StoryCharacter.IRIS, "I heard somet--");
		AddSub(StoryCharacter.IRIS, "*BLAM* *BLAM* *RATATAT*");
		AddSub(StoryCharacter.IRIS, "IT'S A SET UP!");
		AddSub(StoryCharacter.ENGINEER, "Open fire, Iri-- *BLAM*");
		AddSub(StoryCharacter.IRIS, "Ah goddamnit, Ren's down!");
		AddSub(StoryCharacter.PILOT, "REN! IRIS, GET OUT OF THERE!");
		AddSub(StoryCharacter.IRIS, "I'm pinned down. Ah jeez, Ren's done for! @N--");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.PILOT, "Iris! Ren? Hello!?");


		CreateStory(); // 198
		AddSub(StoryCharacter.PILOT, "Damnit Iris, answer me!");
		AddSub(StoryCharacter.BRIGHTMAN, "Don't think she'll be doing that, @N.");
		AddSub(StoryCharacter.PILOT, "Brightman...");
		AddSub(StoryCharacter.BRIGHTMAN, "You really thought you could pull this off, huh? Didn't think for a second that I'd be bugging that little coward, Ren.");
		AddSub(StoryCharacter.BRIGHTMAN, "I mean... DEAD little coward.");
		AddSub(StoryCharacter.PILOT, "You bastard...");
		AddSub(StoryCharacter.BRIGHTMAN, "Your plan was foiled from the start, @N. Did you REALLY think you'd find yourself here?");
		AddSub(StoryCharacter.PILOT, "Where are you keeping me, you son of a-");
		AddSub(StoryCharacter.BRIGHTMAN, "Nowhere, @N. Captain @N died four years ago. You're just a ghost now.");
		AddSub(StoryCharacter.PILOT, "What...?");


		CreateStory(); // 199
		AddSub(StoryCharacter.BRIGHTMAN, "You're the best pilot we've ever had. A fine specimen of the human race. But even bodies as strong as yours have their limits.");
		AddSub(StoryCharacter.BRIGHTMAN, "We successfully achieved infinite cloning possibility by cloning you.");
		AddSub(StoryCharacter.BRIGHTMAN, "By that point, your body gave out under the intense process. Your organs failed, and you died.");
		AddSub(StoryCharacter.PILOT, "You... you're lying.");
		AddSub(StoryCharacter.BRIGHTMAN, "We were unsure what effect this would have on the clones.");
		AddSub(StoryCharacter.BRIGHTMAN, "We waited, and over time saw that things seemed to be functioning as normal, apart from the odd migraine.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.BRIGHTMAN, "Luckily, you've never been much of a family person.");
		AddSub(StoryCharacter.BRIGHTMAN, "We blocked you from sending audio messages back home, but let your mother continue to send her gushing crap to you.");
		AddSub(StoryCharacter.BRIGHTMAN, "We thought it would continue to motivate the clones. As far as we could see, it was working.");


		CreateStory(); // 200
		AddSub(StoryCharacter.PILOT, "I'm going to kill you with my bare hands.");
		AddSub(StoryCharacter.BRIGHTMAN, "And exactly how do you plan on doing that? ONE line of code, @N, and you're no longer free thinking.");
		AddSub(StoryCharacter.BRIGHTMAN, "Every clone becomes an empty shell, with only one purpose. To find The Empress and destroy her.");
		AddSub(StoryCharacter.BRIGHTMAN, "You think we've been inhumane? Unethical? We let you have free thought all this time. And you thank us with betrayal.");
		AddSub(StoryCharacter.PILOT, "YOU BASTARD!");
		AddSub(StoryCharacter.BRIGHTMAN, "I'm just a humble servant of the free people of the Alliance Repu-AAGH!");
		AddSub(StoryCharacter.PILOT, "Major Brightman?");
		AddSub(StoryCharacter.BRIGHTMAN, " * BLAM! BLAM! BLAM! *");
		AddSub(StoryCharacter.IRIS, "God DAMN I hated that guy's smug face.");
		AddSub(StoryCharacter.PILOT, "Iris!");
		AddSub(StoryCharacter.IRIS, "Fifth rule of piracy. Always confirm your kill. Guess they don't teach you that in Alliance school.");
		AddSub(StoryCharacter.PILOT, "Are you okay?");
		AddSub(StoryCharacter.IRIS, "I'm hit, but I'll survive. I better get out of here, though.");


		// CHAPTER 45 - FAILURE ONENTERSECTOR
		CreateStory (); // 201
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.IRIS, "...@N, you okay?");
		AddSub(StoryCharacter.PILOT, "Am I okay?!");
		AddSub(StoryCharacter.PILOT, "Ren's dead. Brightman was using me this whole time. I'M dead. I have nothing left.");
		AddSub(StoryCharacter.IRIS, "Uh, can't help but notice I'M still alive. Doesn't that count for something?");
		AddSub(StoryCharacter.PILOT, "I put you at risk. You're injured. And for what? Nothing!");
		AddSub(StoryCharacter.IRIS, "It's just a bullet in my belly. Nothin' I ain't been through before. Hah--aah, ow..");
		AddSub(StoryCharacter.PILOT, "I'm nothing. Just one of many clones of a dead soldier.");

		// 45 ONMIDSECTOR
		CreateStory (); // 202
		AddSub(StoryCharacter.IRIS, "@N, I'm heading back to you, okay? Slow down and we'll talk about this.");
		AddSub(StoryCharacter.PILOT, "Don't waste any more of your time, Iris. It's futile.");

		// 45 ONPOSTBOSSFIGHT
		CreateStory (); // 203
		AddSub(StoryCharacter.IRIS, "Hey! I said slow down! I'm trying to catch you up, dumbass!");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.IRIS, "So this is it, huh? You're just going to give up.");
		AddSub(StoryCharacter.PILOT, "Open your eyes, Iris. It's over. Brightman was just a tiny cog in the Alliance's wheel. All that will be left is my mission to Destroy The Empress.");
		AddSub (StoryCharacter.PILOT, "As soon as they find out what happened, they'll re-program the remaining clones to not have free thought. My consciousness will be gone.");

		// CHAPTER 46 - MAYBE IF
		CreateStory (); // 204
		AddSub(StoryCharacter.IRIS, "I'm about halfway there, @N. Just STOP for a while, will you?!");
		AddSub(StoryCharacter.PILOT, "Just leave me alone, Iris. Go and get medical attention. I'll wire you some $C. Buy yourself whatever the hell you want and get out of my way.");

		//46 ONMIDSECTOR
		CreateStory (); // 205
		AddSub(StoryCharacter.IRIS, "Hey asshole, I took a bullet for you. You're going to just run away now that things have gotten bad?");
		AddSub (StoryCharacter.PILOT, "All my life, I wanted to serve the Alliance Republic. I wanted to be a hero.");
		AddSub (StoryCharacter.PILOT, "A face that the people can look up to and not feel so scared of the war.");

		//46 ONPOSTBOSSFIGHT
		CreateStory (); // 206
		AddSub(StoryCharacter.IRIS, "So you haven't been able to live up to the expectations you set for yourself. Boo freaking hoo.");
		AddSub(StoryCharacter.IRIS, "You play with the cards you're dealt.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.IRIS, "Yeesh. Somebody's a cranky-clone-guts. Poor widdle baby, got screwed over by their crazy government. C'mon @N, grow up.");
		AddSub(StoryCharacter.PILOT, "Leave me alone, pirate.");
		AddSub(StoryCharacter.IRIS, "FINE! Stew in your self pity. I'm gonna go get drunk. ****.");
		AddSub(StoryCharacter.PILOT, "...");

		//SECTOR 47 -  ONENTERSECTOR
		CreateStory (); // 207
		AddSub(StoryCharacter.PILOT, "...");

		// ONMIDSECTOR
		CreateStory (); // 208
		AddSub(StoryCharacter.PILOT, "...");

		// ONPREBOSSFIGHT
		CreateStory (); // 209
		AddSub(StoryCharacter.PILOT, "...");

		// ONPOSTBOSSFIGHT
		CreateStory (); // 210
		AddSub(StoryCharacter.PILOT, "...");

		// SECTOR 48 - SO PROUD OF MY BABY
		//ONENTERSECTOR
		CreateStory (); // 211
		AddSub(StoryCharacter.MOTHER, "You know, any other mother might get angry at their child for never replying to them.");
		AddSub(StoryCharacter.MOTHER, "But not me. I know you're busy, and I'm SO proud of my baby. Hero of the Alliance Republic!");

		// ONMIDSECTOR
		CreateStory (); // 212
		AddSub(StoryCharacter.PILOT, "...");

		// ONPOSTBOSSFIGHT
		CreateStory (); // 213
		AddSub(StoryCharacter.PILOT, "..damnit..");

		// SECTOR 49 - SUBMISSION
		// ONENTERSECTOR
		CreateStory (); // 214
		AddSub(StoryCharacter.PILOT, "...bastards...");
		AddSub(StoryCharacter.PILOT, "JUST HURRY UP AND DO IT ALREADY!");

		//ONMIDSECTOR
		CreateStory (); // 215
		AddSub(StoryCharacter.PILOT, "Computer. Patch me in to Alliance HQ.");
		AddSub(StoryCharacter.COMPUTER, ">> COMMS LINK WITH ALLIANCE HQ INITIATED");
		AddSub(StoryCharacter.PILOT, "This is Gemini Pilot @N. I want to know when you evil pieces of crap are going to reprogram me into submission.");
		AddSub(StoryCharacter.PILOT, "Get it over and done with quickly, damnit...");
		AddSub(StoryCharacter.PILOT, "Hello?! Come in!");
		AddSub(StoryCharacter.PILOT, "GOD DAMNIT!");

		//ONPOSTBOSSFIGHT
		CreateStory (); // 216
		AddSub(StoryCharacter.DALTON, "Well. You've been very busy.");
		AddSub(StoryCharacter.PILOT, "Finally! Who is this? General Ashback? Commander Rhill?");
		AddSub(StoryCharacter.DALTON, "Try... President Dalton.");
		AddSub(StoryCharacter.PILOT, "Mr President...");

		// SECTOR 50 - NO MORE GAMES
		CreateStory(); // 217
		AddSub(StoryCharacter.DALTON, "It took a while to clean up the mess you and your friends made in my compound, Gemini Pilot @N.");
		AddSub(StoryCharacter.PILOT, "I should have told Iris to burn the place to the ground.");
		AddSub(StoryCharacter.DALTON, "Mmm. That would have done little more than light up the night's sky.");
		AddSub(StoryCharacter.PILOT, "Listen to me, you horrible excuse for a human being. I want out. You've used me for long enough.");
		AddSub(StoryCharacter.PILOT, "I know you're not going to let me go. I don't care about that any more.");
		AddSub(StoryCharacter.PILOT, "I don't want to be a conscious part of this mess. Just re-program me and be done with it.");
		AddSub(StoryCharacter.DALTON, "I can't re-program you, @N.");
		AddSub(StoryCharacter.PILOT, "STOP LYING TO ME!");

		// 50 - ONMIDSECTOR
		CreateStory(); // 218
		AddSub(StoryCharacter.DALTON, "No more games, @N. I can assure you that every word you hear from me is the truth. It's no longer in our interests to keep you in the dark.");
		AddSub(StoryCharacter.PILOT, "I despise you and your interests.");
		AddSub(StoryCharacter.DALTON, "What would you like from me, Gemini Pilot? An apology? Some sort of elaborate excuse for the choices we made around Project Gemini?");
		AddSub(StoryCharacter.DALTON, "I will do no such thing.");
		AddSub(StoryCharacter.PILOT, "I just want OUT!");
		AddSub(StoryCharacter.DALTON, "You want out? No problem.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.DALTON, "Destroy the Empress. Then, we'll have no need for you. I'll shut down the project.");

		// 50 - ONPREPAREFORBOSS
		CreateStory(); // 219
		AddSub(StoryCharacter.DALTON, "Imagine a great nation, with a great leader and great people. Over time, the nation grows.");
		AddSub(StoryCharacter.DALTON, "But with growth comes complication. Just like when we grow and become older...");
		AddSub(StoryCharacter.DALTON, "...we become more prone to the score of medical conditions that are hell-bent to stop humanity from becoming TOO strong.");
		AddSub(StoryCharacter.DALTON, "The list of potential complications is endless. The people see the length of the list, and this overwhelms them more than any one thing.");
		AddSub(StoryCharacter.DALTON, "It makes them petrified of living. Incapable of keeping the nation great.");

		// 50 - ONPREBOSSFIGHT
		CreateStory(); // 220
		AddSub(StoryCharacter.DALTON, "So what does the great leader do to get the people's minds off of the list? The leader chooses the item in the list that they fear the most.");
		AddSub(StoryCharacter.DALTON, "They put all of their efforts into showing the people how vile this item is, and how important it is to remove it from the list.");
		AddSub(StoryCharacter.DALTON, "If all the people focus on one problem from the list, then they are truly united in their fear. They share a common enemy.");
		AddSub(StoryCharacter.DALTON, "The other problems pale in comparison, and the people work as hard as they need to in order for the nation to remain great.");
	
		// 50 - ONPOSTBOSSFIGHT
		CreateStory(); // 221
		AddSub(StoryCharacter.PILOT, "Are you calling yourself a great leader?");
		AddSub(StoryCharacter.DALTON, "Consider it a parable of sorts. Something to contextualise my position.. and my decisions.");
		AddSub(StoryCharacter.PILOT, "All of my clones are programmed to find and destroy the Empress. I get that.");
		AddSub(StoryCharacter.PILOT, "But WHY did you let me keep my free thought? Did you think it would just work out fine?");
		AddSub(StoryCharacter.DALTON, "In a word... yes. But I was wrong. I admit it isn't the first time I have been wrong, @N.");
		AddSub(StoryCharacter.DALTON, "Mistakes are something every leader must come to terms with.");

		// SECTOR 51 - 
		//ONENTERSECTOR
		CreateStory(); // 222
		AddSub(StoryCharacter.PILOT, "Your system is ridiculous. Sure, I can't turn around and change route...");
		AddSub(StoryCharacter.PILOT, "...but I could just blow myself up time and time again. I'd never reach the Empress.");
		AddSub(StoryCharacter.DALTON, "You could do that. And I could simply shut down Project Gemini and start again with another soldier.");
		AddSub(StoryCharacter.DALTON, "There are always brave soldiers willing to give their lives to what the Alliance Republic stand for. Just like you did.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.DALTON, "But I don't think you want anyone else to go through what you've been through. And believe me, @N...");
		AddSub(StoryCharacter.DALTON, "I know you've suffered.");
		AddSub(StoryCharacter.DALTON, "Take some time to think about it, if you need to.");
		AddSub(StoryCharacter.PILOT, "What's there to think about? Like you said, I don't want anyone else to have to go through this.");
		AddSub(StoryCharacter.DALTON, "Very admirable of you. You will not regret your decision. I'll be in touch again soon.");

		// 51 - ONMIDSECTOR
		CreateStory(); // 223
		AddSub(StoryCharacter.PILOT, "Computer, request a comms link with Iris.");
		AddSub(StoryCharacter.COMPUTER, ">> COMMS LINK ACCEPTED");
		AddSub(StoryCharacter.PILOT, "Oh good, you're not still mad at me.");
		AddSub(StoryCharacter.IRIS, "I seem to remember it was YOU who was mad at ME.");
		AddSub(StoryCharacter.PILOT, "Sorry, Iris. I let the gravity situation get the better of my emotions.");
		AddSub(StoryCharacter.IRIS, "Hey, I get it. You're emotional. Don't worry, that's not the least bit unattractive or lame. A really strong quality for a hero to have.");
		AddSub(StoryCharacter.PILOT, "Yeah, yeah, alright - I get it, I suck. Listen, there's been a new development.");

		// 51 - ONPREPAREFORBOSS
		CreateStory(); // 224
		AddSub(StoryCharacter.PILOT, "I'm listening.");
		AddSub(StoryCharacter.PILOT, "The Alliance replied to me. President Dalton himself got in touch.");
		AddSub(StoryCharacter.PILOT, "He's taking a much more direct interest in Project Gemini and... me. For obvious reasons.");
		AddSub(StoryCharacter.PILOT, "He's put a simple barter on the table. I finish my mission, he destroys Project Gemini and all its clones.");
		AddSub(StoryCharacter.IRIS, "What the hell kind of lame-ass bartering is that?! He gets what he wants and you get to die?");
		AddSub(StoryCharacter.PILOT, "Iris, we have to face facts. I'm not a real person anymore. I only exist for the project.");
		AddSub(StoryCharacter.PILOT, "At least if I complete the mission, I won't have to keep going through this nightmare.");
		AddSub(StoryCharacter.IRIS, "...whatever. I'm not going to argue about this now. But you should know that while you were playing grab-ass with the President...");
		AddSub(StoryCharacter.IRIS, "I was a busy girl, too.");

		// 51 - ONPREBOSSFIGHT
		CreateStory(); // 225
		AddSub(StoryCharacter.IRIS, "I figured we're going to need some help getting real answers about Project Gemini and how we can get you out.");
		AddSub(StoryCharacter.PILOT, "Get me out? Sigh... Iris, c'm--");
		AddSub(StoryCharacter.IRIS, "Shut up. I want you to meet somebody.");
		AddSub(StoryCharacter.ELI, "Hey, can you guys recommend a good c'LONE for my hot date tonight? Haha!");
		AddSub(StoryCharacter.PILOT, "Uh.. what?");
		AddSub(StoryCharacter.ELI, "Yeesh. You were right, Iris. No sense of humour at all.");
		AddSub(StoryCharacter.IRIS, "That was a bad one to start with, Eli.");
		AddSub(StoryCharacter.ELI, "Aw... I thought it was pretty good...");

		// 51 - ONPOSTBOSSFIGHT
		CreateStory(); // 226
		AddSub(StoryCharacter.IRIS, "@N, meet Eli. Or should I say... 01XEN10.");
		AddSub(StoryCharacter.PILOT, "01XEN10? You're that hacker that tore apart the government website?");
		AddSub(StoryCharacter.ELI, "Hah! Yeah, man, that was hilarious.");
		AddSub(StoryCharacter.IRIS, "Eli here is going to help us.");
		AddSub(StoryCharacter.ELI, "Your plight sounds totally £&%@ing bad ass, @N. Clones?! Government conspiracy?! I love it. Also, Iris always pays well.");
		AddSub(StoryCharacter.IRIS, "I'm something of a crime connoisseur.");

		// 52 - 
		// ONENTERSECTOR
		CreateStory(); // 227
		AddSub(StoryCharacter.ELI, "I've been looking into Project Gemini. A LOT of closed doors, but none I can't open with a little bit of time.");
		AddSub(StoryCharacter.ELI, "Iris said you wanna find out about some 'mission protocol' code, right? The thing that forces you to go hunting for the Empress.");
		AddSub(StoryCharacter.PILOT, "...yeah. What, you think you can find it?");
		AddSub(StoryCharacter.ELI, "Find it? Mutha trucka, I can make that thing sing and dance if you want.");
		AddSub(StoryCharacter.IRIS, "@N, with Eli on our side we can swipe control of Project Gemini right out of the President's dirty mitts.");
		AddSub(StoryCharacter.PILOT, "You thought of this all on your own?");
		AddSub(StoryCharacter.IRIS, "Ya mean all on my own, with my feeble widdle piwate girly bwain?");
		AddSub(StoryCharacter.PILOT, "Sorry.. I didn't mean it like that.");
		AddSub(StoryCharacter.PILOT, "Thanks, Iris. Thank you, Eli.");
		AddSub(StoryCharacter.IRIS, "See what I mean, Eli? Total emo.");
		AddSub(StoryCharacter.ELI, "Reminds me of my ex girlfriend.");
		AddSub(StoryCharacter.PILOT, "Come on, hackers don't have girlfriends. Only jpegs.");
		AddSub(StoryCharacter.ELI, "Hey!");
		AddSub(StoryCharacter.IRIS, "Hahah! Oooowned!");

		// 52 - ONMIDSECTOR
		CreateStory(); // 228
		AddSub(StoryCharacter.PILOT, "Alright, Eli. How long do you need to work your magic on Project Gemini?");
		AddSub(StoryCharacter.ELI, "The mission protocol code is going to be buried deep. It's something they won't have ever wanted to be toyed with. So...");
		AddSub(StoryCharacter.ELI, "...it'll take a bit of time to find it.");
		AddSub(StoryCharacter.PILOT, "Alright. In the mean time, I have another idea.");

		// 52 - ONPREBOSSFIGHT
		CreateStory(); // 229
		AddSub(StoryCharacter.PILOT, "I don't want the President to get away with this. Him and his team of crackpots.");
		AddSub(StoryCharacter.PILOT, "I refuse to believe there's no good people left in the Alliance.");
		AddSub(StoryCharacter.IRIS, "I can believe it. If you ask me, I say just do everyone there a favour and blow the place sky-high.");
		AddSub(StoryCharacter.PILOT, "Iris, you don't have a single place to call home. Patriotism isn't just a military thing.");
		AddSub(StoryCharacter.PILOT, "Too fond to just turn my back on it because of what the President has done.");

		// 52 - ONPOSTBOSSFIGHT
		CreateStory(); // 230
		AddSub(StoryCharacter.PILOT, "There are still many innocent people just trying to live out their lives as best they can on the AR colonies.");
		AddSub(StoryCharacter.PILOT, "I owe it to them to make that easier, by crushing Dalton's regime. Hell, I owe it to Ren...");
		AddSub(StoryCharacter.IRIS, "...Ren. Poor little dork...");
		AddSub(StoryCharacter.IRIS, "Ok, what's your idea?");
		AddSub(StoryCharacter.PILOT, "I want you to spend some time scouring media reports and stalking political forums.");
		AddSub(StoryCharacter.PILOT, "See if you can find someone who seems legit, and not in Dalton's pocket.");

		// SECTOR 53 - 
		// ONENTERSECTOR
		CreateStory(); // 231
		AddSub(StoryCharacter.IRIS, "Hey @N... uhh.. are you sure you want ME to judge whether someone's politically 'legit'?");
		AddSub(StoryCharacter.PILOT, "I don't have web access, Iris. I NEED you to do this for me. Eli's got his hands full with getting into Project Gemini. You'll be fine.");
		AddSub(StoryCharacter.IRIS, "Aaaaalright, okaaaay. But you should know, anything that requires using my brain is charged at double rate.");

		// ONMIDSECTOR
		CreateStory(); // 232
		AddSub(StoryCharacter.ELI, "Oh man, this is so friggin' cool. It's unreal.");
		AddSub(StoryCharacter.PILOT, "Well don't keep it to yourself, Eli, let's hear it.");
		AddSub(StoryCharacter.ELI, "Whenever I use my 'Sacred Crow' ability now, my guy gets plus TWENTY on all stats for fifteen seconds!");
		AddSub(StoryCharacter.PILOT, "Right.");
		AddSub(StoryCharacter.PILOT, "Wait.");
		AddSub(StoryCharacter.PILOT, "What?");
		AddSub(StoryCharacter.ELI, "Yeah! And there's only a three second cool-down. Man this friggin' rocks.");
		AddSub(StoryCharacter.PILOT, "What the hell kind of hacker speak is this?");
		AddSub(StoryCharacter.ELI, "Hacker speak? Nah, I'm talking about Legendcraft: Heroes of War. It's so addictive, man. ");
		AddSub(StoryCharacter.PILOT, "You're playing a GAME?!");
		AddSub(StoryCharacter.ELI, "...only for a minute, jeez. Guy's gotta have some downtime. Don't worry, Clone Ranger, I've got something else for you.");
		AddSub(StoryCharacter.PILOT, "Spit it out.");
		AddSub(StoryCharacter.ELI, "Well, it turns out there's more than one cloning facility than the one Iris and Ren tried to infiltrate.");
		AddSub(StoryCharacter.ELI, "I've found the locations of another five in our colonial system.");
		AddSub(StoryCharacter.PILOT, "Son of a gun. I guess it makes sense, what with the volume of clones they want to produce. Good find, Eli.");

		// ONPREBOSSFIGHT
		CreateStory(); // 233
		AddSub(StoryCharacter.IRIS, "Get ready to do some serious back-patting because momma just struck gold.");
		AddSub(StoryCharacter.PILOT, "What'd you find?");
		AddSub(StoryCharacter.IRIS, "You were right about there being some good folks out there in political land. There's one young lady in particular who's caught my eye.");
		AddSub(StoryCharacter.PILOT, "Who is she?");
		AddSub(StoryCharacter.IRIS, "Her name's Adila Ogonbuwe. A very young but very impressive District Attorney for the Parnilla Colony. She's feisty and HATES Dalton. Great speaker. She's got a lot of support.");

		// ONPOSTBOSSFIGHT
		CreateStory(); // 234
		AddSub(StoryCharacter.IRIS, "But @N, there's more.");
		AddSub(StoryCharacter.IRIS, "You're not going to believe who her aunt is. Or was, I should say.");
		AddSub(StoryCharacter.PILOT, "Who?");
		AddSub(StoryCharacter.IRIS, "Your very own Doctor Grace.");
		AddSub(StoryCharacter.PILOT, "Doctor Grace?! We HAVE to get in touch, then. Adila needs to know the truth.");

		// SECTOR 54 - 
		// ONENTERSECTOR
		CreateStory(); // 235
		AddSub(StoryCharacter.PILOT, "Eli. You there?");
		AddSub(StoryCharacter.ELI, "I'm here. Just enjoying a nice, cold, ice cream clone. Haha! Get it? Ice cream clone?");
		AddSub(StoryCharacter.PILOT, "I need you to find contact details for 'Adila Ogonbuwe', a politician in Parnilla.");
		AddSub(StoryCharacter.ELI, "...alright, what do you want me to tell this Adila chick? And she better be hot!");
		AddSub(StoryCharacter.PILOT, "Just tell her th-");
		AddSub(StoryCharacter.ELI, "Just got a pic up. She's hot. Smoking, in fact.");
		AddSub(StoryCharacter.PILOT, "Tell her tha-");
		AddSub(StoryCharacter.ELI, "Daaa-aaamn.");
		AddSub(StoryCharacter.PILOT, "ELI!");
		AddSub(StoryCharacter.ELI, "Sorry.");
		AddSub(StoryCharacter.PILOT, "Tell her that her Aunt's death was not natural, and that Dalton is involved.");
		AddSub(StoryCharacter.PILOT, "Give her our private comms link to patch in to, but don't tell her who we are yet.");
		AddSub(StoryCharacter.ELI, "Alright, I'm on it.");

		// 54 - ONMIDSECTOR
		CreateStory(); // 236
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING AUDIO MESSAGE FROM 'MOTHER'");
		AddSub(StoryCharacter.MOTHER, "Things have been SO boring here these days! Ohh, I do really miss you honey...");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.MOTHER, "That Mrs Anderson from across the street was bragging about her daughter the other day. Calling her an ace fighter pilot.");
		AddSub(StoryCharacter.MOTHER, "Goodness, it was EVERYTHING in me not to laugh! She obviously forgot who my little spaghetti goblin is!");
		AddSub(StoryCharacter.MOTHER, "Oh shoot, my pan's boiling over! Talk to you later sweetie!");
		AddSub(StoryCharacter.COMPUTER, ">> END AUDIO MESSAGE FROM 'MOTHER'");

		// 54 - ONPREPAREFORBOSS
		CreateStory(); // 237
		AddSub(StoryCharacter.ADILA, "Hello? Is anyone there?");
		AddSub(StoryCharacter.PILOT, "Hello, Ms Ogonbuwe.");
		AddSub(StoryCharacter.ADILA, "I don't know if this is some sort of sick joke, but you better explain yourself, whoever you are.");
		AddSub(StoryCharacter.PILOT, "My name is Gemini Pilot @N.");
		AddSub(StoryCharacter.ADILA, "Gemini Pilot @N?!");
		AddSub(StoryCharacter.ADILA, "...");
		AddSub(StoryCharacter.ADILA, "...so it is true.");

		// 54 - ONPOSTBOSSFIGHT
		CreateStory(); // 238
		AddSub(StoryCharacter.PILOT, "You know about Project Gemini's dirty secret?");
		AddSub(StoryCharacter.ADILA, "Yes. My aunt and I were very close. I knew she was your remote doctor. For a while, she kept me in the dark about the cloning... for my own good, probably.");
		AddSub(StoryCharacter.ADILA, "Then one day she tells me about Project Gemini. I was shocked. I tried to get her to pass more information about you on to me, but, again... she was hesitant.");
		AddSub(StoryCharacter.ADILA, "She must have known what sort of danger it would put me in.");
		AddSub(StoryCharacter.PILOT, "When I signed up for Project Gemini, I was told my original self would be sent on a long-term vacation to some tropical island. But it wasn't the case.");
		AddSub(StoryCharacter.PILOT, "They kept me locked up in a cloning facility, to be cloned continuously. I'm certain they had her killed because she discovered this.");
		AddSub(StoryCharacter.ADILA, "Auntie Grace... I'm so sorry...");

		// SECTOR 55 - THE JUSTICE QUARTET
		CreateStory(); // 239
		AddSub(StoryCharacter.PILOT, "I want Dalton to pay for this, Adila. I still have a couple of friends left, after everything I've been through.");
		AddSub(StoryCharacter.PILOT, "We're working on a plan to seize control of Project Gemini and pull the rug from under Dalton's boots.");
		AddSub(StoryCharacter.PILOT, "I wanted to find someone I can trust. Someone who's prepared to fight against the current government's corruption.");
		AddSub(StoryCharacter.ADILA, "No one wants Dalton out of office more than I do. But you have to understand something. You're not messing with just anyone here.");
		AddSub(StoryCharacter.ADILA, "Dalton has been President for SIX terms. That's longer than any of our Presidents. He is a powerful man.");

		// ONMIDSECTOR
		CreateStory(); // 240
		AddSub(StoryCharacter.PILOT, "There's a time for even the most powerful to fall.");
		AddSub(StoryCharacter.PILOT, "Dalton and his pawns have destroyed my life. They've lied to the people for too long.");
		AddSub(StoryCharacter.PILOT, "They should have been up-front about Project Gemini from the start.");
		AddSub(StoryCharacter.ADILA, "The war against the Royal Empire must be re-addressed if we really want it to end.");
		AddSub(StoryCharacter.ADILA, "With Dalton gone, someone will need to pick up the pieces. That's not a very attractive prospect for any politician - even me.");
		AddSub(StoryCharacter.ADILA, "The people will always blame the new for the faults of the old.");

		// ONPREPAREFORBOSS
		CreateStory(); // 241
		AddSub(StoryCharacter.PILOT, "We've seen you speak. People listen to you. We can gather the evidence you need to bring Dalton down...");
		AddSub(StoryCharacter.PILOT, "...and make your own move towards running for Presidency. You can't say you haven't thought about it.");
		AddSub(StoryCharacter.ADILA, "...");

		// ONPOSTBOSSFIGHT
		CreateStory(); // 242
		AddSub(StoryCharacter.DALTON, "Come on, Adila. Help us. For your aunt. For what the Alliance Republic is REALLY about.");
		AddSub(StoryCharacter.ADILA, "...alright. I'll help any way I can.");
		AddSub(StoryCharacter.ELI, "Yes!");
		AddSub(StoryCharacter.ADILA, "Um, who are you?");
		AddSub(StoryCharacter.ELI, "Oh! Uhh.. I'm the guy who stalked you.");
		AddSub(StoryCharacter.ADILA, "What?");
		AddSub(StoryCharacter.ELI, "Errr...");
		AddSub(StoryCharacter.IRIS, "He's the annoying but terribly useful hacker who found your contact details for us. I'm Iris. Pirate liberator heroine extraordinaire.");
		AddSub(StoryCharacter.ADILA, "I'm Adila...");
		AddSub(StoryCharacter.IRIS, "Quite the dream time you've assembled here, @N. Maybe you should give us a name.");
		AddSub(StoryCharacter.PILOT, "What about... 'The Justice Quartet'?");
		AddSub(StoryCharacter.IRIS, "Lame.");
		AddSub(StoryCharacter.ELI, "Seriously lame.");
		AddSub(StoryCharacter.ADILA, "Room for improvement.");
		AddSub(StoryCharacter.PILOT, "...");

		// CHAPTER 56 - SCRAPBOOK
		// ONENTERSECTOR
		CreateStory(); // 243
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING AUDIO MESSAGE FROM 'MOTHER'");
		AddSub(StoryCharacter.MOTHER, "Heeellloooo!");
		AddSub(StoryCharacter.MOTHER, "Ok, I know you're not going to like this... but I don't care! Hehe!");
		AddSub(StoryCharacter.MOTHER, "I'm just too excited about your tour coming to an end!");
		AddSub(StoryCharacter.MOTHER, "I'm making a SCRAPBOOK! All about you, sweetie!!");

		// ONMIDSECTOR
		CreateStory(); // 244
		AddSub(StoryCharacter.MOTHER, "So I was looking through some of your things.. to get inspiration.. and guess what I found?");
		AddSub(StoryCharacter.MOTHER, "Your high school yearbook!");
		AddSub(StoryCharacter.MOTHER, "Let's see here...!");

		// ONPREBOSSFIGHT
		CreateStory(); // 245
		AddSub(StoryCharacter.MOTHER, "Would you LOOK at that hair! I remember saying at the time you needed a haircut!");
		AddSub(StoryCharacter.MOTHER, "Still. Too good looking for it to matter!");

		// ONPOSTBOSSFIGHT
		CreateStory(); // 246
		AddSub(StoryCharacter.MOTHER, "Now then... let's get straight to the good bit!");
		AddSub(StoryCharacter.MOTHER, "Where is it? Hmmm... aha!");

		// CHAPTER 57 - BAD MEMORIES
		// ONENTERSECTOR
		CreateStory(); // 247
		AddSub(StoryCharacter.MOTHER, "Alumni award nominations and wins for @N.");
		AddSub(StoryCharacter.MOTHER, "Most likely to succeed...");
		AddSub(StoryCharacter.MOTHER, "Most patriotic...");
		
		// ONMIDSECTOR
		CreateStory(); // 248
		AddSub(StoryCharacter.MOTHER, "Accreditation in political and military affairs...");
		AddSub(StoryCharacter.MOTHER, "Best student pilot, six years in a ROW!");
		
		// ONPOSTBOSSFIGHT
		CreateStory(); // 249
		AddSub(StoryCharacter.MOTHER, "My Goodness! The list keeps going! But I'm sure you're VERY embarassed now, so I'll stop reading it.");
		AddSub(StoryCharacter.MOTHER, "I'll just get started on the crapbook!");
		AddSub(StoryCharacter.MOTHER, "I love you honey. Can't wait to see my little one come back home. It'll be soon, right?");
		AddSub(StoryCharacter.MOTHER, "Yes... of course it'll be soon!");

		// CHAPTER 58 - NEW INTEL
		// ONENTERSECTOR
		CreateStory(); // 250
		AddSub(StoryCharacter.DALTON, "@N. Do you copy? I have some bad news. Intel regarding the Royal Empire.");
		AddSub(StoryCharacter.PILOT, "What is it?");
		AddSub(StoryCharacter.DALTON, "They've been building a new fleet. It's being led by an Upper Hierarchy member known as Champion Nitz Sanguinaire.");
		AddSub(StoryCharacter.DALTON, "He has one mission. To destroy Project Gemini.");
		AddSub(StoryCharacter.PILOT, "Doesn't sound so bad to me. Maybe I should let him do it.");
		AddSub(StoryCharacter.PILOT, "He'll be attacking the multiple cloning facilities you have stashed in our galaxy, I take it?");
		AddSub(StoryCharacter.DALTON, "Mmm.. so you know about the other facilities...");
		AddSub(StoryCharacter.PILOT, "Yep.");
		AddSub(StoryCharacter.DALTON, "Then you know that these facilities are on colonies habited by innocent people. If the Champion attacks the facilities...");
		AddSub(StoryCharacter.DALTON, "...lives will be lost, @N. Not just Project Gemini.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.DALTON, "And with Project Gemini gone, what then?");
		AddSub(StoryCharacter.DALTON, "You think I can just start another one in a matter of days, and fight off the Royal Empire? They'll finish us off in no time.");

		// ONMIDSECTOR
		CreateStory(); // 251
		AddSub(StoryCharacter.DALTON, "We need you to defend the facilities, @N. Only you can stand up against such a powerful fleet.");
		AddSub(StoryCharacter.PILOT, "How am I supposed to do that? I can't turn around.");
		AddSub(StoryCharacter.DALTON, "We will make a temporary change to your mission protocol code.");
		AddSub(StoryCharacter.DALTON, "Once you have successfully defended the colonies from the Champion, we will change it back.");
		AddSub(StoryCharacter.PILOT, "Alright. Sounds like a plan, Mr President.");
		AddSub(StoryCharacter.DALTON, "I'm glad you are finally seeing things from my perspective, @N. I think we can learn to work well together.");
		AddSub(StoryCharacter.PILOT, "**** you.");
		AddSub(StoryCharacter.DALTON, "...Dalton out.");

		// ONPREPAREFORBOSS
		CreateStory(); // 252
		AddSub(StoryCharacter.PILOT, "Hear those bells, Eli? Christmas is coming.");
		AddSub(StoryCharacter.ELI, "Oooo, what'd you get me??");
		AddSub(StoryCharacter.PILOT, "The President just got in touch. An Empire fleet is planning an attack on those facilities you found.");
		AddSub(StoryCharacter.PILOT, "They're going to change the mission protocol code so that I can defend them.");
		AddSub(StoryCharacter.ELI, "Hah! Bringing the code right out into the open for me to find and work my magic on.");
		AddSub(StoryCharacter.PILOT, "Bingo.");
		AddSub(StoryCharacter.ELI, "This so so God damn ninja it hurts.");

		// CHAPTER 59 - INFAMOUS

		// ONENTERSECTOR
		CreateStory(); // 253
		AddSub(StoryCharacter.PILOT, "Ugh.. will you two give it a rest?");
		AddSub(StoryCharacter.ADILA, "He started it.");
		AddSub(StoryCharacter.ELI, "Yeah, so I should end it.");
		AddSub(StoryCharacter.ADILA, "No, because I'VE ended it.");
		AddSub(StoryCharacter.IRIS, "SHUT UP, YOU DUMB LITTLE KIDS!");
		AddSub(StoryCharacter.ADILA, "...sorry Iris.");
		AddSub(StoryCharacter.ELI, "...sorry Iris.");
		AddSub(StoryCharacter.PILOT, "Thanks, Iris. Look, we need to stop letting the amount of work on our shoulders get the better of us, ok?");
		AddSub(StoryCharacter.PILOT, "I know you two are stressed, but this bickering can't go on.");
		AddSub(StoryCharacter.ADILA, "...");
		AddSub(StoryCharacter.ELI, "...");
		AddSub(StoryCharacter.PILOT, "Iris and I are nearly at the first cloning facility, which means soon we're going to be wrapped up in a crap-load of blowing stuff up.");
		AddSub(StoryCharacter.IRIS, "Yeah, and unlike @N, I can actually die.");
		AddSub(StoryCharacter.IRIS, "So I don't need you two doing awkward school ground bully-flirting with each other over the comms, distracting me, got it?");
		AddSub(StoryCharacter.ADILA, "Flirting?! Hmph.");
		AddSub(StoryCharacter.ELI, "Yeah I...pfff, what? Hah! ...I mean, no way..");
		AddSub(StoryCharacter.PILOT, "Eli, where are we at with the mission protocol?");
		AddSub(StoryCharacter.ELI, "Just like we thought, the President's tech monkeys have opened that sucker up and shot fireworks into the sky. I know exactly where it is and can access it whenever you're ready.");
		AddSub(StoryCharacter.PILOT, "Great. And they can't change it back afterwards, right?");
		AddSub(StoryCharacter.ELI, "Hey, I'm not some government-trained IT consultant. I'm 01XEN10, got it?");
		AddSub(StoryCharacter.ELI, "When I get in there, control over Project Gemini will be yours and yours alone.");

		// ONMIDSECTOR
		CreateStory(); // 254
		AddSub(StoryCharacter.PILOT, "How's your evidence collecting going, Adila?");
		AddSub(StoryCharacter.ADILA, "Pretty well. Your testimony is obviously the most weight we have, but...");
		AddSub(StoryCharacter.ADILA, "I've managed to track down a few mid-level officials who had beef with Project Gemini but no guts to say anything about it before.");
		AddSub(StoryCharacter.PILOT, "Can we trust them not to give anything away to Dalton?");
		AddSub(StoryCharacter.ADILA, "I only associate with people I trust, @N. Don't worry.");
		AddSub(StoryCharacter.PILOT, "Good. Keep it up, guys. A bit more pressing and--");
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING FOREIGN COMMS TRANSMISSION");
		AddSub(StoryCharacter.PILOT, "What's this?");

		// ONPREBOSSFIGHT
		CreateStory(); // 255
		AddSub(StoryCharacter.CHAMPION, "I could not wait for you to contact me, Captain @N. The anticipation was too great.");
		AddSub(StoryCharacter.PILOT, "You must be the 'Champion'.");
		AddSub(StoryCharacter.CHAMPION, "The one and only. And allow me to say it is a great, great honour to make your acquaintance.");
		AddSub(StoryCharacter.PILOT, "I'm always pretty amazed by the overblown faux-politeness you Royal Empire goons lay on me when you know death is near.");
		AddSub(StoryCharacter.CHAMPION, "HAH! That sort of retort is EXACTLY what I have come to expect - and indeed, longed to hear - from the infamous Captain @N.");

		// ONPOSTBOSSFIGHT
		CreateStory(); // 256
		AddSub(StoryCharacter.PILOT, "It's Gemini Pilot @N, actually. The real Captain @N is six feet under. I'm just the clone of a legend.");
		AddSub(StoryCharacter.CHAMPION, "Ah, but I believe Captain @N still lives on! Cloning is not a process of dilution, but replication.");
		AddSub(StoryCharacter.CHAMPION, "Indeed, if we believed the clones were not 'the real deal', my current mission would be rather pointless.");
		AddSub(StoryCharacter.PILOT, "It's still pointless, Champ. You can bring as big a fleet as you want. I go down, I come back up.");
		AddSub(StoryCharacter.CHAMPION, "Do me the honour of letting me find out for myself, dear Captain @N.");

		// SECTOR 60 - PETRA
		// ONENTERSECTOR
		CreateStory(); // 257
		AddSub(StoryCharacter.COMPUTER, ">> ENTERING PETRA COLONY SPACE");
		AddSub(StoryCharacter.PILOT, "Alright. We're here.");
		AddSub(StoryCharacter.IRIS, "Oh. Ok. That's a lot of ships.");
		AddSub(StoryCharacter.PILOT, "Damnit, we weren't fast enough! They're already swarming around Petra.");
		AddSub(StoryCharacter.IRIS, "Better late than never. Let's give it our damnedest!");

		// ONMIDSECTOR
		CreateStory(); // 258
		AddSub(StoryCharacter.PILOT, "This is a mess. Eli, can you run a report on Petra? Lives lost?");
		AddSub(StoryCharacter.ELI, "I've been keeping an eye on distress reports. There's been plenty of distress - some casualty reports but no mention of fatalities.");
		AddSub(StoryCharacter.PILOT, "Maybe we're not too late after all.");

		// ONPREBOSSFIGHT
		CreateStory(); // 259
		AddSub(StoryCharacter.ELI, "Ah, crap. Bad news.");
		AddSub(StoryCharacter.PILOT, "What is it?");
		AddSub(StoryCharacter.ELI, "Just had confirmation that the cloning facility is up in smoke. The place has been obliterated. Three fatalities. Facility staff who didn't get out in time.");
		AddSub(StoryCharacter.PILOT, "Damnit!");

		// ONPOSTBOSSFIGHT
		CreateStory(); // 260
		AddSub(StoryCharacter.DALTON, "@N, come in!");
		AddSub(StoryCharacter.PILOT, "What now, Dalton?!");
		AddSub(StoryCharacter.DALTON, "You're supposed to be protecting my facilities, damnit! I've been told one has gone down!");
		AddSub(StoryCharacter.PILOT, "We're moving as fast as we can, Dalton. I suggest you up your colony defense forces and stop relying solely on me.");
		AddSub(StoryCharacter.DALTON, "Get to the next colony, NOW!");
		AddSub(StoryCharacter.IRIS, "Ooohh, I REALLY don't like that guy.");
		AddSub(StoryCharacter.PILOT, "We've done what we can here. No apparent enemy targets remaining. Moving on to the next colony, hot on their trail.");

		// SECTOR 61 - LIKE THE PHOENIX
		// ONENTERSECTOR
		CreateStory(); // 261
		AddSub(StoryCharacter.CHAMPION, "Hurry, hurry Captain @N! My fleet is nearing the next facility.");
		AddSub(StoryCharacter.PILOT, "Oh, don't worry - I see the back of the line now. I'll be cutting my way through shortly.");
		AddSub(StoryCharacter.CHAMPION, "Ah, a wonderful image. The warrior silencing foes with one fell swoop of blade.");
		AddSub(StoryCharacter.PILOT, "Instead of perving over images of me killing stuff, why don't you stop and fight me one-on-one?");
		AddSub(StoryCharacter.CHAMPION, "Tempting, but no. That will come later.");

		// ONMIDSECTOR
		CreateStory(); // 262
		AddSub(StoryCharacter.IRIS, "This feels weird.");
		AddSub(StoryCharacter.PILOT, "What do you mean?");
		AddSub(StoryCharacter.IRIS, "Why isn't he attacking all of the facilities at once? Why approach one, leave part of your fleet and then move on to the next?");
		AddSub(StoryCharacter.PILOT, "He's taunting me. Wants me to catch him up. Wants a fight.");
		AddSub(StoryCharacter.IRIS, "...");

		// ONPREBOSSFIGHT
		CreateStory(); // 263
		AddSub(StoryCharacter.DALTON, "Bristaine is an industrial colony, @N. Defence there is minimal - there's never been a need to heavily protect it.");
		AddSub(StoryCharacter.PILOT, "So you think... 'Hey, I know! Let's put a cloning facility which our enemy ****ing HATES and will probably want to destroy on this badly armed colony!'");
		AddSub(StoryCharacter.DALTON, "Funds were low, we--");
		AddSub(StoryCharacter.IRIS, "Why don't you just cram it, suit?");
		AddSub(StoryCharacter.DALTON, "Who was that?");
		AddSub(StoryCharacter.PILOT, "My secretary.");
		AddSub(StoryCharacter.DALTON, "You have a secretary...?");

		// ONPOSTBOSSFIGHT
		CreateStory(); // 264
		AddSub(StoryCharacter.CHAMPION, "You know, when my comrades were destroyed by you, I was dying to be sent to take their place.");
		AddSub(StoryCharacter.CHAMPION, "I watched from their ship's bridge cameras as you skilfully brought down their fleets. As you burst into flames, and like the Phoenix, rose again.");
		AddSub(StoryCharacter.PILOT, "Well, at least you know what's coming.");
		AddSub(StoryCharacter.CHAMPION, "Oh yes. I do indeed. Now, I have the pleasure of a front-row seat.");

		// CHAPTER 62 - BRISTAINE
		// ONENTERSECTOR
		CreateStory(); // 265
		AddSub(StoryCharacter.PILOT, "Entering Bristaine Colony airspace. Let's squash these flies, Iris.");
		AddSub(StoryCharacter.DALTON, "Move quickly, @N. They've already done a lot of damage to the facility and colony. 226 confirmed dead.");
		AddSub(StoryCharacter.ELI, "226...?");
		AddSub(StoryCharacter.PILOT, "Moving in!");

		// ONMIDSECTOR
		CreateStory(); // 266
		AddSub(StoryCharacter.ELI, "Yo, @N. How you holding up out there?");
		AddSub(StoryCharacter.PILOT, "We're doing what we can. Place looks a mess, though. Any word on the state of the facility?");
		AddSub(StoryCharacter.ELI, "13% maintenance levels remaining, according to their last distress signal. In other words, the place is screwed.");
		AddSub(StoryCharacter.PILOT, "God damnit!");
		AddSub(StoryCharacter.ELI, "But hey... Dalton said 226 confirmed dead...");
		AddSub(StoryCharacter.ELI, "Not to belittle people dying or anything, but... there's actually only been 6 confirmed deaths.");
		AddSub(StoryCharacter.PILOT, "Six?! That's a pretty huge difference.");
		AddSub(StoryCharacter.IRIS, "I smell a big, fat, ugly, stupid, rich, powerful, dirty-ass rat.");

		// ONPREBOSSFIGHT
		CreateStory(); // 267
		AddSub(StoryCharacter.DALTON, "Don't let him take any more innocent lives, @N. Stop this madman!");
		AddSub(StoryCharacter.PILOT, "By the way, Dalton... who crunches your numbers for you? A fingerless ape?");
		AddSub(StoryCharacter.DALTON, "What are you talking about?");
		AddSub(StoryCharacter.IRIS, "You know what @N's talking about, ya soft-skulled bag of turd. Your numbers are all wrong!");
		AddSub(StoryCharacter.DALTON, "Now's not the time for nitpicking over numbers. You two have a job to do.");
		AddSub(StoryCharacter.IRIS, "You TWO?! I'M VOLUNTEERING TO DO THIS, ARSE FACE! This ain't my JOB!");

		// ONPOSTBOSSFIGHT
		CreateStory(); // 268
		AddSub(StoryCharacter.CHAMPION, "I have destroyed two of your facilities, @N. What damage has this done to the manufacturing of clones, I wonder?");
		AddSub(StoryCharacter.PILOT, "You're not going to live enough to find out.");

		// SECTOR 63 - PRIVATE SECURITY
		// ONENTERSECTOR
		CreateStory(); // 269
		AddSub(StoryCharacter.ELI, "Hey... I don't get it. @N wants out of Project Gemini. If the facilities all get blown up, @N finally gets to rest in peace.");
		AddSub(StoryCharacter.ELI, "...So why are we protecting the facilities?");
		AddSub(StoryCharacter.ADILA, "Project Gemini can't be terminated. Not yet.");
		AddSub(StoryCharacter.ELI, "What?! So you want to keep that dirty project running, just like Dalton, huh?");
		AddSub(StoryCharacter.ADILA, "Like it or not, Project Gemini is our ONLY effective defence against the Royal Empire.");
		AddSub(StoryCharacter.ADILA, "If the project is destroyed, the Alliance will come swiftly after.");
		AddSub(StoryCharacter.ELI, "...");

		// 63 ONMIDSECTOR
		CreateStory(); // 270
		AddSub(StoryCharacter.DALTON, "What happens when we gain control of Project Gemini, huh? You going to show your true political colours, Adila?");
		AddSub(StoryCharacter.DALTON, "I'll bet Dalton started off as a nice guy. All peace-lovin' and world-changin'.");
		AddSub(StoryCharacter.DALTON, "Maybe it's the project that turns people evil, y'know?");
		AddSub(StoryCharacter.ADILA, "Stop being such a $@%*ing hippie, Eli. The project doesn't control people. They control the project. Besides, @N will have control.");
		AddSub(StoryCharacter.ELI, "So the people should have power, huh? Finally, something we agree on! I've been trying to restore power to the people since I got IN to hacking.");
		AddSub(StoryCharacter.CHAMPION, "By writing 'Dalton loves big wieners' on the government website? You think you're some kinda government-toppling anarchist?");
		AddSub(StoryCharacter.FAKE, "It WAS pretty funny.");
		AddSub(StoryCharacter.FAKE, "Schoolboy graffiti and nothing more.");
		AddSub(StoryCharacter.ELI, "Got more press coverage than any of your debates, though.");
		AddSub(StoryCharacter.ADILA, "**** YOU!");

		// 63 ONPREBOSSFIGHT
		CreateStory(); // 271
		AddSub(StoryCharacter.ADILA, "...sorry, Eli. Sorry everyone. I hate it when I lose my temper. It's a sign of weakness.");
		AddSub(StoryCharacter.IRIS, "Don't sweat it. I lose my rag all the time. It's like therapy, to me.");
		AddSub(StoryCharacter.ADILA, "But you're not hoping to be a leader one day, Iris. I must show more restraint.");
		AddSub(StoryCharacter.ELI, "I was teasing you. Trying to get a rise out of ya... and ...I shouldn't have. I'm sorry, too.");
		AddSub(StoryCharacter.PILOT, "Now that we're all friends again, can we please concentrate on the mission? What's our ETA on the next colony?");
		AddSub(StoryCharacter.IRIS, "Eledan 5 is just a few clicks away. We should see it once we part this curtain of Royal Empire goofballs.");

		// 63 ONPOSTBOSSFIGHT
		CreateStory(); // 272
		AddSub(StoryCharacter.ADILA, "@N, I have arranged for a private security force to help keep the colonies you've cleared safe from any hiding Royal Empire ships.");
		AddSub(StoryCharacter.ELI, "Private security force? You mean you hired mercenaries, out of your own pocket?");
		AddSub(StoryCharacter.ADILA, "Our government hasn't provided enough defence for these colonies. Someone has to step in and protect the people.");
		AddSub(StoryCharacter.ELI, "Wow. That's super nice.");
		AddSub(StoryCharacter.ADILA, "...thanks.");

		// SECTOR 64 - ELEDAN 5
		// 64 ONENTERSECTOR
		CreateStory(); // 273
		AddSub(StoryCharacter.IRIS, "How about I fly up ahead and shoot that Champion douche-bag down now, end this whole thing?");
		AddSub(StoryCharacter.PILOT, "Don't even think about it, Iris. This isn't a joke. He's strong enough to take me down a hundred times.");
		AddSub(StoryCharacter.IRIS, "Pew pew pew! BOOM! Sorry, can't hear you over all the ass I'm kicking!");
		AddSub(StoryCharacter.PILOT, "Grrr...");

		// 64 ONMIDSECTOR
		CreateStory(); // 274
		AddSub(StoryCharacter.CHAMPION, "You certainly do not disappoint. In a way, I feel saddened you and I are not on the same side.");
		AddSub(StoryCharacter.IRIS, "If you like @N so much, why don't you just get married already?");
		AddSub(StoryCharacter.CHAMPION, "I am betrothed to the Royal Empire.");
		AddSub(StoryCharacter.PILOT, "There is an alternative to this, Champion. This war has changed me. Maybe we can start peace tal--");
		AddSub(StoryCharacter.CHAMPION, "You know as well as I that peace is impossible. The cogs in the machine are far too oiled now.");
		AddSub(StoryCharacter.CHAMPION, "They glide effortlessly along.");
		AddSub(StoryCharacter.CHAMPION, "They cannot be stopped.");

		// 64 ONPREPAREFORBOSS
		CreateStory(); // 275
		AddSub(StoryCharacter.CHAMPION, "What is this?! They have defence squads at the colonies already?!");
		AddSub(StoryCharacter.ROYALENGINEER, "They must have privately hired them, sir. Our intel showed no governmental defence.");
		AddSub(StoryCharacter.CHAMPION, "...No matter. Let them waste more resources as well as their time. Keep the fleet actively engaged.");
		AddSub(StoryCharacter.ROYALENGINEER, "Yes sir!");
		AddSub(StoryCharacter.CHAMPION, "Soon you will acknowledge my military genius, Captain @N. You will see that we are equals.");

		// 64 ONPREBOSSFIGHT
		CreateStory(); // 276
		AddSub(StoryCharacter.PILOT, "Why is it too late for peace, Champion? Surely your Empress must be willing to talk with the Alliance.");
		AddSub(StoryCharacter.CHAMPION, "Do you know WHEN the Great War began, Captain @N?");
		AddSub(StoryCharacter.PILOT, "The assassination of President Aldridge in X932.");
		AddSub(StoryCharacter.CHAMPION, "Ha ha ha ha!");
		AddSub(StoryCharacter.IRIS, "Guy's got a weird sense of humour.");

		// 64 ONPOSTBOSSFIGHT
		CreateStory(); // 277
		AddSub(StoryCharacter.DALTON, "Well done, @N. You've managed to secure the facility without much damage caused.");
		AddSub(StoryCharacter.PILOT, "Any idea how many lives were lost this time, Mr President?");
		AddSub(StoryCharacter.DALTON, "Mmm? Oh, um... ");
		AddSub(StoryCharacter.DALTON, "I haven't been given the information.");
		AddSub(StoryCharacter.PILOT, "Weird. Your facility doesn't get destroyed this time and suddenly there's no mention of lives lost.");
		AddSub(StoryCharacter.IRIS, "That IS weird. It's almost like he was mentioning casualties to motivate you to work harder.");
		AddSub(StoryCharacter.DALTON, "The COMMS link is bad, you're breaking up. Dalton out.");
		AddSub(StoryCharacter.IRIS, "HAH! ****ing wuss.");

		// SECTOR 65 - HISTORY LESSON
		// 65 ONENTERSECTOR
		CreateStory(); // 278
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING AUDIO MESSAGE FROM 'MOTHER'");
		AddSub(StoryCharacter.MOTHER, "Achoo!");
		AddSub(StoryCharacter.MOTHER, "Ooo! Bless me. Goodness.");
		AddSub(StoryCharacter.MOTHER, "I've come down with something again. Oh dear. Seems like I'm getting colds every month! I guess that comes with age.");
		AddSub(StoryCharacter.MOTHER, "Stay young, my sweet @N.");
		AddSub(StoryCharacter.MOTHER, "You know, I could really do with hearing your voice right about now. It would make this cold much more bearable!");
		AddSub(StoryCharacter.COMPUTER, ">> END AUDIO MESSAGE FROM MOTHER");
		AddSub(StoryCharacter.PILOT, "Sorry Mom... soon, I promise. Just... can't right now.");

		// 65 ONMIDSECTOR
		CreateStory(); // 279
		AddSub(StoryCharacter.CHAMPION, "I am curious... Do you feel your brethren clones coming to an end, @N? Or do you feel nothing?");
		AddSub(StoryCharacter.PILOT, "Let's not talk about me. I want to know why you laughed at my answer to when the war started.");
		AddSub(StoryCharacter.CHAMPION, "Very well. What do you say to this?");
		AddSub(StoryCharacter.CHAMPION, "The war began in X932, indeed. But with the assassination of Princess Bernadette Delacroix.");

		// 65 ONPREBOSSFIGHT
		CreateStory(); // X 280
		AddSub(StoryCharacter.PILOT, "..I'd say you're wrong.");
		AddSub(StoryCharacter.CHAMPION, "But of COURSE you would. And I completely understand why you would say this.");
		AddSub(StoryCharacter.CHAMPION, "You have been told your whole life that the Great War began with the assassination of Aldridge.");
		AddSub(StoryCharacter.CHAMPION, "Whereas, I have been told MY whole life that the war began with the assassination of Delacroix.");
		AddSub(StoryCharacter.PILOT, "What's your point?");
		AddSub(StoryCharacter.CHAMPION, "Who is correct?");
		AddSub(StoryCharacter.PILOT, "Does it matter?! It was hundreds of years ago! Can we not just admit that enough is enough?");

		// 65 ONPOSTBOSSFIGHT
		CreateStory(); // 281
		AddSub(StoryCharacter.CHAMPION, "Dear, sweet, Captain @N. How I admire your nobility. And indeed, how I admire your change from soldier to peacemaker.");
		AddSub(StoryCharacter.CHAMPION, "You see, there is only one being alive today who also was when the war began.");
		AddSub(StoryCharacter.PILOT, "The Empress...");
		AddSub(StoryCharacter.CHAMPION, "Indeed. And SHE seems to think it was poor Princess Bernadette whose life was unfairly snatched.");
		AddSub(StoryCharacter.CHAMPION, "That it was the Alliance Republic who began the bloodshed.");
		AddSub(StoryCharacter.CHAMPION, "She feels strong enough about it to want to wipe out your people entirely.");
		AddSub(StoryCharacter.PILOT, "So the Royal Empire DO want genocide. This is the first time any of you have ever admitted it.");
		AddSub(StoryCharacter.CHAMPION, "I want my Empress to be happy. I will feel sadness for the innocent lives you will lose...");
		AddSub(StoryCharacter.CHAMPION, "But the Empress is all-knowing, and I am her humble servant.");

		// SECTOR 66 - SEIGMEID
		// ONENTERSECTOR
		CreateStory(); // 282
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.IRIS, "What's wrong?");
		AddSub(StoryCharacter.PILOT, "Dunno. Just... what the Champion said about the Empress. I guess it's true, huh?");
		AddSub(StoryCharacter.IRIS, "What, that she's the only being alive today who was alive when the war began?");
		AddSub(StoryCharacter.PILOT, "Yeah.");
		AddSub(StoryCharacter.IRIS, "Yeah. Totally true. So what?");
		AddSub(StoryCharacter.PILOT, "So... maybe the Alliance Republic DID start the war. Maybe it's all our fault things have gotten this far.");
		AddSub(StoryCharacter.IRIS, "OUR fault? Nah. The fault of your great, great, great, great grand screenslaves? Totally.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.IRIS, "Still don't make what the Royal Empire have done right. Bitch needs to get OVER it and move on.");

		// 66 ONMIDSECTOR
		CreateStory(); // 283
		AddSub(StoryCharacter.DALTON, "I can live with the damage caused to facilities one and two. Facility three is going to take a bit of work, but is mostly fine.");
		AddSub(StoryCharacter.DALTON, "Make sure Seigmeid and Neumann facilities don't get bashed too badly, @N.");
		AddSub(StoryCharacter.PILOT, "I understand, Dalton. We can't afford for the clone production process to slow down. Or else they'll have the upper hand.");
		AddSub(StoryCharacter.DALTON, "Exactly. And listen... I'm sorry about the over-blown casualty numbers from before...");
		AddSub(StoryCharacter.DALTON, "My staff are really getting sloppy these days...");
		AddSub(StoryCharacter.IRIS, "Yeah. Staff. Riiiight.");
		AddSub(StoryCharacter.IRIS, "Admit it, suit. You don't give a crap how many colonists die as long as your facilities are safe.");
		AddSub(StoryCharacter.DALTON, "That is simply not true!");
		AddSub(StoryCharacter.IRIS, "Oh yeah, of course. You need people to RUN the facilities, after all. They can't ALL die.");

		// 66 ONPREBOSSFIGHT
		CreateStory(); // 284
		AddSub(StoryCharacter.IRIS, "Hope you're not still moping about flunking that history exam.");
		AddSub(StoryCharacter.PILOT, "Don't worry about me. My head's in the game.");
		AddSub(StoryCharacter.IRIS, "Pff. Doesn't matter, anyway. There's tonnes of these Royal Empire goons, but...");
		AddSub(StoryCharacter.IRIS, "...they seem totally out of it. Like they've already given up.");
		AddSub(StoryCharacter.PILOT, "They're fighting against you and me. Of COURSE they've given up!");

		// 66 ONPOSTBOSSFIGHT
		CreateStory(); // 285
		AddSub(StoryCharacter.PILOT, "Did you see that, Champion? That was another one of your most elite units going bye-bye.");
		AddSub(StoryCharacter.PILOT, "Why don't you save the rest of your fleet and turn around?");
		AddSub(StoryCharacter.CHAMPION, "You have done well, @N. I was naive to think your government would not provide adequate defense to the colonies.");
		AddSub(StoryCharacter.PILOT, "Actually, it didn't... but that's another thing all together.");
		AddSub(StoryCharacter.CHAMPION, "I must complete my journey, despite the change in the wind.");
		AddSub(StoryCharacter.ELI, "Good work out there, dudes.");
		AddSub(StoryCharacter.ELI, "Their fleet is moving away from Eledan 5 and the damage to the facility was... well, it's fixable.");
		AddSub(StoryCharacter.ADILA, "The private security can take it from here, @N. The Champion's heading to Neumann Colony at full speed.");
		AddSub(StoryCharacter.ADILA, "You better hurry up and catch him.");

		// SECTOR 67 - LOCK BLADES
		// ONENTERSECTOR
		CreateStory(); // 286
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING AUDIO MESSAGE FROM 'MOM'");
		AddSub(StoryCharacter.MOTHER, "I've been watching the reports on these attacks on Petra, Bristaine, Eledan 5...");
		AddSub(StoryCharacter.MOTHER, "It's all getting a bit close to home, these days. Too close for comfort.");
		AddSub(StoryCharacter.MOTHER, "I'm just so grateful our government thought of contracting private security for those colonies!");
		AddSub(StoryCharacter.PILOT, "Private security?! The GOVERNMENT? That was Adila!");
		AddSub(StoryCharacter.MOTHER, "They're keeping us safe while you're out in the thick of it, sweetie, don't worry.");
		AddSub(StoryCharacter.PILOT, "But... it's me who's fending off the attacks.");
		AddSub(StoryCharacter.MOTHER, "Fingers crossed they don't make it to our little colony! Haha!");
		AddSub(StoryCharacter.MOTHER, "Ha... mmm...");
		AddSub(StoryCharacter.MOTHER, "Talk to you later, dear.");

		// 67 ONMIDSECTOR
		CreateStory(); // 287
		AddSub(StoryCharacter.PILOT, "Have you been watching the news reports?");
		AddSub(StoryCharacter.ADILA, "Yep. I should have expected it, to be honest. Of course Dalton would take credit for my work.");
		AddSub(StoryCharacter.ELI, "We should set the story straight.");
		AddSub(StoryCharacter.ADILA, "No point. We've got a bigger story to tell.");
		AddSub(StoryCharacter.ELI, "The media's been reporting ridiculous casualty numbers, too. God damn scare tactics.");
		AddSub(StoryCharacter.PILOT, "They're so used to lying to the public, it must come naturally.");

		// 67 ONPREBOSSFIGHT
		CreateStory(); // 288
		AddSub(StoryCharacter.CHAMPION, "I want you to see that I am different.");
		AddSub(StoryCharacter.PILOT, "Oh, hi. Okay. You're starting to sound a little weird, now. Sort of... stalker weird.");
		AddSub(StoryCharacter.CHAMPION, "My comrades before me... indeed, the were great strategists. But they killed so many innocents.");
		AddSub(StoryCharacter.CHAMPION, "I know that not every mission has to end in so much bloodshed. You can see this, can't you?");

		// 67 ONPOSTBOSSFIGHT
		CreateStory(); // 289
		AddSub(StoryCharacter.PILOT, "Are you fishing for thanks from me, Champion?");
		AddSub(StoryCharacter.PILOT, "Want me to praise you because you haven't slaughtered as many innocents as your comrades?");
		AddSub(StoryCharacter.CHAMPION, "You don't appear concerned about how many of YOUR fleet who have died so far. That doesn't seem odd to you?");
		AddSub(StoryCharacter.FAKE, "Perhaps it was foolish of me to think you would understand.");
		AddSub(StoryCharacter.PILOT, "Your fleet's looking a lot smaller. I think I'll be nipping at your heels any moment now.");
		AddSub(StoryCharacter.PILOT, "And when I sink my teeth in, I won't be letting go.");
		AddSub(StoryCharacter.CHAMPION, "Come to me, then, young @N. Let us lock blades.");

		// SECTOR 68 - NEUMANN
		// ONENTERSECTOR
		CreateStory(); // 290
		AddSub(StoryCharacter.DALTON, "I want to thank you and your... secretary... personally, for your commitment to this deviation from service.");
		AddSub(StoryCharacter.DALTON, "I think I have been very clear about how important this mission is. So I won't repeat myself.");
		AddSub(StoryCharacter.DALTON, "...");
		AddSub(StoryCharacter.DALTON, "...@N?");
		AddSub(StoryCharacter.DALTON, "Hello?");
		AddSub(StoryCharacter.IRIS, "Oh, @N's off COMMS for now. You're just talking to me. And I just want to respond, personally, to your thanks.");
		AddSub(StoryCharacter.IRIS, "Achem.");
		AddSub(StoryCharacter.IRIS, "Go suck on star decay, ya **** suit ****er.");

		// 68 ONMIDSECTOR
		CreateStory(); // 291
		AddSub(StoryCharacter.ADILA, "Well, you definitely shook up the Champion's fleet. Looks like half of them have just jumped away to another sector.");
		AddSub(StoryCharacter.IRIS, "Hah! You can run, but you can't hide!");
		AddSub(StoryCharacter.ELI, "You seem to really like blowin' up Royal Empire ships, for somebody who claims to be uber-neutral, Iris.");
		AddSub(StoryCharacter.IRIS, "Pfff. Whatever. Just shooting whatever's in front of me.");
		AddSub(StoryCharacter.PILOT, "Let's not get sloppy now, Iris. Stay alert and let's finish this.");

		// 68 - ONPREBOSSFIGHT
		CreateStory(); // 292
		AddSub(StoryCharacter.CHAMPION, "I can see you, @N. Your ship is even more magnificent than I imagined.");
		AddSub(StoryCharacter.IRIS, "Seriously, this is worse than Eli and Adila. This guy WANTS you, @N.");
		AddSub(StoryCharacter.CHAMPION, "My mission is nearly complete. We are getting closer to having our duel.");
		AddSub(StoryCharacter.ELI, "Huh? Something weird's just happened on radar.");
		AddSub(StoryCharacter.IRIS, "Uh... what the crap? What's all that light?!");
		AddSub(StoryCharacter.PILOT, "Are those... warp engines?");
		AddSub(StoryCharacter.ELI, "The Champion and the ships most immediately surrounding his have jumped.");

		// 68 ONPOSTBOSSFIGHT
		CreateStory(); // 293
		AddSub(StoryCharacter.ELI, "Ohh! Oh man. Gotta hand it to them, that's pretty smart.");
		AddSub(StoryCharacter.PILOT, "Sigh. What is it?");
		AddSub(StoryCharacter.ELI, "I did a scan on the Royal Empire fleet that's left.");
		AddSub(StoryCharacter.PILOT, "And?");
		AddSub(StoryCharacter.ELI, "No life forms on board any of them. You've been duped, boss. Auto-pilot. All of 'em.");
		AddSub(StoryCharacter.IRIS, "I pissing KNEW it! I told you there was something weird about their mission!");
		AddSub(StoryCharacter.IRIS, "He didn't care about us killing his fleet because there weren't any soldiers inside the ships!");
		AddSub(StoryCharacter.PILOT, "I didn't take you for the cowardly type, Champion. Why run away now?");
		AddSub(StoryCharacter.CHAMPION, "My apologies, @N. My mission is now complete. I must return home.");
		AddSub(StoryCharacter.CHAMPION, "I'm sorry to have teased you - our duel will happen, don't fret.");

		// SECTOR 69 - SOON, MY WARRIOR
		// ONENTERSECTOR
		CreateStory(); // 294
		AddSub(StoryCharacter.CHAMPION, "Final mission status report.");
		AddSub(StoryCharacter.ROYALENGINEER, "Minimal casualties on Alliance Republic colonies breached.");
		AddSub(StoryCharacter.ROYALENGINEER, "AR Cloning facilities one and two confirmed destroyed.");
		AddSub(StoryCharacter.ROYALENGINEER, "Cloning facility three at less than 50% stability. Facilities four and five sustained minimal damage.");
		AddSub(StoryCharacter.CHAMPION, "Very good. That will be enough for now.");

		// 69 ONMIDSECTOR
		CreateStory(); // 295
		AddSub(StoryCharacter.CHAMPION, "Everything went as planned, Empress.");
		AddSub(StoryCharacter.CHAMPION, "The Gemini Pilot and his pirate companion are engaging with the remaining auto-piloted fleet.");
		AddSub(StoryCharacter.EMPRESS, "Excellent, Nitz. You are truly my finest strategist. I am immensly proud of your achievement thus far.");
		AddSub(StoryCharacter.EMPRESS, "Anything else to report?");
		AddSub(StoryCharacter.CHAMPION, "...I long to duel with the Gemini Pilot, Empress.");
		AddSub(StoryCharacter.EMPRESS, "It will come soon, my warrior.");

		// 69 ONPREBOSSFIGHT
		CreateStory(); // 296
		AddSub(StoryCharacter.CHAMPION, "The Gemini Pilot is different from how I had imagined. @N is... quite special.");
		AddSub(StoryCharacter.EMPRESS, "DO NOT SPEAK THE GEMINI PILOT'S NAME!");
		AddSub(StoryCharacter.CHAMPION, "Forgive me, Empress...");
		AddSub(StoryCharacter.EMPRESS, "No, forgive me, my dear warrior.  I must not so easily lose my patience. Please, speak your mind.");
		AddSub(StoryCharacter.CHAMPION, "The Gemini Pilot wants the war to end. It no longer seems to be forced into searching for you.");
		AddSub(StoryCharacter.EMPRESS, "That is only temporary. They changed its mission protocol to protect the facilities, as we had predicted.");

		// 69 ONPOSTBOSSFIGHT
		CreateStory(); // 297
		AddSub(StoryCharacter.CHAMPION, "All the same, Empress...");
		AddSub(StoryCharacter.CHAMPION, "I am both eager to engage with the Gemini Pilot, and curious as to whether a non-violent solution can be made.");
		AddSub(StoryCharacter.EMPRESS, "Nonsense. They had their chance for non-violence solutions.");
		AddSub(StoryCharacter.EMPRESS, "...Do not be fooled by the Gemini Pilot's guile. Its only purpose is to destroy the Empire.");
		AddSub(StoryCharacter.EMPRESS, "It will say anything to do this.");
		AddSub(StoryCharacter.EMPRESS, "Is your love for the Empire not strong enough to deflect the attacks of the enemy?");
		AddSub(StoryCharacter.CHAMPION, "No, Empress.");
		AddSub(StoryCharacter.EMPRESS, "Good. On to your next mission, then.");

		// SECTOR 70 - SEE YA

		// ONENTERCHAPTER
		CreateStory(); // 298
		AddSub(StoryCharacter.COMPUTER, ">> CRYOGENIC STASIS REPORT");
		AddSub(StoryCharacter.COMPUTER, ">> DURATION: 48 HOURS");
		AddSub(StoryCharacter.COMPUTER, ">> STATUS: COMPLETE");
		AddSub(StoryCharacter.COMPUTER, ">> GOOD MORNING, GEMINI SOLDIER @N");
		AddSub(StoryCharacter.PILOT, "Morning, computer.");
		AddSub(StoryCharacter.DALTON, "Good morning, @N.");
		AddSub(StoryCharacter.PILOT, "Yep.");
		AddSub(StoryCharacter.DALTON, "Come on now, a little 'good morning Mr President' won't hurt!");
		AddSub(StoryCharacter.PILOT, "What do you want?");
		AddSub(StoryCharacter.DALTON, "Just making sure you've had a nice rest.");
		AddSub(StoryCharacter.PILOT, "Yeah. Thanks for that 48 hours of rest. It's really made up for the decade of torture you've put me through.");
		AddSub(StoryCharacter.DALTON, "Good! Now then, best get you heading back to the front line. Your mission protocol has been re-engaged.");

		// 70 ONENTERSECTOR
		CreateStory(); // 299
		AddSub(StoryCharacter.DALTON, "Well, perhaps you aren't in a good mood. I won't let you ruin MY mood, though.");
		AddSub(StoryCharacter.PILOT, "Ok, I'll bite. Why are you in such a good mood?");
		AddSub(StoryCharacter.DALTON, "My facilities sustained far less damage than I anticipated. Casualties ended up being very low, and so will the repair costs!");
		AddSub(StoryCharacter.PILOT, "And the media are singing you praises because of the extra defense you called in to protect the colonies.");
		AddSub(StoryCharacter.DALTON, "Hah, well yes - there is THAT, too.");
		AddSub(StoryCharacter.PILOT, "That was a smart move, Mr President. Where did you find that private security force, anyway?");
		AddSub(StoryCharacter.DALTON, "Mmm? Oh, err - I don't remember right now.");

		// 70 ONMIDSECTOR
		CreateStory(); // 300
		AddSub(StoryCharacter.DALTON, "I feel as though we've come a long way, you and I.");
		AddSub(StoryCharacter.DALTON, "You had every reason to despise me and yet, now, you seem willing to go along with my plans.");
		AddSub(StoryCharacter.DALTON, "I want to thank you for finding your trust in me again, @N. I will not let you down. We WILL destroy the Royal Empire opressors...");
		AddSub(StoryCharacter.DALTON, "...and we will terminate Project Gemini so that your consciousness can get the peace it deserves!");
		AddSub(StoryCharacter.PILOT, "Uggh. I can't take this any more. Eli, can we do this already?");
		AddSub(StoryCharacter.DALTON, "What's that, @N? Who's Eli?");
		AddSub(StoryCharacter.ELI, "Ah dude, @N - you totally ****ed up my surprise.");
		AddSub(StoryCharacter.DALTON, "Surprise? What the hell is going on, @N? Who is that?!");

		// 70 ONPREBOSSFIGHT
		CreateStory(); // 301
		AddSub(StoryCharacter.PILOT, "President Dalton, I want you to meet 10XEN01. Although, I think you may have already met.");
		AddSub(StoryCharacter.ELI, "I hardly recognized you with out the hundreds of fallaces I photoshopped to your face on the government's home page.");
		AddSub(StoryCharacter.DALTON, "HIM! THE HACKER! What kind of treason is this, @N?!");
		AddSub(StoryCharacter.PILOT, "Hush now, Mr President. It's my turn to talk.");
		AddSub(StoryCharacter.PILOT, "Consider this my official resignation from my position as Gemini Pilot for the Alliance Republic.");
		AddSub(StoryCharacter.DALTON, "?!");
		AddSub(StoryCharacter.FAKE, "10XEN01 has kindly re-adjusted the mission protocol...");
		AddSub(StoryCharacter.FAKE, "...along with all of Project Gemini's administrative and managerial proceedures...");
		AddSub(StoryCharacter.FAKE, "...so that I am in complete control.");
		AddSub(StoryCharacter.DALTON, "Impossible!");
		AddSub(StoryCharacter.ELI, "Dude. I thought we cleared up who I was already. Does this guy STILL not get who I am?");

		// 70 ONPOSTBOSSFIGHT
		CreateStory(); // 302
		AddSub(StoryCharacter.PILOT, "This isn't even over, Dalton. Taking Project Gemini from you is just the start.");
		AddSub(StoryCharacter.PILOT, "You'll have to wait and see what else I have in store for you.");
		AddSub(StoryCharacter.DALTON, "@N, let's talk about this. I--");
		AddSub(StoryCharacter.PILOT, "YOU no longer have any say in my business. I will decide where I go, and for what purpose I fight.");
		AddSub(StoryCharacter.PILOT, "And it's about time the people knew about all of the lies you've been feeding them.");
		AddSub(StoryCharacter.DALTON, "No, @N. Don't do this. You don't understand, I had t--");
		AddSub(StoryCharacter.PILOT, "Goodbye, Dalton.");
		AddSub(StoryCharacter.DALTON, "@NN!! @NN, DON'T!");
		AddSub(StoryCharacter.ELI, "See ya later, wiener.");
		AddSub(StoryCharacter.IRIS, "Sayonnara, ****-for-brains.");
		AddSub(StoryCharacter.ADILA, "See you in court, sir.");
		AddSub(StoryCharacter.DALTON, "...Ogonbuwe?!");
		AddSub(StoryCharacter.PILOT, "Computer. Close and block COMMS with Alliance HQ.");

		// SECTOR 71 - AUTO-PILOT
		// ONENTERSECTOR
		CreateStory(); // 303
		AddSub(StoryCharacter.ELI, "That was SO cool. SO, SO cool.");
		AddSub(StoryCharacter.IRIS, "Alright, calm down loser.");
		AddSub(StoryCharacter.ELI, "Sorry. I just never got to see the President's dumb face react to something I've done before.");
		AddSub(StoryCharacter.ELI, "It was even sweeter than I imagined.");
		AddSub(StoryCharacter.ADILA, "We did well. Good work, everyone.");
		AddSub(StoryCharacter.PILOT, "Yeah. Great job, guys.");
		AddSub(StoryCharacter.IRIS, "You can thank me by wiring over some sweet, sweet Creds.");
		AddSub(StoryCharacter.PILOT, "Later. Now, we need to go through our next steps.");

		// 71 ONMIDSECTOR
		CreateStory(); // 304
		AddSub(StoryCharacter.PILOT, "Eli has skilfully implemented a new command into Project Gemini. It sends out clone ships in auto-pilot, into Royal Empire sector zones.");
		AddSub(StoryCharacter.IRIS, "So you're sending auto-piloted ships with inactive @N clones in them into the fray? You heartless bastard!");
		AddSub(StoryCharacter.ADILA, "When you put it like that, it does sound a little immoral.");
		AddSub(StoryCharacter.PILOT, "Hey, these are MY clones. I can do what I want with them. Anyway. This frees me and the main clone-replacement system up for more important work.");
		AddSub(StoryCharacter.PILOT, "As we thought might happen, the Royal Empire are sending ships in to outer Republic space to try and build a military presence within closer range.");
		AddSub(StoryCharacter.PILOT, "So for the time being, I'm going to stay here in outer Republic space and engage with their ships.");

		// 71 ONPREBOSSFIGHT
		CreateStory(); // 305
		AddSub(StoryCharacter.ADILA, "Hopefully you won't have to do it for long, @N. We're nearly ready to commence Project Re-Balance.");
		AddSub(StoryCharacter.PILOT, "Glad to hear it, Adila. You're going to make a great President.");
		AddSub(StoryCharacter.ADILA, "Oh! I'm not ready for that... right?");
		AddSub(StoryCharacter.ELI, "I think you'd make a sexy -- err, great President.");
		AddSub(StoryCharacter.ADILA, "Eli!");

		// 71 ONPOSTBOSSFIGHT
		CreateStory(); // 306
		AddSub(StoryCharacter.IRIS, "As much as fighting off Royal Empire goons for the 'greater good' has been, momma's got an itch for loot.");
		AddSub(StoryCharacter.IRIS, "I'm going to head out for a while. Call me when things get dangerous.");
		AddSub(StoryCharacter.PILOT, "Thanks for everything, Iris.");
		AddSub(StoryCharacter.IRIS, "Whatever.");

		// SECTOR 72 - TIME SIMPLY PASSES

		// NO DIALOGUE


		// SECTOR 73 - PUBLIC ADDRESS
		// ONENTERSECTOR
		CreateStory(); // 307
		AddSub(StoryCharacter.ELI, "You ready, @N?");
		AddSub(StoryCharacter.PILOT, "Ready.");
		AddSub(StoryCharacter.ELI, "Alright, here we go. Transferring live video feed...");
		AddSub(StoryCharacter.PILOT, "It's working! I can see the feed.");
		AddSub(StoryCharacter.ELI, "Where is she? I can't see her. Why are they not just filming HER?");
		AddSub(StoryCharacter.PILOT, "Hush, Eli. Here she comes.");
		AddSub(StoryCharacter.ADILA, "Fellow children of the Alliance Republic. Best day and all power to you, one and all.");

		// 73 - ONMIDSECTOR
		CreateStory(); // 308
		AddSub(StoryCharacter.ADILA, "I am sure many of you are very shocked to hear this morning's headlines.");
		AddSub(StoryCharacter.ADILA, "I am here today to make sure this shock does not wear away. We must not let the shock fade.");
		AddSub(StoryCharacter.ADILA, "We must hold it close, and let it motivate us to always discover the truth.");
		AddSub(StoryCharacter.ADILA, "For a decade, our President has been lying to us about the war we are fighting. We were told that ONE person...");
		AddSub(StoryCharacter.ADILA, "...a child of the Alliance Republic - was our saving grace.");
		AddSub(StoryCharacter.ADILA, "A hero so skilled that we need not fear the oppressors who threaten to destroy us.");
		AddSub(StoryCharacter.ADILA, "But as you have discovered today, this is not exactly the case.");
		AddSub(StoryCharacter.ADILA, "This hero - Gemini Pilot @N - was deceived into a prison sentence unlike any other.");

		// 73 - ONPREBOSSFIGHT
		CreateStory(); // 309
		AddSub(StoryCharacter.ADILA, "There have been many critics to the information my team and I have gathered and released.");
		AddSub(StoryCharacter.ADILA, "People who argue that the President's choice to hide the truth behind Project Gemini from the people was a smart choice.");
		AddSub(StoryCharacter.ADILA, "That we need not know the ins-and-outs of such things.");
		AddSub(StoryCharacter.ADILA, "But I say they are wrong. It is one thing to keep something from the people until they NEED to know.");
		AddSub(StoryCharacter.ADILA, "It is another to tell them an entirely different story. Deception is part of an ugly bond that...");
		AddSub(StoryCharacter.ADILA, "...if it were to continue, will never be broken.");
		AddSub(StoryCharacter.ADILA, "We revealed the truth behind Project Gemini because we want to be the purveyors of a new start.");
		AddSub(StoryCharacter.ADILA, "My team and I do not believe in deception. We believe in truth.");


		// 73 - ONPOSTBOSSFIGHT
		CreateStory(); // 310
		AddSub(StoryCharacter.ADILA, "Over the next few weeks, I anticipate the tremors of a great earthquake will begin.");
		AddSub(StoryCharacter.ADILA, "One that will completely change the way we are given information.");
		AddSub(StoryCharacter.ADILA, "Our plan is simple. To reveal. To speak. To listen. We have told you the truth about Project Gemini.");
		AddSub(StoryCharacter.ADILA, "We have seized control of Project Gemini and will no longer hide its inner workings from you.");
		AddSub(StoryCharacter.ADILA, "Whether we like it or not, we are now all responsible for Project Gemini. And it is, truly, our only hope for fending off the Royal Empire.");
		AddSub(StoryCharacter.ADILA, "And it is, for the moment, our only hope for fending off the Royal Empire.");
		AddSub(StoryCharacter.ADILA, "What you need to ask yourselves is whether you can live with a President who does not trust you to know the truth.");
		AddSub(StoryCharacter.ADILA, "Who does not want you to think, or give your opinion.");
		AddSub(StoryCharacter.ADILA, "I know I don't. And I have a feeling you don't, either.");
		AddSub(StoryCharacter.ADILA, "We'll be taking questions in the foyer. Thank you.");

		// 74 - PREPARATION
		// ONENTERSECTOR
		CreateStory(); // 311
		AddSub(StoryCharacter.PILOT, "Great speech you made yesterday, Adila. I wanted to congratulate you right afterward but all your fans were so desperate for autographs...");
		AddSub(StoryCharacter.ADILA, "Ha, ha. Come on, @N. I didn't play up to the attention, did I?");
		AddSub(StoryCharacter.PILOT, "Nah, you were very pro. How do you think people are taking the news?");
		AddSub(StoryCharacter.ADILA, "Extremely well. Polls are showing that a vast majority of people feel upset with having been lied to for so long.");
		AddSub(StoryCharacter.ADILA, "Now they want change. But the people are still scared, @N. Probably more scared, now they're not being lied to.");


		// 74 - ONMIDSECTOR
		CreateStory(); // 312
		AddSub(StoryCharacter.ADILA, "Believe me when I say this, @N. I'm going to find a way for you to feel at peace. For your tour to end...");
		AddSub(StoryCharacter.PILOT, "Don't waste your time with that. Concentrate on what's happening on terra firma. Let me deal with Project Gemini's future.");
		AddSub(StoryCharacter.ADILA, "You're still determined to seek out the Empress?");
		AddSub(StoryCharacter.PILOT, "Can't see any other way around it. The Champion was clear.");
		AddSub(StoryCharacter.PILOT, "The Empress won't listen to peace talks. She wants the complete obliteration of the Alliance.");
		AddSub(StoryCharacter.ADILA, "...");
		AddSub(StoryCharacter.PILOT, "You need to prepare the nation, Adila. Focus needs to be put into a backup plan, if I can't destroy her.");
		AddSub(StoryCharacter.ADILA, "I understand.");

		// 74 - ONPREBOSSFIGHT
		CreateStory(); // 313
		AddSub(StoryCharacter.ELI, "You can do it, Adila. The people love ya. I mean, who wouldn't?");
		AddSub(StoryCharacter.ADILA, "Eli!");
		AddSub(StoryCharacter.ELI, "Hey, just sayin'.");
		AddSub(StoryCharacter.PILOT, "Alright, Eli - let's leave Adila to get back to work. I have a job for you.");
		AddSub(StoryCharacter.ADILA, "Don't work TOO hard now.  Speak to you soon.");
		AddSub(StoryCharacter.ELI, "Alright, what's this job you have for me?");
		AddSub(StoryCharacter.PILOT, "I have the Empress' coordinates. The Prince of the Royal Empire gave them to me before I.. uhh.. killed him.");

		// 74 - ONPOSTBOSSFIGHT
		CreateStory(); // 314
		AddSub(StoryCharacter.ELI, "Sweet! So we know where she is? You can just fly on over and end this.");
		AddSub(StoryCharacter.PILOT, "Hopefully. First, I want you to run some locational traces on the coordinates. See what you can find out before I head there.");
		AddSub(StoryCharacter.PILOT, "See what you can find out before I head there.");
		AddSub(StoryCharacter.ELI, "Sure thing, boss.");

		// SECTOR 75 - DIGGING
		// ONENTERSECTOR
		CreateStory(); // 315
		AddSub(StoryCharacter.ELI, "Whoa. @N. Whoa. WHOA.");
		AddSub(StoryCharacter.PILOT, "What's wrong?");
		AddSub(StoryCharacter.ELI, "I found... I uh... oh jeez. I don't even know where to start.");
		AddSub(StoryCharacter.PILOT, "Beginning is probably a safe bet.");
		AddSub(StoryCharacter.ELI, "Alright... alright, ok.");
		AddSub(StoryCharacter.ELI, "I ran some locational surveys on the coordinates the Prince gave you.");
		AddSub(StoryCharacter.ELI, "It's a sector called XSZ54. Neatly tucked in a gas gluster just outside of hostile Royal Empire space.");
		AddSub(StoryCharacter.ELI, "So I'm thinking - Hell, that's not too far from you. I would have thought the Empress would be a lot further out than that.");
		AddSub(StoryCharacter.PILOT, "Yeah. The Prince suggested it would take an eternity to find her.");

		// 75 - ONMIDSECTOR
		CreateStory(); // 316
		AddSub(StoryCharacter.ELI, "Right. So I start doing some more digging about the sector. And nothing much comes up.");
		AddSub(StoryCharacter.ELI, "JUST as I'm about to give up, I spy a tiny little report by a Craggian cargo ship that went through XSZ54 a couple of years ago.");
		AddSub(StoryCharacter.ELI, "They claim that some sort of un-marked 'warehouse' had recently been built on a moon out there.");
		AddSub(StoryCharacter.ELI, "So now I'm starting to get quite interested. A building that the Empire have constructed in the last year or so?");
		AddSub(StoryCharacter.ELI, "What could it be for? Why didn't the Alliance Republic know about it?");
		AddSub(StoryCharacter.ELI, "So I start searching for XSZ54 in governmental files. But I'm not coming up with anything about XSZ54.");
		AddSub(StoryCharacter.ELI, "Instead, I find some encrypted e-mails that I GUESS the President thought no one could find.");
		AddSub(StoryCharacter.PILOT, "And..?! Does he know about XSZ54? Does he know what's on it?");
		AddSub(StoryCharacter.ELI, "Yep...");
		AddSub(StoryCharacter.PILOT, "What's on it?!");
		AddSub(StoryCharacter.ELI, "...");
		AddSub(StoryCharacter.ELI, "You.");

		// 75 - ONPREBOSSFIGHT
		CreateStory(); // 317
		AddSub(StoryCharacter.PILOT, "This a joke, Eli?");
		AddSub(StoryCharacter.ELI, "Try and keep up, @N.");
		AddSub(StoryCharacter.ELI, "In the first encrypted e-mail, the President tells his most trusted staff that he believes there is a spy in the cloning facility.");
		AddSub(StoryCharacter.ELI, "Not even Major Brightman was included.");
		AddSub(StoryCharacter.ELI, "He goes on to claim that the Royal Empire want to STEAL Captain @N.");
		AddSub(StoryCharacter.PILOT, "...steal?!");
		AddSub(StoryCharacter.ELI, "Yep. Steal. So he tells these guys you need to be MOVED, to protect the project.");
		AddSub(StoryCharacter.PILOT, "Then what?");
		AddSub(StoryCharacter.ELI, "Then, there's a gap in communication. No e-mails for a while, then suddenly one addressed to all his mid to high level staff. It reads:");
		AddSub(StoryCharacter.ELI, "'Captain @N is no more.'");
		AddSub(StoryCharacter.ELI, "Inform staff under level 6 access that the brave Captain has perished due to the strain of the cloning process.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.ELI, "THEN, about two weeks later, he sends an e-mail ONLY to his second-in-command that simply reads...");
		AddSub(StoryCharacter.ELI, "'XSZ54'.");
		AddSub(StoryCharacter.PILOT, "Son of a....");

		// 75 - ONPOSTBOSSFIGHT
		CreateStory(); // 318
		AddSub(StoryCharacter.PILOT, "So you think I... Or well, the REAL me was moved to XSZ54?");
		AddSub(StoryCharacter.ELI, "I think so, boss. The President must have built another cloning facility out there and has been keeping it a secret.");
		AddSub(StoryCharacter.ELI, "No surprise there, I guess.");
		AddSub(StoryCharacter.PILOT, "But the Prince claimed these were the Empress' coordinates. What does that mean?");
		AddSub(StoryCharacter.ELI, "Sorry, @N... that much I don't know.");
		AddSub(StoryCharacter.PILOT, "Get Iris on COMMS. We've got a rescue mission to do.");

		// SECTOR 76 - ADMIT IT
		// ONENTERSECTOR
		CreateStory(); // 319
		AddSub(StoryCharacter.IRIS, "So you mean... you're NOT dead?");
		AddSub(StoryCharacter.IRIS, "You're just hanging out with The Empress? In ANOTHER secret cloning facility?");
		AddSub(StoryCharacter.PILOT, "We don't know if she's there for sure. But it seems pretty likely that... yes... I'm alive.");
		AddSub(StoryCharacter.IRIS, "Well, this sure is one giant head**** of a story for the history books.");
		AddSub(StoryCharacter.PILOT, "I'm going to boot up some of my clones to take my place here in outer Alliance space.");
		AddSub(StoryCharacter.PILOT, "I've been able to defend things pretty well by myself, so a few auto pilots should do the trick.");
		AddSub(StoryCharacter.IRIS, "And let me guess... you want momma to come with you to XSZ54, huh? Hold yer widdle hand while we go fight the scawy monsters?");
		AddSub(StoryCharacter.PILOT, "Pfff.");

		// 76 - ON MID SECTOR
		CreateStory(); // 320
		AddSub(StoryCharacter.PILOT, "Are you in or not?");
		AddSub(StoryCharacter.IRIS, "Not until you admit you're asking me because I'm the best and you can't do it without me.");
		AddSub(StoryCharacter.PILOT, "Uhh... yeah. Ok.");
		AddSub(StoryCharacter.IRIS, "Say it.");
		AddSub(StoryCharacter.PILOT, "Damn it, really?");
		AddSub(StoryCharacter.ELI, "Better say it, @N. You're just wasting time avoiding it.");
		AddSub(StoryCharacter.PILOT, "...you're the best and I can't do it without you...");
		AddSub(StoryCharacter.IRIS, "There we go! That wasn't so hard, was it? Alrighty. Let's go try and save you... again.");

		// 76: BOSS SECTOR
		CreateStory(); // 321
		AddSub(StoryCharacter.ELI, "If you can get yourself back home safely, I can get into the Project Gemini code again and make the clones mindless.");
		AddSub(StoryCharacter.ELI, "They'd only be you in physical form. You'd be back to being strictly one conscious being again.");
		AddSub(StoryCharacter.PILOT, "You think you can do it?");
		AddSub(StoryCharacter.ADILA, "If anyone can, it's Eli.");
		AddSub(StoryCharacter.ELI, "Thanks, Dillie.");
		AddSub(StoryCharacter.IRIS, "Dillie?! Oh God. You guys did it, didn't you? Eli's got a nickname for her! They did it!");
		AddSub(StoryCharacter.ADILA, "What?! No! We-");
		AddSub(StoryCharacter.ELI, "Yeah, totally did.");
		AddSub(StoryCharacter.ADILA, "ELI!");

		// 76: POST BOSS
		CreateStory(); // 322
		AddSub(StoryCharacter.PILOT, "Alright, Iris and I need to start heading out.");
		AddSub(StoryCharacter.ADILA, "Stay safe, guys. I'll keep in touch with what's going on back home through Eli.");
		AddSub(StoryCharacter.PILOT, "Sounds good. You stay safe, too.");
		AddSub(StoryCharacter.IRIS, "Let us know when the wedding is.");
		AddSub(StoryCharacter.ADILA, "Iris! Please!");
		AddSub(StoryCharacter.IRIS, "Hah! Too easy!");

		// SECTOR 77
		// ONENTERSECTOR
		CreateStory(); // 323
		AddSub(StoryCharacter.CHAMPION, "The Gemini Pilot has secured control of the project's control mechanisms. It has gone rogue.");
		AddSub(StoryCharacter.EMPRESS, "Further proof that they are a despicable nation. Even their precious Gemini Pilot cannot stand them.");
		AddSub(StoryCharacter.CHAMPION, "The Gemini Pilot knows of the facility.");
		AddSub(StoryCharacter.EMPRESS, "What?! How have you let this happen?");
		AddSub(StoryCharacter.CHAMPION, "It has help from an infamous Alliance Republic hacker. The hacker has been providing intel.");
		AddSub(StoryCharacter.EMPRESS, "No matter. We will simply begin sooner than we anticipated. Are we prepared to initiate?");
		AddSub(StoryCharacter.CHAMPION, "Yes, Empress. I think it would be best. It will certainly slow the Gemini Pilot's approach enough for me to assemble a strong defence force.");
		AddSub(StoryCharacter.EMPRESS, "Good. Then, my dear Champion...");
		AddSub(StoryCharacter.EMPRESS, "Initiate 'Project Phoenix'.");

		// 77: ON MID
		CreateStory(); // 324
		AddSub(StoryCharacter.IRIS, "Top secret Alliance cloning facility out near Royal Empire sector space.");
		AddSub(StoryCharacter.IRIS, "Sounds dangerous, and expensive. You sure you got the Creds to cover this?");
		AddSub(StoryCharacter.PILOT, "Can you just shut up about money for like... ONE minute?");
		AddSub(StoryCharacter.IRIS, "What if you ain't there? Last time we tried this I took a shot in the gut. It wasn't fun getting patched up for that.");
		AddSub(StoryCharacter.PILOT, "I won't let it happen this time. I don't know what the plan is, but... I'll figure something out.");
		AddSub(StoryCharacter.IRIS, "Right. YOU. You'll figure something out. Sheesh.");

		// 77: BOSS SECTOR
		CreateStory(); // 325
		AddSub(StoryCharacter.ELI, "Yo, @N. Got an old friend on the line. Wants to talk to you.");
		AddSub(StoryCharacter.PILOT, "Who is it?");
		AddSub(StoryCharacter.DALTON, "Hello, @N.");
		AddSub(StoryCharacter.PILOT, "Are you serious, Eli? Why are you patching HIM in?");
		AddSub(StoryCharacter.DALTON, "I'm asking for two minutes of your time. Then you can keep ignoring me as much as you like.");
		AddSub(StoryCharacter.DALTON, "But I think you will want to hear this.");
		AddSub(StoryCharacter.PILOT, "Clock's ticking.");

		// 77: PRE BOSS
		CreateStory(); // 326
		AddSub(StoryCharacter.DALTON, "It has been a stressful few months for me. Much to your amusement, I'm sure. That Ogonbuwe bitch is really making things tough.");
		AddSub(StoryCharacter.DALTON, "I know it is pointless trying to explain myself to you. You have changed and no longer listen to your family in arms.");
		AddSub(StoryCharacter.DALTON, "I remember when Brightman brought your file to me as his recommendation for Project Gemini.");
		AddSub(StoryCharacter.DALTON, "I was so proud to have such a noble specimen of soldier volunteer to be the face of my project.");
		AddSub(StoryCharacter.IRIS, "Quit kissing ass and get to the point, suit.");

		// 77: POST BOSS
		CreateStory(); // 327
		AddSub(StoryCharacter.DALTON, "You were so WILLING, then. Like any good soldier, you simply trusted in your nation to do what was right.");
		AddSub(StoryCharacter.PILOT, "Then I was taken advantage of. That changes people.");



		// SECTOR 78
		// 78: ENTER
		CreateStory(); // 328
		AddSub(StoryCharacter.DALTON, "But you must know... I never planned to HIDE you.");
		AddSub(StoryCharacter.DALTON, "Like you, I thought the original Captain @N would be able to relax in an extended holiday leave once the initial cloning process had begun.");
		AddSub(StoryCharacter.DALTON, "And that's exactly what DID happen! For a time...");
		AddSub(StoryCharacter.DALTON, "Then I started getting threats from the Royal Empire. They said they knew about the project.");
		AddSub(StoryCharacter.DALTON, "And that they would take you away from me. They looked to you like some sort of... demigod.");
		AddSub(StoryCharacter.IRIS, "@N's got a big enough ego without you using a word like 'demigod'. Jeez.");

		// 78: MID
		CreateStory(); // 329
		AddSub(StoryCharacter.DALTON, "Perhaps they thought... without you, the project would not be able to continue. And perhaps I thought the same. I panicked.");
		AddSub(StoryCharacter.DALTON, "I ordered a team to capture and subdue you. At least, then, I could keep you safe from the Royal Empire.");
		AddSub(StoryCharacter.PILOT, "This is starting to sound like a big pile of excuses again.");
		AddSub(StoryCharacter.DALTON, "I have been doing some... tidying up, in the office. Clearing my desk, in preparation, I suppose.");
		AddSub(StoryCharacter.DALTON, "At some point, I get the suspicion my encrypted e-mails had been tampered with.");
		AddSub(StoryCharacter.DALTON, "It was the expletives they had been replaced with that gave it away.");
		AddSub(StoryCharacter.ELI, "Heh. Sorry. Couldn't help myself.");

		// 78: BOSS SECTOR
		CreateStory(); // 330
		AddSub(StoryCharacter.PILOT, "Then you know that we know... I was moved to XSZ54, to your extra little secret cloning facility.");
		AddSub(StoryCharacter.DALTON, "Yes, you were moved to XSZ54. But not by me.");
		AddSub(StoryCharacter.PILOT, "I've heard enough. Even when it's me with the spoon, you keep trying and feed me lies.");
		AddSub(StoryCharacter.ELI, "Weird analogy, dude.");
		AddSub(StoryCharacter.DALTON, "Wait, @N! You don't understand! It really wasn't me! It's not our facili--");
		AddSub(StoryCharacter.PILOT, "Eli, shut him up.");
		AddSub(StoryCharacter.ELI, "You got it.");

		// 78: POST BOSS
		CreateStory(); // 331
		AddSub(StoryCharacter.IRIS, "You know, I hate that guy's dumb voice too... but maybe you should have just heard him out one last time.");
		AddSub(StoryCharacter.IRIS, "I get the feeling he wasn't lying. And I know liars.");
		AddSub(StoryCharacter.PILOT, "It doesn't matter. Even if Dalton's telling the truth, it means nothing.");
		AddSub(StoryCharacter.PILOT, "So far we've discovered everything ourselves, and dealt with all the crap that came with it.");
		AddSub(StoryCharacter.PILOT, "That system's served us well so far. If it ain't broke, don't fix it.");



		// SECTIR 79
		// 79: ENTER
		CreateStory(); // 332
		AddSub(StoryCharacter.FAKE, "I'm going to leave your corpse in a comical pose.");
		AddSub(StoryCharacter.PILOT, "Nn... mmgh....");
		AddSub(StoryCharacter.FAKE, "The bigger the clone, the bigger the explosion!");
		AddSub(StoryCharacter.PILOT, "Aanh... huh... mmm..");
		AddSub(StoryCharacter.PILOT, "Ahh!");
		AddSub(StoryCharacter.PILOT, "Huh... just a dream...");
		AddSub(StoryCharacter.IRIS, "Morning, sleepy head. Nice of you to join us.");
		AddSub(StoryCharacter.PILOT, "Huh? What, did I sleep in?");
		AddSub(StoryCharacter.IRIS, "Let's just say while you were begging mommy for five more minutes, I was able to fly ahead about a hundred clicks and come back.");
		AddSub(StoryCharacter.PILOT, "Alright, show off. What'd you find?");
		AddSub(StoryCharacter.IRIS, "A totally massive fleet of Royal Empire goons heading straight for us.");
		AddSub(StoryCharacter.PILOT, "They know we're here. Alright, let's get strapped in.");

		// 79: MID
		CreateStory(); // 333
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING FOREIGN COMMS REQUEST FROM 'CHAMPION'");
		AddSub(StoryCharacter.PILOT, "Let it through, computer.");
		AddSub(StoryCharacter.CHAMPION, "I should have known your hacker friend might stumble across XSZ54.");
		AddSub(StoryCharacter.PILOT, "Listen up, Champion, I'm tired of playing games. Answer me straight, one soldier to another.");
		AddSub(StoryCharacter.PILOT, "Have you taken over XSZ54? Is the Empress there?");
		AddSub(StoryCharacter.CHAMPION, "No, and no.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.CHAMPION, "We didn't 'take over' the facility on XSZ54...");
		AddSub(StoryCharacter.CHAMPION, "We built it.");

		// 79: BOSS SECTOR
		CreateStory(); // 334
		AddSub(StoryCharacter.PILOT, "You... built it?");
		AddSub(StoryCharacter.IRIS, "Told you Dalton wasn't lying.");
		AddSub(StoryCharacter.CHAMPION, "It was the Empress who saw the genius in Project Gemini. Personally, I found the whole thing rather crude.");
		AddSub(StoryCharacter.CHAMPION, "All the same, I was ordered to capture Captain @N from the Alliance Republic and bring him to our facility on XSZ54.");
		AddSub(StoryCharacter.CHAMPION, "We would beat the Alliance Republic at their own game.");
		AddSub(StoryCharacter.PILOT, "I'm coming to get myself back, Champion. After our encounter at the other facilities, I could tell your heart is not in this war.");
		AddSub(StoryCharacter.PILOT, "You're just doing what you're told, but you want change. I can SEE it.");

		// 79: POST BOSS
		CreateStory(); // 335
		AddSub(StoryCharacter.CHAMPION, "It does sadden me to see the great Captain @N... lying in a chamber, kept in slumber. Unable to break free and do what the warrior does best.");
		AddSub(StoryCharacter.PILOT, "Then release me. You and I can talk, Champion. It doesn't have to be this way!");
		AddSub(StoryCharacter.CHAMPION, "When we moved you... we heard the clones were badly effected. Migraines, I believe it was. Is it true?");



		// SECTOR 80
		// 80: ENTER
		CreateStory(); // 336
		AddSub(StoryCharacter.PILOT, "The migraines... they were because of the move...?");
		AddSub(StoryCharacter.CHAMPION, "It would seem so. The stress of the move had an effect on the clones. ");
		AddSub(StoryCharacter.CHAMPION, "There is a bond between Captain @N and the clones that transcends the machinery which would otherwise seem to be their only connection.");
		AddSub(StoryCharacter.PILOT, "Why didn't you kill Captain @N, then? Maybe it would destroy all of the clones.");
		AddSub(StoryCharacter.CHAMPION, "Perhaps. But if it did not, we would not have Project Phoenix.");
		AddSub(StoryCharacter.PILOT, "Project... Phoenix?");

		// 80: MID
		CreateStory(); // 337
		AddSub(StoryCharacter.PILOT, "I know you don't want this, Champion.");
		AddSub(StoryCharacter.PILOT, "Even if you succeed, once I'm eliminated the entire Alliance Republic will be wiped out. I KNOW you hold innocent lives dear.");
		AddSub(StoryCharacter.CHAMPION, "...you know nothing, Captain @N.");

		// 80: PRE BOSS
		CreateStory(); // 338
		AddSub(StoryCharacter.IRIS, "I know something. I know I'm going to love watching your facility go up in flames.");
		AddSub(StoryCharacter.PILOT, "We are close, Champion. Let us in. Let me come home, then let me try to change things. No more of these projects designed to obliterate!");
		AddSub(StoryCharacter.CHAMPION, "...");

		// 80: CHAPTER
		CreateStory(); // 339
		AddSub(StoryCharacter.CHAMPION, "You are too late. Project Phoenix has been initiated.");
		AddSub(StoryCharacter.CHAMPION, "I think it is about time you met your extended family, Captain @N.");
		AddSub(StoryCharacter.COMPUTER, ">> COMMS WITH 'CHAMPION' CLOSED.");
		AddSub(StoryCharacter.PILOT, "...");


		// SECTOR 81
		// 81: ENTER
		CreateStory(); // 340
		AddSub(StoryCharacter.ELI, "@N...");
		AddSub(StoryCharacter.ELI, "Crap...");
		AddSub(StoryCharacter.PILOT, "What is it?");
		AddSub(StoryCharacter.ELI, "I'm detecting a large number of Royal Empire ships heading out of XSZ54 space that don't match any diagnostic reports we have on their tech.");
		AddSub(StoryCharacter.ELI, "They're... they're clones of you.");
		AddSub(StoryCharacter.FAKE, "Best day and power to you all, my Imperial comrades! Today we free ourselves from the Alliance oppressors!");
		AddSub(StoryCharacter.PILOT, "...it's started.");

		// 81: MID
		CreateStory(); // 341
		AddSub(StoryCharacter.IRIS, "Hey, I just blew up your cousin, @N #423.");
		AddSub(StoryCharacter.PILOT, "Now's not the time for jokes.");
		AddSub(StoryCharacter.IRIS, "I kinda think now's the PERFECT time for jokes.");
		AddSub(StoryCharacter.FAKE, "Hey @N! I've heard a lot about you. I hope we can meet each other real soon.");
		AddSub(StoryCharacter.PILOT, "Great. Now they're talking to me.");

		// 81: BOSS SECTOR
		CreateStory(); // 342
		AddSub(StoryCharacter.PILOT, "Eli, do we know how many clones they've produced?");
		AddSub(StoryCharacter.ELI, "All I can tell is they're re-producing at a pretty slow rate - slower than our own clones are created.");
		AddSub(StoryCharacter.PILOT, "Well, that's something at least.");

		// 81: POST
		CreateStory(); // 343
		AddSub(StoryCharacter.PILOT, "Let's keep pushing towards XSZ54, Iris.");
		AddSub(StoryCharacter.PILOT, "Just take out as many as these... clones... as you can.");
		AddSub(StoryCharacter.IRIS, "No problem. I've been dreaming about shooting you since the day we met.");


		// SECTOR 82
		// 82: ENTER
		CreateStory(); // 344
		AddSub(StoryCharacter.FAKE, "I just wanna get out there and kill me some Republic Alliance space dorks!");
		AddSub(StoryCharacter.PILOT, "...");

		// 82: MID
		CreateStory(); // 345
		AddSub(StoryCharacter.FAKE, "OORAH!");
		AddSub(StoryCharacter.FAKE, "That all you got, little clone?");
		AddSub(StoryCharacter.PILOT, "...");

		// 82: BOSS SECTOR
		CreateStory(); // 346
		AddSub(StoryCharacter.FAKE, "Knock me down, I'll just get right back up again.");

		// 82: PRE BOSS
		CreateStory(); // 347
		AddSub(StoryCharacter.FAKE, "Wait, @N, please! Don't shoot! I'm on your side! I don't want to die!");
		AddSub(StoryCharacter.FAKE, "HAHAHAHA!");


		// SECTOR 83
		// 83: ENTER
		CreateStory(); // 348
		AddSub(StoryCharacter.COMPUTER, ">> INCOMING AUDIO MESSAGE FROM MOTHER");
		AddSub(StoryCharacter.MOTHER, "...");
		AddSub(StoryCharacter.MOTHER, "...");
		AddSub(StoryCharacter.MOTHER, "I don't want to believe it.");
		AddSub(StoryCharacter.MOTHER, "I... didn't want to believe it.");
		AddSub(StoryCharacter.MOTHER, "At first I thought... it's just left-wing propaganda. Hippies jealous of my sweet @N, trying to demonise!");

		// 83: MID
		CreateStory(); // 349
		AddSub(StoryCharacter.MOTHER, "Then the reports are bigger... and bigger...");
		AddSub(StoryCharacter.MOTHER, "And it's all confirmed true. Not a single person denying it now.");
		AddSub(StoryCharacter.MOTHER, "Are you really, my sweet @N...?");
		AddSub(StoryCharacter.MOTHER, "Is my baby really gone?");
		AddSub(StoryCharacter.PILOT, "No Mom.. no, I'm here.. I'm... I'm here!");
		AddSub(StoryCharacter.FAKE, "YEAH MOM! I'm DEAD! Just like YOU'RE going to be dead!");
		AddSub(StoryCharacter.PILOT, "What the?! How's it hearing my audio messages?");

		// 83: BOSS SECTOR
		CreateStory(); // 350
		AddSub(StoryCharacter.MOTHER, "H-How long have you been dead for? They said three years on the news...");
		AddSub(StoryCharacter.FAKE, "I was dead from the moment I was born, mommy!");
		AddSub(StoryCharacter.PILOT, "AHH, SHUT UP!");
		AddSub(StoryCharacter.IRIS, "Hey, freakazoid. You alright in there?");
		AddSub(StoryCharacter.PILOT, "...I'm fine.");

		// 83: POST BOSS
		CreateStory(); // 351
		AddSub(StoryCharacter.MOTHER, "If I could just hear your voice one last time...");
		AddSub(StoryCharacter.MOTHER, "Why am I even sending this? If you're really...");
		AddSub(StoryCharacter.MOTHER, "If you're really dead... you can't hear this...");
		AddSub(StoryCharacter.MOTHER, "...I love you, baby @N...");
		AddSub(StoryCharacter.COMPUTER, ">> END OF AUDIO MESSAGE FROM MOTHER");
		AddSub(StoryCharacter.FAKE, "Love you too, mommy!");
		AddSub(StoryCharacter.PILOT, "...");


		// SECTOR 84
		// 84: ENTER
		CreateStory(); // 352
		AddSub(StoryCharacter.PILOT, "That's it. We need to end this, NOW.");
		AddSub(StoryCharacter.IRIS, "Great idea, @N, but just ooooonnee little problem. We keep getting attacked by the Royal Empire and your extended family.");
		AddSub(StoryCharacter.PILOT, "Maybe I can press into the facility. You can stay up here and...");
		AddSub(StoryCharacter.IRIS, "And fight off your clones?! Open your eyes, @N - they're chasing YOU. We need to take advantage of that.");

		// 84: MID
		CreateStory(); // 353
		AddSub(StoryCharacter.PILOT, "What are you saying?");
		AddSub(StoryCharacter.IRIS, "I ain't sayin' nothing else. I'm heading in to the facility.");
		AddSub(StoryCharacter.PILOT, "Iris, no! We need to do this together!");
		AddSub(StoryCharacter.IRIS, "Will you stop trying to protect me, God damn it?! Let ME protect YOU!");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.ELI, "She's slipping right through their fleet, @N. It's like she doesn't exist.");
		AddSub(StoryCharacter.PILOT, "...they're just focused on me.");
		AddSub(StoryCharacter.PILOT, "Alright. I'll give them something to focus on. Sorry, Iris. Get in there and save me!");
		AddSub(StoryCharacter.IRIS, "Aarrr.");

		// 84: BOSS SECTOR
		CreateStory(); // 354
		AddSub(StoryCharacter.IRIS, "I'm coming in to land. The place looks totally deserted. They must all be trying to shoot you down.");
		AddSub(StoryCharacter.PILOT, "Be on your guard. They aren't that stupid.");
		AddSub(StoryCharacter.IRIS, "I guess we don't have a map of the facility, huh?");
		AddSub(StoryCharacter.ELI, "If you can get to any sort of console with trans-space COMMS and send me a signal, I should be able to get in there and direct you.");
		AddSub(StoryCharacter.IRIS, "Sounds dorky. Can't I just shoot my way through?");

		// 84: POST BOSS
		CreateStory(); // 355
		AddSub(StoryCharacter.FAKE, "You think you're one of us, but you're not. You're just a clone of a clone. WE'RE the real deal!");
		AddSub(StoryCharacter.PILOT, "Get a move on, Iris. These clones are driving me nuts...");
		AddSub(StoryCharacter.IRIS, "Keep your socks on. Momma's movin' as fast as she can.");


		// SECTOR 85
		// 85: ENTER
		CreateStory(); // 356
		AddSub(StoryCharacter.FAKE, "The Empress appreciates what we have to endure, @N. She loves us.");
		AddSub(StoryCharacter.FAKE, "Join us. Join the Empress. She loves us. She loves you.");
		AddSub(StoryCharacter.PILOT, "SHUT UP!");

		// 85: MID
		CreateStory(); // 357
		AddSub(StoryCharacter.ELI, "Don't want to put extra pressure on you, Iris, but... @N is starting to lose it. He says the clones are talking, but I only hear @N on COMMS.");
		AddSub(StoryCharacter.IRIS, "....I'm moving as fast as I can, Eli. Have you tracked down where the real @N is?");
		AddSub(StoryCharacter.ELI, "Alright, you're in the facility's D wing now. There's a room at the end of the corridor that is connected to the main cloning systems.");
		AddSub(StoryCharacter.ELI, "If I had to guess, I'd say @N is there.");
		AddSub(StoryCharacter.IRIS, "Alright. Moving in.");

		// 85: PRE BOSS
		CreateStory(); // 358
		AddSub(StoryCharacter.PILOT, "You're just copies... just like me. How did she... how did they turn you against me?");
		AddSub(StoryCharacter.FAKE, "We didn't need much prompting.");
		AddSub(StoryCharacter.FAKE, "Just a line of code, @N.");
		AddSub(StoryCharacter.FAKE, "Just a simple line of code, @N.");
		AddSub(StoryCharacter.FAKE, "ATTEN-TION, SOLDIER!");
		AddSub(StoryCharacter.PILOT, "Just a line of code...");

		// 85: POST BOSS
		CreateStory(); // 359
		AddSub(StoryCharacter.IRIS, "Ding DONG! We're in business! I spy a very ugly looking Captain @N lying on a table.");
		AddSub(StoryCharacter.PILOT, "I'm... I'm alive? It's me?");
		AddSub(StoryCharacter.IRIS, "I'm going wake this jerk up.");
		AddSub(StoryCharacter.ELI, "You should be able to gently--");
		AddSub(StoryCharacter.IRIS, "HEY! WAKE UP, DOUCHE BAG! I'M HERE TO RESCUE YOU!");



		// SECTOR 86
		// 86: ENTER
		CreateStory(); // 360
		AddSub(StoryCharacter.REAL, "What... what is... what's happening...?");
		AddSub(StoryCharacter.REAL, "Who... who are you?");
		AddSub(StoryCharacter.IRIS, "Ah jeez. You mean to tell me the real @N doesn't have any of the clone memories?");
		AddSub(StoryCharacter.ELI, "Looks like you'll have to fill in the gaps for Captain @N.");
		AddSub(StoryCharacter.IRIS, "Long story, Cap'n. I'm Iris, a friend, who you don't know yet. ");
		AddSub(StoryCharacter.IRIS, "Your clones are up there fighting off Royal Empire freaks while I rescue you from an endless life of clone production.");

		// 86: MID
		CreateStory(); // 361
		AddSub(StoryCharacter.REAL, "I'm... not at the Alliance HQ?");
		AddSub(StoryCharacter.IRIS, "Try to keep up, idiot. Come on, put your arm around me. We need to get out of here.");
		AddSub(StoryCharacter.REAL, "So... we're friends?");
		AddSub(StoryCharacter.IRIS, "Much as I hate to admit it, yeah.");
		AddSub(StoryCharacter.REAL, "Why... where... where's Brightman? President Dalton?");
		AddSub(StoryCharacter.IRIS, "Brightman's dead. Dalton's a prick. They did this to you.");
		AddSub(StoryCharacter.IRIS, "Then the Royal Empire stole you and started making their own clones. It's all pretty ****ed up.");

		// 86: POST BOSS
		CreateStory(); // 362
		AddSub(StoryCharacter.PILOT, "Let me talk to... me.");
		AddSub(StoryCharacter.IRIS, "Alright. I'm going to let you talk to another friend. Try not to freak out.");
		AddSub(StoryCharacter.PILOT, "Hello.");
		AddSub(StoryCharacter.REAL, "...Hi.");
		AddSub(StoryCharacter.PILOT, "We don't have much time. Things are tense up here. Iris will look after you, don't worry.");
		AddSub(StoryCharacter.PILOT, "Just follow her and everything will be fine. We'll explain everything when we're back in Alliance space.");
		AddSub(StoryCharacter.REAL, "Okay... okay, yes. I'll do that.");
		AddSub(StoryCharacter.REAL, "Thanks... @N.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.ELI, "Yo, we're going to have to save all the pleasantries for later. You've got company, Iris. The facility's defence drones have been activated.");
		AddSub(StoryCharacter.IRIS, "FINALLY! I get to shoot stuff!");



		// SECTOR 87
		// 87: ENTER
		CreateStory(); // 363
		AddSub(StoryCharacter.FAKE, "We will fight each other for all eternity!");
		AddSub(StoryCharacter.FAKE, "It's our destiny, cousin!");
		AddSub(StoryCharacter.PILOT, "I'm not your God damn cousin.");

		// 87: MID
		CreateStory(); // 364
		AddSub(StoryCharacter.IRIS, "These drones are a dawdle. I've fought toilets that were more dangerous.");
		AddSub(StoryCharacter.FAKE, "It's your sixth consecutive tour, @N! You know what that means!");
		AddSub(StoryCharacter.REAL, "How about you stop showing off and we get the hell off that planet?");
		AddSub(StoryCharacter.FAKE, "All I think about, @N!");
		AddSub(StoryCharacter.IRIS, "Yep, you're @N alright. Buzzkill through and through.");
		AddSub(StoryCharacter.FAKE, "Blowing the living hell out of the Alliance Republic!");



		// SECTOR 89
		// 89: PRE BOSS
		CreateStory(); // 365
		AddSub(StoryCharacter.CHAMPION, "Your brethren seem to trouble you, Captain @N. Does it worry you, how easy it was to convert them to our cause?");
		AddSub(StoryCharacter.PILOT, "You keep strutting about, calling yourself a warrior. Taunting me. But face it...");
		AddSub(StoryCharacter.PILOT, "You can't win against me. You know it.");
		AddSub(StoryCharacter.PILOT, "The only thing you're hoping for is that MAYBE these clones will distract me enough so that you might have the pleasure of taking me out.");
		AddSub(StoryCharacter.CHAMPION, "My dear Captain @N. When will you acknowledge my military genius? Must I always explain my every move?");
		AddSub(StoryCharacter.PILOT, "...");

		// 89: POST BOSS
		CreateStory(); // 366
		AddSub(StoryCharacter.CHAMPION, "You are quite right. I cannot defeat the Gemini PILOT. The infinite army of clones...");
		AddSub(StoryCharacter.CHAMPION, "But I CAN defeat Captain @N.");
		AddSub(StoryCharacter.PILOT, "...What?");
		AddSub(StoryCharacter.CHAMPION, "Do you really think I would just leave the cloning facility un-attended for your pirate friend to walk in and take Captain @N?");
		AddSub(StoryCharacter.ELI, "Oh, ****. Iris, uh... dunno how to tell you this. But... the Champion just blew your ship up.");
		AddSub(StoryCharacter.IRIS, "HE DID WHAT?! MY ****ING SHIP!?!");



		// SECTOR 90
		// 90: ENTER
		CreateStory(); // 367
		AddSub(StoryCharacter.CHAMPION, "My next missiles are locked on to the facility. Enough fire power to destroy all life forms on XSZ54.");
		AddSub(StoryCharacter.CHAMPION, "Project Phoenix, Captain @N, and pirate Iris shall be extinguished like the candle's flame.");
		AddSub(StoryCharacter.IRIS, "Hey @N, do me a favour? Kill that guy. Kill him FAST. Like, before he blows me up.");
		AddSub(StoryCharacter.ELI, "Iris, there's an emergency room in the facility. Take a right down that corridor and head to the H wing.");
		AddSub(StoryCharacter.ELI, "I'll bet they have some escape pods there.");
		AddSub(StoryCharacter.IRIS, "Good thinking, dork. I'm on it.");

		// 90: MID
		CreateStory(); // 368
		AddSub(StoryCharacter.CHAMPION, "You were right about something else, as well. My heart is no longer with the Empress and her plans to wipe out the Republic Alliance.");
		AddSub(StoryCharacter.CHAMPION, "That is not something I long for any more.");
		AddSub(StoryCharacter.CHAMPION, "I simply long to defeat MY enemy. To mount the head of the greatest beast that ever lived upon my wall.");
		AddSub(StoryCharacter.PILOT, "Don't you ****ing dare. They are innocent. This is between YOU and ME!");
		AddSub(StoryCharacter.EMPRESS, "Champion Nitz. You will stop this treacherous behaviour at ONCE.");
		AddSub(StoryCharacter.CHAMPION, "My thirst for the duel is too great. Greater than my service to you.");
		AddSub(StoryCharacter.EMPRESS, "NITZ! DO NOT FIRE! Project Phoenix is EVERYTHING to your Empress!");

		// 90: PRE BOSS
		CreateStory(); // 369
		AddSub(StoryCharacter.IRIS, "Found the escape pod. But hey, guess what? Shit just got REAL interesting. There's only one pod. And it's not big enough for two.");
		AddSub(StoryCharacter.PILOT, "God damnit!");
		AddSub(StoryCharacter.IRIS, "Alright, here's the plan. I send Captain douchebag up in the escape pod, you blow up the Champion and high-tail your ass down here to -- OW, HEY!");
		AddSub(StoryCharacter.ELI, "What the hell's going on?");
		AddSub(StoryCharacter.IRIS, "@N, you devious little prick, what'd you push me into the pod for?");
		AddSub(StoryCharacter.REAL, "I can't let you risk your life any further for me. My brain tells me I don't know who you are. But another voice tells me otherwise.");
		AddSub(StoryCharacter.REAL, "I'm from the Republic Alliance. Back there, I'm not known for letting a stranger die to save me. It's me who does the saving.");
		AddSub(StoryCharacter.PILOT, "No... no, shut up! What am I doing?!");
		AddSub(StoryCharacter.REAL, "And I'm not about to start getting known for that, now.");
		AddSub(StoryCharacter.REAL, "Goodbye. Thank you, all of you, for trying your best. It wasn't meant to be.");
		AddSub(StoryCharacter.IRIS, "No! Wait! Stop! Aaahhhhh! @N!");

		// 90: POST BOSS
		CreateStory(); // 370
		AddSub(StoryCharacter.CHAMPION, "Forgive me, Empress...");
		AddSub(StoryCharacter.CHAMPION, "Forgive me, Captain @N...");
		AddSub(StoryCharacter.CHAMPION, "Now... I can... rest.");
		AddSub(StoryCharacter.CHAMPION, "*BOOM!*");
		AddSub(StoryCharacter.EMPRESS, "No, Nitz! What have you done?!");
		AddSub(StoryCharacter.ELI, "Champion's arsenal has been launched... XSZ54 is hit...");
		AddSub(StoryCharacter.ELI, "...");
		AddSub(StoryCharacter.ELI, "Complete destruction of the facility confirmed.");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.ELI, "Iris? Iris, do you copy? Did she make it out of the explosion...?");
		AddSub(StoryCharacter.IRIS, "I'm here...");
		AddSub(StoryCharacter.IRIS, "God damnit... after all of that.");
		AddSub(StoryCharacter.ELI, "I'm sorry, @N... we were so close.");



		// SECTOR 91
		// 91: ENTER
		CreateStory(); // 371
		AddSub(StoryCharacter.ADILA, "The sixteenth day of Primus, X45X.");
		AddSub(StoryCharacter.ADILA, "Three hours ago, President Graham Dalton was sentenced to 25 years imprisonment.");
		AddSub(StoryCharacter.ADILA, "His actions were deemed as traitorous and oppressive against the free-thinking people of the Alliance Republic.");
		AddSub(StoryCharacter.ADILA, "Above anyone else, I think it's me who has wanted to see Dalton removed from office and his true colours revealed.");
		AddSub(StoryCharacter.ADILA, "I thought when this day came, I would be happy. That I would feel... invincible.");
		AddSub(StoryCharacter.ADILA, "...");
		AddSub(StoryCharacter.ADILA, "But instead... I feel like I could have done better. I could have fought harder.");
		AddSub(StoryCharacter.ADILA, "Because now I realize seeing the end of Dalton's reign should not have been my main objective.");
		AddSub(StoryCharacter.ADILA, "It should have been...");
		AddSub(StoryCharacter.ADILA, "Seeing the end of the war. And the beginning of peace.");

		// 91: MID
		CreateStory(); // 372
		AddSub(StoryCharacter.ELI, "I spent almost my entire life sat in front of a computer screen, in a dimly lit room, talking to people who are miles away from me.");
		AddSub(StoryCharacter.ELI, "I probably had ... I dunno... six, maybe seven, interactions with a REAL, living thing, during the ages of 17 to 22.");
		AddSub(StoryCharacter.ELI, "Hah... crazy, right?");
		AddSub(StoryCharacter.ELI, "So when Adila and I started getting serious, and then she dropped the marriage bombshell...");
		AddSub(StoryCharacter.ELI, "It kinda screwed me up.");
		AddSub(StoryCharacter.ELI, "But you know... I got over it.");
		AddSub(StoryCharacter.ELI, "And I really got in to married life, hah! Dude, I wouldn't have guessed. It's bad ass!");
		AddSub(StoryCharacter.ELI, "She goes out and works her ass off. Sometimes I barely see her.");
		AddSub(StoryCharacter.ELI, "We make time for COMMS calls, but it ain't the same as being in the same room.");
		AddSub(StoryCharacter.ELI, "Because of her... now I feel WEIRD always being alone.");
		AddSub(StoryCharacter.ELI, "But... not just because of her.");
		AddSub(StoryCharacter.ELI, "Because of everything that happened.");

		// 91: BOSS SECTOR
		CreateStory(); // 373
		AddSub(StoryCharacter.ELI, "So she gets me this job working for her office.");
		AddSub(StoryCharacter.ELI, "I know, I know! ME. Working for a freaking POLITICIAN.");
		AddSub(StoryCharacter.ELI, "But ... yeah, I dunno. It's kinda fun, to be honest. I'm still hacking and stuff. But I feel like the things I do now...");
		AddSub(StoryCharacter.ELI, "...they actually mean something.");

		// 91: POST BOSS
		CreateStory(); // 374
		AddSub(StoryCharacter.ELI, "Every now and again I try to find Iris.");
		AddSub(StoryCharacter.ELI, "All these years thinking I was the best hacker in the Universe, and I can't find one loudmouthed, rude and dangerous pirate chick.");
		AddSub(StoryCharacter.ELI, "...");
		AddSub(StoryCharacter.ELI, "Guess she was really cut up about the argument. She really thought we were going to save Captain @N.");
		AddSub(StoryCharacter.ELI, "And this time, she wasn't able to look on the bright side.");
		AddSub(StoryCharacter.ELI, "I know it was only a few years ago... But man.");
		AddSub(StoryCharacter.ELI, "It feels like EVERYTHING has changed.");



		// SECTOR 92
		// 92: ENTER
		CreateStory(); // 375
		AddSub(StoryCharacter.ADILA, "The twentieth day of Primus, X45X.");
		AddSub(StoryCharacter.ADILA, "The Gemini Project continues towards the new coordinates we were given by Prince Cyril Enfant.");
		AddSub(StoryCharacter.ADILA, "The Prince's sudden return was a surprise for everyone. I felt a bit dumb for not knowing his death had been faked.");
		AddSub(StoryCharacter.ADILA, "His time away from the Royal Empire was very beneficial. He came back with a renewed patriotism for his land, and the desire to change things.");
		AddSub(StoryCharacter.ADILA, "He's done well to pry a lot of power from the Empress. The vast majority of the Empire now support a ceasefire with the Alliance Republic.");
		AddSub(StoryCharacter.ADILA, "A significantly smaller, but lethally devout fleet continues to fight for the Empress' unwithering plan to see us exterminated.");

		// 92: MID
		CreateStory(); // 376
		AddSub(StoryCharacter.ADILA, "The last time I spoke with Gemini Pilot @N, I promised to find a way out of this wormhole we seem to have been pulled into.");
		AddSub(StoryCharacter.ADILA, "I know that @N did not believe me then. Nor do I think @N believes I will do it, now.");
		AddSub(StoryCharacter.ADILA, "And in truth... some days I don't believe it myself.");
		AddSub(StoryCharacter.ADILA, "But I think we're getting closer. We HAVE to be.");

		// 92: BOSS SECTOR
		CreateStory(); // 377
		AddSub(StoryCharacter.ADILA, "My staff tell me that they believe @N will find and destroy the Empress...");
		AddSub(StoryCharacter.ADILA, "...before the Prince and I will have a chance to create a truce between our two nations.");
		AddSub(StoryCharacter.ADILA, "They suggest that if this happens, I should terminate Project Gemini as the ultimate gesture of good will to the Prince.");
		AddSub(StoryCharacter.ADILA, "They say we should put Gemini Pilot @N to rest.");

		// 92: POST BOSS
		CreateStory(); // 378
		AddSub(StoryCharacter.ADILA, "But control over Project Gemini is not mine. It belongs only to Gemini Pilot @N.");
		AddSub(StoryCharacter.ADILA, "Some of my staff claim that the day Captain @N died, Gemini Pilot @N changed.");
		AddSub(StoryCharacter.ADILA, "They worry that even if @N destroys the Empress, it will not quench the thirst for destruction the clones have now developed after so many years.");
		AddSub(StoryCharacter.ADILA, "They criticise my decision to give full control of the project to @N. Of course, it wasn't MY decision. It was my husband's...");
		AddSub(StoryCharacter.ADILA, "Who trusts @N implicitly.");
		AddSub(StoryCharacter.ADILA, "And I trust my husband.");


		// SECTOR 93
		// 93: ENTER
		CreateStory(); // 379
		AddSub(StoryCharacter.ELI, "@N?");
		AddSub(StoryCharacter.ELI, "Adila isn't sure if you'll make it to the Empress.");
		AddSub(StoryCharacter.ELI, "Don't take that to heart, though. It's not like she's dissing your sweet piloting skills.");
		AddSub(StoryCharacter.ELI, "She just thinks that this truce thing with the Prince is going to be the end of the war.");
		AddSub(StoryCharacter.ELI, "...Actually, a lot of people think you won't make it to the Empress.");
		AddSub(StoryCharacter.ELI, "The Prince has fleets attacking the Empress' loyalists. You've had more allies watching your back now then ever before.");
		AddSub(StoryCharacter.ELI, "They think it's only a matter of time until the Empress' force dwindles...");
		AddSub(StoryCharacter.ELI, "...and the combined force of the Alliance Republic and the Prince's nationalists will be what finally destroys the Empress.");
		AddSub(StoryCharacter.ELI, "@N, do yo copy?");

		// 93: MID
		CreateStory(); // 380
		AddSub(StoryCharacter.ELI, "But I don't believe that. I think you're in too deep, now, to not be the one who takes her out.");
		AddSub(StoryCharacter.ELI, "I know you're determined to put an end to all of this.");
		AddSub(StoryCharacter.ELI, "...");
		AddSub(StoryCharacter.ELI, "...Why else would you ask me to make the termination code");

		// 93: BOSS SECTOR
		CreateStory(); // 381
		AddSub(StoryCharacter.ELI, "And that's the thing, you know?");
		AddSub(StoryCharacter.ELI, "Like ... the hardest thing, here, for ME...");
		AddSub(StoryCharacter.ELI, "Like ... the hardest thing, here, for ME...");
		AddSub(StoryCharacter.ELI, "It's not helping you. Because I know this is what you want... And I know that helping you in the first place was the start of everything that I now love.");
		AddSub(StoryCharacter.ELI, "The hardest thing is...");
		AddSub(StoryCharacter.ELI, "Lying to her about it. Not telling her the truth.");

		// 93: POST BOSS
		CreateStory(); // 382
		AddSub(StoryCharacter.ELI, "You know that she respects you SO much, she's stopped listening to some of her staff? I'm serious.");
		AddSub(StoryCharacter.ELI, "Some of her dudes are like 'let @N kill the Empress, then we destroy @N. It's the only way.'");
		AddSub(StoryCharacter.ELI, "And she's all like, 'Nah man, I'm going to make a statue in @N's honour'... haha!");
		AddSub(StoryCharacter.ELI, "Ah, jeez.");
		AddSub(StoryCharacter.ELI, "It's all a big joke. But no one knows who wrote it.");


		// SECTOR 94
		// 94: ENTER
		CreateStory(); // 383
		AddSub(StoryCharacter.ELI, "Anyway, I guess I'll shut up, now.");
		AddSub(StoryCharacter.ELI, "...");
		AddSub(StoryCharacter.ELI, "It's all set up for you, @N.");
		AddSub(StoryCharacter.ELI, "...");
		AddSub(StoryCharacter.ELI, "When you're finished with the Empress, just tell your computer to intitiate termination protocol, then tell it the password you gave me.");
		AddSub(StoryCharacter.ELI, "You'll be okay, right? @N? Say something.");
		AddSub(StoryCharacter.ELI, "...");
		AddSub(StoryCharacter.ELI, "Goodbye, @N.");

		// 94: MID
		CreateStory(); // 384
		AddSub(StoryCharacter.ADILA, "The fifth day of Auldstar, X45X.");
		AddSub(StoryCharacter.ADILA, "Today, I paid my respects and attended the funeral of Gemini Pilot @N's mother, Mrs Petra Shields.");
		AddSub(StoryCharacter.ADILA, "I met the woman some seven months ago, finally, after everythng that has happened. I was under very strict orders from @N to not approach her.");
		AddSub(StoryCharacter.ADILA, "@N believed that the truth would hurt her far more than what she came to discover through the media.");
		AddSub(StoryCharacter.ADILA, "I'm inclined to agree with @N, now, after all.");
		AddSub(StoryCharacter.ADILA, "I just wish I could have told Mrs Shields that @N's story will soon be told, in its entirety.");
		AddSub(StoryCharacter.ADILA, "We won't hold back. We will celebrate @N's every heroic deed, and think on @N's mistakes.");

		// 94: BOSS SECTOR
		CreateStory(); //  385
		AddSub(StoryCharacter.ADILA, "As I went to drop flowers by the grave, I became aware of the inscription on her headstone. It reads...");
		AddSub(StoryCharacter.ADILA, "'We are only truly gone when we are no longer thought of.'");
		AddSub(StoryCharacter.ADILA, "The words resonate with me. I wonder if @N still thinks of her.");

		// 94: POST BOSS
		CreateStory(); // 386
		AddSub(StoryCharacter.ADILA, "I long for @N to hurry up and complete the mission. So that we can move on.");
		AddSub(StoryCharacter.ADILA, "So that I can use the power I have been bestowed to give a life back to our nation's most loyal soldier.");
		AddSub(StoryCharacter.ADILA, "I long for this experience to have changed @N in a way that has not crossed the minds of my critics.");
		AddSub(StoryCharacter.ADILA, "I hope that @N will make peace with Iris. That @N will come home, so we can all support each other again the way we did when the four of us met.");
		AddSub(StoryCharacter.ADILA, "But these days... I get the feeling that I wasted all my hope in Dalton getting the boot, and me becoming President.");
		AddSub(StoryCharacter.ADILA, "...wasted all my hope on myself.");



		// SECTOR 95
		// 95: ENTER
		CreateStory(); // 387
		AddSub(StoryCharacter.EMPRESS, "I suppose it is down to me to break the silence between us, Gemini Pilot.");
		AddSub(StoryCharacter.EMPRESS, "I must admit... it was easy enough for me to not speak with you.");
		AddSub(StoryCharacter.EMPRESS, "You have taken everything from me. Everything I worked so hard for.");
		AddSub(StoryCharacter.EMPRESS, "You are but a soldier who is only trained to kill. And yet... you stole the hearts of those I loved most.");
		AddSub(StoryCharacter.EMPRESS, "My son, and my Champion. They were the mortar of my great temple.");

		// 95: MID
		CreateStory(); // 388
		AddSub(StoryCharacter.EMPRESS, "The year X932 is but a number to you.");
		AddSub(StoryCharacter.EMPRESS, "Because the normal human life span is so short, you could never comprehend the feeling of holding anger for so many years. For lifetimes.");
		AddSub(StoryCharacter.EMPRESS, "Your nation murdered my daughter. My first ever creation. And never expressed any notion of regret for it.");
		AddSub(StoryCharacter.EMPRESS, "Never...");
		AddSub(StoryCharacter.EMPRESS, "I am the only being to have lived long enough to see that ignorance being passed down from generation to generation.");
		AddSub(StoryCharacter.EMPRESS, "Until remorse has been completely diluted from the poisonous concoction that is the soul of a human.");

		// 95: PRE BOSS
		CreateStory(); // 389
		AddSub(StoryCharacter.EMPRESS, "Now ... my own son is fighting against me.");
		AddSub(StoryCharacter.EMPRESS, "I was so resistant to talking with him, that he thought it best to ally with my greatest enemy, and fake his DEATH than to keep trying to reason with me.");
		AddSub(StoryCharacter.EMPRESS, "I feel as though it need not have come this far. Despite my anger, I thought I was a ruler whose stone heart could be softened by her own family, at least.");
		AddSub(StoryCharacter.EMPRESS, "Apparently, I was not. And I continue not to be. I am plummeting further into a pit dug by my persistence.");
		AddSub(StoryCharacter.EMPRESS, "Love him as I do, he can never understand. But you...");

		// 95: POST BOSS
		CreateStory(); // 390
		AddSub(StoryCharacter.EMPRESS, "You understand why, don't you?");
		AddSub(StoryCharacter.EMPRESS, "Yes, you understand better than any other.");
		AddSub(StoryCharacter.EMPRESS, "Once you have committed your heart and soul to the destruction of a thing... an ideal...");
		AddSub(StoryCharacter.EMPRESS, "...you become engaged in a war against time. That war cannot be won.");



		// SECTOR 96
		// 96: ENTER
		CreateStory(); // 391
		AddSub(StoryCharacter.EMPRESS, "You and I might battle on for a millennia.");
		AddSub(StoryCharacter.EMPRESS, "You might snatch the life from me in the next few hours.");
		AddSub(StoryCharacter.EMPRESS, "Or perhaps you will someday run out of clones, and I will finally obliterate your kind.");
		AddSub(StoryCharacter.EMPRESS, "But whatever the result... the only true winner... is the one who gets to rest.");
		AddSub(StoryCharacter.EMPRESS, "And does not live to be forced to fight another day.");

		// 96: MID
		CreateStory(); // 392
		AddSub(StoryCharacter.PILOT, "You're right.");
		AddSub(StoryCharacter.EMPRESS, "You speak! Oh, it really is nice to hear your voice...");
		AddSub(StoryCharacter.PILOT, "My own existence has only been around for a fraction of yours. But I'm probably the only other being who understands what it is to have lived many, many lives.");
		AddSub(StoryCharacter.PILOT, "So yeah. I think know what it is to hold a grudge over a lifetime.");
		AddSub(StoryCharacter.EMPRESS, "When my Champion destroyed the Project Phoenix cloning facility, I knew it was over.");

		// 96: PRE BOSS
		CreateStory(); // 393
		AddSub(StoryCharacter.PILOT, "This thing between you and me is personal, now. I don't think it has a lot to do with the Alliance or the Empire.");
		AddSub(StoryCharacter.EMPRESS, "Yes... I think I would agree.");
		AddSub(StoryCharacter.PILOT, "I've had a lot of time to think, these past few years. I told someone I loved very much to turn her back on me.");
		AddSub(StoryCharacter.PILOT, "But see, I knew she wouldn't listen if I told her the truth.");
		AddSub(StoryCharacter.PILOT, "So I had to say some horrible, vile things to her. I had to insult her right down to her very core, so she was so filled with rage...");
		AddSub(StoryCharacter.PILOT, "...that she wouldn't see that I was just trying to stop her from following me any further down this path I'm following.");
		AddSub(StoryCharacter.PILOT, "I had to save her life, because she was always saving mine.");
		AddSub(StoryCharacter.PILOT, "I think your son wants to save your life. Will you let him?");

		// 96: POST BOSS
		CreateStory(); // 394
		AddSub(StoryCharacter.EMPRESS, "It is not he who must save my life.");
		AddSub(StoryCharacter.EMPRESS, "Like you did for your friend, I must save his.");
		AddSub(StoryCharacter.EMPRESS, "I have blamed your people for the sins of your fathers...");
		AddSub(StoryCharacter.EMPRESS, "Whereas my people are being blamed for MY sins. My son... if I am not stopped... he will be blamed for my sin.");
		AddSub(StoryCharacter.PILOT, "...");



		// SECTOR 97
		// 97: ENTER
		CreateStory(); // 395
		AddSub(StoryCharacter.PILOT, "There was a time where I would have argued with you over this for ... well, forever.");
		AddSub(StoryCharacter.PILOT, "It's clear your heart wants to change. Hell, it's clear to me it HAS changed.");

		// 97: MID
		CreateStory(); // 396
		AddSub(StoryCharacter.PILOT, "But you've been at it for too long, now.");
		AddSub(StoryCharacter.PILOT, "Something else has taken over all together. It's kinda like ... the strongest muscle memory, ever.");

		// 97: PRE BOSS
		CreateStory(); // 397
		AddSub(StoryCharacter.PILOT, "Muscles that have memorised movements for a millennia. The only way for them to stop repeating them now...");
		AddSub(StoryCharacter.PILOT, "Is to cut off those limbs.");

		// 97: POST BOSS
		CreateStory(); // 398
		AddSub(StoryCharacter.EMPRESS, "Yes. Please, come to me, Gemini Pilot. Show me the end. Turn me to ash and scatter me amongst the stars.");
		AddSub(StoryCharacter.EMPRESS, "Promise me. Promise me you will do this.");
		AddSub(StoryCharacter.PILOT, "I promise...");
		AddSub(StoryCharacter.PILOT, "And afterwards, I'll do the same to myself.");




		// 98 is BLANK



		// 99: ENTER
		CreateStory(); // 399
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.PILOT, "...");
		AddSub(StoryCharacter.PILOT, "Computer... play back audio message #743.");
		AddSub(StoryCharacter.COMPUTER, ">> AUDIO MESSAGE FOUND. PLAYING.");
		AddSub(StoryCharacter.IRIS, "I'm sorry we didn't get to save you that day.");
		AddSub(StoryCharacter.IRIS, "...and I know now that you were a complete dick afterwards because you were worried about me. You don't want me to die.");
		AddSub(StoryCharacter.IRIS, "Guess it didn't matter how many times I proved I was a better pilot than you. All that danger, and here I am... still alive.");
		AddSub(StoryCharacter.IRIS, "Wheras you... jeez, you've died more times than I've looted.");
		AddSub(StoryCharacter.IRIS, "And that's a lot.");

		// 99: MID
		CreateStory(); // 400
		AddSub(StoryCharacter.IRIS, "I guess it ain't about being the best. It's just about your destiny. ");
		AddSub(StoryCharacter.IRIS, "I dunno what mine is... but yours is to complete your mission. And I get it, now. You have to do it alone.");
		AddSub(StoryCharacter.IRIS, "I think, if I was you, I'd want to die after I destroy the Empress.");
		AddSub(StoryCharacter.IRIS, "I wouldn't have admitted that to you before. But I'm starting to understand, now.");
		AddSub(StoryCharacter.IRIS, "I spend a lot of time alone. I don't talk to Eli or Adila.");
		AddSub(StoryCharacter.IRIS, "And I've been pissed off at you long enough to hold back from sending more than this one message.");

		// 99: BOSS SECTOR
		CreateStory(); // 401
		AddSub(StoryCharacter.IRIS, "The thing about being alone is that you get reminded, in equal measures, of what you hated about someone ... and what you...");
		AddSub(StoryCharacter.IRIS, "...what you DIDN'T hate about someone.");
		AddSub(StoryCharacter.IRIS, "And that's enough to drive you nuts, out here in the great starry vaccum.");
		AddSub(StoryCharacter.IRIS, "I've kind of started to forget how we've even got to where we are now.");
		AddSub(StoryCharacter.IRIS, "I'm just propelled by my thoughts. Good and bad.");
		AddSub(StoryCharacter.IRIS, "Anyway. I need change. Everyone else has had change. I guess I'm jealous.");

		// 99: POST BOSS
		CreateStory(); // 402
		AddSub(StoryCharacter.IRIS, "So... this is my goodbye. I'm going to piss off somewhere different. I'm not going to let anyone from the past connect with my COMMS.");
		AddSub(StoryCharacter.IRIS, "That includes you. No getting in touch.");
		AddSub(StoryCharacter.IRIS, "Not if you're just a ghost.");
		AddSub(StoryCharacter.IRIS, "Only if you're someone new.");
		AddSub(StoryCharacter.IRIS, "...whatever.");
		AddSub(StoryCharacter.IRIS, "Bye...");
		AddSub(StoryCharacter.IRIS, "...");
		AddSub(StoryCharacter.IRIS, "...bye, @N.");


		// SECTOR 100
		// 100: ENTER
		CreateStory(); // 403
		AddSub(StoryCharacter.ADILA, "Brothers and sisters of the Alliance Republic.");
		AddSub(StoryCharacter.ADILA, "Today, the war has ended.");
		AddSub(StoryCharacter.ADILA, "Gemini Pilot @N, with the experience of defying life and death every single day for over a decade, has finally reached our enemy.");
		AddSub(StoryCharacter.ADILA, "And shall now deliver justice.");

		// 100: MID
		CreateStory(); // 404
		AddSub(StoryCharacter.ELI, "Don't hate me, Adila. Just try to understand.");
		AddSub(StoryCharacter.ADILA, "UNDERSTAND?! Everything @N has done for us counts for NOTHING if you just let @N die! I can't believe you did this!");
		AddSub(StoryCharacter.ELI, "What am I supposed to do, huh?! Tell @N I'm not going to help?");
		AddSub(StoryCharacter.ADILA, "Yes!");
		AddSub(StoryCharacter.ELI, "I couldn't do that, ok?!");
		AddSub(StoryCharacter.ADILA, "...");
		AddSub(StoryCharacter.ELI, "...but I did do SOMETHING...");
		AddSub(StoryCharacter.ADILA, "...something?");
		AddSub(StoryCharacter.ELI, "I gave @N some more time to think.");
		AddSub(StoryCharacter.ELI, "Cuz even when you think it's the end... you can never have too much time to think.");

		// 100: PRE BOSS
		CreateStory(); // 405
		AddSub(StoryCharacter.EMPRESS, "I am trembling.");
		AddSub(StoryCharacter.EMPRESS, "The muscle memory you spoke of continues relentlessly, but my mind knows that it is over.");
		AddSub(StoryCharacter.EMPRESS, "It is not death that makes me tremble. It is knowing that my anger - my drive - my mission... which has lasted an age...");
		AddSub(StoryCharacter.EMPRESS, "...will come to an end.");
		AddSub(StoryCharacter.PILOT, "Rest easy, Empress. Change is here. We may have unjustly forgotten your daughter so many years ago, but we will never forget you.");

		// 100: POST BOSS
		CreateStory(); // 406
		AddSub(StoryCharacter.EMPRESS, "Yes... You will never forget me.");
		AddSub(StoryCharacter.EMPRESS, "You will never forget... that I did it all... for my children...");
		AddSub(StoryCharacter.EMPRESS, "* BOOM! *");


		// CHAPTER 22 PAROLE ONENTERCHAPTER ADDITION ******
		CreateStory (); // To be inserted between 138 and 139
		AddSub (StoryCharacter.BRIGHTMAN, "Three months since @N took down the Crown Prosecutor and NOTHING?");
		AddSub (StoryCharacter.ENGINEER, "Not exactly, sir. They continue to upgrade their ships. Elite units are bigger and badder. Their weaponry is stronger.");
		AddSub (StoryCharacter.BRIGHTMAN, "I don't care about their god damn arsenal, Engineer Ren. That'll never stand up to the punch we pack. I want the Upper Hierarchy. That's it.");
		AddSub (StoryCharacter.ENGINEER, "Yes sir. Still no signs of the Upper Hierarchy, sir.");
		AddSub (StoryCharacter.BRIGHTMAN, "Don't let @N slack. You hear me? You get him to get out there and FIND them.");
		AddSub(StoryCharacter.ENGINEER, "Yes sir.");








		// XX: CHAPTER
		CreateStory(); // X
		AddSub(StoryCharacter.PILOT, "");
		AddSub(StoryCharacter.PILOT, "");
		AddSub(StoryCharacter.PILOT, "");
		AddSub(StoryCharacter.PILOT, "");
		AddSub(StoryCharacter.PILOT, "");
		AddSub(StoryCharacter.PILOT, "");
		AddSub(StoryCharacter.PILOT, "");

		// XX: CHAPTER
		CreateStory(); // X
		AddSub(StoryCharacter.ADILA, "");
		AddSub(StoryCharacter.ADILA, "");
		AddSub(StoryCharacter.ADILA, "");
		AddSub(StoryCharacter.ADILA, "");
		AddSub(StoryCharacter.ADILA, "");
		AddSub(StoryCharacter.ADILA, "");
		AddSub(StoryCharacter.ADILA, "");




//		Debug.Log ("Init Story");
	}

	public static Story At (int index)
	{
		return storyList[index];
	}

	public static void CreateStory (string name = "Story Name")
	{
		story = new Story(name);
		story.id = storyList.Count;
		storyList.Add (story);
	}

	public static void AddSub (StoryCharacter character, string contentLine, int creditGain = 0, int gemGain = 0)
	{
		story.Add (new StoryPair(character, contentLine, creditGain, gemGain));
	}
}
