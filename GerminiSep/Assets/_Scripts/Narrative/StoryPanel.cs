using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoryPanel : KJBehavior {
	
	public UILabel textLabel;
	public UISprite characterImage;
	public UILabel characterName;

	int blogCount  ;
	int charCount;
	List<StoryPair> storyList ;
	Story story ;
	string currentBlogString;
	bool isPuaseForNewBlog;
	bool isEndingBlog;
	
	public void Deploy ( Story story )
	{
		TimeSpaceType = KJTime.SpaceType.System;

		this.storyList = story.list;
		this.story = story;
		blogCount = 0 ;
		charCount = 0 ;
		UpdateStory();

		//	AddTimer(UpdateStory,1.0f,storyList.Count);
		
		gameObject.SetActive(true);
	}

	void UpdateStory ()
	{
		isEndingBlog = false;
		if(blogCount == storyList.Count)
		{
			KJCanary.PlaySoundEffect("BackButton");
			UIController.ClosePopUp();
			return;
		}

		StoryPair storyPair = storyList[blogCount];

		if (storyPair.gemGain > 0) { 
			ProfileController.Gems += storyPair.gemGain;
			UIController.UpdateCurrency();
		}

		if (storyPair.creditGain > 0) {
			ProfileController.Credits += storyPair.creditGain;
			UIController.UpdateCurrency();
		}

		textLabel.text = "";
		textLabel.color = storyPair.character.textColor;

		if (storyPair.character.name == "@N") {
			KJCanary.PlaySoundEffect("CutSend");
		} else {
			KJCanary.PlaySoundEffect("CutGet");
		}

		if (storyPair.character == StoryCharacter.ENGINEER) {
			if (ProfileController.Singleton.narrativeProgress > 197) {
				storyPair.character = StoryCharacter.ELI;
			}
		}

		characterName.text = storyPair.character.name.Replace("@N", ProfileController.DisplayName).ToUpper();
		characterImage.spriteName = storyPair.character.spriteName;
		characterImage.MakePixelPerfect();

		currentBlogString = storyPair.contentLine;
		currentBlogString = currentBlogString.Replace("@NN", ProfileController.DisplayName.ToUpper());
		currentBlogString = currentBlogString.Replace("@N", ProfileController.DisplayName);

		currentBlogString = ReplaceCurrencyText(currentBlogString);

		charCount = 1 ;
		AddTimer(Speech);


		
		
		blogCount++;
		
	}

	const string CreditToken = "$";

	string ReplaceCurrencyText (string inputText)
	{
		if (string.IsNullOrEmpty(inputText)) return inputText;

		string returnText = inputText;

		while (returnText.Contains(CreditToken))
		{
			int startIndex = -1, endIndex = -1, subIndex = -1;
			string newString, tokenString;
			bool isGem = false;

			startIndex = returnText.IndexOf(CreditToken);
			string subString = returnText.Substring(startIndex);

			// Find the SubIndex
			int cIndex = subString.IndexOf("C");
			int gIndex = subString.IndexOf("G");
//			Debug.Log ("C" + cIndex + "   G"+gIndex);

			if ((cIndex < gIndex || gIndex == -1) && cIndex != -1) {
				isGem = false;
				subIndex = cIndex;
			}

			if ((gIndex < cIndex || cIndex == -1) && gIndex != -1) {
				isGem = true;
				subIndex = gIndex;
			}

			if (subIndex != -1) {
				endIndex = startIndex + subIndex;
				tokenString = subString.Substring(0, subIndex + 1);
			} else {
				tokenString = CreditToken;
			}

			newString = tokenString;

			// Change the Credit Word
			if (subIndex != -1) {

				string miniReplaceToken = isGem ? "G" : "C";
				string miniReplaceString = "";
				bool isMoreThanOne = false;

				if (subIndex > 1) miniReplaceString += " ";
				miniReplaceString += isGem ? "Gem" : "Credit";


				if (subIndex > 1) {

					string numberValueString = newString.Substring(1, subIndex - 1);
					int numberValue = 0;
					int.TryParse(numberValueString,  out numberValue);
					if (numberValue > 1) isMoreThanOne = true;

					if (isMoreThanOne) miniReplaceString += "s";

				} else {
					miniReplaceString += "s";
				}


//				Debug.Log (miniReplaceToken + " : " + miniReplaceString);
				newString = newString.Replace(miniReplaceToken, miniReplaceString);
				newString += "[-]";
			}

			// Change Colors
			if (isGem) {
				newString = "[80E7FF]" + newString;
			} else {
				newString = "[FFD264]" + newString;
			}
			
			newString = newString.Replace(CreditToken, ""); // Take out the Token
//			Debug.Log (tokenString + " : " + newString);

			returnText = returnText.Replace(tokenString, newString);
//			returnText = returnText.Replace(CreditToken, "");


		}

		return returnText;
	}


	void Speech ()
	{
		string finalText = currentBlogString.Substring(0,charCount);

//		int remainingChar = currentBlogString.Length - charCount;
//
//		while (remainingChar > 0) {
//			if (currentBlogString.
//			finalText += " ";
//			remainingChar --;
//		}

		textLabel.text = finalText; //currentBlogString.Substring(0,charCount);
		if(charCount ==  currentBlogString.Length && !isEndingBlog )
		{
			//	Debug.Log("complete with "+ textLabel.text);
			OnEndSpeech();
			isPuaseForNewBlog = true;
			//UpdateStory();
		}
		else
		{
			string charEnd = "";
			if (charCount < currentBlogString.Length) charEnd = currentBlogString.Substring(charCount, 1);
			bool needLoop = false;

			if (charEnd == "[") {
				needLoop = true;
				while (needLoop) {
					charCount++;
					if (currentBlogString.Substring(charCount - 1, 1) == "]")
						needLoop = false;
				}
			} else {
				charCount++;

			}
		}
	}
	
	void OnEndSpeech ()
	{
		if (storyList[0].tutorialId != -1) TutorialData.PostDeploy(storyList[0].tutorialId);
		RemoveTimerOrFlash(Speech);
	}
	
	void OnTap ()
	{
		if(isPuaseForNewBlog)
		{
			UpdateStory();
			isPuaseForNewBlog = false;
		}
		else {
			OnEndSpeech();
			isEndingBlog = true;
			charCount = currentBlogString.Length;
			Speech();
			UpdateStory();
			isPuaseForNewBlog = false;
		}
	}

	void OnSkip ()
	{
		OnEndSpeech();
		UIController.ClosePopUp();
	}

}
