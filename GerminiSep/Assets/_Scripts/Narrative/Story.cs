﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Story {

	public List<StoryPair> list;
	public int id;
	string name;

	public Story (string name)
	{
		this.name = name;
		list = new List<StoryPair>();
	}

	public void Add (StoryPair storyPair)
	{
		list.Add (storyPair);
	}

	public static bool storyDeployed = false;

	public static void DeployStory (int id)
	{
		// Check if player has seen or is past this story ID:
		Debug.Log ("Show Story: " + id + " // Nar Pro: " + ProfileController.Singleton.narrativeProgress);

		if ( ProfileController.Singleton.narrativeProgress < id && !ProfileController.secretMission && !ProfileController.secretBoss && !ProfileController.secretRaid)
		{
			storyDeployed = true;
			ProfileController.Singleton.narrativeProgress = id;
			UIController.ShowStory(id);
		}
	}
}
