﻿using UnityEngine;
using System.Collections;

public class StoryCharacter {

	
	public static StoryCharacter BRIGHTMAN;
	public static StoryCharacter ENGINEER;
	public static StoryCharacter IRIS;
	public static StoryCharacter CROWN;
	public static StoryCharacter VICEROY;
	public static StoryCharacter MOTHER;
	public static StoryCharacter PILOT;
	public static StoryCharacter COMPUTER;
	public static StoryCharacter HIDDENIRIS;
	public static StoryCharacter IMPERIALPILOT;
	public static StoryCharacter ROYALENGINEER;
	public static StoryCharacter MEDIC;
	public static StoryCharacter PRINCE;
	public static StoryCharacter EMPRESS;

	public static StoryCharacter ADILA;
	public static StoryCharacter ELI;
	public static StoryCharacter DALTON;
	public static StoryCharacter FAKE;
	public static StoryCharacter CHAMPION;
	public static StoryCharacter REAL;

	private static bool hasBeenInit = false;

	public static void Initialize ()
	{
		if (hasBeenInit) return;
		hasBeenInit = true;

		Color32 standardColor = new Color32 (195, 255, 175, 255);
		Color32 enemyColor = new Color32 (255, 160, 160, 255);
		Color32 pirateColor = new Color32 (255, 220, 150, 255);
		Color32 comColor = new Color32 (120, 255, 70, 255);
		Color32 selfColor = new Color32 (150, 225, 255, 255);


		ENGINEER = new StoryCharacter("Engineer Ren", "Engineer", standardColor);
		IRIS = new StoryCharacter("Iris", "Iris", pirateColor);
		HIDDENIRIS = new StoryCharacter("????", "Iris", pirateColor);
		CROWN = new StoryCharacter("Crown Prosecutor", "Crown", enemyColor);
		BRIGHTMAN = new StoryCharacter("Major Brightman", "Major", standardColor);
		PILOT = new StoryCharacter("@N", "Pilot", selfColor);
		COMPUTER = new StoryCharacter("Computer", "Computer", comColor);
		MOTHER = new StoryCharacter("Mother", "Mother", standardColor);
		VICEROY = new StoryCharacter("Viceroy", "Viceroy", enemyColor);
		IMPERIALPILOT = new StoryCharacter("Imperial Pilot", "Imperial", enemyColor);
		ROYALENGINEER = new StoryCharacter("Royal Engineer", "Imperial", enemyColor);
		PRINCE = new StoryCharacter("Prince", "Prince", enemyColor);
		EMPRESS = new StoryCharacter("Empress", "Queen", enemyColor);
		MEDIC = new StoryCharacter("Doctor Grace", "Medic", standardColor);

		ADILA = new StoryCharacter("Adila", "Adila", standardColor);
		ELI = new StoryCharacter("Eli", "Hacker", standardColor);
		DALTON = new StoryCharacter("President Dalton", "President", standardColor);
		FAKE = new StoryCharacter("@N", "Red", enemyColor);
		CHAMPION = new StoryCharacter("Imperial Champion", "Champion", enemyColor);
		REAL = new StoryCharacter("Captain @N", "Real", standardColor);

	}

	public string name = "Unnamed Character";
	public string spriteName = "Character1";
	public Color textColor = Color.white;

	public StoryCharacter (string name, string spriteName, Color32 textColor)
	{
		this.name = name;
		this.spriteName = spriteName;
		this.textColor = textColor;
	}


}
