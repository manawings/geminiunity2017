﻿using UnityEngine;
using System.Collections;

public class TutorialPrompter : MonoBehaviour {

	public KJNGUIButtonFunction button;
	public UISprite glowSprite;
	public Color glowColor;
	public int tutorialId;
	public TutorialTapHere tapHere;	

	public float yOffset = 20.0f, moveRange = 10.0f, delay = 0.5f;

	public bool manualDeploy = false;
	public bool hideTapHere = false;

	void Start () {
		if (!manualDeploy) {

			if (!MenuController.HasTutorialPrompt && !TutorialData.HasCompleted(tutorialId) && TutorialData.CheckTutorial(tutorialId)) {

				if (delay == 0) {
					DeployTutorial();
				} else {
					InputController.LockControls();
					KJTime.Add (DeployTutorial, delay, 1);
				}

				MenuController.HasTutorialPrompt = true;

			}
		}
	}

	public void DeployTutorial ()
	{
		TutorialData.Deploy(tutorialId, this);
	}

	public void PostExecute ()
	{
		if (button != null) {
			InputController.LockControlsExceptFor(button);
			if (tapHere != null) button.tapHere = tapHere;
			if (tapHere != null) tapHere.Deploy(button.gameObject, glowSprite, glowColor, yOffset, moveRange, hideTapHere);
		}
	}
}
