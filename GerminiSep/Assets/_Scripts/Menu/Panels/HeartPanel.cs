using UnityEngine;
using System.Collections;

public class HeartPanel : KJBehavior
{

	public HeartIcon[] heartIcons;
	public UILabel refillLabel;
	
	void Start ()
	{
		refillLabel.text = "";
//
//		ProfileController.LoseHeart();
//		ProfileController.LoseHeart();
//		ProfileController.LoseHeart();
//		ProfileController.LoseHeart();

		if (ProfileController.RefillMarkedTime > System.DateTime.UtcNow.Ticks) {
			float timeDifferenceInSec = (float)((ProfileController.RefillMarkedTime - System.DateTime.UtcNow.Ticks) / 10000000 );
			//int timeDifferenceInSec = System.TimeSpan.FromTicks(ProfileController.RefillMarkedTime - System.DateTime.UtcNow.Ticks).Seconds;
			// Debug.Log("Time Diff; " + timeDifferenceInSec);
			// Debug.Log("Heart Minus: " + Mathf.Ceil(timeDifferenceInSec / ProfileController.SecondsPerRefill));
			ProfileController.Hearts = (ProfileController.MaxHearts - (int) Mathf.Ceil(timeDifferenceInSec / ProfileController.SecondsPerRefill));

#if UNITY_IOS

			if (ProfileController.Hearts == 0) {
//				EtceteraTwoBinding.cancelAllLocalNotifications();
//				EtceteraTwoBinding.scheduleLocalNotification(((int) timeDifferenceInSec), "Your pilots are fully restored. Ready for action!", "Play");
			}
			// int secondsUntilTomorrow = 60 * 60 * 24;
			// if (!ProfileController.Singleton.hasGoldReward) EtceteraTwoBinding.scheduleLocalNotification(secondsUntilTomorrow, "You have been awarded a Gold Credit. Come and collect it!", "Play", 1, string.Empty, string.Empty);
#endif


			// Debug.Log("Hearts: " + ProfileController.Hearts);
		}
		if (ProfileController.Hearts < ProfileController.MaxHearts) {
			AddTimer(Tick, 1.0f);
			Tick ();
		}
		//Debug.Log(ProfileController.Hearts);
		SetCurrentHearts ();
	}
	
	void SetCurrentHearts ()
	{
		for ( int i = 0 ; i < heartIcons.Length ; i ++ )
		{
			HeartIcon heart = heartIcons[i];
			if (ProfileController.Hearts > i) {	
				heart.SetToFull();	
			} else {
				heart.SetToEmpty();
			}
		}
	}
	
	void Tick ()
	{

		// Ticks every second to check how much time is left until refill.
		// print ("ProfileController.RefillMarkedTime: " + ProfileController.RefillMarkedTime);
		// print ("2: " + (System.DateTime.UtcNow.Ticks + ProfileController.TicksPerRefill + (ProfileController.TicksPerRefill * (ProfileController.MaxHearts - ProfileController.Hearts))));

		long timeDifferenceInTicks = ProfileController.RefillMarkedTime - (System.DateTime.UtcNow.Ticks + ProfileController.HeartTimeAdjuster);
			// (System.DateTime.UtcNow.Ticks + ProfileController.TicksPerRefill + (ProfileController.TicksPerRefill * (ProfileController.MaxHearts - ProfileController.Hearts)));

		if (ProfileController.RefillMarkedTime == 0) timeDifferenceInTicks = 0;

		//Debug.Log(timeDifferenceInTicks);
		if (timeDifferenceInTicks <= 0) 
		{
			timeDifferenceInTicks = 0;
			ProfileController.GainHeart();
			RefillHeart();
			
			if (ProfileController.Hearts == ProfileController.MaxHearts) {
				refillLabel.text = "";
				RemoveAllTimers();
			}
			
		} else {
			
			int refillMinutes = System.TimeSpan.FromTicks(timeDifferenceInTicks).Minutes;
			int refillSeconds = System.TimeSpan.FromTicks(timeDifferenceInTicks).Seconds;
			
			string refillMinutesString = (refillMinutes < 10 && refillMinutes > 0 ? "0" + refillMinutes : refillMinutes.ToString());
			string refillSecondsString = (refillSeconds < 10 && refillSeconds > 0 ? "0" + refillSeconds : refillSeconds.ToString());
			
			if (refillMinutesString == "0") refillMinutesString = "00";
			if (refillSecondsString == "0") refillSecondsString = "00";
			
			refillLabel.text = "Refill in "+ refillMinutesString +":"+ refillSecondsString;
			
		}
	}
	
	void RefillHeart ()
	{
		for ( int i = 0 ; i < heartIcons.Length ; i ++ )
		{
			HeartIcon heart = heartIcons[i];
			if (ProfileController.Hearts > i) {	
				heart.Refill();
				DataController.SaveAllData();
			}
		}
	}

	void BuyHearts ()
	{
		if (ProfileController.Hearts == 4) {
			UIController.ShowSingleClose("PILOTS READY", "Lives are fully restored. All Gemini Pilots standby for action!");
		} else {
			UIController.ShowPopupBuyHeart();
		}
	}

	void RestoreHearts ()
	{
		ProfileController.RestoreHeart();

		for ( int i = 0 ; i < heartIcons.Length ; i ++ )
		{
			HeartIcon heart = heartIcons[i];
			heart.Refill();

		}


	}
}

