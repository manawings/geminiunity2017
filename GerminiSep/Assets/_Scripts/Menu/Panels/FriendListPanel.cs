using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FriendListPanel : MonoBehaviour {

	public GameObject itemPlatePrefab;
	public GameObject currentItemPlate;
	public UILabel listLabel;
	
	int maxItemPlates;
	
	private const float panelSize = 50.0f;
	private const float extendedPanelSize = 24.0f;
	private const float panelGap = 10.0f;
	
	float topCutOff, topOrigin, bottomCutOff, oldClipPos;
	
	UIDraggablePanel uiDragPanel;
	UIPanel uiPanel;
	
	List<FriendButton> itemButtonList = new List<FriendButton>();
	List<FriendItem> proxyList;
	
	void Awake ()
	{
		uiPanel = GetComponent<UIPanel>();
		uiDragPanel = uiPanel.GetComponent<UIDraggablePanel>();
		
		float heightFactor = 1.0f; // tk2dCamera.Instance.ScreenExtents.height / 480.0f;
		Vector4 newClipRange = uiPanel.clipRange;
		newClipRange.w = newClipRange.w * heightFactor;
		uiPanel.clipRange  = newClipRange;
	}
	
	void Update () {
		
		float moveDifference = uiPanel.clipRange.y - oldClipPos;
		
		if (moveDifference > 0) {
			CheckPlateBottom();
		} else if (moveDifference < 0) {
			CheckPlateTop();
		}
		
		oldClipPos = uiPanel.clipRange.y;
	}
	
	
	void CreateItemPlates ()
	{
		/// Math.
		
		maxItemPlates = 2 + (int) Mathf.Ceil(uiPanel.clipRange.w / (panelSize + panelGap));
		topOrigin = ((uiPanel.clipRange.w - panelSize - extendedPanelSize) * 0.5f) - uiPanel.clipSoftness.y;
		topCutOff = ((uiPanel.clipRange.w + panelSize + extendedPanelSize) * 0.5f) + panelGap;
		bottomCutOff = -topCutOff;
		oldClipPos = 0.0f;
		
		/// Clear the list.
		
		itemButtonList.Clear();
		
		for (int i = 0 ; i < maxItemPlates ; i ++ )
		{
			
			/// Create and physically put the plate into position.
			
			GameObject newItemPlate;
			
			newItemPlate = (GameObject) Instantiate(itemPlatePrefab);
			
			newItemPlate.transform.parent = transform;
			newItemPlate.transform.localScale = Vector3.one;
			newItemPlate.transform.localPosition = Vector3.zero;
			
			float newYPosition = topOrigin - (panelGap + panelSize) * i;
			// if (i != 0) newYPosition -= extendedPanelSize * 0.5f;
			KJMath.SetY(newItemPlate.transform, newYPosition);
//			
			
			FriendButton itemButton = newItemPlate.GetComponent<FriendButton>();
			itemButton.dragPanelContent.draggablePanel = uiDragPanel;
			itemButtonList.Add(itemButton);
			itemButton.Clear();
			
		}
	}


//	IEnumerator LoadImage()
//	{
//		
//		string url = "";
//		FriendItem friendItem = SocialController.allFacebookFriendList[0];
//		url = friendItem.profilePicURL;
//		WWW www = new WWW(url);
//		
//		yield return www;
//		
//		SocialController.FriendListType = SocialController.ListType.Facebook;
//		friendButton.SetItem(friendItem);
//		tempFBTexture.mainTexture = www.texture;
//		friendButton.SetFBTexture(www.texture);
//		Debug.Log ("Load Complete PIC");
//		
//	}

	IEnumerator LoadImage()
	{
		for (int i = 0 ; i < SocialController.allFacebookFriendList.Count ; i ++ )
		{
			FriendItem friendItem = SocialController.allFacebookFriendList[i];
			string url = friendItem.profilePicURL;

			WWW www = new WWW(url);
			// Debug.Log(SocialController.allFacebookFriendList.Count);
			yield return www;

			friendItem.fbTexture = www.texture;
			foreach (FriendButton friendButton in itemButtonList)
			{
				if (friendButton.friendItem == friendItem) friendButton.SetItem(friendItem);
			}
		}
	}

//	void LoadImagesIntoItems ()
//	{
//		for (int i = 0 ; i < SocialController.allFacebookFriendList.Count ; i ++ )
//		{
//			FriendItem friendItem = SocialController.allFacebookFriendList[i];
//			friendItem
//		}
//	}

	public void Refresh ()
	{
		List<FriendItem> copyList = new List<FriendItem>();
		listLabel.text = string.Empty;

		if (ProfileController.Singleton.isOffline) {
			if (itemButtonList.Count == 0) CreateItemPlates();
			SocialController.friendListRequest = new List<FriendItem>();
			foreach (FriendItem friendItem in SocialController.friendListRequest)
			{
				copyList.Add (friendItem);
			}

			int totalHelepd = ProfileController.HelpedFriends + ProfileController.HelpedStrangers;
			if (totalHelepd > 0) {

				FriendItem rewardItem = new FriendItem();
				rewardItem.status = FriendItem.FriendStatus.FriendReward;
				rewardItem.rewardPoints = totalHelepd;
				rewardItem.rewardAmount = totalHelepd * 10;
				copyList.Add(rewardItem);
			}

			string daySession = NistTime.DaySession(1);
			Debug.Log(daySession);
			if (daySession != PlayerPrefs.GetString(ProfileController.keyDaySession)) {
				ProfileController.Singleton.hasGoldReward = true;
			} else {
				ProfileController.Singleton.hasGoldReward = false;
			}
			if (ProfileController.Singleton.hasGoldReward) {

				FriendItem rewardItem = new FriendItem();
				rewardItem.status = FriendItem.FriendStatus.GoldReward;
				rewardItem.rewardPoints = 1;
				rewardItem.rewardAmount = 1;
				copyList.Add(rewardItem);
			}
			if (copyList.Count == 0) listLabel.text = "Your Inbox is empty.";

			proxyList = new List<FriendItem>();

			for (int i = 0 ; i < copyList.Count ; i ++ )
			{

				// Add it to the list normally.
				proxyList.Add(copyList[i]);

			}

			Debug.Log("itemButtonList.Count" + itemButtonList.Count);
			for (int i = 0 ; i < itemButtonList.Count ; i ++ )
			{

				FriendButton itemButton = itemButtonList[i];
				itemButton.SetCurrentList(proxyList);
				float newYPosition = topOrigin - (panelGap + panelSize) * i;
				KJMath.SetY(itemButton.transform, newYPosition);
				itemButton.SetItem(i);

			}

			uiDragPanel.ResetPosition();

			return;
		}

		//below for online 
		if (itemButtonList.Count == 0) CreateItemPlates();

		switch (SocialController.FriendListType)
		{
			case SocialController.ListType.Friends:
				copyList = SocialController.friendListConfirmed;
				if (copyList.Count == 0) listLabel.text = "Your Friend List is empty.";
				break;

			case SocialController.ListType.Facebook:
				copyList = SocialController.allFacebookFriendList;
				if (copyList.Count == 0) listLabel.text = "Your Friend List is empty.";
				StartCoroutine(LoadImage());
				break;

			case SocialController.ListType.Inbox:

				foreach ( FriendItem friendItem in SocialController.friendListRequest)
				{
					copyList.Add (friendItem);
				}

			int totalHelepd = ProfileController.HelpedFriends + ProfileController.HelpedStrangers;
				if (totalHelepd > 0) {

					FriendItem rewardItem = new FriendItem();
					rewardItem.status = FriendItem.FriendStatus.FriendReward;
					rewardItem.rewardPoints = totalHelepd;
					rewardItem.rewardAmount = totalHelepd * 10;
					copyList.Add(rewardItem);
				}

				if (ProfileController.Singleton.hasGoldReward) {
					
					FriendItem rewardItem = new FriendItem();
					rewardItem.status = FriendItem.FriendStatus.GoldReward;
					rewardItem.rewardPoints = 1;
					rewardItem.rewardAmount = 1;
					copyList.Add(rewardItem);
				}
				if (copyList.Count == 0) listLabel.text = "Your Inbox is empty.";
				break;

			case SocialController.ListType.Sent:
				
				foreach ( FriendItem friendItem in SocialController.friendListSent)
				{
					copyList.Add (friendItem);
				}
				if (copyList.Count == 0) listLabel.text = "Your Request list is empty.";

				break;
		}


		

		
		proxyList = new List<FriendItem>();
		
		for (int i = 0 ; i < copyList.Count ; i ++ )
		{

			// Add it to the list normally.
			proxyList.Add(copyList[i]);
			
		}
		
		for (int i = 0 ; i < itemButtonList.Count ; i ++ )
		{
			
			FriendButton itemButton = itemButtonList[i];
			itemButton.SetCurrentList(proxyList);
			float newYPosition = topOrigin - (panelGap + panelSize) * i;
			KJMath.SetY(itemButton.transform, newYPosition);
			itemButton.SetItem(i);

		}
		
		uiDragPanel.ResetPosition();
	}
	
	void CheckPlateTop ()
	{
		FriendButton topItem = itemButtonList[1];
		FriendButton endItem = itemButtonList[itemButtonList.Count - 1];
		
		if (endItem.itemId == proxyList.Count - 1) return;
		
		if (topItem.transform.localPosition.y - uiPanel.clipRange.y > topCutOff)
		{
			
			
			float newY = endItem.transform.localPosition.y - panelGap - panelSize;
			topItem.SetItem(endItem.itemId + 1);
			
			itemButtonList.RemoveAt(1);
			itemButtonList.Add(topItem);
			KJMath.SetY(topItem, newY);
		}
	}
	
	void CheckPlateBottom ()
	{
		FriendButton bottomItem = itemButtonList[itemButtonList.Count - 1];
		FriendButton endItem = itemButtonList[1];
		
		if (endItem.itemId == 1) return;
		
		if (bottomItem.transform.localPosition.y - uiPanel.clipRange.y < bottomCutOff)
		{
			
			float newY = endItem.transform.localPosition.y + panelGap + panelSize;
			bottomItem.SetItem(endItem.itemId - 1);
			
			itemButtonList.RemoveAt(itemButtonList.Count - 1);
			itemButtonList.Insert(1, bottomItem);
			KJMath.SetY(bottomItem, newY);
		}
	}

}
