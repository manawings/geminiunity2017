﻿using UnityEngine;
using System.Collections;

public class ShipPreviewPanel : MonoBehaviour {

	public UILabel itemName;
	public UILabel itemType;
	public UILabel itemDesc;
	
	public BlitzButton greenButton;
	public BlitzButton greyButton;
	public BlitzButton blueButton;
	public BlitzButton previewButton;
	
	public UISprite previewIcon;
	
	int itemId;
	ItemShip item;


	public void Calibrate (ItemShip item)	
	{
		
		this.item = item;
		itemId = item.id;

		string[] rarityName = new string[]{"", "Standard", "Custom", "Prototype", "Classified"};
		
		itemName.text = item.Name.ToUpper();
		itemType.color = item.TextColor;
		itemType.text = (rarityName[ (int) item.rarity ] + " " + item.shipType).ToUpper() + " CLASS";

		previewIcon.spriteName = item.shipClip.Replace("ps_", "p") + "Off";
		previewIcon.MakePixelPerfect();
		// upgradeBar.SetProgress(item.upgradeLevel / 5.0f, true);
		
		// TODO: Temp. Make this prettier and simpler.
		
		SetItemDesc();
		
		if (ProfileController.IsShipUnlocked(item.id)) // ProfileController.HasItem(item)
		{
			blueButton.gameObject.SetActive(false);
			
			if (ProfileController.ShipId == item.id) {
				
				greenButton.gameObject.SetActive(false);
				greyButton.gameObject.SetActive(true);
				previewButton.gameObject.SetActive(false);
				
			} else {
				
				greenButton.gameObject.SetActive(true);
				greyButton.gameObject.SetActive(false);
				previewButton.gameObject.SetActive(false);
				
			}
			
		} else {

			greenButton.gameObject.SetActive(false);
			blueButton.gameObject.SetActive(true);
			greyButton.gameObject.SetActive(false);
			previewButton.gameObject.SetActive(true);

			previewButton.defaultPrice = 150;
			previewButton.SetOptions();

			if (item.isSpecialShip) {
				blueButton.isCoreCurrency = true;
				blueButton.SetTealColor();
			} else {
				blueButton.isCoreCurrency = false;
				blueButton.SetLightYellowColor();
			}

			blueButton.defaultPrice = item.cost;
			blueButton.SetOptions();
			
		}
	}
	
	void SetItemDesc ()
	{
		string descText = string.Empty;
		descText += item.shipDescription;


		if (item.moduleId != 0) {
			Module module = ModuleLibrary.GetFromId(item.moduleId);		
			module.Calibrate(1.0f, 1.0f);	
//			Debug.Log (module.Description);
		}

		if (item.specialDescription != "") {



	
			descText += "\n";
			descText += "\n";

			descText += "[89FF52]" + item.specialDescription + "[-]"; 

		}

		itemDesc.text = descText;
		
	}

	
	
	public void ClickUpgrade ()
	{
		
//		Debug.Log("Click Upgrade!");
		
	}

	public void ClickPreview () {

		// Click to Preview a Ship
		Debug.Log ( "Click Preview" );

		UIController.ShowPopupPreview (item);

//		if (ProfileController.Gems >= 250)
//		{
//			UIController.ShowPopupUnlockShip(item);
//		} else {
//			UIController.ShowPopupNoCredits();
//		}

	}

	public void ExecutePreview () {

	}
	
	public void ClickEquip ()
	{
		
//		Debug.Log("Click Equip / Unequip!");
		
		ProfileController.EquipShip(item);
		ShipMenuController.Singleton.GoBackToShip(true);
//		DataController.SaveAllData();
		
	}
	
	public void ClickBuy ()
	{
		if (!item.isSpecialShip && ProfileController.Credits < item.cost)
		{
			UIController.ShowPopupNoCredits();
		} else if (item.isSpecialShip && ProfileController.ShipParts < item.cost) {
			UIController.ShowPopupNoCores();
		} else {
			UIController.ShowPopupUnlockShip(item);
		}
	}
	
	void DelayedCalibrate ()
	{
		Calibrate(item);
		InputController.UnlockControls();
	}
	
	void DelayedControlLock ()
	{
		InputController.LockControls();
	}
}
