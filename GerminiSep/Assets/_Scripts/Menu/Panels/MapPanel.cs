using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapPanel : MonoBehaviour {
	
	
	public GameObject mapIconPrefab;
	public GameObject mapLinePrefab;

	public GameObject currentMapIcon;
	public GameObject navPanel;

	int maxMapIcons;
	
	private const float panelSize = 50.0f;
	private const float extendedPanelSize = 24.0f;
	private const float panelGap = 10.0f;

	// Panel Moving
	float panelExcess; // Length from middle of panel to top of panel visibility.
	public float panelMaxY, panelMinY;
	
	float topCutOff, topOrigin, bottomCutOff, oldClipPos;
	
	UIDraggablePanel uiDragPanel;
	UIPanel uiPanel;
	
	List<MapIcon> mapIconList = new List<MapIcon>();
	List<ItemBasic> proxyList;
	
	void Awake ()
	{
		uiPanel = GetComponent<UIPanel>();
		uiDragPanel = uiPanel.GetComponent<UIDraggablePanel>();
		
		float heightFactor = tk2dCamera.Instance.ScreenExtents.height / 480.0f;
		Vector4 newClipRange = uiPanel.clipRange;
		newClipRange.w = newClipRange.w * heightFactor;
		uiPanel.clipRange  = newClipRange;

		// Calculate the panel excess.
		panelExcess = newClipRange.w * 0.5f - (uiPanel.clipSoftness.y + 20.0f);

	}
	
	public void Refresh ()
	{



		Random.seed = 99;
		// Create all the map icons.
		int panelOffset = 120;
		int currentYPos = panelOffset;

		for (int i = 1 ; i < ProfileController.SectorsUnlocked + 1 ; i ++)
		{

			GameObject newMapIcon = (GameObject) Instantiate(mapIconPrefab);
			newMapIcon.transform.parent = navPanel.transform;
			Vector3 newPosition = Vector3.up * currentYPos;
			newPosition.x = Random.Range(-120, 10);
			newMapIcon.transform.localPosition = newPosition;
			newMapIcon.transform.localScale = Vector3.one;


			MapIcon mapIcon = newMapIcon.GetComponent<MapIcon>();
			mapIcon.sectorId = i;

			currentYPos += 60 + Random.Range(0, 100);

			mapIconList.Add(mapIcon);


		}

		panelMinY = - (mapIconList[mapIconList.Count-1].transform.localPosition.y - panelExcess) - 30;
		panelMaxY = 0.0f - panelExcess - panelOffset + 30;

//		Debug.Log ("MinY : " + panelMinY);

		for (int i = 1 ; i < ProfileController.SectorsUnlocked + 1 ; i ++)
		{
			MapIcon mapIcon = mapIconList[i - 1];
			MapIcon nextIcon = i != ProfileController.SectorsUnlocked ? mapIconList[i] : null;

			if (i == ProfileController.SectorId)
			{

				navPanel.transform.localPosition = Vector3.down * mapIcon.transform.localPosition.y;
				MapMenuController.SetCurrentPosition(navPanel.transform.localPosition);

			}
		}

	}

}
