using UnityEngine;
using System.Collections;

public class HeartIcon : KJBehavior
{
	
	public UISprite sprite;
	
	const string heartFull = "HeartFull_v3";
	const string heartEmpty = "HeartEmpty_v3";
	
	void Start ()
	{
	
	}
	
	public void SetToEmpty ()
	{
		sprite.spriteName = heartEmpty;
	}
	
	public void SetToFull ()
	{
		sprite.spriteName = heartFull;
	}
	
	public void Refill ()
	{
		if (sprite.spriteName == heartEmpty) {
			SetToFull();
			AddFlash(FlashOn, FlashOff, 0.05f, 3);
		}
	}
	
	public Color flashColor = new Color(1.0f, 0.35f, 0.85f, 1.0f);
	
	void FlashOn ()
	{
		sprite.color = flashColor;
	}
	
	void FlashOff ()
	{
		sprite.color = Color.black;
	}
}

