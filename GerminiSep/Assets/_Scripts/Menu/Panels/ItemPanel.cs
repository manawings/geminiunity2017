﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemPanel : MonoBehaviour {
	
	
	public GameObject itemPlatePrefab;
	public GameObject currentItemPlate;
	int maxItemPlates;
	
	private const float panelSize = 50.0f;
	private const float extendedPanelSize = 24.0f;
	private const float panelGap = 10.0f;
	
	float topCutOff, topOrigin, bottomCutOff, oldClipPos;
	
	UIDraggablePanel uiDragPanel;
	UIPanel uiPanel;
	
	List<ItemButton> itemButtonList = new List<ItemButton>();
	List<ItemBasic> proxyList;
	
	void Awake ()
	{
		uiPanel = GetComponent<UIPanel>();
		uiDragPanel = uiPanel.GetComponent<UIDraggablePanel>();
		
		float heightFactor = tk2dCamera.Instance.ScreenExtents.height / 480.0f;
		Vector4 newClipRange = uiPanel.clipRange;
		newClipRange.w = newClipRange.w * heightFactor;
		uiPanel.clipRange  = newClipRange;
	}

	void Update () {
		
		float moveDifference = uiPanel.clipRange.y - oldClipPos;
		
		if (moveDifference > 0) {
			CheckPlateBottom();
		} else if (moveDifference < 0) {
			CheckPlateTop();
		}
		
		oldClipPos = uiPanel.clipRange.y;
	}

	
	void CreateItemPlates ()
	{
		/// Math.

		maxItemPlates = 2 + (int) Mathf.Ceil(uiPanel.clipRange.w / (panelSize + panelGap));
		topOrigin = ((uiPanel.clipRange.w - panelSize - extendedPanelSize) * 0.5f) - uiPanel.clipSoftness.y;
		topCutOff = ((uiPanel.clipRange.w + panelSize + extendedPanelSize) * 0.5f) + panelGap;
		bottomCutOff = -topCutOff;
		oldClipPos = 0.0f;
		
		/// Clear the list.

		itemButtonList.Clear();
		
		for (int i = 0 ; i < maxItemPlates ; i ++ )
		{
			
			/// Create and physically put the plate into position.

			GameObject newItemPlate;
			
			if (i == 0) {
				newItemPlate = (GameObject) Instantiate(currentItemPlate);
			} else {
				newItemPlate = (GameObject) Instantiate(itemPlatePrefab);
			}
			
			newItemPlate.transform.parent = transform;
			newItemPlate.transform.localScale = Vector3.one;
			newItemPlate.transform.localPosition = Vector3.zero;
			
			float newYPosition = topOrigin - (panelGap + panelSize) * i;
			if (i != 0) newYPosition -= extendedPanelSize * 0.5f;
			KJMath.SetY(newItemPlate.transform, newYPosition);
			newItemPlate.GetComponentInChildren<UIDragPanelContents>().draggablePanel = uiDragPanel;
			
			ItemButton itemButton = newItemPlate.GetComponent<ItemButton>();
			itemButtonList.Add(itemButton);
			
		}
	}
	
	public void Refresh ()
	{
		
		if (itemButtonList.Count == 0) CreateItemPlates();
		
		List<ItemBasic> copyList = new List<ItemBasic>();
		
		switch (HangarController.Singleton.currentItemType)
		{
			
			case ItemBasic.Type.Ship:
			//copyList = ProfileController.Singleton.shipList;//ItemPattern.ShipList;
			break;
			
			case ItemBasic.Type.Module:
			//copyList = ItemPattern.ModuleList;
			break;

		}
		
		proxyList = new List<ItemBasic>();
		
		for (int i = 0 ; i < copyList.Count ; i ++ )
		{
			proxyList.Add (copyList[i]);
		}
		
		
		for (int i = 0 ; i < itemButtonList.Count ; i ++ )
		{
			
			ItemButton itemButton = itemButtonList[i];
			
			itemButton.SetCurrentList(proxyList);
			
			float newYPosition = topOrigin - (panelGap + panelSize) * i;
			if (i != 0) newYPosition -= extendedPanelSize * 0.5f;
			KJMath.SetY(itemButton.transform, newYPosition);
			
			itemButton.SetItem(i);
			
		}
		
		uiDragPanel.ResetPosition();
	}
	
	void CheckPlateTop ()
	{
		ItemButton topItem = itemButtonList[1];
		ItemButton endItem = itemButtonList[itemButtonList.Count - 1];
		
		if (endItem.itemId == proxyList.Count - 1) return;
		
		if (topItem.transform.localPosition.y - uiPanel.clipRange.y > topCutOff)
		{
			
			
			float newY = endItem.transform.localPosition.y - panelGap - panelSize;
			topItem.SetItem(endItem.itemId + 1);
			
			itemButtonList.RemoveAt(1);
			itemButtonList.Add(topItem);
			KJMath.SetY(topItem, newY);
		}
	}
	
	void CheckPlateBottom ()
	{
		ItemButton bottomItem = itemButtonList[itemButtonList.Count - 1];
		ItemButton endItem = itemButtonList[1];
		
		if (endItem.itemId == 1) return;
		
		if (bottomItem.transform.localPosition.y - uiPanel.clipRange.y < bottomCutOff)
		{
			
			float newY = endItem.transform.localPosition.y + panelGap + panelSize;
			bottomItem.SetItem(endItem.itemId - 1);
				
			itemButtonList.RemoveAt(itemButtonList.Count - 1);
			itemButtonList.Insert(1, bottomItem);
			KJMath.SetY(bottomItem, newY);
		}
	}
}
