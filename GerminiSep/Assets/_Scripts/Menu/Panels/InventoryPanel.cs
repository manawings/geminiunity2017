﻿using UnityEngine;
using System.Collections;

public class InventoryPanel : MonoBehaviour {

	public ItemSlot[] equipSlots;
	
	public StatBar damageBar;
	public StatBar armorBar;
	public StatBar lifeBar;
	
	[HideInInspector] public int pageCurrent = 0;
	public const int pageMax = 2;
	
	public void Deploy ()
	{
		
		ProfileController.CalibrateStats();
		
		for (int i = 0 ; i < equipSlots.Length ; i ++)
		{
			
			ItemSlot equipSlot = equipSlots[i];
			
			if ( i < ProfileController.Singleton.equipList.Count)
			{
			
				if (ProfileController.Singleton.equipList[i] != null )
				{
					equipSlot.Deploy(ProfileController.Singleton.equipList[i]);
					continue;
				}
				
			}
			
			equipSlot.Deploy(null);
			
		}
		
		// Display Stats.
		string statString = string.Empty;
		
		statString += ProfileController.Singleton.finalStats.damage + " Damage :: ";
		statString += ProfileController.Singleton.finalStats.armor + " Armor :: ";
		statString += ProfileController.Singleton.finalStats.life + " Life :: ";
		statString += ProfileController.Singleton.finalStats.energy + " Energy";

		damageBar.Deploy("DAMAGE", ProfileController.Singleton.finalStats.damage);
		armorBar.Deploy("ARMOR", ProfileController.Singleton.finalStats.armor);
		lifeBar.Deploy("LIFE", ProfileController.Singleton.finalStats.life);
		// energyBar.Deploy("ENG", ProfileController.Singleton.finalStats.energy);
	}

	public TutorialPrompter tutorialPrompter;
	
	void OnSlideComplete()
	{
		tutorialPrompter.DeployTutorial();
	}
}
