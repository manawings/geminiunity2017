﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipPanel : MonoBehaviour {
	
	
	public GameObject itemPlatePrefab;
	public GameObject currentItemPlate;

	int maxItemPlates;
	
	private const float panelSize = 50.0f;
	private const float extendedPanelSize = 24.0f;
	private const float panelGap = 10.0f;
	
	float topCutOff, topOrigin, bottomCutOff, oldClipPos;
	
	UIDraggablePanel uiDragPanel;
	UIPanel uiPanel;
	
	List<ShipButton> itemButtonList = new List<ShipButton>();
	List<ItemShip> proxyList;
	
	void Awake ()
	{
		uiPanel = GetComponent<UIPanel>();
		uiDragPanel = uiPanel.GetComponent<UIDraggablePanel>();
		
		float heightFactor = tk2dCamera.Instance.ScreenExtents.height / 480.0f;
		Vector4 newClipRange = uiPanel.clipRange;
		newClipRange.w = newClipRange.w * heightFactor;
		uiPanel.clipRange  = newClipRange;
	}

	void Update () {
		
		float moveDifference = uiPanel.clipRange.y - oldClipPos;

		if (moveDifference > 0) {
			CheckPlateBottom();
		} else if (moveDifference < 0) {
			CheckPlateTop();
		}
		
		oldClipPos = uiPanel.clipRange.y;

		// Anchor the X to 0.
		if (uiPanel.clipRange.x != 0) {
			Vector4 newVector = uiPanel.clipRange;
			newVector.x = 0;
			uiPanel.clipRange = newVector;
		}
	}

	
	void CreateItemPlates ()
	{
		/// Math.

		maxItemPlates = 3 + (int) Mathf.Ceil(uiPanel.clipRange.w / (panelSize + panelGap));
		topOrigin = ((uiPanel.clipRange.w - panelSize - extendedPanelSize) * 0.5f) - uiPanel.clipSoftness.y;
		topCutOff = ((uiPanel.clipRange.w + panelSize + extendedPanelSize) * 0.5f) + panelGap;
		bottomCutOff = -topCutOff;
		oldClipPos = 0.0f;
		
		/// Clear the list.

		itemButtonList.Clear();
		
		for (int i = 0 ; i < maxItemPlates ; i ++ )
		{
			
			/// Create and physically put the plate into position.

			GameObject newItemPlate;

			/*
			if (i == 0) {
				newItemPlate = (GameObject) Instantiate(currentItemPlate);
			} else {
				newItemPlate = (GameObject) Instantiate(itemPlatePrefab);
			}
			*/

			newItemPlate = (GameObject) Instantiate(itemPlatePrefab);
			
			newItemPlate.transform.parent = transform;
			newItemPlate.transform.localScale = Vector3.one;
			newItemPlate.transform.localPosition = Vector3.zero;
			
			float newYPosition = topOrigin - (panelGap + panelSize) * i;
			// if (i != 0) newYPosition -= extendedPanelSize * 0.5f;
			KJMath.SetY(newItemPlate.transform, newYPosition);
			newItemPlate.GetComponentInChildren<UIDragPanelContents>().draggablePanel = uiDragPanel;
			
			ShipButton itemButton = newItemPlate.GetComponent<ShipButton>();
			itemButtonList.Add(itemButton);
			
		}
	}
	
	public void Refresh (bool resetAtTop = false)
	{
		
		if (itemButtonList.Count == 0) CreateItemPlates();
		
		List<ItemShip> copyList = new List<ItemShip>();
		
		copyList = ItemPattern.ShipList;
		
		proxyList = new List<ItemShip>();
		
		for (int i = 0 ; i < copyList.Count ; i ++ )
		{
			if (ProfileController.Singleton.shipId == i)
			{
				// Shove it in at the start.
				proxyList.Insert(0, copyList[i]);
			} else {
				// Add it to the list normally.
				proxyList.Add(copyList[i]);
			}
			
		}
		
		for (int i = 0 ; i < itemButtonList.Count ; i ++ )
		{
			
			ShipButton itemButton = itemButtonList[i];
			
			itemButton.SetCurrentList(proxyList);
			
			float newYPosition = topOrigin - (panelGap + panelSize) * i;
			KJMath.SetY(itemButton.transform, newYPosition);
			
			if (i == 0) itemButton.SetShip(i, true);
			else itemButton.SetShip(i, false);

			if (i == itemButtonList.Count - 1) {
				itemButton.SetShip(proxyList.Count - 1, false);
				newYPosition = topOrigin - (panelGap + panelSize) * (proxyList.Count - 1);
				KJMath.SetY(itemButton.transform, newYPosition);
			}
			
		}

		
		
		if (resetAtTop)	uiDragPanel.ResetPosition();

		for (int i = 0 ; i < 25 ; i ++ )
		{
			CheckPlateTop();
			CheckPlateBottom();
		}
	}
	
	void CheckPlateTop ()
	{
		ShipButton topItem = itemButtonList[1];
		ShipButton endItem = itemButtonList[itemButtonList.Count - 2];
		
		if (endItem.itemId == proxyList.Count - 2) return;
		
		if (topItem.transform.localPosition.y - uiPanel.clipRange.y > topCutOff)
		{
			
			
			float newY = endItem.transform.localPosition.y - panelGap - panelSize;
			topItem.SetShip(endItem.itemId + 1);
			
			itemButtonList.RemoveAt(1);
			itemButtonList.Insert(itemButtonList.Count - 1, topItem);
			KJMath.SetY(topItem, newY);
		}
	}
	
	void CheckPlateBottom ()
	{
		ShipButton bottomItem = itemButtonList[itemButtonList.Count - 2];
		ShipButton endItem = itemButtonList[1];
		
		if (endItem.itemId == 1) return;
		
		if (bottomItem.transform.localPosition.y - uiPanel.clipRange.y < bottomCutOff)
		{
			
			float newY = endItem.transform.localPosition.y + panelGap + panelSize;
			bottomItem.SetShip(endItem.itemId - 1);
				
			itemButtonList.RemoveAt(itemButtonList.Count - 2);
			itemButtonList.Insert(1, bottomItem);
			KJMath.SetY(bottomItem, newY);
		}
	}

	void OnSlideComplete ()
	{
		SpringPanel springPanel = GetComponent<SpringPanel>();
		if (springPanel != null) {
			springPanel.target = transform.localPosition;
			//			DestroyImmediate(springPanel);
		}
	}
}
