﻿using UnityEngine;
using System.Collections;

public class PreviewShip : KJSpriteBehavior {
	
	void Start () {
	
	}
	
	KJEmitter boosterEmitter;
	KJEmitter boosterEmitter2;
	
	bool isActive = false;
	
	public void Deploy (ItemBasic item)
	{
		
		gameObject.SetActive(true);
		
		boosterEmitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		boosterEmitter.CopyFromModel(EffectController.Singleton.shipBooster);
		boosterEmitter.SetColor(item.boosterColorStart, item.boosterColorEnd);
		boosterEmitter.SetObjectLink(gameObject);
		boosterEmitter.SetIsContained(true);
		
		boosterEmitter2 = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		boosterEmitter2.CopyFromModel(EffectController.Singleton.shipBoosterStay);
		boosterEmitter2.SetColor(item.boosterColorStart, item.boosterColorEnd);
		boosterEmitter2.SetObjectLink(gameObject);
		boosterEmitter2.SetIsContained(true);
		
		animator.Play(item.shipClip);
		animator.Stop();
		
		isActive = true;

	}
	
	public void StartEmitters ()
	{
		
		if (!isActive) return;
		
		animator.Play();
		
		boosterEmitter.Deploy();
		
		boosterEmitter2.Deploy();
		
	}
	
	public void EndEmitters ()
	{
		animator.Stop();
		
		if (boosterEmitter != null) boosterEmitter.Terminate();
		if (boosterEmitter2 != null) boosterEmitter2.Terminate();
	}
	
	public void Hide ()
	{
		isActive = false;
		
		EndEmitters();
		gameObject.SetActive(false);
	}
}
