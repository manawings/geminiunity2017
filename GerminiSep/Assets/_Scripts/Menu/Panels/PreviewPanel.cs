using UnityEngine;
using System.Collections;

public class PreviewPanel : MonoBehaviour {
	
	public UILabel itemName;
	public UILabel itemType;
	
	public UILabel itemDescLong;
	public UILabel itemDescShort;


	public UISprite equipButton;
	public ItemSlot itemSlot;
	
	public KJBarGlow upgradeBar;
	
	public GameObject previewObject;
	
	int itemId;
	ItemBasic item;
	
	public void Calibrate (ItemBasic item)	
	{
		
		this.item = item;
		itemId = item.id;
		item.isNew = false;
		
		itemName.text = item.Name;
		itemName.color = item.TextColor;
		itemType.text = "Lvl. " + item.upgradeLevel;
		
		itemSlot.Deploy(item);

		if (equipButton != null) {
			if (ProfileController.IsItemEquipped(item))
			{
				equipButton.spriteName = "Unequip_v3";
			} else {
				equipButton.spriteName = "Equip_v3";
			}
		}
		
		
		upgradeBar.SetProgress(item.upgradeLevel / 5.0f, true);
		
		// TODO: Temp. Make this prettier and simpler.
		
		SetItemDesc();
	}
	
	void SetItemDesc ()
	{
		string descText = string.Empty;
		int lines = 0, statId = 0;
		bool hasDesc = false;
		
		// descText += "Level +" + item.level.ToString() + "\n";
		if (item.DamageFinal > 0.0f) {
			descText += "+ " + item.DamageFinal.ToString() + " Damage";
			lines++;
			statId = 0;
		}

		if (item.LifeFinal > 0.0f) {
			if (lines > 0) descText += "\n";
			descText += "+ " + item.LifeFinal.ToString() + " Life"; 
			lines++;
			statId = 1;
		}

		if (item.ArmorFinal > 0.0f) {
			if (lines > 0) descText += "\n";
			descText += "+ " + item.ArmorFinal.ToString() + " Armor"; 
			lines++;
			statId = 2;
		}

//		if (item.EnergyFinal > 0.0f) descText += "+ " + item.EnergyFinal.ToString() + " Energy" + "\n"; lines++;
		
		if (item.module != null ) {

			if (lines > 0) descText += "\n";
			descText += "\n";
			descText += "[D8FFB8]" + item.module.name + ": [-]"; // item.module.name
			descText += "[89FF52]" + item.module.Description + "[-]"; 
			hasDesc = true;
		}
		
		itemDescLong.text = string.Empty;
		itemDescShort.text = string.Empty;
		
		if (hasDesc)
		{

			itemDescLong.text = descText;

		} else {

			itemDescShort.text = descText;
		}
	}
	
	public void RunPreviewFollow ()
	{
		KJMath.SetX(previewObject.transform, transform.localPosition.x);
	}
	
	public void ClickSell ()
	{
//		Debug.Log("Click Sell!");
		
		UIController.ShowPopupSell(item);
		// HangarController.Singleton.SwapTo(HangarController.PanelType.Inventory, true);
	}
	
	public void ClickUpgrade ()
	{
//		Debug.Log("Click Upgrade!");
		
		if (item.upgradeLevel < 5)
		{
			UIController.ShowPopupUpgrade(item);
		}
	}
	
	public void ClickEquip ()
	{
		
//		Debug.Log("Click Equip / Unequip!");
		
		
		if (ProfileController.IsItemEquipped(item))
		{
			// If Object is equipped, then unequip.
			ProfileController.UnequipItem(item);
//			DataController.SaveAllData(); //nun
		} else if (!ProfileController.IsItemEquipped(item)) {
			
			// If Object is in inventory, then try to equip.
			if (ProfileController.HasEquipSpace) {
				ProfileController.EquipItem(item);
//				DataController.SaveAllData(); //nun
			} else {
				UIController.ShowPopupEquip(item);
//				DataController.SaveAllData(); //nun
				return;
			}
		}
		
		ItemMenuController.Singleton.GoBackToInventory();
		// HangarController.Singleton.SwapTo(HangarController.PanelType.Inventory, true);
		
	}
	
	public void ClickBuy ()
	{
		if (ProfileController.Gems >= item.cost)
		{
			ProfileController.Gems -= item.cost;
			// ProfileController.GainItem(item.type, item.id);
			ProfileController.EquipItem(item);
			
			UIController.UpdateCurrency();
			
			KJTime.Add(DelayedControlLock, 0.05f, 1);
			KJTime.Add(DelayedCalibrate, 0.30f, 1);
		}
	}
	
	void DelayedCalibrate ()
	{
		Calibrate(item);
		InputController.UnlockControls();
	}
	
	void DelayedControlLock ()
	{
		InputController.LockControls();
	}

	public TutorialPrompter tutorialPrompter;
	public TutorialPrompter tutorialPrompterUpgrade;

	void OnSlideComplete()
	{
		if (tutorialPrompter != null) tutorialPrompter.DeployTutorial();
		if (tutorialPrompterUpgrade != null) tutorialPrompterUpgrade.DeployTutorial();
	}
	
}
