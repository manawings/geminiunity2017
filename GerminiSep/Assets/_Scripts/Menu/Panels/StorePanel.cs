﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StorePanel : MonoBehaviour {

	#if UNITY_IPHONE
	public GameObject storePlatePrefab;
	public StoreController storeController;
	
	int maxItemPlates;
	
	private const float panelSize = 50.0f;
	private const float extendedPanelSize = 24.0f;
	private const float panelGap = 10.0f;
	
	float topCutOff, topOrigin, bottomCutOff, oldClipPos;
	
	UIDraggablePanel uiDragPanel;
	UIPanel uiPanel;
	
	List<StoreButton> itemButtonList = new List<StoreButton>();
	List<StoreObject> proxyList;
	
	void Awake ()
	{
		uiPanel = GetComponent<UIPanel>();
		uiDragPanel = uiPanel.GetComponent<UIDraggablePanel>();
		
		float heightFactor = tk2dCamera.Instance.ScreenExtents.height / 480.0f;
		Vector4 newClipRange = uiPanel.clipRange;
		newClipRange.w = newClipRange.w * heightFactor;
		uiPanel.clipRange  = newClipRange;
	}

	void Start ()
	{
		Refresh();
	}
	
	void Update () {
		
//		float moveDifference = uiPanel.clipRange.y - oldClipPos;
//		
//		if (moveDifference > 0) {
//			CheckPlateBottom();
//		} else if (moveDifference < 0) {
//			CheckPlateTop();
//		}
//		
//		oldClipPos = uiPanel.clipRange.y;
	}
	
	
	void CreateItemPlates ()
	{
		/// Math.
		
//		maxItemPlates = 2 + (int) Mathf.Ceil(uiPanel.clipRange.w / (panelSize + panelGap));
//		topOrigin = ((uiPanel.clipRange.w - panelSize - extendedPanelSize) * 0.5f) - uiPanel.clipSoftness.y;
//		topCutOff = ((uiPanel.clipRange.w + panelSize + extendedPanelSize) * 0.5f) + panelGap;
//		bottomCutOff = -topCutOff;
//		oldClipPos = 0.0f;
//		
//		/// Clear the list.
//		
//		itemButtonList.Clear();
//		proxyList = storeController.storeList;
//
//		if (maxItemPlates > proxyList.Count) maxItemPlates = proxyList.Count;
//		
//		for (int i = 0 ; i < maxItemPlates ; i ++ )
//		{
//			
//			/// Create and physically put the plate into position.
//			
//			GameObject newItemPlate;
//
//			
//			newItemPlate = (GameObject) Instantiate(storePlatePrefab);
//			
//			newItemPlate.transform.parent = transform;
//			newItemPlate.transform.localScale = Vector3.one;
//			newItemPlate.transform.localPosition = Vector3.zero;
//			
//			float newYPosition = topOrigin - (panelGap + panelSize) * i;
//			// if (i != 0) newYPosition -= extendedPanelSize * 0.5f;
//			KJMath.SetY(newItemPlate.transform, newYPosition);
//			newItemPlate.GetComponentInChildren<UIDragPanelContents>().draggablePanel = uiDragPanel;
//			
//			StoreButton itemButton = newItemPlate.GetComponent<StoreButton>();
//			itemButtonList.Add(itemButton);
//			
//		}
	}
	
	public void Refresh ()
	{
		
//		if (itemButtonList.Count == 0) CreateItemPlates();
//
//
//
//		
//		for (int i = 0 ; i < itemButtonList.Count ; i ++ )
//		{
//			
//			StoreButton itemButton = itemButtonList[i];	
//			float newYPosition = topOrigin - (panelGap + panelSize) * i;
//			KJMath.SetY(itemButton.transform, newYPosition);
//
//			if (i < proxyList.Count) {
//				itemButton.SetStoreItem(proxyList[i], i);
//			} else {
//
//			}
//			
//		}
//		
//		uiDragPanel.ResetPosition();
	}

//	public void CalibrateProductList ( List<StoreKitProduct> productList )
//	{
//		StoreKitProduct product;
//		for (int i = 0 ; i < itemButtonList.Count ; i ++ )
//		{
//			StoreButton storeButton = itemButtonList[i];
//			if (productList.Count > i) {
//				product = productList[i];
//				storeButton.CalibrateWithProduct(product);
//			}
//		}
//	}
//	
//	void CheckPlateTop ()
//	{
//		StoreButton topItem = itemButtonList[1];
//		StoreButton endItem = itemButtonList[itemButtonList.Count - 1];
//		
//		if (endItem.itemId == proxyList.Count - 1) return;
//		
//		if (topItem.transform.localPosition.y - uiPanel.clipRange.y > topCutOff)
//		{
//			
//			
//			float newY = endItem.transform.localPosition.y - panelGap - panelSize;
//			int newId = endItem.itemId + 1;
//			topItem.SetStoreItem(proxyList[newId], newId);
//			// topItem.SetShip(endItem.itemId + 1);
//			
//			itemButtonList.RemoveAt(1);
//			itemButtonList.Add(topItem);
//			KJMath.SetY(topItem, newY);
//		}
//	}
//	
//	void CheckPlateBottom ()
//	{
//		StoreButton bottomItem = itemButtonList[itemButtonList.Count - 1];
//		StoreButton endItem = itemButtonList[1];
//		
//		if (endItem.itemId == 1) return;
//		
//		if (bottomItem.transform.localPosition.y - uiPanel.clipRange.y < bottomCutOff)
//		{
//			
//			float newY = endItem.transform.localPosition.y + panelGap + panelSize;
//			int newId = endItem.itemId - 1;
//			bottomItem.SetStoreItem(proxyList[newId], newId);
//			// bottomItem.SetShip(endItem.itemId - 1);
//			
//			itemButtonList.RemoveAt(itemButtonList.Count - 1);
//			itemButtonList.Insert(1, bottomItem);
//			KJMath.SetY(bottomItem, newY);
//		}
//	}
	#endif
}
