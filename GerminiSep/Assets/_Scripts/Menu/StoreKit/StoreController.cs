﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoreController : MonoBehaviour {

	#if UNITY_IPHONE

	public List<StoreObject> storeList;
	public StorePanel storePanel;

	void Start ()
	{
		storeList = new List<StoreObject>();

		storeList.Add ( new StoreObject("Single of Gold", "HC1", "", false, 1));
		storeList.Add ( new StoreObject("Handful of Gold", "HC2", "", false, 5));
		storeList.Add ( new StoreObject("Bunch of Gold", "HC3", "", false, 10));
		storeList.Add ( new StoreObject("Box of Gold", "HC4", "", false, 25));
		storeList.Add ( new StoreObject("Vault of Gold", "HC5", "", false, 50));

	//	KJTime.Add(GetProductData, 0.5f, 1);

		// KJTime.Add(UIController.HideWait, 5.0f, 1);
	}
//
//	void GetProductData ()
//	{
//		UIController.ShowWait();
//
//		string[] productIdentifiers = new string[storeList.Count];
//		for (int i = 0 ; i < storeList.Count ; i ++ )
//		{
//			productIdentifiers[i] = storeList[i].id;
//		}
//		StoreKitBinding.requestProductData( productIdentifiers );
//	}
//
//	public void CalibrateProductList ( List<StoreKitProduct> productList )
//	{
//		UIController.ClosePopUp();
//		storePanel.CalibrateProductList(productList);
//	}
//
	#endif

}
