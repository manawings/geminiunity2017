using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

public class StoreObject {

	public string name, storeName;
	public string id, appleId, andriodId;

	public string price;
	public bool isSC;

	public ObscuredInt units;

	public StoreObject (string name, string id, string price, bool isSC = false, int units = 0)
	{
		// Display Name
		this.name = name;
		this.storeName = storeName;

		// Store ID
		this.id = id;
		appleId = id;
		andriodId = id;

		// Current Localized Price
		this.price = price;

		// Product Info
		this.isSC = isSC;
		this.units = units;

	}

	public void CalibrateProduct (string storeName, string price)
	{
		this.storeName = storeName;
		this.price = price;
//		Debug.Log("Calibrated Price: " + this.price);
	}

	public void BuyMe ()
	{
		if (isSC) {
			ProfileController.Gems += units;
		} else {
			ProfileController.Credits += units;
		}

//		DataController.SaveAllData();
	}

}
