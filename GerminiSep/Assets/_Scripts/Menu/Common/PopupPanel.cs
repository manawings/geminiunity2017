﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

public class PopupPanel : KJBehavior {
	
	public enum Type {
		None,
		Sell,
		Equip,
		Upgrade,
		UnlockShip,
		UpgradeShip,
		ResetData,
		Waiting,
		Empty,
		NoHearts,
		BuyHearts,
		Story,
		Error,
		NotEnoughGems,
		NotEnoughCredits,
		IAP,
		Revive,
		YesOrNo,
		SingleClose,
		Pause,
		SingleYes,
		ViewAd,
		ViewAdSC,
		Preview,
		NotEnoughCores,
	}
	
	Type type;
	Type fallBackType;

	// Main Panels
	public GameObject waitPanel;
	public GameObject pausePanel;
	public GameObject mainPanel;
	public GameObject commonPanel;
	public StoryPanel storyPanel;
	public GameObject standardPanel;
	public IAPPanel iapPanel;

	
	// References
	public UILabel headerText;
	public UILabel bodyText;
	
	public GameObject equipSubPanel;
	public GameObject sellSubPanel;
	public GameObject upgradeSubPanel;
	

	// Item Equipping
	public ItemSlot[] equipSlots;
	
	// Item Selling
	ItemBasic item;
	public ObscuredInt salePrice;
	public ItemSlot itemSlotSell;
	public UILabel bodyTextSell;
	
	// Item Upgrading
	public UILabel bodyTextUpgrade, upgradeResultsLabel, upgradeNameLabel;
	public ButtonFlashBehavior closeXButton;
	public BlitzButton upgradeButton;
	public BlitzButton sellButton;
	public BlitzButton standardYesButton;
	public BlitzButton standardNoButton;
	public BlitzButton standardBuyButton;
	public UILabel upgradeCostLabel;
	public UISprite upgradeGemSprite;
	public ItemSlot itemSlotUpgrade1;
	public ItemSlot itemSlotUpgrade2;
	
	// References for pop-in, pop-up animation.
	
	public UISprite blackCover;
	public GameObject panelMain;
	
	Hashtable panelTweenIn;
	Hashtable panelTweenOut;

	public TutorialPrompter upgradeTutorialPrompter;

	void Awake ()
	{
		UIController.popupPanel = this;	
		Initialize();
	}

	
	void Initialize ()
	{
		
		panelTweenIn = new Hashtable();
		panelTweenIn.Add("from", 0.0f);
        panelTweenIn.Add("to", 1.0f);
        panelTweenIn.Add("time", 0.50f);
        panelTweenIn.Add("onupdate", "OnPanelTween");
		panelTweenIn.Add("oncomplete", "OnPanelCompleteIn");
		panelTweenIn.Add("easetype", iTween.EaseType.easeOutBack);
		
		panelTweenOut = new Hashtable();
		panelTweenOut.Add("from", 1.0f);
        panelTweenOut.Add("to", 0.0f);
        panelTweenOut.Add("time", 0.50f);
        panelTweenOut.Add("onupdate", "OnPanelTween");
		panelTweenOut.Add("oncomplete", "OnPanelCompleteOut");
		panelTweenOut.Add("easetype", iTween.EaseType.easeInBack);
		
		// Originally Hide the pop-up elements.
		blackCover.alpha = 0.0f;
		KJMath.SetY(panelMain.transform, 400.0f);
		
		standardPanel.SetActive(true);
		gameObject.SetActive(false);
	}

	bool lockBlacker = false;
	
	void OnPanelTween (float val)
	{
		if (!lockBlacker || blackCover.alpha != 0.90f) blackCover.alpha = val * 0.90f;
		float yPosition = 400.0f - (val * 400.0f);
		KJMath.SetY(panelMain.transform, yPosition);
		KJMath.SetY(storyPanel.transform, yPosition);
		KJMath.SetY(iapPanel.transform, yPosition);
		KJMath.SetY(commonPanel.transform, yPosition);
		waitPanel.transform.localScale = Vector3.one * val;
	}
	
	void OnPanelCompleteIn ()
	{
		InputController.LockControlForPopUp();

		if (upgradeTutorialPrompter != null) {
			if (TutorialData.CheckTutorial(15)) {
				KJTime.Resume();
				upgradeTutorialPrompter.DeployTutorial();
				OnCompleteCloseDelegate = ShowPostUpgradeStory;
			}
		}
	}

	void ShowPostUpgradeStory () {

		TutorialData.Deploy(16);
	}
	
	void OnPanelCompleteOut ()
	{
		if (nextType == Type.None) {



			OnYesDelegate = null;

			item = null;
			currentType = nextType;
			InputController.UnlockControls();
			KJTime.Resume();
			gameObject.SetActive(false);

			if ( OnCompleteCloseDelegate  != null ) OnCompleteCloseDelegate();
			OnCompleteCloseDelegate = null;


		} else {

			SwitchToType();

		}


	}

	public KJTime.TimeDelegate OnYesDelegate;
	public KJTime.TimeDelegate OnCompleteCloseDelegate;

	public Type currentType = Type.None;
	public Type nextType = Type.None;

	public void DeployStory ( Story story ) {
		storyPanel.Deploy(story);
		Deploy(Type.Story);
	}

	void OnExitToMenu ()
	{
		DataController.SaveAllData();
		GameController.LoseGameActual();
	}

	void SwitchToType ()
	{

		currentType = nextType;
		nextType = Type.None;

		OnPanelTween(0);
		iTween.Stop(gameObject);
		iTween.ValueTo(gameObject, panelTweenIn);
		
		gameObject.SetActive(true);
		sellSubPanel.SetActive(false);
		equipSubPanel.SetActive(false);
		upgradeSubPanel.SetActive(false);

		closeXButton.gameObject.SetActive(true);
		

		commonPanel.SetActive(true);
		mainPanel.SetActive(true);
		waitPanel.SetActive(false);
		iapPanel.gameObject.SetActive(false);
		storyPanel.gameObject.SetActive(false);
		pausePanel.SetActive(false);
		
		bodyText.text = "";
		
//		InputController.LockControlForPopUp();
		
		this.type = currentType;

		// Default
		bodyText.pivot = UIWidget.Pivot.Left;

		closeXButton.targetObject = this.gameObject;
		closeXButton.methodName = "ClosePanel";
		closeXButton.gameObject.SetActive(true);

		standardYesButton.buttonLabel = "CONFIRM";
		standardYesButton.methodName = "OnYes";
		standardYesButton.gameObject.SetActive(false);
		standardYesButton.isFC = false;
		standardYesButton.sideLineLabel = false;
		KJMath.SetY(standardYesButton, -100);

		standardNoButton.buttonLabel = "CLOSE";
		standardNoButton.methodName = "ClosePanel";
		standardNoButton.gameObject.SetActive(false);

		standardBuyButton.buttonLabel = "PURCHASE";
		standardBuyButton.methodName = "OnSpend";
		standardBuyButton.plusPrice = false;
		standardBuyButton.isSoftCurrency = false;
		standardBuyButton.isFC = false;
		standardBuyButton.isCoreCurrency = false;
		standardBuyButton.gameObject.SetActive(false);
		standardBuyButton.SetYellowColor();

		switch (type)
		{
			
		case Type.Empty:
			
			headerText.text = displayHeader;
			bodyText.text = displayBody;
			
			break;

		case Type.SingleYes:
			
			headerText.text = displayHeader;
			bodyText.text = displayBody;
			standardYesButton.buttonLabel = displayYes;
			standardYesButton.gameObject.SetActive(true);
			closeXButton.gameObject.SetActive(false);
			KJMath.SetY(standardYesButton, -150);
			break;

		case Type.Pause:
			
			headerText.text = "";
			closeXButton.gameObject.SetActive(false);
			pausePanel.SetActive(true);
			
			break;


		case Type.SingleClose:
			
			headerText.text = displayHeader;
			bodyText.text = displayBody;
			standardNoButton.gameObject.SetActive(true);
			break;

		case Type.YesOrNo:
			
			headerText.text = displayHeader;
			bodyText.text = displayBody;

			standardYesButton.buttonLabel = displayYes;
			standardNoButton.buttonLabel = displayNo;
			standardYesButton.methodName = "OnYes";
			
			standardYesButton.gameObject.SetActive(true);
			standardNoButton.gameObject.SetActive(true);
			
			break;

		case Type.Error:

			headerText.text = preSaveHeader;
			bodyText.text = preSaveBody;
			standardNoButton.gameObject.SetActive(true);

			if (UIController.tryAgainDelegate != null) {
				standardYesButton.buttonLabel = "TRY AGAIN?";
				standardYesButton.gameObject.SetActive(true);
			}

			if (UIController.forceTryAgain) {
				standardNoButton.gameObject.SetActive(false);
				closeXButton.gameObject.SetActive(false);
				KJMath.SetY(standardYesButton, -150);

			}
			
			break;
			
		case Type.NoHearts:
			
			headerText.text = "NO LIVES";
			bodyText.text = "No lives left! All pilots have been killed in battle. Please wait for your pilots to be restored.";
			
			break;
			
		case Type.Sell:
			
			sellSubPanel.SetActive(true);
			salePrice = item.SalePrice;
			headerText.text = "SELL ITEM";
			bodyTextSell.text = "Are you sure you want to sell this item for " + salePrice + " Gems?";
			itemSlotSell.Deploy(item);

			sellButton.defaultPrice = salePrice;
			sellButton.SetOptions();
			
			break;
			
		case Type.Equip:
			
			for (int i = 0 ; i < equipSlots.Length ; i ++)
			{
				
				ItemSlot equipSlot = equipSlots[i];
				equipSlot.itemId = i;
				equipSlot.Deploy(ProfileController.Singleton.equipList[i]);
				
			}

			headerText.text = string.Empty;
			standardNoButton.buttonLabel = "CANCEL";
			standardNoButton.gameObject.SetActive(true);
			closeXButton.gameObject.SetActive(false);
			equipSubPanel.SetActive(true);


			break;
			
		case Type.Upgrade:
			
			upgradeSubPanel.SetActive(true);
			salePrice = item.UpgradePrice;
			headerText.text = "UPGRADE ITEM";
			bodyTextUpgrade.text = item.Name;
			bodyTextUpgrade.color = item.TextColor;
			upgradeButton.defaultPrice = salePrice;
			upgradeButton.SetOptions();

			string lifeColor = "9EFF5D", damageColor = "FF4D4D", armorColor = "FACE40";


			string upgradeResultsText = string.Empty;
			string upgradeNameLabelText = string.Empty;
			bool needsNew = false;

			if (item.DamageBoostFromUpgrade != 0) {
//				upgradeResultsText += "+ " + item.DamageBoostFromUpgrade + " Damage →";
				upgradeResultsText += item.DamageFinal + " ["+damageColor+"]→[-] " + item.damageAtLevel(item.level, item.upgradeLevel + 1);
				upgradeNameLabelText += "["+damageColor+"]DAMAGE[-]";
				needsNew = true;
			}

			if (item.LifeBoostFromUpgrade != 0) {
				if (needsNew) {
					upgradeResultsText += "\n";
					upgradeNameLabelText += "\n";
				}
				//				upgradeResultsText += "+ " + item.LifeBoostFromUpgrade + " Life →";
				upgradeResultsText += item.LifeFinal + " ["+lifeColor+"]→[-] " + item.lifeAtLevel(item.level, item.upgradeLevel + 1);
				upgradeNameLabelText += "["+lifeColor+"]LIFE[-]";
				needsNew = true;
			}

			if (item.ArmorBoostFromUpgrade != 0) {
				if (needsNew) {
					upgradeResultsText += "\n";
					upgradeNameLabelText += "\n";
				}
//				upgradeResultsText += "+ " + item.ArmorBoostFromUpgrade + " Armor ";
				upgradeResultsText += item.ArmorFinal + " ["+armorColor+"]→[-] " + item.armorAtLevel(item.level, item.upgradeLevel + 1);
				upgradeNameLabelText += "["+armorColor+"]ARMOR[-]";
				needsNew = true;
			}



			upgradeResultsLabel.text = upgradeResultsText;
			upgradeNameLabel.text = upgradeNameLabelText;

			
//			float textEndPoint = - upgradeCostLabel.font.CalculatePrintedSize(salePrice.ToString(), true, UIFont.SymbolStyle.None).x * upgradeCostLabel.transform.localScale.x;
//			textEndPoint += upgradeCostLabel.transform.localPosition.x;
//			textEndPoint -= upgradeGemSprite.transform.localScale.x / 2;
//			textEndPoint -= 5.0f;
//			KJMath.SetX(upgradeGemSprite.transform, textEndPoint);
			
			itemSlotUpgrade1.Deploy(item);
			itemSlotUpgrade2.Deploy(item);


			
			break;

		case Type.BuyHearts:
			
			headerText.text = "INSTANT REVIVAL";
			bodyText.text = "Instantly restore all of your lives?";
			bodyText.pivot = UIWidget.Pivot.Center;
			salePrice = 1;
			
			standardBuyButton.defaultPrice = salePrice;
			standardBuyButton.gameObject.SetActive(true);
			standardBuyButton.buttonLabel = "RESTORE";
			
			standardNoButton.gameObject.SetActive(true);
			
			break;

		case Type.Revive:
			
			headerText.text = "COUNTER ATTACK";
			bodyText.text = "Your ship has been destroyed. However, you may instantly deploy a second unit to resume the mission for one Credit. You have " + ProfileController.Credits + " Credits remaining.";
			salePrice = 1;
			
			standardBuyButton.defaultPrice = salePrice;
			standardBuyButton.gameObject.SetActive(true);
		//	standardBuyButton.buttonLabel = "DEPLOY";
			standardBuyButton.buttonLabel = "REVIVE";
			standardBuyButton.buttonLabel = "REVIVE (USE 1 CREDIT)";
			standardBuyButton.defaultPrice = 0;
			standardNoButton.buttonLabel = "RETREAT";
			standardNoButton.gameObject.SetActive(true);


			if(ProfileController.Singleton.adsReviveAvailable)
			{
				//standardYesButton.buttonLabel = "FREE REVIVE";
				standardYesButton.buttonLabel = "REVIVE (WATCH AD)";
			//	standardYesButton.buttonLabel = "REVIVE";

				standardYesButton.gameObject.SetActive(true);
				standardYesButton.methodName = "OnWatchVideo";
				standardYesButton.isFC = true;
				standardYesButton.sideLineLabel = true;
	//			standardYesButton.gameObject.SetActive(false);
				KJMath.SetY(standardYesButton, -100);
				KJMath.SetY(standardBuyButton, -50);
			}
			else {
				KJMath.SetY(standardBuyButton, -100);
				KJMath.SetY(standardNoButton, -150);
			}
			
			break;
			
		case Type.UnlockShip:

			salePrice = item.cost;
			headerText.text = item.Name.ToUpper();
			string currencyString = item.isSpecialShip ? "Cores" : "Credits";

			bodyText.text = "Are you sure you want to unlock this ship for for " + salePrice + " " + currencyString + "?";

			standardBuyButton.defaultPrice = salePrice;
			standardBuyButton.gameObject.SetActive(true);
			standardBuyButton.buttonLabel = "Unlock Ship";

			if (item.isSpecialShip) {
				standardBuyButton.SetTealColor();
				standardBuyButton.isCoreCurrency = true;
			}

			standardNoButton.gameObject.SetActive(true);
			
			break;

		case Type.ViewAd:

			headerText.text = "COMMS SIGNAL";
			bodyText.text = "Incoming COMMS signal! You may watch the video transmission to receive a bonus Credit.";
			
			standardBuyButton.defaultPrice = 1;
			standardBuyButton.plusPrice = true;
			standardBuyButton.gameObject.SetActive(true);
			standardBuyButton.buttonLabel = "Watch Video";
			
			standardNoButton.gameObject.SetActive(true);
			standardNoButton.buttonLabel = "NO THANKS";

			break;

		case Type.ViewAdSC:
			
			headerText.text = "COMMS SIGNAL";
			bodyText.text = "Incoming COMMS signal! You may watch the video transmission to receive 50 bonus Gems.";
			
			standardBuyButton.defaultPrice = 50;
			standardBuyButton.plusPrice = true;
			standardBuyButton.isSoftCurrency = true;
			standardBuyButton.gameObject.SetActive(true);
			standardBuyButton.buttonLabel = "Watch Video";

			standardBuyButton.SetBlueColor();
			
			standardNoButton.gameObject.SetActive(true);
			standardNoButton.buttonLabel = "NO THANKS";
			
			break;

		case Type.Preview:
			
			headerText.text = "SHIP PREVIEW";
			bodyText.text = "Start a simulation of this ship for a small cost. The ship will be equipped with your current items, and will be simulated against your current enemies.";

			salePrice = 150;
			standardBuyButton.defaultPrice = salePrice;

			standardBuyButton.isSoftCurrency = true;
			standardBuyButton.gameObject.SetActive(true);
			standardBuyButton.buttonLabel = "SIMULATE";

			standardBuyButton.SetBlueColor();
			
			standardNoButton.gameObject.SetActive(true);
			standardNoButton.buttonLabel = "CANCEL";
			
			break;
			
		case Type.Waiting:
			commonPanel.SetActive(false);
			waitPanel.SetActive(true);
			break;

		case Type.Story:
			headerText.text = "";
			mainPanel.SetActive(false);
			storyPanel.gameObject.SetActive(true);
			closeXButton.targetObject = storyPanel.gameObject;
			closeXButton.methodName = "OnSkip";
			break;

		case Type.IAP:
			headerText.text = "CREDIT EXCHANGE";
			mainPanel.gameObject.SetActive(false);
			iapPanel.Deploy();
			break;

		case Type.NotEnoughGems:

			headerText.text = "FAILED";
			bodyText.text = "You do not have enough Gems. You need " + salePrice + " Gems to do this, but you only have " + ProfileController.Gems + " Gems.";
			standardNoButton.gameObject.SetActive(true);

			break;

		case Type.NotEnoughCores:
			salePrice = 3;
			headerText.text = "FAILED";
			bodyText.text = "You do not have enough Cores. You need " + salePrice + " Cores to do this, but you only have " + ProfileController.ShipParts + " Core(s). \n\n Cores are gained from completely defeating the Raid bosses, which appear in secret missions on the Star Map.";
			standardNoButton.gameObject.SetActive(true);
			
			break;


		case Type.NotEnoughCredits:
			
			headerText.text = "FAILED";
			bodyText.text = "You do not have enough Credits. Shall we stock up?";

			standardYesButton.buttonLabel = "Get More Credits";
			standardYesButton.methodName = "OnMoreCredits";

			standardYesButton.gameObject.SetActive(true);
			standardNoButton.gameObject.SetActive(true);
			
			break;
			
		}

		headerText.text = headerText.text.ToUpper();

		if (standardYesButton.gameObject.activeInHierarchy) standardYesButton.SetOptions();
		if (standardNoButton.gameObject.activeInHierarchy) standardNoButton.SetOptions();
		if (standardBuyButton.gameObject.activeInHierarchy) standardBuyButton.SetOptions();

	}

	public void OnMoreCredits ()
	{
		IAPManager.RequestIAPData(); // Deploy (Type.IAP);
	}

	private string preSaveHeader, preSaveBody;

	public void DeployError (string header, string body)
	{
		preSaveHeader = header;
		preSaveBody = body;

		IAPManager.DisableListeners();

		Deploy(Type.Error);
	}

	string displayHeader, displayBody, displayYes, displayNo;
	
	public void Deploy (Type type, string header = "NULL", string body = "NULL", string yes = "CONFIRM", string no = "CLOSE")
	{

		displayHeader = header;
		displayBody = body;
		displayYes = yes;
		displayNo = no;



		if (currentType != nextType && nextType != Type.None) currentType = nextType;

//		Debug.Log ("Deploy from " + currentType.ToString() + " to " + type.ToString());

		if (currentType != type) {

			InputController.LockControls();
			KJTime.Pause();
			nextType = type;

			if (currentType == Type.None) {
				SwitchToType();
			} else {
				ClosePanelToNext();
			}

		}
	}
	
	public void SetItem (ItemBasic item)
	{
		this.item = item;
	}
	
	void OnPress ()
	{
		gameObject.SetActive(false);
	}

	ItemShip itemShip;

	void OnSpend ()
	{

		if (type == Type.ViewAd || type == Type.ViewAdSC) {

            //UIController.ClosePopUp();
           // AdManagerUnity.Singleton.ShowAdHC();
			AdmobManager.Singleton.ShowAdHC();
			//MainMenuController.Singleton.adsUnity.GetComponent<AdManagerUnity>().ShowAd();
			//KJTime.Add (AdManager.ShowAd , 0.15f, 1);
			return;
		}

		// Spend HC First then execute Later

		if (type == Type.Preview) {

			if (ProfileController.Gems >= salePrice)
			{

				// Subtract the Gems and Execute the Preview.
				ProfileController.Gems -= salePrice;
				UIController.UpdateCurrency();
				ClosePanel();

				// Go to the actual preview.
				KJTime.RemoveGameTimers();
				ProfileController.allyStandby = false;
				ProfileController.previewMode = true;
				ProfileController.previewShip = item.id;
				Debug.Log ("Enter Preview For Ship: " + item.id);
				MenuController.LoadToScene(1);

				return;
				
			} else {

				// Not enough Gems.
				Deploy(Type.NotEnoughGems);
				return;
				
			}
		}

		if (type == Type.UnlockShip) {

			if (item.isSpecialShip) {
				if (ProfileController.ShipParts < salePrice)
				{
					
					Deploy(Type.NotEnoughCores);
					return;
					
				} else {

					itemShip = (ItemShip)item;
					ExecuteUnlockShip();
					UIController.UpdateCurrency();
					return;

				}
			} else {

				if (ProfileController.Credits < salePrice) {
					Deploy(Type.NotEnoughCredits);
					return;
				}
			}

		} else {

			if (ProfileController.Credits < salePrice) {	
				Deploy(Type.NotEnoughCredits);
				return;
			}
		}


		switch (type)
		{
			case Type.UnlockShip: 

				itemShip = (ItemShip)item;
				ExecuteUnlockShip();

			break;

			case Type.BuyHearts: 

				ProfileController.Credits -= salePrice;
				ProfileController.RestoreHeart();
				ClosePanel();

			break;

			case Type.Revive:

				ProfileController.Credits -= salePrice;
				GameController.Revive();
				ClosePanel();

			break;
		}

		UIController.UpdateCurrency();
	}
	
	void OnYes ()
	{	
		
		switch (type)
		{
			
		case Type.Sell: 
			ExecuteSell();
			break;
			
		case Type.Upgrade: 
			ExecuteUpgrade(); 
			break;

		case Type.YesOrNo:
			if (OnYesDelegate != null ) OnYesDelegate();
			break;

		case Type.SingleYes:
			if (OnYesDelegate != null ) OnYesDelegate();
			break;

		case Type.Error:
			if (UIController.tryAgainDelegate != null) UIController.tryAgainDelegate();
			break;
			
		}
		
//		ClosePanel();
		UIController.UpdateCurrency();
	}

	void OnWatchVideo ()
	{
		if(ProfileController.Singleton.adsReviveAvailable)
			ShowAdsForRevive();
			//UIController.ShowPopupYesOrNo("FREE REVIVE","Do you want to watch a video ad for a free revive?",ShowAdsForRevive);
		else
		{
			//UIController.ShowPopup("ERROR","You have already used your free revival for this mission. You can only free revive once per mission.");
			UIController.ShowPopup("ERROR","You have already used your free revival for this mission. You can only free revive once per battle.");
			ClosePanelToNext();
		}

	}

	
	void ShowAdsForRevive ()
	{
		//AdManagerUnity.Singleton.ShowAdHC("",true);
		AdmobManager.Singleton.ShowAdHC("",true);
		//ClosePanel();
		//KJTime.Pause();
		GameController.Singleton.puaseForRevive = true;

	}

	void OnNo ()
	{
		ClosePanel();
	}
	
	void ExecuteUpgrade ()
	{
		
		if (ProfileController.Gems >= salePrice)
		{
			item.upgradeLevel ++;
			item.Calibrate();

			if (item.upgradeLevel == 5)
				Achievement.AchievementComplete(Achievement.AchievementType.A10UpgradeItem);
			
			ProfileController.Gems -= salePrice;
			UIController.UpdateCurrency();
			ItemMenuController.Singleton.RefreshPreviewFromUpgrade();



			if (SocialController.isLoggedIn) UIController.ShowWait(15, OnUpgradeFail, ExecuteUpgrade, null, false);

			DataController.SaveAllData(false, ClosePanel, OnUpgradeFail);


		} else {

			UIController.popupPanel.salePrice = salePrice;
			Deploy(Type.NotEnoughGems);

		}
	}

	void DelayedUpgradeSave ()
	{

	}

	void OnUpgradeFail ()
	{
		// Refund the money.
		ProfileController.Gems += salePrice;

		// Undo the upgrade.
		item.upgradeLevel --;

		item.Calibrate();
		UIController.UpdateCurrency();
		ItemMenuController.Singleton.RefreshPreviewFromUpgrade();
		UIController.ShowComplexError(ExecuteUpgrade);

	}

	void ExecuteUnlockShip ()
	{
		if (item.isSpecialShip) {

			if (ProfileController.ShipParts >= salePrice)
			{
				ProfileController.ShipParts -= salePrice;
				ProfileController.UnlockShip(itemShip.id);
				
				if (SocialController.isLoggedIn) UIController.ShowWait(15, OnUnlockShipFail, ExecuteUnlockShip, null, false);
				DataController.SaveAllData(false, OnUnlockShipSuccess, OnUnlockShipFail);
				
				
			} else {
				
				Deploy(Type.NotEnoughCores);
				
			}


		} else {
			if (ProfileController.Credits >= salePrice)
			{
				ProfileController.Credits -= salePrice;
				ProfileController.UnlockShip(itemShip.id);
				
				if (SocialController.isLoggedIn) UIController.ShowWait(15, OnUnlockShipFail, ExecuteUnlockShip, null, false);
				DataController.SaveAllData(false, OnUnlockShipSuccess, OnUnlockShipFail);
				
				
			} else {
				
				Deploy(Type.NotEnoughCredits);
				
			}
		}

	}

	void OnUnlockShipFail ()
	{
		if (item.isSpecialShip) {

			ProfileController.ShipParts += salePrice;
			ProfileController.LockShip(item.id);
			UIController.ShowComplexError(ExecuteUnlockShip);

		} else {

			ProfileController.Credits += salePrice;
			ProfileController.LockShip(item.id);
			UIController.ShowComplexError(ExecuteUnlockShip);

		}

	}

	void OnUnlockShipSuccess ()
	{

		Achievement.AchievementComplete(Achievement.AchievementType.A9UnlockShip);

		ProfileController.EquipShip(itemShip);
		ShipMenuController.Singleton.GoBackToShip(true);
		FlurryController.ShipBuying(itemShip.Name, itemShip.shipType);
		ClosePanel();
	}

	bool sellItemWasEquip = false;
	
	void ExecuteSell ()
	{
		
		if (ProfileController.Singleton.equipList.Contains(item))
		{
			ProfileController.Singleton.equipList.Remove(item);
			sellItemWasEquip = true;
		} else if (ProfileController.Singleton.inventoryList.Contains(item)) {
			ProfileController.Singleton.inventoryList.Remove(item);
			sellItemWasEquip = false;
		}
		
		ProfileController.Gems += salePrice;


		if (SocialController.isLoggedIn) UIController.ShowWait(15, OnSellFail, ExecuteSell, null, false);

		DataController.SaveAllData(false, OnSellComplete, OnSellFail);

	}

	void OnSellComplete ()
	{
		ItemMenuController.Singleton.GoBackToInventory();
		UIController.ClosePopUp();
	}

	void OnSellFail ()
	{
		// Refund the money.
		ProfileController.Gems -= salePrice;
		
		// Undo the upgrade.
		if (sellItemWasEquip)
		{
			ProfileController.Singleton.equipList.Add(item);
		} else {
			ProfileController.Singleton.inventoryList.Add(item);
		}

		UIController.UpdateCurrency();
		UIController.ShowComplexError(ExecuteSell);
	}

	public void FadeIn () {
		//		Debug.Log ("FadeOut Order");
		blackCover.alpha = 1.00f;
		
		gameObject.SetActive(true);
		blackCover.gameObject.SetActive(true);
		standardPanel.gameObject.SetActive(false);
		commonPanel.gameObject.SetActive(false);

	}

	public void FadeOut () {
//		Debug.Log ("FadeOut Order");
		blackCover.alpha = 1.00f;

		gameObject.SetActive(true);
		blackCover.gameObject.SetActive(true);
		standardPanel.gameObject.SetActive(false);
		commonPanel.gameObject.SetActive(false);

		iTween.Stop(gameObject);
		iTween.ValueTo(gameObject, panelTweenOut);
//		ClosePanel();
	}
	
	public void ClosePanel ()
	{
		if (type == Type.ViewAd || type == Type.ViewAdSC) {
			MenuController.LoadToScene(ProfileController.Singleton.loadSectorWhenEndAds);
		}

		if (fallBackType != Type.None) {

			nextType = fallBackType;
			fallBackType = Type.None;

		} else {

			nextType = Type.None;

		}

		// Only Lock and Close if the current is NOT None.
		if (currentType != Type.None) ClosePanelToNext();

	}

	void ClosePanelToNext ()
	{

		InputController.LockControls();

		if (nextType == Type.None) { 
			IAPManager.DisableListeners();
			lockBlacker = false; 
		} else { 
			lockBlacker = true; 
		}

		if (currentType == Type.Waiting) UIController.ClearTimeOutDelegates();

		if (nextType == Type.NotEnoughCredits) {
			fallBackType = currentType;
//			Debug.Log ("Set Fallback: " + fallBackType.ToString());
		}

		iTween.Stop(gameObject);
		iTween.ValueTo(gameObject, panelTweenOut);
	}
	
	public void SwapEquip (int id)
	{
		// Swap the currently previewed item into the given equipment slot.
		
		ItemBasic tempItem = ProfileController.Singleton.equipList[id];
		
		ProfileController.Singleton.equipList[id] = item;
		ProfileController.Singleton.inventoryList.Remove(item);
		ProfileController.Singleton.inventoryList.Add(tempItem);
		
		ClosePanel();
		ItemMenuController.Singleton.GoBackToInventory();
		
	}
}
