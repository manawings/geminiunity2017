﻿using UnityEngine;
using System.Collections;

public class Shutter : KJBehavior {
	
	public UISprite topPanel;
	public UISprite bottomPanel;
	
	const float InPosition = 150.0f;
	const float OutPosition = 436.0f;
	const float TweenTime = 0.40f;
	
	float topOrigin, topFactor, botOrigin, botFactor;
	
	Hashtable tweenIn, tweenOut;
	
	void Awake ()
	{
		tweenIn = new Hashtable();
        tweenIn.Add("from", 0.0f);
        tweenIn.Add("to", 1.0f);
        tweenIn.Add("time", TweenTime);
        tweenIn.Add("onupdate", "Run");
		tweenIn.Add("easetype", iTween.EaseType.easeOutCirc);
		
        tweenOut = new Hashtable();
        tweenOut.Add("from", 0.0f);
        tweenOut.Add("to", 1.0f);
        tweenOut.Add("time", TweenTime + 0.15f);
        tweenOut.Add("onupdate", "Run");
		tweenOut.Add("easetype", iTween.EaseType.easeInOutCirc);
		
		TimeSpaceType = KJTime.SpaceType.UI;
		
	}

	void Start () {
		
		MenuController.SetShutterAsCurrent(this);
		
		if (MenuController.isShutterClosed)
		{
			CloseShutters(true);
			OpenShutters(false);
		} else {
			OpenShutters(true);	
		}	
	}
	
	public void CloseShutters (bool instant = false)
	{
		if (instant)
		{
			KJMath.SetY(topPanel, InPosition);
			KJMath.SetY(bottomPanel, -InPosition);
			return;	
		}

		KJCanary.PlaySoundEffect("ShutterClose");

		InputController.LockControls();
		iTween.Stop(gameObject);
		
		topOrigin = topPanel.transform.localPosition.y;
		botOrigin = bottomPanel.transform.localPosition.y;
		topFactor = InPosition - topOrigin;
		botFactor = - InPosition - botOrigin;
		
		gameObject.SetActive(true);
		iTween.ValueTo(gameObject, tweenIn);
		AddTimer(OnClosed, TweenTime + 0.05f, 1);

	}
	
	public void OpenShutters (bool instant = false)
	{
		if (instant)
		{
			KJMath.SetY(topPanel, OutPosition);
			KJMath.SetY(bottomPanel, -OutPosition);
			gameObject.SetActive(false);
			return;	
		}

		KJCanary.PlaySoundEffect("ShutterOpen");
		
		iTween.Stop(gameObject);
		
		topOrigin = topPanel.transform.localPosition.y;
		botOrigin = bottomPanel.transform.localPosition.y;
		topFactor = OutPosition - topOrigin;
		botFactor = - OutPosition - botOrigin;
		
		iTween.ValueTo(gameObject, tweenOut);
		AddTimer(OnOpened, TweenTime + 0.20f, 1);
		UIController.ClearCoroutines();

		KJCanary.Singleton.SetPosition();
		KJTime.Resume();

	}
	
	void Run (float val)
	{
		KJMath.SetY(topPanel, val * topFactor + topOrigin);
		KJMath.SetY(bottomPanel, val * botFactor + botOrigin);

	}
	
	void OnClosed ()
	{
		RemoveAllTimers();
		MenuController.OnShutterClosed();
	}
	
	void OnOpened ()
	{
		RemoveAllTimers();
		InputController.UnlockControls();
		gameObject.SetActive(false);
	}
}
