﻿using UnityEngine;
using System.Collections;

public class MarkerFlash : KJBehavior {

	public Color flashColor;

	float currentWave = 0.0f, waveIncreaser = 0.03f;
	bool goingUp = true;

	float anchorY = -12.0f, moveRange = 3.0f;

	UISprite sprite;

	// Use this for initialization
	void Start () {
		sprite = GetComponent<UISprite>();
//		Run();
//		AddTimer(Run);
	}
	
	// Update is called once per frame
	void Update () {

//		if (goingUp) {
//			currentWave += waveIncreaser;
//			if (currentWave > 1.0f) {
//				currentWave = 1.0f;
//				goingUp = false;
//			}
//		} else {
//			currentWave -= waveIncreaser;
//			if (currentWave < 0.0f) {
//				currentWave = 0.0f;
//				goingUp = true;
//			}
//		}
//
//		sprite.color = Color.Lerp (Color.black, flashColor, currentWave);


		float sinFactor = Mathf.Sin(Time.time * 2.0f);
		float colorFactor =  Mathf.Abs(Mathf.Sin(Time.time * 3.0f));
		float newY = anchorY + sinFactor * moveRange;
		
//		glowSprite.color = Color.Lerp(glowColor, originalColor, colorFactor);
		sprite.color = Color.Lerp (Color.black, flashColor, colorFactor);
		
		KJMath.SetY(this, newY);
	}
}
