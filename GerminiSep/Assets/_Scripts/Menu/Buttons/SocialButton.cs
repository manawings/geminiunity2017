﻿using UnityEngine;
using System.Collections;

public class SocialButton : MonoBehaviour {

	int newValue;
	public GameObject itemAlert;
	public UILabel infoLabel;


	void Start ()
	{
		SetNewValue(0);
	}

	public void SetNewValue (int newValue = 0)
	{
		this.newValue = newValue;

		if (newValue == 0) {
			itemAlert.SetActive(false);
		} else {
			itemAlert.SetActive(true);
			infoLabel.text = newValue.ToString();
		}
	}
}
