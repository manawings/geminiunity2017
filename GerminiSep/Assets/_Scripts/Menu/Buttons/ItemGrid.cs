using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemGrid : MonoBehaviour
{
	public GameObject itemSlotPrefab;
	int maxItemRows, maxItemPlates;
	
	private const float panelSize = 40.0f;
	private const float extendedPanelSize = 0.0f;
	private const float panelGap = 10.0f;
	private const int slotsPerRow = 4;
	
	
	// Inventory and Cargo System
	public InventoryPanel inventoryPanel;
	GameObject currentHolder, targetHolder;
	
	// New Slot Holder System
	
	List<ItemSlot> holderSlots1 = new List<ItemSlot>();
	List<ItemSlot> holderSlots2 = new List<ItemSlot>();

	public TutorialPrompter tutorialPrompter;
	
	int itemsPerHolder = 8;
	const float holderOffset = 225.0f;
	Hashtable tweenHash;
	bool isTweeningLeft;
	
	public GameObject leftButton, rightButton;

	float topCutOff, topOrigin, bottomCutOff, oldClipPos;
	
	UIPanel uiPanel;
	
	List<ItemSlot> itemSlotList = new List<ItemSlot>();
	List<ItemBasic> proxyList;
	
	void Awake ()
	{
		uiPanel = GetComponent<UIPanel>();
		tweenHash = new Hashtable();
		tweenHash.Add("from", 0.0f);
        tweenHash.Add("to", 1.0f);
        tweenHash.Add("time", 0.35f);
        tweenHash.Add("onupdate", "RunSlide");
		tweenHash.Add("oncomplete", "OnSlideComplete");
		tweenHash.Add("easetype", iTween.EaseType.easeOutCirc);
	}
	
	void CreateItemPlates ()
	{
		
		// Make sure GameObjects have already been made.
		
		if (currentHolder == null) {
			currentHolder = new GameObject("Current Holder (1)");
			targetHolder = new GameObject("Target Holder (2)");
		}
		
		/// Math.

		maxItemRows = 2; // + (int) Mathf.Ceil(uiPanel.clipRange.w / (panelSize + panelGap));
		maxItemPlates = maxItemRows * slotsPerRow;
		
		topOrigin = ((uiPanel.clipRange.w - panelSize - extendedPanelSize) * 0.5f) - uiPanel.clipSoftness.y;
		topCutOff = ((uiPanel.clipRange.w + panelSize + extendedPanelSize) * 0.5f) + panelGap;
		bottomCutOff = -topCutOff;
		oldClipPos = 0.0f;
		
		/// Clear the list.

		itemSlotList.Clear();
		holderSlots1.Clear();
		holderSlots2.Clear();
		
		// TEMP Find X Position.
		float xSize = 0.5f * (slotsPerRow - 1) * (panelSize + panelGap);
		float xStart = - xSize;
		float xFactor = panelSize + panelGap;
		
		GameObject proxyHolder;
		List<ItemSlot> proxySlotList;
		
		for (int k = 0 ; k < 2 ; k ++ )
		{
		
			if (k == 0) {
				proxyHolder = currentHolder;
				proxySlotList = holderSlots1;
				
			} else {
				proxyHolder = targetHolder;
				proxySlotList = holderSlots2;
			}
			
			proxyHolder.transform.parent = transform;
			proxyHolder.transform.localScale = Vector3.one;
			proxyHolder.transform.localPosition = Vector3.zero;
			
			for (int i = 0 ; i < maxItemRows ; i ++ )
			{
				
				/// Create and physically put the plate into position.
				
				
				float newYPosition = topOrigin - (panelGap + panelSize) * i;
				
				// Create a new row of items
				
				for (int j = 0 ; j < slotsPerRow ; j ++)
				{
					
					GameObject newItemPlate;
				
					newItemPlate = (GameObject) Instantiate(itemSlotPrefab);
					
					newItemPlate.transform.parent = proxyHolder.transform;
					newItemPlate.transform.localScale = Vector3.one;
					newItemPlate.transform.localPosition = Vector3.zero;
					
					
					float newXPosition = xStart + xFactor * j;
					KJMath.SetX(newItemPlate.transform, newXPosition);
					KJMath.SetY(newItemPlate.transform, newYPosition);
					
					ItemSlot itemSlot = newItemPlate.GetComponent<ItemSlot>();
					proxySlotList.Add(itemSlot);
					itemSlotList.Add (itemSlot);

					if (i == 0 && j == 0 && k == 0)
					{
						tutorialPrompter.button = itemSlot;
						tutorialPrompter.glowSprite =  itemSlot.iconGraphic;
						tutorialPrompter.DeployTutorial();
					}
				}	
			}
			
		}
	}
	
	public void Refresh (int pageId = 0)
	{
		
		inventoryPanel.pageCurrent = pageId;
		
		leftButton.SetActive(pageId != 0);
		rightButton.SetActive(pageId != InventoryPanel.pageMax);
		
		if (itemSlotList.Count == 0) CreateItemPlates();
		
		List<ItemBasic> copyList = new List<ItemBasic>();
		proxyList = ProfileController.Singleton.inventoryList;
		
		GameObject proxyHolder;
		List<ItemSlot> proxySlotList;
		
		for (int k = 0 ; k < 2 ; k ++ )
		{
			
			if (k == 0) {
				proxyHolder = currentHolder;
				proxySlotList = holderSlots1;
				KJMath.SetX(proxyHolder.transform, 0.0f);
				
			} else {
				proxyHolder = targetHolder;
				proxySlotList = holderSlots2;
				KJMath.SetX(proxyHolder.transform, holderOffset);
			}
			
			for (int i = 0 ; i < proxySlotList.Count ; i ++ )
			{
				
				ItemSlot itemSlot = proxySlotList[i];
				
				float rowIndex = Mathf.Floor((float) i / (float) slotsPerRow);
				float newYPosition = topOrigin - (panelGap + panelSize) * rowIndex;
				
				KJMath.SetY(itemSlot.transform, newYPosition);
				
				int itemId = i + (inventoryPanel.pageCurrent + k) * itemsPerHolder;
				itemSlot.SetItem(itemId);
				
			}
		}
	}
	
	public void SlideLeft ()
	{
		// Check if action is possible.
		if (inventoryPanel.pageCurrent >= InventoryPanel.pageMax) return;
		
		InputController.LockControls();
		
		// Populate the target panel with the correct icons
		for (int i = 0 ; i < holderSlots2.Count ; i ++ )
		{
				
			ItemSlot itemSlot = holderSlots2[i];
				
			float rowIndex = Mathf.Floor((float) i / (float) slotsPerRow);
			float newYPosition = topOrigin - (panelGap + panelSize) * rowIndex;
				
			KJMath.SetY(itemSlot.transform, newYPosition);
				
			int itemId = i + (inventoryPanel.pageCurrent + 1) * itemsPerHolder;
			itemSlot.SetItem(itemId);
				
		}
		
		// Position the target panel on the correct side
		isTweeningLeft = true;
		KJMath.SetX(targetHolder.transform, holderOffset);
		
		// Activate the tweens.
		iTween.ValueTo(gameObject, tweenHash);
		
	}
	
	public void SlideRight ()
	{
		if (inventoryPanel.pageCurrent <= 0) return;
		
		InputController.LockControls();
		
		// Populate the target panel with the correct icons
		for (int i = 0 ; i < holderSlots2.Count ; i ++ )
		{
				
			ItemSlot itemSlot = holderSlots2[i];
				
			float rowIndex = Mathf.Floor((float) i / (float) slotsPerRow);
			float newYPosition = topOrigin - (panelGap + panelSize) * rowIndex;
				
			KJMath.SetY(itemSlot.transform, newYPosition);
				
			int itemId = i + (inventoryPanel.pageCurrent - 1) * itemsPerHolder;
			itemSlot.SetItem(itemId);
				
		}
		
		// Position the target panel on the correct side
		isTweeningLeft = false;
		KJMath.SetX(targetHolder.transform, - holderOffset);
		
		// Activate the tweens.
		iTween.ValueTo(gameObject, tweenHash);
	}
	
	void RunSlide (float val)
	{
		if (isTweeningLeft)
		{
			KJMath.SetX (currentHolder.transform, val * - holderOffset);
			KJMath.SetX (targetHolder.transform, (1.0f - val) * holderOffset);
		} else {
			KJMath.SetX (currentHolder.transform, val * holderOffset);
			KJMath.SetX (targetHolder.transform, (1.0f - val) * - holderOffset);
		}
	}
	
	void OnSlideComplete ()
	{
		int newPageId = inventoryPanel.pageCurrent;
		newPageId += isTweeningLeft ? 1 : -1;
		Refresh(newPageId);
		
		InputController.UnlockControls();
	}
	
	/*
	void CheckPlateTop ()
	{
		ItemSlot topItem = itemSlotList[0];
		ItemSlot endItem = itemSlotList[itemSlotList.Count - 1];
		
		if (endItem.itemId >= proxyList.Count - 1) return;
		
		if (topItem.transform.localPosition.y - uiPanel.clipRange.y > topCutOff)
		{
			
			
			float newY = endItem.transform.localPosition.y - panelGap - panelSize;
			
			for (int i = 0 ; i < slotsPerRow ; i ++ )
			{
				
				topItem = itemSlotList[0];
				endItem = itemSlotList[itemSlotList.Count - 1];
				
				topItem.SetItem(endItem.itemId + 1);
				itemSlotList.RemoveAt(0);
				itemSlotList.Add(topItem);
				KJMath.SetY(topItem, newY);
			}
		}
	}
	
	void CheckPlateBottom ()
	{

		ItemSlot bottomItem = itemSlotList[itemSlotList.Count - 1];
		ItemSlot endItem = itemSlotList[0];
		
		if (endItem.itemId == 0) return;
		
		if (bottomItem.transform.localPosition.y - uiPanel.clipRange.y < bottomCutOff)
		{
			
			float newY = endItem.transform.localPosition.y + panelGap + panelSize;
			
			for (int i = 0 ; i < slotsPerRow ; i ++)
			{
				
				bottomItem = itemSlotList[itemSlotList.Count - 1];
				endItem = itemSlotList[0];
				
				bottomItem.SetItem(endItem.itemId - 1);
				itemSlotList.RemoveAt(itemSlotList.Count - 1);
				itemSlotList.Insert(0, bottomItem);
				KJMath.SetY(bottomItem, newY);
			}
			
		}
	}
	*/
}

