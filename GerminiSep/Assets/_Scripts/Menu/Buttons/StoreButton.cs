﻿using UnityEngine;
using System.Collections;

public class StoreButton : MonoBehaviour {

	#if UNITY_IPHONE
	public UILabel itemLabel;
	public UILabel itemDescLabel;
	//public StoreKitProduct product;
	public StoreObject storeObject;

	public int itemId = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetStoreItem (StoreObject storeObject, int id)
	{
		itemId = id;
		this.storeObject = storeObject;
		itemLabel.text = storeObject.name+" / +" + storeObject.units;
		itemDescLabel.text = "???";
	}

//	public void CalibrateWithProduct (StoreKitProduct product) 
//	{
//		itemDescLabel.text = product.formattedPrice;
//		this.product = product;
//	}
//
//	void BuyIAP ()
//	{
//		if (product != null) {
//			UIController.ShowWait();
//			StoreKitBinding.purchaseProduct( product.productIdentifier, 1 );
//		}
//	}
	#endif
}
