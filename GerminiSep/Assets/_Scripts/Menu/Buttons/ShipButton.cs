using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipButton : MonoBehaviour
{

	public UILabel itemLabel;
	public UILabel itemDescLabel;
	public UILabel hcLabel;

//	public UISprite greenButton;
//	public UISprite redButton;

	public UISprite shipIcon, hcSprite;

	public static Color activeColor = new Color32(161, 255, 107, 255);
	public static Color lockedColor = new Color32(255, 75, 75, 255);
	public static Color standbyColor = new Color32(144, 255, 232, 255);
	
	public int itemId = 0;
	
	ItemBasic item; 
	ItemShip ship;
	List<ItemShip> currentList;
	
	public void SetCurrentList (List<ItemShip> proxyList)
	{
		currentList = proxyList;
	}
	
	public void SetShip (int id, bool isCurrent = false)
	{
		itemId = id;
		
		if (id < currentList.Count)
		{
			ship = currentList[id];
			shipIcon.spriteName = ship.shipClip.Replace("ps_", "p") + "Off";
			shipIcon.MakePixelPerfect();
		} else {
			ship = null;
		}
		
		if (ship == null) {
			gameObject.SetActive(false);
		} else {
			gameObject.SetActive(true);
			SetShip(ship, isCurrent);
		}
	}
	
	public void SetShip (ItemShip ship, bool isCurrent = false)
	{
		itemLabel.text = ship.Name.ToUpper();
//		itemLabel.color = ship.TextColor;

		hcLabel.text = "";
		hcSprite.gameObject.SetActive(false);
		
		if (ProfileController.IsShipUnlocked(ship.id))
		{

//			greenButton.gameObject.SetActive(true);
//			redButton.gameObject.SetActive(false);


			itemDescLabel.text = "Status - Standby";
			itemDescLabel.color = standbyColor;

		} else {

//			greenButton.gameObject.SetActive(false);
//			redButton.gameObject.SetActive(true);

			itemDescLabel.text = "Status - Locked";
			itemDescLabel.color = lockedColor;

			hcLabel.text = ship.cost.ToString();
			hcSprite.gameObject.SetActive(true);

			if (ship.isSpecialShip) {
				hcSprite.spriteName = "XCSmall_v3";
				hcLabel.color = new Color32 (96, 255, 189, 255);
			} else {
				hcSprite.spriteName = "HCSmall_v3";
				hcLabel.color = new Color32 (255, 232, 171, 255);
			}

			hcSprite.MakePixelPerfect ();


		}

		if (isCurrent) {
			itemDescLabel.text = "Status - Active";
			itemDescLabel.color = activeColor;
		}

		// itemDescLabel.color = Color.red;
	}
	
	public void Clear ()
	{
		gameObject.SetActive(false);
	}
	
	void ClickSelectShip ()
	{
		ShipMenuController.Singleton.GoToPreviewFor(ship);
	}
}

