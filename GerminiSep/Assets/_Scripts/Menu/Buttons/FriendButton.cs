﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FriendButton : MonoBehaviour {

	public int itemId, friendId;
	List<FriendItem> friendItemList;

	public UILabel mainLabel;
	public UILabel descLabel;

	public FriendItem friendItem;

	public UISprite shipSprite;
	public UISprite gemSprite;

	public UITexture fbTexture;

	public UIDragPanelContents dragPanelContent;

	public void SetItem (int newId)
	{

		// friendItem = friendItemList[newId];
		itemId = newId;

		if (newId < friendItemList.Count)
		{
			friendItem = friendItemList[newId];
			// shipIcon.spriteName = ship.shipClip;
		} else {
			friendItem = null;
		}
		
		if (friendItem == null) {
			gameObject.SetActive(false);
		} else {
			gameObject.SetActive(true);
			SetItem(friendItem);
		}
	}

	public void SetItem (FriendItem friendItem)
	{
		// if
		this.friendItem = friendItem;

		fbTexture.gameObject.SetActive(false);
		shipSprite.gameObject.SetActive(false);
		gemSprite.gameObject.SetActive(false);

		if (friendItem.status == FriendItem.FriendStatus.FriendReward || friendItem.status == FriendItem.FriendStatus.GoldReward) {

			gemSprite.gameObject.SetActive(true);


			if (friendItem.status == FriendItem.FriendStatus.GoldReward) {
				gemSprite.spriteName = "HC_Big";
				mainLabel.text = "Credit Reward";
				descLabel.text = "Collect your daily reward.";
			} else {
				gemSprite.spriteName = "SC_Big";
				mainLabel.text = "Bounty Reward";
				descLabel.text = "Collect your assist bounty.";
			}

		} else {

			descLabel.text = string.Empty;
			KJMath.SetY(mainLabel.transform , 0);

			if (SocialController.FriendListType == SocialController.ListType.Facebook)
			{
				mainLabel.text = friendItem.realName;

				if (friendItem.fbTexture != null) {
					SetFBTexture(friendItem.fbTexture);
				}

			} else {

				mainLabel.text = friendItem.displayName.ToUpper();

				ItemShip itemShip = ItemPattern.ShipList[friendItem.shipId];
//				shipSprite.spriteName = itemShip.shipClip;
				shipSprite.spriteName = itemShip.shipClip.Replace("ps_", "p") + "Off";
				shipSprite.gameObject.SetActive(true);

			}
		}
	}

	public void SetFBTexture (Texture textureFromFB)
	{
		fbTexture.gameObject.SetActive(true);
		fbTexture.mainTexture = textureFromFB;
	}

	public void SetCurrentList (List<FriendItem> proxyList)
	{
		friendItemList = proxyList;
	}

	void ClickSelectFriend ()
	{
		if (ProfileController.Singleton.isOffline) {
			SocialController.friendItem = friendItem;

			if (friendItem.status == FriendItem.FriendStatus.FriendReward || friendItem.status == FriendItem.FriendStatus.GoldReward) {

				ProfileController.rewardItem = this.friendItem;

				if (ProfileController.rewardItem.status == FriendItem.FriendStatus.GoldReward) {

					ProfileController.Singleton.hasGoldReward = false;
					string daySession = NistTime.DaySession(1);
					PlayerPrefs.SetString(ProfileController.keyDaySession, daySession);
					ProfileController.Credits += ProfileController.rewardItem.rewardAmount;
					GoToRewardActual();
				//	DB.Singleton.CallDatabase("CollectGold", GoToRewardActual, null, "NULL", ProfileController.Credits);

				} else {

					ProfileController.HelpedFriends = 0;
					ProfileController.HelpedStrangers = 0;
					ProfileController.Gems += ProfileController.rewardItem.rewardAmount;
					GoToRewardActual(); 

				//	DB.Singleton.CallDatabase("CollectReward", GoToRewardActual, null, "NULL", ProfileController.Gems);

				}

			}
			return;
		}
			
		//below for online
		SocialController.friendItem = friendItem;

		if (friendItem.status == FriendItem.FriendStatus.FriendReward || friendItem.status == FriendItem.FriendStatus.GoldReward) {

			ProfileController.rewardItem = this.friendItem;
			UIController.ShowWait();
			
			if (ProfileController.rewardItem.status == FriendItem.FriendStatus.GoldReward) {

				ProfileController.Singleton.hasGoldReward = false;
				ProfileController.Credits += ProfileController.rewardItem.rewardAmount;
				DB.Singleton.CallDatabase("CollectGold", GoToRewardActual, null, "NULL", ProfileController.Credits);

			} else {

				ProfileController.HelpedFriends = 0;
				ProfileController.HelpedStrangers = 0;
				ProfileController.Gems += ProfileController.rewardItem.rewardAmount;
				DB.Singleton.CallDatabase("CollectReward", GoToRewardActual, null, "NULL", ProfileController.Gems);

			}

		} else {

			FriendListController.Singleton.GoToFriendProfile(friendItem.id);
		}
		// ShipMenuController.Singleton.GoToPreviewFor(ship);
	}

	void GoToRewardActual ()
	{
		UIController.ClosePopUp();
		FriendListController.Singleton.GoToRewardCollection();
	}

	public void Clear ()
	{
		gameObject.SetActive(false);
	}
}
