using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemButton : KJBehavior {

	public UILabel itemLabel;
	public UISprite greenButton;
	public UISprite redButton;
	
	public int itemId = 0;
	
	ItemBasic item; 
	List<ItemBasic> currentList;
	
	public void SetCurrentList (List<ItemBasic> proxyList)
	{
		currentList = proxyList;
	}
	
	public void SetItem (int id)
	{
		itemId = id;
		
		
		
		if (id < currentList.Count)
		{
			item = currentList[id];
		} else {
			item = null;
		}
		
		if (item == null) {
			gameObject.SetActive(false);
		} else {
			gameObject.SetActive(true);
			SetItem(item);
		}
	}
	
	public void SetItem (ItemBasic item)
	{
		itemLabel.text = item.Name;
		itemLabel.color = item.TextColor;
		
		if (true) // ProfileController.HasItem(item.type, item.id)
		{
			greenButton.gameObject.SetActive(true);
			redButton.gameObject.SetActive(false);
			
		} else {
			
			greenButton.gameObject.SetActive(false);
			redButton.gameObject.SetActive(true);
			
		}
	}
	
	public void SetSector (int level)
	{
		itemLabel.text = "Sector " + level;
		itemId = level;
		
		greenButton.gameObject.SetActive(true);
		redButton.gameObject.SetActive(false);
	}
	
	public void Clear ()
	{
		gameObject.SetActive(false);
	}
	
	void ClickSetSector ()
	{
		ProfileController.SetSector(itemId);
		ProfileController.SubSector = 0;
		MapMenuController.Singleton.OnBack();

	}
	
	void ClickSetItem ()
	{
		HangarController.Singleton.CalibratePreviewForItem(item);
	}
	
}
