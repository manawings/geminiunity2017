﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

public class ControlSceneController : MonoBehaviour {

	public BlitzButton controlButton, sensitivityButton;
	public UILabel controlLabel;


	void Start ()
	{
		SetButtonGraphics();
	}

    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }
    }

    void OnBack ()
	{

        MenuController.LoadToScene(8);
	}
	

	void OnControl ()
	{

		if (ProfileController.controlMode == 0) {
			ProfileController.controlMode = 1;
		} else {
			ProfileController.controlMode = 0;
		}

		PlayerPrefs.SetInt(DataController.tControlMode, ProfileController.controlMode);
		PlayerPrefs.Save();

		SetButtonGraphics();
	}

	void OnSensitivity ()
	{
		ProfileController.controlSensitivity += 0.25f;

		if (ProfileController.controlSensitivity > 2.0f) {
			ProfileController.controlSensitivity = 1.0f;
		}
		
		PlayerPrefs.SetFloat(DataController.tSensitivity, ProfileController.controlSensitivity);
		PlayerPrefs.Save();

		SetButtonGraphics();
	}


	void SetButtonGraphics ()
	{
		if (ProfileController.controlMode == 0) {

			controlButton.buttonLabel = "NORMAL";
			controlButton.SetOptions();
			sensitivityButton.gameObject.SetActive(false);
			controlLabel.text = "In the standard mode, the ship follows your finger.";
			KJMath.SetY(controlLabel, -15.0f);

		} else {

			controlButton.buttonLabel = "RELATIVE";
			controlButton.SetOptions();
			sensitivityButton.gameObject.SetActive(true);
			controlLabel.text = "In the relative mode, the ship's movement is mapped to your finger's movement. You may also adjust sensitivity.";
			KJMath.SetY(controlLabel, -70.0f);

			sensitivityButton.buttonLabel = "SENSITIVITY: " + ProfileController.controlSensitivity + "X" ;
			sensitivityButton.SetOptions();

		}
	}
}
