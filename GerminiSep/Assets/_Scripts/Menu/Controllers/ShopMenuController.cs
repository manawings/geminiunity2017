﻿using UnityEngine;
using System.Collections;

public class ShopMenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }
    }

    void OnBack ()
	{
        MenuController.LoadToScene(2);
	}
}
