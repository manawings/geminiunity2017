﻿using UnityEngine;
using System.Collections;

public class ShipMenuController : SlidingMenuController {

	public GameObject shipPanel;
	public GameObject previewPanel;
	public ItemShip previewShip;
	
	public static ShipMenuController Singleton {
		get; private set;	
	}
	
	protected void Start ()
	{
		Singleton = this;
		isShipPreview = false;
		CalibrateShipPanel();
		currentPanel = shipPanel;
		
		base.Start();
	}

    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }
    }

    void OnBack ()
    {
        if (currentPanel == shipPanel) MenuController.LoadToScene(2);
		if (currentPanel == previewPanel) GoBackToShip();
	}
	
	void CalibrateShipPanel (bool resetAtTop = false)
	{
		// shipPanel.GetComponent<InventoryPanel>().Deploy();
		shipPanel.GetComponentInChildren<ShipPanel>().Refresh(resetAtTop);
	}
	
	void CalibratePreviewPanel (ItemShip ship)
	{
		previewShip = ship;
		previewPanel.GetComponent<ShipPreviewPanel>().Calibrate(ship);
	}
	
	public void RefreshPreviewFromUpgrade ()
	{
		CalibratePreviewPanel(previewShip);
	}

	bool isShipPreview = false;
	
	public void GoToPreviewFor (ItemShip ship)
	{
		if (!isShipPreview) {
			isShipPreview = true;
			SwapTo(previewPanel, false);
			CalibratePreviewPanel(ship);
		}
	}
	
	public void GoBackToShip (bool resetAtTop = false)
	{
		if (isShipPreview) {
			isShipPreview = false; 
			SwapTo(shipPanel, true);
			CalibrateShipPanel(resetAtTop);
		}
	}

	void GoBack ()
	{

	}
}
