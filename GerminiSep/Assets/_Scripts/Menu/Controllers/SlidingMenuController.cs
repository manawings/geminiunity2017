using UnityEngine;
using System.Collections;

public class SlidingMenuController : MonoBehaviour
{
	
	protected GameObject currentPanel;
	protected GameObject targetPanel;

	Hashtable tweenHash;
	
	float outPosition, positionForCurrent, positionForTarget;
	
	protected void Start ()
	{
		
		outPosition = tk2dCamera.Instance.ScreenExtents.width;
		
		tweenHash = new Hashtable();
        tweenHash.Add("from", 0.0f);
        tweenHash.Add("to", 1.0f);
        tweenHash.Add("time", 0.5f);
        tweenHash.Add("onupdate", "Run");
		tweenHash.Add("oncomplete", "OnComplete");
		tweenHash.Add("easetype", iTween.EaseType.easeInOutQuad);
		
	}
	
	public void SwapTo (GameObject panelObject, bool backwards)
	{

		targetPanel = panelObject;
		
		if (backwards) {
			
			positionForCurrent = outPosition;
			positionForTarget = -outPosition;
			KJMath.SetX(targetPanel.transform, -outPosition);
			
		} else {
			
			positionForCurrent = -outPosition;
			positionForTarget = outPosition;
			KJMath.SetX(targetPanel.transform, outPosition);
			
		}
		
		targetPanel.SetActive(true);
		iTween.Stop(gameObject);
		iTween.ValueTo(gameObject, tweenHash);
		InputController.LockControls();
		
	}
	
	void Run (float val)
	{
		KJMath.SetX(targetPanel.transform, (1.0f - val) * positionForTarget);
		KJMath.SetX(currentPanel.transform, val * positionForCurrent);
	}
	
	void OnComplete ()
	{

		currentPanel.SetActive(false);
		currentPanel = targetPanel;
		InputController.UnlockControls();
		currentPanel.SendMessage("OnSlideComplete", SendMessageOptions.DontRequireReceiver);
	}
	
}

