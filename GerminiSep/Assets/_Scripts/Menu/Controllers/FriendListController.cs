﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FriendListController : MonoBehaviour {

	public GameObject friendPanel;
	
	public static FriendListController Singleton {
		get; private set;	
	}

	protected void Start ()
	{
		Singleton = this;
		CalibrateFriendPanel();
	}

    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }
    }

    void OnBack ()
	{
		MenuController.LoadToScene(9);
		//if (currentPanel == previewPanel) GoBackToShip();
	}
	
	void CalibrateFriendPanel ()
	{
		// shipPanel.GetComponent<InventoryPanel>().Deploy();
		friendPanel.GetComponentInChildren<FriendListPanel>().Refresh();
	}

	public void GoToFriendProfile (string friendId)
	{
		UIController.ShowWait();
		DB.Singleton.CallDatabase("RequestCompactProfile", OnSmallDataSuccess, null, friendId);

	}

	void OnSmallDataSuccess ()
	{
		UIController.ClosePopUp();
		FriendProfileController.isPostBattle = false;
		MenuController.LoadToScene(11);
	}

	public void GoToRewardCollection ()
	{
		MenuController.LoadToScene(14);
	}
}
