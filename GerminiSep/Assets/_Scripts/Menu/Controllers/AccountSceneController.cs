﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class AccountSceneController : MonoBehaviour {

	void Start ()
	{

	}
    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }
    }

    void OnBack ()
	{
        MenuController.LoadToScene(8);
	}

	void OnReset ()
	{
		
		UIController.ShowPopupYesOrNo("RESET DATA", "Warning! This will clear all login data, offline profiles and achievement data for this game. Online profiles will not be reset. Are you sure you want to continue?", OnResetActual);
	}

	void OnResetStory () {

		UIController.ShowPopupYesOrNo("RESET STORY", "This will clear your current story progress and set your map back to Sector One. You will still have your items, and can still fly to your current Sector. Proceed?", OnResetStoryActual);

	}

	void OnResetStoryActual () {

		if (SocialController.isLoggedIn) {
			
			// Call Data Base for small data and wait for response.
			UIController.ShowWait(30);

		}

		ProfileController.SetSector (1);
		ProfileController.SubSector = 1;
		ProfileController.Singleton.narrativeProgress = 0;

		DataController.SaveAllData (false, UIController.ClosePopUp);

	}

	void OnExitMenu () 
	{

		if (PlayerPrefs.GetInt("isOffline") == 1) {
			PlayerPrefs.SetInt ("OfflineSaved", 1);
			PlayerPrefs.Save ();
			DataController.SaveAllData ();
		} else {
			PlayerPrefs.DeleteKey ("OfflineSaved");
		}

		PlayerPrefs.DeleteKey("isOffline");
		PlayerPrefs.DeleteKey("isGuest");

		OnExitMenuActual ();
	}

	void OnExitMenuActual () {

		ProfileController.Reset();
		SocialController.isLoggedIn = false;
		ProfileController.Singleton.isGuest = 0;
		ProfileController.Singleton.isOffline = false;
		ProfileController.GemDouble = false;
		ProfileController.GemDoubleAsInt = 0;
//		if (FB.IsLoggedIn) FB.LogOut();
		Debug.Log("ProfileController.GemDouble" + ProfileController.GemDouble);
		MenuController.LoadToScene(0);

	}

	void OnResetActual ()
	{
		// PlayerPrefs.DeleteKey("isGuest"); PlayerPrefs.DeleteKey("isOffline");
//		AchievementManager.ResetAchievements(); // TODO: Temp
//		Achievement.ResetAchievements();
		
		// Clear Player Prefs. But Backup the Guest Key first.
		string backupGuestKey = DataController.GuestKey;
		
		// Back up all the data that we don't want to get erased by Player Prefs.
		string[] backupString = { AdManager.AdTimeKey, SocialController.tLastTimeCheck, SocialController.tServerTime };
		Dictionary<string, string> storeValue = new Dictionary<string, string>();
		
		foreach (string dataName in backupString) {
			storeValue.Add(dataName, PlayerPrefs.GetString(dataName, "0"));
		}
		
		PlayerPrefs.DeleteAll();
		DataController.GuestKey = backupGuestKey;
		
		// Restore all the data.
		foreach (string dataName in backupString) {
			PlayerPrefs.SetString( dataName, storeValue[dataName]);
		}
		
		PlayerPrefs.Save();
		OnExitMenuActual ();

		
	}

}
