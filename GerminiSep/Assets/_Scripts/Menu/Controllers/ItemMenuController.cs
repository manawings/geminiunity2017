﻿using UnityEngine;
using System.Collections;

public class ItemMenuController : SlidingMenuController {
	
	public GameObject inventoryPanel;
	public GameObject previewPanel;
	public ItemBasic previewItem;
	
	public static ItemMenuController Singleton {
		get; private set;	
	}
	
	protected void Start ()
	{
		//SocialController.isLoggedIn = true;
		//SocialController.id = "50d62555db1f74f093363ae65e3cb94c9564de28";
		//DB.Singleton.CallDatabase("Open_Cargo", null, null, "BM");
		//DB.Singleton.CallDatabase("IAP_HC", null, null, "HC1");
		//DB.Singleton.CallDatabase("HC_Used", null, null, null, 100);
		Singleton = this;
		CalibrateInventoryPanel();
		currentPanel = inventoryPanel;
		
		base.Start();
	}

    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }
    }

    void OnBack ()
	{
	    if (currentPanel == inventoryPanel) MenuController.LoadToScene(2);
		if (currentPanel == previewPanel) GoBackToInventory();
	}
	
	void CalibrateInventoryPanel ()
	{
		inventoryPanel.GetComponent<InventoryPanel>().Deploy();
		inventoryPanel.GetComponentInChildren<ItemGrid>().Refresh();
	}
	
	void CalibrateItemPanel (ItemBasic item)
	{
		previewItem = item;
		previewPanel.GetComponent<PreviewPanel>().Calibrate(item);
	}
	
	public void RefreshPreviewFromUpgrade ()
	{
		CalibrateItemPanel(previewItem);
	}
	
	public void GoToPreviewFor (ItemBasic item)
	{
		SwapTo(previewPanel, false);
		CalibrateItemPanel(item);
	}
	
	public void GoBackToInventory ()
	{
		SwapTo(inventoryPanel, true);
		CalibrateInventoryPanel();
	}
}
