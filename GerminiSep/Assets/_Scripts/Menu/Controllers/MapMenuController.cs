﻿using UnityEngine;
using System.Collections;

public class MapMenuController : MonoBehaviour {

	public MapPanel mapPanel;
	public GameObject navPanel;
	public GameObject backPanel;
	public GameObject lowPanel;
	public GameObject highPanel;

	public GameObject botPanel;

	public GameObject starPrefab;

	bool isPressed = false;

	Vector3 originPosition;
	float offsetPosition;
	Vector3 targetPosition;
	Vector3 currentPosition;
	Vector3 projectedOriginPosition;
	
	public static MapMenuController Singleton {
		get; private set;
	}

	public static void SetCurrentPosition (Vector3 currentPosition)
	{

		if (currentPosition.y < Singleton.mapPanel.panelMinY) currentPosition.y = Singleton.mapPanel.panelMinY;
		if (currentPosition.y > Singleton.mapPanel.panelMaxY) currentPosition.y = Singleton.mapPanel.panelMaxY;

		Singleton.targetPosition = currentPosition;
		Singleton.currentPosition = currentPosition;
	}
	
	void Start ()
	{

		originPosition = Vector3.zero;
		offsetPosition = 0.0f;

		targetPosition = Vector3.zero;
		projectedOriginPosition = Vector3.zero;
		currentPosition = Vector3.zero;

		CreateStars();

		Singleton = this;
		mapPanel.Refresh();
		isPressed = false;

//		if (ProfileController.forceMap) {
//			botPanel.SetActive(false);
//		}
	}

	void CreateStars ()
	{
		int maxWidth = 200;
		int maxHeight = 500;
		int maxStars = 50;

		while (maxStars > 0)
		{

			Color starColor = new Color();
			
			starColor.r =  0.3f + Random.value * 0.7f;
			starColor.g =  0.7f + Random.value * 0.3f;
			starColor.b =  0.8f + Random.value * 0.2f;
			starColor.a = 0.7f + Random.value * 0.3f;

			GameObject newStar = (GameObject) Instantiate(starPrefab);
			GameObject newStar2 = (GameObject) Instantiate(starPrefab);

			newStar.transform.parent = highPanel.transform;
			newStar2.transform.parent = highPanel.transform;
			newStar.GetComponent<UISprite>().MakePixelPerfect();
			newStar.GetComponent<UISprite>().color = starColor;
			newStar2.GetComponent<UISprite>().MakePixelPerfect();
			newStar2.GetComponent<UISprite>().color = starColor;

			Vector3 newPosition = Vector3.zero;
			newPosition.x = Random.Range(-maxWidth, maxWidth);
			newPosition.y = Random.Range(- maxHeight * 0.5f, maxHeight * 0.5f);

			newStar.transform.localPosition = newPosition;

			newPosition.y += maxHeight;
			newStar2.transform.localPosition = newPosition;


			maxStars--;
		}

		maxStars = 50;

		while (maxStars > 0)
		{

			Color starColor = new Color();

			starColor.r =  0.3f + Random.value * 0.7f;
			starColor.g =  0.7f + Random.value * 0.3f;
			starColor.b =  0.8f + Random.value * 0.2f;
			starColor.a = 0.3f + Random.value * 0.2f;

			GameObject newStar = (GameObject) Instantiate(starPrefab);
			GameObject newStar2 = (GameObject) Instantiate(starPrefab);
			
			newStar.transform.parent = lowPanel.transform;
			newStar2.transform.parent = lowPanel.transform;
			newStar.GetComponent<UISprite>().MakePixelPerfect();
			newStar.GetComponent<UISprite>().color = starColor;
			newStar2.GetComponent<UISprite>().MakePixelPerfect();
			newStar2.GetComponent<UISprite>().color = starColor;
			
			Vector3 newPosition = Vector3.zero;
			newPosition.x = Random.Range(-maxWidth, maxWidth);
			newPosition.y = Random.Range(- maxHeight * 0.5f, maxHeight * 0.5f);
			
			newStar.transform.localPosition = newPosition;
			
			newPosition.y += maxHeight;
			newStar2.transform.localPosition = newPosition;

			maxStars--;

		}
	}

    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }

        if (Input.GetMouseButtonDown(0) && !isPressed) {
			isPressed = true;
			OnTapDown();
		}

		if (Input.GetMouseButton(0) && isPressed) {
			OnTapMove();
		}

		if (Input.GetMouseButtonUp(0) && isPressed) {
			isPressed = false;
			OnTapUp();
		}

		currentPosition = Vector3.Lerp(currentPosition, targetPosition, 0.3f);
		currentPosition.x = 0.0f;

		navPanel.transform.localPosition = currentPosition;
		backPanel.transform.localPosition = currentPosition * 0.25f;
		highPanel.transform.localPosition = currentPosition * 0.75f;
		lowPanel.transform.localPosition = currentPosition * 0.5f;

		// Function to set the BG's to loop:
		Vector3 newBackPosition;

		newBackPosition = backPanel.transform.localPosition;
		newBackPosition.y = newBackPosition.y % 640;
		backPanel.transform.localPosition = newBackPosition;

		newBackPosition = highPanel.transform.localPosition;
		newBackPosition.y = newBackPosition.y % 500;
		highPanel.transform.localPosition = newBackPosition;

		newBackPosition = lowPanel.transform.localPosition;
		newBackPosition.y = newBackPosition.y % 500;
		lowPanel.transform.localPosition = newBackPosition;

	}

	void OnTapDown ()
	{
		if ( ProfileController.SectorsUnlocked < 4 ) return;
		originPosition = Input.mousePosition;
		projectedOriginPosition = currentPosition;
	}

	void OnTapMove ()
	{
		if ( ProfileController.SectorsUnlocked < 4 ) return;
		offsetPosition = (Input.mousePosition.y - originPosition.y) / Screen.height;
		offsetPosition *= tk2dCamera.Instance.ScreenExtents.height;

		targetPosition = projectedOriginPosition;
		targetPosition.y += offsetPosition;

		float excessDistance;
		float addedDistance;

		if (targetPosition.y < Singleton.mapPanel.panelMinY) {
			excessDistance = targetPosition.y - Singleton.mapPanel.panelMinY;
			addedDistance = Mathf.Pow(-excessDistance, 0.85f);
			targetPosition.y -= excessDistance;
			targetPosition.y -= addedDistance;
		}

		if (targetPosition.y > Singleton.mapPanel.panelMaxY) {
			excessDistance = targetPosition.y - Singleton.mapPanel.panelMaxY;
			addedDistance = Mathf.Pow(excessDistance, 0.85f);
			targetPosition.y -= excessDistance;
			targetPosition.y += addedDistance;
		}
	}

	void OnTapUp ()
	{
		if ( ProfileController.SectorsUnlocked < 4 ) return;
		if (targetPosition.y < Singleton.mapPanel.panelMinY) targetPosition.y = Singleton.mapPanel.panelMinY;
		if (targetPosition.y > Singleton.mapPanel.panelMaxY) targetPosition.y = Singleton.mapPanel.panelMaxY;

	}

    public void OnBack ()
	{
   
        MenuController.LoadToScene(2);
	}
	
}
