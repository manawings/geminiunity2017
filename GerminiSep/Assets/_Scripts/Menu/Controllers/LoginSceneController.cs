﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class LoginSceneController : MonoBehaviour {

	ConnectFacebook connectFB = new ConnectFacebook();

	void OnLoginGuestReal () {

		UIController.ShowWait();
		SocialController.LoadSocialGuestID();
		ProfileController.SocialId = SocialController.id;
		ProfileController.Singleton.isGuest = 1;
		PlayerPrefs.SetInt("isOffline", 0);
		PlayerPrefs.SetInt("isGuest", 1);

		ProfileController.IsLoadData = true;

		DB.Singleton.OnConnectFail = ShowOfflineOption;
		//		DB.Singleton.CallDatabase("CheckAccount", OnCheckAccountSuccess, OnCheckAccountFail);
		
		// If the ID's are different, do the first check.
		if (SocialController.oldID == SocialController.id) {
			// IDs are the same - do as normal
//			Debug.Log ("Call Normal Case: " + SocialController.id);
			DB.Singleton.CallDatabase("CheckAccount", OnCheckAccountSuccess, OnCheckAccountFail);
		} else {
			// IDs are the different - check the new one first.
//			Debug.Log ("Call Backup Case");
			DB.Singleton.CallDatabase("CheckAccount", OnCheckAccountSuccess, OnFirstCheckAccountFail);
		}

	}
	
	void OnLoginGuest ()
	{
		UIController.ShowPopupYesOrNo("GUEST MODE", "This mode requires an internet connection to play. An offline mode is available if you do not want to connect to the internet. You can later link your account to Facebook (which we recommend) to secure your data.", "PLAY AS GUEST", "CANCEL", OnLoginGuestReal);

	}

	void OnFirstCheckAccountFail ()
	{
		// The Vendor ID Account Failed. Switch to Guest Account and Check.
//		Debug.Log ("OnFirstCheckAccountFail");
		UIController.RefreshWaitDuration();
		SocialController.id = SocialController.oldID; // Set to the old ID.
		DB.Singleton.CallDatabase("CheckAccount", OnSecondCheckAccountSuccess, OnSecondCheckAccountFail);
	}
	
	void OnSecondCheckAccountSuccess ()
	{
//		Debug.Log ("OnSecondCheckAccountSuccess");
		// The Vendor ID Account Failed. Switch to Guest Account and Check.
		SocialController.BeginLogin(SocialController.id, OnSecondDBLoginSuccess, OnDBLoginFail);
	}
	
	void OnSecondDBLoginSuccess ()
	{
//		Debug.Log ("OnSecondDBLoginSuccess");
		// Load the OLD Friend List
		DB.Singleton.CallDatabase("RequestFriendList", SecondCalibrateForUser, null);
	}
	
	void SecondCalibrateForUser ()
	{
//		Debug.Log ("SecondCalibrateForUser");
		// Logged into the old account. Load everything as normal, but change ID to the new account right away.
		SocialController.MergeFriendLists();
		UIController.ClosePopUp();
		DataController.LoadSocialData();
		MenuController.LoadToScene(2);
		
		SocialController.id = SocialController.vendorId;
		
	}
	
	void OnSecondCheckAccountFail ()
	{
//		Debug.Log ("OnSecondCheckAccountFail");
		// No OLD account either - this user is new, so login using the vendor ID right away.
		SocialController.id = SocialController.vendorId;
		GoToEnterNameScene();
	}



	void PromptOffline () {
		UIController.ShowPopupYesOrNo("OFFLINE MODE", "This mode does not require internet connection. However this will cause some features to be disabled. You will also NOT be able to link it with an online account later. Do you wish to continue?", "PLAY OFFLINE", "CANCEL", OnOffline);
	}

	void OnOffline ()
	{

		ProfileController.Singleton.isOffline = true;
		PlayerPrefs.SetInt("isOffline", 1);
		PlayerPrefs.SetInt("isGuest", 0);
		GoToEnterNameScene();

//		if (PlayerPrefs.GetInt ("OfflineSaved") == 1) {
//
//			DataController.LoadAllData();
//			MenuController.LoadToScene(2);
//
//		} else {
//			GoToEnterNameScene();
//		}


	}

	void ShowOfflineOption ()
	{
		UIController.ShowPopupYesOrNo("CONNECTION ERROR", "Unable to connect to the servers. You may still play in OFFLINE MODE without internet connection. However this will cause some features to be disabled. You will also NOT be able to link it with an online account later.", "PLAY OFFLINE", "CANCEL", OnOffline);
	}

	void OnLoginFBReal ()
	{
		UIController.ShowWait();
		
		//		ProfileController.Singleton.isGuest = 1;
		//		PlayerPrefs.SetInt("isGuest", 1);
		//		FB.Logout();
		//		Debug.Log("FB.IsLoggedIn = " + FB.IsLoggedIn);
		if (FB.IsLoggedIn) {
			
			// Already Logged In. Skip FB and connect to the DB.
			OnFacebookLoginSuccess();
			
		} else {
			
			// Connect to FB.
			connectFB.Connect(OnFacebookLoginSuccess, OnFacebookLoginFail);
			
		} 
	}

	void OnLoginFB ()
	{
		UIController.ShowPopupYesOrNo("ONLINE MODE", "In this mode, your data will be the most secure, and you can transfer your account between different devices. However, internet connection will be required to play. An offline mode is available if you do not want to connect to the internet.", "PLAY ONLINE", "CANCEL", OnLoginFBReal);
	}

    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }
    }

    void OnBack ()
	{
 
        MenuController.LoadToScene(0);
	}


	void OnFacebookLoginSuccess ()
	{
		// Facebook Login Successful, now login to the main AWS DB.
		SocialController.id = AccessToken.CurrentAccessToken.UserId;
		ProfileController.Singleton.isGuest = 0;
		PlayerPrefs.SetInt("isGuest", 0);
		PlayerPrefs.SetInt("isOffline", 0);

		ProfileController.IsLoadData = true;

		DB.Singleton.CallDatabase("CheckAccount", OnCheckAccountSuccess, OnCheckAccountFail);

		Debug.Log("AccessToken.CurrentAccessToken.UserId : " + AccessToken.CurrentAccessToken.UserId);

		UIController.RefreshWaitDuration();
	}

	void OnCheckAccountSuccess ()
	{
		Debug.Log("OnCheckAccountSuccess");
//		Debug.Log ("OnCheckAccountSuccess");
		SocialController.BeginLogin(SocialController.id, OnDBLoginSuccess, OnDBLoginFail);
		UIController.RefreshWaitDuration();
	}

	void OnCheckAccountFail ()
	{
		GoToEnterNameScene();
	}

	void GoToEnterNameScene ()
	{
		UIController.ClearTimeOutDelegates();
		UIController.ClosePopUp();
		MenuController.LoadToScene(12);
	}

	void OnFacebookLoginFail ()
	{
		// Handle FB Login Fail.
		UIController.ClearTimeOutDelegates();
		ShowOfflineOption();
	}

	void OnDBLoginFail ()
	{
		// Database Login Failed
		SocialController.MergeFriendLists();
		ShowOfflineOption();
		
	}

	void CalibrateForUser ()
	{
		// User has successfully logged in.
		SocialController.MergeFriendLists();
		UIController.ClosePopUp();
		DataController.LoadSocialData();
		MenuController.LoadToScene(2);

	}
	
	void OnDBLoginSuccess ()
	{
		// First step of loggin in successful.
//		Debug.Log ("OnDBLoginSuccess");


		// this the new version for load Data
		UIController.ClosePopUp();

		ProfileController.Singleton.isOffline = true;
		ProfileController.Singleton.isGuest = 0;
		SocialController.isLoggedIn = false;

		PlayerPrefs.SetInt("isOffline", 1);
		PlayerPrefs.SetInt("isGuest", 0);

		DataController.LoadSocialData();
		ProfileController.IsLoadData = true;
		MenuController.LoadToScene(2);
		//

//		DB.Singleton.CallDatabase("RequestFriendList", CalibrateForUser, null);

//		UIController.RefreshWaitDuration();

	}
}
