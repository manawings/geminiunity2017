﻿using UnityEngine;
//using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class OptionSceneController : MonoBehaviour {

	public BlitzButton musicButton, soundButton;

	[SerializeField] GameObject restore;

	public UILabel gameVersionLabel;


	void Start ()
	{
		SetButtonGraphics();
		gameVersionLabel.text = "GAME VER: " + ProfileController.VersionID;

		#if UNITY_ANDROID
		restore.gameObject.SetActive(false);
		#endif

	}

    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }
    }

    void OnBack ()
	{
 
        MenuController.LoadToScene(2);
	}

	void OnControl ()
	{
		MenuController.LoadToScene(17);
	}

	void OnAch ()
	{

#if UNITY_IOS
		if(GeminiGameCenter.Singleton != null)
			GeminiGameCenter.ShowGamcenterAchievementUI();
//
//		if (GameCenterBinding.isPlayerAuthenticated()) {
//
//			AchievementManager.ShowGCAchievements();
//
//		} else {
//
//			AchievementManager.Singleton.ResetShowAuth();
//
//		}

#endif

	}

	void OnMusic ()
	{
		if (KJCanary.IsMusicEnabled) {
			KJCanary.DisableMusic();
		} else {
			KJCanary.EnableMusic();
			KJCanary.Singleton.SwitchToAmbience();
		}
		SetButtonGraphics();
	}

	void OnSound ()
	{
		if (KJCanary.IsSoundEnabled) {
			KJCanary.DisableSoundEffects();
		} else {
			KJCanary.EnableSoundEffects();
		}
		SetButtonGraphics();
	}

	void OnAccount ()
	{
		MenuController.LoadToScene(18);
//		UIController.ShowPopupYesOrNo("RESET DATA", "Warning! This will clear all login data, offline profiles and achievement data for this game. Online profiles will not be reset. Are you sure you want to continue?", OnResetActual);
	}

	void OnReset ()
	{
		UIController.ShowPopupYesOrNo("RESET DATA", "Warning! This will clear all login data, offline profiles and achievement data for this game. Online profiles will not be reset. Are you sure you want to continue?", OnResetActual);
	}
	

	void OnResetActual ()
	{
		// PlayerPrefs.DeleteKey("isGuest"); PlayerPrefs.DeleteKey("isOffline");
//		AchievementManager.ResetAchievements(); // TODO: Temp
		Achievement.ResetAchievements();


		// Clear Player Prefs. But Backup the Guest Key first.
		string backupGuestKey = DataController.GuestKey;

		// Back up all the data that we don't want to get erased by Player Prefs.
		string[] backupString = { AdManager.AdTimeKey, SocialController.tLastTimeCheck, SocialController.tServerTime };
		Dictionary<string, string> storeValue = new Dictionary<string, string>();

		foreach (string dataName in backupString) {
			storeValue.Add(dataName, PlayerPrefs.GetString(dataName, "0"));
		}

		PlayerPrefs.DeleteAll();
		DataController.GuestKey = backupGuestKey;

		// Restore all the data.
		foreach (string dataName in backupString) {
			PlayerPrefs.SetString( dataName, storeValue[dataName]);
		}

		PlayerPrefs.Save();

//		Debug.Log ("GK: " +  DataController.GuestKey );


		ProfileController.Reset();
		SocialController.isLoggedIn = false;
		ProfileController.Singleton.isGuest = 0;
		ProfileController.Singleton.isOffline = false;

		if (FB.IsLoggedIn) FB.LogOut();

		MenuController.LoadToScene(0);

	}


	void OnRestore () {
		//IAPManager.RestoreTransactions();
		IAPController.Singleton.RestorePurchases();
	}


	void OnGraphics ()
	{
		ProfileController.Singleton.lowGraphic  = ProfileController.Singleton.lowGraphic ? false : true;
		SetButtonGraphics();
	}

	void OnCredits ()
	{
		MenuController.LoadToScene(16);
	}

	void SetButtonGraphics ()
	{
		string adder = ProfileController.Singleton.lowGraphic ? "LOW" : "HIGH";
//		graphicsButton.buttonLabel = "GRAPHICS: " + adder;
//		graphicsButton.SetOptions();

		adder = KJCanary.IsMusicEnabled ? "ON" : "OFF";
		musicButton.buttonLabel = "MUSIC: " + adder;
		musicButton.SetOptions();

		adder = KJCanary.IsSoundEnabled ? "ON" : "OFF";
		soundButton.buttonLabel = "SOUND: " + adder;
		soundButton.SetOptions();
	}
}
