using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class SocialMenuController : MonoBehaviour {
	
	public UILabel loginLabel;
	public UILabel displayLabel;

	public BlitzButton inboxButton, connectFacebookButton, facebookFriendButton;

	ConnectFacebook connectFB = new ConnectFacebook();
	// Use this for initialization
	void Start () {
		Debug.Log("ProfileController.Singleton.isOffline " + ProfileController.Singleton.isOffline);
		if (!ProfileController.Singleton.isOffline) {
			CalibrateForUser();
			if (SocialController.shouldLoadInfo) {
				KJTime.Add(ReloadData, 0.2f, 1);
				SocialController.shouldLoadInfo = false;
			}
		} else {
			CalibrateForUser();
			Debug.Log("Offline Mode");
		}
	}

	void OnFacebookLoginSuccess ()
	{
		// Facebook Login Successful, now login to the main AWS DB.
		SocialController.id = AccessToken.CurrentAccessToken.UserId;
		ProfileController.Singleton.isGuest = 0;

		PlayerPrefs.SetInt("isGuest", 0);
		PlayerPrefs.SetInt("isOffline", 0);

		UIController.RefreshWaitDuration();

		SocialController.BeginLogin(SocialController.id, OnDataLoaded, CalibrateNull);

	}

	void WaitDataFacebook () {

	}

	void OnFacebookLoginFail ()
	{
		// Handle FB Login Fail.
		UIController.ShowError("ERROR", "Could not connect to Facebook. Please try again later.");
	}

	void OnAccountId () {

//		if ( SystemInfo.deviceUniqueIdentifier == iPhone.vendorIdentifier ) {
//			UIController.ShowSingleClose( "ACCOUNT ID", ProfileController.DisplayName + "\n" + ProfileController.SocialId.ToString() );
//		} else {
		UIController.ShowSingleClose( "ACCOUNT ID", "DN: " + ProfileController.DisplayName + "\n\n SID: " + ProfileController.SocialId.ToString() + "\n\n DUI: " + SystemInfo.deviceUniqueIdentifier + "\n\n GK: " + DataController.GuestKey);
//		}

	}


	void OnBack ()
	{
		MenuController.LoadToScene(2);
	}
	
	void ReloadData ()
	{


		UIController.ShowWait(10.0f, OnFailClose);
		SocialController.BeginLogin(SocialController.id, OnDataLoaded, CalibrateNull);
//		connectFB.Connect(OnFacebookLoginSuccess, OnFacebookLoginFail);


	}

	void ConnectWithFB ()
	{
		UIController.ShowWait();
		Debug.Log("FB.IsLoggedIn " + FB.IsLoggedIn);
		CheckLogin();

	}

	void CheckLogin ()
	{
		
		if (FB.IsLoggedIn) {
			
			// Already Logged In. Skip FB and connect to the DB.
			// Facebook Login Successful, now login to the main AWS DB.
			OnFacebookLoginSuccess();
			
		} else {
			// Go to Splash.
			connectFB.Connect(OnFacebookLoginSuccess, OnFacebookLoginFail);
			
		} 
		
	}

	void OnLoadFailed ()
	{
		UIController.ShowPopup();
	}

	void OnFailClose ()
	{
		UIController.ClosePopUp();
		MenuController.LoadToScene(2);
	}
	
	void GoToFriendList ()
	{
		if (SocialController.isLoggedIn) {
			SocialController.FriendListType = SocialController.ListType.Friends;
			MenuController.LoadToScene(10);
			//  DB.Singleton.CallDatabase("RequestFriendList");
		}
	}

	void GoToFacebookList ()
	{
		if (FB.IsLoggedIn) {
			UIController.ShowWait();
			connectFB.RequestFacebookFriendList(OnGetFacebookFriendList);


		}
	}

	void GoToInbox ()
	{
		if (ProfileController.Singleton.isOffline) {
			SocialController.FriendListType = SocialController.ListType.Inbox;
			MenuController.LoadToScene(10);
		} else if (SocialController.isLoggedIn) {
			SocialController.FriendListType = SocialController.ListType.Inbox;
			MenuController.LoadToScene(10);
			// connectFB.RequestFacebookFriendList(OnGetFacebookFriendList);
		}
	}

	void GoToSent ()
	{
		if (SocialController.isLoggedIn) {
			SocialController.FriendListType = SocialController.ListType.Sent;
			MenuController.LoadToScene(10);
			// connectFB.RequestFacebookFriendList(OnGetFacebookFriendList);
		}
	}

	void OnGetFacebookFriendList ()
	{
		UIController.ClosePopUp();
//		Debug.Log("FaceBook On Get FriendList");

		CalibrateForUser();
		SocialController.FriendListType = SocialController.ListType.Facebook;
		MenuController.LoadToScene(10);

	}


	private IEnumerator WaitLogin()
	{
		while (true)
		{
			if(connectFB.isSetProfile) {
				//connectFB.SetMyProfile();
				//Debug.Log(FacebookData.myProfile["id"]);
				//DataController.SaveAllData();
				SocialController.BeginLogin((string)FacebookData.myProfile["id"], OnDataLoaded, CalibrateForUser);
				break; 
			}
			yield return null; 
		}
	}
	
	void Challenge ()
	{
		SocialController.BeginChallenge();
	}

	void OnDataLoaded ()
	{
//		Debug.Log ("OnDataLoaded!");

		if (SocialController.isLoggedIn) {
			DB.Singleton.CallDatabase("RequestFriendList", CalibrateForUser);
		}
	}
	

	void CalibrateForUser ()
	{

		if (ProfileController.Singleton.isOffline) {
			UIController.ClosePopUp();
			loginLabel.text = "Pilot Name : " + ProfileController.DisplayName;
			UIController.UpdateCurrency();
			return;
		}

	
	//below for online
//		Debug.Log ("CalibrateForUser");

		if (FB.IsLoggedIn) {
			connectFacebookButton.gameObject.SetActive(false);
			facebookFriendButton.gameObject.SetActive(true);
		} else {
			connectFacebookButton.gameObject.SetActive(true);
			facebookFriendButton.gameObject.SetActive(false);
		}

		if (SocialController.isLoggedIn) {

				SocialController.MergeFriendLists();

//				int inboxCount = SocialController.friendListRequest.Count;
//				if (ProfileController.HelpedFriends + ProfileController.HelpedStrangers > 0) inboxCount ++;
//				if (ProfileController.Singleton.hasGoldReward) inboxCount ++;

				inboxButton.SetNewValue(ProfileController.InboxCount);

				if (ProfileController.Singleton.isGuest == 0)
					loginLabel.text = "Connected as " + ProfileController.SocialName;
				else 
					loginLabel.text = "Logged in as Guest";

				displayLabel.text = ProfileController.DisplayName.ToUpper();

		} else {

			displayLabel.text = ProfileController.DisplayName;

		}

		UIController.ClosePopUp();
		UIController.UpdateCurrency();
		// challengeButton.SetActive(true);
	}
	
	void CalibrateNull ()
	{
		loginLabel.text = "Not Connected.";
		// challengeButton.SetActive(false);
	}
}