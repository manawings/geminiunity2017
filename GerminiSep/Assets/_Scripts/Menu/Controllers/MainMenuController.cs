using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;

public class MainMenuController : MonoBehaviour {
	
	
	// Sector Display
	
	public KJBarGlow progressBar;
	public UILabel nameLabel;
	public UILabel sectorLabel;
	public UILabel progressLabel;

	public GameObject itemAlert;
	public UILabel itemAlertLabel;

	public UISprite bmSprite, shipSprite, mapSprite;
	public UISprite markerYou, markerBoss;

	public MainBGController bgController;

	public GameObject secretInfo, inboxMarker;

	void Start () {
		
		if (!Advertisement.IsReady ()) {
			Debug.Log("Initialize");
			Advertisement.Initialize (ProfileController.adsUnityID, false);
		} 
//		AdManager.ToggleShowAdPopUp();
//		AddAdsUnity();
		ProfileController.secretMission = false;
		ProfileController.secretBoss = false;
		ProfileController.secretRaid = false;
		ProfileController.previewMode = false;
		ProfileController.previewShip = 0;

		ProfileController.CheckQuestRenew();
		ProfileController.Singleton.SetSecretSector();

		NetworkController.Singleton.CheckAndTest();
		UIController.UpdateCurrency();
		Calibrate();

		KJCanary.Singleton.SwitchToAmbience();
		// TutorialData.Deploy(0);

		if (!TutorialData.CheckTutorialState(4)) {
			bmSprite.spriteName = "BlackMarketOff_v3";
		}

		if (!TutorialData.CheckTutorialState(20)) {
			shipSprite.spriteName = "ShipsOff_v3";
		}

		if (!TutorialData.HasCompleted(19) && ProfileController.SectorsUnlocked == 1) {
			mapSprite.spriteName = "MapLock_v3";
			mapSprite.MakePixelPerfect();
		}

		bmSprite.MakePixelPerfect();
		shipSprite.MakePixelPerfect();

		ProfileController.forceMap = false;

		bgController.Deploy();
		BGGraphics graphics = ProfileController.Sector.SectorGraphic;
		DeployAtmosphere(graphics.particleColorStart, graphics.particleColorEnd);

		secretInfo.gameObject.SetActive(false);

		if (TutorialData.HasCompleted(19) && ProfileController.CheckQuestTime() && ProfileController.secretSector != -1) {
			secretInfo.gameObject.SetActive(true);
		}

		if (TutorialData.HasCompleted(19) && ProfileController.CheckQuestBossTime() && ProfileController.secretBossSector != -1) {
			secretInfo.gameObject.SetActive(true);
		}

		if (TutorialData.HasCompleted(19) && ProfileController.CheckRaidTime() && ProfileController.secretRaidSector != -1) {
			secretInfo.gameObject.SetActive(true);
		}

//		if (TutorialData.HasCompleted(19) && AdManager.CanAdBeShown && ProfileController.secretBroadcastSector != -1) {
//			secretInfo.gameObject.SetActive(true);
//		}


		inboxMarker.gameObject.SetActive(false);

		//Debug.Log("Next Time" + new System.DateTime(ProfileController.markInboxTime));
		if (SocialController.isLoggedIn && ProfileController.CheckInBoxTime()) {

			System.DateTime nextTime = System.DateTime.UtcNow.AddMinutes(1);
			ProfileController.markInboxTime = nextTime.Ticks;

			//Debug.Log("Next Time" + new System.DateTime(ProfileController.markInboxTime));

			DB.Singleton.CallDatabase("CheckInbox", OnCheckAccountSuccess, OnCheckAccountFail);
			UIController.ShowWait(5, OnCheckAccountFail);
		}

		if (ProfileController.Singleton.isOffline) {
			
		}

		if (TutorialData.CheckTutorial(36) && ProfileController.isDie == true) {
			InputController.LockControls();
			KJTime.Add (DeployDieTut, 0.15f, 1);
		}

		ProfileController.isDie = false;
//		CheckShowAds();
//		KJTime.Add (UIController.ShowPopupViewAd, 2.0f, 1);
//		KJTime.Add (KJTime.SpaceType.System, UIController.FadeOut, 3.0f, 1);
		SocialController.CheckTime();

	}

//	public GameObject adsUnity;
//	void AddAdsUnity () {
//		adsUnity = new GameObject("Ads Unity");
//		adsUnity.AddComponent<AdManagerUnity>();
//		adsUnity.transform.SetParent(gameObject.transform);
//	}
//
//	void CheckShowAds () {
//
//		if (ProfileController.SectorId >= 3) {
//			System.DateTime timeNow = System.DateTime.Now;
//			System.DateTime targetTime = System.Convert.ToDateTime(ProfileController.Singleton.targetOfferAdsTime);
//		//Debug.Log("targetTime - timeNow.TotalMinutes" + ((targetTime - timeNow).TotalMinutes));
//			if ((targetTime - timeNow).TotalMinutes <= 0) {
//				UIController.ShowPopupViewAd();
//			} else if (ProfileController.Singleton.countPlayForAds > ProfileController.Singleton.minCountShowAds || ProfileController.Singleton.currentIntervalTimer > ProfileController.Singleton.intervalAdstime) {
//				GameObject.Find("Ads Unity").GetComponent<AdManagerUnity>().ShowAdSkip();
//				ProfileController.Singleton.countPlayForAds = 0;
//				ProfileController.Singleton.currentIntervalTimer = 0;
//			}
//			DataController.SaveAllData();
//		}
//	}

	void DeployDieTut ()
	{
		TutorialData.Deploy(36);
	}

	void OnCheckAccountSuccess ()
	{
		UIController.ClosePopUp();
		inboxMarker.gameObject.SetActive(true);
	}

	void OnCheckAccountFail ()
	{
		UIController.ClosePopUp();
	}

	Color CopiedColor (Color from, float alpha = 1.0f )
	{
		return new Color(from.r, from.g, from.b, alpha);
	}

	public void DeployAtmosphere (Color startColor, Color endColor)
	{
		
		//		if (ProfileController.Singleton.lowGraphic) return;
		KJEmitter cloudEmitter;
		KJEmitter starEmitter;

		cloudEmitter = gameObject.AddComponent<KJEmitter>();
		cloudEmitter.CopyFromModel(EffectController.Singleton.bgCloud);
		
		starEmitter = gameObject.AddComponent<KJEmitter>();
		starEmitter.CopyFromModel(EffectController.Singleton.bgStar);
		
		cloudEmitter.SetColor(
			CopiedColor(startColor, 0.15f),
			CopiedColor(endColor, 0.15f));
		
		starEmitter.SetColor(
			startColor,
			endColor);
		
		//		cloudEmitter.SetGraphic(KJParticleGraphic.Type.Cloud);
		
		cloudEmitter.Deploy();
		starEmitter.Deploy();
	}

	void Calibrate ()
	{

#if UNITY_IOS
//		if (!ProfileController.isDie && ProfileController.SectorsUnlocked > 4)
//			EtceteraBinding.askForReview(0, 72.0f, "Rate Gemini Strike ", "Enjoying the game? Please let us know what you think!", "797701494");
#endif

		float sectorMax = (float) ProfileController.MaxSubSectors;
		float sectorNow = (float) ProfileController.SubSector;
		float progress = (sectorNow - 1) / (sectorMax - 1);
		progressBar.SetProgress(progress, true);

		nameLabel.text = ProfileController.Sector.name.ToUpper();
		sectorLabel.text = "SECTOR " + ProfileController.SectorId + "." + ProfileController.SubSector;
//		progressLabel.text = "PROGRESS: " + (progress * 100).ToString("F0") + "%";


		float mapMarkerStart = -90;
		float mapMarkerLength = 180;

		if (progress != 1)
			KJMath.SetX(markerYou, mapMarkerStart + (progress * mapMarkerLength));
		else 
			KJMath.SetX(markerYou, mapMarkerStart + (0.86f * mapMarkerLength));

		int newItems = ProfileController.NewItems;
		if (newItems > 0) {
			itemAlertLabel.text = newItems.ToString();
		} else {
			itemAlert.SetActive(false);
		}

		KJTime.Add(DeployStory, 0.10f, 1);

	}

	void DeployStory ()
	{

		Story.storyDeployed = false;

		if (ProfileController.SubSector == 1) {
			ProfileController.Sector.DeployEnterStory();
		}
		
		if (ProfileController.SubSector == 4) {
			ProfileController.Sector.DeployMidSectorStory();
		}
		
		if (ProfileController.SubSector == ProfileController.MaxSubSectors) {
			ProfileController.Sector.DeployBossSectorStory();
		}

//		if (!Story.storyDeployed)
//			DeployVideo();
	}

//	void DeployVideo () {
//		AdManager.ToggleShowAdPopUp();
//	}
	
	void GoToInventory ()
	{
		MenuController.LoadToScene(5);
	}
	
	void GoToShips ()
	{
		if (TutorialData.CheckTutorialState(20)) {
			
			MenuController.LoadToScene(6);
			
		} else {
			
			UIController.ShowSingleClose("Locked", "Access denied! This feature will be unlocked later as you make progress through the game.");
			
		}

	}

	void OnOptions ()
	{
		MenuController.LoadToScene(8);
	}
	
	void GoToGame ()
	{
		if (ProfileController.Hearts == 0) {
			if (TutorialData.CheckTutorial(37)) {
				TutorialData.Deploy(37);
			} else {
				UIController.ShowPopupHeart();
			}

		} else if (!ProfileController.HasInventorySpace) {

			NotEnoughSpace();

		} else {

			ProfileController.secretMission = false;
			ProfileController.secretBoss = false;
			ProfileController.secretRaid = false;

			ProfileController.CalibratePreGame();

			if (SocialController.isLoggedIn) {

				// Call Data Base for small data and wait for response.
				UIController.ShowWait();

				if (ProfileController.SectorId > 1 && ProfileController.SubSector != 3) {
					DB.Singleton.CallDatabase("RandomPlayer", OnAllyDecodeSuccess);
				} else {
					KJTime.Add(KJTime.SpaceType.System, GoWithoutAlly, 0.8f, 1);
				}

			} else {

				GoWithoutAlly();
			}

			FlurryController.WaveTypeTime();
		}
	}

	void GoWithoutAlly ()
	{
		ProfileController.LoseHeart(OnActualGo);
		ProfileController.allyStandby = false;
	}

	void OnAllyDecodeSuccess ()
	{
		ProfileController.LoseHeart(OnActualGo);
	}

	void OnActualGo ()
	{
		UIController.ClosePopUp();
		KJTime.RemoveGameTimers();
		MenuController.LoadToScene(1);
	}
	
	void GoToMap ()
	{
		if (ProfileController.SectorsUnlocked > 1 || TutorialData.HasCompleted(19)) {
			MenuController.LoadToScene(7);
		} else {
			UIController.ShowSingleClose("Locked", "Access denied! This feature will be unlocked later as you make progress through the game.");
		}
	}
	
	void GoToOptions ()
	{
		
	}

	void GoToBlackMarket ()
	{
		if (TutorialData.CheckTutorialState(4)) {

			if (ProfileController.HasInventorySpace) {
				MenuController.LoadToScene(13);
			} else {
				NotEnoughSpace();
			}

		} else {

			UIController.ShowSingleClose("Locked", "Access denied! This feature will be unlocked later as you make progress through the game.");

		}

	}

	void NotEnoughSpace ()
	{
		UIController.ShowPopupYesOrNo(
			"CARGO FULL", 
			"You do not have enough space in your inventory to do this.\n\nPlease clear out some space in your inventory first by selling items that you no longer need.",
			"Go To Inventory",
			"Close",
			GoToInventory);
	}
	
	void GoToSocial ()
	{
		UIController.ShowWait();
		if (ProfileController.Singleton.isOffline) {
			if(!NetworkController.Singleton.CheckConnecttion) {
				UIController.ShowPopUpSingleYes("Internet Problem", "Unable to connect to the internet. Please check your device's internet connection and try again.", "OK", UIController.ClosePopUp);
			} else {
				GoToSocialActual();
			}
			//UIController.ShowPopup("ERROR", "Social features are not available for Offline Mode. You must RESET the account from the options menu, and then sign in with an online account to access these features.");

		} else {

			UIController.ShowWait();
			DataController.SaveAllData(false, GoToSocialActual);

		}
		// ShowPeers();
	}

	void GoToSocialActual ()
	{
		UIController.ClosePopUp();
		SocialController.shouldLoadInfo = true;
		MenuController.LoadToScene(9);
	}
	
	void ShowPeers ()
	{
		/*
		#if UNITY_IPHONE
		Debug.Log("Show Peers");
		GameKitBinding.showPeerPicker(true);
		#endif
		*/
	}

	void TempResetData ()
	{
//		if (SocialController.isLoggedIn) {
//
//			UIController.ShowWait();
//			DB.Singleton.CallDatabase("RequestDelete", OnTempResetData);
//
//		} else {
//
//			DataController.ResetAllData();
//			MenuController.LoadToScene(0);
//
//		}

	}

	void OnTempResetData ()
	{

//		DataController.ResetAllData();
//		MenuController.LoadToScene(0);
	}

    public void JumpToAds ()
    {
        ProfileController.SectorId = 2;
        ProfileController.SubSector = 6;
        ProfileController.Singleton.SetDebugItems();
        if (ProfileController.Hearts == 0)
        {
            if (TutorialData.CheckTutorial(37))
            {
                TutorialData.Deploy(37);
            }
            else
            {
                UIController.ShowPopupHeart();
            }

        }
        else if (!ProfileController.HasInventorySpace)
        {

            NotEnoughSpace();

        }
        else
        {

            ProfileController.secretMission = false;
            ProfileController.secretBoss = false;
            ProfileController.secretRaid = false;

            ProfileController.CalibratePreGame();

            if (SocialController.isLoggedIn)
            {

                // Call Data Base for small data and wait for response.
                UIController.ShowWait();

                if (ProfileController.SectorId > 1 && ProfileController.SubSector != 3)
                {
                    DB.Singleton.CallDatabase("RandomPlayer", OnAllyDecodeSuccess);
                }
                else
                {
                    KJTime.Add(KJTime.SpaceType.System, GoWithoutAlly, 0.8f, 1);
                }

            }
            else
            {

                GoWithoutAlly();
            }

            FlurryController.WaveTypeTime();
        }
    }

	void TempBuyIAP ()
	{
//		Debug.Log ("Store Purchase Try");
//		Debug.Log ("Try");
		// StoreKitBinding.cancelDownloads();
		// StoreKitBinding.purchaseProduct("HC_1", 1);
	}
	
}
