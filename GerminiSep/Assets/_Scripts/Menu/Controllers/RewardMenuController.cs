using UnityEngine;
using System.Collections;

public class RewardMenuController : MonoBehaviour {

	public UISprite gemSprite, glowSprite;
	public GameObject spriteHolder;
	public UILabel textLabel, rewardTitle;
	FriendItem rewardItem;

	// Use this for initialization
	void Start () {

		rewardItem = ProfileController.rewardItem;

		if (rewardItem == null ) {
			rewardItem = new FriendItem();
//			rewardItem.status = FriendItem.FriendStatus.GoldReward;
			rewardItem.rewardAmount = 1;// = FriendItem.FriendStatus.GoldReward
			rewardItem.rewardPoints = 1;// = FriendItem.FriendStatus.GoldReward
		}

		if (rewardItem.status == FriendItem.FriendStatus.GoldReward) {

			gemSprite.spriteName = "HC_Big";
			rewardTitle.text = "CREDIT REWARD";
			textLabel.text = "For your continued support to the war effort, you have been awarded your daily Credit.";
			gemSprite.color = new Color(1.0f, 0.8f, 0.2f, 1.0f);
			glowSprite.color =  new Color(1.0f, 0.4f, 0.0f, 1.0f);
//			ProfileController.Credits += rewardItem.rewardAmount;


		} else {

			gemSprite.spriteName = "SC_Big";
			rewardTitle.text = "BOUNTY REWARD";
			textLabel.text = "You helped " + rewardItem.rewardPoints + " other pilots and have been rewarded " + rewardItem.rewardAmount + " Gems.";
			gemSprite.color = new Color(0.2f, 0.8f, 1.0f, 1.0f);
			glowSprite.color =  new Color(0.0f, 0.3f, 1.0f, 1.0f);
//			ProfileController.Gems += rewardItem.rewardAmount;

		}

		spriteHolder.SetActive(false);
		glowSprite.gameObject.SetActive(false);


		UIController.UpdateCurrency();
		KJTime.Add (DelayedStart, 0.35f, 1);

	}

	void RunSpriteToNormal ()
	{
		spriteHolder.transform.localScale = Vector3.Lerp(spriteHolder.transform.localScale, Vector3.one, 0.20f);
		gemSprite.color = Color.Lerp(gemSprite.color, Color.black, 0.15f);
		glowSprite.transform.eulerAngles = glowSprite.transform.eulerAngles + Vector3.forward * 1.0f;
		glowSprite.transform.localScale = Vector3.Lerp(glowSprite.transform.localScale, Vector3.one * 146.0f, 0.20f);
	}

	void DelayedStart ()
	{

		KJCanary.PlaySoundEffect("Reward");
		spriteHolder.SetActive(true);
		glowSprite.gameObject.SetActive(true);
		spriteHolder.transform.localScale = Vector3.one * 4.0f;
		glowSprite.transform.localScale = Vector3.one * 0.2f;

		KJTime.Add (RunSpriteToNormal);

		KJEmitter newEmitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");

		if (rewardItem.status == FriendItem.FriendStatus.GoldReward) {
			newEmitter.SetColor(
				new Color (1.0f, 0.7f, 0.0f, 1.0f),
				new Color (1.0f, 0.0f, 0.0f, 0.0f));
		} else {
			newEmitter.SetColor(
				new Color (0.3f, 0.8f, 1.0f, 1.0f),
				new Color (0.0f, 0.0f, 1.0f, 0.0f));
		}

		newEmitter.SetRadius(0.0f);
		newEmitter.SetPower(1200.0f, 0.0f, 0.85f);
		newEmitter.SetDirection(0.0f, Mathf.PI);
		newEmitter.SetDecay(0.3f, 1.5f);
		newEmitter.SetScale(1.0f);
		newEmitter.SetScale(1.0f);
		newEmitter.SetGraphic(KJParticleGraphic.Type.Spark);
		newEmitter.SetExplosion(120, 0.15f, 0, 0.85f);
		newEmitter.SetSpawnInDirection(true);
		newEmitter.SetDecceleration(0.85f);
		newEmitter.Explode(Vector3.zero, Vector3.up * 30);
	}

	void OnUpdateComplete ()
	{
		UIController.ClosePopUp();
	}

    private bool oncTime = false;
    void Update()
    {
        if (!oncTime)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    OnBack();
                    oncTime = true;
                }
            }
        }
    }

    void OnBack ()
	{


        if (ProfileController.InboxCount == 0)
			MenuController.LoadToScene(9);
		else 
			MenuController.LoadToScene(10);
	}
}
