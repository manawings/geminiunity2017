using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class EnterNameController : MonoBehaviour {

	public UIInput nameLabel;

	void OnPlay ()
	{
		string useName =  nameLabel.text.Replace("|", "");

		if (useName == string.Empty) {
			UIController.ShowPopup("ERROR", "The name cannot be empty. Please type a name into the field!");
			return;
		}

		ProfileController.DisplayName = useName;

		if (ProfileController.Singleton.isOffline) {
			GoToGame();
			return;
		}

		if (FB.IsLoggedIn) {

			UIController.ShowWait();
			SocialController.BeginLogin(AccessToken.CurrentAccessToken.UserId, OnDBLoginSuccess, OnDBLoginFail);

		} else {

			// SocialController.id = "101";//finalString;
			SocialController.LoadSocialGuestID();
//			Debug.Log("UDID: " + SocialController.id);
			UIController.ShowWait();
			SocialController.BeginLogin( SocialController.id, OnDBLoginSuccess, OnDBLoginFail );
		}
	}

	void OnNameEnter ()
	{
//		Debug.Log("Name Entered");
	}

	void GoToGame ()
	{
		DataController.SaveAllData();
		MenuController.LoadToScene(2);
	}

	void OnDBLoginSuccess ()
	{
		// First step of loggin in successful.
		if (SocialController.isLoggedIn) {
			DB.Singleton.CallDatabase("RequestFriendList", CalibrateForUser, null);
		}
	}

	void OnDBLoginFail ()
	{
		// Database Login Failed
		SocialController.MergeFriendLists();
		UIController.ClosePopUp();
		
	}
	
	void CalibrateForUser ()
	{
		// User has successfully logged in.
		SocialController.MergeFriendLists();
		UIController.ClosePopUp();
		GoToGame();
		
	}
	

}
