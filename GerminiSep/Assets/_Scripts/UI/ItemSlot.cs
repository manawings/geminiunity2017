﻿using UnityEngine;
using System.Collections;

public class ItemSlot : KJNGUIButtonFunction
{
	
	ItemBasic item;
	[HideInInspector] public int itemId = 0;
	public bool isToBeReplaced = false; // For the pop-up when the slots are full.
	public bool canBePressed = true;
	public UISprite iconGraphic;
	public UISprite iconFrame;
	public PopupPanel popUp = null;

	public UISprite newSprite;
	bool newFlashingUp = true;
	int flashTime = 0;
	
	Color flashColor = new Color(0.3f, 0.3f, 0.3f, 1.0f);
	Color newFlashColor = new Color(0.015f, 0.005f, 0.0f, 0.0f);
	
	protected void Start ()
	{
		base.Start();
	}
	
	public void Deploy (ItemBasic item, bool noNewDisplay = false)
	{
		
		this.item = item;
		RemoveAllTimers();
		
		// Set the Icon to the right graphic.
		if (item == null)
		{
			iconGraphic.spriteName = "Icon_Backer";
			iconFrame.spriteName = frameIdForRarity(ItemBasic.Rarity.Common);
			if ( newSprite != null ) newSprite.gameObject.SetActive(false);
		} else {
			iconGraphic.spriteName = item.spriteName; // + item.iconId;
			iconFrame.spriteName = frameIdForRarity(item.rarity);
			if (newSprite != null) {
				if (item.isNew && !noNewDisplay ) {
					newSprite.gameObject.SetActive(true);
					AddTimer(RunNewGlow);
				} else {
					newSprite.gameObject.SetActive(false);
				}
			}
		}

	}

	void RunNewGlow ()
	{
		if (newFlashingUp) {
			newSprite.color += newFlashColor; //Color.Lerp (newSprite.color, newFlashColor, 0.15f);
		} else {
			newSprite.color -= newFlashColor;
			// newSprite.color = Color.Lerp (newSprite.color, Color.black, 0.15f);
		}

		if (flashTime == 60) {
			newFlashingUp = !newFlashingUp;
			flashTime = 0;
		} else {
			flashTime ++;
		}
	}

	private static string frameIdForRarity (ItemBasic.Rarity rarity)
	{

		switch (rarity)
		{

		case ItemBasic.Rarity.Common:
			return "Border_White_v3";
			break;

		case ItemBasic.Rarity.Uncommon:
			return "Border_Green_v3";
			break;

		case ItemBasic.Rarity.Rare:
			return "Border_Purple_v3";
			break;

		case ItemBasic.Rarity.Secret:
			return "Border_Secret_v3";
			break;

		}

		return "Border_White_v3";
	}
	
	protected override void KJ_OnPress ()
	{
		// Debug.Log ("Item Slot Pressed: " + canBePressed + " " + item.Name);

		if (!canBePressed) return;
		if (item == null) return;

		KJCanary.PlaySoundEffect("EquipItem");
		AddFlash(FlashOn, FlashOff, 0.05f, 3, FlashDone);
		// KJTime.Add(ActualFunction, 0.05f, 1);
	}
	
	protected override void KJ_OnClick ()
	{

	}
	
	
	
	void FlashOn ()
	{
		iconGraphic.color = flashColor;
	}
	
	void FlashOff ()
	{
		iconGraphic.color = Color.black;
	}
	
	void FlashDone ()
	{
		if (isToBeReplaced)
		{
			
			// Swap the two items.
			popUp.SwapEquip(itemId);
			
		} else {
			ActualFunction();
		}
	}

	public FriendProfileController friendProfileController;
	
	void ActualFunction ()
	{
		// Debug.Log("Item Graphic: " + item.GetSpriteName);
		if (!isToBeReplaced)
		{

			if (friendProfileController != null) {
				friendProfileController.GoToPreviewFor(item);
			} else {
				// Do a preview of the item.
				ItemMenuController.Singleton.GoToPreviewFor(item);
				InputController.LockControls();
			}
			
		}
	
	}
	
	public void SetItem (int id)
	{
		itemId = id;
		
		if (id > -1 && id < ProfileController.Singleton.inventoryList.Count)
		{
			item = ProfileController.Singleton.inventoryList[id];
		} else {
			item = null;
		}
		
		if (item == null) {
			//gameObject.SetActive(false);
		} else {
			//gameObject.SetActive(true);
			
		}
		
		Deploy(item);
	}
}
