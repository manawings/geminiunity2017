using UnityEngine;
using System.Collections;

public class ButtonFunction : MonoBehaviour {
	
	public int levelToLoad;

	// Use this for initialization
	void Start () {
		 UIEventListener.Get(gameObject).onClick += MyClickFunction;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void MyClickFunction (GameObject gameObject)
	{
		Application.LoadLevel(levelToLoad);
	}
}
