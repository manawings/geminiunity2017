﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IAPManager : MonoBehaviour {
	
	
	// TOP LEVEL FUNCTIONS --------------------------
	
	static StoreObject currentStoreObject;
	static bool restoring = false;
	static string dummyId = "";
	
	static bool hasListeners = false;

	const string gemID = "GDX";
	
	static List<StoreObject> _StoreList;
	public static List<StoreObject> StoreList {
		get {
			if (_StoreList == null) {
				_StoreList = new List<StoreObject>();
				
				//				_StoreList.Add ( new StoreObject("3 Credits", "HC1", "$T", false, 3)); // $2 -- 0.66    
				_StoreList.Add ( new StoreObject("5 Credits", "HC2", "$T2", false, 5)); // $3 -- 0.60    0
				_StoreList.Add ( new StoreObject("15 Credits", "HC6", "$T6", false, 15)); // $8 -- 0.53    1
				_StoreList.Add ( new StoreObject("25 Credits", "HC3", "$T3", false, 25)); // $12 -- 0.52   2
				_StoreList.Add ( new StoreObject("50 Credits", "HC4", "$T4", false, 50)); // $20 -- 0.40   3
				_StoreList.Add ( new StoreObject("100 Credits", "HC5", "$T4", false, 100)); // $35 -- 0.35   4
				_StoreList.Add ( new StoreObject("Gem Doubler", gemID, "$T5", true, 1)); // $5    5
				//				_StoreList.Add ( new StoreObject("Elite Pack", "EP", "$T6", true, 15)); // $5 -- 0.33    6
				
			}
			return _StoreList;
		}
	}
	
	
	public static void SetIAPTarget () {
		UIController.ClosePopUp();
	}
	
	public static void RequestIAPData () {
		
		EnableListeners();
		UIController.ShowWait();
		restoring = false;

		#if UNITY_IOS
//		KJTime.Add(KJTime.SpaceType.System, DummyReceiveFunction, 3, 1);
		RequestIAPDataApple();
		#else
		// UIController.ShowError("Error", "The Credit Store is not available on this device!");
		KJTime.Add(KJTime.SpaceType.System, DummyReceiveFunction, 3, 1);
		#endif
	}
	
	public static void ReceiveIAPData () {
		
		UIController.ShowPopupIAP();
		
	}
	
	public static void FailedIAPData () {
		
		UIController.ShowError("IAP ERROR", "Failed to retrieve IAP data. Please check your internet connection and try again.");
		
	}
	
	public static void FailedTransData (string error) {
		
		UIController.ShowError("IAP ERROR", "Failed to restore IAP data: " + error);
		
	}
	
	public static void EnableListeners () {
		
		#if UNITY_IOS
		OnEnableApple();
		#endif
		
	}
	
	public static void DisableListeners () {
		
		#if UNITY_IOS
		OnDisableApple();
		#endif
		
	}

	public static void RequestBuyItem (int id) {
		
		
		StoreObject storeObject = StoreList[id];
		currentStoreObject = storeObject;
		
//		Debug.Log("Current Store Object: " + storeObject.appleId);
		dummyId = storeObject.appleId;
		
		#if UNITY_IOS
		RequestBuyIAPApple();
//		KJTime.Add(KJTime.SpaceType.System, DummyRequestBuyFunction, 1.0f, 1);
		#else
		UIController.ShowError("Error", "The Credit Store is not available on this device!");
		//		KJTime.Add(KJTime.SpaceType.System, DummyRequestBuyFunction, 1.0f, 1);
		#endif
		
		
		UIController.ShowWait(180);
	}
	
	public static void OnBuySuccess (string id = "") {
		
//		Debug.Log ("BUY SUCCESS: " + id);
		
		if (id == gemID) {
			
			ProfileController.GemDouble = true;
			
			if (!restoring) {
				SaveData();
			}
			
		} else {
			
			if (id == "EP") {
				
				ProfileController.Gems += 2500;
			}

			if (id.Contains("HC")) {

				if (SocialController.isLoggedIn) {
			
					lastHCID = id;
					refreshCount = 5;
					BuyHCFromDB();

				} else {

					ProfileController.Credits += currentStoreObject.units;
					UIController.UpdateCurrency();
					SaveData();

				}

			}
		}
	}

	static int refreshCount = 0;
	static string lastHCID = "";

	static void OnIAPSuccess () {
		// NOTE: The script to ++ the HC is already in the DB class as the default callback.
		UIController.ShowPopUpSingleYes("SUCCESS", "The Credits have now been added!", "DONE", UIController.ClosePopUp);

	}

	static void OnIAPFail () {
		if (refreshCount == 0) {
			UIController.ShowComplexError("FAILED2", "There was an error connecting. Please try again.", BuyHCFromDB, false);
		} else {
			refreshCount --;
			UIController.ShowComplexError("FAILED3", "There was an error connecting. Please try again.", BuyHCFromDB, true);
		}

	}

	static void BuyHCFromDB () {
		UIController.ShowWait(30, null, BuyHCFromDB, null, true);
		DB.Singleton.CallDatabase("IAP_HC", OnIAPSuccess, OnIAPFail, lastHCID);
	}

	
	public static void SaveData () {
		
		UIController.ShowWait(30, null, SaveData, null, true);
		DataController.SaveAllData(false, OnSaveSuccess, OnSaveFail);
		
	}
	
	
	public static void OnSaveSuccess () {
		UIController.ClosePopUp();
	}
	
	public static void OnSaveFail ()
	{
		UIController.ShowComplexError("SAVE FAILED", "Failed to save the game. Please check your internet connection and try again.", SaveData, true);
	}
	
	public static void OnBuyFailed () {
		
		UIController.ShowError("IAP ERROR", "Purchase failed to process. Please check your internet connection and try again.");
		
	}
	
	public static void OnBuyCancelled () {
		
		UIController.ShowError("IAP ERROR", "The purchase was cancelled.");
		
	}
	
	static void CalibrateProduct (string id, string name, string price) {
		
		foreach ( StoreObject storeObject in StoreList ) {
			if (storeObject.id == id) {
				storeObject.CalibrateProduct(name, price);
			}
		}
	}
	
	public static void RestoreTransactions () {
		
		EnableListeners();
		restoring = true;

		#if UNITY_IOS
		RequestIAPDataApple();
		#endif
		
	}
	
	// LOWER LEVEL APIS --------------------------
	
	static void DummyReceiveFunction ()
	{
		StoreList.ForEach( p => CalibrateProduct(p.appleId, p.name, "$99.99"));
		ReceiveIAPData();
	}
	
	static void DummyRequestBuyFunction ()
	{
		KJTime.Add(KJTime.SpaceType.System, OnDummyBuySuccess, 2.0f, 1);
	}
	
	static void OnDummyBuySuccess () {
		OnBuySuccess(dummyId);
	}
	
	// APPLE
	
	#if UNITY_IOS
	
	static void RequestIAPDataApple ()
	{
		string[] productIdentifiers = new string[StoreList.Count];
		for (int i = 0 ; i < StoreList.Count ; i ++ )
		{
			productIdentifiers[i] = StoreList[i].appleId;
		}
		
//		StoreKitBinding.requestProductData( productIdentifiers );
		
	}
	
	static void RequestBuyIAPApple ()
	{
		//		Debug.Log ("StoreKit buy: " + currentStoreObject.appleId);
		restoring = false;
	//	StoreKitBinding.purchaseProduct(currentStoreObject.appleId, 1);
	}
	
	
	static void OnEnableApple()
	{
		if (hasListeners) return;
		
//		Debug.Log ("Listeners Enabled");
		// Listens to all the StoreKit events. All event listeners MUST be removed before this object is disposed!
//		StoreKitManager.transactionUpdatedEvent += transactionUpdatedEvent;
//		StoreKitManager.productPurchaseAwaitingConfirmationEvent += productPurchaseAwaitingConfirmationEvent;
//		StoreKitManager.purchaseSuccessfulEvent += purchaseSuccessfulEvent;
//		StoreKitManager.purchaseCancelledEvent += purchaseCancelledEvent;
//		StoreKitManager.purchaseFailedEvent += purchaseFailedEvent;
//		StoreKitManager.productListReceivedEvent += productListReceivedEvent;
//		StoreKitManager.productListRequestFailedEvent += productListRequestFailedEvent;
//		StoreKitManager.restoreTransactionsFailedEvent += restoreTransactionsFailedEvent;
//		StoreKitManager.restoreTransactionsFinishedEvent += restoreTransactionsFinishedEvent;
//		StoreKitManager.paymentQueueUpdatedDownloadsEvent += paymentQueueUpdatedDownloadsEvent;
		
		hasListeners = true;
		
	}
	
	
	static void OnDisableApple()
	{
		
		if (!hasListeners) return;
		
//		Debug.Log ("Listeners Disabled");
		// Remove all the event handlers
//		StoreKitManager.transactionUpdatedEvent -= transactionUpdatedEvent;
//		StoreKitManager.productPurchaseAwaitingConfirmationEvent -= productPurchaseAwaitingConfirmationEvent;
//		StoreKitManager.purchaseSuccessfulEvent -= purchaseSuccessfulEvent;
//		StoreKitManager.purchaseCancelledEvent -= purchaseCancelledEvent;
//		StoreKitManager.purchaseFailedEvent -= purchaseFailedEvent;
//		StoreKitManager.productListReceivedEvent -= productListReceivedEvent;
//		StoreKitManager.productListRequestFailedEvent -= productListRequestFailedEvent;
//		StoreKitManager.restoreTransactionsFailedEvent -= restoreTransactionsFailedEvent;
//		StoreKitManager.restoreTransactionsFinishedEvent -= restoreTransactionsFinishedEvent;
//		StoreKitManager.paymentQueueUpdatedDownloadsEvent -= paymentQueueUpdatedDownloadsEvent;
		
		hasListeners = false;
	}
	
//	static void productListReceivedEvent( List<StoreKitProduct> productList )
//	{
//		//		Debug.Log( "productListReceivedEvent. total products received: " + productList.Count );
//		
//		//		productList.ForEach( p => Debug.Log( "Got Product Info: " + p.productIdentifier + "\n" ));
//		productList.ForEach( p => CalibrateProduct(p.productIdentifier, p.title, p.formattedPrice));
//
//		if (restoring) {
//			RestoreTransactionsApple();
//		} else {
//			ReceiveIAPData();
//		}
//		//		restoring = true;
//		//		StoreKitBinding.restoreCompletedTransactions();
//		
//	}
	
	static void RestoreTransactionsApple () {
		
		UIController.ShowWait();
		restoring = true;
	//	StoreKitBinding.restoreCompletedTransactions();
		
	}
	
	static void restoreTransactionsFailedEvent( string error )
	{
		FailedTransData(error);
	}
	
	static void restoreTransactionsFinishedEvent()
	{
		Debug.Log ("RESTORE FINISH EVENT");
		KJTime.Add (KJTime.SpaceType.System, EndRestore, 1.2f, 1);
		
	}
	
	static void EndRestore () {
		UIController.ShowPopup("SUCCESS", "Restoration completed. All previously purhcased items have been restored!");
		DisableListeners();
	}
	
	
	
	
	
	static void productListRequestFailedEvent( string error )
	{
		FailedIAPData();
	}
	
	
	
//	
//	static void productPurchaseAwaitingConfirmationEvent( StoreKitTransaction transaction )
//	{
//		//		Debug.Log( "productPurchaseAwaitingConfirmationEvent: " + transaction );
//	}
//	
//	
//	static void purchaseSuccessfulEvent( StoreKitTransaction transaction )
//	{
//		Debug.Log( "E: purchaseSuccessfulEvent: " + transaction );
//		OnBuySuccess(transaction.productIdentifier);
//	}
//	
	static void purchaseFailedEvent( string error )
	{
		OnBuyFailed();
	}
	
//	
//	static void transactionUpdatedEvent( StoreKitTransaction transaction )
//	{
//		Debug.Log ("E: transactionUpdatedEvent: " + transaction.productIdentifier);
//	}
//	
//	static void paymentQueueUpdatedDownloadsEvent(List<StoreKitDownload> downloads)
//	{
//		Debug.Log ("E: paymentQueueUpdatedDownloadsEvent: " + downloads.Count);
//	}

	static void purchaseCancelledEvent( string error )
	{
		UIController.ShowError("IAP ERROR", "Purchase cancelled. Please check your internet connection and try again.");
	}
	
	#endif
}
