using UnityEngine;
using System.Collections;

public class MapIcon : MonoBehaviour {

	public UILabel nameLabel;
	public UILabel sectorLabel;
	public UISprite sectorIcon;
	public GameObject groupObjects;
	public UISprite arrowSprite;
	public UISprite highlighter;
	public UISprite mapSprite;

	int _sectorId = 0;
	float rotation = 0.0f;

	bool isMyMap = false;

	public int sectorId
	{
		get {
			return _sectorId;
		}

		set {

			_sectorId = value;

			string sectorText = string.Empty;
			sectorText += "Sector " + _sectorId;
//			highlighter.gameObject.SetActive(false);


			if (_sectorId == ProfileController.SectorId) {

//				sectorIcon.spriteName = "MapGreen_v3";
				highlighter.gameObject.SetActive(true);
				isMyMap = true;

				if (ProfileController.forceMap) {

					sectorText += "\nTAP HERE!";
				} else {
					sectorText += "\nYOU ARE HERE";
				}


				// Change Color to green

				nameLabel.color = new Color32(168, 255, 141, 255);
				sectorLabel.color = new Color32(200, 255, 180, 255);
				arrowSprite.color = new Color32(200, 255, 180, 255);

				sectorLabel.text = sectorText.ToUpper();
				nameLabel.text = SectorPattern.At(_sectorId - 1).name.ToUpper();

			} else if (ProfileController.Singleton.IsSecretMission(_sectorId)) { // Is Quest Map

				isMyMap = true;
				sectorText = "QUEST!";

//				Destroy(highlighter);

				// Change Color to yellow

				nameLabel.color = new Color32(255, 210, 110, 255);
				sectorLabel.color = new Color32(255, 220, 150, 255);
				arrowSprite.color = new Color32(255, 220, 150, 255);

				nameLabel.text = "SECRET MISSION";
				sectorLabel.text = "CARGO ROUTE!";

			} else if (ProfileController.Singleton.IsSecretBossMission(_sectorId)) {

				isMyMap = true;
				
				sectorText = "QUEST!";

//				Destroy(highlighter);
				
				// Change Color to yellow

				nameLabel.color = new Color32(255, 210, 110, 255);
				sectorLabel.color = new Color32(255, 220, 150, 255);
				arrowSprite.color = new Color32(255, 220, 150, 255);
				
				nameLabel.text = "SECRET MISSION";
				sectorLabel.text = "BOUNTY HUNT!";

			} else if (ProfileController.Singleton.IsRaidMission(_sectorId)) {

				isMyMap = true;
				
				sectorText = "RAID!";
				
				nameLabel.color = new Color32(100, 255, 190, 255);
				sectorLabel.color = new Color32(100, 255, 190, 255);
				arrowSprite.color = new Color32(100, 255, 190, 255);
				
				nameLabel.text = "RAID MISSION";
				sectorLabel.text = "UNKNOWN ENCOUNTER";

				switch (ProfileController.RaidProgress) {

				case 0:
					sectorLabel.text = "1ST ENCOUNTER";
					break;

				case 1:
					sectorLabel.text = "2ND ENCOUNTER";
					break;

				case 2:
					sectorLabel.text = "3RD ENCOUNTER";
					break;

				case 3:
					sectorLabel.text = "FINAL ENCOUNTER";
					break;

				}

			} else if (ProfileController.Singleton.IsBroadcastMission(_sectorId)) {

				// Ad can be shown in a broadcast.

				isMyMap = true;
			
				
				sectorText = "BROADCAST";
				
				nameLabel.color = new Color32(255, 210, 110, 255);
				sectorLabel.color = new Color32(255, 220, 150, 255);
				arrowSprite.color = new Color32(255, 220, 150, 255);
				
				nameLabel.text = "BROADCAST";
				sectorLabel.text = "COMMS SIGNAL";

			} else {

				isMyMap = false;

				groupPosition.x = 0;
				groupObjects.transform.localPosition = groupPosition;

				nameLabel.color = new Color32(255, 255, 255, 255);
				sectorLabel.color = new Color32(193, 240, 255, 255);

				arrowSprite.gameObject.SetActive(false);
				groupObjects.transform.localPosition = Vector3.left * 15.0f;

//				Destroy(highlighter);

				sectorLabel.text = sectorText.ToUpper();
				nameLabel.text = SectorPattern.At(_sectorId - 1).name.ToUpper();

			}


			mapSprite.color = SectorPattern.At(_sectorId - 1).SectorGraphic.moonColorTop * new Color(0.25f, 0.25f, 0.25f, 1.0f);
		}
	}

	float waveSpeed = 0.065f;
	float waveTotal = 0.0f;
	float waveMagnitude = 6.0f;
	Vector3 groupPosition = Vector3.zero;

	public void Update ()
	{
		if (isMyMap) {
			highlighter.transform.eulerAngles = Vector3.forward * rotation;
			rotation += 1.5f;

			waveTotal += waveSpeed;
			groupPosition.x = Mathf.Sin (waveTotal) * waveMagnitude;
			groupObjects.transform.localPosition = groupPosition;

		}
	}

	public void Start ()
	{
		// nameLabel.text = "Sector Name";
	}

	public void ClickSector ()
	{


//		if (!ProfileController.forceMap ||  isMyMap) {

			if (_sectorId == ProfileController.SectorId) {

				MapMenuController.Singleton.OnBack();

			} else if (ProfileController.Singleton.IsSecretMission(_sectorId)) {

				UIController.ShowPopupYesOrNo("SECRET MISSION", "You have one chance to intercept the Royal Cargo. Are you ready?", GoSecretNormal);

			} else if (ProfileController.Singleton.IsSecretBossMission(_sectorId)) {

				UIController.ShowPopupYesOrNo("SECRET MISSION", "You have one chance to secure the bounty. Are you ready?", GoSecretBoss);

			} else if (ProfileController.Singleton.IsRaidMission(_sectorId)) {

				UIController.ShowPopupYesOrNo("RAID MISSION", "An extremely powerful unit is detected in this area. You may engage it, but if you lose the fight, it will escape. Are you ready?", GoRaidBoss);

			} else if (ProfileController.Singleton.IsBroadcastMission(_sectorId)) {

				GoToBroadcast(); //UIController.ShowPopupYesOrNo("BROADCAST MISSION", "Broadcast mission.", GoToBroadcast);

			} else {

				FlurryController.SectorRepeated();

				UIController.ShowPopupYesOrNo("TRAVEL", "If you travel to this Sector, your progress in the current Sector will be lost. Do you wish to continue?", GoToSector);
			}

//		}
	}

	void GoSecretNormal ()
	{
		GoToSecret(false, false);
	}

	void GoSecretBoss ()
	{
		GoToSecret(true, false);
	}

	void GoRaidBoss ()
	{
		GoToSecret(false, true);
	}

	void GoToBroadcast ()
	{
		AdManager.ToggleShowAdPopUp(OnAdShown);
	}

	void OnAdShown () {

		Debug.Log("ON AD SHOWN");

		// Set that the Ad can't be seen anymore.
		AdManager.CanAdBeShown = false;

		// Reset the Sector
		sectorId = sectorId;

		if (SocialController.isLoggedIn) {
			
			// Call Data Base for small data and wait for response.
			UIController.ShowWait(30);
			DataController.SaveAllData (false, UIController.ClosePopUp);

		} else {

			DataController.SaveAllData ();

		}
	}

	void GoToSecret (bool isBoss = false, bool isRaid = false)
	{

		if (ProfileController.Hearts == 0) {
			
			UIController.ShowPopupHeart();
			
		} else if (!ProfileController.HasInventorySpace) {
			
			NotEnoughSpace();

		} else {

			ProfileController.CalibratePreGame();

//			DataController.SaveAllData();


			ProfileController.allyStandby = false;

			if (isBoss) {

				ProfileController.secretBossSector = _sectorId;
				ProfileController.secretMission = false;
				ProfileController.secretRaid = false;
				ProfileController.secretBoss = true;
				ProfileController.SetMarkQuestBossTime();

			} else if (isRaid) {

				ProfileController.secretRaidSector = _sectorId;
				ProfileController.secretMission = false;
				ProfileController.secretBoss = false;
				ProfileController.secretRaid = true;
				ProfileController.SetMarkRaidTime();

			} else {

				ProfileController.secretSector = _sectorId;
				ProfileController.secretMission = true;
				ProfileController.secretBoss = false;
				ProfileController.secretRaid = false;
				ProfileController.SetMarkQuestTime();

			}




			UIController.ShowWait();

			ProfileController.LoseHeart(ActualGo);

		}
	}

	void ActualGo ()
	{
		UIController.ClosePopUp();
		KJTime.RemoveGameTimers();
		ProfileController.allyStandby = false;
		MenuController.LoadToScene(1);
	}

	void NotEnoughSpace ()
	{
		UIController.ShowPopupYesOrNo(
			"CARGO FULL", 
			"You do not have enough space in your inventory to do this.\n\nPlease clear out some space in your inventory first by selling items that you don't need.",
			"Go To Inventory",
			"Close",
			GoToInventory);
	}

	void GoToInventory ()
	{
		MenuController.LoadToScene(5);
	}

	void GoToSector ()
	{


		UIController.ClosePopUp();
		ProfileController.SetSector(sectorId);
		ProfileController.SubSector = 1;
		MapMenuController.Singleton.OnBack();
	}




}
