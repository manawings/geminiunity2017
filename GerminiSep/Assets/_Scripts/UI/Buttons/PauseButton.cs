using UnityEngine;
using System.Collections;

public class PauseButton : KJNGUIButtonFunction
{

	protected override void KJ_OnPress ()
	{
		if (KJTime.IsPausedFor(KJTime.SpaceType.Game))
		{
//			KJTime.Resume();
		} else {
			KJCanary.PlaySoundEffect("PositiveButton");
			KJTime.Pause();
			UIController.ShowPause();
		}
		
		AddFlash(FlashOn, FlashOff, 0.05f, 3);
	}
	
	Color flashColor = new Color(0.5f, 0.3f, 0.0f, 1.0f);
	
	void FlashOn ()
	{
		Sprite.color = flashColor;
	}
	
	void FlashOff ()
	{
		Sprite.color = Color.black;
	}
}

