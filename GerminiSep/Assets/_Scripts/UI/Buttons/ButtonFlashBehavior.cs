using UnityEngine;
using System.Collections;

public class ButtonFlashBehavior : KJNGUIButtonFunction
{

	public enum SoundClip {
		PositiveButton,
		NegativeButton,
		NavButton,
		None,
		SellItem,
		EquipItem,
		UpgradeItem,
		PlayButton,
		BackButton
	}
	

	public GameObject targetObject;
	public string methodName;
	public bool instant = false;

	public int timesToFlash = 3;
	public int sceneToLoad = -1;

	public Color holdColor;
	public Color originalColor;
	public Color currentColor;

	public SoundClip sound;

	public UISprite frame;
	public UILabel buttonLabel;
	public UISprite iconSprite;
	
	public bool sizeReaction = false;
	public bool sizeReactionPress= true;

	float reducedSize = 0.85f;
	float currentSize = 1.0f;

	// Block the hold
	public bool listMode = false;
	float holdTimeOut = 0.0f;
	float holdTimeOutMax = 0.15f;

//	Vector3 reducedSizeVector = new Vector3(0.5f, 0.5f);
//	Vector3 normalSizeVector = new Vector3(1.0f, 1.0f);
	Vector3 originalBoxColliderSize;

	BoxCollider collider;

	float pressCooldown = 0.0f;
	float pressCooldownMax = 0.15f;

	void Start ()
	{
//		reducedSizeVector = Sprite.transform.localScale * reducedSize;
//		normalSizeVector = Sprite.transform.localScale;
		TimeSpaceType = KJTime.SpaceType.UI;
		collider = gameObject.GetComponent<BoxCollider>();
		originalBoxColliderSize = collider.size;
		if (Widget != null) originalColor = Widget.color;
		base.Start();
	}

	protected override void StartHold ()
	{

		if (hasTapRange) {
			InputController.BeginMark();
		}

		isPressed = true;
		holdTimeOut = 0.0f;

		AddTimer(RunCompressSize);

	}

	void PlaySound ()
	{
		if (sound == SoundClip.None) return;
		KJCanary.PlaySoundEffect(sound.ToString());
	}

	float sizeStep = 0.0f, sizeMag = 0.0f;

	const float TapRange = 5.0f;
	public bool hasTapRange = false;
	
	protected override void ReleaseHold ()
	{
		isPressed = false;

		if (hasTapRange) {
			if (InputController.LargestMarkedDistance > TapRange) {
				return;
			}
		}

		if (sizeReactionPress && (holdTimeOut < holdTimeOutMax)) {

			sizeStep = -1.0f;
			currentSize = 2.0f;
			sizeMag = 0.25f;

			if (sceneDelay) currentColor = flashColor;

			RemoveTimerOrFlash(RunCompressSize);
			AddTimer(RunNormalizeSize);


		}
	}

	void RunCompressSize ()
	{

		if (sizeReaction) {
			float sizeDifference = reducedSize - currentSize;
			currentSize += sizeDifference * 0.3f;

			transform.localScale = Vector3.one * currentSize;
			collider.size = originalBoxColliderSize * (currentSize / 1.0f);

			currentColor = Color.Lerp(currentColor, holdColor, 0.35f);
			Widget.color = currentColor;

			if (sizeDifference == 0) {
				RemoveTimerOrFlash(RunCompressSize);
				return;
			}
			
			if (sizeDifference > -0.01f) {

				currentColor = holdColor;
				sizeDifference = 0;
			}
		}

		if (listMode) holdTimeOut += DeltaTime;


	}

	void RunNormalizeSize ()
	{
		// float sizeDifference = 1.0f - currentSize;
		sizeStep += 0.35f;
		sizeMag *= 0.85f;
		currentSize = 1.0f + (Mathf.Sin (sizeStep) * sizeMag);//sizeDifference * 0.15f;

		currentColor = Color.Lerp(currentColor, originalColor, 0.2f);
		Widget.color = currentColor;

		transform.localScale = Vector3.one * currentSize;
		collider.size = originalBoxColliderSize * (currentSize / 1.0f);

		if (sizeMag == 0) {
			RemoveTimerOrFlash(RunNormalizeSize);
			return;
		}

		if (sizeMag < 0.01f) {
			sizeMag = 0;
		}
	}


	
	protected override void KJ_OnPress ()
	{
		StartHold();
		if (onPress) {

			AddFlash(FlashOn, FlashOff, 0.05f, timesToFlash, FlashDone);
			if (instant) ExecuteMethod();


		}
	}
	

	protected override void KJ_OnClick ()
	{
		if (!onPress) {

			AddFlash(FlashOn, FlashOff, 0.05f, timesToFlash, FlashDone);
			if (instant) ExecuteMethod();

		}
	}
	
	public Color flashColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	
	void FlashOn ()
	{
		Widget.color = flashColor;
	}
	
	void FlashOff ()
	{
		Widget.color = originalColor;
	}
	
	void FlashDone ()
	{
		if (!instant) ExecuteMethod();
	}

	void RunPressCooldown ()
	{
		pressCooldown -= Time.deltaTime;
		if (pressCooldown < 0.0f) {
			pressCooldown = 0.0f;
			RemoveTimerOrFlash(RunPressCooldown);
		}
	}
	
	protected override void ExecuteMethod ()
	{
		hasBeenPressed = false;


		if (pressCooldown == 0.0f && holdTimeOut < holdTimeOutMax) {

			pressCooldown = pressCooldownMax;
			AddTimer(RunPressCooldown);

			PlaySound();

			if (!onPress && !sceneDelay) {
				currentColor = flashColor;
				// AddFlash(FlashOn, FlashOff, 0.05f, timesToFlash);
			}


			if (!string.IsNullOrEmpty(methodName))
			{
				if (targetObject == null)
				{
					SendMessage(methodName);
				} else {
					targetObject.SendMessage(methodName);
				}
			}
			
			if (sceneToLoad != -1) MenuController.LoadToScene(sceneToLoad);

		}
	}
}

