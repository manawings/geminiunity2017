using UnityEngine;
using System.Collections;

public class SceneButton : KJNGUIButtonFunction
{
	
	public int sceneToLoad;
	public bool loadData = false;
	public float delay = 0.0f;

	protected override void KJ_OnClick ()
	{
		if (delay > 0.0f)
		{
			AddTimer(DelayedFunction, delay, 1);
		} else {
			DelayedFunction();	
		}
	}
	
	void DelayedFunction ()
	{
		if (loadData) DataController.LoadAllData();
		MenuController.LoadToScene(sceneToLoad);
	}
}

