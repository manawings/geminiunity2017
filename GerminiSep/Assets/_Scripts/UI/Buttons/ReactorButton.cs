﻿using UnityEngine;
using System.Collections;

public class ReactorButton : KJNGUIButtonFunction
{
	public bool isReady = false;

	public void Fire ()
	{
		if (GameController.PlayerUnit.IsAlive) {
			if (isReady) KJ_OnPress();
		}
	}

	protected override void KJ_OnPress ()
	{
		isReady = false;
		AddFlash(FlashOn, FlashOff, 0.05f, 3, FlashDone);
		GameController.PlayerUnit.FireReactor();
	}

	Color flashColor;

	Color blueFlash = new Color (0.0f, 0.2f, 0.5f, 1.0f);
	Color greenFlash = new Color (0.0f, 0.5f, 0.2f, 1.0f);
	Color yellowFlash = new Color (0.35f, 0.35f, 0.0f, 1.0f);
	Color pinkFlash = new Color (0.5f, 0.0f, 0.35f, 1.0f);

	bool isGraphicOn = false;
	Collectable.Type type;
	
	void FlashOn ()
	{
		Sprite.color = flashColor;
	}
	
	void FlashOff ()
	{
		Sprite.color = Color.black;
	}
	
	void FlashDone ()
	{
		if (isReady == false) {
			UIController.HideReactorButton();
			RemoveTimerOrFlash(AutoSwitchGraphic);
		}
	}

	void GraphicOn ()
	{

	}

	void GraphicOff ()
	{

	}

	public void CollectReactorGraphic (Collectable.Type type)
	{
		this.type = type;
		SwitchGraphicOn(type);
		AddFlash(FlashOn, FlashOff, 0.05f, 2);
		// AddTimer(AutoSwitchGraphic, 0.5f);
	}
	
	public void SwitchGraphicOn (Collectable.Type type)
	{
		switch (type)
		{
			
		case Collectable.Type.Green:
			Sprite.spriteName = "DamageGreen";
			flashColor = greenFlash;
			break;
			
			case Collectable.Type.Yellow:
			Sprite.spriteName = "DamageYellow";
			flashColor = yellowFlash;
			break;
			
			case Collectable.Type.Pink:
			Sprite.spriteName = "DamagePink";
			flashColor = pinkFlash;
			break;
			
			case Collectable.Type.Blue:
			Sprite.spriteName = "DamageBlue";
			flashColor = blueFlash;
			break;
			
		}

		isGraphicOn = true;
	}

	public void SwitchGraphicOff (Collectable.Type type)
	{
		switch (type)
		{
			
		case Collectable.Type.Green:
			Sprite.spriteName = "WeaponBlueOff";
			break;
			
		case Collectable.Type.Yellow:
			Sprite.spriteName = "WeaponBlueOff";
			break;
			
		case Collectable.Type.Pink:
			Sprite.spriteName = "WeaponBlueOff";
			break;
			
		case Collectable.Type.Blue:
			Sprite.spriteName = "WeaponBlueOff";
			break;
			
		}

		isGraphicOn = false;
	}

	void AutoSwitchGraphic ()
	{
		if (isGraphicOn)
		{
			SwitchGraphicOff(type);
		} else {
			SwitchGraphicOn(type);
		}
	}
}
