using UnityEngine;
using System.Collections;

public class PanelButtonBehavior : KJNGUIButtonFunction
{
	
	public HangarController.PanelType panelType;
	public bool backwards = false;
	
	protected override void KJ_OnClick ()
	{
		KJTime.Add(ActualFunction, 0.05f, 1);
	}
	
	void ActualFunction ()
	{
		InputController.LockControls();
		HangarController.Singleton.SwapTo(panelType, backwards);
	}
	
}

