﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]

public class CombatHeader : KJBehavior {

	public UILabel headLabel, subLabel;
	public UISprite leftClasp, rightClasp;

	public KJTime.TimeDelegate headerDelegate;
	public Color headerColor, bodyColor;

	Hashtable headerTweenIn;
	Hashtable headerTweenOut;

	ColorType colorType = ColorType.Green;

	public enum ColorType {

		Green,
		Yellow,
		Blue,
		Red,
		Pink

	}

	void SetColors ()
	{
		switch (colorType) {

		case ColorType.Green:
			headerColor = new Color32(165, 255, 210, 255);
			bodyColor = new Color32(40, 255, 100, 255);
			break;

		case ColorType.Yellow:
			headerColor = new Color32(255, 210, 80, 255);
			bodyColor = new Color32(255, 140, 40, 255);
			break;

		case ColorType.Blue:
			headerColor = new Color32(160, 215, 255, 255);
			bodyColor = new Color32(5, 180, 255, 255);
			break;

		case ColorType.Red:
			headerColor = new Color32(255, 165, 130, 255);
			bodyColor = new Color32(255, 77, 77, 255);
			break;

		case ColorType.Pink:
			headerColor = new Color32(255, 165, 255, 255);
			bodyColor = new Color32(200, 100, 255, 255);
			break;
		}

		headLabel.color = headerColor;
		subLabel.color = bodyColor;
		leftClasp.color = bodyColor;
		rightClasp.color = bodyColor;
	}

//	void Update ()
//	{
//		headLabel.color = headerColor;
//		subLabel.color = bodyColor;
//		leftClasp.color = bodyColor;
//		rightClasp.color = bodyColor;
//	}


	void Start () {



		headerTweenIn = new Hashtable();
		headerTweenIn.Add("name", "headerTween");
		headerTweenIn.Add("from", 0.0f);
		headerTweenIn.Add("to", 1.0f);
		headerTweenIn.Add("time", 0.50f);
		headerTweenIn.Add("onupdate", "OnHeaderTween");
		headerTweenIn.Add("oncomplete", "OnHeaderCompleteIn");
		headerTweenIn.Add("easetype", iTween.EaseType.easeOutBack);
		
		headerTweenOut = new Hashtable();
		headerTweenOut.Add("name", "headerTween");
		headerTweenOut.Add("from", 1.0f);
		headerTweenOut.Add("to", 0.0f);
		headerTweenOut.Add("time", 0.50f);
		headerTweenOut.Add("onupdate", "OnHeaderTween");
		headerTweenOut.Add("oncomplete", "OnHeaderCompleteOut");
		headerTweenOut.Add("easetype", iTween.EaseType.easeInBack);

		gameObject.SetActive(false);
		
	}

	bool isTweening = false;

	public bool ShowHeader (string labelText, string subText, KJTime.TimeDelegate headerDelegate, ColorType colorType)
	{
		if (isTweening) return false;

		isTweening = true;

		headLabel.text = labelText.ToUpper();
		subLabel.text = subText.ToUpper();
		this.headerDelegate = headerDelegate;
		this.colorType = colorType;

		SetColors();

		gameObject.SetActive(true);
		
		try { iTween.Stop(gameObject, "headerTween"); }
		catch (UnityException error) { }
		
		iTween.ValueTo(gameObject, headerTweenIn);
		OnHeaderTween(0.0f);
		
		// KJTime.Flash(FlashHeaderOn, FlashHeaderOff, 0.05f, 5);
		return true;
	}
	
	void FlashHeaderOn ()
	{
		headLabel.color = Color.white;
		subLabel.color = Color.white;
		leftClasp.color = Color.white;
		rightClasp.color = Color.white;
	}
	
	void FlashHeaderOff ()
	{
		headLabel.color = headerColor;
		subLabel.color = bodyColor;
		leftClasp.color = bodyColor;
		rightClasp.color = bodyColor;
	}
	
	void HideHeader ()
	{
		iTween.ValueTo(gameObject, headerTweenOut);	
	}

	float claspOut = 240.0f, claspIn = 120.0f;

	
	void OnHeaderTween (float val)
	{
		headLabel.alpha = val;
		headLabel.transform.localScale = Vector3.one * val * 22.0f;

		subLabel.alpha = val;
		subLabel.transform.localScale = Vector3.one * val * 16.5f;
		// KJMath.SetY(headerLabel.transform, 400.0f - (val * 400.0f));

		float claspDifference = claspOut - claspIn;
		float claspPosition = claspOut - val * claspDifference;

		KJMath.SetX(rightClasp.transform, claspPosition);
		KJMath.SetX(leftClasp.transform, - claspPosition);


	}
	
	void OnHeaderCompleteIn ()
	{
		KJTime.Add(HideHeader, 2.0f, 1);
	}
	
	void OnHeaderCompleteOut ()
	{
		isTweening = false;
		if (headerDelegate != null) headerDelegate();
		headerDelegate = null;
		gameObject.SetActive(false);
	}


}
