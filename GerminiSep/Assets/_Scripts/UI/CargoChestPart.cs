﻿using UnityEngine;
using System.Collections;

public class CargoChestPart : MonoBehaviour {

	UISprite sprite;

	// Use this for initialization
	void Start () {
		sprite = GetComponent<UISprite>();
	}

	float yVel, xVel, spin;
	Vector3 newPosition;

	public void Deploy (Color color, float yVel, float xVel, float spin)
	{
		this.yVel = yVel;
		this.xVel = xVel;
		this.spin = spin;

		sprite.color = color;
		newPosition = transform.localPosition;
		KJTime.Add (Run);
	}

	void Run ()
	{
		newPosition.x += xVel;
		newPosition.y += yVel;

		xVel *= 0.82f;
		yVel *= 0.82f;

		// sprite.transform.eulerAngles += Vector3.forward * spin;

		sprite.color = Color.Lerp(sprite.color, Color.black, 0.02f);
		transform.localPosition = newPosition;

		if (sprite.color.r < 0.05f) {
			KJTime.Remove(Run);
			gameObject.SetActive (false);
		}

	}
}
