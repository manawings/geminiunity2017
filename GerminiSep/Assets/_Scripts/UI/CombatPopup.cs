﻿using UnityEngine;
using System.Collections;

public class CombatPopup : KJBehavior {
	
	UILabel uiLabel;
	
	void Awake ()	
	{
		uiLabel = GetComponent<UILabel>();
	}
	
	public UIFont damageWhite;
	public UIFont damageGreen;
	public UIFont damageYellow;
	public UIFont damageRed;
	public UIFont damageBlue;
	public UIFont damagePink;
	
	float currentScale;
	float targetScale;
	float currentAlpha, targetAlpha;
	
	Color myColor = Color.white;
	Color startColor;
	Color endColor = new Color (0.0f, 0.0f, 0.0f, 1.0f);
	
	Vector3 scaleOne = Vector3.one * 5159.0f; // Intended Size / UIRoot scale.
	
	bool isFadingOut = false;
	bool isCritical = false;
	
	public enum Type {
		White,
		Red,
		Yellow,
		Green,
		Blue,
		Pink
	}

	public void Deploy (Vector3 position, string damageString, CombatPopup.Type type = CombatPopup.Type.White, bool isCritical = false)
	{
		this.isCritical = isCritical;

		switch (type)
		{
			
		case Type.White:
			startColor = Color.white * 1.5f;
			uiLabel.font = damageWhite;
			break;
			
		case Type.Yellow:
			startColor = new Color(1.0f, 0.8f, 0.2f) * 1.5f;
			uiLabel.font = damageYellow;
			break;
			
		case Type.Green:
			startColor = new Color(0.3f, 1.0f, 0.2f) * 1.5f;
			uiLabel.font = damageGreen;
			break;
			
		case Type.Red:
			startColor = new Color(1.0f, 0.3f, 0.3f) * 1.5f;
			uiLabel.font = damageRed;
			break;
			
		case Type.Blue:
			startColor = new Color(0.3f, 0.5f, 1.0f) * 1.5f;
			uiLabel.font = damageBlue;
			break;

		case Type.Pink:
			startColor = new Color(1.0f, 0.5f, 0.8f) * 1.5f;
			uiLabel.font = damagePink;
			break;
			
		}
		
		scaleOne.z = 0.0f;
		
		transform.position = position;//new Vector3(position.x, position.y, -400.0f);
		transform.parent = UIController.MyUIPanel.transform;
		
		Vector3 tempPos = transform.position;

		uiLabel.text = damageString;

		float endTime;

		if (isCritical)
		{
			currentScale = 7.0f;
			targetScale = 1.2f;
			endTime = 3.0f;
			AddFlash(FlashOn, FlashOff, 0.03f, 7);
			tempPos.z = -5.0f;
		} else {
			currentScale = 5.0f;
			targetScale = 1.0f;
			endTime = 1.5f;
			myColor = startColor;
			tempPos.z = -1.0f;
		}
		
		currentAlpha = targetAlpha = 1.0f;


		transform.position = tempPos;

		
		Run ();
		
		AddTimer(Run);
		AddTimer(FadeOut, endTime - 1.0f, 1);
		AddTimer(End, endTime, 1);
		
		isFadingOut = false;
	}

	void FlashOn ()
	{
		uiLabel.color = Color.white;
	}

	void FlashOff ()
	{
		uiLabel.color = endColor;
	}
	
	void Run ()
	{
		float differenceScale = targetScale - currentScale;
		currentScale += differenceScale * 0.25f;
		uiLabel.transform.localScale = scaleOne * currentScale;
		
		if (isFadingOut)
		{
			differenceScale = targetAlpha - currentAlpha;
			currentAlpha += differenceScale * 0.15f;
			uiLabel.alpha = currentAlpha;
		} else {
			if (!isCritical) {
				myColor = Color.Lerp(myColor, endColor, 0.10f);
				uiLabel.color = myColor;
			}
		}
		
		transform.Translate(Vector3.up * 10.0f * DeltaTime, Space.Self);
		
		
	}
	
	void End ()
	{
		Deactivate();	
	}
	
	void FadeOut ()
	{
		isFadingOut = true;
		targetAlpha = 0.0f;
	}
}
