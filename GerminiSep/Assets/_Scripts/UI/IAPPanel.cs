﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IAPPanel : KJBehavior {

	public BlitzButton[] iapButtons;
	public BlitzButton gdButton;

	
	public void Deploy ( )
	{

		gameObject.SetActive(true);
		Calibrate();

	}

	public void Calibrate ()
	{

//		Debug.Log ("CALIBRATING IAP PANEL --------------- ");

		for ( int i = 0 ; i < 5 ; i ++ )
		{
			BlitzButton button = iapButtons[i];
			StoreObject storeObject = IAPController.StoreList[i];

			// Calibrate the Button
			button.buttonLabel = storeObject.price;
			button.defaultPrice = storeObject.units;
			if (button.gameObject.activeInHierarchy) button.SetOptions();

//			Debug.Log("Calibrated: " + storeObject.appleId + " " + storeObject.units);
		}

		// Calibrate the Gem Doubler


		gdButton.defaultPrice = IAPController.StoreList[5].units;

//		Debug.Log ("Double: " + ProfileController.GemDouble);
		if (ProfileController.GemDouble)  {
			gdButton.buttonLabel = string.Empty;

			gdButton.graphicColor = new Color32 (15, 50, 10, 255);
			gdButton.holdColor = new Color32 (0, 125, 60, 255);
			gdButton.flashColor = new Color32 (80, 255, 0, 255);
			gdButton.frameColor = new Color32 (25, 70, 50, 255);

		}
		else {

			gdButton.graphicColor = new Color32 (10, 25, 50, 255);
			gdButton.holdColor = new Color32 (0, 50, 125, 255);
			gdButton.flashColor = new Color32 (0, 255, 245, 255);
			gdButton.frameColor = new Color32 (25, 50, 70, 255);

			gdButton.buttonLabel = IAPController.StoreList[5].price;

		}

		gdButton.SetOptions();
	}

	void RequestItemPurchase (int id)
	{
		//IAPManager.RequestBuyItem(id);
		UIController.ShowWait(10f);
		IAPController.Singleton.RequestBuyItem(id);
	}

	void Buy1 ()
	{
		RequestItemPurchase(0);
	}

	void Buy2 ()
	{
		RequestItemPurchase(1);
	}

	void Buy3 ()
	{
		RequestItemPurchase(2);
	}

	void Buy4 ()
	{
		RequestItemPurchase(3);
	}

	void Buy5 ()
	{
		RequestItemPurchase(4);
	}

	void BuyGD () {

		if (ProfileController.GemDouble) {
			UIController.ShowSingleClose ("GEM DOUBLER", "You have already purchased this upgrade!");
		} else {
			UIController.ShowPopupYesOrNo("GEM DOUBLER", "This is a permanent upgrade that will double the Gems you gain from combat.", "BUY", "CANCEL", OnGDBuy);
		}

	}

	void OnGDBuy () {
		RequestItemPurchase(5);
	}
	
	void OnTap ()
	{

	}
	
	void OnExit ()
	{
		UIController.ClosePopUp();
	}
	
}