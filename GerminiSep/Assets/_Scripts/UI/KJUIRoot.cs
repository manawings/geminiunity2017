﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]

public class KJUIRoot : MonoBehaviour {
	
	UIRoot _uiRoot;
	UIRoot uiRoot
	{
		get {
			if (_uiRoot == null) _uiRoot = GetComponent<UIRoot>();
			return _uiRoot;
		}
	}

	// Use this for initialization
	void Start () {
		SetScreen();
			
	}

#if UNITY_EDITOR
//	void Update ()
//	{
//		SetScreen();
//	}
#endif
	

	void SetScreen ()
	{
		uiRoot.manualHeight = 0;

		float originalAspectRatio = 2.0f / 3.0f;
		float currentAspectRatio = (float) Screen.width / (float) Screen.height;

		bool snapToWidth = true;
		if (currentAspectRatio > (originalAspectRatio + 0.01f)) snapToWidth = false;

//		Debug.Log ("W: " + Screen.width + " H: " + Screen.height + " AR: "+ currentAspectRatio);

		// uiRoot.manualHeight = Screen.height;
//

			 
		// Calculate Height to keep width the same.
		float heightDivision = Mathf.Floor(Screen.height / 480.0f);
		if (heightDivision == 3.0f) heightDivision = 2.0f;
		float adjustedHeight = ((float) Screen.height / heightDivision);
		float adjustedWidth = ((float) Screen.width / heightDivision);
		float targetWidth = 320;
		float widthRatio ;// = targetWidth / adjustedWidth;
			
		//uiRoot.manualHeight = (int) ((float) Screen.height / heightDivision);

//		Debug.Log ("snapToWidth: " + snapToWidth);
			

		if (snapToWidth) {

			widthRatio = targetWidth / adjustedWidth;
			float newHeight = widthRatio * adjustedHeight;
			uiRoot.manualHeight = (int) newHeight;

		} else {

			// widthRatio = adjustedWidth / targetWidth;
			uiRoot.manualHeight = (int) ((float) Screen.height / heightDivision);

		}
	}
}
