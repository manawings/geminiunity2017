﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]



public class MainBGController : MonoBehaviour {

	public Color skyColorTop, skyColorBottom;
	public Color moonColorTop, moonColorBottom;
	public Color glowColor;
	public Color midMainColor;
	public Color particleColorStart;
	public Color particleColorEnd;

	public GameObject subBGTester;
	public GameObject moobObject;
	public tk2dSprite midMainS;

	public MidBGController midBG;

	public tk2dSprite skySprite;
	public tk2dSprite moonSprite;
	public tk2dSprite glowSprite;

	string bgSet = string.Empty;
	int currentBgId = 0;

	void Start () {



		Update();

	}

	public void Deploy ()
	{


//		ProfileController.SetSector(ProfileController.SectorId);

		BGGraphics graphics = ProfileController.ActualSector.SectorGraphic;
		currentBgId =  ProfileController.ActualSector.SectorBGStartId;
		bgSet =  ProfileController.ActualSector.SectorBGSet;

		moobObject.transform.localPosition = ProfileController.ActualSector.SectorMoonPosition;
		moobObject.transform.localRotation = Quaternion.AngleAxis(ProfileController.ActualSector.SectorMoonRotation, Vector3.forward);// Quaternion.Euler(new Vector3(0, 0, ProfileController.Sector.SectorMoonRotation));
		
		skyColorTop = graphics.skyColorTop;
		skyColorBottom = graphics.skyColorBottom;
		moonColorTop = graphics.moonColorTop;
		moonColorBottom = graphics.moonColorBottom;
		glowColor = graphics.glowColor;
		midMainColor = graphics.midMainColor;

		skySprite.topColor = skyColorTop;
		skySprite.bottomColor = skyColorBottom;
		moonSprite.topColor = moonColorTop;
		moonSprite.bottomColor = moonColorBottom;
		glowSprite.color = glowColor;
		midMainS.color = midMainColor;

		UnityEngine.Random.seed = ProfileController.ActualSector.Seed;
		moonSprite.SetSprite("Moon" + UnityEngine.Random.Range(1, 9));



//		Update();
//		if (ProfileController.Singleton.lowGraphic) {
//			midBG.gameObject.SetActive(false);
//			subBGTester.SetActive(false);
//		} else {
//
//		}

		midBG.Deploy(midMainColor, bgSet, currentBgId);
		subBGTester.SetActive(false);

	}

	void Update ()
	{
#if UNITY_EDITOR

		skySprite.topColor = skyColorTop;
		skySprite.bottomColor = skyColorBottom;
		moonSprite.topColor = moonColorTop;
		moonSprite.bottomColor = moonColorBottom;
		glowSprite.color = glowColor;
		midMainS.color = midMainColor;
#endif
	}

	public void CopyDataToClipBoard ()
	{
		string finalString = string.Empty;

		// Create (new Color(2.0f, 1.0f, 2.0f, 1.0f), new Color(2.0f, 1.0f, 2.0f, 1.0f));

		Color[] colorArray = {skyColorTop, skyColorBottom, moonColorTop, moonColorBottom, glowColor, midMainColor, particleColorStart, particleColorEnd};

		finalString = "Create (";

		for (int i = 0 ; i < colorArray.Length ; i ++ )
		{
			Color proxyColor = colorArray[i];
			finalString += "new Color (";

			finalString += proxyColor.r.ToString("F2") + "f, ";
			finalString += proxyColor.g.ToString("F2") + "f, ";
			finalString += proxyColor.b.ToString("F2") + "f, ";
			finalString += proxyColor.a.ToString("F2") + "f ";

			finalString += ")";

			if (i != colorArray.Length - 1) finalString += ",";
		}


		finalString += ");";

//		Debug.Log ("Copy: " + finalString);

		#if UNITY_EDITOR
		EditorGUIUtility.systemCopyBuffer = finalString;
		#endif

	}

	private static PropertyInfo m_systemCopyBufferProperty = null;
	private static PropertyInfo GetSystemCopyBufferProperty()
	{
		if (m_systemCopyBufferProperty == null)
		{
			Type T = typeof(GUIUtility);
			m_systemCopyBufferProperty = T.GetProperty("systemCopyBuffer", BindingFlags.Static | BindingFlags.NonPublic);
			if (m_systemCopyBufferProperty == null)
				throw new Exception("Can't access internal member 'GUIUtility.systemCopyBuffer' it may have been removed / renamed");
		}
		return m_systemCopyBufferProperty;
	}

	public static string ClipBoard
	{
		get 
		{
			PropertyInfo P = GetSystemCopyBufferProperty();
			return (string)P.GetValue(null,null);
		}
		set
		{
			PropertyInfo P = GetSystemCopyBufferProperty();
			P.SetValue(null,value,null);
		}
	}

}
