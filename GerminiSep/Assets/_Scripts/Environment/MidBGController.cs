﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MidBGController : KJBehavior {

	public GameObject midBGSectionPrefab;
	float gameHeight, sectionHalfHeight, screenLimit;
	// Difference = Gap between the edges of two sections.
	// Actual = Gap between the anchor of two sections.

	float paddingPercent = 0.1f;
	float sectionHeight = 400.0f;
	float sectionWidth = 390.0f;
	float sectionGap = 500.0f;
	float sectionXAttach;
	bool isLeft;

	Color mainColor;

	List<MidBGSection> sections;

	int currentBgId;
	string setName;
	
	public void Deploy (Color mainColor, string setName, int currentId) {
	
		this.mainColor = mainColor;
		this.currentBgId = currentId;
		this.setName = setName;

		isLeft = KJDice.Roll(50);

		// Create all the BG Sections
		sections = new List<MidBGSection>();
		sectionHalfHeight = sectionHeight * 0.5f;
		screenLimit = tk2dCamera.Instance.ScreenExtents.yMin;
		sectionXAttach = 0.5f * GameController.gameStageWidth - sectionWidth;

		int i = 3;
		while (i > 0)
		{

			MidBGSection bgSection = ((GameObject) Instantiate(midBGSectionPrefab)).GetComponent<MidBGSection>();
			bgSection.transform.parent = transform;
			bgSection.SetColors(mainColor);
			SetSectionToSlot(bgSection);

			sections.Add(bgSection);
			i--;
		}

		AddTimer (Run);

	}

	void Run ()
	{
		for ( int i = sections.Count - 1 ; i > - 1 ; i -- )
		{
			MidBGSection section = sections[i];
			section.Run();

			float sectionYBorder = section.transform.position.y + sectionHalfHeight;
			if (sectionYBorder < screenLimit) {
				SetSectionToSlot(section);
			}
		}
	}

	void SetSectionToSlot (MidBGSection section)
	{
		float insertPositionX = 0;
		float insertPositionY = 0;
		float xFactor = isLeft ? -1 : 1;

		insertPositionY = TopSectionY + sectionGap;
		insertPositionX = sectionXAttach * xFactor;

		isLeft = !isLeft;
	
		currentBgId ++; if (currentBgId > 3) currentBgId = 1;
		section.SetPosition(new Vector3(insertPositionX, insertPositionY, 0.0f), isLeft, setName, currentBgId);

	}

	float TopSectionY {
		get {

			float currentSectionHeight = - 150.0f - sectionGap;

			for ( int i = sections.Count - 1 ; i > - 1 ; i -- )
			{
				MidBGSection section = sections[i];
				if (section.transform.localPosition.y > currentSectionHeight) {
					currentSectionHeight = section.transform.localPosition.y;
				}
			}

			return currentSectionHeight;
		}
	}
}
