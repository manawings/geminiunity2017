﻿using UnityEngine;
using System.Collections;

public class MidBGSection : KJBehavior {

	float scrollSpeed = 35.0f;

	void Start () {

		cameraTransform = Camera.main.transform;

	}
	
	Vector3 newPosition;
	Transform cameraTransform;
	float savedX;

	public tk2dSprite mainPart;

	public void SetColors (Color mainColor)
	{
		mainPart.color = mainColor;
	}

	public void Run () {
		
		newPosition.y -= scrollSpeed * DeltaTime;
		newPosition.x = cameraTransform.position.x * 0.5f + savedX;
		transform.localPosition = newPosition;

	}

	public void SetPosition (Vector3 newPosition, bool isLeft, string setName = "Set1Back", int sectionNum = 1)
	{
		transform.localPosition = newPosition;
		this.newPosition = newPosition;
		savedX = newPosition.x;

		mainPart.SetSprite(setName + sectionNum);

		Vector3 newScale = Vector3.one;

		if (isLeft)
		{

		} else {
			// newScale
			newScale.x = -1;
			newScale.y = -1;
		}

		transform.localScale = newScale;
	}
}
