using UnityEngine;
using System.Collections;

public class ModuleLibrary
{

	public static readonly int numberOfModules = 69; // Number of Modules ##61 is Old Version##

	public static Module GetFromId (int id)
	{
		
		// TODO: Find a way to implement a more robust system?
		
		switch (id)
		{

		case 1:
			return new M_Reflector();
			break;
			
		case 2:
			return new M_CounterGreenMissile();
			break;
			
		case 3:
			return new M_CounterBlueMissile();
			break;
			
		case 4:
			return new M_CounterRedMissile();
			break;
		
		case 5:
			return new M_CounterPurpleMissile();
			break;
			
		case 6:
			return new M_AutoBlackHole();
			break;
			
		case 7:
			return new M_UltimateMissile();
			break;
			
		case 8:
			return new M_BlockDamage();
			break;
		
		case 9:
			return new M_Regeneration();
			break;
		
		case 10:
			return new M_AutoShield();
			break;
			
		case 11:
			return new M_Burn();
			break;
		
		case 12:
			return new M_Stun();
			break;
			
		case 13:
			return new M_CriticalStrike();
			break;	
		
		case 14:
			return new M_LifeSteal();
			break;
		
		case 15:
			return new M_Repair();
			break;
			
		case 16:
			return new M_UltimateScatter();
			break;
			
		case 17:
			return new M_UltimateChain();
			break;
		
		case 18:
			return new M_HealEachKill();
			break;
			
		case 19:
			return new M_GreenLaserScatter();
			break;
			
		case 20:
			return new M_ReactorEachKill();
			break;
		
		case 21:
			return new M_GreenMissile();
			break;	
			
		case 22:
			return new M_BlueMissile();
			break;			
			
		case 23:
			return new M_RedMissile();
			break;				
			
		case 24:
			return new M_PurpleMissile();
			break;	
		
		case 25:
			return new M_InverseDamage();
			break;

		case 26:
			return new M_GreenChain();
			break;

		case 27:
			return new M_BlueChain();
			break;

		case 28:
			return new M_RedChain();
			break;

		case 29:
			return new M_PurpleChain();
			break;

		case 30:
			return new M_CounterGreenChain();
			break;

		case 31:
			return new M_CounterBlueChain();
			break;

		case 32:
			return new M_CounterRedChain();
			break;

		case 33:
			return new M_CounterPurpleChain();
			break;

		case 34:
			return new M_GreenScatter();
			break;

		case 35:
			return new M_BlueScatter();
			break;

		case 36:
			return new M_RedScatter();
			break;

		case 37:
			return new M_PurpleScatter();
			break;
			
		case 38:
			return new M_CollectGemGetHp();
			break;

		case 39:
			return new M_ReactorRecycle();
			break;

		case 40:
			return new M_ReactorHeal();
			break;

		case 41:
			return new M_ReactorCharge();
			break;

		case 42:
			return new M_MineGuard();
			break;

		case 43:
			return new M_MissileGuard();
			break;
		
		case 44:
			return new M_BeamGuard();
			break;

		case 45:
			return new M_Regeneration_S();
			break;

		case 46:
			return new M_LifeSteal_S();
			break;

		case 47:
			return new M_BlockDamage_S();
			break;

		case 48:
			return new M_CriticalStrike_S();
			break;

		case 49:
			return new M_Repair_S();
			break;

		case 50:
			return new M_GreenLaser();
			break;

		case 51:
			return new M_BlueLaser();
			break;

		case 52:
			return new M_RedLaser();
			break;

		case 53:
			return new M_PurpleLaser();
			break;

		case 54:
			return new M_UltimateLaser();
			break;

		case 55:
			return new M_AbsorptionShield();
			break;

		case 56:
			return new M_StaticShield();
			break;

		case 57:
			return new M_DeflectorShield();
			break;

		case 58:
			return new M_ReactorShield();
			break;

		case 59:
			return new M_RedLaserScatter();
			break;

		case 60:
			return new M_BlueLaserScatter();
			break;

		case 61:
			return new M_PurpleLaserScatter();
			break;

		// New Module
		case 62:
			return new M_SonarGreen();
			break;
		
		case 63:
			return new M_SonarBlue();
			break;

		case 64:
			return new M_SonarRed();
			break;

		case 65:
			return new M_SonarPink();
			break;

		case 66:
			return new M_UltimateSonar();
			break;

		case 67:
			return new M_BurnGuard();
			break;

		case 68:
			return new M_AbsorptionShield2();
			break;

		case 69:
			return new M_BurnHeal();
			break;

			//		case 62:
//			return new M_CounterGreenScatter();
//			break;
//
//		case 63:
//			return new M_CounterBlueScatter();
//			break;
//			
//		case 64:
//			return new M_CounterRedScatter();
//			break;
//			
//		case 65:
//			return new M_CounterPurpleScatter();
//			break;
			
			
		case 100:
			return new M_Ultimate();
			break;
		
		//Ship
		case 301:
			return new Ship_Power();
			break;
		case 302:
			return new Ship_Defense();
			break;
		case 303:
			return new Ship_LifePoint();
			break;
		case 304:
			return new Ship_Missile_Green();
			break;
		case 305:
			return new Ship_Missile_Red();
			break;
		case 306:
			return new Ship_Missile_Blue_2();
			break;
		case 307:
			return new Ship_Sonar_1();
			break;
		case 308:
			return new Ship_Sonar_2();
			break;
		case 309:
			return new Ship_Sonar_3();
			break;
		case 310:
			return new Ship_Scatter_1();
			break;
		case 311:
			return new Ship_Scatter_2();
			break;
		case 312:
			return new Ship_Scatter_3();
			break;
		case 313:
			return new Ship_Scatter_4();
			break;
		case 314:
			return new Ship_Shock_1();
			break;
		case 315:
			return new Ship_Shock_2();
			break;
		case 316:
			return new Ship_Shock_3();
			break;
		case 317:
			return new Ship_Shock_4();
			break;
		case 318:
			return new Ship_Shock_5();
			break;
		case 319:
			return new Ship_Block_Hit_1();
			break;
		case 320:
			return new Ship_Block_Hit_2();
			break;
		case 321:
			return new Ship_Block_Hit_3();
			break;
		case 322:
			return new Ship_Laser_1();
			break;
		case 323:
			return new Ship_Laser_2();
			break;
		case 324:
			return new Ship_Laser_3();
			break;
		case 325:
			return new Ship_Shield_Odin_1();
			break;
		case 326:
			return new Ship_Shield_Odin_2();
			break;
		case 327:
			return new Ship_Conah_1();
			break;
		case 328:
			return new Ship_Oriash_1();
			break;
		case 329:
			return new Ship_Zarall_1();
			break;
		case 330:
			return new Ship_Conah_2();
			break;
		case 331:
			return new Ship_Oriash_2();
			break;
		case 332:
			return new Ship_Zarall_2();
			break;
		case 333:
			return new Ship_Conah_3();
			break;
		case 334:
			return new Ship_Conah_4();
			break;
		case 335:
			return new Ship_Shield_EnergyYellow();
			break;

			//Elite && Enemy

		case 201:
			return new M_Elite_BlockDamage();
			break;
		case 202:
			return new M_Elite_Shield();
			break;
		case 203:
			return new M_Elite_BlockDamage_Easy();
			break;
		case 204:
			return new M_Elite_Shield_Easy();
			break;
		case 205:
			return new M_E_BeamShield_Pink();
			break;
		case 206:
			return new M_E_BeamShield_Red();
			break;
		case 207:
			return new M_E_BeamShield_Green();
			break;
		case 208:
			return new M_E_BeamShield_Yellow();
			break;
		case 209:
			return new M_E_BeamShield_Blue();
			break;	
		case 210:
			return new M_E_BeamShield_Orange();
			break;
		case 211:
			return new M_Elite_BlockDamageHard();
			break;
		case 212:
			return new M_Elite_ShieldHard();
			break;
		case 213:
			return new M_Elite_Invisible();
			break;


		//Rival

		case 401:
			return new M_Rival_Shield();
			break;
		


		}
		return new Module();
	}
}

