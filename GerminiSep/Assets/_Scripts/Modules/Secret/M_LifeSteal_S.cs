using UnityEngine;
using System.Collections;

public class M_LifeSteal_S : Module {
	public const float lifeStealPercent = 0.03f;


	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		staticPower = lifeStealPercent  *  unit.stats.damage;

	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Prime Vampire Strike";
		rarity = ItemBasic.Rarity.Secret;
		rarityScaleUp = 0;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnDealDamage;
		procChance = 10;
		cooldown = 0f;
		itemGroup = ItemGroup.Weapon;
		itemColor = ItemColor.Pink;
		// Calculate the Power

		// Write the custom Description
//		descriptionString = "When attack target you have a "+procChance+" % chance to heal "+ (lifeStealPercent * 100.0f )+"% of Power ";
		descriptionString = ConvertString("@PRC% chance when damaging an enemy, to restore " + (lifeStealPercent * 100.0f )+ "% of the damage as HP.");
	}
	
	public override float ActionWithFloat ( float inputValue )
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
		
		return inputValue;
	}
}
