using UnityEngine;
using System.Collections;

public class M_CriticalStrike_S : Module {
	const float cridamagePercent  = 2.9f;

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		staticPower = unit.stats.damage * cridamagePercent ;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Prime Critical Strike";
		rarity = ItemBasic.Rarity.Secret;
		rarityScaleUp = 0;
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnProjectileFire;
		procChance = 30;
		cooldown = 0f;
		itemGroup = ItemGroup.Weapon;
		itemColor = ItemColor.Pink;
		// Calculate the Power
	
		// Write the custom Description
//		descriptionString = "When attack target you have a "+procChance+" % chance to deal "+(cridamagePercent * 100.0f) +"% damage. ";
		descriptionString = ConvertString("Each shot has @PRC% chance to deal " + (cridamagePercent * 100.0f) + "% extra damage.");
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) {
			projectile.MAddDamage(staticPower);
		}
	}
}
