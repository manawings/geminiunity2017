using UnityEngine;
using System.Collections;

public class M_BlockDamage_S : Module {
	const float blockdamagePercent  = 0.50f;
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Prime Deflection";
		rarity = ItemBasic.Rarity.Secret;
		rarityScaleUp = 0;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeDamage;
		procChance = 85;
		cooldown = 0.0f;
		staticPower = blockdamagePercent;
		//staticPower = 2.0f + Stats.NGVForLevel(level) * power * 0.5f;
//		descriptionString = "When you take damage give a change "+procChance+"% to block "+( blockdamagePercent * 100 )+"% damage";
		descriptionString = ConvertString("The ship has @PRC% chance to block " + ( blockdamagePercent * 100 ) + "% damage from each hit.");
		itemGroup = ItemGroup.Shield;
		itemColor = ItemColor.Yellow;
	}
	
	public override float ActionWithFloat (float inputValue)
	{

		if (CanBeFired) {
			unit.isBlocked = true;
			unit.SetShieldColor(Unit.ShieldColor.Orange);
			inputValue *= (1.0f - staticPower);
		}
		
		return inputValue;
	}
	
}
