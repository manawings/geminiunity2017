using UnityEngine;
using System.Collections;

public class M_Regeneration_S : Module {
	public const float regenPercent = 0.07f; 
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		staticPower = regenPercent  *  unit.stats.lifePoints;
	}
	
	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Mega Regen";
		rarity = ItemBasic.Rarity.Secret;
		rarityScaleUp = 0;
		actionType = ActionType.Action;
		condition = Condition.None;
		procChance = 100;
		cooldown = 5f;
		itemGroup = ItemGroup.Repair;
		itemColor = ItemColor.Pink;

		// Calculate the Power


	
		// Write the custom Description
//		descriptionString = "Every "+(int)cooldown+" second heal "+(regenPercent * 100 )+"% life";

		descriptionString = ConvertString("Automatically restores " + (regenPercent * 100 ) + "% HP every @CD seconds.");
		
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
	}
}
