﻿using UnityEngine;
using System.Collections;

public class M_Repair_S : Module{

	// Use this for initialization
	const float regenFactor = 0.5f ;


	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		staticPower =  unit.stats.lifePoints * regenFactor ;
	}
	
	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Repair";
		rarity = ItemBasic.Rarity.Secret;
		rarityScaleUp = 0;
		actionType = ActionType.Action;
		condition = Condition.OnIsNotMoving;
		procChance = 100;
		cooldown = 5f;
		isCooldownConditional = true;
		itemGroup = ItemGroup.Repair;
		itemColor = ItemColor.Pink;
		// Calculate the Power
	//	staticPower = 5.0f + Stats.NGVForLevel(level) * power * 2.25f;
		
		// Write the custom Description
//		descriptionString = "If you stop moving every "+(int)cooldown+" second you get "+(regenFactor * 100 )+"% life";
		descriptionString = ConvertString("Repairs " + (regenFactor * 100 ) + "% HP whenever the total time spent stationary reaches @CD seconds.");
		
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
	}
}
