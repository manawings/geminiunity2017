﻿using UnityEngine;
using System.Collections;

public class Ship_Oriash_1 : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		unit.stats.armor *= 1.10f;
		unit.stats.damage *= 1.25f;
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.Ship_Oriash);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = unit.stats.damage * 5.0f;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Oriash";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 6f;
		
		// Calculate the Power
		//staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 2f;
		// Write the custom Description
		descriptionString = "\nAdd Oriash-Missle weapon (500% damage, Cooldown 6 seconds)\nAdd damage 25% for ship\nAdd Armor 10% for ship";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
