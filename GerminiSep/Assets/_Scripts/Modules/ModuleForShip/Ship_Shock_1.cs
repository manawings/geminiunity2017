using UnityEngine;
using System.Collections;

public class Ship_Shock_1 : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		unit.stats.armor *= 1.05f;
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.Ship_chain_Green);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = unit.stats.damage * 1.00f;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Storm";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 7f;
		
		// Calculate the Power
		//staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 2f;
		// Write the custom Description
		descriptionString = "\nAdd Chain to normal weapon (100% damage, Cooldown 7 seconds)\nAdd Armor 5% for ship";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
