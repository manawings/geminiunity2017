using UnityEngine;
using System.Collections;

public class Ship_Missile_Red : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);

		unit.stats.lifePointsMax *= 1.05f;
		unit.stats.lifePoints *= 1.05f;
		unit.stats.damage *= 1.05f;

		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.Ship_missile_red);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = unit.stats.damage * 0.25f;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Ember";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 5.0f;
		
		// Calculate the Power
		//staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 2f;
		// Write the custom Description
		descriptionString = "\nAdd Red-Missile weapon (25% damage, Burn 1 second, Cooldown 5 seconds)\nAdd Damage 5% for ship\nAdd HP 5% for ship";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
