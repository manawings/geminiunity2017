﻿using UnityEngine;
using System.Collections;

public class Ship_Zarall_2 : Module {
	public const float regenPercent = 0.06f; 
	public override void Deploy(Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		unit.stats.lifePointsMax *= 1.20f;
		unit.stats.lifePoints *= 1.20f;
		unit.stats.armor *= 1.20f;
		staticPower = regenPercent * unit.stats.lifePoints;
	}
	
	public override void Calibrate(float level, float power)
	{
		
		// Basic Settings ** MUST DO! **
		name = "Zarall";
		rarity = ItemBasic.Rarity.Rare;
		rarityScaleUp = 1;
		actionType = ActionType.Action;
		condition = Condition.None;
		procChance = 100;
		coolDownStartsReady = true;
		cooldown = 8f;
		itemGroup = ItemGroup.Shield;
		itemColor = ItemColor.Blue;
		// Calculate the Power
		
		// Write the custom Description
		descriptionString = "\nAdd Zarall Repair (Automatically recovers 6% HP every 8 seconds)\nAdd Hp 20% for ship\nAdd Armor 20% for ship";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
	}
}
