﻿using UnityEngine;
using System.Collections;

public class Ship_Sonar_1 : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);

		unit.stats.damage *= 1.05f;

		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.Ship_sonar_1);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = unit.stats.damage * 10.00f;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Sonar";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 8.0f;
		
		// Calculate the Power
		//staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 2f;
		// Write the custom Description
		descriptionString = "\nAdd Sonar weapon (1000% damage, Cooldown 8 seconds)\nAdd Damage 5% for ship";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
