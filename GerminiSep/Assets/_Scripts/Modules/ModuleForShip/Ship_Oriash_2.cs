﻿using UnityEngine;
using System.Collections;

public class Ship_Oriash_2 : Module {
	public const float regenPercent = 0.05f; 
	public override void Deploy(Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		unit.stats.lifePointsMax *= 1.20f;
		unit.stats.lifePoints *= 1.20f;
		unit.stats.armor *= 1.20f;
		staticPower = regenPercent * unit.stats.lifePoints;
	}
	
	public override void Calibrate(float level, float power)
	{
		
		// Basic Settings ** MUST DO! **
		name = "Oriash";
		rarity = ItemBasic.Rarity.Rare;
		rarityScaleUp = 1;
		actionType = ActionType.Action;
		condition = Condition.None;
		procChance = 100;
		coolDownStartsReady = true;
		cooldown = 7f;
		itemGroup = ItemGroup.Shield;
		itemColor = ItemColor.Blue;
		// Calculate the Power
		
		// Write the custom Description
		descriptionString = "\nAdd Oriash Repair (Automatically recovers 5% HP every 7 seconds)\nAdd Hp 20% for ship\nAdd Armor 20% for ship";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
	}
}
