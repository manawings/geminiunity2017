﻿using UnityEngine;
using System.Collections;

public class Ship_Power : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		unit.stats.damage *= 1.15f;
	}

	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
//		name = "Power";
//		rarity = ItemBasic.Rarity.Secret;
//		rarityScaleUp = 0;
//		actionType = ActionType.Action;
//		condition = Condition.OnIsShooting;
//		procChance = 100;
//		cooldown = 4f;
//		
//		// Calculate the Power
//		//staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 1f;
//		// Write the custom Description
//		descriptionString = "Every "+(int)cooldown+" seconds on fire you fire auto Ultimate-Laser for 175% of your damage.";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
		//	weapon.Fire();
		}
	}
}
