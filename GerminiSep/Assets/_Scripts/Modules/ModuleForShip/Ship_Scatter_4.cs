﻿using UnityEngine;
using System.Collections;

public class Ship_Scatter_4 : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		unit.stats.armor *= 1.15f;
		unit.stats.lifePointsMax *= 1.15f;
		unit.stats.lifePoints *= 1.15f;
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.Ship_scatter_4);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = unit.stats.damage * 1.25f;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Artemis";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 4.0f;
		
		// Calculate the Power
		//staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 2f;
		// Write the custom Description
		descriptionString = "\nAdd Double-Scatter weapon (125% damage, Cooldown 4 seconds)\nAdd Armor 15% for ship\nAdd Hp 15% for ship";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
