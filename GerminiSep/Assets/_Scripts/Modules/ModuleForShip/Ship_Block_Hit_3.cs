﻿using UnityEngine;
using System.Collections;

public class Ship_Block_Hit_3 : Module {
	
	public override void Deploy(Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		unit.stats.armor *= 1.15f;
	}
	
	const float reCooldown  = 10.0f;
	
	public override void Calibrate(float level, float power)
	{
		
		// Basic Settings ** MUST DO! **
		name = "Corsair";
		rarity = ItemBasic.Rarity.Rare;
		rarityScaleUp = 1;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeDamage;
		procChance = 100;
		coolDownStartsReady = true;
		cooldown = 0f;
		// Calculate the Power
		staticPower = 1.0f + Stats.NGVForLevel(level) * power;
		
		// Write the custom Description
		descriptionString = "\nAdd Corsair-Shield (Prevent all damage 1 hit, Cooldown 20 seconds)\nAdd Armor 15% for ship";
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		if (CanBeFired) {
			unit.isBlocked = true;
			unit.ApplyShield(2.0f, 0.1f, Unit.ShieldColor.Red);
			inputValue = 0;
			RewriteCooldown(reCooldown);
//			Debug.Log("Block");
		}
		return inputValue;
	}
}
