﻿using UnityEngine;
using System.Collections;

public class Ship_Conah_3 : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		unit.stats.armor *= 1.20f;
		unit.stats.damage *= 1.15f;
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.Ship_Oriash);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = unit.stats.damage * 4.0f;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Conah";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 5f;
		descriptionString = "\nAdd Conah-Missle weapon (400% damage, Cooldown 6 seconds)\nAdd damage 15% for ship\nAdd Armor 20% for ship";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
