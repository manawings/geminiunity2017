﻿using UnityEngine;
using System.Collections;

public class Ship_Shield_1 : Module {
	
	public override void Deploy(Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		unit.stats.armor *= 1.05f;
	}
	
	public override void Calibrate(float level, float power)
	{
		
		// Basic Settings ** MUST DO! **
		name = "Shield";
		rarity = ItemBasic.Rarity.Uncommon;
		rarityScaleUp = 1;
		actionType = ActionType.Action;
		condition = Condition.None;
		procChance = 100;
		cooldown = 10f;
		
		// Calculate the Power
		staticPower = 1.0f + Stats.NGVForLevel(level) * power;
		
		// Write the custom Description
		descriptionString = "Every 10 second gain a shield for 2 seconds.\nAdd Armor 5% for ship";
	}
	
	public override void Action()
	{
		if (CanBeFired)
		{
			//Debug.Log(Stats.NGVForLevel(level) +"\\"+ power);
			unit.ApplyShield(2.0f, 2f, Unit.ShieldColor.White);
		}
	}
}
