using UnityEngine;
using System.Collections;

public class M_AutoBlackHole : Module
{
    
    public override void Deploy(Unit unit, float level, float power = 9999.0f)
    {
        base.Deploy(unit, level, power);
        
        // Start the Weapon
        weapon = unit.gameObject.AddComponent<Weapon>();
        weapon.Deploy();
        
        // Set the Weapon
        weapon.CopyDataFrom(WeaponPattern.M_BlackHole);
        
        // Set the Damage
        weapon.damage = 0;
        weapon.staticDamage = unit.stats.damage * 5f;
    }

    public override void Calibrate(float level, float power)
    {
        
        // Basic Settings ** MUST DO! **
        name = "Void Launcher";
        rarity = ItemBasic.Rarity.Secret;
        rarityScaleUp = 0;
        actionType = ActionType.Action;
        condition = Condition.None;
        isCooldownConditional = true;
        procChance = 100;
        cooldown = 15f;
        
        // Calculate the Power
        //staticPower = 10.0f + Stats.NGVForLevel(level) * power * 10;
        // Write the custom Description
//        descriptionString = "When you attack you have " + procChance + "% chance to create a blackhole.";
		descriptionString = ConvertString("Fires a Void Bomb when attacking. Reloads in 15 seconds.");

		itemGroup = ItemGroup.Weapon;
		itemColor = ItemColor.Mix;
    }
    
    public override void Action()
    {
        if (CanBeFired)
        {
            weapon.Fire();
        }
    }
}
