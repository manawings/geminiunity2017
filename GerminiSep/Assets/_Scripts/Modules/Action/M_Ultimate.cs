using UnityEngine;
using System.Collections;

public class M_Ultimate : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		//weapon.CopyDataFrom(WeaponPattern.M_BlueMissile);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Ultimate";
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 1f;
		
		// Calculate the Power
		staticPower = (30.0f + Stats.NGVForLevel(level) * power )* 3.5f;

		// Write the custom Description
		descriptionString = "Ultimate !!!";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.CopyDataFrom(WeaponPattern.M_PurpleMissile);
			weapon.Fire();
			weapon.CopyDataFrom(WeaponPattern.M_RedMissile);
			weapon.Fire();
			weapon.CopyDataFrom(WeaponPattern.M_GreenMissile);
			weapon.Fire();
			weapon.CopyDataFrom(WeaponPattern.M_BlueMissile);
			weapon.Fire();
			weapon.CopyDataFrom(WeaponPattern.M_CounterMissile_4);
			weapon.Fire();
			weapon.CopyDataFrom(WeaponPattern.M_GreenLaserScatter);
			weapon.Fire();
			weapon.CopyDataFrom(WeaponPattern.M_GreenChain);
			weapon.Fire();
			weapon.CopyDataFrom(WeaponPattern.M_GreenScatter);
			weapon.Fire();
		}
	}
}
