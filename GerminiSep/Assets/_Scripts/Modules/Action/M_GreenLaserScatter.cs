using UnityEngine;
using System.Collections;

public class M_GreenLaserScatter : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_GreenLaserScatter);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Photon Sweep Laser";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 4f;
		
		// Calculate the Power
		staticPower = 10.0f + Stats.NGVForLevel(level) * power * 2.0f;
		// Write the custom Description
//		descriptionString = "Every 4 seconds on you fire you fire auto Green Laser Scatter for "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Automatically fires Photon sweep lasers that deal @POW damage. Reloads in @CD seconds.");
		itemGroup = ItemGroup.Laser;
		itemColor = ItemColor.Green;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
