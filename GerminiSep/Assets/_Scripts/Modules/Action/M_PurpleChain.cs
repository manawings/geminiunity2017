using UnityEngine;
using System.Collections;

public class M_PurpleChain : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_PurpleChain);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Plasma Chainer";
		rarity = ItemBasic.Rarity.Rare;
		rarityScaleUp = 1;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 3f;
		itemGroup = ItemGroup.Electro;
		itemColor = ItemColor.Pink;
		
		// Calculate the Power
		staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 3.5f;
		
		// Write the custom Description
//		descriptionString = "Every "+(int)cooldown+" seconds on you fire you fire auto Purple-Chain for "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Automatically fires Plasma Chains that deal @POW damage. Reloads in @CD seconds.");
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
