using UnityEngine;
using System.Collections;

public class M_ReactorEachKill : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Reactor Scavenger";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnEnemyKilled;
		procChance = 10;
		cooldown = 0f;
		
		// Calculate the Power
		//staticPower = 1.0f + (Stats.NGVForLevel(level) * power)/3;
		
		// Write the custom Description
//		descriptionString = "When you kill an enemy you have "+procChance+"% chance to gain a random Reactor.";
		descriptionString = ConvertString("@PRC% chance to gain a random Reactor when an enemy is killed.");

		itemGroup = ItemGroup.Reactor;
		itemColor = ItemColor.Mix;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			switch (Random.Range(1,5)) 
			{
			case 1:
				unit.GainReactor(Collectable.Type.Blue);
				break;
			case 2:
				unit.GainReactor(Collectable.Type.Yellow);
				break;			
			case 3:
				unit.GainReactor(Collectable.Type.Green);
				break;
			case 4:
				unit.GainReactor(Collectable.Type.Pink);
				break;
			}
		}
		
	}
}
