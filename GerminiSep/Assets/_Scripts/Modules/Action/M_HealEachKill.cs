using UnityEngine;
using System.Collections;

public class M_HealEachKill : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Scavenger";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnEnemyKilled;
		procChance = 50;
		cooldown = 0f;
		
		// Calculate the Power
		staticPower = 1.0f + (Stats.NGVForLevel(level) * power) * 0.125f;
		
		// Write the custom Description
//		descriptionString = "Gain "+(int)staticPower+" HP with every kill.";
		descriptionString = ConvertString("@PRC% chance to recover @POW HP whenever an enemy is killed.");
		itemGroup = ItemGroup.Misc;
		itemColor = ItemColor.Mix;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
		
	}
}
