using UnityEngine;
using System.Collections;

public class M_AutoShield : Module {

    public override void Deploy(Unit unit, float level, float power = 9999.0f)
    {
        base.Deploy(unit, level, power);
        
    }
    
    public override void Calibrate(float level, float power)
    {
        
        // Basic Settings ** MUST DO! **
        name = "Ion Shield";
        rarity = ItemBasic.Rarity.Uncommon;
        rarityScaleUp = 1;
        actionType = ActionType.Action;
        condition = Condition.None;
        procChance = 100;
        cooldown = 10f;
        
        // Calculate the Power
        staticPower = 1.0f + Stats.NGVForLevel(level) * power;
        
        // Write the custom Description
//        descriptionString = "Every 6 second gain a shield for 2.5 seconds.";
		descriptionString = ConvertString("An shield is deployed every @CD seconds. The shield lasts for 2.5 seconds.");
		itemGroup = ItemGroup.Shield;
		itemColor = ItemColor.Blue;
    }
    
    public override void Action()
    {
        if (CanBeFired)
        {
            //Debug.Log(Stats.NGVForLevel(level) +"\\"+ power);
            unit.ApplyShield(2.0f, 2.5f, Unit.ShieldColor.Blue);
        }
    }
}
