using UnityEngine;
using System.Collections;

public class M_ReactorHeal : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Reactor Repair";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnReactorUse;
		procChance = 100;
		cooldown = 0f;
		
		// Calculate the Power
		staticPower =  Stats.NGVForLevel(level) * power * 1.7f;
		
		// Write the custom Description
//		descriptionString = "When you use a Rector you get "+(int)staticPower+" HP.";
		descriptionString = ConvertString("Restores @POW HP whenever a Reactor is used.");
		itemGroup = ItemGroup.Repair;
		itemColor = ItemColor.Mix;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
	}
}
