using UnityEngine;
using System.Collections;

public class M_ReactorRecycle : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Reactor Overdrive";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnReactorUse;
		procChance = 25;
		cooldown = 0f;
		
		// Calculate the Power
		//staticPower = 1.0f + Stats.NGVForLevel(level) * power * 0.20f;
		
		// Write the custom Description
//		descriptionString = "When you use a Reactor you have "+procChance+"% to gain a Reactor.";
		descriptionString = ConvertString("When a Reactor is used, it has a @PRC% chance to be recharged with a random color.");
		itemGroup = ItemGroup.Reactor;
		itemColor = ItemColor.Mix;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			switch (Random.Range(1,5)) 
			{
			case 1:
				unit.GainReactor(Collectable.Type.Blue);
				break;
			case 2:
				unit.GainReactor(Collectable.Type.Yellow);
				break;			
			case 3:
				unit.GainReactor(Collectable.Type.Green);
				break;
			case 4:
				unit.GainReactor(Collectable.Type.Pink);
				break;
			}
		}
	}
}
