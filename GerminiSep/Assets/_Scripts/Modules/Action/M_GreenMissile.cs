using UnityEngine;
using System.Collections;

public class M_GreenMissile : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_GreenMissile);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Photon Strike";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 2f;
		itemGroup = ItemGroup.Missile;
		itemColor = ItemColor.Green;
		
		// Calculate the Power
		staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 2f;

		// Write the custom Description
//		descriptionString = "Every "+(float)cooldown+" seconds on you fire you fire auto Green-Missile for "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Automatically fires Photon Missiles that deal @POW damage. Reloads in @CD seconds.");
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
