﻿using UnityEngine;
using System.Collections;

public class M_ReactorShield : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}

	const float time = 2.0f;

	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Reactor Shield";
		rarity = ItemBasic.Rarity.Uncommon;
		rarityScaleUp = 1;
		actionType = ActionType.Action;
		condition = Condition.OnReactorUse;
		procChance = 100;
		cooldown = 0f;
		itemGroup = ItemGroup.Shield;
		itemColor = ItemColor.Pink;
		// Calculate the Power
		//staticPower = 1.0f + Stats.NGVForLevel(level) * power * 0.20f;
		
		// Write the custom Description
//		descriptionString = "When you use a Reactor you ship activate shield "+time+" seconds";
		descriptionString = ConvertString("Deploys an Ion Shield for " +time+ " seconds whenever a Reactor is used.");
		
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.ApplyShield(2.0f, time, Unit.ShieldColor.Pink);
		}
	}
}
