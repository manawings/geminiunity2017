using UnityEngine;
using System.Collections;

public class M_BlueScatter : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_BlueScatter);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "EMP Auto Scatter";
		rarity = ItemBasic.Rarity.Uncommon;
		rarityScaleUp = 1;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 3f;
		itemGroup = ItemGroup.Scatter;
		itemColor = ItemColor.Blue;
		
		// Calculate the Power
		staticPower = 10.0f + Stats.NGVForLevel(level) * power * 1;

		// Write the custom Description
//		descriptionString = "Every "+(int)cooldown+" seconds on you fire you fire auto Blue-Scatter for "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Automatically fires EMP shots that stun and deal @POW damage. Reloads in @CD seconds.");
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
