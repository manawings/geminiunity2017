using UnityEngine;
using System.Collections;

public class M_GreenScatter : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_GreenScatter);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Photon Auto Scatter";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 1f;
		itemGroup = ItemGroup.Scatter;
		itemColor = ItemColor.Green;
		
		// Calculate the Power
		staticPower = (10.0f + Stats.NGVForLevel (level) * power) * 0.75f;
		
		// Write the custom Description
//		descriptionString = "Every "+(int)cooldown+" seconds on you fire you fire auto Green-Scatter for "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Automatically fires Photon shots that deal @POW damage. Reloads in @CD seconds.");
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
