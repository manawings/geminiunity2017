using UnityEngine;
using System.Collections;

public class M_ReactorCharge : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Reactor Fusion";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsNotMoving;
		procChance = 100;
		cooldown = 10f;
		isCooldownConditional = true;
		//isCooldownConditionalReset = true;
		
		// Calculate the Power
		//staticPower = 3.0f + Stats.NGVForLevel(level) * power * 4;
		
		// Write the custom Description
//		descriptionString = "If you stop moving, every 10 second you gain a Reactor.";
		descriptionString = ConvertString("Gain a random Reactor whenever the total time spent stationary reaches @CD seconds.");
		itemGroup = ItemGroup.Reactor;
		itemColor = ItemColor.Mix;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			switch (Random.Range(1,5)) 
			{
			case 1:
				unit.GainReactor(Collectable.Type.Blue);
				break;
			case 2:
				unit.GainReactor(Collectable.Type.Yellow);
				break;			
			case 3:
				unit.GainReactor(Collectable.Type.Green);
				break;
			case 4:
				unit.GainReactor(Collectable.Type.Pink);
				break;
			}
		}
	}

}
