using UnityEngine;
using System.Collections;

public class M_BlueLaser : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_BlueLaser);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "EMP Auto Laser";
		rarity = ItemBasic.Rarity.Uncommon;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 4f;
		itemGroup = ItemGroup.Laser;
		itemColor = ItemColor.Blue;
		// Calculate the Power
		staticPower = (10.0f + Stats.NGVForLevel(level) *  power ) * 2f;
		// Write the custom Description
//		descriptionString = "Every 4 seconds on you fire you fire auto Blue-Laser for "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Automatically fires EMP lasers that stun and deal @POW damage. Reloads in @CD seconds.");
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
