﻿using UnityEngine;
using System.Collections;

public class M_UltimateLaser : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_UltimateLaser);
	
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = unit.stats.damage * 2f;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Omega Laser";
		rarity = ItemBasic.Rarity.Secret;
		rarityScaleUp = 0;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 4f;
		itemGroup = ItemGroup.Laser;
		itemColor = ItemColor.Pink;
		// Calculate the Power
		//staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 1f;
		// Write the custom Description
//		descriptionString = "Every "+(int)cooldown+" seconds on fire you fire auto Ultimate-Laser for 175% of your damage.";
		descriptionString = ConvertString("Automatically fires lasers that deal 200% of the ship's damage. Reloads in @CD seconds.");
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
