using UnityEngine;
using System.Collections;

public class M_CollectGemGetHp : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Gem Converter";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnCollectGem;
		procChance = 100;
		cooldown = 0f;
		
		// Calculate the Power
		staticPower = 5.0f + Stats.NGVForLevel(level) * power * 0.25f;
		
		// Write the custom Description
//		descriptionString = "When you collect a Gem you recover "+(int)staticPower+" HP.";
		descriptionString = ConvertString("Restores @POW HP whenever a Gem is collected.");
		itemGroup = ItemGroup.Repair;
		itemColor = ItemColor.Mix;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
	}
}
