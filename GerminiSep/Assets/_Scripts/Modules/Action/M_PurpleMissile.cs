using UnityEngine;
using System.Collections;

public class M_PurpleMissile : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_PurpleMissile);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Plasma Strike";
		rarity = ItemBasic.Rarity.Rare;
		rarityScaleUp = 1;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 2f;
		itemGroup = ItemGroup.Missile;
		itemColor = ItemColor.Pink;
		
		// Calculate the Power
		staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 3.5f;

		// Write the custom Description
//		descriptionString = "Every "+(float)cooldown+" seconds on you fire you fire auto Purple-Missile for "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Automatically fires Plasma Missiles that deal @POW damage. Reloads in @CD seconds.");
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
