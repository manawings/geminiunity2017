using UnityEngine;
using System.Collections;

public class M_Regeneration : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Regeneration";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.None;
		procChance = 100;
		cooldown = 5f;
		
		// Calculate the Power
		staticPower = 2.0f + Stats.NGVForLevel(level) * power * 0.25f;
		// Write the custom Description
//		descriptionString = "Every "+(int)cooldown+" second heal "+(int)staticPower+" Hp.";
		descriptionString = ConvertString("Automatically recovers @POW HP every @CD seconds.");
		itemGroup = ItemGroup.Repair;
		itemColor = ItemColor.Green;

	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
	}
}
