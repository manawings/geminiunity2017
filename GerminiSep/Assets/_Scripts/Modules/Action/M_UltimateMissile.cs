using UnityEngine;
using System.Collections;

public class M_UltimateMissile : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_UltimateMissile);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = unit.stats.damage * 1.75f;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Wrath Missiles";
		rarity = ItemBasic.Rarity.Secret;
		rarityScaleUp = 0;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 4f;
		
		// Calculate the Power
		//staticPower = unit.stats.damage;
		// Write the custom Description
//		descriptionString = "Every "+(int)cooldown+" seconds on you fire you fire auto Ultimate-Missile 175% for your damage.";
		descriptionString = ConvertString("Automatically fires rockets that each deal 175% of the ship's damage. Reloads in @CD seconds.");

		itemGroup = ItemGroup.Weapon;
		itemColor = ItemColor.Mix;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
