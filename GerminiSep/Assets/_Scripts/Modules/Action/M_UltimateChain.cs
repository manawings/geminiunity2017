using UnityEngine;
using System.Collections;

public class M_UltimateChain : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_UltimateChain);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = unit.stats.damage * 1.75f;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Reaper's Chain";
		rarity = ItemBasic.Rarity.Secret;
		rarityScaleUp = 0;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 6f;
		
		// Calculate the Power
		//staticPower = 10.0f + Stats.NGVForLevel(level) * power * 5;
		// Write the custom Description
//		descriptionString = "Every "+(int)cooldown+" seconds on you fire you fire auto Ultimate-Chain 175% your damage.";
		descriptionString = ConvertString("Automatically fires electro chains that deals 175% of the ship's damage. Reloads in @CD seconds.");
		itemGroup = ItemGroup.Electro;
		itemColor = ItemColor.Pink;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			weapon.Fire();
		}
	}
}
