using UnityEngine;
using System.Collections;

public class M_Repair : Module {
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Repair";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.Action;
		condition = Condition.OnIsNotMoving;
		procChance = 100;
		cooldown = 5f;
		isCooldownConditional = true;
		
		// Calculate the Power
		staticPower = 5.0f + Stats.NGVForLevel(level) * power * 1.35f;
		
		// Write the custom Description
//		descriptionString = "If you stop moving every "+(int)cooldown+" second you get "+(int)staticPower+" life";
		descriptionString = ConvertString("Repairs @POW HP whenever the total time spent stationary reaches @CD seconds.");
		itemGroup = ItemGroup.Repair;
		itemColor = ItemColor.Green;
		
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
	}
}
