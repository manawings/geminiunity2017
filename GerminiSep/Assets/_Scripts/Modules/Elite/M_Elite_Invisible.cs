﻿using UnityEngine;
using System.Collections;

public class M_Elite_Invisible : Module {

	public override void Deploy(Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy(unit);
	}
	
	public override void Calibrate(float level, float power)
	{
		
		// Basic Settings ** MUST DO! **
		name = "Invisible";
		rarity = ItemBasic.Rarity.Rare;
		rarityScaleUp = 1;
		actionType = ActionType.Action;
		condition = Condition.None;
		procChance = 100;
		cooldown = 0.5f;
		
		// Calculate the Power
		staticPower = 1.0f + Stats.NGVForLevel(level) * power;
		
		// Write the custom Description
		descriptionString = "When your take damage and hp lowest 15% shield will activate 5 seconds.\nCooldown shield 30 seconds";
	}
	
	public override void Action ()
	{
		if (CanBeFired)
		{
			if (unit.stats.lifePoints <= unit.stats.MaxLifePoints * 0.50f && unit.stats.lifePoints >= 1){
				//Debug.Log("unit.stats.lifePoints =" + unit.stats.lifePoints + " ## unit.stats.MaxLifePoints = " + unit.stats.MaxLifePoints);
				unit.SetInvisible();
				unit.SetShieldColor(Unit.ShieldColor.White);
				unit.isBlocked = true;
				if(isActionOnce) {
					unit.ApplyShield(2.0f, 6.0f, Unit.ShieldColor.White);
//					weapon.CopyDataFrom(WeaponPattern.B_ShockWave);
//					weapon.Fire();
					isActionOnce = false;
				}
			}
		}
	}
}
