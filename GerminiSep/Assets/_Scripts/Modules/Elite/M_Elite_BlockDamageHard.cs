﻿using UnityEngine;
using System.Collections;

public class M_Elite_BlockDamageHard : Module {

	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Block Damage";
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeDamage;
		procChance = 40;
		cooldown = 0.0f;
		//staticPower = 1.0f + Stats.NGVForLevel(level) * power * 0.65f;
		descriptionString = "When you take damage give a change "+procChance+"% to block "+(int)staticPower+" damage";
		
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		
		if (CanBeFired) {
			unit.isBlocked = true;
			unit.SetShieldColor(Unit.ShieldColor.Pink);
			inputValue *= 0.20f;
		}
		
		return inputValue;
	}
}
