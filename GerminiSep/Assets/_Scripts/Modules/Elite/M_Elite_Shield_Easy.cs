﻿using UnityEngine;
using System.Collections;

public class M_Elite_Shield_Easy : Module {
	
	public override void Deploy(Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		
	}
	
	public override void Calibrate(float level, float power)
	{
		
		// Basic Settings ** MUST DO! **
		name = "Shield of Absorption";
		rarity = ItemBasic.Rarity.Rare;
		rarityScaleUp = 1;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeDamage;
		procChance = 100;
		coolDownStartsReady = true;
		cooldown = 0f;
		
		// Calculate the Power
		staticPower = 1.0f + Stats.NGVForLevel(level) * power;
		
		// Write the custom Description
		descriptionString = "When your take damage and hp lowest 15% shield will activate 5 seconds.\nCooldown shield 30 seconds";
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		//Debug.Log(unit.stats.LifePercent);
		if (CanBeFired)
		{
			if (inputValue > unit.stats.lifePoints) {
				
				unit.stats.lifePoints = 1;
				unit.ApplyShield(2.0f, 3.0f, Unit.ShieldColor.Green);
				RewriteCooldown(30.0f);
				
			}else if (unit.stats.lifePoints - inputValue <= unit.stats.MaxLifePoints * 0.10f){
				
				unit.stats.lifePoints = unit.stats.lifePoints - inputValue;
				unit.ApplyShield(2.0f, 3.0f, Unit.ShieldColor.Green);
				RewriteCooldown(30.0f);
				
			}
		}
		return inputValue;
	}
}
