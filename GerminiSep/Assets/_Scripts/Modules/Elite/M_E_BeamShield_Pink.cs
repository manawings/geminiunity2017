﻿using UnityEngine;
using System.Collections;

public class M_E_BeamShield_Pink : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {

		name = "Block Damage";
		actionType = ActionType.ActionWithSwitch;
		condition = Condition.OnStartAndStopShoot;
		procChance = 100;
		cooldown = 0.0f;
		//staticPower = 1.0f + Stats.NGVForLevel(level) * power * 0.65f;
		descriptionString = "When you take damage give a change "+procChance+"% to block "+(int)staticPower+" damage";
		
	}
	
	public override void ActionWithSwitch (bool isSwitchedOn)
	{
		if (CanBeFired) {

			if (isSwitchedOn)
			{
				unit.ApplyShield(2.0f, 2.0f, Unit.ShieldColor.Pink);
			} else {
				// Turn off SHIELDS	
			}
		}

	}
}
