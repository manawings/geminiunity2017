using UnityEngine;
using System.Collections;

public class M_Reflector : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy(unit);
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_UltimateMissile);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		
		// Basic Settings ** MUST DO! **
		name = "Mine Reactor";
		actionType = ActionType.Action;
		condition = Condition.OnIsNotMoving;
		procChance = 100;
		cooldown = 0.5f;

		rarity = ItemBasic.Rarity.Common;

		
		// Calculate the Power
		staticPower = 5000.0f + Stats.NGVForLevel(level) * power * 3.5f;
		
		// Write the custom Description
		descriptionString = "This reactor is broken!";
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			
			// unit.Heal(8.0f);
		    weapon.Fire();
			// unit.ApplyShield(10.0f, 5.0f);
			//unit.GainReactor(Collectable.Type.Blue);
			
		}
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		if (CanBeFired) {
			inputValue -= 50.0f;
		}
		
		return inputValue;
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) {
			projectile.MAddSpeed(400.0f);
			projectile.MAddDamage(2f);
			projectile.MAddBuff(Buff.Type.Shield);
			unit.GainReactor(Collectable.Type.Yellow);
			unit.Heal(1f);
			//projectile.MAddBuff(Buff.Type.Stun);
		}

		staticPower = unit.stats.damage * 0.25f;
	}
	
	public override void ActionWithSwitch ( bool isSwitchedOn )
	{
		if (isSwitchedOn)
		{
			if (CanBeFired) {
				weapon.Fire();
				// unit.sprite.color = Color.green;
			}
		} else {
			if (CanBeFired) {
				weapon.Fire();
				// unit.sprite.color = Color.red;
			}
		}
	}	
}
