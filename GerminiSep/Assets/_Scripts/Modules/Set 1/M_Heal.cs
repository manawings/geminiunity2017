using UnityEngine;
using System.Collections;

public class M_Heal : Module
{
	
	public override void Deploy (Unit unit, float level, float power)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		condition = Condition.OnHitByProjectile;
		procChance = 50;
		cooldown = 0.0f;
		staticPower = 3.0f + power * 2.0f;
		descriptionString = procChance + "% chance to instantly repair " + ((int) staticPower).ToString() + " HP when damaged.";
	
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
//		Debug.Log("HEAL: " + staticPower);
		unit.stats.lifePoints += staticPower;
	}
}

