using UnityEngine;
using System.Collections;

public class M_Burn : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Phoenix Rounds";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnProjectileFire;
		procChance = 35;
		cooldown = 0f;
		
		// Calculate the Power
		staticPower = 1.0f + Stats.NGVForLevel(level) * power;
		
		// Write the custom Description
//		descriptionString = "When attacking a target you have a "+procChance+"% chance to burn damage "+(int)staticPower+" per seconds for 5 seconds . ";
		descriptionString = ConvertString("Each shot has @PRC% chance to burn the target for " + (int)staticPower * 5 + " extra damage over 5 seconds.");
		itemGroup = ItemGroup.Weapon;
		itemColor = ItemColor.Red;
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) {
			projectile.MAddBuff(Buff.Type.Burn, staticPower, 5.0f, false);
		}
	}
}
