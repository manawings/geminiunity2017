using UnityEngine;
using System.Collections;

public class M_CriticalStrike : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Critical Strike";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnProjectileFire;
		procChance = 15;
		cooldown = 0f;
		
		// Calculate the Power
		staticPower = 20.0f + Stats.NGVForLevel(level) * power * 0.9f;
		// Write the custom Description
//		descriptionString = "When attack target you have a "+procChance+" % chance to deal "+(int)staticPower+" extra damage. ";
		descriptionString = ConvertString("Each shot has @PRC% chance to deal @POW extra damage.");
		itemGroup = ItemGroup.Weapon;
		itemColor = ItemColor.Pink;
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) {
			projectile.MAddDamage(staticPower);
		}
	}
}
