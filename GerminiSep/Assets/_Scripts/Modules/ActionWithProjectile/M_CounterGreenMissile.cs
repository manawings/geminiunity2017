using UnityEngine;
using System.Collections;

public class M_CounterGreenMissile : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_GreenMissile);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Hunter Retaliation";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnHitByProjectile;
		procChance = 80;
		cooldown = 0.0f;
		staticPower = (20.0f + Stats.NGVForLevel(level) * ( power )) * 2f ;

		// Whenever the ship takes damage, it has X% chance to counter attack with Hunter Missiles for X damage.
//		descriptionString = "When you take damage, you have a " + procChance + "% chance to Green-Missile for " + ((int)staticPower).ToString() + " damage.";
		descriptionString = ConvertString("When hit, the ship has @PRC% chance to counter-attack with Hunter Missiles for @POW damage.");
		itemGroup = ItemGroup.Missile;
		itemColor = ItemColor.Green;

	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) weapon.Fire();
	}
	
}
