using UnityEngine;
using System.Collections;

public class M_CounterPurpleChain : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_PurpleChain);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Plasma Counter Chain";
		rarity = ItemBasic.Rarity.Rare;
		rarityScaleUp = 1;
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnHitByProjectile;
		procChance = 60;
		cooldown = 0.0f;
		staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 3.5f;
//		descriptionString = "When you take damage, you have a " + procChance + "% chance to Purple-Chain for " + ((int)staticPower).ToString() + " damage.";
		descriptionString = ConvertString("When hit, the ship has @PRC% chance to counter-attack with Plasma Chains for @POW damage.");
		itemGroup = ItemGroup.Electro;
		itemColor = ItemColor.Pink;
		
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) weapon.Fire();
	}
	
}
