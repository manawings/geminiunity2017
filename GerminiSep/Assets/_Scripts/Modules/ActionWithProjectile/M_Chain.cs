using UnityEngine;
using System.Collections;

public class M_Chain : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_GreenChain);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnHitByProjectile;
		procChance = 100;
		cooldown = 0.0f;
		staticPower = 5.0f + Stats.NGVForLevel(level) * power * 3.5f;
		descriptionString = "When damaged, you have a " + procChance + "% chance to chain-attack for " + ((int)staticPower).ToString() + " damage.";

	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) weapon.Fire();
	}
	
}
