using UnityEngine;
using System.Collections;

public class M_CounterGreenChain : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_GreenChain);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Photon Counter Chain";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnHitByProjectile;
		procChance = 60;
		cooldown = 0.0f;
		staticPower = (20.0f + Stats.NGVForLevel(level) *  power ) * 2f;
//		descriptionString = "When you take damage, you have a " + procChance + "% chance to Green-Chain for " + ((int)staticPower).ToString() + " damage.";
		descriptionString = ConvertString("When hit, the ship has @PRC% chance to counter-attack with Photon Chains for @POW damage.");
		itemGroup = ItemGroup.Electro;
		itemColor = ItemColor.Green;
		
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) weapon.Fire();
	}
	
}
