using UnityEngine;
using System.Collections;

public class M_CounterBlueChain : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_BlueChain);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "EMP Counter Chain";
		rarity = ItemBasic.Rarity.Uncommon;
		rarityScaleUp = 1;
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnHitByProjectile;
		procChance = 60;
		cooldown = 0.0f;
		staticPower = (10.0f + Stats.NGVForLevel(level) *  power ) * 1f;
//		descriptionString = "When you take damage, you have a " + procChance + "% chance to Blue-Chain for " + ((int)staticPower).ToString() + " damage.";
		descriptionString = ConvertString("When hit, the ship has @PRC% chance to counter-attack with EMP Chains for @POW damage.");
		itemGroup = ItemGroup.Electro;
		itemColor = ItemColor.Blue;
		
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) weapon.Fire();
	}
	
}
