﻿using UnityEngine;
using System.Collections;

public class M_SonarGreen : Module {
	bool rapidSwitch = true;
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_SonarGreen);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Photon Sonar";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 1;
		actionType = ActionType.Action;
		condition = Condition.OnIsShooting;
		procChance = 100;
		cooldown = 2.0f;
		
		// Calculate the Power
		staticPower = (10.0f + Stats.NGVForLevel (level) * power) * 3.0f;
		// Write the custom Description
		descriptionString = ConvertString("Automatically fires Sonar shots that deal @POW damage. Reloads in @CD seconds.");
		itemGroup = ItemGroup.Weapon;
		itemColor = ItemColor.Green;
	}
	
	public override void Action ()
	{
		if (CanBeFired) {
			if (rapidSwitch) {
				weapon.SetRapidVars(4, 0.1f, 0.0f, 0.9f);
				rapidSwitch = false;
			} else {
				weapon.SetRapidVars(4, 0.1f, 0.0f, -0.9f);
				rapidSwitch = true;
			}

			weapon.Fire();
		}
	}
}
