using UnityEngine;
using System.Collections;

public class M_CounterRedMissile : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
		// Start the Weapon
		weapon = unit.gameObject.AddComponent<Weapon>();
		weapon.Deploy();
		
		// Set the Weapon
		weapon.CopyDataFrom(WeaponPattern.M_RedMissile);
		
		// Set the Damage
		weapon.damage = 0;
		weapon.staticDamage = staticPower;
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Flame Retaliation";
		rarity = ItemBasic.Rarity.Uncommon;
		rarityScaleUp = 1;
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnHitByProjectile;
		procChance = 80;
		cooldown = 0.0f;
		staticPower = (5.0f + Stats.NGVForLevel(level) * power )* 1f;
		descriptionString = ConvertString("When hit, the ship has @PRC% chance to counter-attack with Phoenix Missiles for @POW damage.");
		itemGroup = ItemGroup.Missile;
		itemColor = ItemColor.Red;
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) weapon.Fire();
	}
	
}
