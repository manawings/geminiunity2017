using UnityEngine;
using System.Collections;

public class M_Stun : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "EMP Rounds";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithProjectile;
		condition = Condition.OnProjectileFire;
		procChance = 6;
		cooldown = 0f;
		
		// Calculate the Power
		//staticPower = 1.0f + Stats.NGVForLevel(level) * power;
		
		// Write the custom Description
//		descriptionString = "When attack target you have a "+procChance+"% chance to stun for 2 seconds. ";
		descriptionString = ConvertString("Each shot has @PRC% chance to stun the target for 2 seconds.");
		itemGroup = ItemGroup.Weapon;
		itemColor = ItemColor.Blue;
	}
	
	public override void ActionWithProjectile ( Projectile projectile )
	{
		if (CanBeFired) {
			projectile.MAddBuff(Buff.Type.Stun);
		}
	}
}
