using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ModuleManager
{
	private bool isMoving = false;
	private bool isShooting = false;
	
	// Module Collections
	
	private List<Module> allModules;
	
	// Event Delegates
	private event Module.ActionWithSwitchDelegate OnStartAndStopMoveEvent;
	private event Module.ActionWithSwitchDelegate OnStartAndStopShootEvent;
	private event Module.ActionWithProjectileDelegate OnProjectileHitEvent;
	private event Module.ActionWithProjectileDelegate OnProjectileFireEvent;
	
	private event Module.ActionDelegate OnCollectGemEvent;
	private event Module.ActionDelegate OnCollectPowerUpEvent;
	private event Module.ActionDelegate OnReactorUseEvent;
	private event Module.ActionDelegate OnEnemyKilledEvent;
	
	public void Run (float deltaTime)
	{
		foreach (Module module in allModules)
		{

			if (!module.isCooldownConditional) module.RunCooldown(deltaTime);
			
			if (module.actionType == Module.ActionType.Action)
			{
				if (module.condition == Module.Condition.None ||
					(module.condition == Module.Condition.OnIsShooting && isShooting) ||
					(module.condition == Module.Condition.OnIsNotShooting && !isShooting) ||
					(module.condition == Module.Condition.OnIsMoving && isMoving) ||
					(module.condition == Module.Condition.OnIsNotMoving && !isMoving)
					)

				{
					module.Action();
					if (module.isCooldownConditional) module.RunCooldown(deltaTime);

				} else if (module.isCooldownConditionalReset) { //nun

					module.ResetCooldown();

				}
			}
		}
	}
	
	public void OnHitFromProjectile (Projectile projectile)
	{
		if (OnProjectileHitEvent != null)
			OnProjectileHitEvent(projectile);
	}
	
	public void OnFireProjectile (Projectile projectile)
	{
		if (OnProjectileFireEvent != null)
			OnProjectileFireEvent(projectile);
	}
	
	public float OnTakeDamage (float damage, Unit.DamageType damageType = Unit.DamageType.Normal)
	{
		foreach (Module module in allModules)
		{
			if (module.condition == Module.Condition.OnTakeDamage ||
			    ( module.condition == Module.Condition.OnTakeNormalDamage && damageType == Entity.DamageType.Normal ) ||
			    ( module.condition == Module.Condition.OnTakeMissileDamage && damageType == Entity.DamageType.Missile ) ||
			    ( module.condition == Module.Condition.OnTakeMineDamage && damageType == Entity.DamageType.Mine ) ||
			    ( module.condition == Module.Condition.OnTakeBeamDamage && damageType == Entity.DamageType.Beam ) ||
			    ( module.condition == Module.Condition.OnTakeBurnDamage && damageType == Entity.DamageType.Burn )
			    ) 
			{
				damage = module.ActionWithFloat(damage);	
			}
		}
		
		return damage;
	}
	
	public float OnDealDamage (float damage)
	{
		foreach (Module module in allModules)
		{
			if (module.condition == Module.Condition.OnDealDamage) damage = module.ActionWithFloat(damage);	
		}
		
		return damage;
	}
	
	public void OnStartMove ()
	{
		if (!isMoving)
		{
			isMoving = true;
			if (OnStartAndStopMoveEvent != null) OnStartAndStopMoveEvent(true);
			
		}
	}
	
	public void OnStopMove()
	{
		if (isMoving)
		{
			isMoving = false;
			if (OnStartAndStopMoveEvent != null) OnStartAndStopMoveEvent(false);
			
		}
	}
	
	public void OnStartShoot ()
	{
		if (!isShooting)
		{
			isShooting = true;
			if (OnStartAndStopShootEvent != null) OnStartAndStopShootEvent(true);
		}
	}
	
	public void OnStopShoot ()
	{
		if (isShooting)
		{
			isShooting = false;
			if (OnStartAndStopShootEvent != null) OnStartAndStopShootEvent(false);
			
		}
	}
	
	public void OnCollectGem ()
	{
		if (OnCollectGemEvent != null) OnCollectGemEvent();
	}
	
	public void OnCollectPowerUp ()
	{
		if (OnCollectPowerUpEvent != null) OnCollectPowerUpEvent();
	}
	
	public void OnReactorUse ()
	{
		if (OnReactorUseEvent != null) OnReactorUseEvent();
	}
	
	public void OnEnemyKilled ()
	{
		if (OnEnemyKilledEvent != null) OnEnemyKilledEvent();
	}
	
	public void AddModule (Module module)
	{
		allModules.Add (module);
		AddModuleDelegate(module);
	}
	
	void AddModuleDelegate (Module module)
	{
		switch (module.actionType)
		{
			
		case Module.ActionType.Action:
			
			if (module.condition == Module.Condition.OnCollectGem)
			{
				OnCollectGemEvent += module.Action;
			} else if (module.condition == Module.Condition.OnCollectPowerUp) {
				OnCollectPowerUpEvent += module.Action;
			} else if (module.condition == Module.Condition.OnReactorUse) {
				OnReactorUseEvent += module.Action;
			} else if (module.condition == Module.Condition.OnEnemyKilled) {
				OnEnemyKilledEvent += module.Action;
			}
			
			break;
			
		case Module.ActionType.ActionWithProjectile:
			
			if (module.condition == Module.Condition.OnHitByProjectile) {
				OnProjectileHitEvent += module.ActionWithProjectile;
			} else if (module.condition == Module.Condition.OnProjectileFire) {
				OnProjectileFireEvent += module.ActionWithProjectile;
			}
			
			break;
			
		case Module.ActionType.ActionWithSwitch:
			if (module.condition == Module.Condition.OnStartAndStopMove)
			{	
				OnStartAndStopMoveEvent += module.ActionWithSwitch;
			} else if (module.condition == Module.Condition.OnStartAndStopShoot) {
				OnStartAndStopShootEvent += module.ActionWithSwitch;
			}
			
			break;
			
		}
	}
	
	public void Clear ()
	{
		if (allModules != null) {
			foreach (Module module in allModules)
			{
				module.Terminate();
			}
			allModules.Clear();
		} else {
			allModules = new List<Module>();
		}

		isMoving = false;
		isShooting = false;

		OnStartAndStopMoveEvent = null;
		OnStartAndStopShootEvent = null;
		OnProjectileHitEvent = null;
		
		OnCollectGemEvent = null;
		OnCollectPowerUpEvent = null;
		OnReactorUseEvent = null;
		OnEnemyKilledEvent = null;
		

	}
}

