using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class Module
{
	
	// TEST TEMP
	
	public const int moduleIdToUse = 0;
	public const float modulePowerToUse = 0.50f;
	
	// Reference
	
	protected Unit unit;
	protected Weapon weapon;

	//Item Group
	public enum ItemGroup {
		Armor,
		Weapon,
		Defensive,
		Missile,
		Scatter,
		Laser,
		Beam,
		Electro,
		Repair,
		Shield,
		Reactor,
		Misc
	}

	public enum ItemColor {
		Red, 
		Blue, 
		Green, 
		Pink, 
		Yellow,
		Mix
	}

	public ItemGroup itemGroup = ItemGroup.Misc;
	public ItemColor itemColor = ItemColor.Mix;

	// Condition
	
	public Condition condition;
	public float conditionValue = 0.0f;
	private float currentCooldown = 0.0f;
	public bool isCooldownConditional = false; // If true, the cooldown will only tick if the condition is true.
	public bool isCooldownConditionalReset = false; // If true, will reset cooldown when condition is not met.
	public bool coolDownStartsReady = false; // If true, it will set cooldown to 0.
	public bool rewriteCooldown = false;

	public bool isActionOnce = true;

	protected float cooldown = 0.0f; // Time in seconds to activate the Module.
	protected int procChance = 100; // 0% - 100% Chance on condition to activate the effect.

	// Item Availability

	public int minLevel = 0;
	public int maxLevel = int.MaxValue;
	public ItemBasic.Rarity rarity = ItemBasic.Rarity.Any;
	public int rarityScaleUp = int.MaxValue;
	
	// Effect
	
	public ActionType actionType = ActionType.None; // This needs to be set, so the module manager knows to add to delegate.
	protected float level; // Level of the item.
	protected float power; // % Budget of the module.
	protected float staticPower; // Calculated value of the module.
	
	// Fixed Description
	
	public string name { get; protected set; }
	protected string descriptionString = string.Empty;
	
	// Conditions Enum
	
	public enum Condition
	{
		None,
		
		// Taking Damage
		
		OnHitByProjectile, // Action w Projectile
		OnTakeDamage, // Action w Float
		OnProjectileFire, // Action w Projectile

		OnTakeNormalDamage, // Action w Float
		OnTakeBeamDamage, // Action w Float
		OnTakeMissileDamage, // Action w Float
		OnTakeMineDamage, // Action w Float
		OnTakeBurnDamage, // Action w Float
		
		// Movement
		
		OnStartAndStopMove, // Action w Switch
		OnIsMoving, // Action
		OnIsNotMoving, // Action
		
		// Shooting
		
		OnStartAndStopShoot, // Action w Switch
		OnIsShooting, // Action
		OnIsNotShooting, // Action
		
		// Resources
		
		OnCollectPowerUp, // Action
		OnCollectGem, // Action
		
		// Offensive

		// OnEnemyNear, NOT BEING USED
		OnEnemyKilled, // Action
		OnDealDamage, // Action w Float
		OnReactorUse // Action
		
		// Other Ideas...
		// OnReactorHave
		// OnNoReactorHave
		
	}
	
	// Action Type Enum
	
	public enum ActionType {
		None,
		Action,
		ActionWithFloat,
		ActionWithUnit,
		ActionWithProjectile,
		ActionWithSwitch
	}

	// Module Delegates
	
	public delegate void ActionDelegate ();
	public delegate float ActionWithFloatDelegate ( float inputValue );
	public delegate void ActionWithUnitDelegate ( Unit unit );
	public delegate void ActionWithProjectileDelegate ( Projectile projectile );
	public delegate void ActionWithSwitchDelegate ( bool isActive );
	
	public virtual void Action ()
	{

	}
	
	public virtual float ActionWithFloat ( float inputValue )
	{
		return inputValue;
	}
	
	public virtual void ActionWithUnit ( Unit unit )
	{
		
	}
	
	public virtual void ActionWithProjectile ( Projectile projectile )
	{
		
	}

	protected string ConvertString (string original) {
		original = original.Replace("@PRC", procChance.ToString());
//		Regex rgx = new Regex(@"\d{2}$");
//		if (rgx.IsMatch(cooldown.ToString())) {
//
//		} else {
//
//		}
		original = original.Replace("@CD", cooldown.ToString("F0"));

		original = original.Replace("@POW", ((int)staticPower).ToString());

		return original;
	}
	
	public virtual void ActionWithSwitch ( bool isSwitchedOn )
	{
		if (isSwitchedOn)
		{
			// Turn on SHIELDS	
		} else {
			// Turn off SHIELDS	
		}
	}
	
	virtual public void Calibrate (float level, float power)
	{
		this.power = power;
		this.level = level;

	}
	
	virtual public void Deploy (Unit unit, float level, float power)
	{
		this.unit = unit;
		Calibrate(level, power);
		PostCalibrate();
	}

	void PostCalibrate ()
	{
		if (coolDownStartsReady) {
			currentCooldown = 0;
		} else {
			currentCooldown = cooldown;
		}

	}
	
	virtual public string Description
	{
		get 
		{
			return descriptionString;
		}
	}

	public void BoltWeaponToModule ()
	{
		if (weapon != null) weapon.isModule = true;
	}

	public void Terminate ()
	{
		if (weapon != null)
		{
			weapon.Terminate();
			Object.Destroy(weapon);
		}
	}
	
	public void RunCooldown (float deltaTime)
	{
		//Debug.Log (currentCooldown); //nun
		if (currentCooldown > 0) {
			currentCooldown -= deltaTime;
			if (currentCooldown < 0) currentCooldown = 0;
		}
	}

	public void ResetCooldown ()
	{
		currentCooldown = cooldown;
	}

	public void RewriteCooldown (float reCooldown) {
		currentCooldown = reCooldown;
	}

	protected bool CanBeFired
	{
		get {
			if (currentCooldown == 0 )
			{
				if (procChance == 100 || KJDice.Roll(procChance)) {
					currentCooldown = cooldown;
					if (weapon != null) {
						weapon.direction = unit.WeaponDirection;
					}
					return true;	
				}
			}
			
			return false;
		}
	}

	public static bool ModulesAreAvailableFor (int level = 0, ItemBasic.Rarity rarity = ItemBasic.Rarity.Common)
	{
		if (GetEligableModuleListFor(level, rarity).Count == 0) {
			return false;
		} else {
			return true;
		}
	}

	private static List<int> GetEligableModuleListFor (int level = 0, ItemBasic.Rarity rarity = ItemBasic.Rarity.Common)
	{
		List<int> moduleList = new List<int>();

		for (int i = 2 ; i <= ModuleLibrary.numberOfModules ; i ++ )//nun
		{
			Module module = ModuleLibrary.GetFromId(i);
			module.Calibrate(level, 1.0f);
//			Debug.Log ("Module Name: " + module.name + " // Module Rarity: " + module.rarity + " // Module ScaleUp: " + module.rarityScaleUp + " // Item R: " +rarity + " // Is Compatiable.. " + isRarityCompatible(module.rarity, rarity, module.rarityScaleUp));

			if (level >= module.minLevel && level <= module.maxLevel && isRarityCompatible(module.rarity, rarity, module.rarityScaleUp))
			{
				moduleList.Add (i);
			}
		}

		return moduleList;
	}

	public static int GetRandomModuleIdFor (int level = 0, ItemBasic.Rarity rarity = ItemBasic.Rarity.Common)
	{
		List<int> moduleList = GetEligableModuleListFor(level, rarity);

		if (moduleList.Count == 0)
		{
			return 0;
		} else {
			return KJMath.RandomMemberOf<int>(moduleList);
		}
	}

	public static bool isRarityCompatible (ItemBasic.Rarity rarityOfModule, ItemBasic.Rarity rarityOfItem, int scaleUp = int.MaxValue)
	{

		if (rarityOfModule == ItemBasic.Rarity.Any) return true;

		int rarityDifference = (int) rarityOfItem - (int) rarityOfModule;
//		Debug.Log ("rarity of item: " + rarityOfItem);
//		Debug.Log ("rarity of mod: " + rarityOfModule);
//		Debug.Log ("RarityDifference: " + rarityDifference);
//		Debug.Log ("====================================");
		if (rarityDifference <= scaleUp && rarityDifference >= 0)
		{
			return true;
		}

		return false;
	}
}

