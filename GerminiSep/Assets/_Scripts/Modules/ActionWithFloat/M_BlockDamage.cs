using UnityEngine;
using System.Collections;

public class M_BlockDamage : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Deflection";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeDamage;
		procChance = 60;
		cooldown = 0.0f;
		staticPower = 8.0f + Stats.NGVForLevel(level) * power * 0.3f;
//		descriptionString = "When you take damage, you have a "+procChance+"% chance to block "+(int)staticPower+" damage.";
		descriptionString = ConvertString("The ship has @PRC% chance to block @POW damage from each hit.");
		itemGroup = ItemGroup.Shield;
		itemColor = ItemColor.Yellow;
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		if (CanBeFired) {
			unit.isBlocked = true;
			unit.SetShieldColor(Unit.ShieldColor.Orange);
			inputValue -= staticPower;
		}
		
		return inputValue;
	}
	
}
