using UnityEngine;
using System.Collections;

public class M_InverseDamage : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Subversion";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 3;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeDamage;
		procChance = 12;
		cooldown = 0.0f;
		staticPower = 2.0f + Stats.NGVForLevel(level) * power * 1.5f;
//		descriptionString = "When you take damage give you have a "+procChance+"% chance to heal "+(int)staticPower+" HP.";
		descriptionString = ConvertString("@PRC% chance to repair @POW HP when the ship takes damage.");
		itemGroup = ItemGroup.Defensive;
		itemColor = ItemColor.Mix;

	}
	
	public override float ActionWithFloat (float inputValue)
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
		
		return inputValue;
	}
	
}
