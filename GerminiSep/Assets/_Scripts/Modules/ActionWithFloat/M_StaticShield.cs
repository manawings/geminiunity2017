﻿using UnityEngine;
using System.Collections;

public class M_StaticShield : Module {

	public override void Deploy(Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		
	}

	const float reCooldown  = 30.0f;
	const float time = 5.0f;
	const float hpPercent = 0.15f;

	public override void Calibrate(float level, float power)
	{
		
		// Basic Settings ** MUST DO! **
		name = "Static Shield";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeDamage;
		procChance = 100;
		coolDownStartsReady = true;
		cooldown = 0f;
		itemGroup = ItemGroup.Shield;
		itemColor = ItemColor.Green;
		// Calculate the Power
		//staticPower = 1.0f + Stats.NGVForLevel(level) * power;
		
		// Write the custom Description
//		descriptionString = "When your take damage and hp lowest "+hpPercent*100+"% shield will activate "+time+" seconds.\nCooldown shield "+reCooldown+" seconds";
		descriptionString = ConvertString("Deploys an Ion Shield for " +time+ " seconds whenever HP is dropped below " + (hpPercent*100) + "%. Reloads in " +reCooldown+ " seconds.");
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		//Debug.Log(unit.stats.LifePercent);
		if (CanBeFired)
		{
			if (inputValue >= unit.stats.lifePoints) {
				
				unit.stats.lifePoints = 1;
				unit.ApplyShield(2.0f, time, Unit.ShieldColor.Green);
				RewriteCooldown(reCooldown);
				
			}else if (unit.stats.lifePoints - inputValue <= unit.stats.MaxLifePoints * hpPercent){
				
				unit.stats.lifePoints = unit.stats.lifePoints - inputValue;
				unit.ApplyShield(2.0f, time, Unit.ShieldColor.Green);
				RewriteCooldown(reCooldown);
				
			}
		}
		return inputValue;
	}
}
