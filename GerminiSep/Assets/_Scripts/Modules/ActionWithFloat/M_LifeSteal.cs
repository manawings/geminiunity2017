using UnityEngine;
using System.Collections;

public class M_LifeSteal : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
		
	}
	
	public override void Calibrate (float level, float power) {
		
		// Basic Settings ** MUST DO! **
		name = "Vampire Strike";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnDealDamage;
		procChance = 8;
		cooldown = 0f;
		
		// Calculate the Power
		staticPower = 2.0f + ( (Stats.NGVForLevel(level) * power)/4 ) - 1.0f;
		
		// Write the custom Description
//		descriptionString = "When you damage a target, you have a "+procChance+"% chance to heal "+(int)staticPower+" HP.";
		descriptionString = ConvertString("@PRC% chance to recover @POW HP when damaging an enemy.");
		itemGroup = ItemGroup.Weapon;
		itemColor = ItemColor.Yellow;
	}
	
	public override float ActionWithFloat ( float inputValue )
	{
		if (CanBeFired) {
			unit.Heal(staticPower);
		}
		
		return inputValue;
	}
}
