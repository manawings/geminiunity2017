﻿using UnityEngine;
using System.Collections;

public class M_AbsorptionShield2 : Module {
	
	public override void Deploy(Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy(unit, level, power);
		
	}
	
	const float reCooldown  = 10.0f;
	const float time = 0.5f;
	const float hpPercent = 100.0f;
	
	public override void Calibrate(float level, float power)
	{
		
		// Basic Settings ** MUST DO! **
		name = "Frost Shield";
		rarity = ItemBasic.Rarity.Rare;
		rarityScaleUp = 1;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeDamage;
		procChance = 100;
		coolDownStartsReady = true;
		cooldown = 0f;
		itemGroup = ItemGroup.Shield;
		itemColor = ItemColor.Pink;
		// Calculate the Power
		staticPower = 1.0f + Stats.NGVForLevel(level) * power;
		
		// Write the custom Description
		//		descriptionString = "When your take damage and hp lowest "+hpPercent*100+"% shield will activate "+time+" seconds.\nCooldown shield "+reCooldown+" seconds";
		descriptionString = ConvertString("Deploys a Frost Shield for half a second whenever you take damage. Reloads in " +reCooldown+ " seconds.");
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		//Debug.Log(unit.stats.LifePercent);
		if (CanBeFired)
		{
			if (inputValue >= unit.stats.lifePoints) {
				inputValue = 0;
				//unit.stats.lifePoints = 1;
				unit.ApplyShield(2.0f, time, Unit.ShieldColor.White);
				RewriteCooldown(reCooldown);
				
			}else if (unit.stats.lifePoints - inputValue <= unit.stats.MaxLifePoints * hpPercent){
				inputValue = 0;
				//unit.stats.lifePoints = unit.stats.lifePoints - inputValue;
				unit.ApplyShield(2.0f, time, Unit.ShieldColor.White);
				RewriteCooldown(reCooldown);
				
			}
		}
		return inputValue;
	}
}
