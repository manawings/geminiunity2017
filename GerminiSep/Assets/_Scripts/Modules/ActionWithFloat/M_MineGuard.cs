using UnityEngine;
using System.Collections;

public class M_MineGuard : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Mine Guard";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeMineDamage;
		procChance = 100;
		cooldown = 0.0f;
		staticPower = 4.0f + Stats.NGVForLevel(level) * power * 0.45f;
//		descriptionString = "When you take Mine damage you block "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Reduces damage from mines by @POW.");
		itemGroup = ItemGroup.Defensive;
		itemColor = ItemColor.Mix;
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		if (CanBeFired) {
			unit.isBlocked = true;
			unit.SetShieldColor(Unit.ShieldColor.Green);
			inputValue -= staticPower;
		}
		
		return inputValue;
	}
	
}
