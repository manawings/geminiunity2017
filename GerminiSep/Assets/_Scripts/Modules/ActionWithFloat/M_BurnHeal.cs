﻿using UnityEngine;
using System.Collections;

public class M_BurnHeal : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Burn Heal";
		rarity = ItemBasic.Rarity.Uncommon;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeBurnDamage;
		procChance = 35;
		cooldown = 0.0f;
		staticPower = Stats.NGVForLevel(level) * power * 0.15f - 1.0f;
		if (staticPower < 1) staticPower = 1; 
		//		descriptionString = "When you take Beam damage you block "+(int)staticPower+" damage.";
		descriptionString = ConvertString("@PRC% chance to repair @POW HP when the ship takes burn damage.");
		itemGroup = ItemGroup.Defensive;
		itemColor = ItemColor.Mix;
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		if (CanBeFired) {
			unit.isBlocked = true;
			unit.SetShieldColor(Unit.ShieldColor.Red);
			inputValue = 0;
			unit.Heal(staticPower);
		}
		
		return inputValue;
	}
}
