﻿using UnityEngine;
using System.Collections;

public class M_BurnGuard : Module {

	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Burn Guard";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeBurnDamage;
		procChance = 100;
		cooldown = 0.0f;
		staticPower = Stats.NGVForLevel(level) * power * 0.13f - 1.0f;
		if (staticPower < 1) staticPower = 1; 
		//		descriptionString = "When you take Beam damage you block "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Reduces damage from burn effects by @POW.");
		itemGroup = ItemGroup.Defensive;
		itemColor = ItemColor.Mix;
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		if (CanBeFired) {
			unit.isBlocked = true;
			unit.SetShieldColor(Unit.ShieldColor.Red);
			inputValue -= staticPower;
		}
		
		return inputValue;
	}
}
