using UnityEngine;
using System.Collections;

public class M_MissileGuard : Module {
	
	
	public override void Deploy (Unit unit, float level, float power = 9999.0f)
	{
		base.Deploy (unit, level, power);
	}
	
	public override void Calibrate (float level, float power) {
		
		name = "Missile Guard";
		rarity = ItemBasic.Rarity.Common;
		rarityScaleUp = 2;
		actionType = ActionType.ActionWithFloat;
		condition = Condition.OnTakeMissileDamage;
		procChance = 100;
		cooldown = 0.0f;
		staticPower = 3.0f + Stats.NGVForLevel(level) * power * 1.75f;
//		descriptionString = "When you take Missile damage you block "+(int)staticPower+" damage.";
		descriptionString = ConvertString("Reduces damage from missiles by @POW.");
		itemGroup = ItemGroup.Defensive;
		itemColor = ItemColor.Mix;
	}
	
	public override float ActionWithFloat (float inputValue)
	{
		if (CanBeFired) {
			inputValue -= staticPower;
		}
		
		return inputValue;
	}
	
}
