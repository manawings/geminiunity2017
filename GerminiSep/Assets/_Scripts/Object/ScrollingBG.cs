﻿using UnityEngine;
using System.Collections;

public class ScrollingBG : KJBehavior {
	
	public float scrollSpeed = 35.0f;
	
	// Use this for initialization
	void Start () {
		cameraTransform = Camera.main.transform;
		newPosition = transform.position;
		savedX = newPosition.x;
		AddTimer(Run);
		
	}
	
	Vector3 newPosition;
	Transform cameraTransform;
	float savedX;
	
	// Update is called once per frame
	void Run () {
		
		newPosition.y -= scrollSpeed * DeltaTime;
		newPosition.x = cameraTransform.position.x * 0.5f + savedX;
		transform.position = newPosition;
		
		if (transform.position.y < - 600.0f)
		{
			newPosition.y += 1200.0f;
		}
	}
}
