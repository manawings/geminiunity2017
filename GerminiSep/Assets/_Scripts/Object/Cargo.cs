﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cargo : Unit
{

	float lifemod = 100.0f;
	float armormod = 1.75f;
	bool isLeftSpawn = true;

	private static GameObject collectableObject;
	private static Collectable collectable;

	private static List<Cargo> _allActive;
	public static List<Cargo> allActive
	{
		get {
			if (_allActive == null)
			{
				_allActive = new List<Cargo>();
			}
			
			return _allActive;
		}
	}
	
	tk2dSpriteAnimator _spriteAnimator;
	tk2dSpriteAnimator spriteAnimator {
		get {
			if (_spriteAnimator == null)
			{
				_spriteAnimator = GetComponent<tk2dSpriteAnimator>();	
			}
			
			return _spriteAnimator;
		}
	}

	public void TutorialDelay () {
		TutorialData.Deploy(17);
	}

	public void Deploy ()
	{	
		KJTime.Add(TutorialDelay, 5.0f, 1); 

		isLeftSpawn = KJDice.Roll(50);

		float xPos = 350.0f;
		if (isLeftSpawn) xPos *= -1.0f;

		Vector3 position = new Vector3 (xPos, -20.0f);
		x = position.x;
		y = position.y;
		transform.position = new Vector3(position.x, position.y, 0.0f);

		float statFactor = Stats.NGVForLevel(ProfileController.SectorLevel);
		stats.life = 1.0f + lifemod * 1.0f * Stats.EnemyDifficultyFactor(0.60f, ProfileController.SectorLevel) * statFactor;
		stats.armor =  1.0f + armormod * 0.65f * statFactor;

		stats.Calibrate();


		base.Deploy();

		maxSpeed = 25.0f;

		if (isLeftSpawn) {
			targetTravelDirection = KJMath.RIGHT_ANGLE;
		} else {
			targetTravelDirection = - KJMath.RIGHT_ANGLE;
		}

		travelDirection = faceDirection = targetTravelDirection;

		AddTimer (Run);
	}
	
	void Run ()
	{


		if (!IsStunned) {

				
			Vector2 travelVector;

			travelVector = new Vector2(0.0f, -50.0f);

			if (isLeftSpawn) {
				if (targetTravelDirection > 0.0f) targetTravelDirection -= 0.0012f;
			} else {
				if (targetTravelDirection < 0.0f) targetTravelDirection += 0.0012f;
			}
					
			if (travelDirection != targetTravelDirection) travelDirection = ConvergeAngleTo(travelDirection, targetTravelDirection, 0.05f, 0.01f);				
			speed =  KJMath.Cap(travelVector.magnitude * 2.0f, 0, maxSpeed);
				
		}
		
		// RotationByRadians += 1.00f * DeltaTime;
		// transform.Translate(new Vector2(0.0f, -5.0f) * DeltaTime, Space.World);

		faceDirection = travelDirection;
		base.Run ();

		if (y > 400.0f) Deactivate();
	
	}
	
	public override void TakeHit (float damage = 25.0f, Entity.DamageType damageType = Entity.DamageType.Normal, bool isCrit = false, bool ignoreArmor = false)
	{
		// Unit must be under a certain Y for it to be able to take damage.
		// ExternalHit(0.0f);
		if (buffAcid.IsActive) damage += buffAcid.power;
		TakeDamage(damage, damageType, isCrit, ignoreArmor);

		/*
		int displayDamage = (int) damage;

		if (displayDamage > 0) {
			
			CombatPopup combatPopup = KJActivePool.GetNewOf<CombatPopup>("Popup");
			
			string damageString = displayDamage.ToString();
			
			CombatPopup.Type popupType = CombatPopup.Type.Yellow;
			if (isCrit) damageString += "!";
			
			combatPopup.Deploy(transform.position + Random.insideUnitSphere * 35.0f, damageString, popupType, isCrit);
		}

		stats.life -= damage;
		if (stats.life <= 0) {
			Explode();
		}*/
	}

	protected override void Explode ()
	{
		x = transform.position.x;
		y = transform.position.y;

		stats.lifePoints = 0;
		isCritical = false;

		RemoveAllTimers();
		TerminateEffects();
		SpwanGem();
		SpawnReactor();


		if (!ProfileController.hasCollectedCargo && !Collectable.CargoSpawned) SpawnCargo();

		EffectController.StandardExplosionAt(new Vector3(x, y, -5.0f));
		Deactivate();
		
	}

	void SpawnCargo () {

		collectableObject = KJActivePool.GetNew("Collectable");
		collectable = collectableObject.GetComponent<Collectable>();
		collectable.Deploy(Collectable.Type.Cargo, x + Random.Range (-20.0f , 20.0f), y + Random.Range (-20.0f , 20.0f), false); 

	}

	void SpwanGem () {

		int randomGem = (int) ( ProfileController.SectorId / 5 ) + Random.Range(6, 12);
		if (randomGem > 30) randomGem = 30;
		for (int i = 0; i <= randomGem; i++ ) {
			collectableObject = KJActivePool.GetNew("Collectable");
			collectable = collectableObject.GetComponent<Collectable>();
			collectable.Deploy(Collectable.Type.Gem, x + Random.Range (-20.0f , 20.0f), y + Random.Range (-20.0f , 20.0f), false); 
		}

	}

	void SpawnReactor ()
	{
		Collectable collectable = KJActivePool.GetNewOf<Collectable>("Collectable");
		
		Collectable.Type collectableType;
		
		float randomFloat = Random.value;
		
		// Maybe write another function later to adjust color bias.
		
		if (randomFloat > 0.75f) {
			
			collectableType = Collectable.Type.Green;
			
		} else if (randomFloat > 0.5f) {
			
			collectableType = Collectable.Type.Pink;
			
		} else if (randomFloat > 0.25f) {
			
			collectableType = Collectable.Type.Blue;
			
		} else {
			
			collectableType = Collectable.Type.Yellow;
			
		}
		
		collectable.Deploy(collectableType, transform.position.x, y);
	}

	protected override void OnActivate ()	
	{
		if (!EnemyUnit.allActive.Contains(this)) EnemyUnit.allActive.Add(this);
		base.OnActivate();
	}
	
	protected override void OnDeactivate ()
	{
		EnemyUnit.allActive.Remove(this);
		base.OnDeactivate();
	}
	

}

