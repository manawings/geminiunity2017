﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Debris : KJSpriteBehavior {
	
	Vector3 velocity;
	float spin;
	private static List<string> spriteNames = new List<string>() { "Debris1", "Debris2", "Debris3", "Debris4", "Debris5"};
	Color debrisColor, targetColor;

	float time;
	const float TimeMax = 5.0f;

	public void Deploy (Vector3 position, Vector3 velocity, string unitSprite = "")
	{
		this.velocity = velocity * Random.Range(0.10f, 0.35f);
		transform.position = new Vector3(position.x, position.y, 0.0f);
		FlatScale = 1.0f;
		spin = Random.Range(-12.0f, 12.0f);

		if (unitSprite != string.Empty) {
			sprite.SetSprite(unitSprite + "_" + spriteNames[Random.Range(0, 5)]);
		} else {
			sprite.SetSprite(KJMath.RandomMemberOf<string>(spriteNames));
		}

		debrisColor = new Color(1.5f, 0.25f, 0.0f, 1.0f);
		targetColor = Color.black;
		AddTimer (Run);

		time = TimeMax;

	}
	
	void Run ()
	{
//		velocity *= 10.0f * DeltaTime;
//		FlatScale *= 0.995f;
		velocity *= 0.995f;
		RotationByRadians += spin * DeltaTime;
		transform.position += velocity * DeltaTime;
		debrisColor = Color.Lerp(debrisColor, targetColor, 0.04f);
		sprite.color = debrisColor;
		time -= DeltaTime;

		if (time < 1.0f) targetColor = Color.clear;

		if (time < 0.0f) Deactivate();

	}
}
