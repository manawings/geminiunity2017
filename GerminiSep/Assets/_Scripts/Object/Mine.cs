using UnityEngine;
using System.Collections;

public class Mine : KJSpriteBehavior
{

	Vector2 speed;
	Vector2 randomVelocity;

	bool isFusing = false;
	float damage = 15.0f;
	float timeGap = 1.0f;
	float damageFactor = 1.0f;
	const int hitRangeSq = 20 * 20;
	const int fuseRangeSq = 20 * 20;
	const int blastRangeSq = 120 * 120;

	tk2dSpriteAnimator _spriteAnimator;
	tk2dSpriteAnimator spriteAnimator {
		get {
			if (_spriteAnimator == null)
			{
				_spriteAnimator = GetComponent<tk2dSpriteAnimator>();	
			}
			
			return _spriteAnimator;
		}
	}

	public void Deploy (Vector3 position, float speedX, float speedY, float timeGap = 1.0f, float damageFactor = 1.0f)
	{

		TutorialData.Deploy(11);

		this.timeGap = timeGap;	
		this.damageFactor = damageFactor;
		isFusing = false;
		damage = 5.0f + Stats.NGVForLevel(ProfileController.SectorLevel) * 1.7f;
		if(WaveController.type == WaveController.Type.BrutalMine) damage *= 0.7f;
		RotationByRadians = 0.0f;
		
		transform.position = position;
		speed = new Vector2(speedX, speedY);
			
		spriteAnimator.Play("MineOff");
		AddTimer (Run);
	}
	
	void Run ()
	{
		// Runtime code goes here.
		RotationByRadians += 1.00f * DeltaTime;
			
		transform.Translate(speed * DeltaTime, Space.World);
			
		if (transform.position.y < -300)
		{
			Deactivate();	
		} else {
			CheckForHit();
		}
	}
	
	void CheckForHit ()
	{
		if (GameController.PlayerUnit.IsAlive) {
			float distanceToPlayerUnit = KJMath.DistanceSquared2d( gameObject, GameController.PlayerUnit.gameObject );

			if (distanceToPlayerUnit < hitRangeSq) {
				FuseExplode();
			} 
//			else if (!isFusing) {
//				if (distanceToPlayerUnit < fuseRangeSq) {
//					StartFuse();
//				}
//			}
		}
	}
	
	void StartFuse ()
	{
		// Start Flashing

		KJCanary.PlaySoundEffect("MineOn");

		AddFlash(FlashOn, FlashOff, 0.10f, 0);
		AddTimer(FuseExplode, timeGap, 1);
		spriteAnimator.Play("MineOn");
		isFusing = true;
	}
	
	void FuseExplode ()
	{
		KJCanary.PlaySoundEffect("MineBoom");

		float distanceToPlayerUnit = KJMath.DistanceSquared2d( gameObject, GameController.PlayerUnit.gameObject );
			
		if (distanceToPlayerUnit < blastRangeSq) {

			GameController.PlayerUnit.TakeDamage(damage, Entity.DamageType.Mine, true);
			GameController.Singleton.BigShakeScreen();
			
		}

		if (GameController.AllyUnit != null && GameController.AllyUnit.IsAlive) {
			distanceToPlayerUnit = KJMath.DistanceSquared2d( gameObject, GameController.AllyUnit.gameObject );
			
			if (distanceToPlayerUnit < blastRangeSq) {
				
				GameController.AllyUnit.TakeDamage(damage, Entity.DamageType.Mine, true);
				
			}
		}
			
		for (int i = EnemyUnit.allActive.Count - 1 ; i > -1 ; i --)
		{
			if (i < EnemyUnit.allActive.Count) { // TODO: Temp. Control enemy list more efficiently.
				
				Unit enemyUnit = EnemyUnit.allActive[i];
				float distanceToExplosion = KJMath.DistanceSquared2d(gameObject, enemyUnit.gameObject);
				if (distanceToExplosion < blastRangeSq) {
					
					enemyUnit.ExternalBlast( KJMath.DirectionFromPosition(gameObject.transform.position, enemyUnit.transform.position), 1.0f * (1.0f - distanceToExplosion / blastRangeSq));
					enemyUnit.TakeDamage(damage * 1.5f * damageFactor, Entity.DamageType.Mine, true);
					
				}
				
			}
		}
			
		EffectController.MineExplosionAt(transform.position);
		spriteAnimator.Stop();
		Deactivate ();
	}
	
	private static Color mineFlash = new Color (0.9f, 0.15f, 0.03f, 1.0f);
	
	void FlashOn ()
	{
		sprite.color = mineFlash;
	}
	
	void FlashOff ()
	{
		sprite.color = Color.black;
	}
}

