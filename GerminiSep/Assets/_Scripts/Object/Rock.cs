using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rock : KJSpriteBehavior
{
	
	Vector2 speed;
	Vector2 randomVelocity;
	
	bool isFusing = false;
	float damage = 0.0f;
	float timeGap = 1.0f;
	
	const int hitRangeSq = 22 * 22;
	const int blastRangeSq = 120 * 120;

	private static List<Rock> _allActive;
	public static List<Rock> allActive
	{
		get {
			if (_allActive == null)
			{
				_allActive = new List<Rock>();
			}
			
			return _allActive;
		}
	}

	tk2dSpriteAnimator _spriteAnimator;
	tk2dSpriteAnimator spriteAnimator {
		get {
			if (_spriteAnimator == null)
			{
				_spriteAnimator = GetComponent<tk2dSpriteAnimator>();	
			}
			
			return _spriteAnimator;
		}
	}
	
	public void Deploy (Vector3 position, float speedX, float speedY, float timeGap = 1.0f, bool hasTail = true)
	{

		sprite.SetSprite("Rock" + Random.Range(1, 6).ToString());

		if (emitter != null) {
			emitter.Terminate();
		}

		if (hasTail) {
			emitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
			emitter.CopyFromModel(EffectController.Singleton.rockTail);
			emitter.SetObjectLink(gameObject);
			emitter.Deploy();
		}

		TutorialData.Deploy(12);
		this.timeGap = timeGap;	
		isFusing = false;

		damage = 5.0f + Stats.NGVForLevel(ProfileController.SectorLevel) * 3.00f;
		if(WaveController.type == WaveController.Type.BrutalRock) damage *= 0.5f;

		RotationByRadians = 0.0f;
		
		transform.position = position;
		speed = new Vector2(speedX, speedY);
		
		// spriteAnimator.Play("MineOff");
		AddTimer (Run);
	}

	KJEmitter emitter;

	protected override void OnDeactivate ()
	{
		if (emitter != null) {
			emitter.Terminate();
		}

		base.OnDeactivate();
	}
	
	void Run ()
	{
		// Runtime code goes here.
		RotationByRadians += 1.00f * DeltaTime;
		
		transform.Translate(speed * DeltaTime, Space.World);
		
		if (transform.position.y < -300)
		{
			Deactivate();	
		} else {
			CheckForHit();
		}
	}
	
	void CheckForHit ()
	{
		float distanceToPlayerUnit;

		if (GameController.PlayerUnit.IsAlive) {
			distanceToPlayerUnit = KJMath.DistanceSquared2d( gameObject, GameController.PlayerUnit.gameObject );
			if (distanceToPlayerUnit < hitRangeSq) {

				KJCanary.PlaySoundEffect("Rock", transform.position);
				GameController.PlayerUnit.TakeDamage(damage, Entity.DamageType.Mine, true);
				GameController.Singleton.BigShakeScreen();
				EffectController.MineExplosionAt(transform.position);
				Deactivate ();
				return;
			}
		}

		if (GameController.AllyUnit != null && GameController.AllyUnit.IsAlive) {
			distanceToPlayerUnit = KJMath.DistanceSquared2d( gameObject, GameController.AllyUnit.gameObject );
			if (distanceToPlayerUnit < hitRangeSq) {

				KJCanary.PlaySoundEffect("Rock", transform.position);
				GameController.AllyUnit.TakeDamage(damage, Entity.DamageType.Mine, true);
				EffectController.MineExplosionAt(transform.position);
				Deactivate ();
			}
		}
	}
}

