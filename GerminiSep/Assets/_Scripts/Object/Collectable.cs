using UnityEngine;
using System.Collections;

public class Collectable : KJBehavior {
	
	public float x, y = 0;
	public float direction = 0;
	public float directionChange = 0;
	public float speed = 0;
	public float spin = 0, rot = 0;
	
	private float distanceToPlayer = 0;
	
	private const int reactorRadius = 2000;
	private const int standardRadius = 10000;
	int pullRadius = standardRadius;

	public static bool CargoSpawned = false;

	public float yDrop = 45.0f;
	
	private Type type = Type.Gem;

	KJEmitter emitter;
	
	private tk2dSprite _sprite;
	private tk2dSprite sprite
	{
		get {
			if (_sprite == null)
			{
				_sprite = GetComponent<tk2dSprite>();
			}
			return _sprite;
		}
	}

	private tk2dSpriteAnimator _spriteAnimator;
	private tk2dSpriteAnimator spriteAnimator
	{
		get {
			if (_spriteAnimator == null)
			{
				_spriteAnimator = GetComponent<tk2dSpriteAnimator>();
			}
			return _spriteAnimator;
		}
	}
	
	public enum Type {
		Yellow,
		Pink,
		Blue,
		Green,
		Gem,
		Medic,
		Cargo
	}
	
	void SetSprite ()
	{

		sprite.scale = Vector3.one;

		switch (type)
		{
			
		case Type.Yellow:
			spriteAnimator.Play("PowerUpYellow");
			emitter.SetColor(new Color (1.0f, 0.8f, 0.2f, 1.0f),
			                 new Color (1.0f, 0.0f, 0.0f, 1.0f));
			break;
			
		case Type.Blue:
			spriteAnimator.Play("PowerUpBlue");
			emitter.SetColor(new Color (0.2f, 0.3f, 1.0f, 1.0f),
			                 new Color (0.0f, 0.0f, 1.0f, 1.0f));
			break;
			
		case Type.Pink:
			spriteAnimator.Play("PowerUpPink");
			emitter.SetColor(new Color (1.0f, 0.6f, 0.8f, 1.0f),
			                 new Color (0.3f, 0.0f, 1.0f, 1.0f));
			break;
			
		case Type.Green:
			spriteAnimator.Play("PowerUpGreen");
			emitter.SetColor(new Color (0.3f, 1.0f, 0.1f, 1.0f),
			                 new Color (0.0f, 0.6f, 0.8f, 1.0f));
			break;
			
		case Type.Medic:
			sprite.SetSprite("PuMedic5");
			break;
			
		case Type.Gem:
			spriteAnimator.Play("Gem");
			sprite.scale = Vector3.one * 0.5f;
			break;

		case Type.Cargo:

			if (ProfileController.secretMission) {
				spriteAnimator.Play("PuCargoSecret");
				emitter.SetColor(new Color (1.0f, 0.6f, 0.1f, 1.0f),
				                 new Color (1.0f, 0.0f, 0.0f, 1.0f));
			} else {
				spriteAnimator.Play("PuCargo");
				emitter.SetColor(new Color (0.3f, 0.9f, 0.7f, 1.0f),
				                 new Color (0.0f, 0.2f, 1.0f, 1.0f));
			}



			break;
		}
	}
	
	public void Deploy (Type type, float x, float y, bool fixMovementAsGem = true)
	{
		if (type == Type.Cargo) {
			CargoSpawned = true;
		}

		this.type = type;
		this.x = x;
		this.y = y;

		if (emitter != null) {
			emitter.Terminate();
			emitter = null;
		}

		if (type != Type.Gem) {
			emitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
			emitter.CopyFromModel(EffectController.Singleton.cargoTrail);
			emitter.SetObjectLink(gameObject);
		}


		// Reactors have a closer radius.
		if (type == Type.Gem || type == Type.Cargo || type == Type.Medic) {
			pullRadius = standardRadius;
		} else {
			pullRadius = reactorRadius;
		}


		SetSprite();

		if (type != Type.Gem) emitter.Deploy();
		
		yDrop = 45.0f;
		
		spin = Random.Range( -0.08f, 0.08f);
		directionChange = Random.Range( -0.03f, 0.03f);
		speed = Random.Range(30.0f, 90.0f);
			
		// Direction to Center	
		direction = KJMath.DirectionFromPosition(new Vector2(x, y), Vector2.zero) + Random.Range(-1.0f, 1.0f);
		
		AddTimer(Run);
		AddTimer(CheckBounds, 1.0f);
		
		if (type == Type.Gem && fixMovementAsGem) {
			FixMovement();
		}
		
		transform.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f); // TODO: Better Rotation functions
		transform.position = new Vector3(x, y, transform.position.z);
	}
	
	
	void Run ()
	{
		// Runtime code goes here.
			
		x += Mathf.Sin(direction) * speed * DeltaTime;
		y += (Mathf.Cos(direction) * speed - yDrop) * DeltaTime;
		
		direction += directionChange;
			
		if ( GameController.PlayerUnit.IsAlive )
		{
			distanceToPlayer = KJMath.DistanceSquared2d(gameObject, GameController.Singleton.playerShip);
				
			if (distanceToPlayer < pullRadius)
			{
				// Activate the pull - move the collectable closer towards the player.
				
				float factor = 1.0f - (distanceToPlayer / pullRadius);
				float directionToPlayer = KJMath.DirectionFromPosition(new Vector3(x, y, 0.0f), GameController.Singleton.playerShip.transform.position);
				x += factor * Mathf.Sin(directionToPlayer) * 7.0f;
				y += factor * Mathf.Cos(directionToPlayer) * 7.0f;
				
			}
				
			if (distanceToPlayer < 500.0f)
			{
				
				// Item is close enough to be collected.
				
				if (type == Type.Gem)
				{
					// Is a Gem.
					KJCanary.PlaySoundEffect("Gem", transform.position);
					GameController.PlayerUnit.CollectGem();

				} else if (type == Type.Cargo) {

					// Gain Cargo Box
					KJCanary.PlaySoundEffect("CollectCargo", transform.position);
					GameController.PlayerUnit.CollectCargo();

				} else {

					// Is another power-up.
					KJCanary.PlaySoundEffect("Reactor", transform.position);
					GameController.PlayerUnit.GainReactor(type);

				}
				
				Deactivate();
			}
		}
		
		
		// Transform, spin, rotate, etc.
		
		rot += spin;
		transform.localEulerAngles = new Vector3(0.0f, 0.0f, - Mathf.Rad2Deg * rot); // TODO: Better Rotation functions
		transform.position = new Vector3(x, y, transform.position.z);

	}
	
	void CheckBounds ()
	{
		// Check to see if the collectable has left the screen.
		
		if (x > 350.0f || x < - 350.0f || y > 250.0f || y < -250.0f)
		{
			direction = KJMath.DirectionFromPosition(new Vector2(x, y), Vector2.zero) + Random.Range(-1.0f, 1.0f);
		}
	}
	
	public void FixMovement()
	{
		// Set the movement so it does not move or spin.
		
		yDrop = 0.0f;
		spin = 0.05f;
		directionChange = 0.0f;
		rot = 0;
		direction = Mathf.PI;
		speed = 90.0f;
		RemoveTimerOrFlash(CheckBounds);
			
		if (type == Type.Gem)
		{
			transform.localEulerAngles = Vector3.zero;
			spin = 0.0f;
		}
	}
	
	protected override void OnDeactivate ()
	{
		if (type == Type.Cargo) {
			CargoSpawned = false;
		}

		if (emitter != null) {
			emitter.Terminate();
			emitter = null;
		}

		base.OnDeactivate();	
	}
}
