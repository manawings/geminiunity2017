﻿using UnityEngine;
using System.Collections;

public class PlatformTurretData {

	public Weapon weapon;
	public float shootTime, stopTime;
	public float delay;
	public int positionID;
	public float direction = 0;
	public int index;
	public float speedY;
	public MoveMent moveMent = MoveMent.None;

	public enum MoveMent {
		None,
		Down,
		Up,
		TopDown,
		ButtomUp
	}

}
