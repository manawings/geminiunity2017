﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformWall : KJBehavior {

	private static Vector2 WallSpeed = new Vector2(0.0f, -30.0f);
	void Start () {
	}

	public void Deploy (int side, float yPosition)
	{
		transform.position = new Vector3(210.0f * side , yPosition + 140.0f, 200.0f);
		AddTimer(Run);
		GetComponent<tk2dSprite>().FlipX = side == 1 ? true : false;
	}

	void Run ()
	{
		transform.Translate(WallSpeed * DeltaTime, Space.World);

		if (transform.position.y < -400)
		{
			Deactivate();
		}
	}
}
