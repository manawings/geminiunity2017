﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformData  {

	public Vector3 posLeft = new Vector3(-170 , 500.0f, 0.0f);
	public Vector3 posRight = new Vector3(170 , 500.0f, 0.0f);
	public float speedY = -40;

	public Vector2 speed;
	public List<PlatformTurretData> platformTurretDatas = new List<PlatformTurretData>();
	public Vector3 position;
	public bool isRight = false;

	public enum TurretType {
		standard,
		standard2,
		standard3,
		crossBeamR
	}
	public TurretType turretType;

	public void AddTurret (Weapon weapon,int index, float shootTime, float stopTime, float delay, float direction = 0,
	                       PlatformTurretData.MoveMent moveMent = PlatformTurretData.MoveMent.None, float speedY = 30) {
		PlatformTurretData platformTurretData = new PlatformTurretData();
		//turret.position = pos;
		platformTurretData.index = index;
		platformTurretData.direction = direction;
		platformTurretData.weapon = weapon;
		platformTurretData.shootTime = shootTime;
		platformTurretData.stopTime = stopTime;
		platformTurretData.moveMent = moveMent;
		platformTurretData.speedY = speedY;
		platformTurretDatas.Add(platformTurretData);
	}

	public List<Vector2> TurretPositionVector {
		get {
			return TurretPosition(turretType);
		}
	}

	public Vector2 TurretPositionAt (int index) {
		List<Vector2> positionList = TurretPositionVector;
		if (index < positionList.Count) 
			return positionList[index];
		else 
			return Vector2.zero;
		
	}

	public List<Vector2> TurretPosition (TurretType turretType) {
		switch (turretType) {
			case TurretType.standard : {
				List<Vector2> turretPos = new List<Vector2>();
				turretPos.Add(new Vector2(0 , 150));
				turretPos.Add(new Vector2(0 , 50));
				turretPos.Add(new Vector2(0 , 0));
				turretPos.Add(new Vector2(0 , -50));
				turretPos.Add(new Vector2(0 , -150));
				return turretPos;
			}
			case TurretType.standard2 : {
				List<Vector2> turretPos = new List<Vector2>();
				turretPos.Add(new Vector2(0 , 150));
				turretPos.Add(new Vector2(0 , 120));
				turretPos.Add(new Vector2(0 , 90));
				turretPos.Add(new Vector2(0 , 60));
				turretPos.Add(new Vector2(0 , 30));
				turretPos.Add(new Vector2(0 , 0));
				turretPos.Add(new Vector2(0 , -30));
				turretPos.Add(new Vector2(0 , -60));
				turretPos.Add(new Vector2(0 , -90));
				turretPos.Add(new Vector2(0 , -120));
				turretPos.Add(new Vector2(0 , -150));
				return turretPos;
			}
			case TurretType.standard3 : {
				List<Vector2> turretPos = new List<Vector2>();
				turretPos.Add(new Vector2(0 , 150));
				turretPos.Add(new Vector2(0 , 100));
				turretPos.Add(new Vector2(0 , 0));
				turretPos.Add(new Vector2(0 , -100));
				return turretPos;
			}
			case TurretType.crossBeamR : {
				List<Vector2> turretPos = new List<Vector2>();
				turretPos.Add(new Vector2(0 , 150));
				turretPos.Add(new Vector2(0 , -150));
				turretPos.Add(new Vector2(0 , -13));
				return turretPos;
			}
		}
		return new List<Vector2>();
	}

}
