﻿using UnityEngine;
using System.Collections;

public class PlatformTurret : Entity {

	public Weapon weapon;
	public Vector2 position;
	public float direction;
	bool isRight = false;
	public PlatformTurretData platformTurretData;
	public bool isSwitchPosition = false;

	public void Deploy (PlatformTurretData data, bool isRight = false)
	{
		isSwitchPosition = false;
		//team = Unit.Team.Enemy;
		this.platformTurretData = data;
		this.isRight = isRight;
		AddWeapon(data.weapon);
		if (ProfileController.SectorId == 2) {
			stats.damage = 5.0f + Stats.NGVForLevel(ProfileController.SectorLevel) * 0.5f;
		} else {
			stats.damage = 5.0f + Stats.NGVForLevel(ProfileController.SectorLevel) * 1.5f;
		}

	}

	public void AddWeapon ( Weapon modelWeapon )
	{
		if (gameObject.GetComponent<Weapon>() == null) {
			weapon = gameObject.AddComponent<Weapon>();
		} else {
			weapon = gameObject.GetComponent<Weapon>();
		}

		weapon.CopyDataFrom(modelWeapon);
		 
		if (platformTurretData.direction != 0)
			direction = Mathf.PI * platformTurretData.direction;
		else
			direction = isRight ? Mathf.PI * -0.5f : Mathf.PI * 0.5f;

		if(weapon.isBeam) weapon.SetBeamVars(platformTurretData.shootTime);

		weapon.direction = direction;
		weapon.Deploy();
		KJTime.Add(StartShooting, 0.05f , 1);

		RotationByRadians = direction;
	}

	public void StartShooting () {
		weapon.StartShooting();
		//StopShooting();
		KJTime.Add(StopShooting, platformTurretData.shootTime , 1);
	}

	public void StopShooting () {
		weapon.StopShooting();
		KJTime.Add(StartShooting, platformTurretData.stopTime , 1);
	}

	public void Terminate ()
	{
		KJTime.Remove(StartShooting);
		KJTime.Remove(StopShooting);
		Deactivate();
	}

	public override void SetBeamLock ()
	{
		//base.SetBeamLock ();
	}
}
