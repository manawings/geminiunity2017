using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformPattern  {

	static bool hasBeenInit = false;
	public List<PlatformData> platformData = new List<PlatformData>();
    string name = "";

	public int minLevel = 0;
	public int maxLevel = int.MaxValue;

	public static List<PlatformPattern> allPatterns = new List<PlatformPattern>();

	//Pattern
	public static PlatformPattern standard;
	public static PlatformPattern standard_1;
	public static PlatformPattern standard_2;
	public static PlatformPattern standard_3;
	public static PlatformPattern standard2;
	public static PlatformPattern standard2_1;
	public static PlatformPattern standard2_2;
	public static PlatformPattern standard3;
	public static PlatformPattern standard3_1;
	public static PlatformPattern standard3_2;
	public static PlatformPattern standard3_3;
	public static PlatformPattern standard4;
	public static PlatformPattern standard5;
	public static PlatformPattern standard6;
	public static PlatformPattern standard7;
	public static PlatformPattern standard8;
	public static PlatformPattern standard9;
	public static PlatformPattern beam1;
	public static PlatformPattern beam2;
	public static PlatformPattern beamHard1;
	public static PlatformPattern beamHard2;
	public static PlatformPattern beamHard3;
	public static PlatformPattern allBeam;

	public static PlatformPattern slow1;
	public static PlatformPattern slow1_1;
	public static PlatformPattern slow1_2;
	public static PlatformPattern slow1_3;
	public static PlatformPattern slow2;
	public static PlatformPattern slow2_1;
	public static PlatformPattern slow2_2;
	public static PlatformPattern slow2_3;
	public static PlatformPattern slow3;
	public static PlatformPattern slow3_1;
	public static PlatformPattern slow3_2;
	public static PlatformPattern slow3_3;
	public static PlatformPattern slow4;
	public static PlatformPattern slow4_1;
	public static PlatformPattern slow4_2;
	public static PlatformPattern slow4_3;
	public static PlatformPattern slow5;
	public static PlatformPattern slow5_1;
	public static PlatformPattern slow5_2;
	public static PlatformPattern slow5_3;

	public static PlatformPattern speed1;
	public static PlatformPattern speed2;
	public static PlatformPattern speed3;
	public static PlatformPattern speed4;
	public static PlatformPattern speed5;
	public PlatformPattern () 
	{
		allPatterns.Add(this);
	}
	
	public static void Initialize ()
	{
		PlatformData platformData = new PlatformData();

		standard = new PlatformPattern();
		standard.name = "standard";
		platformData = new PlatformData();

		platformData.AddTurret(WeaponPattern.P_RoundGreen,0 , 2, 1.25f, 1, 0.80f);
		platformData.AddTurret(WeaponPattern.P_BeamGreen,0 , 2, 0.1f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard.platformData.Add(platformData);

		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_RoundGreen,0 , 2, 1.25f, 1, -0.80f);
		platformData.AddTurret(WeaponPattern.P_RoundGreen,4 , 2, 2.0f, 1, 0, PlatformTurretData.MoveMent.TopDown);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard.platformData.Add(platformData);
		//

		platformData = new PlatformData();
		
		standard_1 = new PlatformPattern();
		standard_1.name = "standard_1";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_RoundGreen,0 , 2, 1.25f, 1, 0.80f);
		platformData.AddTurret(WeaponPattern.P_RoundGreen,4 , 2, 2.0f, 1, 0, PlatformTurretData.MoveMent.ButtomUp);

		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard_1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_RoundGreen,0 , 2, 1.25f, 1, -0.80f);
		platformData.AddTurret(WeaponPattern.P_BeamGreen,4 , 2, 0.1f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard_1.platformData.Add(platformData);
		//
		
		platformData = new PlatformData();
		
		standard_2 = new PlatformPattern();
		standard_2.name = "standard_1";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_RoundGreen,0 , 2, 1.25f, 1, 0.80f);
		platformData.AddTurret(WeaponPattern.P_RoundGreen,4 , 2, 2.0f, 1, 0, PlatformTurretData.MoveMent.ButtomUp);
		
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard_2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_RoundGreen,0 , 2, 1.25f, 1, -0.80f);
		platformData.AddTurret(WeaponPattern.P_BeamGreen,2 , 2, 0.1f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard_2.platformData.Add(platformData);

		//
		
		platformData = new PlatformData();
		
		standard_3 = new PlatformPattern();
		standard_3.name = "standard_1";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_RoundGreen,0 , 2, 1.25f, 1, 0.80f);
		platformData.AddTurret(WeaponPattern.P_RoundGreen,4 , 2, 2.0f, 1, 0, PlatformTurretData.MoveMent.ButtomUp);
		
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard_3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_RoundGreen,0 , 2, 1.25f, 1, -0.80f);
		platformData.AddTurret(WeaponPattern.P_BeamGreen,4 , 2, 0.1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 50.0f);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard_3.platformData.Add(platformData);

		//
		standard2 = new PlatformPattern();
		standard2.name = "standard2";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_ElectroGreen,4 , 1.5f, 2f, 1, 0.00f, PlatformTurretData.MoveMent.TopDown, 100.0f);
		platformData.AddTurret(WeaponPattern.P_BeamGreen, 0, 1f, 0.1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 50f);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_ElectroGreen,0 , 1.5f, 2f, 1, 0.00f, PlatformTurretData.MoveMent.ButtomUp, 100.0f);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard2.platformData.Add(platformData);
		//
		standard2_1 = new PlatformPattern();
		standard2_1.name = "standard2";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_ElectroGreen,4 , 1.5f, 2f, 1, 0.00f, PlatformTurretData.MoveMent.TopDown, 100.0f);
		platformData.AddTurret(WeaponPattern.P_BeamGreen, 0, 1f, 0.1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 50f);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard2_1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_ElectroGreen,0 , 1.5f, 2f, 1, 0.00f, PlatformTurretData.MoveMent.ButtomUp, 100.0f);
		platformData.AddTurret(WeaponPattern.P_ElectroGreen, 0, 0.75f, 0.5f, 2, 0);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard2_1.platformData.Add(platformData);
		//
		standard2_2 = new PlatformPattern();
		standard2_2.name = "standard2";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_ElectroGreen,4 , 1.5f, 2f, 1, 0.00f, PlatformTurretData.MoveMent.TopDown, 100.0f);
		platformData.AddTurret(WeaponPattern.P_BeamGreen, 0, 1f, 0.1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 50f);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard2_2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_ElectroGreen,0 , 1.5f, 2f, 1, 0.00f, PlatformTurretData.MoveMent.ButtomUp, 100.0f);
		platformData.AddTurret(WeaponPattern.P_ElectroGreen, 4, 0.75f, 0.5f, 2, 0);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard2_2.platformData.Add(platformData);
		//
		//
		beam1 = new PlatformPattern();
		beam1.name = "beam1";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_BeamPink,0 , 2.0f, 0.1f, 1 , 0.73f);
		platformData.AddTurret(WeaponPattern.P_BeamPink,4 , 3.5f, 0.1f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		beam1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_BeamPink,0 , 2.0f, 0.1f, 1 , -0.73f);
		platformData.AddTurret(WeaponPattern.P_BeamPink,0 , 3.5f, 0.1f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamPink,2 , 1.5f, 0.1f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		beam1.platformData.Add(platformData);
		//
		beamHard1 = new PlatformPattern();
		beamHard1.name = "beamHard1";
		platformData = new PlatformData();

		platformData.AddTurret(WeaponPattern.P_BeamPink, 0, 2.0f, 0.1f, 1, 0.73f);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 0, 2f, 0.1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 40f);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		beamHard1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_BeamPink, 0, 2.0f, 0.1f, 1, -0.73f);
		//platformData.AddTurret(WeaponPattern.P_MissilePink, 2, 1.5f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_MissilePink, 0, 1.5f, 2f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		beamHard1.platformData.Add(platformData);
		//
		beamHard2 = new PlatformPattern();
		beamHard2.name = "beamHard2";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_BeamRed, 0, 3.25f, 0.1f, 1, 0.73f, PlatformTurretData.MoveMent.TopDown, 40f);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 0, 3.25f, 0.1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 40f);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		beamHard2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_BeamOrange, 4, 2.75f, 0.1f, 1, -0.73f, PlatformTurretData.MoveMent.ButtomUp, 40f);
		platformData.AddTurret(WeaponPattern.P_BeamOrange, 4, 2.75f, 0.1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 40f);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		beamHard2.platformData.Add(platformData);
		//
		allBeam = new PlatformPattern();
		allBeam.name = "allBeam";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 10, 2.0f, 3.0f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 9, 2.0f, 3.0f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 8, 2.0f, 3.0f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 7, 2.0f, 3.0f, 1);
//		platformData.AddTurret(WeaponPattern.P_BeamBlue, 6, 1.0f, 5.0f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 5, 0.1f, 3.0f, 1);
//		platformData.AddTurret(WeaponPattern.P_BeamBlue, 4, 1.0f, 5.0f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 3, 2.0f, 3.0f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 2, 2.0f, 3.0f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 1, 2.0f, 3.0f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 0, 2.0f, 3.0f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		allBeam.platformData.Add(platformData);
		
		platformData = new PlatformData();

		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		allBeam.platformData.Add(platformData);
		//
		standard3 = new PlatformPattern();
		standard3.name = "standard3";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_SonarRed, 0, 1.25f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 250f);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 0, 1f, 0.1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 70f);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_SonarRed, 4, 1.25f, 2f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 250f);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard3.platformData.Add(platformData);
		//
		standard3_1 = new PlatformPattern();
		standard3_1.name = "standard3";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_StarBlue, 0, 1.5f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 150f);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard3_1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlue, 4, 1.5f, 2f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 150f);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 0, 1f, 0.1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 50f);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard3_1.platformData.Add(platformData);
		//
		standard3_2 = new PlatformPattern();
		standard3_2.name = "standard3";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_StarBlue, 0, 1.5f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 150f);
		platformData.AddTurret(WeaponPattern.P_StarBlue, 4, 0.9f, 0.4f, 1, 0);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard3_2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlue, 4, 1.5f, 2f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 150f);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 0, 1f, 0.1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 50f);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard3_2.platformData.Add(platformData);
		//
		standard3_3 = new PlatformPattern();
		standard3_3.name = "standard3";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_StarBlue, 0, 1.5f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 150f);
		platformData.AddTurret(WeaponPattern.P_StarBlue, 0, 0.9f, 0.4f, 1, 0);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard3_3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlue, 4, 1.5f, 2f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 150f);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 0, 1f, 0.1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 50f);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard3_3.platformData.Add(platformData);
		//
		standard4 = new PlatformPattern();
		standard4.name = "standard4";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_SpeedBlue, 1, 1.5f, 2f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 150f);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 2, 2f, 0.1f, 1, 0);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard4.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_SpeedBlue, 1, 1.5f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 150f);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard4.platformData.Add(platformData);
		//
		standard5 = new PlatformPattern();
		standard5.name = "standard5";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_StandarBlue, 0, 2.25f, 1.8f, 1, 0.73f);
		platformData.AddTurret(WeaponPattern.P_StandarBlue, 0, 2.0f, 1.4f, 1 );
		platformData.AddTurret(WeaponPattern.P_BeamRed, 2, 1f, 0.1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 40f);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard5.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StandarBlue, 4, 2.0f, 1.4f, 1 );
		platformData.AddTurret(WeaponPattern.P_StandarBlue, 0, 1.75f, 1.8f, 1, -0.73f);
		//platformData.AddTurret(WeaponPattern.P_BeamBlue, 2, 1.75f, 0.1f, 1, 0);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard5.platformData.Add(platformData);
		//
		beam2 = new PlatformPattern();
		beam2.name = "beam2";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_BeamGreen, 0, 1f, 3.25f, 1, 0);
		platformData.AddTurret(WeaponPattern.P_BeamGreen, 1, 1f, 3.25f, 1, 0);
		platformData.AddTurret(WeaponPattern.P_BeamGreen, 3, 1f, 3.25f, 1, 0);
		platformData.AddTurret(WeaponPattern.P_BeamGreen, 4, 1f, 3.25f, 1, 0);
		platformData.AddTurret(WeaponPattern.P_ElectroGreen, 4, 2f, 1f, 1, 0);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		beam2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_ElectroRed, 0, 1.5f, 1f, 1, 0);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 1, 1f, 4f, 1, 0);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 2, 1f, 4f, 1, 0);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 3, 1f, 4f, 1, 0);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard3;
		platformData.isRight = true;
		beam2.platformData.Add(platformData);
		//
		standard6 = new PlatformPattern();
		standard6.name = "standard2";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_StandarGreen,2 ,1.5f, 1f, 1, 0.3f);
		platformData.AddTurret(WeaponPattern.P_StandarGreen,2 ,2, 1f, 1, 0f);
		platformData.AddTurret(WeaponPattern.P_StandarGreen,2 ,1.5f, 1f, 1, 0.7f);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard6.platformData.Add(platformData);
		
		platformData = new PlatformData();
	//	platformData.AddTurret(WeaponPattern.P_BeamRed,0 , 0.5f, 0.5f, 1, 0.00f, PlatformTurretData.MoveMent.TopDown, 50.0f);
		platformData.AddTurret(WeaponPattern.P_BeamRed,4 , 0.5f, 0.5f, 1, 0.00f, PlatformTurretData.MoveMent.ButtomUp, 50.0f);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard6.platformData.Add(platformData);
		//
		beamHard3 = new PlatformPattern();
		beamHard3.name = "beamHard3";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_BeamRed, 0, 2f, 0.1f, 1, 0.625f);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 2, 2f, 0.1f, 1, 0.625f);
		//platformData.AddTurret(WeaponPattern.P_MissileRed, 2, 1.5f, 1f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		beamHard3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_BeamPink, 0, 2.75f, 0.1f, 1, -0.625f);
		platformData.AddTurret(WeaponPattern.P_BeamPink, 2, 2.75f, 0.1f, 1, -0.625f);
		platformData.AddTurret(WeaponPattern.P_MissilePink, 0, 1.7f, 1f, 1);
		platformData.AddTurret(WeaponPattern.P_MissilePink, 4, 1.7f, 1f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		beamHard3.platformData.Add(platformData);
		//
		standard7 = new PlatformPattern();
		standard7.name = "standard8";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 0, 1f, 1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 300);
		//platformData.AddTurret(WeaponPattern.P_BeamGreen, 4, 1f, 1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 50);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 2, 0.75f, 1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 275);
		//platformData.AddTurret(WeaponPattern.P_BeamBlue, 4, 2f, 1f, 1, 0);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard7.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 0, 1f, 1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 300);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 2, 0.75f, 1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 275);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 0, 2f, 1f, 1, 0);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard7.platformData.Add(platformData);
		//
		//
		standard8 = new PlatformPattern();
		standard8.name = "standard8";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 0, 1f, 0.9f, 1, 0.55f, PlatformTurretData.MoveMent.TopDown, 300);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 2, 1f, 0.9f, 1, 0.55f, PlatformTurretData.MoveMent.ButtomUp, 300);
		//platformData.AddTurret(WeaponPattern.P_BeamBlue, 4, 1f, 1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 50);
		//platformData.AddTurret(WeaponPattern.P_BeamBlue, 4, 2f, 1f, 1, 0);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard8.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 0, 1f, 0.9f, 1, -0.45f, PlatformTurretData.MoveMent.TopDown, 300);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 2, 1f, 0.9f, 1, -0.45f, PlatformTurretData.MoveMent.ButtomUp, 300);
		//platformData.AddTurret(WeaponPattern.P_BeamBlue, 0, 2f, 1f, 1, 0);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard8.platformData.Add(platformData);
		//
		standard9 = new PlatformPattern();
		standard9.name = "standard9";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_StarRedS, 0, 1f, 2f, 1, 0.45f, PlatformTurretData.MoveMent.TopDown, 300);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 2, 1f, 1.5f, 1, 0.45f, PlatformTurretData.MoveMent.ButtomUp, 300);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 4, 1f, 1f, 1, 0, PlatformTurretData.MoveMent.ButtomUp, 50);
		//platformData.AddTurret(WeaponPattern.P_BeamRed, 4, 2f, 1f, 1, 0);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		standard9.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarRedS, 0, 1f, 1.7f, 1, -0.4f, PlatformTurretData.MoveMent.TopDown, 300);
		//platformData.AddTurret(WeaponPattern.P_StarRedS, 2, 1f, 1.3f, 1, -0.4f, PlatformTurretData.MoveMent.ButtomUp, 300);
		//platformData.AddTurret(WeaponPattern.P_BeamRed, 0, 2f, 1f, 1, 0);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		standard9.platformData.Add(platformData);
		//
		slow1 = new PlatformPattern();
		slow1.name = "slow1";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 0, 0.25f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 1, 0.25f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 2, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 3, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 4, 0.25f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 5, 0.25f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_MissilePink, 0, 0.25f, 0.5f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 350);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 6, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 7, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 8, 0.25f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 9, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 10, 0.25f, 2.0f, 1);
		platformData.AddTurret(WeaponPattern.P_MissilePink, 0, 0.25f, 0.5f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 350);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow1.platformData.Add(platformData);
		//
		slow1_1 = new PlatformPattern();
		slow1_1.name = "slow1";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 6, 0.35f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 7, 0.25f, 2.35f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 8, 0.55f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 9, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 10, 0.35f, 2.0f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 0, 0.5f, 0.75f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 350);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow1_1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 0, 0.25f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 1, 0.325f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 2, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 3, 0.45f, 2.2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 4, 0.25f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 5, 0.5f, 2.75f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 10, 0.5f, 0.75f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 350);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow1_1.platformData.Add(platformData);
		//
		slow1_2 = new PlatformPattern();
		slow1_2.name = "slow1";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 6, 0.35f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 7, 0.25f, 2.35f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 8, 0.55f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 9, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 10, 0.5f, 1.7f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 0, 0.5f, 0.65f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 450);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow1_2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 0, 0.25f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 1, 0.325f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 2, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 3, 0.45f, 2.2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 4, 0.25f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 5, 0.5f, 2.75f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 10, 0.5f, 0.65f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 450);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow1_2.platformData.Add(platformData);
		//
		slow1_3 = new PlatformPattern();
		slow1_3.name = "slow1";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarRedS, 0, 0.5f, 0.65f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 450);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 10, 0.5f, 0.65f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 450);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 10, 3f, 1f, 1, 0);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow1_3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 6, 0.35f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 7, 0.25f, 2.35f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 8, 0.55f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 9, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 10, 0.5f, 1.7f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 0, 0.25f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 1, 0.325f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 2, 0.25f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 3, 0.45f, 2.2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 4, 0.25f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 5, 0.5f, 2.75f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow1_3.platformData.Add(platformData);
		//
		slow2 = new PlatformPattern();
		slow2.name = "slow2";
		platformData = new PlatformData();
		
		platformData.AddTurret(WeaponPattern.P_MissileRed, 0, 1f, 1f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 2, 1f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 4, 1f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 6, 1f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 8, 1f, 3f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 1, 0.25f, 2.9f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 3, 0.25f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 5, 0.25f, 1.8f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 7, 0.25f, 2.2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 9, 0.25f, 1.7f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow2.platformData.Add(platformData);
		//
		slow2_1 = new PlatformPattern();
		slow2_1.name = "slow2";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 0, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 350);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 350);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 2, 1f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_MissileRed, 4, 1f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 8, 1f, 3f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow2_1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 1, 0.25f, 2.9f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 3, 0.25f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 5, 0.25f, 1.8f, 1);
//		platformData.AddTurret(WeaponPattern.P_MissileRed, 6, 2f, 1f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 7, 0.25f, 2.2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 9, 0.25f, 1.7f, 1);
//		platformData.AddTurret(WeaponPattern.P_MissileRed, 10, 1f, 1f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow2_1.platformData.Add(platformData);
		//
		slow2_2 = new PlatformPattern();
		slow2_2.name = "slow2";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 0, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 400);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 350);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 10, 1f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_MissilePink, 4, 1f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_MissilePink, 0, 1f, 1f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow2_2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 1, 0.25f, 2.9f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 3, 0.25f, 2.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 5, 0.25f, 1.8f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 7, 0.25f, 2.2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 9, 0.25f, 1.7f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow2_2.platformData.Add(platformData);
		//
		slow2_3 = new PlatformPattern();
		slow2_3.name = "slow2";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 0, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 275);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 400);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 10, 1f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_MissileBlue, 4, 1f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_MissileBlue, 0, 1f, 1f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow2_3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 1, 0.25f, 1.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 3, 0.25f, 2.75f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 5, 0.25f, 2.4f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 7, 0.25f, 2.0f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 9, 0.25f, 1.7f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow2_3.platformData.Add(platformData);
		//
		slow3 = new PlatformPattern();
		slow3.name = "slow3";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 1, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 5, 0.25f, 4f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 7, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 9, 1f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 0, 2f, 1f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 9, 2f, 1f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow3.platformData.Add(platformData);
		//
		slow3_1 = new PlatformPattern();
		slow3_1.name = "slow3";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 0, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 325);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 1, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 5, 0.25f, 4f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 7, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 9, 1f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow3_1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 400);
		platformData.AddTurret(WeaponPattern.P_MissileBlue, 0, 1f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_MissileBlue, 9, 1f, 1f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow3_1.platformData.Add(platformData);
		//
		slow3_2 = new PlatformPattern();
		slow3_2.name = "slow3";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarRedS, 0, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 425);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 1, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 3, 1.2f, 1.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 5, 0.25f, 4f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 7, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 9, 1.5f, 1.7f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow3_2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarRedS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 350);
		platformData.AddTurret(WeaponPattern.P_MissileRed, 0, 1f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_MissileRed, 9, 1f, 1f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow3_2.platformData.Add(platformData);
		//
		slow3_3 = new PlatformPattern();
		slow3_3.name = "slow3";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 0, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.ButtomUp, 325);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 1, 1.5f, 1.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 5, 1.5f, 2f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 7, 1.25f, 1.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 9, 1f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow3_3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 400);
		platformData.AddTurret(WeaponPattern.P_MissileYellow, 0, 1f, 3f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamOrange, 9, 1f, 1f, 1);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow3_3.platformData.Add(platformData);
		//
		slow4 = new PlatformPattern();
		slow4.name = "slow4";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 0, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlue, 0, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 6, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlue, 6, 1f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow4.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 3, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlue, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 9, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlue, 9, 1f, 1.25f, 1);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow4.platformData.Add(platformData);
		//
		slow4_1 = new PlatformPattern();
		slow4_1.name = "slow4";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 300);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 0, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRed, 2, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 6, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRed, 7, 1f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow4_1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 400);
		platformData.AddTurret(WeaponPattern.P_StarRed, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 9, 2f, 1.25f, 1);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow4_1.platformData.Add(platformData);
		//
		slow4_2 = new PlatformPattern();
		slow4_2.name = "slow4";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 300);
		platformData.AddTurret(WeaponPattern.P_StarYellow, 2, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 6, 2f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow4_2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_BeamOrange, 10, 1f, 1.1f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 400);
		platformData.AddTurret(WeaponPattern.P_StarYellow, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 9, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamOrange, 0, 1f, 1.1f, 1);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow4_2.platformData.Add(platformData);
		//
		slow4_3 = new PlatformPattern();
		slow4_3.name = "slow4";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 325);
		platformData.AddTurret(WeaponPattern.P_StarGreen, 0, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 2, 1f, 1.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreen, 6, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 7, 1.2f, 1.5f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow4_3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 10, 0.5f, 0.9f, 1, 0 , PlatformTurretData.MoveMent.TopDown, 375);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 3, 1f, 1.5f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreen, 9, 2f, 1.25f, 1);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow4_3.platformData.Add(platformData);
		//
		slow5 = new PlatformPattern();
		slow5.name = "slow5";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 0, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlue, 0, 2f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 500);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 6, 2f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 6, 1f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow5.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 3, 2f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 9, 2f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 9, 1f, 1.25f, 1);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow5.platformData.Add(platformData);
		//
		slow5_1 = new PlatformPattern();
		slow5_1.name = "slow5";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 0, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRed, 0, 2f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 375);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 6, 2f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 6, 1f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow5_1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarRed, 0, 2f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 250);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 3, 2f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarGreenS, 9, 2f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 9, 1f, 1.25f, 1);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow5_1.platformData.Add(platformData);
		//
		slow5_2 = new PlatformPattern();
		slow5_2.name = "slow5";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 0, 2f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarRedS, 0, 2f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 325);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 6, 1f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 6, 1f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow5_2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarRed, 0, 2f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 400);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 3, 1f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 9, 2f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 9, 1f, 1.25f, 1);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow5_2.platformData.Add(platformData);
		//
		slow5_3 = new PlatformPattern();
		slow5_3.name = "slow5";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 0, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarPink, 0, 2f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 350);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 6, 2f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 6, 1f, 1.25f, 1);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		slow5_3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarPinkS, 0, 2f, 2f, 1, 0, PlatformTurretData.MoveMent.TopDown, 375);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 3, 1f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 3, 1f, 1.25f, 1);
		platformData.AddTurret(WeaponPattern.P_StarYellowS, 9, 2f, 1.25f, 1);
		//platformData.AddTurret(WeaponPattern.P_StarBlue, 9, 1f, 1.25f, 1);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		slow5_3.platformData.Add(platformData);
		//
		speed1 = new PlatformPattern();
		speed1.name = "speed1";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_BeamRed, 0, 2f, 1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 700);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		speed1.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 2, 2f, 1.25f, 1);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		speed1.platformData.Add(platformData);
		//
		speed2 = new PlatformPattern();
		speed2.name = "speed2";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_BeamRed, 0, 2f, 1f, 1, 0, PlatformTurretData.MoveMent.Down, 500);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		speed2.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarRed, 2, 2f, 1.5f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamRed, 4, 2f, 1f, 1, 0, PlatformTurretData.MoveMent.Up, 500);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		speed2.platformData.Add(platformData);
		//
		speed3 = new PlatformPattern();
		speed3.name = "speed3";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 0, 0.25f, 1.75f, 1);
		//platformData.AddTurret(WeaponPattern.P_BeamBlue, 4, 2f, 1f, 1, 0, PlatformTurretData.MoveMent.Up, 35);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		speed3.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.AddTurret(WeaponPattern.P_StarBlueS, 4, 0.25f, 1.75f, 1);
		platformData.AddTurret(WeaponPattern.P_BeamBlue, 0, 2f, 1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 50);
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		speed3.platformData.Add(platformData);
		//
		speed4 = new PlatformPattern();
		speed4.name = "speed4";
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_MissileYellow, 0, 2f, 0.1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 1000);
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard;
		speed4.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard;
		platformData.isRight = true;
		speed4.platformData.Add(platformData);
		//
		speed5 = new PlatformPattern();
		speed5.name = "speed4";
		platformData = new PlatformData();
		platformData.position = platformData.posLeft;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.isRight = false;
		platformData.turretType = PlatformData.TurretType.standard2;
		speed5.platformData.Add(platformData);
		
		platformData = new PlatformData();
		platformData.AddTurret(WeaponPattern.P_MissileGreen, 2, 2f, 0.1f, 1, 0, PlatformTurretData.MoveMent.TopDown, 1000);
		platformData.position = platformData.posRight;
		platformData.speed = new Vector2(0 , platformData.speedY);
		platformData.turretType = PlatformData.TurretType.standard2;
		platformData.isRight = true;
		speed5.platformData.Add(platformData);

	}

}
