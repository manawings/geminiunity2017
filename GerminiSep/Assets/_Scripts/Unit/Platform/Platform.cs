﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Platform : KJSpriteBehavior {
	
	public Vector2 speed;
	public List<PlatformTurret> platformTurrets = new List<PlatformTurret>();
	public Vector3 position;
	public bool isRight = false;
	public PlatformData platformData;

	public static Vector2 PlatformSpeed = new Vector2(0.0f, -40.0f);

	public void Deploy (PlatformData platformData)
	{
		platformTurrets = new List<PlatformTurret>();

		this.position = platformData.position;
		this.isRight = platformData.isRight;
		this.speed = PlatformSpeed;
		this.platformData = platformData;

		GetComponent<tk2dSprite>().FlipX = platformData.isRight;

		position.z = 10;

		for (int i = 0 ; i < platformData.platformTurretDatas.Count ; i++) {
			PlatformTurretData platformTurretData = platformData.platformTurretDatas[i];
			DeployTurret(platformTurretData);
			SetTurretPosition();
		}

		transform.position = position;

		AddTimer (Run);
	}

	void Run ()
	{

		transform.Translate(Platform.PlatformSpeed * DeltaTime, Space.World);

		foreach (PlatformTurret platformTurret in platformTurrets) {
			if (platformTurret.platformTurretData.moveMent == PlatformTurretData.MoveMent.None) {
				//platformTurret.gameObject.transform.Translate(speed * DeltaTime, Space.World);
			}
			if (platformTurret.platformTurretData.moveMent == PlatformTurretData.MoveMent.Up) {
				MoveMentUp(platformTurret);
			}
			if (platformTurret.platformTurretData.moveMent == PlatformTurretData.MoveMent.Down) {
				MoveMentDown(platformTurret);
			}
			if (platformTurret.platformTurretData.moveMent == PlatformTurretData.MoveMent.TopDown) {
				MoveMentTopDown(platformTurret);
			}
			if (platformTurret.platformTurretData.moveMent == PlatformTurretData.MoveMent.ButtomUp) {
				MoveMentButoomUp(platformTurret);
			}
		}

		if (transform.position.y < -500)
		{
			foreach (PlatformTurret platformTurret in platformTurrets) {
				platformTurret.gameObject.transform.parent = null;
				platformTurret.Terminate();
			}
			Deactivate();
		} else {
			//CheckForHit();
		}
	}

	void MoveMentDown (PlatformTurret platformTurret) {
		float speedY = platformTurret.platformTurretData.speedY;
		speedY = speedY + UnityEngine.Random.Range(-speedY * 0.2f, speedY * 0.2f);
		platformTurret.platformTurretData.speedY = platformTurret.platformTurretData.speedY + UnityEngine.Random.Range(0, platformTurret.platformTurretData.speedY * 0.5f);
		if (platformTurret.gameObject.transform.localPosition.y >= platformTurret.position.y - 150.0f && !platformTurret.isSwitchPosition) {
			Vector2 newSpeed = new Vector2(0, -speedY);
			platformTurret.gameObject.transform.Translate(newSpeed * DeltaTime, Space.World);
		} else {
			platformTurret.isSwitchPosition = true;
		}
		if (platformTurret.gameObject.transform.localPosition.y <= platformTurret.position.y && platformTurret.isSwitchPosition) {
			Vector2 newSpeed = new Vector2(0, speedY);
			platformTurret.gameObject.transform.Translate(newSpeed * DeltaTime, Space.World);
		} else {
			platformTurret.isSwitchPosition = false;
		}
	}

	void MoveMentUp (PlatformTurret platformTurret) {
		float speedY = platformTurret.platformTurretData.speedY;
		speedY = speedY + UnityEngine.Random.Range(-speedY * 0.2f, speedY * 0.2f);
		if (platformTurret.gameObject.transform.localPosition.y <= platformTurret.position.y + 150.0f && !platformTurret.isSwitchPosition) {
			Vector2 newSpeed = new Vector2(0, speedY);
			platformTurret.gameObject.transform.Translate(newSpeed * DeltaTime, Space.World);
		} else {
			platformTurret.isSwitchPosition = true;
		}

		if (platformTurret.gameObject.transform.localPosition.y >= platformTurret.position.y && platformTurret.isSwitchPosition) {
			Vector2 newSpeed = new Vector2(0, -speedY);
			platformTurret.gameObject.transform.Translate(newSpeed * DeltaTime, Space.World);
		} else {
			platformTurret.isSwitchPosition = false;
		}
	}

	void MoveMentTopDown (PlatformTurret platformTurret) {
		float speedY = platformTurret.platformTurretData.speedY;
		speedY = speedY + UnityEngine.Random.Range(-speedY * 0.2f, speedY * 0.2f);
		if (platformTurret.gameObject.transform.localPosition.y >= -150.0f && !platformTurret.isSwitchPosition) {
			Vector2 newSpeed = new Vector2(0, -speedY);
			platformTurret.gameObject.transform.Translate(newSpeed * DeltaTime, Space.World);
		} else {
			platformTurret.isSwitchPosition = true;
		}
		if (platformTurret.gameObject.transform.localPosition.y <= 150 && platformTurret.isSwitchPosition) {
			Vector2 newSpeed = new Vector2(0, speedY);
			platformTurret.gameObject.transform.Translate(newSpeed * DeltaTime, Space.World);
		} else {
			platformTurret.isSwitchPosition = false;
		}
	}

	void MoveMentButoomUp (PlatformTurret platformTurret) {
		float speedY = platformTurret.platformTurretData.speedY;
		speedY = speedY + UnityEngine.Random.Range(-speedY * 0.2f, speedY * 0.2f);
		if (platformTurret.gameObject.transform.localPosition.y >= -150.0f && !platformTurret.isSwitchPosition) {
			Vector2 newSpeed = new Vector2(0, -speedY);
			platformTurret.gameObject.transform.Translate(newSpeed * DeltaTime, Space.World);
		} else {
			platformTurret.isSwitchPosition = true;
		}
		if (platformTurret.gameObject.transform.localPosition.y <= 150 && platformTurret.isSwitchPosition) {
			Vector2 newSpeed = new Vector2(0, speedY);
			platformTurret.gameObject.transform.Translate(newSpeed * DeltaTime, Space.World);
		} else {
			platformTurret.isSwitchPosition = false;
		}
	}

	void SetTurretPosition () {
		foreach (PlatformTurret platformTurret in platformTurrets) {
			platformTurret.gameObject.transform.parent = transform;
			platformTurret.gameObject.transform.position = new Vector2(gameObject.transform.position.x , (gameObject.transform.position.y + platformTurret.position.y));
		}
	}

	void DeployTurret(PlatformTurretData platformTurretData) {
		PlatformTurret platformTurret = KJActivePool.GetNewOf<PlatformTurret>("PlatformTurret");
		platformTurret.position = platformData.TurretPositionAt(platformTurretData.index);
		platformTurret.Deploy(platformTurretData, isRight);
		platformTurrets.Add(platformTurret);
	}


	void AddWall () {

	}
	
}
