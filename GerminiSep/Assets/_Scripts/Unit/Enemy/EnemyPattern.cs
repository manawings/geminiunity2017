using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
public class EnemyPattern
{
	
	public Entity.Image image = Entity.Image.EnemySimple;
	
	public Weapon primaryWeapon = null;
	public Weapon secondaryWeapon = null;

	public int moduleId = 0;

	public List<WaveController.Type> waveTypes = new List<WaveController.Type>();

	public enum UnitColor {
		None,
		Red,
		Green,
		Pink,
		Yellow,
		Blue
	}

	UnitColor unitcolor = UnitColor.None;

	// Module and Additional Weapons
	
	public float lifeMod = 1.0f;
	public float armorMod = 1.0f;
	public float damageMod = 1.0f;
	public float speed = 380.0f;
	public float dangerValue = 1.0f;
	public bool isSingleUnit = false;
	public bool isFaceToPlayer = false;
	public bool isEvent = false;

	public int minLevel = 0;
	public int maxLevel = int.MaxValue;
	public string name;

	public const int maxEasyLevel = 3;
	public const int maxNormalLevel = 7;

	public const int babyLevel = 3;
	public const int easyLevel = 6;
	public const int mediumLevel = 9;
	public const int hardLevel = 12;
	public const int hellLevel = 15;
	public const int extremeLevel = 20;
	public const int ultimateLevel = 25;

	public const int eliteUpgradeLevel = 15;

	public static EnemyPattern Standard, Support, Standard2, StandardBH;
	public static EnemyPattern Sweeper;
	public static EnemyPattern Rapid1, Rapid2, RapidBH;
	public static EnemyPattern Spread1, Spread2;
	public static EnemyPattern Scatter1, Scatter2;
	public static EnemyPattern Homing1, HomingFast;
	public static EnemyPattern Striker;
	public static EnemyPattern Beamer, Beamer2, Beamerhell, Beamerstun, Beamerburn;
	public static EnemyPattern Chainer1, Chainer2;
	public static EnemyPattern BlackHole;
	public static EnemyPattern Stunner;
	public static EnemyPattern Laser1, Laser2, Laser3, Laser4;
	public static EnemyPattern LaserCirclePink;
	public static EnemyPattern LaserCircleGreen;

	//New Unit
	public static EnemyPattern LaserCircleBlue;
	public static EnemyPattern LaserCircleRed;
	public static EnemyPattern SweepBlue;
	public static EnemyPattern SpreadBlue;
	public static EnemyPattern ChainerBlue2;
	public static EnemyPattern ChainerRed;
	public static EnemyPattern ChainerPurple;
	public static EnemyPattern ChainerRed2;
	public static EnemyPattern ChainerPurple2;
	public static EnemyPattern StrikerMissilesBlue;
	public static EnemyPattern StrikerMissilesRed;
	public static EnemyPattern StrikerMissilesPink;
	public static EnemyPattern HomingRed;
	public static EnemyPattern HomingPurple;
	public static EnemyPattern HomingBlue;
	public static EnemyPattern Homing2;
	public static EnemyPattern HomingRed2;
	public static EnemyPattern HomingPurple2;
	public static EnemyPattern HomingBlue2;
	public static EnemyPattern ShockWaveRed;
	public static EnemyPattern ShockWaveBlue;
	public static EnemyPattern ShockWavePink;
	public static EnemyPattern ShockWaveGreen;

	// Boomerang
	public static EnemyPattern BoomerangGreen;
	public static EnemyPattern BoomerangYellow;
	public static EnemyPattern BoomerangRed;
	public static EnemyPattern BoomerangBlue;
	public static EnemyPattern BoomerangPink;

	public static EnemyPattern BoomerangGreen2;
	public static EnemyPattern BoomerangYellow2;
	public static EnemyPattern BoomerangRed2;
	public static EnemyPattern BoomerangBlue2;
	public static EnemyPattern BoomerangPink2;

	public static EnemyPattern SwirlYellow;
	public static EnemyPattern SwirlRed;
	public static EnemyPattern SwirlGreen;
	public static EnemyPattern SwirlBlue;
	public static EnemyPattern SwirlPink;

	public static EnemyPattern SwirlYellow2;
	public static EnemyPattern SwirlRed2;
	public static EnemyPattern SwirlGreen2;
	public static EnemyPattern SwirlBlue2;
	public static EnemyPattern SwirlPink2;

	public static EnemyPattern Engage_Y;
	public static EnemyPattern Engage_R;
	public static EnemyPattern Engage_G;
	public static EnemyPattern Engage_B;
	public static EnemyPattern Engage_P;
	

	public static EnemyPattern TestEnemy;

	//Extreme Unit
	public static EnemyPattern ScatterSlowGreen;
	public static EnemyPattern ScatterSlowBlue;
	public static EnemyPattern ScatterSlowRed;
	public static EnemyPattern ScatterSlowPink;
	public static EnemyPattern DoubleSonarGreen;
	public static EnemyPattern DoubleSonarBlue;
	public static EnemyPattern DoubleSonarRed;
	public static EnemyPattern DoubleSonarPink;
	public static EnemyPattern SupremeMissilePink;
	public static EnemyPattern SupremeMissileRed;

	// Elite Units
	public static EnemyPattern Elite;
	public static EnemyPattern Elite_Easy_1;
	public static EnemyPattern Elite_Easy_2;
	public static EnemyPattern Elite_Easy_3;
	public static EnemyPattern Elite_Easy_4;
	public static EnemyPattern Elite_Easy_5;
	public static EnemyPattern Elite_Easy_6;
	public static EnemyPattern Elite_Easy_7;
	public static EnemyPattern Elite_Easy_8;
	public static EnemyPattern Elite_Easy_9;
	public static EnemyPattern Elite_Easy_10;

	//normal
	public static EnemyPattern Elite_GreenRanger;
	public static EnemyPattern Elite_BlueRanger;
	public static EnemyPattern Elite_RedRanger;
	public static EnemyPattern Elite_PinkRanger;
	public static EnemyPattern Elite_BeamerSweep;
	public static EnemyPattern Elite_BeamerRapid;
	public static EnemyPattern Elite_BeamerMissile1;
	public static EnemyPattern Elite_BeamerMissile2;
	public static EnemyPattern Elite_RedBlueDefender;
	public static EnemyPattern Elite_GreenBlueDefender;
	public static EnemyPattern Elite_PinkBlueDefender;
	public static EnemyPattern Elite_Speed;

	public static EnemyPattern Elite_GreenLaser;
	public static EnemyPattern Elite_BlueLaser;
	public static EnemyPattern Elite_RedLaser;
	public static EnemyPattern Elite_PinkLaser;
	public static EnemyPattern Elite_WaveGreen;
	public static EnemyPattern Elite_WaveBlue;
	public static EnemyPattern Elite_WaveRed;
	public static EnemyPattern Elite_WavePink;
	public static EnemyPattern Elite_Damage;
	public static EnemyPattern Elite_Striker;
	public static EnemyPattern Elite_RedStriker;
	public static EnemyPattern Elite_BlueStriker;
	public static EnemyPattern Elite_GreenCircle;
	public static EnemyPattern Elite_BlueCircle;
	public static EnemyPattern Elite_RedCircle;
	public static EnemyPattern Elite_PinkCircle;

	//Hard
	public static EnemyPattern Elite_GreenCircleMissile;
	public static EnemyPattern Elite_BlueCircleMissile;
	public static EnemyPattern Elite_RedCircleMissile;
	public static EnemyPattern Elite_PinkCircleMissile;
	public static EnemyPattern Elite_GreenCircleMissile2;
	public static EnemyPattern Elite_BlueCircleMissile2;
	public static EnemyPattern Elite_RedCircleMissile2;
	public static EnemyPattern Elite_PinkCircleMissile2;
	public static EnemyPattern Elite_GreenLaser2;
	public static EnemyPattern Elite_BlueLaser2;
	public static EnemyPattern Elite_RedLaser2;
	public static EnemyPattern Elite_PinkLaser2;
	public static EnemyPattern Elite_GreenRanger2;
	public static EnemyPattern Elite_BlueRanger2;
	public static EnemyPattern Elite_RedRanger2;
	public static EnemyPattern Elite_PinkRanger2;
	public static EnemyPattern Elite_BeamerStriker;
	public static EnemyPattern Elite_BeamerStriker2;
	public static EnemyPattern Elite_RedBlueDefender2;
	public static EnemyPattern Elite_GreenBlueDefender2;
	public static EnemyPattern Elite_PinkBlueDefender2;
	public static EnemyPattern Elite_WaveGreen2;
	public static EnemyPattern Elite_WaveBlue2;
	public static EnemyPattern Elite_WaveRed2;
	public static EnemyPattern Elite_WavePink2;

	//Extreme
	public static EnemyPattern Elite_Extreme1;
	public static EnemyPattern Elite_Extreme2;
	public static EnemyPattern Elite_Extreme3;
	public static EnemyPattern Elite_Extreme4;
	public static EnemyPattern Elite_Extreme5;
	public static EnemyPattern Elite_Extreme6;
	public static EnemyPattern Elite_Extreme7;
	public static EnemyPattern Elite_Extreme8;
	public static EnemyPattern Elite_Extreme9;
	public static EnemyPattern Elite_Extreme10;
	public static EnemyPattern Elite_Extreme11;
	public static EnemyPattern Elite_Extreme12;
	public static EnemyPattern Elite_Extreme13;
	public static EnemyPattern Elite_Extreme14;
	public static EnemyPattern Elite_Extreme15;
	public static EnemyPattern Elite_Extreme16;
	public static EnemyPattern Elite_Extreme17;
	public static EnemyPattern Elite_Extreme18;
	public static EnemyPattern Elite_Extreme19;
	public static EnemyPattern Elite_Extreme20;
	public static EnemyPattern Elite_Extreme21;
	public static EnemyPattern Elite_Extreme22;
	public static EnemyPattern Elite_Extreme23;
	public static EnemyPattern Elite_Extreme24;
	public static EnemyPattern Elite_Extreme25;


	private static List<EnemyPattern> allPatterns;
	private static List<EnemyPattern> allBalanced;
	private static List<EnemyPattern> allBulletHell;
	private static List<EnemyPattern> allAoE;
	private static List<EnemyPattern> allBeamHell;
	private static List<EnemyPattern> allHoming;

	public static List<EnemyPattern> allMine;
	private static List<EnemyPattern> allQuest_Bonus;
	private static List<EnemyPattern> allQuest_Boss;

	public static List<EnemyPattern> allElite;

	public static List<EnemyPattern> GetListForType (WaveController.Type type)
	{
		switch (type)
		{

		case WaveController.Type.BulletHell:
			return allBulletHell;

		case WaveController.Type.AoE:
			return allAoE;

		case WaveController.Type.BeamHell:
			return allBeamHell;

		case WaveController.Type.Homing:
			return allHoming;

		case WaveController.Type.Mines:
			return allMine;

		case WaveController.Type.Quest_Bonus:
			return allQuest_Bonus;

		case WaveController.Type.Quest_Boss:
			return allQuest_Boss;

		case WaveController.Type.Raid:
			return allBulletHell;

		default:
			return allBalanced;
		}
	}
	
	public static void Initialize ()	
	{

		allPatterns = new List<EnemyPattern>();
		allBalanced = new List<EnemyPattern>();
		allBulletHell = new List<EnemyPattern>();
		allAoE = new List<EnemyPattern>();
		allBeamHell = new List<EnemyPattern>();
		allHoming = new List<EnemyPattern>();
		allMine = new List<EnemyPattern>();
		allElite = new List<EnemyPattern>();

		allQuest_Bonus = new List<EnemyPattern>();
		allQuest_Boss = new List<EnemyPattern>();


		// Level 1 - 10 Standard Mobs

		Standard = new EnemyPattern(true, false, true, false, true, true, false);
		Standard.primaryWeapon = WeaponPattern.E_Slow;
		Standard.lifeMod = 0.5f;
		Standard.damageMod = 1.0f;
		Standard.image = Entity.Image.EnemySimple;
		Standard.dangerValue = 0.0f;
		Standard.minLevel = 0;
		Standard.maxLevel = int.MaxValue;
		Standard.name = "Standard";
		
		Standard2 = new EnemyPattern(false, false, false, false, false, false, true);
		Standard2.primaryWeapon = WeaponPattern.E_Slow2;
		Standard2.lifeMod = 0.5f;
		Standard2.damageMod = 1.0f;
		Standard2.image = Entity.Image.EnemySimple;
		Standard2.dangerValue = 0.0f;
		Standard2.minLevel = 0;
		Standard2.maxLevel = int.MaxValue;
		Standard2.name = "Standard2";
		
		StandardBH = new EnemyPattern(false, true);
		StandardBH.primaryWeapon = WeaponPattern.E_BH_Slow;
		StandardBH.lifeMod = 0.5f;
		StandardBH.damageMod = 1.0f;
		StandardBH.image = Entity.Image.EnemySimple;
		StandardBH.dangerValue = 0.0f;
		StandardBH.minLevel = 0;
		StandardBH.maxLevel = int.MaxValue;
		StandardBH.name = "StandardBH";
		
		Support = new EnemyPattern(true, false, false, false, false, false, false);
		Support.primaryWeapon = WeaponPattern.E_Support;
		Support.lifeMod = 0.5f;
		Support.damageMod = 1.0f;
		Support.image = Entity.Image.EnemySimple;
		Support.dangerValue = 0.0f;
		Support.minLevel = 0;
		
		Rapid1 = new EnemyPattern(true, false, true, false, true, true, true);
		Rapid1.primaryWeapon = WeaponPattern.E_Rapid;
		Rapid1.lifeMod = 1.0f;
		Rapid1.damageMod = 1.0f;
		Rapid1.image = Entity.Image.EnemySwift;
		Rapid1.dangerValue = 2.0f;
		Rapid1.minLevel = 0;
		Rapid1.maxLevel = babyLevel;
		Rapid1.name = "Rapid1";
		
		Rapid2 = new EnemyPattern(true, false, true, false, true, true, true);
		Rapid2.primaryWeapon = WeaponPattern.E_Rapid;
		Rapid2.lifeMod = 1.0f;
		Rapid2.damageMod = 1.0f;
		Rapid2.image = Entity.Image.EnemySwift;
		Rapid2.dangerValue = 3f;
		Rapid2.minLevel = babyLevel;
		Rapid2.maxLevel = int.MaxValue;
		Rapid2.name = "Rapid2";
		
		RapidBH = new EnemyPattern(false, true);
		RapidBH.primaryWeapon = WeaponPattern.E_BH_Rapid;
		RapidBH.lifeMod = 4.0f;
		RapidBH.damageMod = 3.0f;
		RapidBH.image = Entity.Image.RE_Kite;
		RapidBH.dangerValue = 3.0f;
		RapidBH.minLevel = babyLevel;
		RapidBH.maxLevel = int.MaxValue;
		RapidBH.name = "RapidBH";
		
		Sweeper = new EnemyPattern(true, false, true, false, true, true, true);
		Sweeper.primaryWeapon = WeaponPattern.E_Sweep;
		Sweeper.lifeMod = 1.0f;
		Sweeper.damageMod = 1.0f;
		Sweeper.image = Entity.Image.EnemyStandard;
		Sweeper.dangerValue = 2.0f;
		Sweeper.minLevel = maxEasyLevel;
		Sweeper.maxLevel = int.MaxValue;
		Sweeper.name = "Sweeper";

		SweepBlue = new EnemyPattern(true, false, true, false, true, true, true);
		SweepBlue.primaryWeapon = WeaponPattern.E_SweepBlue;
		SweepBlue.lifeMod = 1.0f;
		SweepBlue.damageMod = 0.75f;
		SweepBlue.image = Entity.Image.EnemyStandard;
		SweepBlue.dangerValue = 2.0f;
		SweepBlue.minLevel = easyLevel;
		SweepBlue.maxLevel = int.MaxValue;
		SweepBlue.name = "SweepBlue";
		SweepBlue.unitcolor = UnitColor.Blue;
		
		Spread1 = new EnemyPattern(true, false, false, false, false, false, true);
		Spread1.primaryWeapon = WeaponPattern.E_Spread;
		Spread1.lifeMod = 1.0f;
		Spread1.damageMod = 1.0f;
		Spread1.image = Entity.Image.EnemyStandard;
		Spread1.dangerValue = 1.5f;
		Spread1.minLevel = 0;
		Spread1.minLevel = babyLevel;
		Spread1.name = "Spread1";
		
		Spread2 = new EnemyPattern(true, false, false, false, false, false, true);
		Spread2.primaryWeapon = WeaponPattern.E_Spread;
		Spread2.lifeMod = 1.0f;
		Spread2.damageMod = 1.0f;
		Spread2.image = Entity.Image.EnemyStandard;
		Spread2.dangerValue = 2.5f;
		Spread2.minLevel = babyLevel;
		Spread2.maxLevel = int.MaxValue;
		Spread2.name = "Spread2";

		SpreadBlue = new EnemyPattern(true, false, false, false, false, false, true);
		SpreadBlue.primaryWeapon = WeaponPattern.E_SpreadBlue;
		SpreadBlue.lifeMod = 1.0f;
		SpreadBlue.damageMod = 0.75f;
		SpreadBlue.image = Entity.Image.EnemyStandard;
		SpreadBlue.dangerValue = 2.5f;
		SpreadBlue.minLevel = easyLevel;
		SpreadBlue.maxLevel = int.MaxValue;
		SpreadBlue.name = "SpreadBlue";
		SpreadBlue.unitcolor = UnitColor.Blue;
		
		Scatter1 = new EnemyPattern(true, false, false, false, false, false, true);
		Scatter1.primaryWeapon = WeaponPattern.E_Scatter;
		Scatter1.lifeMod = 1.0f;
		Scatter1.damageMod = 1.0f;
		Scatter1.image = Entity.Image.EnemyStandard;
		Scatter1.dangerValue = 1.0f;
		Scatter1.minLevel = 0;
		Scatter1.maxLevel = maxNormalLevel;
		Scatter1.name = "Scatter1";
		
		Scatter2 = new EnemyPattern(false, true);
		Scatter2.primaryWeapon = WeaponPattern.E_BH_Scatter;
		Scatter2.lifeMod = 0.7f;
		Scatter2.damageMod = 1.0f;
		Scatter2.image = Entity.Image.RE_Kite;
		Scatter2.dangerValue = 2.5f;
		Scatter2.minLevel = 0;
		Scatter2.maxLevel = int.MaxValue;
		Scatter2.name = "Scatter2";
		
		Homing1 = new EnemyPattern(true, false, false, false, false, false, true);
		Homing1.primaryWeapon = WeaponPattern.E_Homing;
		Homing1.lifeMod = 1.5f;
		Homing1.damageMod = 2.0f;
		Homing1.image = Entity.Image.EnemyStriker;
		Homing1.dangerValue = 2.5f;
		Homing1.minLevel = babyLevel;
		Homing1.maxLevel = int.MaxValue;
		Homing1.name = "Homing1";

		HomingBlue = new EnemyPattern(true, false, false, false, false, false, true);
		HomingBlue.primaryWeapon = WeaponPattern.E_HomingBlue;
		HomingBlue.lifeMod = 1.5f;
		HomingBlue.damageMod = 2.00f;
		HomingBlue.image = Entity.Image.EnemyStriker;
		HomingBlue.dangerValue = 2.5f;
		HomingBlue.minLevel = easyLevel;
		HomingBlue.maxLevel = int.MaxValue;
		HomingBlue.name = "HomingBlue";
		HomingBlue.unitcolor = UnitColor.Blue;
		
		HomingRed = new EnemyPattern(true, false, false, false, false, false, true);
		HomingRed.primaryWeapon = WeaponPattern.E_HomingRed;
		HomingRed.lifeMod = 1.5f;
		HomingRed.damageMod = 0.5f;
		HomingRed.image = Entity.Image.EnemyStriker;
		HomingRed.dangerValue = 3.0f;
		HomingRed.minLevel = mediumLevel;
		HomingRed.maxLevel = int.MaxValue;
		HomingRed.name = "HomingRed";
		HomingRed.unitcolor = UnitColor.Red;
		
		HomingPurple = new EnemyPattern(true, false, false, false, false, false, true);
		HomingPurple.primaryWeapon = WeaponPattern.E_HomingPurple;
		HomingPurple.lifeMod = 1.5f;
		HomingPurple.damageMod = 2.75f;
		HomingPurple.image = Entity.Image.RE_Frigate;
		HomingPurple.dangerValue = 3.0f;
		HomingPurple.minLevel = mediumLevel;
		HomingPurple.maxLevel = int.MaxValue;
		HomingPurple.name = "HomingPurple";
		HomingPurple.unitcolor = UnitColor.Red;

		Homing2 = new EnemyPattern(true, false, false, false, false, false, true);
		Homing2.primaryWeapon = WeaponPattern.E_Homing2;
		Homing2.lifeMod = 1.5f;
		Homing2.damageMod = 1.5f;
		Homing2.image = Entity.Image.EnemyStriker;
		Homing2.dangerValue = 2.5f;
		Homing2.minLevel = babyLevel;
		Homing2.maxLevel = int.MaxValue;
		Homing2.name = "Homing2";
		Homing2.isFaceToPlayer = true;

		HomingRed2 = new EnemyPattern(true, false, false, false, false, false, true);
		HomingRed2.primaryWeapon = WeaponPattern.E_HomingRed2;
		HomingRed2.lifeMod = 1.5f;
		HomingRed2.damageMod = 0.5f;
		HomingRed2.image = Entity.Image.EnemyStriker;
		HomingRed2.dangerValue = 3.0f;
		HomingRed2.minLevel = hardLevel;
		HomingRed2.maxLevel = int.MaxValue;
		HomingRed2.name = "HomingRed2";
		HomingRed2.isFaceToPlayer = true;
		HomingRed2.isSingleUnit = true;
		HomingRed2.unitcolor = UnitColor.Red;

		HomingBlue2 = new EnemyPattern(true, false, false, false, false, false, true);
		HomingBlue2.primaryWeapon = WeaponPattern.E_HomingBlue2;
		HomingBlue2.lifeMod = 1.5f;
		HomingBlue2.damageMod = 1.5f;
		HomingBlue2.image = Entity.Image.EnemyStriker;
		HomingBlue2.dangerValue = 3.0f;
		HomingBlue2.minLevel = hardLevel;
		HomingBlue2.maxLevel = int.MaxValue;
		HomingBlue2.name = "HomingBlue2";
		HomingBlue2.isFaceToPlayer = true;
		HomingBlue2.isSingleUnit = true;
		HomingBlue2.unitcolor = UnitColor.Blue;

		HomingPurple2 = new EnemyPattern(true, false, false, false, false, false, true);
		HomingPurple2.primaryWeapon = WeaponPattern.E_HomingPurple2;
		HomingPurple2.lifeMod = 1.5f;
		HomingPurple2.damageMod = 2.25f;
		HomingPurple2.image = Entity.Image.RE_Frigate;
		HomingPurple2.dangerValue = 3.5f;
		HomingPurple2.minLevel = hellLevel;
		HomingPurple2.maxLevel = int.MaxValue;
		HomingPurple2.name = "HomingPurple2";
		HomingPurple2.isFaceToPlayer = true;
		HomingPurple2.isSingleUnit = true;
		HomingPurple2.unitcolor = UnitColor.Pink;
		
		HomingFast = new EnemyPattern(false, false, false, false, false, true);
		HomingFast.primaryWeapon = WeaponPattern.E_FastHoming;
		HomingFast.lifeMod = 1.5f;
		HomingFast.damageMod = 1.0f;
		HomingFast.image = Entity.Image.EnemyStriker;
		HomingFast.dangerValue = 2.5f;
		HomingFast.minLevel = babyLevel;
		HomingFast.maxLevel = int.MaxValue;
		HomingFast.name = "Homing2";
		
		Striker = new EnemyPattern(true, false, true, false, false, true, false);
		Striker.primaryWeapon = WeaponPattern.E_StrikerMissiles;
		Striker.lifeMod = 1.5f;
		Striker.damageMod = 2.0f;
		Striker.image = Entity.Image.EnemyStriker;
		Striker.dangerValue = 4.0f;
		Striker.minLevel = easyLevel;
		Striker.maxLevel = int.MaxValue;
		Striker.name = "Striker";
		Striker.isFaceToPlayer = true;

		StrikerMissilesBlue = new EnemyPattern(true, false, false, false, false, true, false);
		StrikerMissilesBlue.primaryWeapon = WeaponPattern.E_StrikerMissilesBlue;
		StrikerMissilesBlue.lifeMod = 1.5f;
		StrikerMissilesBlue.damageMod = 1.5f;
		StrikerMissilesBlue.image = Entity.Image.EnemyStriker;
		StrikerMissilesBlue.dangerValue = 4.0f;
		StrikerMissilesBlue.minLevel = hardLevel;
		StrikerMissilesBlue.maxLevel = int.MaxValue;
		StrikerMissilesBlue.name = "StrikerMissilesBlue";
		StrikerMissilesBlue.isFaceToPlayer = true;
		StrikerMissilesBlue.unitcolor = UnitColor.Blue;

		StrikerMissilesRed = new EnemyPattern(true, false, false, false, false, true, false);
		StrikerMissilesRed.primaryWeapon = WeaponPattern.E_StrikerMissilesRed;
		StrikerMissilesRed.lifeMod = 1.5f;
		StrikerMissilesRed.damageMod = 0.35f;
		StrikerMissilesRed.image = Entity.Image.EnemyStriker;
		StrikerMissilesRed.dangerValue = 4.0f;
		StrikerMissilesRed.minLevel = hellLevel;
		StrikerMissilesRed.maxLevel = int.MaxValue;
		StrikerMissilesRed.name = "StrikerMissilesRed";
		StrikerMissilesRed.isFaceToPlayer = true;
		StrikerMissilesRed.unitcolor = UnitColor.Red;

		StrikerMissilesPink = new EnemyPattern(true, true, false, false, false, true, false);
		StrikerMissilesPink.primaryWeapon = WeaponPattern.E_StrikerMissilesPink;
		StrikerMissilesPink.lifeMod = 1.5f;
		StrikerMissilesPink.damageMod = 2.0f;
		StrikerMissilesPink.image = Entity.Image.RE_Frigate;
		StrikerMissilesPink.dangerValue = 4.0f;
		StrikerMissilesPink.minLevel = hellLevel;
		StrikerMissilesPink.maxLevel = int.MaxValue;
		StrikerMissilesPink.name = "StrikerMissilesPink";
		StrikerMissilesPink.isFaceToPlayer = true;
		StrikerMissilesPink.unitcolor = UnitColor.Pink;

		ShockWaveRed = new EnemyPattern(true, false, false, false, false, true, false);
		ShockWaveRed.primaryWeapon = WeaponPattern.E_SonarRed;
		ShockWaveRed.lifeMod = 2.5f;
		ShockWaveRed.damageMod = 1.5f;
		ShockWaveRed.image = Entity.Image.EnemyFrigate;
		ShockWaveRed.dangerValue = 4.0f;
		ShockWaveRed.minLevel = extremeLevel;
		ShockWaveRed.maxLevel = int.MaxValue;
		ShockWaveRed.name = "ShockWaveRed";
		ShockWaveRed.isFaceToPlayer = true;
		ShockWaveRed.unitcolor = UnitColor.Red;

		ShockWaveBlue = new EnemyPattern(true, false, false, false, false, true, false);
		ShockWaveBlue.primaryWeapon = WeaponPattern.E_SonarBlue;
		ShockWaveBlue.lifeMod = 2.5f;
		ShockWaveBlue.damageMod = 1.0f;
		ShockWaveBlue.image = Entity.Image.EnemyFrigate;
		ShockWaveBlue.dangerValue = 4.0f;
		ShockWaveBlue.minLevel = extremeLevel;
		ShockWaveBlue.maxLevel = int.MaxValue;
		ShockWaveBlue.name = "ShockWaveBlue";
		ShockWaveBlue.isFaceToPlayer = true;
		ShockWaveBlue.unitcolor = UnitColor.Blue;
	
		ShockWavePink = new EnemyPattern(true, true, false, false, false, true, false);
		ShockWavePink.primaryWeapon = WeaponPattern.E_SonarPink;
		ShockWavePink.lifeMod = 2.5f;
		ShockWavePink.damageMod = 1.8f;
		ShockWavePink.image = Entity.Image.RE_Frigate;
		ShockWavePink.dangerValue = 4.0f;
		ShockWavePink.minLevel = ultimateLevel;
		ShockWavePink.maxLevel = int.MaxValue;
		ShockWavePink.name = "ShockWavePink";
		ShockWavePink.isFaceToPlayer = true;
		ShockWavePink.unitcolor = UnitColor.Pink;

		ShockWaveGreen = new EnemyPattern(true, false, false, false, false, true, false);
		ShockWaveGreen.primaryWeapon = WeaponPattern.E_SonarGreen;
		ShockWaveGreen.lifeMod = 2.5f;
		ShockWaveGreen.damageMod = 1.8f;
		ShockWaveGreen.image = Entity.Image.EnemyFrigate;
		ShockWaveGreen.dangerValue = 4.0f;
		ShockWaveGreen.minLevel = hardLevel;
		ShockWaveGreen.maxLevel = int.MaxValue;
		ShockWaveGreen.name = "ShockWavePink";
		ShockWaveGreen.isFaceToPlayer = true;
		ShockWaveGreen.unitcolor = UnitColor.Pink;

		DoubleSonarGreen = new EnemyPattern(true, false, false, false, false, true, false);
		DoubleSonarGreen.primaryWeapon = WeaponPattern.E_DoubleSonarGreen;
		DoubleSonarGreen.lifeMod = 2.5f;
		DoubleSonarGreen.damageMod = 1.8f;
		DoubleSonarGreen.image = Entity.Image.EnemyKite;
		DoubleSonarGreen.dangerValue = 4.0f;
		DoubleSonarGreen.minLevel = ultimateLevel;
		DoubleSonarGreen.maxLevel = int.MaxValue;
		DoubleSonarGreen.name = "ShockWavePink";
		DoubleSonarGreen.isFaceToPlayer = true;
		DoubleSonarGreen.unitcolor = UnitColor.Green;

		DoubleSonarBlue = new EnemyPattern(true, false, false, false, false, true, false);
		DoubleSonarBlue.primaryWeapon = WeaponPattern.E_DoubleSonarBlue;
		DoubleSonarBlue.lifeMod = 2.5f;
		DoubleSonarBlue.damageMod = 1.8f;
		DoubleSonarBlue.image = Entity.Image.EnemyKite;
		DoubleSonarBlue.dangerValue = 4.0f;
		DoubleSonarBlue.minLevel = ultimateLevel;
		DoubleSonarBlue.maxLevel = int.MaxValue;
		DoubleSonarBlue.name = "DoubleSonarBlue";
		DoubleSonarBlue.isFaceToPlayer = true;
		DoubleSonarBlue.unitcolor = UnitColor.Blue;

		DoubleSonarRed = new EnemyPattern(true, false, false, false, false, true, false);
		DoubleSonarRed.primaryWeapon = WeaponPattern.E_DoubleSonarRed;
		DoubleSonarRed.lifeMod = 2.5f;
		DoubleSonarRed.damageMod = 1.8f;
		DoubleSonarRed.image = Entity.Image.EnemyKite;
		DoubleSonarRed.dangerValue = 4.0f;
		DoubleSonarRed.minLevel = ultimateLevel;
		DoubleSonarRed.maxLevel = int.MaxValue;
		DoubleSonarRed.name = "DoubleSonarRed";
		DoubleSonarRed.isFaceToPlayer = true;
		DoubleSonarRed.unitcolor = UnitColor.Red;

		DoubleSonarPink = new EnemyPattern(true, false, false, false, false, true, false);
		DoubleSonarPink.primaryWeapon = WeaponPattern.E_DoubleSonarPink;
		DoubleSonarPink.lifeMod = 2.5f;
		DoubleSonarPink.damageMod = 1.8f;
		DoubleSonarPink.image = Entity.Image.RE_Frigate;
		DoubleSonarPink.dangerValue = 4.0f;
		DoubleSonarPink.minLevel = ultimateLevel;
		DoubleSonarPink.maxLevel = int.MaxValue;
		DoubleSonarPink.name = "DoubleSonarPink";
		DoubleSonarPink.isFaceToPlayer = true;
		DoubleSonarPink.unitcolor = UnitColor.Pink;

		ScatterSlowGreen = new EnemyPattern(true, false, true, false, false, true, false);
		ScatterSlowGreen.primaryWeapon = WeaponPattern.E_ScatterSlowGreen;
		ScatterSlowGreen.lifeMod = 2.5f;
		ScatterSlowGreen.damageMod = 1.0f;
		ScatterSlowGreen.image = Entity.Image.EnemyKite;
		ScatterSlowGreen.dangerValue = 4.0f;
		ScatterSlowGreen.minLevel = ultimateLevel;
		ScatterSlowGreen.maxLevel = int.MaxValue;
		ScatterSlowGreen.name = "ScatterSlowGreen";
		ScatterSlowGreen.unitcolor = UnitColor.Red;
		ScatterSlowGreen.moduleId = 207;
		ScatterSlowGreen.isFaceToPlayer = true;
		ScatterSlowGreen.isSingleUnit = true;

		ScatterSlowBlue = new EnemyPattern(true, false, true, false, false, true, false);
		ScatterSlowBlue.primaryWeapon = WeaponPattern.E_ScatterSlowBlue;
		ScatterSlowBlue.lifeMod = 2.5f;
		ScatterSlowBlue.damageMod = 1.0f;
		ScatterSlowBlue.image = Entity.Image.EnemyKite;
		ScatterSlowBlue.dangerValue = 4.0f;
		ScatterSlowBlue.minLevel = ultimateLevel;
		ScatterSlowBlue.maxLevel = int.MaxValue;
		ScatterSlowBlue.name = "ScatterSlowBlue";
		ScatterSlowBlue.unitcolor = UnitColor.Red;
		ScatterSlowBlue.moduleId = 209;
		ScatterSlowBlue.isFaceToPlayer = true;
		ScatterSlowBlue.isSingleUnit = true;

		ScatterSlowRed = new EnemyPattern(true, false, true, false, false, true, false);
		ScatterSlowRed.primaryWeapon = WeaponPattern.E_ScatterSlowRed;
		ScatterSlowRed.lifeMod = 2.5f;
		ScatterSlowRed.damageMod = 1.0f;
		ScatterSlowRed.image = Entity.Image.EnemyKite;
		ScatterSlowRed.dangerValue = 4.0f;
		ScatterSlowRed.minLevel = ultimateLevel;
		ScatterSlowRed.maxLevel = int.MaxValue;
		ScatterSlowRed.name = "ScatterSlowRed";
		ScatterSlowRed.unitcolor = UnitColor.Red;
		ScatterSlowRed.moduleId = 206;
		ScatterSlowRed.isFaceToPlayer = true;
		ScatterSlowRed.isSingleUnit = true;

		ScatterSlowPink = new EnemyPattern(true, true, true, false, false, true, false);
		ScatterSlowPink.primaryWeapon = WeaponPattern.E_ScatterSlowPink;
		ScatterSlowPink.lifeMod = 2.5f;
		ScatterSlowPink.damageMod = 1.8f;
		ScatterSlowPink.image = Entity.Image.RE_Kite;
		ScatterSlowPink.dangerValue = 4.0f;
		ScatterSlowPink.minLevel = ultimateLevel;
		ScatterSlowPink.maxLevel = int.MaxValue;
		ScatterSlowPink.name = "ScatterSlowPink";
		ScatterSlowPink.unitcolor = UnitColor.Pink;
		ScatterSlowPink.moduleId = 205;
		ScatterSlowPink.isFaceToPlayer = true;
		ScatterSlowPink.isSingleUnit = true;
		
//		public static EnemyPattern ;
//		public static EnemyPattern ;
//		public static EnemyPattern DouleSonarRed;
//		public static EnemyPattern DouleSonarPink;
//		public static EnemyPattern SupremeMissilePink;
//		public static EnemyPattern SupremeMissileRed;


		Stunner = new EnemyPattern(true, false, false, false, false, false, true);
		Stunner.primaryWeapon = WeaponPattern.E_BlueStun;
		Stunner.lifeMod = 2.5f;
		Stunner.damageMod = 2.0f;
		Stunner.image = Entity.Image.EnemySwift;
		Stunner.dangerValue = 3f;
		Stunner.minLevel = easyLevel;
		Stunner.maxLevel = int.MaxValue;
		Stunner.name = "Stunner";
		
		Beamer = new EnemyPattern(true, false, false, false, false, false, true);
		Beamer.primaryWeapon = WeaponPattern.E_Beam;
		Beamer.lifeMod = 4.5f;
		Beamer.damageMod = 2.0f;
		Beamer.image = Entity.Image.EnemyBeamer;
		Beamer.dangerValue = 3.5f;
		Beamer.minLevel = easyLevel;
		Beamer.maxLevel = int.MaxValue;
		Beamer.name = "Beamer";
		Beamer.moduleId = 210;
		Beamer.isSingleUnit = true;
		
		Beamer2 = new EnemyPattern(false, false, true, false, false);
		Beamer2.primaryWeapon = WeaponPattern.E_Beam;
		Beamer2.lifeMod = 4.0f;
		Beamer2.damageMod = 1.5f;
		Beamer2.image = Entity.Image.EnemyBeamer;
		Beamer2.dangerValue = 3.5f;
		Beamer2.minLevel = 0;
		Beamer2.maxLevel = int.MaxValue;
		Beamer2.name = "Beamer2";
		Beamer2.moduleId = 210;
		Beamer2.isSingleUnit = true;
		
		Beamerstun = new EnemyPattern(true, false, false, false, true);
		Beamerstun.primaryWeapon = WeaponPattern.E_BeamStun;
		Beamerstun.lifeMod = 4.5f;
		Beamerstun.damageMod = 2.0f;
		Beamerstun.image = Entity.Image.GN_Beamer;
		Beamerstun.dangerValue = 3.5f;
		Beamerstun.minLevel = easyLevel;
		Beamerstun.maxLevel = int.MaxValue;
		Beamerstun.name = "Beamerstun";
		Beamerstun.moduleId = 209;
		Beamerstun.isSingleUnit = true;
		
		Beamerburn = new EnemyPattern(true, false, false, false, true);
		Beamerburn.primaryWeapon = WeaponPattern.E_BeamBurn;
		Beamerburn.lifeMod = 4.5f;
		Beamerburn.damageMod = 0.25f;
		Beamerburn.image = Entity.Image.RE_Beamer;
		Beamerburn.dangerValue = 3.5f;
		Beamerburn.minLevel = mediumLevel;
		Beamerburn.maxLevel = int.MaxValue;
		Beamerburn.name = "Beamerburn";
		Beamerburn.moduleId = 206;
		Beamerburn.isSingleUnit = true;
		
		Beamerhell  = new EnemyPattern(false, false, false, false, true);
		Beamerhell.primaryWeapon = WeaponPattern.E_BeamHell;
		Beamerhell.lifeMod = 3.5f;
		Beamerhell.damageMod = 3.0f;
		Beamerhell.image = Entity.Image.RE_Beamer;
		Beamerhell.dangerValue = 2.0f;
		Beamerhell.minLevel = 0;
		Beamerhell.maxLevel = int.MaxValue;
		Beamerhell.name = "Beamerhell";
		Beamerhell.moduleId = 205;
		Beamerhell.isSingleUnit = true;
		
		Chainer1 = new EnemyPattern(true, false, false, false, true, false, true);
		Chainer1.primaryWeapon = WeaponPattern.E_Chainer;
		Chainer1.lifeMod = 1.0f;
		Chainer1.damageMod = 1.5f;
		Chainer1.image = Entity.Image.EnemyRound;
		Chainer1.dangerValue = 2.0f;
		Chainer1.minLevel = mediumLevel;
		Chainer1.maxLevel = int.MaxValue;
		Chainer1.name = "Chainer1";
		
		Chainer2 = new EnemyPattern(true, false, false, false, true, true);
		Chainer2.primaryWeapon = WeaponPattern.E_Chainer;
		Chainer2.lifeMod = 1.2f;
		Chainer2.damageMod = 2.0f;
		Chainer2.image = Entity.Image.EnemyRound;
		Chainer2.dangerValue = 3.0f;
		Chainer2.minLevel = mediumLevel;
		Chainer2.maxLevel = int.MaxValue;
		Chainer2.name = "Chainer2";

		ChainerBlue2 = new EnemyPattern(true, false, false, false, true, true);
		ChainerBlue2.primaryWeapon = WeaponPattern.E_ChainerBlue2;
		ChainerBlue2.lifeMod = 1.2f;
		ChainerBlue2.damageMod = 1.5f;
		ChainerBlue2.image = Entity.Image.EnemyRound;
		ChainerBlue2.dangerValue = 3.5f;
		ChainerBlue2.minLevel = mediumLevel;
		ChainerBlue2.maxLevel = int.MaxValue;
		ChainerBlue2.name = "ChainerBlue2";
		ChainerBlue2.unitcolor = UnitColor.Blue;
		ChainerBlue2.isSingleUnit = true;
		ChainerBlue2.isFaceToPlayer = true;

		ChainerRed = new EnemyPattern(true, false, false, false, false, true);
		ChainerRed.primaryWeapon = WeaponPattern.E_ChainerRed;
		ChainerRed.lifeMod = 1.2f;
		ChainerRed.damageMod = 0.5f;
		ChainerRed.image = Entity.Image.EnemyRound;
		ChainerRed.dangerValue = 4f;
		ChainerRed.minLevel = hardLevel;
		ChainerRed.maxLevel = int.MaxValue;
		ChainerRed.name = "ChainerRed";
		ChainerRed.unitcolor = UnitColor.Red;

		ChainerRed2 = new EnemyPattern(true, false, false, false, true, true);
		ChainerRed2.primaryWeapon = WeaponPattern.E_ChainerRed2;
		ChainerRed2.lifeMod = 1.2f;
		ChainerRed2.damageMod = 0.35f;
		ChainerRed2.image = Entity.Image.EnemyRound;
		ChainerRed2.dangerValue = 4f;
		ChainerRed2.minLevel = hellLevel;
		ChainerRed2.maxLevel = int.MaxValue;
		ChainerRed2.name = "ChainerRed2";
		ChainerRed2.unitcolor = UnitColor.Red;
		ChainerRed2.isSingleUnit = true;
		ChainerRed2.isFaceToPlayer = true;

		ChainerPurple = new EnemyPattern(true, false, false, false, false, true);
		ChainerPurple.primaryWeapon = WeaponPattern.E_ChainerPurple;
		ChainerPurple.lifeMod = 1.2f;
		ChainerPurple.damageMod = 3.0f;
		ChainerPurple.image = Entity.Image.RE_Stingray;
		ChainerPurple.dangerValue = 4f;
		ChainerPurple.minLevel = hellLevel;
		ChainerPurple.maxLevel = int.MaxValue;
		ChainerPurple.name = "ChainerPurple";
		ChainerPurple.unitcolor = UnitColor.Pink;

		ChainerPurple2 = new EnemyPattern(true, false, false, false, true, true);
		ChainerPurple2.primaryWeapon = WeaponPattern.E_ChainerPurple2;
		ChainerPurple2.lifeMod = 1.2f;
		ChainerPurple2.damageMod = 3.5f;
		ChainerPurple2.image = Entity.Image.RE_Stingray;
		ChainerPurple2.dangerValue = 4f;
		ChainerPurple2.minLevel = extremeLevel;
		ChainerPurple2.maxLevel = int.MaxValue;
		ChainerPurple2.name = "ChainerPurple2";
		ChainerPurple2.unitcolor = UnitColor.Pink;
		ChainerPurple2.isSingleUnit = true;
		ChainerPurple2.isFaceToPlayer = true;
		
		BlackHole = new EnemyPattern(false, false, true);
		BlackHole.primaryWeapon = WeaponPattern.E_BlackHole;
		BlackHole.lifeMod = 3.0f;
		BlackHole.damageMod = 2.0f;
		BlackHole.image = Entity.Image.RE_Kite;
		BlackHole.dangerValue = 0.0f;
		BlackHole.minLevel = mediumLevel;
		BlackHole.maxLevel = int.MaxValue;
		BlackHole.name = "BlackHole";
		BlackHole.isSingleUnit = true;
		
		Laser1 = new EnemyPattern(true, false, false, false, true);
		Laser1.primaryWeapon = WeaponPattern.E_LaserGreen;
		Laser1.lifeMod = 4.5f;
		Laser1.damageMod = 1.0f;
		Laser1.image = Entity.Image.EnemyLaser;
		Laser1.dangerValue = 2.0f;
		Laser1.minLevel = mediumLevel;
		Laser1.maxLevel = int.MaxValue;
		Laser1.name = "Laser1";
		Laser1.isFaceToPlayer = true;
		
		Laser2 = new EnemyPattern(true, false, false, false, true);
		Laser2.primaryWeapon = WeaponPattern.E_LaserBlue;
		Laser2.lifeMod = 4.5f;
		Laser2.damageMod = 1.0f;
		Laser2.image = Entity.Image.EnemyLaser;
		Laser2.dangerValue = 3.5f;
		Laser2.minLevel = hardLevel;
		Laser2.maxLevel = int.MaxValue;
		Laser2.name = "Laser2";
		Laser2.isFaceToPlayer = true;
		
		Laser3 = new EnemyPattern(true, false, false, false, true);
		Laser3.primaryWeapon = WeaponPattern.E_LaserRed;
		Laser3.lifeMod = 4.5f;
		Laser3.damageMod = 0.5f;
		Laser3.image = Entity.Image.EnemyLaser;
		Laser3.dangerValue = 4.0f;
		Laser3.minLevel = hardLevel + 1;
		Laser3.maxLevel = int.MaxValue;
		Laser3.name = "Laser3";
		Laser3.isFaceToPlayer = true;
		
		Laser4 = new EnemyPattern(true, true, false, false, true);
		Laser4.primaryWeapon = WeaponPattern.E_LaserPink;
		Laser4.lifeMod = 4.5f;
		Laser4.damageMod = 2.0f;
		Laser4.image = Entity.Image.RE_Kite;
		Laser4.dangerValue = 4.0f;
		Laser4.minLevel = hardLevel + 2;
		Laser4.maxLevel = int.MaxValue;
		Laser4.name = "Laser4";
		Laser4.isFaceToPlayer = true;
		
		LaserCircleGreen = new EnemyPattern(true, false, true, false, true, true, true);
		LaserCircleGreen.primaryWeapon = WeaponPattern.E_LaserCircleGreen;
		LaserCircleGreen.lifeMod = 6.5f;
		LaserCircleGreen.damageMod = 2.0f;
		LaserCircleGreen.image = Entity.Image.EnemyOrbit;
		LaserCircleGreen.dangerValue = 4f;
		LaserCircleGreen.minLevel = easyLevel;
		LaserCircleGreen.maxLevel = int.MaxValue;
		LaserCircleGreen.name = "LaserCircleGreen";
		LaserCircleGreen.moduleId = 207;
		LaserCircleGreen.isSingleUnit = true;
		
		LaserCirclePink = new EnemyPattern(false, true, true, false, false);
		LaserCirclePink.primaryWeapon = WeaponPattern.E_LaserCirclePink;
		LaserCirclePink.lifeMod = 6.5f;
		LaserCirclePink.damageMod = 3.5f;
		LaserCirclePink.image = Entity.Image.RE_Diamond;
		LaserCirclePink.dangerValue = 4f;
		LaserCirclePink.minLevel = hardLevel;
		LaserCirclePink.maxLevel = int.MaxValue;
		LaserCirclePink.name = "LaserCirclePink";
		LaserCirclePink.moduleId = 205;
		LaserCirclePink.isSingleUnit = true;
		LaserCirclePink.unitcolor = UnitColor.Pink;

		LaserCircleBlue = new EnemyPattern(true, false, true, false, true, true, true);
		LaserCircleBlue.primaryWeapon = WeaponPattern.E_LaserCircleBlue;
		LaserCircleBlue.lifeMod = 6.5f;
		LaserCircleBlue.damageMod = 1.25f;
		LaserCircleBlue.image = Entity.Image.GN_Diamond;
		LaserCircleBlue.dangerValue = 4f;
		LaserCircleBlue.minLevel = mediumLevel;
		LaserCircleBlue.maxLevel = int.MaxValue;
		LaserCircleBlue.name = "LaserCircleBlue";
		LaserCircleBlue.moduleId = 209;
		LaserCircleBlue.isSingleUnit = true;
		LaserCircleBlue.unitcolor = UnitColor.Blue;

		LaserCircleRed = new EnemyPattern(true, false, true, false, true, true, true);
		LaserCircleRed.primaryWeapon = WeaponPattern.E_LaserCircleRed;
		LaserCircleRed.lifeMod = 6.5f;
		LaserCircleRed.damageMod = 0.75f;
		LaserCircleRed.image = Entity.Image.GN_Diamond;
		LaserCircleRed.dangerValue = 4f;
		LaserCircleRed.minLevel = hellLevel;
		LaserCircleRed.maxLevel = int.MaxValue;
		LaserCircleRed.name = "LaserCircleRed";
		LaserCircleRed.moduleId = 206;
		LaserCircleRed.isSingleUnit = true;
		LaserCircleRed.unitcolor = UnitColor.Red;

		BoomerangYellow = new EnemyPattern(true, false, true, false, false, true, true);
		BoomerangYellow.primaryWeapon = WeaponPattern.E_BoomerangYellow;
		BoomerangYellow.lifeMod = 4.5f;
		BoomerangYellow.damageMod = 1.25f;
		BoomerangYellow.image = Entity.Image.EnemyStingray;
		BoomerangYellow.dangerValue = 3f;
		BoomerangYellow.minLevel = babyLevel;
		BoomerangYellow.maxLevel = int.MaxValue;
		BoomerangYellow.name = "BoomerangYellow";
		BoomerangYellow.moduleId = 208;
		BoomerangYellow.isFaceToPlayer = true;
		BoomerangYellow.isSingleUnit = true;
		BoomerangYellow.unitcolor = UnitColor.Yellow;

		BoomerangGreen = new EnemyPattern(true, false, true, false, false, true, true);
		BoomerangGreen.primaryWeapon = WeaponPattern.E_BoomerangGreen;
		BoomerangGreen.lifeMod = 4.5f;
		BoomerangGreen.damageMod = 1.25f;
		BoomerangGreen.image = Entity.Image.EnemyStingray;
		BoomerangGreen.dangerValue = 3f;
		BoomerangGreen.minLevel = easyLevel;
		BoomerangGreen.maxLevel = int.MaxValue;
		BoomerangGreen.name = "BoomerangGreen";
		BoomerangGreen.isFaceToPlayer = true;
		BoomerangGreen.isSingleUnit = true;
		BoomerangGreen.unitcolor = UnitColor.Green;

		BoomerangRed = new EnemyPattern(true, false, true, false, false, true, true);
		BoomerangRed.primaryWeapon = WeaponPattern.E_BoomerangRed;
		BoomerangRed.lifeMod = 4.5f;
		BoomerangRed.damageMod = 1.25f;
		BoomerangRed.image = Entity.Image.GN_Stingray;
		BoomerangRed.dangerValue = 3f;
		BoomerangRed.minLevel = easyLevel;
		BoomerangRed.maxLevel = int.MaxValue;
		BoomerangRed.name = "BoomerangRed";
		BoomerangRed.isFaceToPlayer = true;
		BoomerangRed.isSingleUnit = true;
		BoomerangRed.unitcolor = UnitColor.Red;

		BoomerangBlue = new EnemyPattern(true, false, true, false, false, true, true);
		BoomerangBlue.primaryWeapon = WeaponPattern.E_BoomerangBlue;
		BoomerangBlue.lifeMod = 4.5f;
		BoomerangBlue.damageMod = 1.25f;
		BoomerangBlue.image = Entity.Image.GN_Stingray;
		BoomerangBlue.dangerValue = 3f;
		BoomerangBlue.minLevel = easyLevel;
		BoomerangBlue.maxLevel = int.MaxValue;
		BoomerangBlue.name = "BoomerangBlue";
		BoomerangBlue.isFaceToPlayer = true;
		BoomerangBlue.isSingleUnit = true;
		BoomerangBlue.unitcolor = UnitColor.Blue;

		BoomerangPink = new EnemyPattern(true, true, true, false, false, true, true);
		BoomerangPink.primaryWeapon = WeaponPattern.E_BoomerangPink;
		BoomerangPink.lifeMod = 4.5f;
		BoomerangPink.damageMod = 1.25f;
		BoomerangPink.image = Entity.Image.RE_Stingray;
		BoomerangPink.dangerValue = 3f;
		BoomerangPink.minLevel = easyLevel;
		BoomerangPink.maxLevel = int.MaxValue;
		BoomerangPink.name = "BoomerangPink";
		BoomerangPink.isFaceToPlayer = true;
		BoomerangPink.isSingleUnit = true;
		BoomerangPink.unitcolor = UnitColor.Pink;

		BoomerangYellow2 = new EnemyPattern(true, false, true, false, false, true, true);
		BoomerangYellow2.primaryWeapon = WeaponPattern.E_BoomerangYellow2;
		BoomerangYellow2.lifeMod = 4.5f;
		BoomerangYellow2.damageMod = 1.25f;
		BoomerangYellow2.image = Entity.Image.EnemyStingray;
		BoomerangYellow2.dangerValue = 3.5f;
		BoomerangYellow2.minLevel = mediumLevel;
		BoomerangYellow2.maxLevel = int.MaxValue;
		BoomerangYellow2.name = "BoomerangYellow2";
		BoomerangYellow2.isFaceToPlayer = true;
		BoomerangYellow2.isSingleUnit = true;
		BoomerangYellow2.unitcolor = UnitColor.Yellow;
		
		BoomerangGreen2 = new EnemyPattern(true, false, true, false, false, true, true);
		BoomerangGreen2.primaryWeapon = WeaponPattern.E_BoomerangGreen2;
		BoomerangGreen2.lifeMod = 4.5f;
		BoomerangGreen2.damageMod = 1.25f;
		BoomerangGreen2.image = Entity.Image.EnemyStingray;
		BoomerangGreen2.dangerValue = 3.5f;
		BoomerangGreen2.minLevel = mediumLevel;
		BoomerangGreen2.maxLevel = int.MaxValue;
		BoomerangGreen2.name = "BoomerangGreen2";
		BoomerangGreen2.isFaceToPlayer = true;
		BoomerangGreen2.isSingleUnit = true;
		BoomerangGreen2.unitcolor = UnitColor.Green;
		
		BoomerangRed2 = new EnemyPattern(true, false, true, false, false, true, true);
		BoomerangRed2.primaryWeapon = WeaponPattern.E_BoomerangRed2;
		BoomerangRed2.lifeMod = 4.5f;
		BoomerangRed2.damageMod = 1.25f;
		BoomerangRed2.image = Entity.Image.GN_Stingray;
		BoomerangRed2.dangerValue = 3.5f;
		BoomerangRed2.minLevel = mediumLevel;
		BoomerangRed2.maxLevel = int.MaxValue;
		BoomerangRed2.name = "BoomerangRed2";
		BoomerangRed2.isFaceToPlayer = true;
		BoomerangRed2.isSingleUnit = true;
		BoomerangRed2.unitcolor = UnitColor.Red;
		
		BoomerangBlue2 = new EnemyPattern(true, false, true, false, false, true, true);
		BoomerangBlue2.primaryWeapon = WeaponPattern.E_BoomerangBlue2;
		BoomerangBlue2.lifeMod = 4.5f;
		BoomerangBlue2.damageMod = 1.25f;
		BoomerangBlue2.image = Entity.Image.GN_Stingray;
		BoomerangBlue2.dangerValue = 3.5f;
		BoomerangBlue2.minLevel = mediumLevel;
		BoomerangBlue2.maxLevel = int.MaxValue;
		BoomerangBlue2.name = "BoomerangBlue2";
		BoomerangBlue2.isFaceToPlayer = true;
		BoomerangBlue2.isSingleUnit = true;
		BoomerangBlue2.unitcolor = UnitColor.Blue;
		
		BoomerangPink2 = new EnemyPattern(true, true, true, false, false, true, true);
		BoomerangPink2.primaryWeapon = WeaponPattern.E_BoomerangPink2;
		BoomerangPink2.lifeMod = 4.5f;
		BoomerangPink2.damageMod = 1.25f;
		BoomerangPink2.image = Entity.Image.RE_Stingray;
		BoomerangPink2.dangerValue = 3.5f;
		BoomerangPink2.minLevel = mediumLevel;
		BoomerangPink2.maxLevel = int.MaxValue;
		BoomerangPink2.name = "BoomerangPink2";
		BoomerangPink2.isFaceToPlayer = true;
		BoomerangPink2.isSingleUnit = true;
		BoomerangPink2.unitcolor = UnitColor.Pink;

		SwirlYellow = new EnemyPattern(true, false, true, false, false, true, true);
		SwirlYellow.primaryWeapon = WeaponPattern.E_SwirlYellow;
		SwirlYellow.lifeMod = 4.5f;
		SwirlYellow.damageMod = 1.6f;
		SwirlYellow.image = Entity.Image.EnemyDiamond;
		SwirlYellow.dangerValue = 3f;
		SwirlYellow.minLevel = babyLevel;
		SwirlYellow.maxLevel = int.MaxValue;
		SwirlYellow.name = "SwirlYellow";
		SwirlYellow.isFaceToPlayer = true;
		SwirlYellow.isSingleUnit = true;
		SwirlYellow.unitcolor = UnitColor.Yellow;
		
		SwirlGreen = new EnemyPattern(true, false, true, false, false, true, true);
		SwirlGreen.primaryWeapon = WeaponPattern.E_SwirlGreen;
		SwirlGreen.lifeMod = 4.5f;
		SwirlGreen.damageMod = 1.6f;
		SwirlGreen.image = Entity.Image.EnemyDiamond;
		SwirlGreen.dangerValue = 3f;
		SwirlGreen.minLevel = easyLevel;
		SwirlGreen.maxLevel = int.MaxValue;
		SwirlGreen.name = "SwirlGreen";
		SwirlGreen.isFaceToPlayer = true;
		SwirlGreen.isSingleUnit = true;
		SwirlGreen.unitcolor = UnitColor.Green;
		
		SwirlRed = new EnemyPattern(true, false, true, false, false, true, true);
		SwirlRed.primaryWeapon = WeaponPattern.E_SwirlRed;
		SwirlRed.lifeMod = 4.5f;
		SwirlRed.damageMod = 1.6f;
		SwirlRed.image = Entity.Image.GN_Diamond;
		SwirlRed.dangerValue = 3f;
		SwirlRed.minLevel = easyLevel;
		SwirlRed.maxLevel = int.MaxValue;
		SwirlRed.name = "SwirlRed";
		SwirlRed.isFaceToPlayer = true;
		SwirlRed.isSingleUnit = true;
		SwirlRed.unitcolor = UnitColor.Red;
		
		SwirlBlue = new EnemyPattern(true, false, true, false, false, true, true);
		SwirlBlue.primaryWeapon = WeaponPattern.E_SwirlBlue;
		SwirlBlue.lifeMod = 4.5f;
		SwirlBlue.damageMod = 1.6f;
		SwirlBlue.image = Entity.Image.GN_Diamond;
		SwirlBlue.dangerValue = 3f;
		SwirlBlue.minLevel = easyLevel;
		SwirlBlue.maxLevel = int.MaxValue;
		SwirlBlue.name = "SwirlBlue";
		SwirlBlue.isFaceToPlayer = true;
		SwirlBlue.isSingleUnit = true;
		SwirlBlue.unitcolor = UnitColor.Blue;
		
		SwirlPink = new EnemyPattern(true, true, true, false, false, true, true);
		SwirlPink.primaryWeapon = WeaponPattern.E_SwirlPink;
		SwirlPink.lifeMod = 4.5f;
		SwirlPink.damageMod = 1.6f;
		SwirlPink.image = Entity.Image.RE_Diamond;
		SwirlPink.dangerValue = 3f;
		SwirlPink.minLevel = easyLevel;
		SwirlPink.maxLevel = int.MaxValue;
		SwirlPink.name = "SwirlPink";
		SwirlPink.isFaceToPlayer = true;
		SwirlPink.isSingleUnit = true;
		SwirlPink.unitcolor = UnitColor.Pink;
		
		SwirlYellow2 = new EnemyPattern(true, false, true, false, false, true, true);
		SwirlYellow2.primaryWeapon = WeaponPattern.E_SwirlYellow2;
		SwirlYellow2.lifeMod = 4.5f;
		SwirlYellow2.damageMod = 1.6f;
		SwirlYellow2.image = Entity.Image.GN_Diamond;
		SwirlYellow2.dangerValue = 3.5f;
		SwirlYellow2.minLevel = mediumLevel;
		SwirlYellow2.maxLevel = int.MaxValue;
		SwirlYellow2.name = "SwirlYellow2";
		SwirlYellow2.isFaceToPlayer = true;
		SwirlYellow2.isSingleUnit = true;
		SwirlYellow2.unitcolor = UnitColor.Yellow;
		
		SwirlGreen2 = new EnemyPattern(true, false, true, false, false, true, true);
		SwirlGreen2.primaryWeapon = WeaponPattern.E_SwirlGreen2;
		SwirlGreen2.lifeMod = 4.5f;
		SwirlGreen2.damageMod = 1.6f;
		SwirlGreen2.image = Entity.Image.GN_Diamond;
		SwirlGreen2.dangerValue = 3.5f;
		SwirlGreen2.minLevel = mediumLevel;
		SwirlGreen2.maxLevel = int.MaxValue;
		SwirlGreen2.name = "SwirlGreen2";
		SwirlGreen2.isFaceToPlayer = true;
		SwirlGreen2.isSingleUnit = true;
		SwirlGreen2.unitcolor = UnitColor.Green;
		
		SwirlRed2 = new EnemyPattern(true, false, true, false, false, true, true);
		SwirlRed2.primaryWeapon = WeaponPattern.E_SwirlRed2;
		SwirlRed2.lifeMod = 4.5f;
		SwirlRed2.damageMod = 1.6f;
		SwirlRed2.image = Entity.Image.GN_Diamond;
		SwirlRed2.dangerValue = 3.5f;
		SwirlRed2.minLevel = mediumLevel;
		SwirlRed2.maxLevel = int.MaxValue;
		SwirlRed2.name = "SwirlRed2";
		SwirlRed2.isFaceToPlayer = true;
		SwirlRed2.isSingleUnit = true;
		SwirlRed2.unitcolor = UnitColor.Red;
		
		SwirlBlue2 = new EnemyPattern(true, false, true, false, false, true, true);
		SwirlBlue2.primaryWeapon = WeaponPattern.E_SwirlBlue2;
		SwirlBlue2.lifeMod = 4.5f;
		SwirlBlue2.damageMod = 1.6f;
		SwirlBlue2.image = Entity.Image.GN_Diamond;
		SwirlBlue2.dangerValue = 3.5f;
		SwirlBlue2.minLevel = mediumLevel;
		SwirlBlue2.maxLevel = int.MaxValue;
		SwirlBlue2.name = "SwirlBlue2";
		SwirlBlue2.isFaceToPlayer = true;
		SwirlBlue2.isSingleUnit = true;
		SwirlBlue2.unitcolor = UnitColor.Blue;
		
		SwirlPink2 = new EnemyPattern(true, true, true, false, false, true, true);
		SwirlPink2.primaryWeapon = WeaponPattern.E_SwirlPink2;
		SwirlPink2.lifeMod = 4.5f;
		SwirlPink2.damageMod = 1.6f;
		SwirlPink2.image = Entity.Image.RE_Diamond;
		SwirlPink2.dangerValue = 3.5f;
		SwirlPink2.minLevel = mediumLevel;
		SwirlPink2.maxLevel = int.MaxValue;
		SwirlPink2.name = "SwirlPink2";
		SwirlPink2.isFaceToPlayer = true;
		SwirlPink2.isSingleUnit = true;
		SwirlPink2.unitcolor = UnitColor.Pink;


		Engage_Y = new EnemyPattern(true, false, true, false, false, true, true);
		Engage_Y.primaryWeapon = WeaponPattern.E_Engage_Y;
		Engage_Y.lifeMod = 4.5f;
		Engage_Y.damageMod = 1.4f;
		Engage_Y.image = Entity.Image.EnemyOx;
		Engage_Y.dangerValue = 4f;
		Engage_Y.minLevel = mediumLevel;
		Engage_Y.maxLevel = int.MaxValue;
		Engage_Y.name = "Engage_Y";
		Engage_Y.isFaceToPlayer = true;
		Engage_Y.isSingleUnit = true;
		Engage_Y.unitcolor = UnitColor.Yellow;
		
		Engage_G = new EnemyPattern(true, false, true, false, false, true, true);
		Engage_G.primaryWeapon = WeaponPattern.E_Engage_G;
		Engage_G.lifeMod = 4.5f;
		Engage_G.damageMod = 1.4f;
		Engage_G.image = Entity.Image.EnemyOx;
		Engage_G.dangerValue = 4f;
		Engage_G.minLevel = mediumLevel;
		Engage_G.maxLevel = int.MaxValue;
		Engage_G.name = "Engage_G";
		Engage_G.isFaceToPlayer = true;
		Engage_G.isSingleUnit = true;
		Engage_G.unitcolor = UnitColor.Green;
		
		Engage_R = new EnemyPattern(true, false, true, false, false, true, true);
		Engage_R.primaryWeapon = WeaponPattern.E_Engage_R;
		Engage_R.lifeMod = 4.5f;
		Engage_R.damageMod = 1.4f;
		Engage_R.image = Entity.Image.GN_Ox;
		Engage_R.dangerValue = 4f;
		Engage_R.minLevel = mediumLevel;
		Engage_R.maxLevel = int.MaxValue;
		Engage_R.name = "Engage_R";
		Engage_R.isFaceToPlayer = true;
		Engage_R.isSingleUnit = true;
		Engage_R.unitcolor = UnitColor.Red;
		
		Engage_B = new EnemyPattern(true, false, true, false, false, true, true);
		Engage_B.primaryWeapon = WeaponPattern.E_Engage_B;
		Engage_B.lifeMod = 4.5f;
		Engage_B.damageMod = 1.4f;
		Engage_B.image = Entity.Image.GN_Ox;
		Engage_B.dangerValue = 4f;
		Engage_B.minLevel = mediumLevel;
		Engage_B.maxLevel = int.MaxValue;
		Engage_B.name = "Engage_B";
		Engage_B.isFaceToPlayer = true;
		Engage_B.isSingleUnit = true;
		Engage_B.unitcolor = UnitColor.Blue;
		
		Engage_P = new EnemyPattern(true, true, true, false, false, true, true);
		Engage_P.primaryWeapon = WeaponPattern.E_Engage_P;
		Engage_P.lifeMod = 4.5f;
		Engage_P.damageMod = 1.4f;
		Engage_P.image = Entity.Image.RE_Ox;
		Engage_P.dangerValue = 3.5f;
		Engage_P.minLevel = mediumLevel;
		Engage_P.maxLevel = int.MaxValue;
		Engage_P.name = "Engage_P";
		Engage_P.isFaceToPlayer = true;
		Engage_P.isSingleUnit = true;
		Engage_P.unitcolor = UnitColor.Pink;

		//Elite
		
		Elite = new EnemyPattern(true, true, true, true, true, true, true);
		Elite.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite.secondaryWeapon = WeaponPattern.E_EliteMissile2;
		Elite.lifeMod = 20.0f;
		Elite.damageMod = 2.0f;
		Elite.image = Entity.Image.EnemyElite;
		Elite.speed = 900.0f;
		Elite.minLevel = maxEasyLevel;
		Elite.maxLevel = int.MaxValue;
		Elite.name = "Elite";
		Elite.moduleId = 201;
		
		Elite_GreenRanger = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_GreenRanger.primaryWeapon = WeaponPattern.E_EliteGreenScatter;
		Elite_GreenRanger.secondaryWeapon = WeaponPattern.E_EliteSweep;
		Elite_GreenRanger.lifeMod = 20.0f;
		Elite_GreenRanger.damageMod = 2.0f;
		Elite_GreenRanger.image = Entity.Image.EnemyElite;
		Elite_GreenRanger.speed = 700.0f;
		Elite_GreenRanger.minLevel = maxEasyLevel;
		Elite_GreenRanger.maxLevel = int.MaxValue;
		Elite_GreenRanger.name = "Elite_GreenRanger";
		Elite_GreenRanger.moduleId = 201;
		
		Elite_RedRanger = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedRanger.primaryWeapon = WeaponPattern.E_EliteRedScatter;
		Elite_RedRanger.secondaryWeapon = WeaponPattern.E_EliteSweep;
		Elite_RedRanger.lifeMod = 20.0f;
		Elite_RedRanger.damageMod = 2.0f;
		Elite_RedRanger.image = Entity.Image.EnemyElite;
		Elite_RedRanger.speed = 700.0f;
		Elite_RedRanger.minLevel = maxEasyLevel;
		Elite_RedRanger.maxLevel = int.MaxValue;
		Elite_RedRanger.name = "Elite_RedRanger";
		Elite_RedRanger.moduleId = 201;
		
		Elite_BlueRanger = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BlueRanger.primaryWeapon = WeaponPattern.E_EliteBlueScatter;
		Elite_BlueRanger.secondaryWeapon = WeaponPattern.E_EliteRapid2;
		Elite_BlueRanger.lifeMod = 20.0f;
		Elite_BlueRanger.damageMod = 2.0f;
		Elite_BlueRanger.image = Entity.Image.EnemyElite;
		Elite_BlueRanger.speed = 700.0f;
		Elite_BlueRanger.minLevel = maxEasyLevel;
		Elite_BlueRanger.maxLevel = int.MaxValue;
		Elite_BlueRanger.name = "Elite_BlueRanger";
		Elite_BlueRanger.moduleId = 201;
		
		Elite_PinkRanger = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_PinkRanger.primaryWeapon = WeaponPattern.E_ElitePinkScatter;
		Elite_PinkRanger.secondaryWeapon = WeaponPattern.E_EliteRapid2;
		Elite_PinkRanger.lifeMod = 15.0f;
		Elite_PinkRanger.damageMod = 3.00f;
		Elite_PinkRanger.image = Entity.Image.EnemyElite;
		Elite_PinkRanger.speed = 700.0f;
		Elite_PinkRanger.minLevel = maxEasyLevel;
		Elite_PinkRanger.maxLevel = int.MaxValue;
		Elite_PinkRanger.name = "Elite_PinkRanger";
		Elite_PinkRanger.moduleId = 201;
		
		Elite_GreenRanger2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_GreenRanger2.primaryWeapon = WeaponPattern.E_EliteGreenScatter;
		Elite_GreenRanger2.secondaryWeapon = WeaponPattern.E_EliteSweep;
		Elite_GreenRanger2.lifeMod = 20.0f;
		Elite_GreenRanger2.damageMod = 2.0f;
		Elite_GreenRanger2.image = Entity.Image.EnemyElite;
		Elite_GreenRanger2.speed = 700.0f;
		Elite_GreenRanger2.minLevel = maxNormalLevel;
		Elite_GreenRanger2.maxLevel = int.MaxValue;
		Elite_GreenRanger2.name = "Elite_GreenRanger2";
		Elite_GreenRanger2.moduleId = 211;
		
		Elite_RedRanger2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedRanger2.primaryWeapon = WeaponPattern.E_EliteRedScatter;
		Elite_RedRanger2.secondaryWeapon = WeaponPattern.E_EliteSweep;
		Elite_RedRanger2.lifeMod = 20.0f;
		Elite_RedRanger2.damageMod = 2.0f;
		Elite_RedRanger2.image = Entity.Image.EnemyElite;
		Elite_RedRanger2.speed = 700.0f;
		Elite_RedRanger2.minLevel = maxNormalLevel;
		Elite_RedRanger2.maxLevel = int.MaxValue;
		Elite_RedRanger2.name = "Elite_RedRanger2";
		Elite_RedRanger2.moduleId = 211;
		
		Elite_BlueRanger2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BlueRanger2.primaryWeapon = WeaponPattern.E_EliteBlueScatter;
		Elite_BlueRanger2.secondaryWeapon = WeaponPattern.E_EliteRapid2;
		Elite_BlueRanger2.lifeMod = 20.0f;
		Elite_BlueRanger2.damageMod = 2.0f;
		Elite_BlueRanger2.image = Entity.Image.EnemyElite;
		Elite_BlueRanger2.speed = 700.0f;
		Elite_BlueRanger2.minLevel = maxNormalLevel;
		Elite_BlueRanger2.maxLevel = int.MaxValue;
		Elite_BlueRanger2.name = "Elite_BlueRanger2";
		Elite_BlueRanger2.moduleId = 211;
		
		Elite_PinkRanger2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_PinkRanger2.primaryWeapon = WeaponPattern.E_ElitePinkScatter;
		Elite_PinkRanger2.secondaryWeapon = WeaponPattern.E_EliteRapid2;
		Elite_PinkRanger2.lifeMod = 15.0f;
		Elite_PinkRanger2.damageMod = 3.00f;
		Elite_PinkRanger2.image = Entity.Image.EnemyElite;
		Elite_PinkRanger2.speed = 700.0f;
		Elite_PinkRanger2.minLevel = maxNormalLevel;
		Elite_PinkRanger2.maxLevel = int.MaxValue;
		Elite_PinkRanger2.name = "Elite_PinkRanger2";
		Elite_PinkRanger2.moduleId = 211;
		
		Elite_BeamerSweep = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BeamerSweep.primaryWeapon = WeaponPattern.E_EliteBeam;
		Elite_BeamerSweep.secondaryWeapon = WeaponPattern.E_EliteSweep;
		Elite_BeamerSweep.lifeMod = 15.0f;
		Elite_BeamerSweep.damageMod = 2.0f;
		Elite_BeamerSweep.image = Entity.Image.EnemyElite;
		Elite_BeamerSweep.speed = 900.0f;
		Elite_BeamerSweep.minLevel = maxEasyLevel;
		Elite_BeamerSweep.maxLevel = int.MaxValue;
		Elite_BeamerSweep.name = "Elite_BeamerSweep";
		Elite_BeamerSweep.moduleId = 202;
		
		Elite_BeamerRapid = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BeamerRapid.primaryWeapon = WeaponPattern.E_EliteBeam;
		Elite_BeamerRapid.secondaryWeapon = WeaponPattern.E_EliteRapid2;
		Elite_BeamerRapid.lifeMod = 15.0f;
		Elite_BeamerRapid.damageMod = 2.0f;
		Elite_BeamerRapid.image = Entity.Image.EnemyElite;
		Elite_BeamerRapid.speed = 900.0f;
		Elite_BeamerRapid.minLevel = maxEasyLevel;
		Elite_BeamerRapid.maxLevel = int.MaxValue;
		Elite_BeamerRapid.name = "Elite_BeamerRapid";
		Elite_BeamerRapid.moduleId = 202;
		
		Elite_BeamerMissile1 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BeamerMissile1.primaryWeapon = WeaponPattern.E_EliteBeam;
		Elite_BeamerMissile1.secondaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_BeamerMissile1.lifeMod = 15.0f;
		Elite_BeamerMissile1.damageMod = 2.0f;
		Elite_BeamerMissile1.image = Entity.Image.EnemyElite;
		Elite_BeamerMissile1.speed = 900.0f;
		Elite_BeamerMissile1.minLevel = maxEasyLevel;
		Elite_BeamerMissile1.maxLevel = int.MaxValue;
		Elite_BeamerMissile1.name = "Elite_BeamerMissile1";
		Elite_BeamerMissile1.moduleId = 201;
		
		Elite_BeamerMissile2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BeamerMissile2.primaryWeapon = WeaponPattern.E_EliteBeam;
		Elite_BeamerMissile2.secondaryWeapon = WeaponPattern.E_EliteMissile2;
		Elite_BeamerMissile2.lifeMod = 15.0f;
		Elite_BeamerMissile2.damageMod = 2.0f;
		Elite_BeamerMissile2.image = Entity.Image.EnemyElite;
		Elite_BeamerMissile2.speed = 900.0f;
		Elite_BeamerMissile2.minLevel = maxEasyLevel;
		Elite_BeamerMissile2.maxLevel = int.MaxValue;
		Elite_BeamerMissile2.name = "Elite_BeamerMissile2";
		Elite_BeamerMissile2.moduleId = 201;
		
		Elite_BeamerStriker = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BeamerStriker.primaryWeapon = WeaponPattern.E_EliteBeam;
		Elite_BeamerStriker.secondaryWeapon = WeaponPattern.E_EliteRedStriker;
		Elite_BeamerStriker.lifeMod = 15.0f;
		Elite_BeamerStriker.damageMod = 2.0f;
		Elite_BeamerStriker.image = Entity.Image.EnemyElite;
		Elite_BeamerStriker.speed = 900.0f;
		Elite_BeamerStriker.minLevel = maxNormalLevel;
		Elite_BeamerStriker.maxLevel = int.MaxValue;
		Elite_BeamerStriker.name = "Elite_BeamerStriker";
		Elite_BeamerStriker.moduleId = 213;
		
		Elite_BeamerStriker2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BeamerStriker2.primaryWeapon = WeaponPattern.E_EliteBeam;
		Elite_BeamerStriker2.secondaryWeapon = WeaponPattern.E_EliteBlueStriker;
		Elite_BeamerStriker2.lifeMod = 15.0f;
		Elite_BeamerStriker2.damageMod = 2.0f;
		Elite_BeamerStriker2.image = Entity.Image.EnemyElite;
		Elite_BeamerStriker2.speed = 900.0f;
		Elite_BeamerStriker2.minLevel = maxNormalLevel;
		Elite_BeamerStriker2.maxLevel = int.MaxValue;
		Elite_BeamerStriker2.name = "Elite_BeamerStriker2";
		Elite_BeamerStriker2.moduleId = 213;
		
		Elite_GreenBlueDefender = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_GreenBlueDefender.primaryWeapon = WeaponPattern.E_EliteBlueHoming;
		Elite_GreenBlueDefender.secondaryWeapon = WeaponPattern.E_EliteGreenHoming;
		Elite_GreenBlueDefender.lifeMod = 20.0f;
		Elite_GreenBlueDefender.damageMod = 2.0f;
		Elite_GreenBlueDefender.image = Entity.Image.EnemyElite;
		Elite_GreenBlueDefender.speed = 500.0f;
		Elite_GreenBlueDefender.minLevel = maxEasyLevel;
		Elite_GreenBlueDefender.maxLevel = int.MaxValue;
		Elite_GreenBlueDefender.name = "Elite_GreenBlueDefender";
		Elite_GreenBlueDefender.moduleId = 203;
		
		Elite_PinkBlueDefender = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_PinkBlueDefender.primaryWeapon = WeaponPattern.E_EliteBlueHoming;
		Elite_PinkBlueDefender.secondaryWeapon = WeaponPattern.E_ElitePinkHoming;
		Elite_PinkBlueDefender.lifeMod = 20.0f;
		Elite_PinkBlueDefender.damageMod = 2.0f;
		Elite_PinkBlueDefender.image = Entity.Image.EnemyElite;
		Elite_PinkBlueDefender.speed = 500.0f;
		Elite_PinkBlueDefender.minLevel = maxEasyLevel;
		Elite_PinkBlueDefender.maxLevel = int.MaxValue;
		Elite_PinkBlueDefender.name = "Elite_PinkBlueDefender";
		Elite_PinkBlueDefender.moduleId = 203;
		
		Elite_RedBlueDefender = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedBlueDefender.primaryWeapon = WeaponPattern.E_EliteBlueHoming;
		Elite_RedBlueDefender.secondaryWeapon = WeaponPattern.E_EliteRedHoming;
		Elite_RedBlueDefender.lifeMod = 20.0f;
		Elite_RedBlueDefender.damageMod = 2.0f;
		Elite_RedBlueDefender.image = Entity.Image.EnemyElite;
		Elite_RedBlueDefender.speed = 500.0f;
		Elite_RedBlueDefender.minLevel = maxEasyLevel;
		Elite_RedBlueDefender.maxLevel = int.MaxValue;
		Elite_RedBlueDefender.name = "Elite_RedBlueDefender";
		Elite_RedBlueDefender.moduleId = 203;
		
		Elite_GreenBlueDefender2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_GreenBlueDefender2.primaryWeapon = WeaponPattern.E_EliteBlueHoming2;
		Elite_GreenBlueDefender2.secondaryWeapon = WeaponPattern.E_EliteGreenHoming2;
		Elite_GreenBlueDefender2.lifeMod = 20.0f;
		Elite_GreenBlueDefender2.damageMod = 2.0f;
		Elite_GreenBlueDefender2.image = Entity.Image.EnemyElite;
		Elite_GreenBlueDefender2.speed = 500.0f;
		Elite_GreenBlueDefender2.minLevel = maxNormalLevel;
		Elite_GreenBlueDefender2.maxLevel = int.MaxValue;
		Elite_GreenBlueDefender2.name = "Elite_GreenBlueDefender2";
		Elite_GreenBlueDefender2.moduleId = 212;
		
		Elite_PinkBlueDefender2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_PinkBlueDefender2.primaryWeapon = WeaponPattern.E_EliteBlueHoming2;
		Elite_PinkBlueDefender2.secondaryWeapon = WeaponPattern.E_ElitePinkHoming2;
		Elite_PinkBlueDefender2.lifeMod = 20.0f;
		Elite_PinkBlueDefender2.damageMod = 2.0f;
		Elite_PinkBlueDefender2.image = Entity.Image.EnemyElite;
		Elite_PinkBlueDefender2.speed = 500.0f;
		Elite_PinkBlueDefender2.minLevel = maxNormalLevel;
		Elite_PinkBlueDefender2.maxLevel = int.MaxValue;
		Elite_PinkBlueDefender2.name = "Elite_PinkBlueDefender2";
		Elite_PinkBlueDefender2.moduleId = 212;
		
		Elite_RedBlueDefender2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedBlueDefender2.primaryWeapon = WeaponPattern.E_EliteBlueHoming2;
		Elite_RedBlueDefender2.secondaryWeapon = WeaponPattern.E_EliteRedHoming2;
		Elite_RedBlueDefender2.lifeMod = 20.0f;
		Elite_RedBlueDefender2.damageMod = 2.0f;
		Elite_RedBlueDefender2.image = Entity.Image.EnemyElite;
		Elite_RedBlueDefender2.speed = 500.0f;
		Elite_RedBlueDefender2.minLevel = maxNormalLevel;
		Elite_RedBlueDefender2.maxLevel = int.MaxValue;
		Elite_RedBlueDefender2.name = "Elite_RedBlueDefender2";
		Elite_RedBlueDefender2.moduleId = 212;
		
		Elite_Speed = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Speed.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_Speed.secondaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_Speed.lifeMod = 15.0f;
		Elite_Speed.damageMod = 2.0f;
		Elite_Speed.image = Entity.Image.EnemyElite;
		Elite_Speed.speed = 2000.0f;
		Elite_Speed.minLevel = maxEasyLevel;
		Elite_Speed.maxLevel = int.MaxValue;
		Elite_Speed.name = "Elite_Speed";
		Elite_Speed.moduleId = 201;
		
		Elite_GreenLaser = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_GreenLaser.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_GreenLaser.secondaryWeapon = WeaponPattern.E_EliteGreenLaser;
		Elite_GreenLaser.lifeMod = 20.0f;
		Elite_GreenLaser.damageMod = 2.0f;
		Elite_GreenLaser.image = Entity.Image.EnemyElite;
		Elite_GreenLaser.speed = 900.0f;
		Elite_GreenLaser.minLevel = maxEasyLevel;
		Elite_GreenLaser.maxLevel = int.MaxValue;
		Elite_GreenLaser.name = "Elite_GreenLaser";
		Elite_GreenLaser.moduleId = 201;
		
		Elite_BlueLaser = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BlueLaser.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_BlueLaser.secondaryWeapon = WeaponPattern.E_EliteBlueLaser;
		Elite_BlueLaser.lifeMod = 20.0f;
		Elite_BlueLaser.damageMod = 2.0f;
		Elite_BlueLaser.image = Entity.Image.EnemyElite;
		Elite_BlueLaser.speed = 900.0f;
		Elite_BlueLaser.minLevel = maxEasyLevel;
		Elite_BlueLaser.maxLevel = int.MaxValue;
		Elite_BlueLaser.name = "Elite_BlueLaser";
		Elite_BlueLaser.moduleId = 201;
		
		Elite_RedLaser = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedLaser.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_RedLaser.secondaryWeapon = WeaponPattern.E_EliteRedLaser;
		Elite_RedLaser.lifeMod = 20.0f;
		Elite_RedLaser.damageMod = 2.0f;
		Elite_RedLaser.image = Entity.Image.EnemyElite;
		Elite_RedLaser.speed = 900.0f;
		Elite_RedLaser.minLevel = maxEasyLevel;
		Elite_RedLaser.maxLevel = int.MaxValue;
		Elite_RedLaser.name = "Elite_RedLaser";
		Elite_RedLaser.moduleId = 201;
		
		Elite_PinkLaser = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_PinkLaser.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_PinkLaser.secondaryWeapon = WeaponPattern.E_ElitePinkLaser;
		Elite_PinkLaser.lifeMod = 20.0f;
		Elite_PinkLaser.damageMod = 2.0f;
		Elite_PinkLaser.image = Entity.Image.EnemyElite;
		Elite_PinkLaser.speed = 900.0f;
		Elite_PinkLaser.minLevel = maxEasyLevel;
		Elite_PinkLaser.maxLevel = int.MaxValue;
		Elite_PinkLaser.name = "Elite_PinkLaser";
		Elite_PinkLaser.moduleId = 202;
		
		Elite_GreenLaser2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_GreenLaser2.primaryWeapon = WeaponPattern.E_EliteRapidHard;
		Elite_GreenLaser2.secondaryWeapon = WeaponPattern.E_EliteGreenLaser;
		Elite_GreenLaser2.lifeMod = 20.0f;
		Elite_GreenLaser2.damageMod = 2.0f;
		Elite_GreenLaser2.image = Entity.Image.EnemyElite;
		Elite_GreenLaser2.speed = 900.0f;
		Elite_GreenLaser2.minLevel = maxNormalLevel;
		Elite_GreenLaser2.maxLevel = int.MaxValue;
		Elite_GreenLaser2.name = "Elite_GreenLaser2";
		Elite_GreenLaser2.moduleId = 213;
		
		Elite_BlueLaser2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BlueLaser2.primaryWeapon = WeaponPattern.E_EliteRapidHard;
		Elite_BlueLaser2.secondaryWeapon = WeaponPattern.E_EliteBlueLaser;
		Elite_BlueLaser2.lifeMod = 20.0f;
		Elite_BlueLaser2.damageMod = 2.0f;
		Elite_BlueLaser2.image = Entity.Image.EnemyElite;
		Elite_BlueLaser2.speed = 900.0f;
		Elite_BlueLaser2.minLevel = maxNormalLevel;
		Elite_BlueLaser2.maxLevel = int.MaxValue;
		Elite_BlueLaser2.name = "Elite_BlueLaser2";
		Elite_BlueLaser2.moduleId = 213;
		
		Elite_RedLaser2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedLaser2.primaryWeapon = WeaponPattern.E_EliteRapidHard;
		Elite_RedLaser2.secondaryWeapon = WeaponPattern.E_EliteRedLaser;
		Elite_RedLaser2.lifeMod = 20.0f;
		Elite_RedLaser2.damageMod = 2.0f;
		Elite_RedLaser2.image = Entity.Image.EnemyElite;
		Elite_RedLaser2.speed = 900.0f;
		Elite_RedLaser2.minLevel = maxNormalLevel;
		Elite_RedLaser2.maxLevel = int.MaxValue;
		Elite_RedLaser2.name = "Elite_RedLaser2";
		Elite_RedLaser2.moduleId = 213;
		
		Elite_PinkLaser2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_PinkLaser2.primaryWeapon = WeaponPattern.E_EliteRapidHard;
		Elite_PinkLaser2.secondaryWeapon = WeaponPattern.E_ElitePinkLaser;
		Elite_PinkLaser2.lifeMod = 20.0f;
		Elite_PinkLaser2.damageMod = 2.0f;
		Elite_PinkLaser2.image = Entity.Image.EnemyElite;
		Elite_PinkLaser2.speed = 900.0f;
		Elite_PinkLaser2.minLevel = maxNormalLevel;
		Elite_PinkLaser2.maxLevel = int.MaxValue;
		Elite_PinkLaser2.name = "Elite_PinkLaser2";
		Elite_PinkLaser2.moduleId = 213;
		
		Elite_WaveGreen = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_WaveGreen.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_WaveGreen.secondaryWeapon = WeaponPattern.E_EliteWaveGreen;
		Elite_WaveGreen.lifeMod = 20.0f;
		Elite_WaveGreen.damageMod = 2.0f;
		Elite_WaveGreen.image = Entity.Image.EnemyElite;
		Elite_WaveGreen.speed = 900.0f;
		Elite_WaveGreen.minLevel = maxEasyLevel;
		Elite_WaveGreen.maxLevel = int.MaxValue;
		Elite_WaveGreen.name = "Elite_WaveGreen";
		Elite_WaveGreen.moduleId = 202;
		
		Elite_WaveBlue = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_WaveBlue.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_WaveBlue.secondaryWeapon = WeaponPattern.E_EliteWaveBlue;
		Elite_WaveBlue.lifeMod = 20.0f;
		Elite_WaveBlue.damageMod = 2.0f;
		Elite_WaveBlue.image = Entity.Image.EnemyElite;
		Elite_WaveBlue.speed = 900.0f;
		Elite_WaveBlue.minLevel = maxEasyLevel;
		Elite_WaveBlue.maxLevel = int.MaxValue;
		Elite_WaveBlue.name = "Elite_WaveBlue";
		Elite_WaveBlue.moduleId = 202;
		
		Elite_WaveRed = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_WaveRed.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_WaveRed.secondaryWeapon = WeaponPattern.E_EliteWaveRed;
		Elite_WaveRed.lifeMod = 20.0f;
		Elite_WaveRed.damageMod = 2.0f;
		Elite_WaveRed.image = Entity.Image.EnemyElite;
		Elite_WaveRed.speed = 900.0f;
		Elite_WaveRed.minLevel = maxEasyLevel;
		Elite_WaveRed.maxLevel = int.MaxValue;
		Elite_WaveRed.name = "Elite_WaveRed";
		Elite_WaveRed.moduleId = 202;
		
		Elite_WavePink = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_WavePink.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_WavePink.secondaryWeapon = WeaponPattern.E_EliteWavePink;
		Elite_WavePink.lifeMod = 20.0f;
		Elite_WavePink.damageMod = 2.0f;
		Elite_WavePink.image = Entity.Image.EnemyElite;
		Elite_WavePink.speed = 900.0f;
		Elite_WavePink.minLevel = maxEasyLevel;
		Elite_WavePink.maxLevel = int.MaxValue;
		Elite_WavePink.name = "Elite_WavePink";
		Elite_WavePink.moduleId = 212;
		
		Elite_WaveGreen2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_WaveGreen2.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_WaveGreen2.secondaryWeapon = WeaponPattern.E_EliteWaveGreen2;
		Elite_WaveGreen2.lifeMod = 20.0f;
		Elite_WaveGreen2.damageMod = 2.0f;
		Elite_WaveGreen2.image = Entity.Image.EnemyElite;
		Elite_WaveGreen2.speed = 900.0f;
		Elite_WaveGreen2.minLevel = maxNormalLevel;
		Elite_WaveGreen2.maxLevel = int.MaxValue;
		Elite_WaveGreen2.name = "Elite_WaveGreen2";
		Elite_WaveGreen2.moduleId = 213;
		
		Elite_WaveBlue2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_WaveBlue2.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_WaveBlue2.secondaryWeapon = WeaponPattern.E_EliteWaveBlue2;
		Elite_WaveBlue2.lifeMod = 20.0f;
		Elite_WaveBlue2.damageMod = 2.0f;
		Elite_WaveBlue2.image = Entity.Image.EnemyElite;
		Elite_WaveBlue2.speed = 900.0f;
		Elite_WaveBlue2.minLevel = maxNormalLevel;
		Elite_WaveBlue2.maxLevel = int.MaxValue;
		Elite_WaveBlue2.name = "Elite_WaveBlue2";
		Elite_WaveBlue2.moduleId = 213;
		
		Elite_WaveRed2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_WaveRed2.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_WaveRed2.secondaryWeapon = WeaponPattern.E_EliteWaveRed2;
		Elite_WaveRed2.lifeMod = 20.0f;
		Elite_WaveRed2.damageMod = 2.0f;
		Elite_WaveRed2.image = Entity.Image.EnemyElite;
		Elite_WaveRed2.speed = 900.0f;
		Elite_WaveRed2.minLevel = maxNormalLevel;
		Elite_WaveRed2.maxLevel = int.MaxValue;
		Elite_WaveRed2.name = "Elite_WaveRed2";
		Elite_WaveRed2.moduleId = 213;
		
		Elite_WavePink2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_WavePink2.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_WavePink2.secondaryWeapon = WeaponPattern.E_EliteWavePink2;
		Elite_WavePink2.lifeMod = 20.0f;
		Elite_WavePink2.damageMod = 2.0f;
		Elite_WavePink2.image = Entity.Image.EnemyElite;
		Elite_WavePink2.speed = 900.0f;
		Elite_WavePink2.minLevel = maxNormalLevel;
		Elite_WavePink2.maxLevel = int.MaxValue;
		Elite_WavePink2.name = "Elite_WavePink2";
		Elite_WavePink2.moduleId = 213;
		
		Elite_Damage = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Damage.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_Damage.secondaryWeapon = WeaponPattern.E_EliteRapidHell;
		Elite_Damage.lifeMod = 17.5f;
		Elite_Damage.damageMod = 2.5f;
		Elite_Damage.image = Entity.Image.EnemyElite;
		Elite_Damage.speed = 900.0f;
		Elite_Damage.minLevel = maxEasyLevel;
		Elite_Damage.maxLevel = int.MaxValue;
		Elite_Damage.name = "Elite_Damage";
		Elite_Damage.moduleId = 201;
		
		Elite_BlueStriker = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BlueStriker.primaryWeapon = WeaponPattern.E_EliteRapid2;
		Elite_BlueStriker.secondaryWeapon = WeaponPattern.E_EliteBlueStriker;
		Elite_BlueStriker.lifeMod = 25.0f;
		Elite_BlueStriker.damageMod = 2.0f;
		Elite_BlueStriker.image = Entity.Image.EnemyElite;
		Elite_BlueStriker.speed = 500.0f;
		Elite_BlueStriker.minLevel = maxEasyLevel;
		Elite_BlueStriker.maxLevel = int.MaxValue;
		Elite_BlueStriker.name = "Elite_BlueStriker";
		Elite_BlueStriker.moduleId = 201;
		
		Elite_RedStriker = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedStriker.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_RedStriker.secondaryWeapon = WeaponPattern.E_EliteRedStriker;
		Elite_RedStriker.lifeMod = 25.0f;
		Elite_RedStriker.damageMod = 2.0f;
		Elite_RedStriker.image = Entity.Image.EnemyElite;
		Elite_RedStriker.speed = 500.0f;
		Elite_RedStriker.minLevel = maxEasyLevel;
		Elite_RedStriker.maxLevel = int.MaxValue;
		Elite_RedStriker.name = "Elite_RedStriker";
		Elite_RedStriker.moduleId = 201;
		
		Elite_Striker = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Striker.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_Striker.secondaryWeapon = WeaponPattern.E_StrikerMissiles;
		Elite_Striker.lifeMod = 25.0f;
		Elite_Striker.damageMod = 2.0f;
		Elite_Striker.image = Entity.Image.EnemyElite;
		Elite_Striker.speed = 500.0f;
		Elite_Striker.minLevel = maxEasyLevel;
		Elite_Striker.maxLevel = int.MaxValue;
		Elite_Striker.name = "Elite_Striker";
		Elite_Striker.moduleId = 201;
		
		Elite_GreenCircle = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_GreenCircle.primaryWeapon = WeaponPattern.E_EliteMissile_Easy;
		Elite_GreenCircle.secondaryWeapon = WeaponPattern.E_EliteGreenCircle;
		Elite_GreenCircle.lifeMod = 25.0f;
		Elite_GreenCircle.damageMod = 2.0f;
		Elite_GreenCircle.image = Entity.Image.EnemyElite;
		Elite_GreenCircle.speed = 500.0f;
		Elite_GreenCircle.minLevel = maxEasyLevel;
		Elite_GreenCircle.maxLevel = int.MaxValue;
		Elite_GreenCircle.name = "Elite_GreenCircle";
		Elite_GreenCircle.moduleId = 201;
		
		Elite_BlueCircle = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BlueCircle.primaryWeapon = WeaponPattern.E_EliteMissile_Easy;
		Elite_BlueCircle.secondaryWeapon = WeaponPattern.E_EliteBlueCircle;
		Elite_BlueCircle.lifeMod = 25.0f;
		Elite_BlueCircle.damageMod = 2.0f;
		Elite_BlueCircle.image = Entity.Image.EnemyElite;
		Elite_BlueCircle.speed = 500.0f;
		Elite_BlueCircle.minLevel = maxEasyLevel;
		Elite_BlueCircle.maxLevel = int.MaxValue;
		Elite_BlueCircle.name = "Elite_BlueCircle";
		Elite_BlueCircle.moduleId = 201;
		
		Elite_RedCircle = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedCircle.primaryWeapon = WeaponPattern.E_EliteMissile_Easy;
		Elite_RedCircle.secondaryWeapon = WeaponPattern.E_EliteRedCircle;
		Elite_RedCircle.lifeMod = 25.0f;
		Elite_RedCircle.damageMod = 2.0f;
		Elite_RedCircle.image = Entity.Image.EnemyElite;
		Elite_RedCircle.speed = 500.0f;
		Elite_RedCircle.minLevel = maxEasyLevel;
		Elite_RedCircle.maxLevel = int.MaxValue;
		Elite_RedCircle.name = "Elite_RedCircle";
		Elite_RedCircle.moduleId = 201;
		
		Elite_PinkCircle = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_PinkCircle.primaryWeapon = WeaponPattern.E_EliteMissile_Easy;
		Elite_PinkCircle.secondaryWeapon = WeaponPattern.E_ElitePinkCircle;
		Elite_PinkCircle.lifeMod = 25.0f;
		Elite_PinkCircle.damageMod = 2.0f;
		Elite_PinkCircle.image = Entity.Image.EnemyElite;
		Elite_PinkCircle.speed = 500.0f;
		Elite_PinkCircle.minLevel = maxEasyLevel;
		Elite_PinkCircle.maxLevel = int.MaxValue;
		Elite_PinkCircle.name = "Elite_PinkCircle";
		Elite_PinkCircle.moduleId = 201;
		
		Elite_GreenCircleMissile = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_GreenCircleMissile.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_GreenCircleMissile.secondaryWeapon = WeaponPattern.E_EliteGreenCircle;
		Elite_GreenCircleMissile.lifeMod = 25.0f;
		Elite_GreenCircleMissile.damageMod = 2.0f;
		Elite_GreenCircleMissile.image = Entity.Image.EnemyElite;
		Elite_GreenCircleMissile.speed = 500.0f;
		Elite_GreenCircleMissile.minLevel = maxNormalLevel;
		Elite_GreenCircleMissile.maxLevel = int.MaxValue;
		Elite_GreenCircleMissile.name = "Elite_GreenCircleMissile";
		Elite_GreenCircleMissile.moduleId = 211;
		
		Elite_BlueCircleMissile = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BlueCircleMissile.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_BlueCircleMissile.secondaryWeapon = WeaponPattern.E_EliteBlueCircle;
		Elite_BlueCircleMissile.lifeMod = 25.0f;
		Elite_BlueCircleMissile.damageMod = 2.0f;
		Elite_BlueCircleMissile.image = Entity.Image.EnemyElite;
		Elite_BlueCircleMissile.speed = 500.0f;
		Elite_BlueCircleMissile.minLevel = maxNormalLevel;
		Elite_BlueCircleMissile.maxLevel = int.MaxValue;
		Elite_BlueCircleMissile.name = "Elite_BlueCircleMissile";
		Elite_BlueCircleMissile.moduleId = 211;
		
		Elite_RedCircleMissile = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedCircleMissile.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_RedCircleMissile.secondaryWeapon = WeaponPattern.E_EliteRedCircle;
		Elite_RedCircleMissile.lifeMod = 25.0f;
		Elite_RedCircleMissile.damageMod = 2.0f;
		Elite_RedCircleMissile.image = Entity.Image.EnemyElite;
		Elite_RedCircleMissile.speed = 500.0f;
		Elite_RedCircleMissile.minLevel = maxNormalLevel;
		Elite_RedCircleMissile.maxLevel = int.MaxValue;
		Elite_RedCircleMissile.name = "Elite_RedCircleMissile";
		Elite_RedCircleMissile.moduleId = 211;
		
		Elite_PinkCircleMissile = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_PinkCircleMissile.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_PinkCircleMissile.secondaryWeapon = WeaponPattern.E_ElitePinkCircle;
		Elite_PinkCircleMissile.lifeMod = 25.0f;
		Elite_PinkCircleMissile.damageMod = 2.0f;
		Elite_PinkCircleMissile.image = Entity.Image.EnemyElite;
		Elite_PinkCircleMissile.speed = 500.0f;
		Elite_PinkCircleMissile.minLevel = maxNormalLevel;
		Elite_PinkCircleMissile.maxLevel = int.MaxValue;
		Elite_PinkCircleMissile.name = "Elite_PinkCircleMissile";
		Elite_PinkCircleMissile.moduleId = 211;
		
		Elite_GreenCircleMissile2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_GreenCircleMissile2.primaryWeapon = WeaponPattern.E_EliteMissile2;
		Elite_GreenCircleMissile2.secondaryWeapon = WeaponPattern.E_EliteGreenCircle;
		Elite_GreenCircleMissile2.lifeMod = 25.0f;
		Elite_GreenCircleMissile2.damageMod = 2.0f;
		Elite_GreenCircleMissile2.image = Entity.Image.EnemyElite;
		Elite_GreenCircleMissile2.speed = 500.0f;
		Elite_GreenCircleMissile2.minLevel = maxNormalLevel;
		Elite_GreenCircleMissile2.maxLevel = int.MaxValue;
		Elite_GreenCircleMissile2.name = "Elite_GreenCircleMissile2";
		Elite_GreenCircleMissile2.moduleId = 212;
		
		Elite_BlueCircleMissile2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_BlueCircleMissile2.primaryWeapon = WeaponPattern.E_EliteMissile2;
		Elite_BlueCircleMissile2.secondaryWeapon = WeaponPattern.E_EliteBlueCircle;
		Elite_BlueCircleMissile2.lifeMod = 25.0f;
		Elite_BlueCircleMissile2.damageMod = 2.0f;
		Elite_BlueCircleMissile2.image = Entity.Image.EnemyElite;
		Elite_BlueCircleMissile2.speed = 500.0f;
		Elite_BlueCircleMissile2.minLevel = maxNormalLevel;
		Elite_BlueCircleMissile2.maxLevel = int.MaxValue;
		Elite_BlueCircleMissile2.name = "Elite_BlueCircleMissile2";
		Elite_BlueCircleMissile2.moduleId = 212;
		
		Elite_RedCircleMissile2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_RedCircleMissile2.primaryWeapon = WeaponPattern.E_EliteMissile2;
		Elite_RedCircleMissile2.secondaryWeapon = WeaponPattern.E_EliteRedCircle;
		Elite_RedCircleMissile2.lifeMod = 25.0f;
		Elite_RedCircleMissile2.damageMod = 2.0f;
		Elite_RedCircleMissile2.image = Entity.Image.EnemyElite;
		Elite_RedCircleMissile2.speed = 500.0f;
		Elite_RedCircleMissile2.minLevel = maxNormalLevel;
		Elite_RedCircleMissile2.maxLevel = int.MaxValue;
		Elite_RedCircleMissile2.name = "Elite_RedCircleMissile2";
		Elite_RedCircleMissile2.moduleId = 212;
		
		Elite_PinkCircleMissile2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_PinkCircleMissile2.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_PinkCircleMissile2.secondaryWeapon = WeaponPattern.E_ElitePinkCircle;
		Elite_PinkCircleMissile2.lifeMod = 25.0f;
		Elite_PinkCircleMissile2.damageMod = 2.0f;
		Elite_PinkCircleMissile2.image = Entity.Image.EnemyElite;
		Elite_PinkCircleMissile2.speed = 500.0f;
		Elite_PinkCircleMissile2.minLevel = maxNormalLevel;
		Elite_PinkCircleMissile2.maxLevel = int.MaxValue;
		Elite_PinkCircleMissile2.name = "Elite_PinkCircleMissile2";
		Elite_PinkCircleMissile2.moduleId = 212;

		//Elite Extreme

		Elite_Extreme1 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme1.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_Extreme1.secondaryWeapon = WeaponPattern.E_EliteGreenCircle;
		Elite_Extreme1.lifeMod = 33.0f;
		Elite_Extreme1.damageMod = 2.0f;
		Elite_Extreme1.image = Entity.Image.EnemyElite;
		Elite_Extreme1.speed = 500.0f;
		Elite_Extreme1.minLevel = eliteUpgradeLevel;
		Elite_Extreme1.maxLevel = int.MaxValue;
		Elite_Extreme1.name = "Elite_GreenCircleMissile";
		Elite_Extreme1.moduleId = 211;
		
		Elite_Extreme2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme2.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_Extreme2.secondaryWeapon = WeaponPattern.E_EliteBlueCircle;
		Elite_Extreme2.lifeMod = 33.0f;
		Elite_Extreme2.damageMod = 2.0f;
		Elite_Extreme2.image = Entity.Image.EnemyElite;
		Elite_Extreme2.speed = 500.0f;
		Elite_Extreme2.minLevel = eliteUpgradeLevel;
		Elite_Extreme2.maxLevel = int.MaxValue;
		Elite_Extreme2.name = "Elite_BlueCircleMissile";
		Elite_Extreme2.moduleId = 211;
		
		Elite_Extreme3 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme3.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_Extreme3.secondaryWeapon = WeaponPattern.E_EliteRedCircle;
		Elite_Extreme3.lifeMod = 33.0f;
		Elite_Extreme3.damageMod = 2.0f;
		Elite_Extreme3.image = Entity.Image.EnemyElite;
		Elite_Extreme3.speed = 500.0f;
		Elite_Extreme3.minLevel = eliteUpgradeLevel;
		Elite_Extreme3.maxLevel = int.MaxValue;
		Elite_Extreme3.name = "Elite_RedCircleMissile";
		Elite_Extreme3.moduleId = 211;
		
		Elite_Extreme4 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme4.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_Extreme4.secondaryWeapon = WeaponPattern.E_ElitePinkCircle;
		Elite_Extreme4.lifeMod = 33.0f;
		Elite_Extreme4.damageMod = 2.0f;
		Elite_Extreme4.image = Entity.Image.EnemyElite;
		Elite_Extreme4.speed = 500.0f;
		Elite_Extreme4.minLevel = eliteUpgradeLevel;
		Elite_Extreme4.maxLevel = int.MaxValue;
		Elite_Extreme4.name = "Elite_PinkCircleMissile";
		Elite_Extreme4.moduleId = 211;
		
		Elite_Extreme5 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme5.primaryWeapon = WeaponPattern.E_EliteMissile2;
		Elite_Extreme5.secondaryWeapon = WeaponPattern.E_EliteGreenCircle;
		Elite_Extreme5.lifeMod = 33.0f;
		Elite_Extreme5.damageMod = 2.0f;
		Elite_Extreme5.image = Entity.Image.EnemyElite;
		Elite_Extreme5.speed = 500.0f;
		Elite_Extreme5.minLevel = eliteUpgradeLevel;
		Elite_Extreme5.maxLevel = int.MaxValue;
		Elite_Extreme5.name = "Elite_GreenCircleMissile2";
		Elite_Extreme5.moduleId = 212;
		
		Elite_Extreme6 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme6.primaryWeapon = WeaponPattern.E_EliteMissile2;
		Elite_Extreme6.secondaryWeapon = WeaponPattern.E_EliteBlueCircle;
		Elite_Extreme6.lifeMod = 33.0f;
		Elite_Extreme6.damageMod = 2.0f;
		Elite_Extreme6.image = Entity.Image.EnemyElite;
		Elite_Extreme6.speed = 500.0f;
		Elite_Extreme6.minLevel = eliteUpgradeLevel;
		Elite_Extreme6.maxLevel = int.MaxValue;
		Elite_Extreme6.name = "Elite_BlueCircleMissile2";
		Elite_Extreme6.moduleId = 212;
		
		Elite_Extreme7 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme7.primaryWeapon = WeaponPattern.E_EliteMissile2;
		Elite_Extreme7.secondaryWeapon = WeaponPattern.E_EliteRedCircle;
		Elite_Extreme7.lifeMod = 33.0f;
		Elite_Extreme7.damageMod = 2.0f;
		Elite_Extreme7.image = Entity.Image.EnemyElite;
		Elite_Extreme7.speed = 500.0f;
		Elite_Extreme7.minLevel = eliteUpgradeLevel;
		Elite_Extreme7.maxLevel = int.MaxValue;
		Elite_Extreme7.name = "Elite_RedCircleMissile2";
		Elite_Extreme7.moduleId = 212;
		
		Elite_Extreme8 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme8.primaryWeapon = WeaponPattern.E_EliteMissile;
		Elite_Extreme8.secondaryWeapon = WeaponPattern.E_ElitePinkCircle;
		Elite_Extreme8.lifeMod = 33.0f;
		Elite_Extreme8.damageMod = 2.0f;
		Elite_Extreme8.image = Entity.Image.EnemyElite;
		Elite_Extreme8.speed = 500.0f;
		Elite_Extreme8.minLevel = eliteUpgradeLevel;
		Elite_Extreme8.maxLevel = int.MaxValue;
		Elite_Extreme8.name = "Elite_PinkCircleMissile2";
		Elite_Extreme8.moduleId = 212;
		
		Elite_Extreme9 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme9.primaryWeapon = WeaponPattern.E_EliteRapidHard;
		Elite_Extreme9.secondaryWeapon = WeaponPattern.E_EliteGreenLaser;
		Elite_Extreme9.lifeMod = 33.0f;
		Elite_Extreme9.damageMod = 2.0f;
		Elite_Extreme9.image = Entity.Image.EnemyElite;
		Elite_Extreme9.speed = 900.0f;
		Elite_Extreme9.minLevel = eliteUpgradeLevel;
		Elite_Extreme9.maxLevel = int.MaxValue;
		Elite_Extreme9.name = "Elite_GreenLaser2";
		Elite_Extreme9.moduleId = 213;
		
		Elite_Extreme10 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme10.primaryWeapon = WeaponPattern.E_EliteRapidHard;
		Elite_Extreme10.secondaryWeapon = WeaponPattern.E_EliteBlueLaser;
		Elite_Extreme10.lifeMod = 33.0f;
		Elite_Extreme10.damageMod = 2.0f;
		Elite_Extreme10.image = Entity.Image.EnemyElite;
		Elite_Extreme10.speed = 900.0f;
		Elite_Extreme10.minLevel = eliteUpgradeLevel;
		Elite_Extreme10.maxLevel = int.MaxValue;
		Elite_Extreme10.name = "Elite_BlueLaser2";
		Elite_Extreme10.moduleId = 213;
		
		Elite_Extreme11 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme11.primaryWeapon = WeaponPattern.E_EliteRapidHard;
		Elite_Extreme11.secondaryWeapon = WeaponPattern.E_EliteRedLaser;
		Elite_Extreme11.lifeMod = 33.0f;
		Elite_Extreme11.damageMod = 2.0f;
		Elite_Extreme11.image = Entity.Image.EnemyElite;
		Elite_Extreme11.speed = 900.0f;
		Elite_Extreme11.minLevel = eliteUpgradeLevel;
		Elite_Extreme11.maxLevel = int.MaxValue;
		Elite_Extreme11.name = "Elite_RedLaser2";
		Elite_Extreme11.moduleId = 213;
		
		Elite_Extreme12 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme12.primaryWeapon = WeaponPattern.E_EliteRapidHard;
		Elite_Extreme12.secondaryWeapon = WeaponPattern.E_ElitePinkLaser;
		Elite_Extreme12.lifeMod = 33.0f;
		Elite_Extreme12.damageMod = 2.0f;
		Elite_Extreme12.image = Entity.Image.EnemyElite;
		Elite_Extreme12.speed = 900.0f;
		Elite_Extreme12.minLevel = eliteUpgradeLevel;
		Elite_Extreme12.maxLevel = int.MaxValue;
		Elite_Extreme12.name = "Elite_PinkLaser2";
		Elite_Extreme12.moduleId = 213;
		
		Elite_Extreme13 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme13.primaryWeapon = WeaponPattern.E_EliteGreenScatter;
		Elite_Extreme13.secondaryWeapon = WeaponPattern.E_EliteSweep;
		Elite_Extreme13.lifeMod = 33.0f;
		Elite_Extreme13.damageMod = 2.0f;
		Elite_Extreme13.image = Entity.Image.EnemyElite;
		Elite_Extreme13.speed = 700.0f;
		Elite_Extreme13.minLevel = eliteUpgradeLevel;
		Elite_Extreme13.maxLevel = int.MaxValue;
		Elite_Extreme13.name = "Elite_GreenRanger2";
		Elite_Extreme13.moduleId = 211;
		
		Elite_Extreme14 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme14.primaryWeapon = WeaponPattern.E_EliteRedScatter;
		Elite_Extreme14.secondaryWeapon = WeaponPattern.E_EliteSweep;
		Elite_Extreme14.lifeMod = 33.0f;
		Elite_Extreme14.damageMod = 2.0f;
		Elite_Extreme14.image = Entity.Image.EnemyElite;
		Elite_Extreme14.speed = 700.0f;
		Elite_Extreme14.minLevel = eliteUpgradeLevel;
		Elite_Extreme14.maxLevel = int.MaxValue;
		Elite_Extreme14.name = "Elite_RedRanger2";
		Elite_Extreme14.moduleId = 211;
		
		Elite_Extreme15 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme15.primaryWeapon = WeaponPattern.E_EliteBlueScatter;
		Elite_Extreme15.secondaryWeapon = WeaponPattern.E_EliteRapid2;
		Elite_Extreme15.lifeMod = 33.0f;
		Elite_Extreme15.damageMod = 2.0f;
		Elite_Extreme15.image = Entity.Image.EnemyElite;
		Elite_Extreme15.speed = 700.0f;
		Elite_Extreme15.minLevel = eliteUpgradeLevel;
		Elite_Extreme15.maxLevel = int.MaxValue;
		Elite_Extreme15.name = "Elite_BlueRanger2";
		Elite_Extreme15.moduleId = 211;
		
		Elite_Extreme16 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme16.primaryWeapon = WeaponPattern.E_ElitePinkScatter;
		Elite_Extreme16.secondaryWeapon = WeaponPattern.E_EliteRapid2;
		Elite_Extreme16.lifeMod = 33.0f;
		Elite_Extreme16.damageMod = 3.00f;
		Elite_Extreme16.image = Entity.Image.EnemyElite;
		Elite_Extreme16.speed = 700.0f;
		Elite_Extreme16.minLevel = eliteUpgradeLevel;
		Elite_Extreme16.maxLevel = int.MaxValue;
		Elite_Extreme16.name = "Elite_PinkRanger2";
		Elite_Extreme16.moduleId = 211;
		
		Elite_Extreme17 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme17.primaryWeapon = WeaponPattern.E_EliteBeam;
		Elite_Extreme17.secondaryWeapon = WeaponPattern.E_EliteRedStriker;
		Elite_Extreme17.lifeMod = 33.0f;
		Elite_Extreme17.damageMod = 2.0f;
		Elite_Extreme17.image = Entity.Image.EnemyElite;
		Elite_Extreme17.speed = 900.0f;
		Elite_Extreme17.minLevel = eliteUpgradeLevel;
		Elite_Extreme17.maxLevel = int.MaxValue;
		Elite_Extreme17.name = "Elite_BeamerStriker";
		Elite_Extreme17.moduleId = 213;
		
		Elite_Extreme18 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme18.primaryWeapon = WeaponPattern.E_EliteBeam;
		Elite_Extreme18.secondaryWeapon = WeaponPattern.E_EliteBlueStriker;
		Elite_Extreme18.lifeMod = 33.0f;
		Elite_Extreme18.damageMod = 2.0f;
		Elite_Extreme18.image = Entity.Image.EnemyElite;
		Elite_Extreme18.speed = 900.0f;
		Elite_Extreme18.minLevel = eliteUpgradeLevel;
		Elite_Extreme18.maxLevel = int.MaxValue;
		Elite_Extreme18.name = "Elite_BeamerStriker2";
		Elite_Extreme18.moduleId = 213;
		
		Elite_Extreme19 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme19.primaryWeapon = WeaponPattern.E_EliteBlueHoming2;
		Elite_Extreme19.secondaryWeapon = WeaponPattern.E_EliteGreenHoming2;
		Elite_Extreme19.lifeMod = 33.0f;
		Elite_Extreme19.damageMod = 2.0f;
		Elite_Extreme19.image = Entity.Image.EnemyElite;
		Elite_Extreme19.speed = 500.0f;
		Elite_Extreme19.minLevel = eliteUpgradeLevel;
		Elite_Extreme19.maxLevel = int.MaxValue;
		Elite_Extreme19.name = "Elite_GreenBlueDefender2";
		Elite_Extreme19.moduleId = 212;
		
		Elite_Extreme20 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme20.primaryWeapon = WeaponPattern.E_EliteBlueHoming2;
		Elite_Extreme20.secondaryWeapon = WeaponPattern.E_ElitePinkHoming2;
		Elite_Extreme20.lifeMod = 33.0f;
		Elite_Extreme20.damageMod = 2.0f;
		Elite_Extreme20.image = Entity.Image.EnemyElite;
		Elite_Extreme20.speed = 500.0f;
		Elite_Extreme20.minLevel = eliteUpgradeLevel;
		Elite_Extreme20.maxLevel = int.MaxValue;
		Elite_Extreme20.name = "Elite_PinkBlueDefender2";
		Elite_Extreme20.moduleId = 212;
		
		Elite_Extreme21 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme21.primaryWeapon = WeaponPattern.E_EliteBlueHoming2;
		Elite_Extreme21.secondaryWeapon = WeaponPattern.E_EliteRedHoming2;
		Elite_Extreme21.lifeMod = 33.0f;
		Elite_Extreme21.damageMod = 2.0f;
		Elite_Extreme21.image = Entity.Image.EnemyElite;
		Elite_Extreme21.speed = 500.0f;
		Elite_Extreme21.minLevel = eliteUpgradeLevel;
		Elite_Extreme21.maxLevel = int.MaxValue;
		Elite_Extreme21.name = "Elite_RedBlueDefender2";
		Elite_Extreme21.moduleId = 212;
		
		Elite_Extreme22 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme22.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_Extreme22.secondaryWeapon = WeaponPattern.E_EliteWaveGreen2;
		Elite_Extreme22.lifeMod = 33.0f;
		Elite_Extreme22.damageMod = 2.0f;
		Elite_Extreme22.image = Entity.Image.EnemyElite;
		Elite_Extreme22.speed = 900.0f;
		Elite_Extreme22.minLevel = eliteUpgradeLevel;
		Elite_Extreme22.maxLevel = int.MaxValue;
		Elite_Extreme22.name = "Elite_WaveGreen2";
		Elite_Extreme22.moduleId = 213;
		
		Elite_Extreme23 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme23.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_Extreme23.secondaryWeapon = WeaponPattern.E_EliteWaveBlue2;
		Elite_Extreme23.lifeMod = 33.0f;
		Elite_Extreme23.damageMod = 2.0f;
		Elite_Extreme23.image = Entity.Image.EnemyElite;
		Elite_Extreme23.speed = 900.0f;
		Elite_Extreme23.minLevel = eliteUpgradeLevel;
		Elite_Extreme23.maxLevel = int.MaxValue;
		Elite_Extreme23.name = "Elite_WaveBlue2";
		Elite_Extreme23.moduleId = 213;
		
		Elite_Extreme24 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme24.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_Extreme24.secondaryWeapon = WeaponPattern.E_EliteWaveRed2;
		Elite_Extreme24.lifeMod = 33.0f;
		Elite_Extreme24.damageMod = 2.0f;
		Elite_Extreme24.image = Entity.Image.EnemyElite;
		Elite_Extreme24.speed = 900.0f;
		Elite_Extreme24.minLevel = eliteUpgradeLevel;
		Elite_Extreme24.maxLevel = int.MaxValue;
		Elite_Extreme24.name = "Elite_WaveRed2";
		Elite_Extreme24.moduleId = 213;
		
		Elite_Extreme25 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Extreme25.primaryWeapon = WeaponPattern.E_EliteRapid;
		Elite_Extreme25.secondaryWeapon = WeaponPattern.E_EliteWavePink2;
		Elite_Extreme25.lifeMod = 33.0f;
		Elite_Extreme25.damageMod = 2.0f;
		Elite_Extreme25.image = Entity.Image.EnemyElite;
		Elite_Extreme25.speed = 900.0f;
		Elite_Extreme25.minLevel = eliteUpgradeLevel;
		Elite_Extreme25.maxLevel = int.MaxValue;
		Elite_Extreme25.name = "Elite_WavePink2";
		Elite_Extreme25.moduleId = 213;

		//Elite low level
		
		Elite_Easy_1 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_1.primaryWeapon = WeaponPattern.E_EliteRapid_Easy;
		Elite_Easy_1.secondaryWeapon = WeaponPattern.E_EliteMissile_Easy;
		Elite_Easy_1.lifeMod = 35.0f;
		Elite_Easy_1.damageMod = 1.3f;
		Elite_Easy_1.image = Entity.Image.EnemyElite;
		Elite_Easy_1.speed = 500.0f;
		Elite_Easy_1.minLevel = 0;
		Elite_Easy_1.maxLevel = maxEasyLevel;
		Elite_Easy_1.name = "Elite_Easy_1";
		Elite_Easy_1.moduleId = 203;
		
		Elite_Easy_2 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_2.primaryWeapon = WeaponPattern.E_EliteRapid_Easy;
		Elite_Easy_2.secondaryWeapon = WeaponPattern.E_EliteGreenScatter_Easy;
		Elite_Easy_2.lifeMod = 35.0f;
		Elite_Easy_2.damageMod = 1.3f;
		Elite_Easy_2.image = Entity.Image.EnemyElite;
		Elite_Easy_2.speed = 500.0f;
		Elite_Easy_2.minLevel = 0;
		Elite_Easy_2.maxLevel = maxEasyLevel;
		Elite_Easy_2.name = "Elite_Easy_2";
		Elite_Easy_2.moduleId = 203;
		
		Elite_Easy_3 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_3.primaryWeapon = WeaponPattern.E_EliteRapid_Easy;
		Elite_Easy_3.secondaryWeapon = WeaponPattern.E_EliteGreenHoming_Easy;
		Elite_Easy_3.lifeMod = 35.0f;
		Elite_Easy_3.damageMod = 1.3f;
		Elite_Easy_3.image = Entity.Image.EnemyElite;
		Elite_Easy_3.speed = 500.0f;
		Elite_Easy_3.minLevel = 0;
		Elite_Easy_3.maxLevel = maxEasyLevel;
		Elite_Easy_3.name = "Elite_Easy_3";
		Elite_Easy_3.moduleId = 203;
		
		Elite_Easy_4 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_4.primaryWeapon = WeaponPattern.E_EliteRapid_Easy;
		Elite_Easy_4.secondaryWeapon = WeaponPattern.E_EliteWave_Easy;
		Elite_Easy_4.lifeMod = 35.0f;
		Elite_Easy_4.damageMod = 1.3f;
		Elite_Easy_4.image = Entity.Image.EnemyElite;
		Elite_Easy_4.speed = 500.0f;
		Elite_Easy_4.minLevel = 0;
		Elite_Easy_4.maxLevel = maxEasyLevel;
		Elite_Easy_4.name = "Elite_Easy_4";
		Elite_Easy_4.moduleId = 203;
		
		Elite_Easy_5 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_5.primaryWeapon = WeaponPattern.E_EliteSweep_Easy;
		Elite_Easy_5.secondaryWeapon = WeaponPattern.E_EliteMissile_Easy;
		Elite_Easy_5.lifeMod = 35.0f;
		Elite_Easy_5.damageMod = 1.3f;
		Elite_Easy_5.image = Entity.Image.EnemyElite;
		Elite_Easy_5.speed = 500.0f;
		Elite_Easy_5.minLevel = 0;
		Elite_Easy_5.maxLevel = maxEasyLevel;
		Elite_Easy_5.name = "Elite_Easy_5";
		Elite_Easy_5.moduleId = 204;
		
		Elite_Easy_6 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_6.primaryWeapon = WeaponPattern.E_EliteSweep_Easy;
		Elite_Easy_6.secondaryWeapon = WeaponPattern.E_EliteGreenScatter_Easy;
		Elite_Easy_6.lifeMod = 35.0f;
		Elite_Easy_6.damageMod = 1.3f;
		Elite_Easy_6.image = Entity.Image.EnemyElite;
		Elite_Easy_6.speed = 500.0f;
		Elite_Easy_6.minLevel = 0;
		Elite_Easy_6.maxLevel = maxEasyLevel;
		Elite_Easy_6.name = "Elite_Easy_6";
		Elite_Easy_6.moduleId = 204;
		
		Elite_Easy_7 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_7.primaryWeapon = WeaponPattern.E_EliteSweep_Easy;
		Elite_Easy_7.secondaryWeapon = WeaponPattern.E_EliteGreenHoming_Easy;
		Elite_Easy_7.lifeMod = 35.0f;
		Elite_Easy_7.damageMod = 1.3f;
		Elite_Easy_7.image = Entity.Image.EnemyElite;
		Elite_Easy_7.speed = 500.0f;
		Elite_Easy_7.minLevel = 0;
		Elite_Easy_7.maxLevel = maxEasyLevel;
		Elite_Easy_7.name = "Elite_Easy_7";
		Elite_Easy_7.moduleId = 204;
		
		Elite_Easy_8 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_8.primaryWeapon = WeaponPattern.E_EliteSweep_Easy;
		Elite_Easy_8.secondaryWeapon = WeaponPattern.E_EliteWave_Easy;
		Elite_Easy_8.lifeMod = 35.0f;
		Elite_Easy_8.damageMod = 1.3f;
		Elite_Easy_8.image = Entity.Image.EnemyElite;
		Elite_Easy_8.speed = 500.0f;
		Elite_Easy_8.minLevel = 0;
		Elite_Easy_8.maxLevel = maxEasyLevel;
		Elite_Easy_8.name = "Elite_Easy_8";
		Elite_Easy_8.moduleId = 204;
		
		Elite_Easy_9 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_9.primaryWeapon = WeaponPattern.E_EliteSweep_Easy;
		Elite_Easy_9.secondaryWeapon = WeaponPattern.E_EliteGreenLaser_Easy;
		Elite_Easy_9.lifeMod = 35.0f;
		Elite_Easy_9.damageMod = 1.3f;
		Elite_Easy_9.image = Entity.Image.EnemyElite;
		Elite_Easy_9.speed = 500.0f;
		Elite_Easy_9.minLevel = 0;
		Elite_Easy_9.maxLevel = maxEasyLevel;
		Elite_Easy_9.name = "Elite_Easy_9";
		Elite_Easy_9.moduleId = 204;
		
		Elite_Easy_10 = new EnemyPattern(true, true, true, true, true, true, true);
		Elite_Easy_10.primaryWeapon = WeaponPattern.E_EliteRapid_Easy;
		Elite_Easy_10.secondaryWeapon = WeaponPattern.E_EliteGreenLaser_Easy;
		Elite_Easy_10.lifeMod = 35.0f;
		Elite_Easy_10.damageMod = 1.3f;
		Elite_Easy_10.image = Entity.Image.EnemyElite;
		Elite_Easy_10.speed = 500.0f;
		Elite_Easy_10.minLevel = 0;
		Elite_Easy_10.maxLevel = maxEasyLevel;
		Elite_Easy_10.name = "Elite_Easy_10";
		Elite_Easy_10.moduleId = 203;

		TestEnemy = new EnemyPattern(true, true, true, true, true, true, true);
		TestEnemy.primaryWeapon = WeaponPattern.X_Swirl;
		TestEnemy.lifeMod = 4.5f;
		TestEnemy.damageMod = 2.0f;
		TestEnemy.image = Entity.Image.EnemyLaser;
		TestEnemy.dangerValue = 4.0f;
		TestEnemy.minLevel = 0;
		TestEnemy.maxLevel = int.MaxValue;
		TestEnemy.name = "Laser4";
		TestEnemy.isFaceToPlayer = true;


		
	}
	
	public EnemyPattern (bool isBalanced = true, bool isBulletHell = false, bool isAoE = false, bool isElite = false
	                     , bool isBeamHell = false, bool isHoming = false, bool isMine = false, bool isQuest1 = false)
	{

		// Add it to the appropriate list.
		allPatterns.Add(this);	

		if (isElite) {

			allElite.Add (this);

		} else {

			if (!isAoE) {
				allQuest_Bonus.Add (this);
				allQuest_Boss.Add (this);
			}

			if (isBalanced) allBalanced.Add(this);
			if (isBulletHell) allBulletHell.Add(this);
			if (isAoE) allAoE.Add(this);
			if (isBeamHell) allBeamHell.Add(this);
			if (isHoming) allHoming.Add(this);
			if (isMine) allMine.Add(this);

		}

	}

	public static List<EnemyPattern> GetEligablePatternsForLevel (float level, float dangerValue = 0.0f, bool isElite = false)
	{
		List<EnemyPattern> newEnemyPatternList = new List<EnemyPattern>();
		List<EnemyPattern> sourceList;
			
		if (isElite) {
			if(WaveController.TestElite) { //test elite
				allElite.Clear(); //test elite
				allElite.Add(WaveController.TestElitePattern); // test elite
			}
			sourceList = allElite;
		} else {
			sourceList = GetListForType(WaveController.type);
		}
				
		for ( int i = sourceList.Count - 1 ; i > -1 ; i -- )
		{
			EnemyPattern enemyPattern = sourceList[i];

			if ( (enemyPattern.minLevel <= level) && (enemyPattern.maxLevel >= level) )
			{
				if (enemyPattern.dangerValue <= dangerValue || isElite)
				{
					//if(enemyPattern.name != null) Debug.Log(enemyPattern.name);
					newEnemyPatternList.Add(enemyPattern);
				}
			}
		}
		return newEnemyPatternList;
	}
	
}

