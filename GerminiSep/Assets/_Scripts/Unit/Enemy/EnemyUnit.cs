using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyUnit : Unit {
	
	private static List<Unit> _allActive;
	public static List<Unit> allActive
	{
		get {
			if (_allActive == null)
			{
				_allActive = new List<Unit>();
			}
			
			return _allActive;
		}
	}
	
	public Squad squad;

	// Use this for initialization
	void Start () {
		team = Team.Enemy;
	}
	
	EliteSide eliteSide = EliteSide.Right;
	EliteSide roamSide = EliteSide.Left;

	enum EliteSide {
		Left,
		Right
	}

	private Vector3 currentAnchor;
	private Vector3 offsetAnchor;

	public Weapon secondaryWeapon;
	public bool faceToPlayer = false;
	bool fireLock = false; // Prevent the unit from firing.

	private float offsetFactorAmount = 0.0f;
	private float offsetFactorIncrease = 0.035f;
	private float offsetDistance = 0.0f;
	private float offsetDistanceMax = 60.0f;
	private float offsetXFactor = 1.0f;

	private static Vector3 leftAnchor = new Vector3(-100.0f, 130.0f, 0.0f);
	private static Vector3 rightAnchor = new Vector3(100.0f, 130.0f, 0.0f);
	private static Vector3 centerAnchor = new Vector3(0.0f, 150.0f, 0.0f);
	
	protected override void Run ()
	{
		
		//if ( targetTravelDirection != travelDirection ) Debug.Log ("tar: " + targetTravelDirection + " dir: " + travelDirection);
		
		if (isCritical)
		{
			
		} else {
			if (!IsStunned) {
				if (isLockingOn)
				{

					if(beamDir == 0) {
						targetTravelDirection = KJMath.DirectionFromPosition(transform.position, GameController.PlayerUnit.transform.position);
							
						if (targetTravelDirection > Mathf.PI) {
							targetTravelDirection += 0.5f;
							beamDir = -1;
						} else {
							targetTravelDirection -= 0.5f;
							beamDir  = 1;	
						}
					}

					targetTravelDirection += 0.007f * beamDir;
					travelDirection = ConvergeAngleTo(travelDirection, targetTravelDirection, 0.12f, 0.01f);
					speed = KJMath.ConvergeToTarget(speed, 12.0f, 0.01f, 1.0f);
					
				} else {
					
					Vector2 travelVector;
					
					if (squad == null) {
						
						travelVector = new Vector2(0.0f, -50.0f);
						targetTravelDirection = Mathf.PI;
						
					} else {
						
						travelVector = squad.TravelVectorForUnit(this);
						targetTravelDirection = KJMath.DirectionFromVector(travelVector);
						
					}	
					
					if (travelDirection != targetTravelDirection) travelDirection = ConvergeAngleTo(travelDirection, targetTravelDirection, 0.05f, 0.01f);				
					speed =  KJMath.Cap(travelVector.magnitude * 2.0f, 0, maxSpeed);

				}
			}
		}
		if (faceToPlayer && !isLockingOn){
			if (!isElite) {

				float turnSpeed = 0.25f;
				Vector3 proxyPlayerTarget = new Vector3 ( GameController.PlayerUnit.transform.position.x * 0.35f,  GameController.PlayerUnit.transform.position.y, 0.0f);
				float directionToPlayer = KJMath.DirectionFromPosition(transform.position, proxyPlayerTarget);
				faceDirection = ConvergeAngleTo(faceDirection, directionToPlayer, turnSpeed, 0.01f);
	
				//faceDirection = KJMath.DirectionFromPosition(transform.position, GameController.PlayerUnit.transform.position);
			}
		} else {
			faceDirection = travelDirection;
		}

		if (secondaryWeapon != null ) secondaryWeapon.direction = faceDirection;
		base.Run();

//		if (hasRun) Debug.Log("Double Run");
//		hasRun = true;
		
		if (y < -400.0f) Deactivate();
		
	}

	public void SwitchToSelfControl ()
	{
		//Debug.Log ("Switch To Self Control");
		RemoveTimerOrFlash (Run);
		AddTimer (RunElite);
		AddTimer (SwitchSide, 2.5f);
		AddTimer (SwitchRoam, 3.5f);

		currentAnchor = centerAnchor;

		if (KJDice.Roll(50)) 
			eliteSide = EliteSide.Left; 
		else 
			eliteSide = EliteSide.Right;

		SwitchRoam();
	}

	void SwitchRoam ()
	{
		if (roamSide == EliteSide.Left)
		{
			roamSide = EliteSide.Right;
		} else {
			roamSide = EliteSide.Left;
		}

		offsetFactorAmount = Random.value * 10.0f;
		offsetXFactor = Random.Range (0.5f, 1.5f);
		offsetDistance = Random.Range (offsetDistanceMax * 0.5f, offsetDistanceMax);
		AddTimer (SwitchRoam, Random.Range (0.85f, 2.0f));
	}

	void RunElite ()
	{
		if (!isElite) RemoveEliteTimers();

		float turnSpeed = 0.0f;
		if (isLockingOn) {
			turnSpeed = 0.075f;
		} else {
			turnSpeed = 0.1f;
		}
		Vector3 proxyPlayerTarget = new Vector3 ( GameController.PlayerUnit.transform.position.x * 0.35f,  GameController.PlayerUnit.transform.position.y, 0.0f);
		float directionToPlayer = KJMath.DirectionFromPosition(transform.position, proxyPlayerTarget);
		faceDirection = ConvergeAngleTo(faceDirection, directionToPlayer, turnSpeed, 0.01f);
		offsetFactorAmount += offsetFactorIncrease;

		if (roamSide == EliteSide.Left) {
			offsetAnchor.x = Mathf.Sin (offsetFactorAmount) * offsetXFactor;
			offsetAnchor.y = Mathf.Cos (offsetFactorAmount);
		} else {
			offsetAnchor.x = Mathf.Cos (offsetFactorAmount) * offsetXFactor;
			offsetAnchor.y = Mathf.Sin (offsetFactorAmount);
		}
		if (secondaryWeapon != null ) secondaryWeapon.direction = faceDirection;

		SetTargetPoint(currentAnchor + offsetAnchor * offsetDistance);
		RunTargetedMovement(faceDirection);
		base.Run ();

		// Hard Code the Unit from Disappearing off the screen
		if (x > 250) x = 250;
		if (x < -250) x = -250;
		if (y > 280) y = 280;

	}

	void SwitchSide ()
	{
		// Used by elite units to alternate its position.
		if (eliteSide == EliteSide.Left)
		{
			eliteSide = EliteSide.Right;
			currentAnchor = rightAnchor;
			currentAnchor.x += Random.Range(-15.0f, 15.0f);
			currentAnchor.y += Random.Range(-15.0f, 15.0f);
			if (!WaveController.DoubleElite && !isSocial && !isRival) {
				if(!isQuestEliteBoss) WaveController.BurstSpawnSubSquad(-1);
			}

		} else {
			eliteSide = EliteSide.Left;
			currentAnchor = leftAnchor;
			currentAnchor.x += Random.Range(-15.0f, 15.0f);
			currentAnchor.y += Random.Range(-15.0f, 15.0f);
			if (!WaveController.DoubleElite && !isSocial && !isRival) {
				if(!isQuestEliteBoss) WaveController.BurstSpawnSubSquad(1);
			}
		}



		AddTimer (SwitchSide, Random.Range (3.0f, 4.5f));
		AddTimer (SwitchRoam, Random.Range (0.85f, 2.0f));

	}

	public void DeployAsRival ()
	{

		this.isElite = true;
		this.isSocial = true;

		RivalData rival = ProfileController.Rival;

		// Set the stats.
		stats.life = rival.stats.life * 30.0f;
		stats.damage = rival.stats.damage * 0.70f;
		stats.armor = rival.stats.armor * 0.65f;

		maxVelocity = maxSpeed = 850.0f;

		// Set the ship graphic.
		ItemShip item = ItemPattern.ShipList[rival.shipId];
		animator.Play(item.shipClip);
		if (item.moduleId != 0) {
			AddModule(item.moduleId, 1.0f, 1.0f);
		}

		boosterEmitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		boosterEmitter.CopyFromModel(EffectController.Singleton.shipBooster);
		boosterEmitter.SetColor(item.boosterColorStart, item.boosterColorEnd);
		boosterEmitter.SetObjectLink(boosterObject);
		boosterEmitter.SetDirection(0);
		boosterEmitter.Deploy();
		
		boosterEmitter2 = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		boosterEmitter2.CopyFromModel(EffectController.Singleton.shipBoosterStay);
		boosterEmitter2.SetColor(item.boosterColorStart, item.boosterColorEnd);
		boosterEmitter2.SetObjectLink(boosterObject);
		boosterEmitter2.SetIsContained(true);
		boosterEmitter2.Deploy();

		// EnemyPattern newEnemyPattern = new EnemyPattern();
		Deploy(new Vector2(0.0f, 300.0f), WeaponPattern.StandardYellow);

		// Set the modules.
		foreach (ModuleData moduleData in rival.moduleDataList)
		{
			AddModule(moduleData.moduleId, moduleData.moduleLevel, moduleData.modulePower);
		}

		UIController.UpdateBossBar(LifePercent, stats.lifePoints.ToString("F0"));
		SwitchToSelfControl();

	}

	void UnlockFire () {
		fireLock = false; // Allow the elite to shoot again.
	}

	public void DeployAsNormal (Vector2 position2d, EnemyPattern enemyPattern = null, bool isElite = false, bool isQuestBoss = false, bool isRival = false)
	{
//		hitRadiusSq = 20 * 20;
		this.isElite = isElite;
		this.isQuestEliteBoss = isQuestBoss;
		this.isRival = isRival;
		fireLock = false;

		animator.Stop ();
		SetImage(enemyPattern.image);

		float statFactor = Stats.NGVForLevel(WaveController.Level);

		stats.life = 1.0f + enemyPattern.lifeMod * 1.1f * Stats.EnemyDifficultyFactor(0.3f, WaveController.Level) * statFactor;
		stats.damage = 2.0f + enemyPattern.damageMod * 0.75f * Stats.EnemyDifficultyFactor(0.35f, WaveController.Level) * statFactor;
		stats.armor =  1.0f + enemyPattern.armorMod * 0.85f * statFactor;

		if (isElite) {
			if (WaveController.DoubleElite) {
				stats.life *= 2.0f;
				stats.damage *= 1.35f;
			} else {
				if (isQuestBoss) {
					enemyPattern.primaryWeapon = WeaponPattern.PrimaryWeaponElite();
					enemyPattern.secondaryWeapon = WeaponPattern.secondaryWeaponElite();
					stats.life *= 7.5f;
					stats.damage *= 2.0f;
				} else {
					stats.life *= 2.5f;
					stats.life += 20.0f;
					stats.damage *= 2.0f;
				}
			}

			if (isRival) {
				enemyPattern.moduleId = 401;
				enemyPattern.primaryWeapon = WeaponPattern.PrimaryWeaponRival();
				enemyPattern.secondaryWeapon = WeaponPattern.secondaryWeaponRival();
				stats.life *= 0.50f;
				stats.damage *= 0.50f;
			}

		} else {
			// Adjusting for Staring level.
			stats.damage += 4.0f;
		}

		if (NetworkController.Singleton.isMultiplayer)
		{
			stats.life *= 2.5f;	
			stats.damage *= 1.5f;
		}
		maxSpeed = enemyPattern.speed;
		if (isElite) maxVelocity = maxSpeed;

		Deploy(position2d, enemyPattern.primaryWeapon);

		if (isElite) {
			//Debug.Log ("Deploy Elite");

			KJMath.SetY(boosterObject.transform, -30);
//			ItemShip item = ItemPattern.ShipList[3];
//			hitRadiusSq = 50 * 50;

			Color flareStartColor = new Color (1.0f, 0.8f, 0.3f, 1.0f);
			Color flareEndColor = new Color (1.0f, 0.0f, 0.6f, 1.0f);

			List<string> eliteNames = new List<string>(new string[]{"p_elite1", "p_elite2", "p_elite3", "p_elite4", "p_elite5", "p_elite6"});
			List<string> eliteDebrisName = new List<string>(new string[]{"Elite1", "Elite2", "Elite3", "Elite4", "Elite5", "Elite6"});
			List<string> eliteSuffix = new List<string>(new string[]{"_blue", "_yellow"});

			int diceIndex = KJDice.RandomIndexOf(eliteNames);
			debrisSpriteName = eliteDebrisName[diceIndex];

			string eliteAnimationName = eliteNames[diceIndex];

			if (isRival) {

				fireLock = true;
				KJMath.SetY(boosterObject.transform, -10);
				animator.Play("p_rival");
				AddTimer( UnlockFire, 6.0f, 1);

			} else {

				if (isQuestBoss) {
					
					eliteAnimationName = KJDice.Roll(50) ? "p_elite_royal" : "p_elite_royal2";
					debrisSpriteName = "Elite1_Royal";
					
				} else {
					
					if (ProfileController.SectorId > 15) {
						//eliteAnimationName += "_blue";
						
						diceIndex = KJDice.RandomIndexOf(eliteSuffix);
						eliteAnimationName += eliteSuffix[diceIndex];
						
					}
					
				}

				animator.Play(eliteAnimationName);

			}


			boosterEmitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
			boosterEmitter.CopyFromModel(EffectController.Singleton.shipBooster);
			boosterEmitter.SetColor(flareStartColor, flareEndColor);
			boosterEmitter.SetObjectLink(boosterObject);
			boosterEmitter.SetDirection(0);
			boosterEmitter.Deploy();
			
			boosterEmitter2 = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
			boosterEmitter2.CopyFromModel(EffectController.Singleton.shipBoosterStay);
			boosterEmitter2.SetColor(flareStartColor, flareEndColor);
			boosterEmitter2.SetObjectLink(boosterObject);
			boosterEmitter2.SetIsContained(true);
			boosterEmitter2.Deploy();

			// Aggregate Elite HP and update
			if (isElite) {
				
				float finalPercent = 0.0f;
				int numOfElites = 0;
				float finalLife = 0;
				
				foreach (Unit unit in EnemyUnit.allActive)
				{
					if (unit.isElite) {
						finalPercent += unit.stats.LifePercent;
						finalLife += unit.stats.lifePoints;
						numOfElites ++;
					}
				}
				
				UIController.UpdateBossBar(finalPercent / numOfElites, finalLife.ToString("F0"));
			}

		} else {

			KJMath.SetY(boosterObject.transform, -13);
			debrisSpriteName = GetNameOfImage(enemyPattern.image);
		}

		if (enemyPattern.secondaryWeapon != null) {
			secondaryWeapon = gameObject.AddComponent<Weapon>();
			secondaryWeapon.CopyDataFrom(enemyPattern.secondaryWeapon);
			secondaryWeapon.Deploy(this);
			secondaryWeapon.StartShooting();
			if (WaveController.DoubleElite) {
				secondaryWeapon.Cooldown += 1.2f;
				weapon.Cooldown += 1.2f;
			}
		}

		if (enemyPattern.moduleId != 0) AddModule(enemyPattern.moduleId, WaveController.Level, 1.0f);

		if (isElite) {
			animator.Play();
		}

		if(enemyPattern.isFaceToPlayer){
			faceToPlayer = true;
		}
	}

	
	public void Deploy (Vector2 position2d, Weapon weapon)
	{
		WaveController.allEnemy++;

		x = position2d.x;
		y = position2d.y;
		transform.position = new Vector3(position2d.x, position2d.y, 0.0f);
		speed = 0.0f;
		
		targetTravelDirection = travelDirection = Mathf.PI + (Mathf.PI * Random.value * 0.2f) - Mathf.PI * 0.1f;
		faceDirection = travelDirection;
		
		// Primary Weapon

		this.weapon.CopyDataFrom( weapon );
		this.weapon.Deploy(this);
		this.weapon.StartShooting();
		
		// Secondary Weapon
		if (secondaryWeapon != null )
		{
			secondaryWeapon.Terminate();
			Destroy(secondaryWeapon);
		}

		if (isElite) {
			offsetAnchor = new Vector3();
			SetTargetedMoveValues(0.90f);
		}

		
		stats.Calibrate();
		base.Deploy();
		AddTimer (Run);

	}

	private static int _HitYLimit = 0;
	private static int HitYLimit {
		get {
			// The unit's Y needs to be under this number for it to get hit.
			if (_HitYLimit == 0) {
				tk2dCamera camera = Camera.main.GetComponent<tk2dCamera>();
				_HitYLimit = (int) camera.ScreenExtents.yMax + 15;
			}
			return _HitYLimit;
		}
	}

	public override void TakeHit (float damage = 25.0f, Entity.DamageType damageType = Entity.DamageType.Normal, bool isCrit = false, bool ignoreArmor = false)
	{

		if (y < HitYLimit) {
			// Unit must be under a certain Y for it to be able to take damage.
			ExternalHit(0.0f);
			if (buffAcid.IsActive) damage += buffAcid.power;
			TakeDamage(damage, damageType, isCrit, false, ignoreArmor);
		}

		if (isSocial) UIController.UpdateBossBar(stats.LifePercent, stats.lifePoints.ToString("F0"));
	}
	
	void ChangeToNormalColor ()	
	{
		if (isCritical) return;

		sprite.color = Color.Lerp(sprite.color, currentColor, 0.15f);
		
		/*
		if (sprite.color.r < 0.05f) {
			sprite.color = currentColor;
			RemoveTimerOrFlash (ChangeToNormalColor); 
		}
		*/
	}
	
	protected override void Explode ()
	{

		if (hasBeenExploded) return;
		hasBeenExploded = true;

		if (isElite && isEliteCanExplode == false) {
			externalSpin = 0.3f;
			Flash (15);
			AddTimer (ExplodeBurst, 0.25f, 25);
			AddTimer (EliteActualExplode, 1.5f, 1);

			return;
		}

		if (!isActive) return;

		WaveController.allEnemyDie++;

		if (KJDice.Roll(5))
		{
			SpawnCollectable();
		}

		if (isElite) {
			SpawnCollectable();
			KJTime.Add (WaveController.ResumeWhenElitesCleared, 0.5f);
		} 

		if (WaveController.type == WaveController.Type.Quest_Bonus) {
			SpwanGem();
		}

		base.Explode();

	}

	void SpwanGem () {
		GameObject collectableObject;
		Collectable collectable;

		int randomGem = 0;
		if (ProfileController.SectorLevel < 5) {
			randomGem = Random.Range(0, Random.Range(0, 3));
			if (isElite) { 
				randomGem = Random.Range(3, 5);
			}
		} else {
			randomGem = Random.Range(0, Random.Range(0, 3));
			if (isElite) { 
				float maxR = 3.0f + Stats.FactorForLevel(ProfileController.SectorLevel) * 2.5f;
				float minR = 1.0f + Stats.FactorForLevel(ProfileController.SectorLevel);
				randomGem = (int)System.Math.Ceiling((ProfileController.SectorLevel + Random.Range(minR, maxR)) / Random.Range(2, 5)) + Random.Range(2, 5);
			}
		}
		if (randomGem > 20) randomGem = 20;

		if (isQuestEliteBoss && isElite) {

			randomGem += (int)System.Math.Ceiling((Stats.FactorForLevel(ProfileController.SectorLevel) * 40.5f) + Random.Range(15, 25));
			collectable = KJActivePool.GetNewOf<Collectable>("Collectable");
			collectable.Deploy(Collectable.Type.Cargo, x + Random.Range (-20.0f , 20.0f), y + Random.Range (-20.0f , 20.0f), false); 
		}

		for (int i = 0; i < randomGem; i++ ) {
			collectable = KJActivePool.GetNewOf<Collectable>("Collectable");
			collectable.Deploy(Collectable.Type.Gem, x + Random.Range (-20.0f , 20.0f), y + Random.Range (-20.0f , 20.0f), false); 
		}
	}

	void SpawnCollectable ()
	{
		Collectable collectable = KJActivePool.GetNewOf<Collectable>("Collectable");
		
		Collectable.Type collectableType;
		
		float randomFloat = Random.value;
		
		// Maybe write another function later to adjust color bias.
		
		if (randomFloat > 0.75f) {
			
			collectableType = Collectable.Type.Green;
			
		} else if (randomFloat > 0.5f) {
			
			collectableType = Collectable.Type.Pink;
				
		} else if (randomFloat > 0.25f) {
			
			collectableType = Collectable.Type.Blue;
			
		} else {
			
			collectableType = Collectable.Type.Yellow;
			
		}
		
		collectable.Deploy(collectableType, x, y);
	}
	
	protected override void EnterCriticalState ()
	{
		if (!isActive) return;

		float blastDir;// = Random.value * KJMath.CIRCLE;
		hasLostControl = true;
		stats.lifePoints = 0;
		speed = 50.0f;


		if (isElite) {



			blastDir = (Random.value * Mathf.PI) + KJMath.RIGHT_ANGLE;
			velocityX = velocityY = speed = maxSpeed = 0;

			externalVelocityX += Mathf.Sin (blastDir) * 15.0f;
			externalVelocityY = -50.0f;

			externalSpin = 0.01f;

			RemoveTimerOrFlash (RunElite);
			RemoveTimerOrFlash (SwitchSide);
			RemoveTimerOrFlash (SwitchRoam);
			// AddTimer (Run);

		} else {

			blastDir = Random.value * KJMath.CIRCLE;
			externalVelocityX += Mathf.Sin (blastDir) * 70.0f;
			externalVelocityY += Mathf.Cos (blastDir) * 70.0f;

		}

		base.EnterCriticalState();	
	}
	
	void ExternalHit (float fromDirection)
	{
		hasLostControl = true;
		externalVelocityY += 50.0f;
		float spinMagnitude = 0.15f;
		externalSpin = Random.Range(-spinMagnitude, spinMagnitude);
	}

	bool IsSafeDistance {
		get {
			float distanceSquared = KJMath.DistanceSquared2d(gameObject, GameController.PlayerUnit.gameObject);
			if (distanceSquared < 6400) /* 80 * 80 */ return false;
			else return true;
		}
	}
	
	public override bool IsAbleToFire
	{
		get{

			if (fireLock) return false;
			if (buffStun.IsActive) return false;
			if ((!isElite) && (y < - 60 || !IsSafeDistance)) return false;	
			if (squad != null) return squad.IsAbleToFire;
			if (!GameController.PlayerUnit.IsAlive) return false;

			return true;
		}
	}

	public override bool IsAbleToFireBasic
	{
		get{
			
			if (buffStun.IsActive) return false;
			if ((!isElite) && (y < - 50 || !IsSafeDistance)) return false;
			
			return true;
		}
	}
	
	protected override void OnActivate ()	
	{
		if (!allActive.Contains(this)) allActive.Add(this);
		base.OnActivate();
	}
	
	protected override void OnDeactivate ()
	{
		if (squad != null) squad.RemoveUnitFromSquad(this);
		if (isElite) RemoveEliteTimers();

		allActive.Remove(this);
		base.OnDeactivate();
	}

	void RemoveEliteTimers ()
	{
		RemoveTimerOrFlash (RunElite);
		RemoveTimerOrFlash (SwitchSide);
		RemoveTimerOrFlash (SwitchRoam);
	}
	
	public void ReleaseFromSquad ()
	{
		if (y < -400.0f)
		{
			Terminate();
		} else {
			if (isElite) SwitchToSelfControl();
			squad = null;	
		}
	}
	
	public void Terminate ()
	{
		Deactivate();
	}
}
