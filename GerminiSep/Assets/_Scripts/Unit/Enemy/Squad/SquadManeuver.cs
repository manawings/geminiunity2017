using UnityEngine;
using System.Collections;

public class SquadManeuver
{
	
	public static SquadManeuver STRAIGHT;
	public static SquadManeuver CURVE;
	public static SquadManeuver LONE;
	public static SquadManeuver SUPPORT;
	
	// Settings
	public float mainSpeed = 65.0f, maxSpeed = 850.0f, direction = 3.142f;
	public float weaponCooldown = 1.0f;
	public float turnSpeed = 0.6f; // Radians per second.
	public float squadSpawnGap = 1.0f; // Time gap between squad spawns in seconds.
	public float entryPointX = 0, entryPointY = 0;
	public float entryBoundX = 200, entryBoundY = 270;
	public bool isRoaming = false;
	
	public static void Initialize ()
	{
		// Create all the static Squad Maneuvers.
		STRAIGHT = new SquadManeuver();
		STRAIGHT.direction = Mathf.PI;		
		STRAIGHT.turnSpeed = 0;
		STRAIGHT.entryPointY = 600.0f;
		STRAIGHT.entryPointX = 0.0f;
		STRAIGHT.mainSpeed = 65.0f;
		STRAIGHT.weaponCooldown = 0.5f;
			
		CURVE = new SquadManeuver();
		CURVE.direction =  Mathf.PI * 1.35f;
		CURVE.entryPointY = 280.0f;
		CURVE.entryPointX = 380.0f;
		CURVE.mainSpeed = 65.0f;
		CURVE.turnSpeed = - 0.25f;
		CURVE.weaponCooldown = 0.5f;
		CURVE.entryBoundY = 220;
		CURVE.entryBoundX = 200;
			
		LONE = new SquadManeuver();
		LONE.direction =  Mathf.PI;	
		LONE.turnSpeed = - 0.0f;
		LONE.entryPointY = 600.0f;
		LONE.entryPointX = 100.0f;
		LONE.mainSpeed = 65.0f;	
		LONE.weaponCooldown = 0.5f;
		LONE.isRoaming = true;

		SUPPORT = new SquadManeuver();
		SUPPORT.direction = Mathf.PI + KJMath.ANGLE_45;		
		SUPPORT.turnSpeed = 0.0f;
		SUPPORT.entryPointY = 280.0f;
		SUPPORT.entryPointX = 260.0f;
		SUPPORT.mainSpeed = 65.0f;
		SUPPORT.weaponCooldown = 0.5f;
		SUPPORT.entryBoundY = 260;
		SUPPORT.entryBoundX = 300;

	}
	
}

