using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SquadFormation
{
	public List<SquadSlot> slots;
	
	public enum Type
	{
		Wedge,
		ReverseWedge,
		Line,
		Column,
		Entourage
	}
	
	public SquadFormation (int count = 1, Type type = Type.Wedge, int unitGap = 85)
	{
		if (count < 1) return; // Count must be over 0.
		
		
		slots = new List<SquadSlot>();
		
		if (count == 1)
		{
			slots.Add (new SquadSlot(0, 0));
		} else {
			
			float unitPosition;
			float useDir;
			
			switch (type)
			{
				
			case Type.Entourage:
				
				float squadGap = KJMath.CIRCLE / count;
				while (count != 0)
				{
					count --;
					slots.Add (new SquadSlot(count * squadGap, unitGap));
				}
				
				break;
				
			
			case Type.Column:
				
				float unitTotalLength = (unitGap * (count - 1));
				float unitOrigin = - unitTotalLength / 2;
						
				while (count != 0) {
							
					count --;
							
					unitPosition = unitOrigin + unitGap * count;
					useDir = unitPosition < 0 ? 0 : Mathf.PI;
					slots.Add(new SquadSlot(useDir, Mathf.Abs(unitPosition)));			
				}
				
				break;
				
				
			case Type.Wedge:
				
				unitTotalLength = (unitGap * (count - 1));
				unitOrigin = - unitTotalLength / 2;
						
				while (count != 0) {
							
					count --;
							
					unitPosition = unitOrigin + unitGap * count;
					useDir = unitPosition < 0 ? - KJMath.RIGHT_ANGLE / 2 : KJMath.RIGHT_ANGLE / 2;
					useDir += Mathf.PI;
					slots.Add(new SquadSlot(useDir, Mathf.Abs(unitPosition)));
							
				}
				break;

			case Type.ReverseWedge:
				
				unitTotalLength = (unitGap * (count - 1));
				unitOrigin = - unitTotalLength / 2;
				
				while (count != 0) {
					
					count --;
					
					unitPosition = unitOrigin + unitGap * count;
					useDir = unitPosition < 0 ? - KJMath.RIGHT_ANGLE / 2 : KJMath.RIGHT_ANGLE / 2;
					slots.Add(new SquadSlot(useDir, Mathf.Abs(unitPosition)));
					
				}
				
				break;
				
				
			case Type.Line:
				
				unitTotalLength = (unitGap * (count - 1));
				unitOrigin = - unitTotalLength / 2;
						
				while (count != 0) {
							
					count --;
							
					unitPosition = unitOrigin + unitGap * count;
					useDir = unitPosition < 0 ? - KJMath.RIGHT_ANGLE : KJMath.RIGHT_ANGLE;
					slots.Add(new SquadSlot(useDir, Mathf.Abs(unitPosition)));
							
				}
				
				break;
				
			}	
		}
	}
}

