using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class WaveData {

	public SquadFormation.Type formation;
	public int size;
	public SquadManeuver maneuver;
	public List<EnemyPattern> unitPatterns;
	public int tutorialId;

	// Wave Patterns
	List<List<Mode>> wavePatterns2;
	List<List<Mode>> wavePatterns3;
	List<List<Mode>> wavePatterns4;
		
	// Wave Modes. Can be one of these values.
	public enum Mode
	{
		NONE,
		LIGHT,
		MEDIUM,
		HEAVY,
		REWARD
	}
		
	List<SquadFormation.Type> formationTypes;
	List<SquadManeuver> maneuverTypes;
	List<int> numTypes;
	
	
	public WaveData ()
	{
		wavePatterns2 = new List<List<Mode>>();
		wavePatterns3 = new List<List<Mode>>();
		wavePatterns4 = new List<List<Mode>>();
		
		wavePatterns2.Add( CreateModeList(Mode.LIGHT, Mode.HEAVY) );
		wavePatterns2.Add( CreateModeList(Mode.MEDIUM, Mode.MEDIUM) );
		wavePatterns2.Add( CreateModeList(Mode.HEAVY, Mode.LIGHT) );
		wavePatterns2.Add( CreateModeList(Mode.MEDIUM, Mode.HEAVY) );
		
		wavePatterns3.Add( CreateModeList(Mode.LIGHT, Mode.HEAVY, Mode.REWARD) );
		wavePatterns3.Add( CreateModeList(Mode.LIGHT, Mode.MEDIUM, Mode.LIGHT) );
		wavePatterns3.Add( CreateModeList(Mode.MEDIUM, Mode.MEDIUM, Mode.REWARD) );
		wavePatterns3.Add( CreateModeList(Mode.HEAVY, Mode.LIGHT, Mode.REWARD) );
		
		wavePatterns4.Add( CreateModeList(Mode.MEDIUM, Mode.HEAVY, Mode.LIGHT, Mode.REWARD) );
		wavePatterns4.Add( CreateModeList(Mode.LIGHT, Mode.MEDIUM, Mode.HEAVY, Mode.REWARD) );
		wavePatterns4.Add( CreateModeList(Mode.MEDIUM, Mode.LIGHT, Mode.HEAVY, Mode.REWARD) );
		wavePatterns4.Add( CreateModeList(Mode.LIGHT, Mode.HEAVY, Mode.MEDIUM, Mode.REWARD) );
		
		formationTypes = new List<SquadFormation.Type> { SquadFormation.Type.Line, SquadFormation.Type.Wedge, SquadFormation.Type.ReverseWedge, SquadFormation.Type.Entourage};
		maneuverTypes = new List<SquadManeuver> { SquadManeuver.CURVE, SquadManeuver.STRAIGHT }; 
		numTypes = GetSpawnListForLevel(WaveController.Level);
	}
	
	List<Mode> CreateModeList (Mode mode1, Mode mode2 = Mode.NONE, Mode mode3 = Mode.NONE, Mode mode4 = Mode.NONE)
	{
		List<Mode> tempList = new List<Mode>();
		tempList.Add(mode1);

		if (mode2 != Mode.NONE) tempList.Add(mode2);
		if (mode3 != Mode.NONE) tempList.Add(mode3);
		if (mode4 != Mode.NONE) tempList.Add(mode4);
		
		return tempList;
	}
	
	public void Calibrate (float waveMod = 1.0f, bool isElite = false, bool useSeed = true, bool sideIsOnlySupport = false)
	{
		
		if (NetworkController.Singleton.isMultiplayer) Random.seed = NetworkController.Singleton.currentRandomSeed;

		if (useSeed) {
			formation = KJDice.RandomMemberOf<SquadFormation.Type>(formationTypes);
			size = KJDice.RandomMemberOf<int>(numTypes) + (int) ( waveMod - 2 );
			maneuver = KJDice.RandomMemberOf<SquadManeuver> (maneuverTypes);
		} else {
			formation = KJMath.RandomMemberOf<SquadFormation.Type>(formationTypes);
			size = KJMath.RandomMemberOf<int>(numTypes) + (int) ( waveMod - 2 );
			maneuver = KJMath.RandomMemberOf<SquadManeuver> (maneuverTypes);
		}


		float dangerValue = 4.0f + (size * 0.35f);//  + KJMath.Cap(WaveController.Level / 3.0f, 0.0f, 3.0f);

		// Populate the spawn array
		unitPatterns = new List<EnemyPattern>();
		while (unitPatterns.Count < 5)
		{
			EnemyPattern unitPattern = null;
			bool isValidEnemy = false;
			int tries = 0;

			//Test enemy
			if(WaveController.TestEnemy) {
				size = 1;
				unitPatterns.Add(WaveController.TestEnemyPattern);
				continue;
			}

			if (sideIsOnlySupport && unitPatterns.Count > 0) {
				unitPatterns.Add(EnemyPattern.Support);
				continue;
			}

			while (!isValidEnemy) {

				if (useSeed) {
					unitPattern = KJDice.RandomMemberOf<EnemyPattern>(EnemyPattern.GetEligablePatternsForLevel(WaveController.Level, dangerValue, isElite));
				} else {
					unitPattern = KJMath.RandomMemberOf<EnemyPattern>(EnemyPattern.GetEligablePatternsForLevel(WaveController.Level, dangerValue, isElite));
				}

				if (unitPattern.isSingleUnit == true) {
					if (unitPatterns.Count == 0) {
						if ( size % 2 == 0 ) size ++;
						isValidEnemy = true;
					}
				} else {
					isValidEnemy = true;
				}

				tries ++;
				if (tries > 15) isValidEnemy = true;
			}

//			//__Test Log Enemy
//			string exeFolder = System.IO.Path.GetDirectoryName(Application.dataPath);
//			if(unitPattern.name != null) {
//				//Debug.Log(unitPattern.name);
//				using(StreamWriter writer = new StreamWriter(exeFolder + "\\Log\\" + ProfileController.SectorId + "." + ProfileController.SubSector +"_"+ WaveController.type.ToString() +".txt", true))
//				{
//					writer.WriteLine(WaveController.type.ToString() + "##" + unitPattern.name);
//				}
//			}
//			//__End Test

			if(unitPattern.name != null) {
				if (unitPattern.name == "BlackHole") {
					KJTime.Add(TutorialDeploy, 1.5f , 1);
					tutorialId = 28;
				} else if (unitPattern.name == "Chainer1" || unitPattern.name == "Chainer2" || unitPattern.name == "Stunner") {
					KJTime.Add(TutorialDeploy, 1.5f , 1);
					tutorialId = 30;
				}
			}

			unitPatterns.Add(unitPattern);
			dangerValue -= unitPattern.dangerValue;
		}
	}

	void TutorialDeploy () {
		TutorialData.Deploy(tutorialId);
	}

	public void OverridePatterns (EnemyPattern enemyPattern)
	{
		for (int i = 0 ; i < unitPatterns.Count ; i ++ )
		{
			unitPatterns[i] = enemyPattern;
		}
	}
	
	List<int> GetSpawnListForLevel (float level)
	{
		if (level < 5.0f) return new List<int> { 3, 3, 3, 4 };
		
		if (level < 30.0f) return new List<int> { 3, 3, 4 };
		
		if (level < 60.0f) return new List<int> { 3, 4 };
		
		return new List<int> { 3, 4 };
	}
	
	public List<Mode> GetWavePatternForCount (int count)
	{
	
		if (count == 2) return KJMath.RandomMemberOf<List<Mode>>(wavePatterns2);
		if (count == 3) return KJMath.RandomMemberOf<List<Mode>>(wavePatterns3);
		if (count == 4) return KJMath.RandomMemberOf<List<Mode>>(wavePatterns4);
		
		return null;
		
	}
}
