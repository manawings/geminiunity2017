using UnityEngine;
using System.Collections;

public class SquadSlot
{
	public float direction;
	public float radius;
	public float x, y;
	
	public SquadSlot (float direction, float radius)
	{
		this.direction = direction;
		this.radius = radius;
		
		x = Mathf.Sin(direction) * radius;
		y = Mathf.Cos(direction) * radius;
	}
}

