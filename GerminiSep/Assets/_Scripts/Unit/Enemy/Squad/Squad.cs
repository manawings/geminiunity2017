using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Squad : KJBehavior
{
	
	// Reference Variables
	public SquadFormation.Type formationType;
	public SquadFormation squadFormation;
	public List<EnemyUnit> units;
	SquadManeuver squadManeuver;
	Dictionary<EnemyUnit, SquadSlot> slots;
	// Dictionary<EnemyUnit, GameObject> slotPointers;
	
	// private var slots:Dictionary;
		
	// Operational Variables
	float direction = Mathf.PI;
	float speed = 5.0f;
	float weaponsCooldown = 1.5f;
	float turnSpeed = 0.0f;
	float x, y;
	bool isElite = false;
	bool isQuestBoss = false;
	bool isRival = false;
		
	// Aesthetic Variables
	bool isBoostingOut = false;
	bool isInMainPhase = false;
	float spreadFactor = 1.0f;
	int approachDir = 1;
	
	// private const int yWarpBound = 270, xWarpBound = 200;
	private const float warpConverge = 0.15f;


	public void Deploy (SquadManeuver squadManeuver, SquadFormation.Type formationType, int squadNum = 1, 
	                    List<EnemyPattern> squadPattern = null, int approachDir = 1, bool isElite = false , bool isQuestBoss = false, bool isRival = false)
	{
		// First Reset All To Default
		turnSpeed = 0.0f;
		direction = Mathf.PI;
		speed = 5.0f;
		weaponsCooldown = 1.5f;
		turnSpeed = 0.0f;
		x = y = 0;
		spreadFactor = 1.0f;
		transform.position = Vector3.zero;

		this.squadManeuver = squadManeuver;
		this.approachDir = approachDir;
		this.formationType = formationType;
		this.isElite = isElite;
		this.isQuestBoss = isQuestBoss;
		this.isRival = isRival;

		if (squadNum < 3) formationType = SquadFormation.Type.Line;

		squadFormation = new SquadFormation(squadNum, formationType, 85);
		units = new List<EnemyUnit>();
		slots = new Dictionary<EnemyUnit, SquadSlot>();
		// slotPointers = new Dictionary<EnemyUnit, GameObject>();
		
		Vector2 directionVector = new Vector2();
		directionVector.x = Mathf.Sin(squadManeuver.direction) * approachDir;
		directionVector.y = Mathf.Cos(squadManeuver.direction);
		direction = KJMath.DirectionFromVector(directionVector);
			
		x = squadManeuver.entryPointX * approachDir;
		y = squadManeuver.entryPointY;
		
		if (squadManeuver == SquadManeuver.LONE)
		{
			x += Random.Range(-100, 100);
			// TODO: Multiplayer this will need to be changed.
			direction = KJMath.DirectionFromPosition(new Vector3(x, y, 0), GameController.PlayerUnit.transform.position);
		}
			
		weaponsCooldown = squadManeuver.weaponCooldown;
		speed = squadManeuver.maxSpeed;
		turnSpeed = squadManeuver.turnSpeed * approachDir;
		
		CalibrateSquadComposition(squadPattern, squadNum);
		
		AddTimer (RunEntry);
	}
	
	void CalibrateSquadComposition (List<EnemyPattern> squadPattern, float squadNum)
	{
		
		float squadMidPoint = (squadNum - 1) * 0.5f;
		// Debug.Log ("New Squad ------------------- ");
		// Debug.Log ("Squad Pattern: " + squadPattern.Count);
		
		for (int i = 0 ; i < squadFormation.slots.Count ; i ++)
		{	
			int useSpawnIndex = 0;
			if (isElite) {
				useSpawnIndex = i;
			} else {
				useSpawnIndex = (int) Mathf.Abs(i - squadMidPoint);
			}

			EnemyUnit enemyUnit = KJActivePool.GetNewOf<EnemyUnit>("EnemyUnit");
			SquadSlot squadSlot = squadFormation.slots[i];
				
			Vector2 startingPosition = new Vector2();
			startingPosition.x = x +  Mathf.Sin(direction - squadSlot.direction) * squadSlot.radius * 1.5f;// * 1.5;// + Math.sin(squadSlot.direction) * squadSlot.radius;// + KJMath.randomIntInRange( -30, 30);
			startingPosition.y = y + Mathf.Cos(direction - squadSlot.direction) * squadSlot.radius * 1.5f;// * 1.5;// - Math.cos(squadSlot.direction) * squadSlot.radius;// + KJMath.randomIntInRange( -30, 30);

			enemyUnit.DeployAsNormal(startingPosition, squadPattern[useSpawnIndex], isElite, isQuestBoss, isRival);
			enemyUnit.SetTargetDirection(direction, true);
			enemyUnit.squad = this;
			
			if (slots.ContainsKey(enemyUnit)) {

				//slots.Remove(enemyUnit); // TEMP: TODO: Fix
				//units.Remove (enemyUnit);

			} else {
				slots.Add(enemyUnit, squadSlot);
				units.Add(enemyUnit);
			}
		}
	}
	
	void RunEntry ()
	{

		isInMainPhase = false;
		Run ();
		
		if ((y < squadManeuver.entryBoundY) && (x < squadManeuver.entryBoundX) && (x > - squadManeuver.entryBoundX)) {

			speed = KJMath.ConvergeToTarget(speed, squadManeuver.mainSpeed, warpConverge, 1.0f);

			if (speed == squadManeuver.mainSpeed) {

				// Switch Mode.
				RemoveTimerOrFlash (RunEntry);
				AddTimer (RunMain);

				// If this is an elite unit, detach it from the squad.
				if (isElite) {
					for (int i = units.Count - 1; i > -1 ; i -- )
						  

					{
						EnemyUnit unit = units[i];
						if (unit.isElite) unit.ReleaseFromSquad();
					}
				}
				
			}
		}
	}
	
	void RunExit ()
	{
		isInMainPhase = false;
		speed = KJMath.ConvergeToTarget(speed, squadManeuver.maxSpeed, 0.1f, 1.0f);
		isBoostingOut = true;
		spreadFactor += 0.2f;
		Run ();
		
		if (y < - 600.0f || x > 1200.0f || x < -1200.0f) Terminate();
	}
	
	void RunMain ()
	{
		isInMainPhase = true;
		
		direction += turnSpeed * DeltaTime;
		
		if (y > -250.0f) // TODO: Change Values to match screen.
		{
			if (weaponsCooldown > 0)
			{
				weaponsCooldown -= DeltaTime;	
			}
		}
		
		Run();
		
		if (y < -140.0f)
		{
			RemoveTimerOrFlash (RunMain);
			AddTimer (RunExit);
		}
	}

//	void RunElite ()
//	{
//
//	}
//
//	void RunRegular ()
//	{
//
//	}

	float sinDir = 0, cosDir =0;
	
	void Run ()
	{
		sinDir = Mathf.Sin(direction);
		cosDir = Mathf.Cos(direction);
		x += sinDir * speed * DeltaTime;
		y += cosDir * speed * DeltaTime;
		
	}
	
	public bool IsAbleToFire
	{
		get {
			
			if (!isInMainPhase) return false;
			
			if (weaponsCooldown > 0)
			{
				return false;
			} else {
				weaponsCooldown = squadManeuver.weaponCooldown;
				return true;
			}
		}	
	}

	float slotX, slotY;	
	float slotDirection, sinSlot, cosSlot;
	
	public Vector2 TravelVectorForUnit (EnemyUnit enemyUnit)
	{

		SquadSlot slot;

		if (slots.TryGetValue(enemyUnit, out slot))

		{

			slotDirection = direction - slot.direction;

			sinSlot =  Mathf.Sin(slotDirection);
			cosSlot = Mathf.Cos(slotDirection);
			
			if (isBoostingOut) {
				slotX = x + sinSlot * slot.radius * spreadFactor;
				slotY = y + cosSlot * slot.radius + 100.0f;
			} else {
				slotX = x + sinSlot * slot.radius;
				slotY = y + cosSlot * slot.radius;
			}
				
			Vector2 travelVector = new Vector2();
			Vector2 correctionVector = new Vector2();
			Vector2 newVector = new Vector2();

			travelVector.x = sinDir * speed;
			travelVector.y = cosDir * speed;

			correctionVector.x = slotX - enemyUnit.x;
			correctionVector.y = slotY - enemyUnit.y;

			float correctionFactor = 0.75f * KJMath.Cap(correctionVector.sqrMagnitude / 25.0f, 0.0f, 1.0f);
			float travelFactor = 1.0f - correctionFactor;
				
			newVector.x = (correctionVector.x * correctionFactor + travelVector.x * travelFactor);
			newVector.y = (correctionVector.y * correctionFactor + travelVector.y * travelFactor);

			return newVector;

		} else {

			enemyUnit.ReleaseFromSquad();
			return new Vector2(0.0f, -50.0f);

		}
	}
	
	public Vector2 SlotPositionForUnit (EnemyUnit enemyUnit)
	{
		if (slots[enemyUnit] == null) return new Vector2();
		SquadSlot slot = slots[enemyUnit];
		float slotX, slotY;
		slotX = x + Mathf.Sin(direction - slot.direction) * slot.radius;
		slotY = y + Mathf.Cos(direction - slot.direction) * slot.radius;
		
		return new Vector2(slotX, slotY);
	}
	
	public void RemoveUnitFromSquad (EnemyUnit enemyUnit)
	{
		slots.Remove(enemyUnit);
		units.Remove(enemyUnit);
		
		if (units.Count == 0) Terminate();
	}
	
	public void Terminate ()
	{
		// RemoveAllTimers();
		units.ForEach( p => p.ReleaseFromSquad());
		units.Clear();
		slots.Clear();
		Deactivate();
		// Destroy(gameObject);
	}
}

