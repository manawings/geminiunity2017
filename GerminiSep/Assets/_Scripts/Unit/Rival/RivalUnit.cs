using UnityEngine;
using System.Collections;

public class RivalUnit : Unit {
	
	public void Deploy (int seed)
	{

		team = Unit.Team.Enemy;
		isElite = true;

		float statFactor = Stats.NGVForLevel(ProfileController.SectorLevel);
		
		stats.life = 1.0f + 100.00f * Stats.EnemyDifficultyFactor(0.75f, ProfileController.SectorLevel) * statFactor;
		stats.damage = 1.0f + 2.5f * Stats.EnemyDifficultyFactor(0.75f, ProfileController.SectorLevel) * statFactor;
		stats.Calibrate();
		stats.armor =  1.0f + 1.65f * statFactor;

		targetTravelDirection = travelDirection = Mathf.PI;
		faceDirection = travelDirection;

		weapon.CopyDataFrom( WeaponPattern.StandardYellow );
		weapon.Deploy(this);
		weapon.StartShooting();

		SetTargetedMoveValues();
		maxSpeed = 500.0f;

		base.Deploy();

		Run ();
		AddTimer (Run);
		AddTimer (RunAIMovement, 0.35f);
		OnActivate();
	}

	void RunAIMovement()
	{
		targetTravelDirection = KJMath.DirectionFromPosition(transform.position, GameController.PlayerUnit.transform.position); //Random.value * KJMath.CIRCLE;
		// SetTargetPoint(new Vector3(Random.Range(-GameController.Singleton.gameStageWidth * 0.35f, GameController.Singleton.gameStageWidth * 0.35f), Random.Range(220, -30), 0.0f));
	}

	public override void TakeHit (float damage = 25.0f, Entity.DamageType damageType = Entity.DamageType.Normal, bool isCrit = false, bool ignoreArmor = false)
	{
		// ExternalHit(0.0f);
		if (buffAcid.IsActive) damage += buffAcid.power;
		TakeDamage(damage, damageType, isCrit, ignoreArmor);
		UIController.UpdateBossBar(LifePercent, stats.lifePoints.ToString("F0"));
	}


	protected override void Run ()
	{
		//if ( targetTravelDirection != travelDirection ) Debug.Log ("tar: " + targetTravelDirection + " dir: " + travelDirection);
		

			// if (!IsStunned) {
				// if (isLockingOn)
				//{
					

					//travelDirection = ConvergeAngleTo(travelDirection, targetTravelDirection, 0.12f, 0.01f);
					//speed = KJMath.ConvergeToTarget(speed, 15.                                                                                                                         0f, 0.01f, 1.0f);

					
				//} else {
					
					if (travelDirection != targetTravelDirection) travelDirection = ConvergeAngleTo(travelDirection, targetTravelDirection, 0.05f, 0.01f);
					float angleDifferenceThreshold = 1.5f;
					float angleDifferenceFactor =  1.0f - KJMath.Cap(angleDifferenceValue / angleDifferenceThreshold, 0.0f, 0.90f);					
					speed = KJMath.Cap(maxSpeed * angleDifferenceFactor, 150.0f, maxSpeed);
					

			//}

		// }
		
		faceDirection = travelDirection;

		base.Run();
	}

	protected override void OnActivate ()	
	{
		if (!EnemyUnit.allActive.Contains(this)) EnemyUnit.allActive.Add(this);
		base.OnActivate();
	}
	
	protected override void OnDeactivate ()
	{
		EnemyUnit.allActive.Remove(this);
		base.OnDeactivate();

		KJTime.Add (GameController.WinGame, 2.0f, 1);
	}

}
