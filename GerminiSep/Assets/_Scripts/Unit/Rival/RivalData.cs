// ------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RivalData {

	public int sector = 0;
	public int shipId = 0;

	public string displayName = "";
	public string facebook_id = "-1";

	public Stats stats;
	public List<ModuleData> moduleDataList;
	public List<ItemBasic> equipList = new List<ItemBasic>();

	public RivalData ()
	{
		facebook_id = "-1";
		stats = new Stats();
		moduleDataList = new List<ModuleData>();
		equipList = new List<ItemBasic>();
	}
	
}
