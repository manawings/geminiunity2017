using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : Entity {
		
	// MOVEMENT
	
	protected float acceleration = 0, speed = 0, maxSpeed = 0, travelDirection = 0, velocityX = 0, velocityY = 0, spin = 0; // Core Physics
	protected float externalVelocityX = 0, externalVelocityY = 0, externalSpin = 0; // External Physics
	protected Vector3 targetPoint;
	
	// DAMAGE AND CONTROL
	
	protected bool hasLostControl = false;
	protected bool isCritical = false;
	public bool isElite = false;
	public bool isRival = false; // If the Elite is to be smaller and have missile disengaging.
	public bool isSocial = false;
	public bool isQuestEliteBoss = false;

	public int beamDir = 1;
	
	public override bool IsAlive { 
		get { 
			if (stats.lifePoints > 0 || isCritical)
			{
				return true;	
			} else {
				return false;
			}
		} 
	}
	public bool isBlocked = false;
	public float LifePercent { get { return stats.LifePercent; } }
	public float EnergyPercent { get { return stats.EnergyPercent; } }
	
	// BUFF STATUS SYSTEM
	
	protected Buff buffStun = new Buff();
	protected Buff buffBurn = new Buff();
	protected Buff buffAcid = new Buff();
	protected Buff buffShield = new Buff();
	protected Buff buffVoid = new Buff();

	protected Buff eliteShield = new Buff();
	
	public Color currentColor = Color.black; // Use this as the base color of the ship.
	float periodicTime = 0.0f;
	const float periodicTimeMax = 0.25f;

	//Color Shield
	public enum ShieldColor {
		Green,
		Blue,
		Red,
		White,
		Yellow,
		Orange,
		Pink
	}
	public Color startShieldColor, endShieldColor;

	// BLASTING
	
	protected int blastRange = 170;
	protected const int blastRangeSq = 170 * 170;

	protected KJEmitter boosterEmitter;
	protected KJEmitter boosterEmitter2;
	public GameObject boosterObject;
	
	// MODULES
	
	public ModuleManager modules = new ModuleManager();
	
	// MOVEMENT FUNCTIONS
	
	protected float maxStunSpeed = 5.0f;
	protected float stunDecay = 0.97f;

	protected virtual void RunStunAdjuster ()
	{
		if (speed > maxStunSpeed) speed *= stunDecay;
	}

	public bool IsStunned
	{
		get {
			return buffStun.IsActive;
		}
	}
	
	protected virtual void Run () {
		
		if (buffStun.IsActive || buffVoid.IsActive)
		{

			RunStunAdjuster();
			
		} else {
			
			if (!isLockingOn) travelDirection += externalSpin;
		}

		velocityX = Mathf.Sin (travelDirection) * speed;
		velocityY = Mathf.Cos (travelDirection) * speed;
		
		
		x += ( velocityX + externalVelocityX ) * DeltaTime;
		y += ( velocityY + externalVelocityY ) * DeltaTime;

		weapon.direction = faceDirection;

		RotationByRadians = faceDirection;
		transform.position = new Vector3(x, y, 0);

		
		// Buffs
		
		periodicTime -= DeltaTime;
		if (periodicTime < 0.0f) {
		
			periodicTime = periodicTimeMax;
			RunPeriodic();
			
		}
		
		RunBuffs();
		
		// Module Cycling
		modules.Run(DeltaTime);
		
		/** Dealing with lost control. */
		
		if (hasLostControl) {
			if (!isCritical) {
				if (externalSpin != 0 || externalVelocityX != 0 || externalVelocityY != 0)
				{
					StabilizeControl();
				} else {
					hasLostControl = false;	
				}
			}
		}
		
		if (isCritical) RunCriticalState();
		else if (fadeToInvis) sprite.color = Color.Lerp(sprite.color, new Color(0.1f, 0.3f, 1.0f, 0.0f), 0.05f);
		
	}
	
	protected virtual void RunPeriodic ()
	{
		
		if (buffBurn.IsActive) {
			float damage = buffBurn.power * 0.85f;
			TakeDamage(damage, DamageType.Burn, false, false, true);
		}
		
	}
	
	public virtual void GainReactor (Collectable.Type collectableType)
	{
		
	}
	
	public void SetTargetDirection (float targetDirection, bool instant = false)
	{
		this.targetTravelDirection = targetDirection;
		this.faceDirection = targetDirection;
		
		if (instant)
		{
			travelDirection = targetDirection;	
		}
	}
	
	void RunCriticalState ()
	{
		/** Unit is in Critical State. */
		for (int i = EnemyUnit.allActive.Count - 1 ; i > -1 ; i --)
		{
			Unit enemyUnit = EnemyUnit.allActive[i];
			if (enemyUnit == this || enemyUnit.isElite) continue;
			
			float distanceBetweenUnits = KJMath.DistanceSquared2d(transform.position, enemyUnit.transform.position);
			if (distanceBetweenUnits < 600.0f)
			{
				/** Instantly kill the unit it collides with. */
				enemyUnit.Explode();
				Explode();
				return;
			}
		}
	}

	public void AddModule (int moduleId, float moduleLevel, float modulePower)
	{

		Module newModule = ModuleLibrary.GetFromId(moduleId);
		newModule.Deploy(this, moduleLevel, modulePower);
		newModule.BoltWeaponToModule();
		modules.AddModule(newModule);

	}

	public void StartShootingTicker ()	
	{
		modules.OnStartShoot();
	}
	
	public void StopShootingTicker ()	
	{
		modules.OnStopShoot();
	}

	public void StartShooting ()	
	{
		weapon.StartShooting();
		modules.OnStartShoot();
	}
	
	public void StopShooting ()	
	{
		weapon.StopShooting();
		modules.OnStopShoot();
	}
	
	public override void Deploy ()
	{
		isControlLocked = false;
		fadeToInvis = false;

		modules.Clear();
		FlatScale = 1.0f;
		currentColor = Color.black;
		sprite.color = currentColor;

		isCritical = false;
		isBeamLock = false;
		hasBeenExploded = false;
		isEliteCanExplode = false;

		velocityX = velocityY = externalVelocityX = externalVelocityY = externalSpin = 0.0f;
		
		// Initialize Buffs
		
		buffStun.Deploy(Buff.Type.Stun, this);
		buffBurn.Deploy(Buff.Type.Burn, this);
		buffAcid.Deploy(Buff.Type.Acid, this);
		buffShield.Deploy(Buff.Type.Shield, this);
		buffVoid.Deploy(Buff.Type.Burn, this);

		eliteShield.Deploy(Buff.Type.EliteShield, this);
		debrisSpriteName = string.Empty;

		base.Deploy();
	}
	
	public override void Heal (float life)
	{

		if (stats.lifePoints != 0)
		{
			if (life < 1.0f) life = 1.0f;
			int displayValue = (int) life;


			// Show the DAMAGE as a number.
			if (displayValue > 0) {
				CombatPopup combatPopup = KJActivePool.GetNewOf<CombatPopup>("Popup");
				combatPopup.Deploy(transform.position + Random.insideUnitSphere * 25.0f, displayValue.ToString(), CombatPopup.Type.Green);
				stats.lifePoints += displayValue;
			}	
		}
	}

	protected string debrisSpriteName;
	
	public override void TakeDamage (float damage, Entity.DamageType damageType = Entity.DamageType.Normal, bool isCriticalDamage = false, bool instantDeath = false, bool ignoreArmor = false)
	{

		if (!IsAlive) return;

		if (!ignoreArmor) {
			damage *= stats.ArmorModifier;
			damage -= stats.ArmorReducer;
		}

		float preModuleDamage = damage;
		damage = modules.OnTakeDamage(damage, damageType);

		// Whether it counts as a crit or a block

		float damageBlocked = preModuleDamage - damage;
//		if (damageBlocked > 0) {
//			if (damageBlocked > 0.35f * preModuleDamage) isBlocked = true;
//		}

		// Random Variety Range
		int randomRange = (int) (damage * 0.12f) + 2;

		damage += Random.Range(-randomRange, randomRange);

		if (damage < 1.0f) damage = 1.0f;
		
		if (buffShield.IsActive || eliteShield.IsActive) {
			damage = 0.0f;
			if(isElite) EffectController.BigHexShieldAt(transform.position, startShieldColor, endShieldColor);
			else EffectController.HexShieldAt(transform.position, startShieldColor, endShieldColor);
		}
			
		int displayDamage = (int) damage;
			
			
		// Show the DAMAGE as a number.
		if (displayDamage > 0) {

			CombatPopup combatPopup = KJActivePool.GetNewOf<CombatPopup>("Popup");

			string damageString = displayDamage.ToString();

			CombatPopup.Type popupType = CombatPopup.Type.Yellow;
			if (this == GameController.PlayerUnit || this == GameController.AllyUnit) popupType = CombatPopup.Type.Red;
			if (isBlocked) { 
				popupType = CombatPopup.Type.White;
				//Debug.Log("startShieldColor = " + startShieldColor);
				if(isElite) EffectController.BigHexShieldAt(transform.position, startShieldColor, endShieldColor);
				else EffectController.HexShieldAt(transform.position, startShieldColor, endShieldColor);
				isBlocked = false;
			}
			if (isCriticalDamage) damageString += "!";
			
			combatPopup.Deploy(transform.position + Random.insideUnitSphere * 35.0f, damageString, popupType, isCriticalDamage);
		}
		
		if (stats.lifePoints != 0)
		{
			
			if (stats.lifePoints - damage < 1)
			{
				/** Damage would be lethal. */
				stats.lifePoints = 0;

				if (instantDeath) { 
					Explode();
				} else { 
					EnterCriticalState();
				}

			} else {
				
				 /** Take the damage normally. */
				
				stats.lifePoints -= damage;
			}
			
		} else {
			
			if (isCritical) Explode();	
			
		}

		// Aggregate Elite HP and update
		if (isElite) {

			float finalPercent = 0.0f;
			int numOfElites = 0;
			float finalLife = 0;
			
			foreach (Unit unit in EnemyUnit.allActive)
			{
				if (unit.isElite) {
					finalPercent += unit.stats.LifePercent;
					finalLife += unit.stats.lifePoints;
					numOfElites ++;
				}
			}
			
			UIController.UpdateBossBar(finalPercent / numOfElites, finalLife.ToString("F0"));
		}
	}

//	protected bool hasRun = false;
//
//	void Update ()
//	{
//		hasRun = false;
//	}

	protected void ExplodeBurst () 
	{
		float explodeX = x + Random.Range(-30, 30);
		float explodeY = y + Random.Range(-20, 20);
		
		Vector3 explosionPosition = new Vector3(explodeX, explodeY, 0.0f);
		EffectController.StandardExplosionAt(explosionPosition);
		GameController.Singleton.BigShakeScreen();
		
	}

	protected bool isEliteCanExplode, hasBeenExploded;

	protected void EliteActualExplode ()
	{
		isEliteCanExplode = true;
		hasBeenExploded = false;
		Explode();
		isEliteCanExplode = false;
	}
	
	protected virtual void Explode ()
	{



		if (boosterEmitter != null) {
			boosterEmitter.KillTimers();
			boosterEmitter = null;
		}

		if (boosterEmitter2 != null) {
			boosterEmitter2.KillTimers();
			boosterEmitter2 = null;
		}

		RemoveAllTimers();
		TerminateEffects();

		if (!isActive) return;

		if (team == Team.Enemy)
		{
			GameController.PlayerUnit.modules.OnEnemyKilled();
		}
		
		EffectController.StandardExplosionAt(new Vector3(x, y, -5.0f), debrisSpriteName);
		
		// This unit is clinically dead.
		stats.lifePoints = 0;
		isCritical = false;
		
		if (beam != null) beam.TerminateBeam();



		if (canExplode) {

			float pDamage = stats.lifePointsMax * 0.15f;
			canExplode = false;

			GameController.Singleton.ShakeScreen();
			
			for ( int i = EnemyUnit.allActive.Count - 1 ; i > -1 ; i-- ) {

				Unit blastUnit = EnemyUnit.allActive[i];
					
				if (!blastUnit.IsAlive) continue;
				if (blastUnit == this) continue;
					
				float distanceToExplosion = KJMath.DistanceSquared2d(transform.position, blastUnit.transform.position);
				distanceToExplosion = distanceToExplosion < 0 ? -distanceToExplosion : distanceToExplosion;
				
				if (distanceToExplosion < blastRangeSq) {// Radius of 200.
						
					//  * (1.0f - distanceToExplosion / blastRangeSq));
//					if (pDamage < 0) pDamage = 0;
					blastUnit.TakeDamage(pDamage, DamageType.Normal, false, false, true);
					float blastDir = KJMath.DirectionFromPosition(transform.position, blastUnit.transform.position);

					blastUnit.ExternalBlast(blastDir , (1.0f - distanceToExplosion / blastRangeSq));
				}
			}
		}

		modules.Clear();
		Deactivate();
		CalibrateBuffGraphics();
		
	}

	static public bool canExplode = true;

	public static void ResetExplodeCount ()
	{
		canExplode = true;
	}

	public void ExternalBlast (float dir = 0.0f, float power = 1.0f)
	{

		dir = KJMath.TreatAngle(dir);
		hasLostControl = true;

		if (power > 0.5f) power = 0.5f;

		
		// Finding the angle of the spin.
		// Find which quadrant...
		// if (isCritical) power *= 0.15f;
		float spinPower = power;
		if (dir < Mathf.PI + KJMath.RIGHT_ANGLE) spinPower = -power;
		if (dir < Mathf.PI) spinPower = power;
		if (dir < KJMath.RIGHT_ANGLE) spinPower = -power;

		if (isCritical || isLockingOn) power *= 0.15f;
		externalVelocityX += Mathf.Sin(dir) * power * 300.0f;
		externalVelocityY += (Mathf.Cos(dir) * power * 300.0f);
		
		float castDir = dir;
		while (castDir > KJMath.ANGLE_45) {
			castDir -= KJMath.ANGLE_45;
		}
		
		float powerFactor = (KJMath.ANGLE_45 - Mathf.Abs(castDir - KJMath.ANGLE_45)) / KJMath.ANGLE_45;
		
		// Spin the ship out of control in that direciton.
		externalSpin = 0.15f * powerFactor * spinPower;
		// super.directionalBlast(dir);
		
		
	}

	bool fadeToInvis = false;

	public void SetInvisible ()
	{
//		sprite.color = new Color(0.0f, 0.0f, 0.0f, 0.05f);

		fadeToInvis = true;
		if (boosterEmitter != null) {
			boosterEmitter.KillTimers();
			boosterEmitter = null;
		}
		if (boosterEmitter2 != null) {
			boosterEmitter2.KillTimers();
			boosterEmitter2 = null;
		}
	}

	void FlashToHitColor ()
	{
		sprite.color = new Color(1.2f, 0.6f, 0.2f);
	}
	
	void FlashToNormalColor ()
	{
		sprite.color = new Color(0.0f, 0.0f, 0.0f);
	}

	AudioSource audioSource;
	
	protected virtual void EnterCriticalState ()
	{
		if (!isActive) return;

		audioSource = KJCanary.PlaySoundEffect("Critical");

		if (emitter != null) emitter.Terminate();
		emitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		emitter.CopyFromModel(EffectController.Singleton.buffBurn);
		emitter.SetObjectLink(gameObject);
		emitter.Deploy();

		isCritical = true;
		AddTimer (StartCriticalFlare, 1.0f, 1);
		if (beam != null) beam.TerminateBeam();
		AddTimer(ChangeToCriticalColor);
		
	}
	
	void ChangeToCriticalColor ()	
	{
		sprite.color = Color.Lerp(sprite.color, new Color(1.2f, 0.45f, 0.15f), 0.08f);
	}
	
	void StartCriticalFlare ()
	{
		if (!isActive || !IsAlive) return;

		if (emitter != null) emitter.Terminate();
		emitter = KJActivePool.GetNewOf<KJEmitter>("KJEmitter");
		emitter.CopyFromModel(EffectController.Singleton.blueFlare);
		emitter.SetObjectLink(gameObject);
		emitter.Deploy();

		//print ("Emit Blue Flare: " + emitter.gameObject.name+" for "+gameObject.name);
		
		AddTimer (Explode, 0.35f, 1);
	}
	
	void StabilizeControl () {
		
		externalVelocityX = KJMath.ConvergeToZero(externalVelocityX, 0.92f, 0.2f);
		externalVelocityY = KJMath.ConvergeToZero(externalVelocityY, 0.92f, 0.2f);
		externalSpin = KJMath.ConvergeToZero(externalSpin, 0.90f, 0.01f);
		
	}

	// VELOCITY AND ACCELERATION
	protected float maxVelocity = 1500.0f; //900.0f; // 840
	protected float maxAcceleration = 30000.0f; // 160
	protected bool isMoving = false;
	protected float accelerationFactor = 1;
	protected bool setToMove = false;
	protected float velocityDecay = 0.85f; // 0.82 Per Frame. Can't we do a per second module?
	protected float distanceToTarget;

	// TARGET POINT MOVEMENT
	protected float targetRange = 2.0f;
	protected float accelerationRatio = 4;// Dynamic acceleration based on how far object is from the destination.
	protected float maxAccelerationFactor = 0; //maxVelocity * accelerationRatio;
	protected float decayCoefficient = 0;
	protected int calculusLoops = 30;

	protected void SetTargetedMoveValues (float velocityDecay = 0.85f)
	{
		this.velocityDecay = velocityDecay;
		maxAccelerationFactor = maxVelocity * accelerationRatio * 0.05f;
		decayCoefficient = 1.0f;
		maxStunSpeed = 10.0f;
		stunDecay = 0.5f;
		
		for (int i = 0 ; i < calculusLoops ; i++)
		{
			if (i != 0) decayCoefficient += Mathf.Pow(velocityDecay, i);
		}
	}

	float DirectionFromPosition(Vector2 origin, Vector2 target)
	{
		// Finds an angle from the top co-ordinate to this vector using atan2.
		return KJMath.TreatAngle(Mathf.Atan2((origin.y - target.y), (target.x - origin.x)) + Mathf.PI * 0.5f);
	}
	
	protected void RunTargetedMovement (float faceDirection = float.MaxValue)
	{
		accelerationFactor = 1.0f;
		
		if (setToMove) {
			
			
			Vector2 fromVec = new Vector2(transform.position.x, transform.position.y);
			Vector2 toVec = new Vector2(targetPoint.x, targetPoint.y);

			if (faceDirection == float.MaxValue)
			{
				this.faceDirection =  DirectionFromPosition(fromVec, toVec);
			} else {
				this.faceDirection = faceDirection;
			}

			
			distanceToTarget = Vector3.Distance(targetPoint, transform.position);
			travelDirection = DirectionFromPosition(fromVec, toVec);

			if (distanceToTarget - Mathf.Abs(speed * DeltaTime * decayCoefficient) < targetRange) {
				
				if (setToMove) 	setToMove = false;	
				
			} else {
				
				// Acceleration Factor.
				
				accelerationFactor = distanceToTarget / maxAccelerationFactor;
				if (accelerationFactor > 1.0f) accelerationFactor = 1.0f;
				acceleration = maxAcceleration * accelerationFactor * DeltaTime;
				
				speed += acceleration;
				if (speed > maxVelocity) speed = maxVelocity;
				
				
			}
		}
		
		DecayVelocity(DeltaTime);
		
		if (speed < 50.0f) {
			if (isMoving) { modules.OnStopMove(); isMoving = false; }
		} else {
			if (!isMoving) { modules.OnStartMove();	isMoving = true; }
		}
		
	}

	protected bool isControlLocked = false;

	public void SetTargetPoint (Vector3 newTargetPoint)
	{
		if (!isControlLocked) {
			distanceToTarget = Vector3.Distance(newTargetPoint, transform.position);

			if (distanceToTarget > targetRange)
			{
				if (!setToMove) setToMove = true;
				targetPoint = newTargetPoint;
			}
		}
	}

	protected float angleDifferenceValue = 0.0f;
	protected float ConvergeAngleTo( float originalValue, float targetValue, float convergeFactor = 0.3f, float cutoffLimit = 0.01f )
	{
		// Converges the Value to a certain point, via a factor and a limit.
		
		originalValue = KJMath.TreatAngle(originalValue);
		targetValue = KJMath.TreatAngle(targetValue);
		
		if (originalValue == targetValue) return targetValue;
		if (Mathf.Abs(targetValue - originalValue) < cutoffLimit) return targetValue;
		
		float valueDifference = targetValue - originalValue;
		float absDiff = Mathf.Abs(valueDifference);
		float altDiff = KJMath.CIRCLE - absDiff;
		float useDiff;
		
		if (altDiff < absDiff) {
			useDiff = altDiff * (valueDifference / absDiff) * - 1.0f;
		} else {
			useDiff = valueDifference;
		}
		
		angleDifferenceValue = Mathf.Abs(useDiff);
		return originalValue + (useDiff * convergeFactor);
	}

	
	void DecayVelocity (float t)
	{
		
		speed *= velocityDecay;
		
		if (Mathf.Abs(speed) < 5.0f) speed = 0.0f;
		
	}
	
	public void RunBuffs ()
	{
		buffStun.Run(DeltaTime);
		buffBurn.Run(DeltaTime);
		buffAcid.Run(DeltaTime);
		buffShield.Run(DeltaTime);
		buffVoid.Run (DeltaTime);

		eliteShield.Run(DeltaTime);
	}
	
	public void ApplyStun (float power, float time)
	{
		if (!IsStunned) KJCanary.PlaySoundEffect("Stun", transform.position);
		buffStun.Apply(power, time);
		
	}
	
	public void ApplyBurn (float power, float time)
	{
		buffBurn.Apply(power, time);
	}
	
	public void ApplyAcid (float power, float time)
	{
		buffAcid.Apply(power, time);
	}
	
	public void ApplyShield (float power, float time, ShieldColor? color = null)
	{
		if (color != null) {
			SetShieldColor((ShieldColor)color);
		}else {
			SetShieldColor(ShieldColor.Blue);
		}
		if(isElite) {
			if (isRival) {
				buffShield.Apply(power, time);
			} else {
				eliteShield.Apply(power, time);
			}
		} else {
			buffShield.Apply(power, time);
		}
	}

	public void SetShieldColor (ShieldColor color) {
		switch (color) {
			
		case ShieldColor.Green:
			startShieldColor = new Color(0.3f, 0.8f, 0.3f, 1.0f);
			endShieldColor = new Color(0.0f, 0.3f, 0.3f, 1.0f);
			break;
		case ShieldColor.Orange:
			startShieldColor = new Color(1.0f, 0.5f, 0.10f, 1.0f);
			endShieldColor = new Color(1.0f, 0.25f, 0.10f, 1.0f);
			break;
		case ShieldColor.Pink:
			startShieldColor = new Color(1.0f, 0.0f, 0.5f, 1.0f);
			endShieldColor = new Color(1.0f, 0.35f, 0.35f, 1.0f);
			break;
		case ShieldColor.Red:
			startShieldColor = new Color(1.0f, 0.0f, 0.00f, 1.0f);
			endShieldColor = new Color(0.75f, 0.25f, 0.25f, 1.0f);
			break;
		case ShieldColor.White:
			startShieldColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			endShieldColor = new Color(1.0f, 1.0f, 1.0f, 0.75f);
			break;
		case ShieldColor.Yellow:
			startShieldColor = new Color(1.0f, 1.00f, 0.0f, 1.0f);
			endShieldColor = new Color(1.0f, 1.00f, 0.5f, 1.0f);
			break;
		case ShieldColor.Blue:
			startShieldColor = new Color (0.3f, 0.8f, 1.0f, 1.0f);
			endShieldColor = new Color (0.0f, 0.3f, 1.0f, 1.0f);
			break;
		default :
			startShieldColor = new Color (0.3f, 0.8f, 1.0f, 1.0f);
			endShieldColor = new Color (0.0f, 0.3f, 1.0f, 1.0f);
			break;
		}
	}
	
	public void ApplyVoid (float time)
	{
		buffVoid.Apply(0.0f, time);	
	}
	
	public void DispelAll ()
	{
		DispelAllPositiveBuffs();
		DispelAllNegativeBuffs();
	}
	
	public void DispelAllPositiveBuffs ()
	{
		buffShield.Dispel();
	}
	
	public void DispelAllNegativeBuffs ()
	{
		buffStun.Dispel();
		buffBurn.Dispel();
		buffAcid.Dispel();
		buffVoid.Dispel();
	}
	
	public virtual void StopWeapons ()
	{
		foreach (Weapon weapon in gameObject.GetComponents<Weapon>())
		{
			weapon.StopShooting();
		}

		if (beam != null) beam.TerminateBeam();
	}

	public virtual void ResumeWeapons ()
	{
		foreach (Weapon weapon in gameObject.GetComponents<Weapon>())
		{
			weapon.StartShooting();
		}
	}
	
	protected void TerminateBuffs ()
	{
		buffStun.Terminate();
		buffBurn.Terminate();
		buffAcid.Terminate();
		buffShield.Terminate();
		buffVoid.Terminate();
		eliteShield.Terminate();
	}
	
	public void BlackHolePull (Vector3 position)
	{
		float directionToHole = KJMath.DirectionFromPosition(transform.position, position);
		x += Mathf.Sin(directionToHole) * 150.0f * DeltaTime;
		y += Mathf.Cos(directionToHole) * 150.0f * DeltaTime;
	}
	
	public void CalibrateBuffGraphics ()
	{
		// Asses the current state of buffs, and find the most suitable graphic overlay.

		if (!isActive || !IsAlive) return;
		
		Buff.Type buffTypeColor = Buff.Type.None;
		Buff.Type buffTypeEmitter = Buff.Type.None;
		
		if (!isCritical) {
			if (buffVoid.IsActive) {
				buffTypeColor = Buff.Type.Void;
			} else if (buffBurn.IsActive) {
				buffTypeColor = Buff.Type.Burn;
			} else if (buffAcid.IsActive) {
				buffTypeColor = Buff.Type.Acid;
			} else if (buffStun.IsActive) {
				buffTypeColor = Buff.Type.Stun;
			}
		}

//		if (eliteShield.IsActive || buffShield.IsActive) {
			if (buffVoid.IsActive) {
				buffTypeEmitter = Buff.Type.Void;
			} else if (buffStun.IsActive) {
				buffTypeEmitter = Buff.Type.Stun;
			} else if (buffBurn.IsActive) {
				buffTypeEmitter = Buff.Type.Burn;
			} else if (buffAcid.IsActive) {
				buffTypeEmitter = Buff.Type.Acid;
			} 
//		}

		if (buffShield.IsActive) {
			buffTypeEmitter = Buff.Type.Shield;
		}

		if (eliteShield.IsActive) {
			buffTypeEmitter = Buff.Type.EliteShield;
		}
		
		currentColor = Buff.ColorForType(buffTypeColor);
		sprite.color = currentColor;
		
		KJEmitter emitterToUse = Buff.EmitterForType(buffTypeEmitter);
		
		if (emitterToUse == null)
		{
			if (buffEmitter != null)
			{
				buffEmitter.Terminate();
				buffEmitter = null;
			}

		} else {

			if (buffEmitter != null)
			{
				buffEmitter.Terminate();
				buffEmitter = null;
			}

			buffEmitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
			buffEmitter.CopyFromModel(emitterToUse);
			buffEmitter.SetObjectLink(gameObject);
			if(buffShield.IsActive) buffEmitter.SetColor(startShieldColor, endShieldColor);
			if(eliteShield.IsActive) buffEmitter.SetColor(startShieldColor, endShieldColor);
			buffEmitter.Deploy();
		}

	}
	
	protected override void OnDeactivate ()
	{	
		isActive = false;
		TerminateBuffs();

		if (audioSource != null) {
			audioSource.Stop();
		}

		base.OnDeactivate();
//		Debug.Log ("Deactive: " + gameObject.name);
	}
}
