using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Stats
{
	
	public enum StatType {
		Damage,
		Life,
		Armor,
		Energy,
		Speed
	}
	
	public float life = 0;
	public float energy = 0;
	public float damage = 0;
	public float armor = 0;
	
	private float _lifePoints = 0;
	public float lifePointsMax = 0;
	
	public float lifePoints {
		
		get {
			return _lifePoints;
		}
		
		set {
			_lifePoints = value;
			
			if (_lifePoints < 0 ) _lifePoints = 0;
			if (_lifePoints > lifePointsMax) _lifePoints = lifePointsMax;
		}
		
	}
	
	public float energyPoints = 0;
	public float energyPointsMax = 0;
	
	public const float DamageReductionPerArmor = 0.0002f;
	
	float _ArmorModifier = 0;
	public float ArmorModifier
	{
		get { 
			if (_ArmorModifier == 0)
			{
				_ArmorModifier = Mathf.Pow((1 - DamageReductionPerArmor), armor);
			}
			return  _ArmorModifier;
		}
	}
	
	float _ArmorReducer = 0;
	public float ArmorReducer
	{
		get { 
			if (_ArmorReducer == 0)
			{
				_ArmorReducer = 0.2f * armor;//#####	
			}
			return  _ArmorReducer;
		}
	}
	
	public Stats (float life = 0.0f, float damage = 0.0f, float armor = 0.0f, float energy = 0.0f)
	{
		this.life = life;
		this.damage = damage;
		this.armor = armor;
		this.energy = energy;
	}
	
	public void Add (Stats stats)
	{
		life += stats.life;
		damage += stats.damage;
		energy += stats.energy;
		armor += stats.armor;
		
		Calibrate();
	}
	
	public float MaxLifePoints
	{
		get { return life * 2.0f; }
	}
	
	public float MaxEnergyPoints
	{
		get { return life * 2.0f; }
	}
	
	public float LifePercent 
	{
		get { return lifePoints / lifePointsMax; }
	}
	
	public float EnergyPercent 
	{
		get { return energyPoints / energyPointsMax; }
	}
	
	public void Calibrate ()
	{
		lifePointsMax = MaxLifePoints;
		lifePoints = lifePointsMax;
		energyPointsMax = energyPoints = MaxEnergyPoints;
		_ArmorModifier = 0;
		_ArmorReducer = 0;
	}
	
	public static float NGVForLevel (float level)
	{
		// Normalized General Value for a given level. 
		// To be split and distributed as per items.
		
		return 3.0f + 1.5f * Mathf.Pow(level, 1.40f);
	}
	
	public static float NGVForItemLevel (float level)
	{
		return NGVForLevel(level) * 1.00f; // Items need to be split by .25;
	}
	
	public static float EnemyDifficultyFactor (float factor, float level)
	{
		float factorInverse = 1.0f - factor;
		return factorInverse + factor * (level / 100.0f);
	}

	public static int BoxCostForLevel (float level)
	{
		float floatValue = ((level * 2.0f)) - (level * 2.0f) % 5;
		return (int) floatValue + 100;
	}

	public static float FactorForLevel (float level)
	{
		
		return 0.5f * Mathf.Pow(level, 0.55f);
	}

	public static List<Stats.StatType> AllTypes {
		get {
			
			List<Stats.StatType> types = new List<Stats.StatType>();
				
			// TODO Better way to get a list of the enums?
			
			types.Add(StatType.Armor);
			types.Add(StatType.Damage);
			// types.Add(StatType.Energy);
			types.Add(StatType.Life);
			// types.Add(StatType.Speed);
			
			/*
			// Debug.Log ("Enumerating! " + Enum.GetValues(StatType));
			StatType[] statTypes = (StatType[]) Enum.GetValues(typeof(StatType));
			Debug.Log ("Enumerating! " + statTypes[2]);
			foreach (StatType type in Enum.GetValues(typeof(StatType))) {
				Debug.Log ("type: " + type);
			}
			*/
			
			return types;
		}
	}
}

