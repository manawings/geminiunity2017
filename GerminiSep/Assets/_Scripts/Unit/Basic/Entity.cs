using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity : KJSpriteBehavior
{
	
	protected float targetTravelDirection = 0;
	protected float faceDirection = 0;
	public float WeaponDirection { get { return faceDirection; }}

	protected bool isActive = false;
	
	public enum Team {
		Enemy,
		Friendly,
		All
	}
	
	public enum Image {
		PlayerShip,
		EnemySimple,
		EnemyStandard,
		EnemyStriker,
		EnemyBeamer,
		EnemyRound,
		EnemySwift,
		EnemyShooter,
		EnemyDeadly,
		EnemyElite,
		EnemyLaser,
		EnemyOrbit,

		EnemyKite,
		EnemyStingray,
		EnemyDiamond,
		EnemyOx,
		EnemyFrigate,

		GN_Beamer,
		GN_Kite,
		GN_Stingray,
		GN_Diamond,
		GN_Ox,
		GN_Frigate,

		RE_Beamer,
		RE_Kite,
		RE_Stingray,
		RE_Diamond,
		RE_Ox,
		RE_Frigate
	}

	// Damage Types
	
	public enum DamageType
	{
		Normal,
		Missile,
		Beam,
		Mine,
		Burn
	}
	
	public Team team;
	
	// ENTITY STATS
	
	private Stats _stats;
	public Stats stats { get { if (_stats == null) _stats = new Stats(); return  _stats; } protected set { _stats = value; } }
	
	// WEAPONS
	
	private Weapon _weapon;
	public Weapon weapon { get { 
			if (_weapon == null) 
				_weapon = gameObject.GetComponent<Weapon>(); 
			if (_weapon == null) 
				_weapon = gameObject.AddComponent<Weapon>(); 
			return  _weapon; 
		} 
	}
	
	// BEAM
	
	public bool isBeamLock = false;
	
	// EFFECTS
	
	protected KJEmitter emitter;
	protected KJEmitter buffEmitter = null;
	
	protected bool isLockingOn {
		get {
			if (isBeamLock) return true;
			if (beam == null)
			{
				return false;
			} else {
				return true;	
			}
		}
	}
	
	[System.NonSerialized] public Beam beam;
	[System.NonSerialized] public float x = 0.0f, y = 0.0f;

//	public float hitRadiusSq = 20 * 20;
	
	public virtual bool IsAlive { get { return stats.lifePoints == 0 ? false : true; } }
	
	public virtual void TakeHit (float damage = 25.0f, Entity.DamageType damageType = Entity.DamageType.Normal, bool isCrit = false, bool ignoreArmor = false)
	{
		// Override Damage Function Here.
	}
	
	public virtual void TakeDamage (float damage, Entity.DamageType damageType = Entity.DamageType.Normal, bool isCriticalDamage = false, bool instantDeath = false, bool ignoreArmor = false)
	{
		// Override Damage Function Here.
	}
	
	public virtual void Heal (float life)
	{
		// Override Damage Function Here.
	}
	
	protected static string GetNameOfImage (Image image)
	{
		switch (image)
		{
			
		case Image.PlayerShip:
			return "Enemy6";
			break;


			
		case Image.EnemySimple:
			return "Enemy1";
			break;
			
		case Image.EnemyStandard:
			return "Enemy2";
			break;
			
		case Image.EnemyStriker:
			return "Enemy3";
			break;
			
		case Image.EnemyRound:
			return "Enemy4";
			break;

		case Image.EnemyBeamer:
			return "Enemy5";
			break;

		case Image.EnemySwift:
			return "Enemy6";
			break;

		case Image.EnemyShooter:
			return "Enemy7";
			break;

		case Image.EnemyDeadly:
			return "Enemy7";
			break;

		case Image.EnemyElite:
			return "Enemy6";
			break;

		case Image.EnemyLaser:
			return "Enemy7";
			break;

		case Image.EnemyOrbit:
			return "Enemy8";
			break;


			
		case Image.EnemyKite:
			return "Enemy9";
			break;
			
		case Image.EnemyStingray:
			return "Enemy10";
			break;
			
		case Image.EnemyDiamond:
			return "Enemy11";
			break;
			
		case Image.EnemyOx:
			return "Enemy12";
			break;
			
		case Image.EnemyFrigate:
			return "Enemy13";
			break;




		case Image.GN_Beamer:
			return "GN_Enemy1";
			break;

		case Image.GN_Kite:
			return "GN_Enemy2";
			break;

		case Image.GN_Stingray:
			return "GN_Enemy3";
			break;

		case Image.GN_Diamond:
			return "GN_Enemy4";
			break;

		case Image.GN_Ox:
			return "GN_Enemy5";
			break;

		case Image.GN_Frigate:
			return "GN_Enemy6";
			break;



		case Image.RE_Beamer:
			return "RE_Enemy1";
			break;
			
		case Image.RE_Kite:
			return "RE_Enemy2";
			break;
			
		case Image.RE_Stingray:
			return "RE_Enemy3";
			break;
			
		case Image.RE_Diamond:
			return "RE_Enemy4";
			break;
			
		case Image.RE_Ox:
			return "RE_Enemy5";
			break;
			
		case Image.RE_Frigate:
			return "RE_Enemy6";
			break;


		}
		
		return "Enemy6";
	}

	public virtual void Deploy ()
	{
		isActive = true;
		sprite.color = Color.black;
		TerminateEffects();
		RemoveAllTimers();
	}
	
	protected void SetImage (Image image)
	{
		sprite.SetSprite(GetNameOfImage(image));
		if (animator != null) animator.SetSprite(sprite.Collection, sprite.Collection.GetSpriteIdByName(GetNameOfImage(image)));
	}
	
	public virtual void SetBeamLock()
	{	
		isBeamLock = true;
		PlayerUnit playerUnit = GameController.Singleton.playerShip.GetComponent<PlayerUnit>();
		weapon.direction = faceDirection = targetTravelDirection = KJMath.DirectionFromPosition(new Vector2(x, y), new Vector2(playerUnit.x, playerUnit.y));
	}
	
	public virtual bool IsAbleToFireBasic
	{
		get {
			return true;
		}
	}

	public virtual bool IsAbleToFire
	{
		get{
			return true;
		}
	}
	
	
	public virtual Vector3 GetWeaponOffset ()
	{
		return transform.position;
	}
	
	protected override void OnDeactivate ()
	{
		foreach (Weapon weapon in gameObject.GetComponents<Weapon>())
		{
			weapon.Terminate();
		}

		isActive = false;
		TerminateEffects();
		iTween.Stop(gameObject);
		base.OnDeactivate();
	}


	protected void TerminateEffects ()
	{

		if (beam != null) {
			beam.TerminateBeam();
			beam = null;
		}

		if (emitter != null) {
			//Debug.Log ("Terminate Emitter: " + emitter.gameObject.name);
			emitter.Terminate();
			emitter = null;
		}

		if (buffEmitter != null) {
			//Debug.Log ("Terminate Buff Emitter: " + buffEmitter.gameObject.name);
			buffEmitter.Terminate();
			buffEmitter = null;
		}
	}
}

