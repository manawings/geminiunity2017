using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Turret : Entity
{
	
	public float positionDirection = 0, positionDistance = 0, proxyDirection = 0;
		
	// Weapon Reference.
	private List<Weapon> weapons;
	private int currentWeaponId = 0;
		
	// Soft Spot Reference
	private SoftSpot softSpot;
	public bool autoCycle = false;
	
	Weapon proxyWeapon;
	
	public void Deploy (BossWeaponPattern bossWeaponPattern = null, SoftSpot softSpot = null)
	{
		team = Unit.Team.Enemy;
		this.softSpot = softSpot;



		// Set up the Weapons from this Turret.
		weapons = new List<Weapon>();

		// Debug.Log ("Deploy Weapon Pattern: " + bossWeaponPattern);
		// Debug.Log ("Deploy Weapon: " + bossWeaponPattern.weapons.Count);
			
		// Add All Weapons 
		if (bossWeaponPattern != null) {
			for (int i = 0 ; i < bossWeaponPattern.weapons.Count ; i++)
			{
				AddWeapon(bossWeaponPattern.weapons[i]);
			}
		}

		//Debug.Log ("Turret: " + weapons.Count);
			
		// Whether this turret auto-cycles its weapons.
		if (softSpot != null || (weapons.Count == 0)) {
			autoCycle = false;
		} else {
			autoCycle = true;
			// weapon.StartShooting();
		}
			
		stats = GameController.CurrentSectorBoss.stats;

	}
	

	void AddWeapon(Weapon modelWeapon)
	{
		proxyWeapon = gameObject.AddComponent<Weapon>();
		proxyWeapon.CopyDataFrom(modelWeapon);
		proxyWeapon.Deploy();
		weapons.Add(proxyWeapon);
		proxyWeapon.entity = this;
		GameController.CurrentSectorBoss.CalibrateWeapon(proxyWeapon);
	}
	
	public void AlignPosition (KJBehavior spriteBehavior)
	{
		proxyDirection = positionDirection - spriteBehavior.RotationByRadians;

		x = spriteBehavior.transform.position.x + Mathf.Sin(proxyDirection) * positionDistance;
		y = spriteBehavior.transform.position.y - Mathf.Cos(proxyDirection) * positionDistance;

		float directionToPlayer = KJMath.DirectionFromPosition(transform.position, GameController.PlayerUnit.transform.position);
		
		transform.position = new Vector3(x, y, 30.0f);

		if (proxyWeapon != null) {
			if (isLockingOn) {
				proxyWeapon.direction += 0.008f * beamSweepDirection;
			} else {
				proxyWeapon.direction = directionToPlayer;
			}
		}
			
	}
	
	int beamSweepDirection = 0;
	
	public override void SetBeamLock ()
	{
		beamSweepDirection = 0;
		proxyWeapon.direction = KJMath.DirectionFromPosition(transform.position, GameController.PlayerUnit.transform.position);
		
		if (proxyWeapon.direction > Mathf.PI) {
			proxyWeapon.direction += 0.6f;
			beamSweepDirection = -1;
		} else {
			proxyWeapon.direction -= 0.6f;
			beamSweepDirection = 1;	
		}
	
		isBeamLock = true;
	}

//		override public function onBurstFinish():void 
//		{
//			// Pause a gap between this weapon and the next.
//			if (autoCycle)
//			{
//				freezeWeapons();
//				KJTime.scheduleTimerUpdate(cycleWeapon, 1.5, 1);
//			}
//			
//			if (softSpot) {
//				softSpot.onBurstFinish();
//			}
//			
//			super.onBurstFinish();
//		}
//		
//		override public function setBeamLock():void
//		{
//			super.setBeamLock();
//			weaponDirection = targetDirection;
//		}
//		
	public void FreezeWeapons ()
	{
		if (proxyWeapon != null)
		{
			proxyWeapon.StopShooting();
		}
	}
		
	public void CycleWeapon () 
	{
		if (proxyWeapon != null)
		{
			proxyWeapon.StopShooting();
				
			// Find the Next Weapon and Start shooting.
			float oldWeaponDirection = proxyWeapon.direction;
			proxyWeapon = weapons[currentWeaponId];
			proxyWeapon.direction = oldWeaponDirection;
			proxyWeapon.StartShooting();
							
			currentWeaponId ++;
			if (currentWeaponId == weapons.Count) currentWeaponId = 0;
		}
	}
	
	public void Terminate ()
	{
		foreach (Weapon weapon in weapons)
		{
			weapon.Terminate();
		}
		Deactivate();
	}
}

