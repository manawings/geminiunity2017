using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossPattern
{

	public enum MoveMode {
		Normal,
		Encircle,
		Spin
	}

	//DESTROYER TYPE

	public static BossPattern DESTROYER_T0;
	public static BossPattern DESTROYER_T1A;
	public static BossPattern DESTROYER_T1B;
	public static BossPattern DESTROYER_T2 ;


	//CRUSHER TYPE

	public static BossPattern CRUSHER_T0;
	public static BossPattern CRUSHER_T1A;
	public static BossPattern CRUSHER_T1B;
	public static BossPattern CRUSHER_T2;

	
	// GUARDIAN TYPE 

	public static BossPattern GUARDIAN_T0;
	public static BossPattern GUARDIAN_T1A;
	public static BossPattern GUARDIAN_T1B;
	public static BossPattern GUARDIAN_T2;


	// OMEGA TYPE 
	
	public static BossPattern OMEGA_T0;


	// TITAN TYPE 
	
	public static BossPattern TITAN_T0;

	// ORBITAL TYPE

	public static BossPattern ORBITAL_T0;

	// ARBITER TYPE
	
	public static BossPattern ARBITER_T0;

	// DREADNOUGHT TYPE
	
	public static BossPattern DREADNOUGHT_T0;
	public static BossPattern DREADNOUGHT_T1;
	public static BossPattern DREADNOUGHT_T2;
	public static BossPattern DREADNOUGHT_T3;


	
	public static List<BossPattern> allPatterns = new List<BossPattern>();
		
	public List<SoftSpotPattern> softSpotPatterns = new List<SoftSpotPattern>();
	public List<TurretPattern> turretPatterns = new List<TurretPattern>();
		
	// Cycle Gaps
	public float cycleGapChain = 2.0f; // Gap between chaining Soft Spots.
	public float cycleGapWeapon = 1.0f;// Gap between turning on a soft spot and activating the weapon.
	public float cycleGapSwitch = 5.0f;// Switch every X seconds.
	public float cycleGapPause = 3.0f;// Downtime in seconds between cycling.
	public float softSpotDuration  = 3.0f;  //Time that softspot vulnerable for

	
	// Standard Co-Efficients
	public float armorFactor = 1.0f;
	public float lifeFactor = 1.0f;
	public float damageFactor = 1.0f;
		
	// Scaling Functions
	public int minLevel = 0;
	public int maxLevel = int.MaxValue;
	public MoveMode moveMode = MoveMode.Normal;
	
	// Image
	public string bossImageName = "Boss1";

	//Name 
	public string name  = "Boss name ";
		
	public BossPattern () 
	{
		allPatterns.Add(this);
	}
		
	public void AddSoftSpot ( SoftSpotPattern softSpotPattern )
	{
		softSpotPatterns.Add(softSpotPattern);
	}
		
	public void AddTurret ( TurretPattern turretPattern ) 
	{
		turretPatterns.Add(turretPattern);
	}

	private static bool hasBeenInit = false;
		
	public static void Initialize ()
	{	
		if (hasBeenInit) return;

		hasBeenInit = true;

		SoftSpotPattern proxySoftSpot;
		// Destroyer Boss
	
		DESTROYER_T0 = new BossPattern();
		DESTROYER_T0.name = "DESTROYER_T0";
		DESTROYER_T0.bossImageName = "Boss1";
		DESTROYER_T0.minLevel = 1;
		DESTROYER_T0.maxLevel = 10;

		DESTROYER_T0.AddTurret(new TurretPattern(130, KJMath.RIGHT_ANGLE + 0.15f, BossWeaponPattern.DestoryerSpecial));
		DESTROYER_T0.AddTurret(new TurretPattern(130, - KJMath.RIGHT_ANGLE - 0.15f, BossWeaponPattern.DestoryerSpecial));
			
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DestoryerStandard;
		proxySoftSpot.radius = 50;
		proxySoftSpot.direction = Mathf.PI;
		proxySoftSpot.distance = 24;

		DESTROYER_T0.cycleGapChain = 2.5f; // Gap between chaining Soft Spots.
		DESTROYER_T0.cycleGapWeapon = 2.0f;// Gap between turning on a soft spot and activating the weapon.
		DESTROYER_T0.cycleGapSwitch = 6.0f;// Switch every X seconds.
		DESTROYER_T0.cycleGapPause = 4.0f;// Downtime in seconds between cycling.
		DESTROYER_T0.AddSoftSpot(proxySoftSpot);


		DESTROYER_T1A = new BossPattern();
		DESTROYER_T1A.name = "DESTROYER_T1A";
		DESTROYER_T1A.bossImageName = "Boss1";
		DESTROYER_T1A.minLevel = 11;
		DESTROYER_T1A.maxLevel = 50;
		
		DESTROYER_T1A.AddTurret(new TurretPattern(130, KJMath.RIGHT_ANGLE + 0.15f, BossWeaponPattern.DestoryerSpecial));
		DESTROYER_T1A.AddTurret(new TurretPattern(130, - KJMath.RIGHT_ANGLE - 0.15f, BossWeaponPattern.DestoryerSpecial));
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DestoryerStandard;
		proxySoftSpot.radius = 45;
		proxySoftSpot.direction = Mathf.PI;
		proxySoftSpot.distance = 24;
		
		DESTROYER_T1A.cycleGapChain = 2.5f; // Gap between chaining Soft Spots.
		DESTROYER_T1A.cycleGapWeapon = 1.0f;// Gap between turning on a soft spot and activating the weapon.
		DESTROYER_T1A.cycleGapSwitch = 3.0f;// Switch every X seconds.
		DESTROYER_T1A.cycleGapPause = 2.0f;// Downtime in seconds between cycling.
		DESTROYER_T1A.AddSoftSpot(proxySoftSpot);


		DESTROYER_T1B = new BossPattern();
		DESTROYER_T1B.name = "DESTROYER_T1B";
		DESTROYER_T1B.bossImageName = "Boss1";
		DESTROYER_T1B.minLevel = 11;
		DESTROYER_T1B.maxLevel = 50;
		
		DESTROYER_T1B.AddTurret(new TurretPattern(130, KJMath.RIGHT_ANGLE + 0.15f, BossWeaponPattern.DestoryerSpecial));
		DESTROYER_T1B.AddTurret(new TurretPattern(130, - KJMath.RIGHT_ANGLE - 0.15f, BossWeaponPattern.DestoryerSpecial));
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DestoryerStandard;
		proxySoftSpot.radius = 45;
		proxySoftSpot.direction = Mathf.PI;
		proxySoftSpot.distance = 24;
		
		DESTROYER_T1B.cycleGapChain = 2.5f; // Gap between chaining Soft Spots.
		DESTROYER_T1B.cycleGapWeapon = 2.0f;// Gap between turning on a soft spot and activating the weapon.
		DESTROYER_T1B.cycleGapSwitch = 2.0f;// Switch every X seconds.
		DESTROYER_T1B.cycleGapPause = 4.0f;// Downtime in seconds between cycling.
		DESTROYER_T1B.AddSoftSpot(proxySoftSpot);

		DESTROYER_T2 = new BossPattern();
		DESTROYER_T2.name = "DESTROYER_T2";
		DESTROYER_T2.bossImageName = "Boss1";
		DESTROYER_T2.minLevel = 51;
		DESTROYER_T2.maxLevel = 999;
		
		DESTROYER_T2.AddTurret(new TurretPattern(130, KJMath.RIGHT_ANGLE + 0.15f, BossWeaponPattern.DestoryerSpecial));
		DESTROYER_T2.AddTurret(new TurretPattern(130, - KJMath.RIGHT_ANGLE - 0.15f, BossWeaponPattern.DestoryerSpecial));
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DestoryerStandard;
		proxySoftSpot.radius = 40;
		proxySoftSpot.direction = Mathf.PI;
		proxySoftSpot.distance = 24;
		
		DESTROYER_T2.cycleGapChain = 2.5f; // Gap between chaining Soft Spots.
		DESTROYER_T2.cycleGapWeapon = 0.75f;// Gap between turning on a soft spot and activating the weapon.
		DESTROYER_T2.cycleGapSwitch = 6.0f;// Switch every X seconds.
		DESTROYER_T2.cycleGapPause = 4.0f;// Downtime in seconds between cycling.
		DESTROYER_T2.AddSoftSpot(proxySoftSpot);



		// Crusher Boss
			
		CRUSHER_T0 = new BossPattern();
		CRUSHER_T0.name = "CRUSHER_T0";
		CRUSHER_T0.bossImageName = "Boss2";
		CRUSHER_T0.minLevel = 1;
		CRUSHER_T0.maxLevel = 10;
		CRUSHER_T0.lifeFactor = 0.8f;
			
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.CrusherSpecial;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE - 0.115f;
		proxySoftSpot.distance = 98;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		CRUSHER_T0.AddSoftSpot(proxySoftSpot);
			
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.CrusherSpecial;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = - KJMath.RIGHT_ANGLE + 0.115f;
		proxySoftSpot.distance = 98;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		CRUSHER_T0.AddSoftSpot(proxySoftSpot);

		CRUSHER_T0.cycleGapChain = 1.0f; // Gap between chaining Soft Spots.
		CRUSHER_T0.cycleGapWeapon = 2.0f;// Gap between turning on a soft spot and activating the weapon.
		CRUSHER_T0.cycleGapSwitch = 1.0f;// Switch every X seconds.
		CRUSHER_T0.cycleGapPause = 3.0f;

		CRUSHER_T0.AddTurret(new TurretPattern(30, Mathf.PI, BossWeaponPattern.CrusherStandard));
		
		CRUSHER_T1A = new BossPattern();
		CRUSHER_T1A.name = "CRUSHER_T1A";
		CRUSHER_T1A.bossImageName = "Boss2";
		CRUSHER_T1A.minLevel = 11;
		CRUSHER_T1A.maxLevel = 50;
		CRUSHER_T1A.lifeFactor = 0.8f;
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.CrusherSpecial;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE - 0.115f;
		proxySoftSpot.distance = 98;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		CRUSHER_T1A.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.CrusherSpecial;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = - KJMath.RIGHT_ANGLE + 0.115f;
		proxySoftSpot.distance = 98;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		CRUSHER_T1A.AddSoftSpot(proxySoftSpot);

		CRUSHER_T1A.cycleGapChain = 1.0f ;
		CRUSHER_T1A.cycleGapSwitch = 7.0f;
		CRUSHER_T1A.cycleGapWeapon = 1.0f;
		
		CRUSHER_T1A.AddTurret(new TurretPattern(30, Mathf.PI, BossWeaponPattern.CrusherStandard));
		
		CRUSHER_T1B = new BossPattern();
		CRUSHER_T1B.name = "CRUSHER_T1B";
		CRUSHER_T1B.bossImageName = "Boss2";
		CRUSHER_T1B.minLevel = 11;
		CRUSHER_T1B.maxLevel = 50;
		CRUSHER_T1B.lifeFactor = 0.7f;
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.CrusherStandard;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE - 0.115f;
		proxySoftSpot.distance = 98;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		CRUSHER_T1B.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.CrusherStandard;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = - KJMath.RIGHT_ANGLE + 0.115f;
		proxySoftSpot.distance = 98;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		CRUSHER_T1B.AddSoftSpot(proxySoftSpot);

		CRUSHER_T1B.cycleGapChain = 1.0f ;
		CRUSHER_T1B.cycleGapSwitch = 5.0f;
		CRUSHER_T1B.cycleGapWeapon = 0.5f;
		
		CRUSHER_T1B.AddTurret(new TurretPattern(30, Mathf.PI, BossWeaponPattern.CrusherStandard));


		CRUSHER_T2 = new BossPattern();
		CRUSHER_T2.name = "CRUSHER_T2";
		CRUSHER_T2.bossImageName = "Boss2";
		CRUSHER_T2.minLevel = 51;
		CRUSHER_T2.maxLevel = 999;	
		CRUSHER_T2.lifeFactor = 0.7f;

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.CrusherSpecial;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE - 0.115f;
		proxySoftSpot.distance = 98;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		CRUSHER_T2.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.CrusherSpecial;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = - KJMath.RIGHT_ANGLE + 0.115f;
		proxySoftSpot.distance = 98;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		CRUSHER_T2.AddSoftSpot(proxySoftSpot);

		CRUSHER_T2.cycleGapChain = 0.5f;
		CRUSHER_T2.cycleGapSwitch = 4.0f;
		CRUSHER_T2.cycleGapWeapon = 1.0f;

		CRUSHER_T2.AddTurret(new TurretPattern(30, Mathf.PI, BossWeaponPattern.CrusherStandard));

		// Guardian Boss
			
		GUARDIAN_T0 = new BossPattern();
		GUARDIAN_T0.name = "GUARDIAN_T0";
		GUARDIAN_T0.bossImageName = "Boss3";
		GUARDIAN_T0.minLevel = 1;
		GUARDIAN_T0.maxLevel = 10;
		GUARDIAN_T0.moveMode = MoveMode.Spin;

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T0.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE * 3;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T0.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.radius = 40;
		proxySoftSpot.isCycling = true;
		GUARDIAN_T0.AddSoftSpot(proxySoftSpot);
		
		GUARDIAN_T0.cycleGapChain = 1.0f ;
		GUARDIAN_T0.cycleGapWeapon = 2.0f; 
		GUARDIAN_T0.cycleGapSwitch = 1.5f ;
		GUARDIAN_T0.AddTurret(new TurretPattern(0, 0, BossWeaponPattern.GuardianMainCore));
			

		GUARDIAN_T1A = new BossPattern();
		GUARDIAN_T1A.name = "GUARDIAN_T1A";
		GUARDIAN_T1A.bossImageName = "Boss3";
		GUARDIAN_T1A.minLevel = 11;
		GUARDIAN_T1A.maxLevel = 50;
		GUARDIAN_T1A.moveMode = MoveMode.Spin;

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = false;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = 0;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T1A.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = false;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T1A.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = false;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;		
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE * 2;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T1A.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = false;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE * 3;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T1A.AddSoftSpot(proxySoftSpot);

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.radius = 40;
		proxySoftSpot.isCycling = true;
		GUARDIAN_T1A.AddSoftSpot(proxySoftSpot);

		GUARDIAN_T1A.AddTurret(new TurretPattern(0, 0, BossWeaponPattern.GuardianMainCore));

		GUARDIAN_T1A.cycleGapChain = 1f;
		GUARDIAN_T1A.cycleGapWeapon = 1.5f; 
		GUARDIAN_T1A.cycleGapSwitch = 3f ;
		GUARDIAN_T1A.cycleGapPause  = 1.5f;
		GUARDIAN_T1A.softSpotDuration = 3f;

		GUARDIAN_T1B = new BossPattern();
		GUARDIAN_T1B.name = "GUARDIAN_T1B";
		GUARDIAN_T1B.bossImageName = "Boss3";
		GUARDIAN_T1B.minLevel = 11;
		GUARDIAN_T1B.maxLevel = 50;
		GUARDIAN_T1B.moveMode = MoveMode.Spin;
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE * 2;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T1B.AddSoftSpot(proxySoftSpot);

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T1B.AddSoftSpot(proxySoftSpot);

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE * 3;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T1B.AddSoftSpot(proxySoftSpot);

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.radius = 40;
		proxySoftSpot.isCycling = true;
		GUARDIAN_T1B.AddSoftSpot(proxySoftSpot);

		GUARDIAN_T1B.cycleGapChain = 1.5f ;
		GUARDIAN_T1B.cycleGapWeapon = 1.5f; 
		GUARDIAN_T1B.cycleGapSwitch = 7.0f ;

		GUARDIAN_T1B.AddTurret(new TurretPattern(0, 0, BossWeaponPattern.GuardianMainCore));


		GUARDIAN_T2 = new BossPattern();
		GUARDIAN_T2.name = "GUARDIAN_T2";
		GUARDIAN_T2.bossImageName = "Boss3";
		GUARDIAN_T2.minLevel = 51;
		GUARDIAN_T2.maxLevel = 999;
		GUARDIAN_T2.moveMode = MoveMode.Spin;

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = 0;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T2.AddSoftSpot(proxySoftSpot);

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T2.AddSoftSpot(proxySoftSpot);
					
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE * 2;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T2.AddSoftSpot(proxySoftSpot);
					
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.GuardianArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE * 3;
		proxySoftSpot.distance = 97;
		proxySoftSpot.lifeFactor = 0.25f;
		proxySoftSpot.radius = 25;
		GUARDIAN_T2.AddSoftSpot(proxySoftSpot);
					
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.radius = 40;
		proxySoftSpot.isCycling = true;
		GUARDIAN_T2.AddSoftSpot(proxySoftSpot);

		GUARDIAN_T2.cycleGapChain = 1.25f ;
		GUARDIAN_T2.cycleGapWeapon = 1.25f; 
		GUARDIAN_T2.cycleGapSwitch = 7.0f ;

		GUARDIAN_T2.AddTurret(new TurretPattern(0, 0, BossWeaponPattern.GuardianMainCore));


		// OMEGA CLASS
		
		OMEGA_T0 = new BossPattern();
		OMEGA_T0.name = "OMEGA_T0";
		OMEGA_T0.bossImageName = "Boss4";
		OMEGA_T0.minLevel = 1;
		OMEGA_T0.maxLevel = 999;
		OMEGA_T0.lifeFactor = 1.0f;
		
		OMEGA_T0.AddTurret(new TurretPattern(92, KJMath.RIGHT_ANGLE + 0.7f, BossWeaponPattern.OmegaTurret));
		OMEGA_T0.AddTurret(new TurretPattern(92, - KJMath.RIGHT_ANGLE - 0.7f, BossWeaponPattern.OmegaTurret));
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.OmegaWeapon;
		proxySoftSpot.radius = 50;
		proxySoftSpot.direction = Mathf.PI;
		proxySoftSpot.distance = 28;
		proxySoftSpot.cycleWithWeapon = false;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = false;
		
		OMEGA_T0.cycleGapChain = 1.0f; // Gap between chaining Soft Spots.
		OMEGA_T0.cycleGapWeapon = 1.0f;// Gap between turning on a soft spot and activating the weapon.
		OMEGA_T0.cycleGapSwitch = 2.5f;// Switch every X seconds.
		OMEGA_T0.cycleGapPause = 2.5f;// Downtime in seconds between cycling.

		OMEGA_T0.AddSoftSpot(proxySoftSpot);


		// TITAN CLASS
		
		TITAN_T0 = new BossPattern();
		TITAN_T0.name = "TITAN_T0";
		TITAN_T0.bossImageName = "Boss5";
		TITAN_T0.minLevel = 1;
		TITAN_T0.maxLevel = 9999;
		TITAN_T0.lifeFactor = 0.6f;
		
//		TITAN_T0.AddTurret(new TurretPattern(110, KJMath.RIGHT_ANGLE + 0.5f, BossWeaponPattern.CrusherSpecial));
//		TITAN_T0.AddTurret(new TurretPattern(110, - KJMath.RIGHT_ANGLE - 0.5f, BossWeaponPattern.CrusherSpecial));
//		

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.TitanWeapon;
		proxySoftSpot.radius = 50;
		proxySoftSpot.direction = Mathf.PI;
		proxySoftSpot.distance = 53;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		TITAN_T0.AddSoftSpot(proxySoftSpot);

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.TitanWeapon;
		proxySoftSpot.radius = 50;
		proxySoftSpot.direction = Mathf.PI;
		proxySoftSpot.distance = -68;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		TITAN_T0.AddSoftSpot(proxySoftSpot);

		TITAN_T0.cycleGapChain = 2.0f; // Gap between chaining Soft Spots.
		TITAN_T0.cycleGapWeapon = 1.0f;// Gap between turning on a soft spot and activating the weapon.
		TITAN_T0.cycleGapSwitch = 5.0f;// Switch every X seconds.
		TITAN_T0.cycleGapPause = 4.0f;// Downtime in seconds between cycling.

		TITAN_T0.AddTurret(new TurretPattern(0, 0.0f, BossWeaponPattern.TitanTurret));
//		TITAN_T0.AddTurret(new TurretPattern(-160, -0.33f, BossWeaponPattern.TitanTurret));

		TITAN_T0.moveMode = MoveMode.Encircle;

		// ORBITAL BOSS

		ORBITAL_T0 = new BossPattern();
		ORBITAL_T0.name = "ORBITAL_T0";
		ORBITAL_T0.bossImageName = "Boss6";
		ORBITAL_T0.minLevel = 0;
		ORBITAL_T0.maxLevel = 999;
		ORBITAL_T0.moveMode = MoveMode.Spin;
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.OrbitaArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.CIRCLE * 0.166f;
		proxySoftSpot.distance = 112;
		proxySoftSpot.radius = 25;
		ORBITAL_T0.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.OrbitaArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.CIRCLE * 0.50f;
		proxySoftSpot.distance = 113;
		proxySoftSpot.radius = 25;
		ORBITAL_T0.AddSoftSpot(proxySoftSpot);

		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.OrbitaArm;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.reweaponPool = true;
		proxySoftSpot.direction = KJMath.CIRCLE * 0.833f;
		proxySoftSpot.distance = 112;
		proxySoftSpot.radius = 25;
		ORBITAL_T0.AddSoftSpot(proxySoftSpot);
		
		ORBITAL_T0.cycleGapChain = 2.0f; // Gap between chaining Soft Spots.
		ORBITAL_T0.cycleGapWeapon = 2.0f;// Gap between turning on a soft spot and activating the weapon.
		ORBITAL_T0.cycleGapSwitch = 5.0f;// Switch every X seconds.
		ORBITAL_T0.cycleGapPause = 4.0f;// Downtime in seconds between cycling.

		ORBITAL_T0.AddTurret(new TurretPattern(0, 0, BossWeaponPattern.OrbitaCore));

		// ARBITER CLASS

		ARBITER_T0 = new BossPattern();
		ARBITER_T0.name = "ARBITER_T0";
		ARBITER_T0.bossImageName = "Boss7";
		ARBITER_T0.minLevel = 0;
		ARBITER_T0.maxLevel = 999;	
		ARBITER_T0.lifeFactor = 0.7f;
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.ArbiterTurret;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE + 0.20f;
		proxySoftSpot.distance = 66;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		ARBITER_T0.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.ArbiterTurret;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = - KJMath.RIGHT_ANGLE - 0.20f;
		proxySoftSpot.distance = 66;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		ARBITER_T0.AddSoftSpot(proxySoftSpot);
		
		ARBITER_T0.cycleGapChain = 2.0f; // Gap between chaining Soft Spots.
		ARBITER_T0.cycleGapWeapon = 1.0f;// Gap between turning on a soft spot and activating the weapon.
		ARBITER_T0.cycleGapSwitch = 5.0f;// Switch every X seconds.
		ARBITER_T0.cycleGapPause = 4.0f;// Downtime in seconds between cycling.
		
		ARBITER_T0.AddTurret(new TurretPattern(30, Mathf.PI, BossWeaponPattern.ArbiterCore));

		// DREADNOUGHT CLASS
		
		DREADNOUGHT_T0 = new BossPattern();
		DREADNOUGHT_T0.name = "DREADNOUGHT_T0";
		DREADNOUGHT_T0.bossImageName = "Boss8";
		DREADNOUGHT_T0.minLevel = 0;
		DREADNOUGHT_T0.maxLevel = 999;
		DREADNOUGHT_T0.lifeFactor = 1.2f;
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DreadnoughtTurret_0;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE - 0.20f;
		proxySoftSpot.distance = 100;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		DREADNOUGHT_T0.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DreadnoughtTurret_0;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = - KJMath.RIGHT_ANGLE + 0.20f;
		proxySoftSpot.distance = 100;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		DREADNOUGHT_T0.AddSoftSpot(proxySoftSpot);
		
		DREADNOUGHT_T0.cycleGapChain = 2.0f;
		DREADNOUGHT_T0.cycleGapSwitch = 4.0f;
		DREADNOUGHT_T0.cycleGapWeapon = 2.0f;
		
		DREADNOUGHT_T0.AddTurret(new TurretPattern(30, Mathf.PI, BossWeaponPattern.DreadnoughtCore_0));

		//

		DREADNOUGHT_T1 = new BossPattern();
		DREADNOUGHT_T1.name = "DREADNOUGHT_T1";
		DREADNOUGHT_T1.bossImageName = "Boss8";
		DREADNOUGHT_T1.minLevel = 0;
		DREADNOUGHT_T1.maxLevel = 999;
		DREADNOUGHT_T1.lifeFactor = 1.3f;
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DreadnoughtTurret_1;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE - 0.20f;
		proxySoftSpot.distance = 100;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		DREADNOUGHT_T1.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DreadnoughtTurret_1;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = - KJMath.RIGHT_ANGLE + 0.20f;
		proxySoftSpot.distance = 100;
		proxySoftSpot.lifeFactor = 0.50f;
		proxySoftSpot.radius = 25;
		DREADNOUGHT_T1.AddSoftSpot(proxySoftSpot);
		
		DREADNOUGHT_T1.cycleGapChain = 2.0f;
		DREADNOUGHT_T1.cycleGapSwitch = 4.0f;
		DREADNOUGHT_T1.cycleGapWeapon = 2.0f;
		
		DREADNOUGHT_T1.AddTurret(new TurretPattern(30, Mathf.PI, BossWeaponPattern.DreadnoughtCore_1));

		//
		
		DREADNOUGHT_T2 = new BossPattern();
		DREADNOUGHT_T2.name = "DREADNOUGHT_T2";
		DREADNOUGHT_T2.bossImageName = "Boss8";
		DREADNOUGHT_T2.minLevel = 0;
		DREADNOUGHT_T2.maxLevel = 999;
		DREADNOUGHT_T2.lifeFactor = 1.4f;
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DreadnoughtTurret_2;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE - 0.20f;
		proxySoftSpot.distance = 100;
		proxySoftSpot.lifeFactor = 0.5f;
		proxySoftSpot.radius = 25;
		DREADNOUGHT_T2.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DreadnoughtTurret_2;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = - KJMath.RIGHT_ANGLE + 0.20f;
		proxySoftSpot.distance = 100;
		proxySoftSpot.lifeFactor = 0.5f;
		proxySoftSpot.radius = 25;
		DREADNOUGHT_T2.AddSoftSpot(proxySoftSpot);
		
		DREADNOUGHT_T2.cycleGapChain = 1.5f;
		DREADNOUGHT_T2.cycleGapSwitch = 4.0f;
		DREADNOUGHT_T2.cycleGapWeapon = 1.5f;
		
		DREADNOUGHT_T2.AddTurret(new TurretPattern(30, Mathf.PI, BossWeaponPattern.DreadnoughtCore_2));

		//
		
		DREADNOUGHT_T3 = new BossPattern();
		DREADNOUGHT_T3.name = "DREADNOUGHT_T3";
		DREADNOUGHT_T3.bossImageName = "Boss8";
		DREADNOUGHT_T3.minLevel = 0;
		DREADNOUGHT_T3.maxLevel = 999;
		DREADNOUGHT_T3.lifeFactor = 1.5f;
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DreadnoughtTurret_3;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = KJMath.RIGHT_ANGLE - 0.20f;
		proxySoftSpot.distance = 100;
		proxySoftSpot.lifeFactor = 0.5f;
		proxySoftSpot.radius = 25;
		DREADNOUGHT_T3.AddSoftSpot(proxySoftSpot);
		
		proxySoftSpot = new SoftSpotPattern();
		proxySoftSpot.bossWeaponPattern = BossWeaponPattern.DreadnoughtTurret_3;
		proxySoftSpot.cycleWithWeapon = true;
		proxySoftSpot.isCycling = true;
		proxySoftSpot.isManual = true;
		proxySoftSpot.direction = - KJMath.RIGHT_ANGLE + 0.20f;
		proxySoftSpot.distance = 100;
		proxySoftSpot.lifeFactor = 0.5f;
		proxySoftSpot.radius = 25;
		DREADNOUGHT_T3.AddSoftSpot(proxySoftSpot);
		
		DREADNOUGHT_T3.cycleGapChain = 1.0f;
		DREADNOUGHT_T3.cycleGapSwitch = 4.0f;
		DREADNOUGHT_T3.cycleGapWeapon = 1.0f;
		
		DREADNOUGHT_T3.AddTurret(new TurretPattern(30, Mathf.PI, BossWeaponPattern.DreadnoughtCore_3));
		

	}

	public static BossPattern GetBossPatternForLevel (float level)
	{

		// TEST

//		return ORBITAL_T0;

		List<BossPattern> bossList = new List<BossPattern>();

		List<BossPattern> crusherList = new List<BossPattern>();
		List<BossPattern> destroyerList = new List<BossPattern>();
		List<BossPattern> guardianList = new List<BossPattern>();
		List<BossPattern> omegaList = new List<BossPattern>();
		List<BossPattern> arbiterList = new List<BossPattern>();
		List<BossPattern> orbiralList = new List<BossPattern>();

		for ( int i = 0 ; i < allPatterns.Count ; i ++ )
		{
			BossPattern bossPattern = allPatterns[i];
			int realLevel =  ProfileController.SectorId;
			//Debug.Log(bossPattern.minLevel  + " and " + bossPattern.name + " and " + realLevel) ;
			//if ( (bossPattern.minLevel <= Mathf.Round(level)) && (bossPattern.maxLevel >=  Mathf.Round(level)) )
			if ( (bossPattern.minLevel <= realLevel) && (bossPattern.maxLevel >=  realLevel) )
			{
				//Debug.Log("add");
				if (bossPattern == CRUSHER_T0 || bossPattern == CRUSHER_T1A || bossPattern == CRUSHER_T1B || bossPattern == CRUSHER_T2)
					crusherList.Add(bossPattern);
				if (bossPattern == DESTROYER_T0 || bossPattern == DESTROYER_T1A || bossPattern == DESTROYER_T1B || bossPattern == DESTROYER_T2)
					destroyerList.Add(bossPattern);
				if (bossPattern == GUARDIAN_T0 || bossPattern == GUARDIAN_T1A || bossPattern == GUARDIAN_T1B || bossPattern == GUARDIAN_T2)
					guardianList.Add(bossPattern);
				if (bossPattern == OMEGA_T0)
					omegaList.Add(bossPattern);
				if (bossPattern == ARBITER_T0)
					arbiterList.Add(bossPattern);
				if (bossPattern == ORBITAL_T0)
					orbiralList.Add(bossPattern);

				bossList.Add (bossPattern);
			}
			bossList.Remove(TITAN_T0);
		}

//		bossList.Clear(); // test
//		bossList.Add(OMEGA_T0);// test

		//BossPattern returnPattern = KJDice.RandomMemberOf<BossPattern>(bossList);
		BossPattern returnPattern = new BossPattern();
//		Debug.Log("ProfileController.SectorId % 4 = " + ProfileController.SectorId % 4);
		switch ((ProfileController.SectorId % 6)) {
				
			case 1:
			returnPattern = KJDice.RandomMemberOf<BossPattern>(destroyerList);
			break;
				
			case 2:
			returnPattern = KJDice.RandomMemberOf<BossPattern>(crusherList);
			break;
				
			case 3:
			returnPattern = KJDice.RandomMemberOf<BossPattern>(guardianList);
			break;
				
			case 4:
			returnPattern = KJDice.RandomMemberOf<BossPattern>(omegaList);
			break;
				
			case 5:
			returnPattern = KJDice.RandomMemberOf<BossPattern>(arbiterList);
			break;

			case 0:
			returnPattern = KJDice.RandomMemberOf<BossPattern>(orbiralList);
			break;

		}
		//Debug.Log(returnPattern.name);

		if (ProfileController.secretBoss) {

			returnPattern = TITAN_T0;

		} else if (ProfileController.secretRaid) {

			switch (ProfileController.RaidProgress) {
			case 0:
				returnPattern = DREADNOUGHT_T0;
				break;
			case 1:
				returnPattern = DREADNOUGHT_T1;
				break;
			case 2:
				returnPattern = DREADNOUGHT_T2;
				break;
			case 3:
				returnPattern = DREADNOUGHT_T3;
				break;
			}
		} else {

			if (ProfileController.Sector.bossOveride != 0) {
//				Debug.Log("ProfileController.Sector.bossOveride = " +  ProfileController.Sector.bossOveride);
				switch (ProfileController.Sector.bossOveride) {
					
				case 1:
					returnPattern = KJDice.RandomMemberOf<BossPattern>(destroyerList);
					break;
					
				case 2:
					returnPattern = KJDice.RandomMemberOf<BossPattern>(crusherList);
					break;
					
				case 3:
					returnPattern = KJDice.RandomMemberOf<BossPattern>(guardianList);
					break;
					
				case 4:
					returnPattern = KJDice.RandomMemberOf<BossPattern>(omegaList);
					break;

				case 5:
					returnPattern = KJDice.RandomMemberOf<BossPattern>(arbiterList);
					break;

				case 6:
					returnPattern = KJDice.RandomMemberOf<BossPattern>(orbiralList);
					break;
					
//				case 5: 
//					returnPattern = BossPattern.TITAN_T0;
//					break;
				}
			}
		}
		Debug.Log(returnPattern.name);
		return returnPattern;

	}
}

