using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossWeaponPattern
{
	public static BossWeaponPattern Bullet = new BossWeaponPattern();
	public static BossWeaponPattern Support = new BossWeaponPattern();
	public static BossWeaponPattern GuardianMainCore = new BossWeaponPattern();
	public static BossWeaponPattern GuardianArm = new BossWeaponPattern();
	public static BossWeaponPattern Scatter = new BossWeaponPattern();
	public static BossWeaponPattern Disable = new BossWeaponPattern();
	public static BossWeaponPattern NUKE =  new BossWeaponPattern();
	public static BossWeaponPattern STANDARD = new BossWeaponPattern();
	public static BossWeaponPattern SPECIAL = new BossWeaponPattern();
	public static BossWeaponPattern EXTREME = new BossWeaponPattern();

	public static BossWeaponPattern DestoryerStandard = new BossWeaponPattern();
	public static BossWeaponPattern DestoryerSpecial = new BossWeaponPattern();
	public static BossWeaponPattern CrusherStandard =  new BossWeaponPattern();
	public static BossWeaponPattern CrusherSpecial =  new BossWeaponPattern();

	public static BossWeaponPattern TitanTurret =  new BossWeaponPattern();
	public static BossWeaponPattern TitanWeapon = new BossWeaponPattern();

	public static BossWeaponPattern OmegaTurret = new BossWeaponPattern();
	public static BossWeaponPattern OmegaWeapon = new BossWeaponPattern();

	public static BossWeaponPattern OrbitaCore = new BossWeaponPattern();
	public static BossWeaponPattern OrbitaArm = new BossWeaponPattern();

	public static BossWeaponPattern ArbiterCore = new BossWeaponPattern();
	public static BossWeaponPattern ArbiterTurret = new BossWeaponPattern();

	public static BossWeaponPattern DreadnoughtCore_0 = new BossWeaponPattern();
	public static BossWeaponPattern DreadnoughtTurret_0 = new BossWeaponPattern();

	public static BossWeaponPattern DreadnoughtCore_1 = new BossWeaponPattern();
	public static BossWeaponPattern DreadnoughtTurret_1 = new BossWeaponPattern();

	public static BossWeaponPattern DreadnoughtCore_2 = new BossWeaponPattern();
	public static BossWeaponPattern DreadnoughtTurret_2 = new BossWeaponPattern();

	public static BossWeaponPattern DreadnoughtCore_3 = new BossWeaponPattern();
	public static BossWeaponPattern DreadnoughtTurret_3 = new BossWeaponPattern();

	public List<Weapon> availableWeapons;
	public List<Weapon> weapons;
	const float logFactor  = 1.85f;
	public string name = "UNNAMED";


	public void AddWithLevelControl( Weapon weapon ,int minlevel = 1, int maxlevel = 999)
	{
		weapon.minLevelBossUse = minlevel;
		weapon.maxLevelBossUse = maxlevel;
		availableWeapons.Add(weapon);
	}
		
	public static void Reset ()


	{


//		Debug.Log("BossWeaponPattern Reset");

		DestoryerStandard.availableWeapons = new List<Weapon>();
		DestoryerStandard.name = "DestoryerStandard";
		DestoryerStandard.AddWithLevelControl(WeaponPattern.E_Rapid,0,10);
		DestoryerStandard.AddWithLevelControl(WeaponPattern.B_ScatterPink,0,999);
		DestoryerStandard.AddWithLevelControl(WeaponPattern.E_EliteRapid_Easy,0,10);
		DestoryerStandard.AddWithLevelControl(WeaponPattern.B_BlueStun,5,999);
		DestoryerStandard.AddWithLevelControl(WeaponPattern.B_Green_Electricball, 5, 999);
		DestoryerStandard.AddWithLevelControl(WeaponPattern.B_RedScatter, 0, 999);
		DestoryerStandard.AddWithLevelControl(WeaponPattern.B_DestoryerRedScatterLaser);
		DestoryerStandard.AddWithLevelControl(WeaponPattern.B_DestoryerBlueScatterLaser);
		DestoryerStandard.AddWithLevelControl(WeaponPattern.B_DestoryerGreenScatterLaser);
		DestoryerStandard.AddWithLevelControl(WeaponPattern.B_DestoryerPinkScatterLaser);
		DestoryerStandard.MullTo(1);

		DestoryerSpecial.availableWeapons = new List<Weapon>();
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.B_Chainer,5,15);
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.B_Scatter, 0, 49);
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.E_Sweep, 0, 15);
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.E_EliteRapid,0,15);
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.E_StrikerMissiles,31,999);
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.B_Homing, 3, 999);
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.E_EliteRapid2);
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.B_Pink_Electricball,15,999);
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.B_DestoryerBlueMissile, 5, 999);
		DestoryerSpecial.AddWithLevelControl(WeaponPattern.B_Blue_Electricball,8,999);
		DestoryerSpecial.MullTo(4);

		GuardianMainCore.availableWeapons = new List<Weapon>();
		GuardianMainCore.AddWithLevelControl(WeaponPattern.B_BlueStun);
		GuardianMainCore.AddWithLevelControl(WeaponPattern.B_AutoFire);
		GuardianMainCore.AddWithLevelControl(WeaponPattern.B_G_RapidRed);
		GuardianMainCore.AddWithLevelControl(WeaponPattern.B_SlowShockWave);
		GuardianMainCore.AddWithLevelControl(WeaponPattern.B_SpreadStun);
		GuardianMainCore.MullTo(3);

		GuardianArm.availableWeapons = new List<Weapon>();
		GuardianArm.AddWithLevelControl(WeaponPattern.B_Scatter, 0, 15);
		GuardianArm.AddWithLevelControl(WeaponPattern.B_BlueChain,0,10);
		GuardianArm.AddWithLevelControl(WeaponPattern.B_BlueMissile,0,50);
		GuardianArm.AddWithLevelControl(WeaponPattern.B_Blue_Electricball);
		GuardianArm.AddWithLevelControl(WeaponPattern.E_Sweep, 0, 15);
		GuardianArm.AddWithLevelControl(WeaponPattern.E_EliteRapid,0,10);
		GuardianArm.AddWithLevelControl(WeaponPattern.E_StrikerMissiles);
		GuardianArm.AddWithLevelControl(WeaponPattern.B_Homing,0,50);
		GuardianArm.AddWithLevelControl(WeaponPattern.E_EliteRapid2);
		GuardianArm.AddWithLevelControl(WeaponPattern.B_PurpleMissile);
		GuardianArm.AddWithLevelControl(WeaponPattern.B_Pink_Electricball,11,999);
		GuardianArm.AddWithLevelControl(WeaponPattern.E_Beam,11,999);
		GuardianArm.AddWithLevelControl(WeaponPattern.B_FastHoming,11,999);
		GuardianArm.AddWithLevelControl(WeaponPattern.E_EliteRedStriker,51,999);
		GuardianArm.AddWithLevelControl(WeaponPattern.E_EliteBlueStriker,51,999);
		GuardianArm.AddWithLevelControl(WeaponPattern.B_GreenLaserScatter,15,999);
		GuardianArm.AddWithLevelControl(WeaponPattern.B_PurpleScatter,15,999);
		GuardianArm.MullTo(5);

		CrusherStandard.availableWeapons = new List<Weapon>();
		CrusherStandard.AddWithLevelControl(WeaponPattern.B_C_Support,0,50);
		CrusherStandard.AddWithLevelControl(WeaponPattern.B_C_Slow,0,50);
		CrusherStandard.AddWithLevelControl(WeaponPattern.B_BlueStun,0,50);
		CrusherStandard.AddWithLevelControl(WeaponPattern.B_C_ScatterPink,0,50);
		CrusherStandard.AddWithLevelControl(WeaponPattern.B_RapidGreen,0,10);
		CrusherStandard.AddWithLevelControl(WeaponPattern.B_SlowShockWave,0,50);
		CrusherStandard.AddWithLevelControl(WeaponPattern.E_EliteRapid,11,20);
		CrusherStandard.AddWithLevelControl(WeaponPattern.E_EliteRapid2,11,999);
		CrusherStandard.AddWithLevelControl(WeaponPattern.E_BH_Slow,11,20);
		CrusherStandard.AddWithLevelControl(WeaponPattern.E_Sweep,11,15);
		CrusherStandard.AddWithLevelControl(WeaponPattern.B_Green_Electricball,51,999);
		CrusherStandard.AddWithLevelControl(WeaponPattern.B_Red_ElectricBall,15,999);
		CrusherStandard.MullTo(4);

		CrusherSpecial.availableWeapons = new List<Weapon>();
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_BlueChain,1,11);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_GreenScatter,1,11);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_BlueMissile,1,50);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_BlueScatter);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_ShockWave,1,50);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_Blue_Electricball);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_Homing,1,50);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_PurpleMissile,1,50);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_Pink_Electricball,11,999);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.E_StrikerMissiles,11,999);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.M_BlackHole,70,999);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.E_Beam,1,15);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_FastHoming,1,50);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.E_EliteRedStriker,11,999);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.E_EliteBlueStriker,11,999);
		CrusherSpecial.AddWithLevelControl(WeaponPattern.B_PurpleScatter,11,999);
		CrusherSpecial.MullTo(5);

		TitanTurret.availableWeapons = new List<Weapon>();
		TitanTurret.AddWithLevelControl(WeaponPattern.BT_TurretVolleyStun,0,999);
		TitanTurret.AddWithLevelControl(WeaponPattern.BT_TurretSlowStun,0,999);
		TitanTurret.AddWithLevelControl(WeaponPattern.BT_TurretScatterShot,0,999);
		TitanTurret.AddWithLevelControl(WeaponPattern.BT_TurretPlasma,0,999);
		TitanTurret.MullTo(1);

		TitanWeapon.availableWeapons = new List<Weapon>();
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_SonarRed, 10, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_SonarBlue, 10, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_ScatterChain4Blue, 7, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_ScatterChain4Red, 7, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_HommingRed, 0, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_HommingBlue, 0, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_BeamBlue, 5, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_BeamRed, 5, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_ScatterLaserYellow, 0, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_ScatterLaserGreen, 0, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.B_SlowShockWave, 15, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_HomingSonarGreen, 0, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.BT_HomingSonarYellow, 0, 999);
		TitanWeapon.AddWithLevelControl(WeaponPattern.B_SlowShockWave, 0, 999);

		TitanWeapon.MullTo(4);

		OmegaTurret.availableWeapons = new List<Weapon>();
		OmegaTurret.AddWithLevelControl(WeaponPattern.BT_HommingBlue,0,999);
		OmegaTurret.AddWithLevelControl(WeaponPattern.BT_HommingRed, 0, 999);
		OmegaTurret.AddWithLevelControl(WeaponPattern.BT_BeamBlue, 0, 999);
		OmegaTurret.AddWithLevelControl(WeaponPattern.BT_BeamRed, 0, 999);
		OmegaTurret.AddWithLevelControl(WeaponPattern.B_RunScatterPink, 0, 999);
		OmegaTurret.AddWithLevelControl(WeaponPattern.B_RunScatterRed, 0, 999);

		OmegaTurret.MullTo(4);

		OmegaWeapon.availableWeapons = new List<Weapon>();
		OmegaWeapon.AddWithLevelControl(WeaponPattern.E_StrikerMissiles, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_SlowShockWaveRed, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_PurpleMissile, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_GreenLaserScatter, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_ScatterChain4Red, 4, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_ScatterChain4Blue, 4, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_Homing, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_Blue_Electricball, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_Green_Electricball, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_PurpleMissile, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.E_EliteBlueStriker, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_SonarRed, 0, 999);
		OmegaWeapon.AddWithLevelControl(WeaponPattern.B_SonarBlue, 0, 999);
		//OmegaWeapon.AddWithLevelControl(WeaponPattern.B_SuperMissile, 0, 999);
		OmegaWeapon.MullTo(4);

		OrbitaCore.availableWeapons = new List<Weapon>();
		OrbitaCore.AddWithLevelControl(WeaponPattern.Orbita_SpreadBurn);
		OrbitaCore.AddWithLevelControl(WeaponPattern.Orbita_SpreadLaserBurn);
		OrbitaCore.AddWithLevelControl(WeaponPattern.Orbita_LaserBurn);
		OrbitaCore.AddWithLevelControl(WeaponPattern.Orbita_HomingBurn);
		OrbitaCore.AddWithLevelControl(WeaponPattern.Orbita_RedElectricball2);
		OrbitaCore.AddWithLevelControl(WeaponPattern.Orbita_RedElectricball);
		OrbitaCore.AddWithLevelControl(WeaponPattern.Orbita_RedSonarFast);
		OrbitaCore.AddWithLevelControl(WeaponPattern.Orbita_RedSonarSlow);
		OrbitaCore.AddWithLevelControl(WeaponPattern.B_RedScatterLaser);
		OrbitaCore.AddWithLevelControl(WeaponPattern.B_BlueScatterLaser);
		OrbitaCore.AddWithLevelControl(WeaponPattern.B_GreenScatterLaser);
		OrbitaCore.AddWithLevelControl(WeaponPattern.B_PinkScatterLaser);
		OrbitaCore.MullTo(1);
		
		OrbitaArm.availableWeapons = new List<Weapon>();
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_Scatter, 0, 15);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_BlueChain,0,10);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_BlueMissile,0,50);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_Blue_Electricball);
		OrbitaArm.AddWithLevelControl(WeaponPattern.E_Sweep, 0, 15);
		OrbitaArm.AddWithLevelControl(WeaponPattern.E_EliteRapid,0,10);
		OrbitaArm.AddWithLevelControl(WeaponPattern.E_StrikerMissiles);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_Homing,0,50);
		OrbitaArm.AddWithLevelControl(WeaponPattern.E_EliteRapid2);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_PurpleMissile);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_Pink_Electricball,11,999);
		OrbitaArm.AddWithLevelControl(WeaponPattern.E_Beam,11,999);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_FastHoming,11,999);
		OrbitaArm.AddWithLevelControl(WeaponPattern.E_EliteRedStriker,51,999);
		OrbitaArm.AddWithLevelControl(WeaponPattern.E_EliteBlueStriker,51,999);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_GreenLaserScatter,15,999);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_PurpleScatter,15,999);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl1);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl2);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl3);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl4);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl5);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang1);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang2);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang3);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang4);
		OrbitaArm.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang5);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_Engage_Y);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_Engage_B);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_Engage_R);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_Engage_G);
		OrbitaArm.AddWithLevelControl(WeaponPattern.B_Engage_P);
		OrbitaArm.MullTo(1);


		ArbiterCore.availableWeapons = new List<Weapon>();
		ArbiterCore.AddWithLevelControl(WeaponPattern.Arbiter_GreenSupport);
		ArbiterCore.AddWithLevelControl(WeaponPattern.Arbiter_RoundSlow);
		ArbiterCore.AddWithLevelControl(WeaponPattern.Arbiter_StunSupport);
		ArbiterCore.AddWithLevelControl(WeaponPattern.Arbiter_ScatterPink);
		ArbiterCore.AddWithLevelControl(WeaponPattern.Arbiter_ScatterBlue);
		ArbiterCore.AddWithLevelControl(WeaponPattern.Arbiter_RapidSlow);
		ArbiterCore.AddWithLevelControl(WeaponPattern.Arbiter_RapidFast);
		ArbiterCore.AddWithLevelControl(WeaponPattern.B_RedScatterLaser);
		ArbiterCore.AddWithLevelControl(WeaponPattern.B_BlueScatterLaser);
		ArbiterCore.AddWithLevelControl(WeaponPattern.B_GreenScatterLaser);
		ArbiterCore.AddWithLevelControl(WeaponPattern.B_PinkScatterLaser);
		ArbiterCore.AddWithLevelControl(WeaponPattern.B_GreenLaser);
		ArbiterCore.AddWithLevelControl(WeaponPattern.B_RedLaser);
		ArbiterCore.AddWithLevelControl(WeaponPattern.B_BlueLaser);
		ArbiterCore.AddWithLevelControl(WeaponPattern.B_PinkLaser);
		ArbiterCore.MullTo(4);
		
		ArbiterTurret.availableWeapons = new List<Weapon>();
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_BlueChain,1,11);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_GreenScatter,1,11);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_BlueMissile,1,50);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_BlueScatter);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_ShockWave,1,50);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_Blue_Electricball);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_Homing,1,50);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_PurpleMissile,1,50);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_Pink_Electricball,11,999);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_FastHoming,1,50);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.E_Beam,11,999);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_PurpleMissile, 0, 999);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.E_EliteBlueStriker, 0, 999);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_SonarRed, 0, 999);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_SonarBlue, 0, 999);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl1);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl2);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl3);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl4);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl5);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang1);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang2);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang3);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang4);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang5);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_Engage_Y);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_Engage_B);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_Engage_R);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_Engage_G);
		ArbiterTurret.AddWithLevelControl(WeaponPattern.B_Engage_P);
		ArbiterTurret.MullTo(5);

		DreadnoughtCore_0.availableWeapons = new List<Weapon>();
		DreadnoughtCore_0.name = "Dreadnought";
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core1);
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core2);
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core3);
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core4);
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core5);
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core6);
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core7);
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core8);
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core9);
		DreadnoughtCore_0.AddWithLevelControl(WeaponPattern.Dreadnought_Core10);
		DreadnoughtCore_0.MullTo(1);
		
		DreadnoughtTurret_0.availableWeapons = new List<Weapon>();
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret1);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret2);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret3);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret4);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret5);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret6);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret7);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret8);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret9);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret10);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret11);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret12);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret13);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret14);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret15);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret16);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret17);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret18);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret19);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret20);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang1);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang2);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang3);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang4);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang5);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl1);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl2);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl3);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl4);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl5);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.B_Engage_Y);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.B_Engage_B);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.B_Engage_R);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.B_Engage_G);
		DreadnoughtTurret_0.AddWithLevelControl(WeaponPattern.B_Engage_P);
		DreadnoughtTurret_0.MullTo(8);

		DreadnoughtCore_1.availableWeapons = new List<Weapon>();
		DreadnoughtCore_1.name = "Dreadnought";
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core11);
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core12);
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core13);
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core14);
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core15);
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core16);
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core17);
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core18);
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core19);
		DreadnoughtCore_1.AddWithLevelControl(WeaponPattern.Dreadnought_Core20);
		DreadnoughtCore_1.MullTo(1);
		
		DreadnoughtTurret_1.availableWeapons = new List<Weapon>();
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret21);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret22);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret23);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret24);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret25);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret26);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret27);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret28);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret29);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret30);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret31);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret32);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret33);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret34);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret35);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret36);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret37);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret38);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret39);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret40);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang1);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang2);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang3);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang4);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang5);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl1);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl2);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl3);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl4);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl5);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.B_Engage_Y);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.B_Engage_B);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.B_Engage_R);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.B_Engage_G);
		DreadnoughtTurret_1.AddWithLevelControl(WeaponPattern.B_Engage_P);
		DreadnoughtTurret_1.MullTo(8);

		DreadnoughtCore_2.availableWeapons = new List<Weapon>();
		DreadnoughtCore_2.name = "Dreadnought";
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core21);
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core22);
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core23);
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core24);
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core25);
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core26);
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core27);
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core28);
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core29);
		DreadnoughtCore_2.AddWithLevelControl(WeaponPattern.Dreadnought_Core30);
		DreadnoughtCore_2.MullTo(1);
		
		DreadnoughtTurret_2.availableWeapons = new List<Weapon>();
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret41);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret42);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret43);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret44);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret45);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret46);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret47);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret48);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret49);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret50);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret51);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret52);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret53);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret54);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret55);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret56);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret57);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret58);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret59);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret60);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang1);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang2);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang3);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang4);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang5);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang6);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang7);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang8);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang9);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang10);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl6);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl7);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl8);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl9);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl10);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.B_Engage_Y2);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.B_Engage_B2);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.B_Engage_R2);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.B_Engage_G2);
		DreadnoughtTurret_2.AddWithLevelControl(WeaponPattern.B_Engage_P2);

		DreadnoughtTurret_2.MullTo(9);


		DreadnoughtCore_3.availableWeapons = new List<Weapon>();
		DreadnoughtCore_3.name = "Dreadnought";
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core31);
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core32);
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core33);
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core34);
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core35);
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core36);
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core37);
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core38);
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core39);
		DreadnoughtCore_3.AddWithLevelControl(WeaponPattern.Dreadnought_Core40);
		DreadnoughtCore_3.MullTo(1);
		
		DreadnoughtTurret_3.availableWeapons = new List<Weapon>();
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret61);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret62);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret63);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret64);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret65);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret66);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret67);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret68);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret69);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret70);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret71);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret72);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret73);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret74);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret75);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret76);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret77);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret78);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret79);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret80);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang1);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang2);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang3);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang4);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang5);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang6);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang7);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang8);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang9);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Boomerang10);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl6);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl7);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl8);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl9);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.Dreadnought_Turret_Swirl10);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.B_Engage_Y2);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.B_Engage_B2);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.B_Engage_R2);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.B_Engage_G2);
		DreadnoughtTurret_3.AddWithLevelControl(WeaponPattern.B_Engage_P2);
		DreadnoughtTurret_3.MullTo(10);

//		Support.availableWeapons = new List<Weapon>();
//		Support.AddWithLevelControl(WeaponPattern.E_Support,0,15);
//		Support.AddWithLevelControl(WeaponPattern.E_Slow);
//		Support.AddWithLevelControl(WeaponPattern.B_BlueStun,5,999);
//		Support.AddWithLevelControl(WeaponPattern.E_BH_Scatter);
//		Support.AddWithLevelControl(WeaponPattern.E_Rapid);
//		Support.AddWithLevelControl(WeaponPattern.B_SlowShockWave);
//		Support.MullTo(2);
//
//		Bullet.availableWeapons = new List<Weapon>();
//		Bullet.AddWithLevelControl(WeaponPattern.E_EliteRapid);
//		Bullet.AddWithLevelControl(WeaponPattern.E_EliteRapid2);
//		Bullet.AddWithLevelControl(WeaponPattern.E_BH_Slow);
//		//Bullet.availableWeapons.Add(WeaponPattern.E_Sweep);
//		Bullet.AddWithLevelControl(WeaponPattern.B_Green_Electricball);
//		Bullet.AddWithLevelControl(WeaponPattern.B_Red_ElectricBall);
//		Bullet.MullTo(2);

//		Scatter.availableWeapons = new List<Weapon>();
//		Scatter.AddWithLevelControl(WeaponPattern.E_Spread);
////		Scatter.AddWithLevelControl(WeaponPattern.E_BH_Scatter);
//		Scatter.AddWithLevelControl(WeaponPattern.B_Scatter);
//		Scatter.AddWithLevelControl(WeaponPattern.E_Scatter);
//		Scatter.AddWithLevelControl(WeaponPattern.B_GreenLaserScatter);
//		Scatter.AddWithLevelControl(WeaponPattern.B_GreenScatter);
//		Scatter.AddWithLevelControl(WeaponPattern.B_PurpleScatter);
//		Scatter.AddWithLevelControl(WeaponPattern.B_RedScatter);
//		Scatter.MullTo(3);
//
//		Disable.availableWeapons = new List<Weapon>();
//		Disable.AddWithLevelControl(WeaponPattern.B_BlueChain);
//		Disable.AddWithLevelControl(WeaponPattern.B_BlueMissile);
//		Disable.AddWithLevelControl(WeaponPattern.B_BlueScatter);
//		Disable.AddWithLevelControl(WeaponPattern.B_ShockWave);
//		Disable.AddWithLevelControl(WeaponPattern.B_Blue_Electricball);
//		Disable.MullTo(2);

//		NUKE.availableWeapons = new List<Weapon>();
//		NUKE.AddWithLevelControl(WeaponPattern.E_Beam);
//		NUKE.AddWithLevelControl(WeaponPattern.B_Homing);
//		NUKE.AddWithLevelControl(WeaponPattern.B_PurpleMissile);
//		NUKE.AddWithLevelControl(WeaponPattern.B_Pink_Electricball);
//		NUKE.AddWithLevelControl(WeaponPattern.E_StrikerMissiles);
//		NUKE.AddWithLevelControl(WeaponPattern.E_EliteRedStriker);
//		NUKE.MullTo(1);

			
//		STANDARD.availableWeapons = new List<Weapon>();
//		STANDARD.AddWithLevelControl(WeaponPattern.E_Beam);
//		STANDARD.AddWithLevelControl(WeaponPattern.B_Chainer);
//		STANDARD.AddWithLevelControl(WeaponPattern.B_Green_Electricball, 0, 15);
//		STANDARD.AddWithLevelControl(WeaponPattern.B_SlowShockWave, 5);
//		STANDARD.AddWithLevelControl(WeaponPattern.E_EliteBlueLaser, 4);
//		STANDARD.AddWithLevelControl(WeaponPattern.M_BlueChain, 3);
//		STANDARD.AddWithLevelControl(WeaponPattern.E_BH_Slow);
//		STANDARD.MullTo(1);
//		
//		SPECIAL.availableWeapons = new List<Weapon>();
//		SPECIAL.AddWithLevelControl(WeaponPattern.B_Scatter, 0, 15);
//		SPECIAL.AddWithLevelControl(WeaponPattern.E_Sweep, 0, 15);
//		SPECIAL.AddWithLevelControl(WeaponPattern.E_EliteRapid);
//		SPECIAL.AddWithLevelControl(WeaponPattern.E_StrikerMissiles);
//		SPECIAL.AddWithLevelControl(WeaponPattern.B_Homing);
//		SPECIAL.AddWithLevelControl(WeaponPattern.E_EliteRapid2);
//	//	SPECIAL.availableWeapons.Add(WeaponPattern.B_ShockWave);
//		SPECIAL.AddWithLevelControl(WeaponPattern.B_Pink_Electricball);
//		SPECIAL.MullTo(2);

	


//		EXTREME.availableWeapons = new List<Weapon>();
////		EXTREME.availableWeapons.Add(WeaponPattern.M_BlackHole);
//		EXTREME.AddWithLevelControl(WeaponPattern.E_Beam);
//		EXTREME.AddWithLevelControl(WeaponPattern.E_FastHoming);
//		EXTREME.AddWithLevelControl(WeaponPattern.E_EliteRedStriker);
//		EXTREME.AddWithLevelControl(WeaponPattern.E_EliteBlueStriker);
//		EXTREME.MullTo(3);
			
	}
		
	public void MullTo (int amount = 1 )
	{
		int sectorlevel =  ProfileController.SectorId;
		weapons = new List<Weapon>();

		for( int i = availableWeapons.Count - 1 ; i >= 0; i-- )
		{
			if(availableWeapons[i].minLevelBossUse > sectorlevel || availableWeapons[i].maxLevelBossUse < sectorlevel )
			{
				availableWeapons.RemoveAt(i);
			}
		}

//		Debug.Log (name + " MULLING FROM: " + availableWeapons.Count);
			
		while (weapons.Count < amount)
		{
			// Copy a random selection of Weapons for this round.
			if (availableWeapons.Count == 0) break;
			
			int randomIndex = KJDice.RandomIndexOf(availableWeapons);

//			float log =  10.0f -( Mathf.Log((float)sectorlevel) *logFactor ); 
//			Debug.Log ("Log Factor: " + log);
//			selectedweapons.Cooldown *=  log ;
//			Debug.Log ("Weapon Cooldown: " + selectedweapons.Cooldown);

			Weapon selectedweapons = availableWeapons[randomIndex];
			weapons.Add(selectedweapons);
	  		availableWeapons.RemoveAt(randomIndex);
			
		}
		
		//Debug.Log (name + " MULLING TO: " + weapons.Count);
	}	
}

