using UnityEngine;
using System.Collections;

public class SoftSpotPattern
{

	public int radius = 30;
	public float direction = 0;
	public float distance = 0;
	public float lifeFactor = 1;
	public bool isManual = false;
	public bool isCycling = false;
	public bool cycleWithWeapon = false;
	public BossWeaponPattern bossWeaponPattern;
	public bool reweaponPool = false;
}

