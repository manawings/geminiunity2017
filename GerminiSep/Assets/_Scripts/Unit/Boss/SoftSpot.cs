using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoftSpot : Entity
{
	// Physics
	// float x = 0, y = 0;
	private int radius = 30;
	private int radiusSq = 30;
		
	// Vulnerability
	public bool isVulnerable = true;
	public bool isManual = false; // Cycles Manually from the Boss.
	public bool isCycling = false; // Cycles Manually from the Boss.
	public bool cycleWithWeapon = true;
	float flashColorState = 0.0f;
		
	// Cycle Gaps
	public float cycleGapWeapon = 1.0f;// Gap between turning on a soft spot and activating the weapon.
	public float cycleGapSwitch = 5.0f;// Switch every X seconds.
	public float cycleGapPause = 3.0f;// Downtime in seconds between cycling.

	public float softSpotDuration  = 3.0f;  //Time that softspot vulnerable for
		
	// Destructable
	private bool isDestroyed = false;
	private float life = -1.0f; // Set Life to be over 1 to be destructable.
		
	// Position Info.
	private float distance = 100.0f;
	private float direction = KJMath.RIGHT_ANGLE;
	private float proxyDirection = 0;
		
	// Soft Spots Can Also have a Turret.
	private Turret turret;	
	private float projectedX = 0, projectedY = 0;
	
	public void Deploy (float life = -1, int radius = 30, BossWeaponPattern bossWeaponPattern = null) 
	{
		this.radius = radius;
		radiusSq = radius * radius;

		// Set up the turret.
		if (bossWeaponPattern != null) {
			turret = gameObject.GetComponent<Turret>();
			turret.Deploy(bossWeaponPattern, this);
		}
			
		// Set the life to be a factor of the boss' Health.
		this.life = life;


	}
	

	public void SetPosition ( float distance = 0, float direction = 0 )
	{
		this.distance = distance;
		this.direction = direction;
	}
	
	
	public void GetProjectedDistance ( KJSpriteBehavior parentSprite, float directionToPlayer )
	{
		proxyDirection = direction - parentSprite.RotationByRadians;
		RotationByRadians = parentSprite.RotationByRadians;

		projectedX = parentSprite.transform.position.x + Mathf.Sin( proxyDirection ) * distance;
		projectedY = parentSprite.transform.position.y - Mathf.Cos( proxyDirection ) * distance;
			
		x = projectedX;
		y = projectedY;
		
		transform.position = new Vector3(x, y, 30.0f);
		
		if (turret != null) turret.AlignPosition(this);
	}

	public bool IsColliding ( GameObject withObject, int hitRadiusSq )
	{
		float distanceSq = KJMath.DistanceSquared2d(withObject, gameObject);
			
		if (distanceSq < radiusSq + hitRadiusSq)
		{
			return true;
		} 
			
		return false;
	}
	
	public override void TakeDamage (float damage, Entity.DamageType damageType = Entity.DamageType.Normal, bool isCriticalDamage = false, bool instantDeath = false, bool ignoreArmor = false)
	{
		// Override Damage Function Here.
		
		// Lose Life on the Soft Spot
		//if (life > 0)
		//{
		// float proxyDamage = damage;
			

			
			//if (proxyDamage > life) proxyDamage = life;
			//life -= proxyDamage;
			//if (life <= 0) KillMe();
			
		//}	

		if (damage > 0) {
			
			CombatPopup combatPopup = KJActivePool.GetNewOf<CombatPopup>("Popup");
			CombatPopup.Type popupType = CombatPopup.Type.Yellow;
			combatPopup.Deploy(transform.position + Random.insideUnitSphere * 25.0f, damage.ToString("F0"), popupType);
		}
	
		
		ChangeColorToHit();
		GameController.CurrentSectorBoss.TakeDamage(damage);
	}
	
	void ChangeColorToHit ()
	{
		flashColorState = 1.0f;
		sprite.color = flashColor;
		AddTimer(ChangeToNormalColor);
	}
	
	void ChangeToNormalColor ()
	{
		if (flashColorState == 0.0f)
		{
			RemoveTimerOrFlash(ChangeToNormalColor);
		} else {
			if (flashColorState < 0.01f)
			{
				sprite.color = Color.black;
				flashColorState = 0.0f;
			} else {
				flashColorState *= 0.80f;
				sprite.color = Color.Lerp(Color.black, flashColor, flashColorState);	
			}
		}
	}
	
	public void SwitchVulnerable ()
	{
		if (isVulnerable) {
			PermaVulnerableOff();
		} else {
			PermaVulnerableOn();
		}
	}
		
//	public void SwitchVulnerableOn (bool continueCycle = true)
//	{
//		// Turn ON weapon and vulnerability
//		isVulnerable = true;
//		if (turret != null) AddTimer (turret.CycleWeapon, cycleGapWeapon, 1);
//
//		// Add Cycle for next
//		if (continueCycle) {
//			AddTimer (CycleVulnerableOff, 3.0f, 1);
//		}
//	}

	public void PermaVulnerableOn ()
	{
		// Turn ON weapon and vulnerability
		isVulnerable = true;
		sprite.SetSprite("BossGem");
		if (turret != null) AddTimer (turret.CycleWeapon, cycleGapWeapon, 1);
	}
	
	public void CycleVulnerableOn ()
	{
		// Turn ON weapon and vulnerability
		isVulnerable = true;
		sprite.SetSprite("BossGem");
		if (turret != null) AddTimer (turret.CycleWeapon, cycleGapWeapon, 1);
		AddTimer (CycleVulnerableOff, softSpotDuration, 1);

	}

	public void PermaVulnerableOff ()
	{
		isVulnerable = false;
		sprite.SetSprite("BossGemClosed");
		if (turret != null) turret.FreezeWeapons();
	}

	public void CycleVulnerableOff ()
	{
		isVulnerable = false;
		sprite.SetSprite("BossGemClosed");
		if (turret != null) turret.FreezeWeapons();
		
		// Add Cycle for next
		if (isManual) {
			if (GameController.CurrentSectorBoss != null) GameController.CurrentSectorBoss.OnSoftSpotFinish();
		} else {
			if (cycleWithWeapon) AddTimer ( CycleVulnerableOn, cycleGapPause, 1 );
		}
	}
		
//	public virtual void SwitchVulnerableOff (bool continueCycle)
//	{
//		// Turn OFF this soft Spot.
//		// Debug.Log ("SoftSpot: SwitchVulnerableOff (). Continue Cycle: " + continueCycle);
//
//		// Turn OFF weapon and vulnerability
//		isVulnerable = false;
//		if (turret != null) turret.FreezeWeapons();
//			
//		// Add Cycle for next
//		if (continueCycle)
//		{
//			if (isManual) {
//				if (GameController.CurrentSectorBoss != null) GameController.CurrentSectorBoss.OnSoftSpotFinish();
//			} else {
//				if (cycleWithWeapon) AddTimer ( SwitchVulnerableOn, cycleGapPause, 1 );
//			}
//		}
//	}
	
	public void KillMe ()
	{
		
		// if (isActive) {
			//  if (isManual) {
				// if (GameController.currentBoss) GameController.currentBoss.removeSoftSpotFromCycle(this);
			// }
		GameController.CurrentSectorBoss.RemoveSoftSpot(this);
				
		isDestroyed = true;
		isVulnerable = false;
		
		EffectController.StandardExplosionAt(transform.position);

		Deactivate();
		//Destroy(gameObject);

		
	}
	
	public void FreezeAllWeapons ()
	{
		if (turret != null) turret.FreezeWeapons();
	}
		
	public void StartWeapons ()
	{
		if (!isManual) {
			if (!cycleWithWeapon && isCycling) {
				AddTimer(SwitchVulnerable, cycleGapSwitch);
			} 
			PermaVulnerableOn();
		}
	}
	
//		}
//	
//		
//		override public function onBurstFinish():void
//		{
//			// Switch the Vulnerabilty back on.
//			
//			turret.freezeWeapons();
//			
//			if (isCycling) {
//				KJTime.scheduleTimerUpdate(switchVulnerableOff, cycleGapWeapon, 1);
//			} else {
//				KJTime.scheduleTimerUpdate(turret.cycleWeapon, cycleGapWeapon * 1.5, 1);
//			}
//		}
//		
//		private function removeTimers():void
//		{
//			KJTime.removeTimerUpdate(switchVulnerable);
//			KJTime.removeTimerUpdate(switchVulnerableOn);
//			KJTime.removeTimerUpdate(switchVulnerableOff);
//			if (turret) KJTime.removeTimerUpdate(turret.cycleWeapon);
//		}
	
	
}

