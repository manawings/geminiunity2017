using UnityEngine;
using System.Collections;

public class TurretPattern
{

	public float distance;
	public float direction;
	public BossWeaponPattern bossWeaponPattern;
		
	public TurretPattern (float distance = 0.0f, float direction = 0.0f, BossWeaponPattern bossWeaponPattern = null) 
	{
		this.distance = distance;
		this.direction = direction;
		this.bossWeaponPattern = bossWeaponPattern;
	}
}

