using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SectorBoss : KJSpriteBehavior {
	
	// Pre-fabs
	public GameObject bossSoftSpotPrefab;
	public GameObject bossTurretPrefab;
	
	// Movement and Velocity
		
	float x = 0, y = 0;
	protected float maxSpeed = 1.50f;
	protected float currentSpeed = 0.0f;
	protected float acceleration = 0.02f;
	protected float decceleration = 0.02f;
		
	private float stopReach = 10.0f; // At which range to start decceleration.
		
	private Vector3 targetPoint = new Vector3();
	private float moveDirection;
	private float distanceToTargetSq;
		
	// Used to trigger boss activation sequence.
	private bool firstEntry = true;
		
	// Turning
		
	private float maxTurn = 0.005f;
	private float currentTurn = 0.0f;
	private float turnAcceleration = 0.0002f;
	private float turnDecceleration = 0.0002f;
	private int turnDir = 1;
	private float turnRange = 0.20f;
	private bool isTurning = true;
	private bool isRetreating = false;


		
	private float direction = Mathf.PI;
	private float targetDirection = Mathf.PI;
		
	// Temp: Weapons
	private Turret turret;
	private List<Turret> turrets = new List<Turret>();
		
	// Temp: Soft Spot
	public List<SoftSpot> softSpots = new List<SoftSpot>();
		
	private SoftSpot softSpot;
		
	// Soft Spot Cycling
	public List<SoftSpot> cycleSoftSpots =  new List<SoftSpot>();
	private int currentSoftSpotIndex = 0;
		
	// Cycle Gaps - Preset From The Boss Pattern.
	private float cycleGapChain;
	private float cycleGapWeapon;
	private float cycleGapSwitch;
	private float cycleGapPause;
	private float softSpotDuration;
	
	public float directionToPlayer;
	
	// Stats
	public Stats stats;
	BossPattern.MoveMode moveMode;
	private float lifeLimit = 0.0f;// If not zero, it will stop here and 'die' here.
	private bool willRetreat = false;
	
	
	public void Deploy ( BossPattern fromBossPattern = null )
	{

//		Debug.Log("Deploy "+ fromBossPattern.name  +"   " + fromBossPattern.softSpotPatterns.Count);

		// Set the Reference
		gameObject.SetActive(true);	
		GameController.CurrentSectorBoss = this;
		sprite.SetSprite(fromBossPattern.bossImageName);
			
		// Set Scaling Stats
		lifeLimit = 0.0f;
		isRetreating = false;
		stats = new Stats(10.0f, 10.0f, 10.0f, 10.0f);
		float statFactor = Stats.NGVForLevel(WaveController.Level);
		stats.life = 220.0f + fromBossPattern.lifeFactor * 320.0f * Stats.EnemyDifficultyFactor(0.25f, WaveController.Level) * statFactor;
        print(ProfileController.SectorId);
        if (ProfileController.SectorId == 1 || ProfileController.SectorId == 2) stats.life *= 0.5f;
		stats.damage = 15.0f + fromBossPattern.damageFactor * 1.85f * Stats.EnemyDifficultyFactor(0.5f, WaveController.Level) * statFactor;
		stats.armor =  1.0f + fromBossPattern.armorFactor * 1.00f * statFactor;
		if (ProfileController.secretRaid) stats.life *= ProfileController.RaidProgressMax;
		stats.Calibrate();

		if (ProfileController.secretRaid) {

			float lifeChunk = stats.lifePointsMax * (1.0f / (float) ProfileController.RaidProgressMax); // Life Points per Level.

			float lifePercent = (1.0f - ((float) ProfileController.RaidProgress / (float) ProfileController.RaidProgressMax));
			stats.lifePoints = stats.lifePointsMax * lifePercent;

			if (ProfileController.RaidProgressMax - ProfileController.RaidProgress > 1) {
				lifeLimit = stats.lifePoints - lifeChunk;
				willRetreat = true;
			}


//			Debug.Log ("Life Percent: " + lifePercent);
//			Debug.Log ("Life Max: " + stats.lifePointsMax);
//			Debug.Log ("Life Now: " + stats.lifePoints);

		}
			
		// Calibrate the Cycle Gaps.
		cycleGapChain = fromBossPattern.cycleGapChain;
		cycleGapWeapon = fromBossPattern.cycleGapWeapon;
		cycleGapSwitch = fromBossPattern.cycleGapSwitch;
		cycleGapPause = fromBossPattern.cycleGapPause;
		softSpotDuration = fromBossPattern.softSpotDuration;

		float modFactor = (1.0f - BossModValue);
		cycleGapPause *= modFactor;
		cycleGapWeapon *= modFactor;
		cycleGapSwitch *= modFactor;
		cycleGapChain *= modFactor;
		softSpotDuration *= modFactor;

		moveMode = fromBossPattern.moveMode;
			
		// Reset the Weapon Data (To randomize weapon pools)
		BossWeaponPattern.Reset();
	
		// Set up the Soft Spots
		for (int i = 0 ; i < fromBossPattern.softSpotPatterns.Count ; i++)
		{
			AddSoftSpotFromPattern(fromBossPattern.softSpotPatterns[i]);
		}

		// Set up the Weapon
		for (int i = 0 ; i < fromBossPattern.turretPatterns.Count ; i++)
		{
			AddTurretFromPattern(fromBossPattern.turretPatterns[i]);
		}
		
		// Reset Position
		x = 0.0f;
		y = 500.0f;
			
		CalculateStopReach();
		ChangeDirection();
		// FreezeAllWeapons();
			

		firstEntry = true;



		if (moveMode == BossPattern.MoveMode.Normal || moveMode == BossPattern.MoveMode.Spin) {

			targetPoint.x = 0.0f;
			targetPoint.y = 180.0f;
			AddTimer(ChangeDirection);

		} else if (moveMode == BossPattern.MoveMode.Encircle) {
			tFactor = KJMath.CIRCLE * 0.75f;
			AddTimer (SpawnSubSquad, 5.25f);
			AddTimer ( StartWeapons, 2.0f, 1);

		}

		Run ();
		AddTimer(Run);

		UIController.UpdateBossBar(stats.LifePercent, stats.lifePoints.ToString("F0"));
		
	}

	int approachDir = 1;

	public void SpawnSubSquad ()
	{
//		if (y < 0 || x > 220 || x < -220) {

			WaveController.BurstSpawnBossSquad(approachDir);
			approachDir = approachDir == 1 ? -1 : 1;

//		}
	}

	public static float BossModValue
	{
		get {
			float levelFactor = // 0f - 1f
					((KJMath.Cap (WaveController.Level / 100.0f, 0.0f, 1.0f)) * 0.30f) +
					((KJMath.Cap (WaveController.Level / 25.0f, 0.0f, 1.0f)) * 0.70f);
			
			float valueFactor = 0.125f;

			return valueFactor * levelFactor;
		}
	}

	public void CalibrateWeapon (Weapon weapon) 
	{
		weapon.AdjustForBossFactor();
	}

	void RunDefaultMode ()
	{
		// Move unit to the target point.
		moveDirection = KJMath.DirectionFromPosition(transform.position, targetPoint);
		distanceToTargetSq = KJMath.DistanceSquared2d(transform.position, targetPoint);

		if (moveMode == BossPattern.MoveMode.Spin) {

			direction += 0.4f * DeltaTime;
			RotationByRadians = direction;

		} else {

			if (isTurning)
			{
				if (Mathf.Abs(targetDirection - direction) > 0.05f)
				{
					AccelerateTurn();
				} else {
					DeccelerateTurn();
				}
				
				direction += currentTurn * turnDir;
				RotationByRadians = direction;
			}

		}
		
		if (distanceToTargetSq > stopReach)
		{
			// Has reached its target.
			Accelerate();
		} else {
			
			// Needs to go to the target still.
			Deccelerate();
		}

		x += Mathf.Sin(moveDirection) * currentSpeed;
		y += Mathf.Cos(moveDirection) * currentSpeed;
	}

	float tFactor = 0.0f, tSpeed = 0.0f, tRange = 300;
	float tMaxSpeed = 0.9f, tMinSpeed = 0.08f;

	void RunEncircleMove ()
	{


		if (x > 240 || x < -240) {
			if (tSpeed != tMaxSpeed) tSpeed = KJMath.ConvergeToTarget(tSpeed, tMaxSpeed, 0.02f, 0.01f);
		} else {
			if (tSpeed != tMinSpeed) tSpeed = KJMath.ConvergeToTarget(tSpeed, tMinSpeed, 0.07f, 0.01f);
		}

		tFactor += tSpeed * DeltaTime;

		x = Mathf.Sin(tFactor) * tRange * 1.6f;
		y = Mathf.Cos(tFactor) * tRange * 0.6f;

		direction = tFactor + KJMath.RIGHT_ANGLE;
		RotationByRadians = direction;
	}

	void Run ()
	{

		if (moveMode == BossPattern.MoveMode.Encircle) {
			RunEncircleMove();
		} else {
			RunDefaultMode();
		}

		transform.position = new Vector3(x, y, 50.0f);
			
		
		// Position the slots and turrets.
		if (!isRetreating) {
		
			directionToPlayer = KJMath.DirectionFromPosition(transform.position, GameController.PlayerUnit.transform.position);
			
			foreach (SoftSpot softSpot in softSpots)
			{
				softSpot.GetProjectedDistance(this, directionToPlayer);
			}
				
			foreach (Turret turret in turrets)
			{
				turret.AlignPosition(this);
			}

			// Hard Code to prevent boss from leaving
			if (!firstEntry) {
				if (x > 250) x = 250;
				if (x < -250) x = -250;
				if (y > 280) y = 280;
			}

		}
	}
	
	void ChangeDirection ()
	{
		targetDirection = KJMath.TreatAngle(KJMath.DirectionFromPosition(transform.position, GameController.PlayerUnit.transform.position));
		targetDirection = KJMath.Cap(targetDirection, Mathf.PI - turnRange, Mathf.PI + turnRange);
		turnDir = targetDirection - direction > 0.0f ? 1 : -1;
		RotationByRadians = direction;
		isTurning = true;
	}
	
	void AccelerateTurn()
	{
		if (currentTurn < maxTurn)
		{
			currentTurn += turnAcceleration;
			if (currentTurn > maxTurn) currentTurn = maxTurn;
		}
	}
		
	void DeccelerateTurn()
	{
		if ( currentTurn > 0 )
		{
			currentTurn -= turnDecceleration;
			if ( currentTurn < 0 ) currentTurn = 0;

		} else {
			OnReachTargetTurn();
		}
	}
		
		
	void Accelerate()
	{
		if ( currentSpeed < maxSpeed )
		{
			currentSpeed += acceleration;
			if ( currentSpeed > maxSpeed ) currentSpeed = maxSpeed;
		}
	}
		
	void Deccelerate()
	{
		if ( currentSpeed > 0 )
		{
			currentSpeed -= decceleration;
			if ( currentSpeed < 0 ) currentSpeed = 0;

		} else {
			OnReachTargetPoint();
		}
	}
	
	void OnReachTargetPoint()
	{
		if (firstEntry) {
			firstEntry = false;
			StartWeapons();
		}
			
		targetPoint.x = Random.Range(-200, 200);
		targetPoint.y = Random.Range(120, 220);
	}
		
	void OnReachTargetTurn() 
	{
		isTurning = false;
	}
	
	void CalculateStopReach()
	{
		// Calculate the distance this unit will be able to travel if it begins deccelerating.
			
		float proxySpeed = maxSpeed;
		stopReach = 0;
			
		while (proxySpeed > 0)
		{
			proxySpeed -= decceleration;
			stopReach += proxySpeed;
		}
			
		stopReach = stopReach * stopReach; // Sq it for using with distance checks.
	}
	
	public void TakeDamage ( float damage, bool raw = false )
	{
		if (raw) damage *= stats.ArmorModifier;
		stats.lifePoints -= damage;

		if (lifeLimit == 0) {

			// Boss is defeated.
			if (stats.lifePoints == 0)
			{
				StartDying();
			}

		} else {

			// Raid Boss has been damaged to it's defeat point.
			if (stats.lifePoints < lifeLimit)
			{
				stats.lifePoints = lifeLimit;
				StartDying();
			}

		}

		
		UIController.UpdateBossBar(stats.LifePercent, stats.lifePoints.ToString("F0"));
	}
	
	public void StartDying ()
	{
		
		// Turn Off the boss and start exploding.
		
		
		UIController.HideEnemyBar();

		for (int i = softSpots.Count - 1 ; i > -1 ; i --)
		{
			softSpot = softSpots[i];
			softSpot.KillMe();
		}
		
		
		for (int i = 0 ; i < turrets.Count ; i ++)
		{
			turret = turrets[i];
			turret.Terminate();
		}

		GameController.CurrentSectorBoss = null;
		

		ProfileController.isFirstWinBoss = true;


		AddTimer (KillMe, 3, 1);

		if (willRetreat) {
			targetPoint = new Vector3(0.0f, 500.0f, targetPoint.z);
			isRetreating = true;
			Flash (5);
			AddTimer (ExplodeBurst, 0.15f, 7);
		} else {
			Flash (30);
			AddTimer (ExplodeBurst, 0.15f, 25);
		}
			

	}

	public List<SoftSpot> VulnerableSoftSpots
	{

		get {

			List<SoftSpot> returnList = new List<SoftSpot>();

			for (int i = softSpots.Count - 1 ; i > -1 ; i --)
			{
				softSpot = softSpots[i];
				if (softSpot.isVulnerable) returnList.Add(softSpot);
			}

			return returnList;
		}
	}
	
	private void ExplodeBurst () 
	{
		float explodeX = x + Random.Range(-150, 150);
		float explodeY = y + Random.Range(-80, 80);
		
		Vector3 explosionPosition = new Vector3(explodeX, explodeY, 0.0f);
		EffectController.StandardExplosionAt(explosionPosition);

		GameController.Singleton.BigShakeScreen();

		SpwanGem(explodeX, explodeY);
	}
	
	public void KillMe ()
	{
		KJTime.Add(GameController.WinGame, 3.0f, 1);
		Deactivate();
	}
	
	void AddTurretFromPattern(TurretPattern turretPattern)
	{
		// Create the Turret from the Pattern.


		GameObject newGameObject = (GameObject) Instantiate(bossTurretPrefab);
		turret = newGameObject.GetComponent<Turret>();
		turret.Deploy(turretPattern.bossWeaponPattern);

		turret.positionDirection = turretPattern.direction;
		turret.positionDistance = turretPattern.distance;
		turrets.Add(turret);

		turret.FreezeWeapons();
			
	}
	
	void AddSoftSpotFromPattern ( SoftSpotPattern softSpotPattern )
	{

		if(softSpotPattern.reweaponPool)
		{
			BossWeaponPattern.Reset();
		}
	//	Debug.Log ("bossWeaponPattern Count: " +softSpotPattern.bossWeaponPattern.name);
	//	Debug.Log ("bossWeaponPattern Count: " +softSpotPattern.bossWeaponPattern.weapons.Count);

		// Create the Soft Spot From the Pattern.
		GameObject newGameObject = (GameObject) Instantiate(bossSoftSpotPrefab);
		softSpot = newGameObject.GetComponent<SoftSpot>();
		softSpot.Deploy(10.0f, 30, softSpotPattern.bossWeaponPattern);
		softSpot.SetPosition(softSpotPattern.distance, softSpotPattern.direction);
		softSpot.cycleWithWeapon = softSpotPattern.cycleWithWeapon;
		softSpot.isCycling = softSpotPattern.isCycling;

	//	Debug.Log ("Soft Weapons Count: " + softSpotPattern.bossWeaponPattern.weapons.Count);

		// CalibrateDebu the Gaps
		softSpot.cycleGapPause = cycleGapPause;
		softSpot.cycleGapSwitch = cycleGapSwitch;
		softSpot.cycleGapWeapon = cycleGapWeapon;
		softSpot.softSpotDuration = softSpotDuration;


		// Add the Soft Spot to the Lists
		if (softSpotPattern.isManual) {
			softSpot.isManual = true;
			cycleSoftSpots.Add(softSpot);
			
		}

		softSpots.Add(softSpot);

		// Turn it off to start with
		softSpot.PermaVulnerableOff();
		
	}
	
	public bool CheckForHit (Projectile projectile, int hitRadiusSq, float damage)
	{
		for (int i = 0 ; i < softSpots.Count ; i ++)
		{
			softSpot = softSpots[i];
				
			if (softSpot.isVulnerable) {
				if (softSpot.IsColliding(projectile.gameObject, hitRadiusSq))
				{
					projectile.DealDamageToSoftSpot (softSpot);
					return true;
				}
			}
		}

		return false;
	}
	
	public void RemoveSoftSpot (SoftSpot softSpot)
	{
		softSpots.Remove(softSpot);
	}
	
	public void OnSoftSpotFinish ()
	{
		// Cycle to the next soft spot, after a small delay.
		// Debug.Log ("SectorBoss: OnSoftSpotFinish ()");
		AddTimer(CycleNextSoftSpot, cycleGapChain, 1);
	}
	
	private void CycleNextSoftSpot ()
	{
		// Activate the next Soft Spot in the chain.
		// Debug.Log ("SectorBoss: CycleNextSoftSpot ()");
			
		// Make sure we can pass.
		if (GameController.CurrentSectorBoss == null) return;
		if (cycleSoftSpots.Count == 0) return;

		// Turn on the next one.
		softSpot = cycleSoftSpots[currentSoftSpotIndex];
		softSpot.CycleVulnerableOn();
		currentSoftSpotIndex ++;
				
		if (currentSoftSpotIndex == cycleSoftSpots.Count) currentSoftSpotIndex = 0;
			
	}
	
	private void StartWeapons ()
	{

		// Start Independant Soft Spots
		for (int i = 0 ; i < softSpots.Count ; i ++)
		{
			softSpot = softSpots[i];
			softSpot.StartWeapons();
		}
			
		// Start Independant Turrets
		for (int i = 0 ; i < turrets.Count ; i ++)
		{
			turret = turrets[i];
			turret.CycleWeapon();
		}
			
		// Begin the Cycle
		CycleNextSoftSpot();
	}

	void SpwanGem (float newX, float newY) {
		GameObject collectableObject;
		Collectable collectable;
		
		int randomGem = 0;
//		if (ProfileController.SectorLevel < 5) { 
//			randomGem = Random.Range(3, 5);
//		} else {
//			randomGem = Random.Range(0, 3);
//			float maxR = 3.0f + Stats.FactorForLevel(ProfileController.SectorLevel) * 2.5f;
//			float minR = 1.0f + Stats.FactorForLevel(ProfileController.SectorLevel);
//			randomGem = (int)System.Math.Ceiling((ProfileController.SectorLevel + Random.Range(minR, maxR)) / Random.Range(2, 5)) + Random.Range(2, 5);
//		}
//		if (randomGem > 5) randomGem = 5;
//
//		randomGem += (int) System.Math.Ceiling((Stats.FactorForLevel(ProfileController.SectorLevel) * 40.5f) + Random.Range(15, 25));

		randomGem = 3;
		
		for (int i = 0; i < randomGem; i++ ) {
			collectable = KJActivePool.GetNewOf<Collectable>("Collectable");
			collectable.Deploy(Collectable.Type.Gem, newX + Random.Range (-20.0f , 20.0f), newY + Random.Range (-20.0f , 20.0f), false); 
		}
	}

	/*
	private void FreezeAllWeapons ()
	{

		for (int i = 0 ; i < softSpots.Count ; i ++)
		{
			softSpot = softSpots[i];
			softSpot.FreezeAllWeapons();
			softSpot.SwitchVulnerableOff(false);
		}
			
		for (int i = 0 ; i < turrets.Count ; i ++)
		{
			turret = turrets[i];
			turret.FreezeWeapons();
		}
	}
	*/
//		
//		
//		
//		public function removeSoftSpotFromCycle(softSpot:SoftSpot):void
//		{
//			// A Cycled Soft Spot has been destroyed. Remove it from the cycle so that the other may continue to cycle.
//			for (var i:int = 0 ; i < cycleSoftSpots.length ; i++)
//			{
//				if (cycleSoftSpots[i] == softSpot)
//				{
//					cycleSoftSpots.splice(i, 1);
//					if (currentSoftSpotIndex > 0) currentSoftSpotIndex--;
//				}
//			}
//			
//			onSoftSpotFinish();
//		}
//		
//		
//		
//		
//		public function deactivate():void
//		{
//			UIController.removeBossBar();
//			
//			removeFromParent();
//			KJTime.removeUpdate(run);
//			KJTime.removeUpdate(changeDirection, true);
//			KJTime.removeTimerUpdate(explodeBurst);
//			KJTime.removeTimerUpdate(killMe);
//			KJTime.removeTimerUpdate(cycleNextSoftSpot);
//			
//			// TODO: Deactivate Weapons and hitboxes too.
//			
//			for (var i:int = 0 ; i < softSpots.length ; i ++)
//			{
//				softSpot = softSpots[i];
//				softSpot.deactivate();
//			}
//			
//			for (i = 0 ; i < turrets.length ; i ++)
//			{
//				turret = turrets[i];
//				turret.deactivate();
//			}
//		}
	
}
