using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerUnit : Unit
{

	// Reactor System
	Reactor reactor;
	Collectable.Type currentReactorType;
	Dictionary<Collectable.Type, Reactor> reactors = new Dictionary<Collectable.Type, Reactor>();
	bool hasPowerUp;
	float currentCameraRot = 0.0f;
	protected override void Run ()
	{
		RunTargetedMovement(0.0f);
		if (isAlly) SetAIPosition();
		base.Run();
		
		if (isMainPlayer) {

			Vector3 newCamPos = Vector3.Lerp(new Vector3(KJCamera.Singleton.cameraPosition.x, KJCamera.Singleton.cameraPosition.y, 0.0f), new Vector3(x * GameController.Singleton.cameraRatio, KJCamera.Singleton.cameraPosition.y, 0.0f), 0.15f);
			KJCamera.Singleton.cameraPosition.x = newCamPos.x;
			KJCamera.Singleton.cameraPosition.y = newCamPos.y;
		}

	}
	
	void RunMulti ()
	{
		NetworkController.Singleton.SendPosition(targetPoint.x, targetPoint.y);	
	}
	
	
	bool isMainPlayer = true;
	bool isAlly = false;

	public void Revive (int itemId)
	{
		isActive = true;
		OnActivate();
		gameObject.SetActive(true);

		foreach( Weapon eWeapon in gameObject.GetComponents<Weapon>())
		{
			eWeapon.Terminate();
			DestroyImmediate(eWeapon);
		}

		foreach( Reactor eReactor in gameObject.GetComponents<Reactor>())
		{
			DestroyImmediate(eReactor);
		}

		Initialize(itemId, true);

		transform.position = new Vector3(0.0f, -300.0f, 0.0f);
		x = transform.position.x;
		y = transform.position.y;

		maxVelocity = 240.0f;

		SetTargetPoint( new Vector3(0.0f, -150.0f, 0.0f) );

		ApplyShield(10.0f, 10.0f);
		isControlLocked = true;
		KJTime.Add(OnRevieComplete, 1.5f, 1);

	}



	public void OnRevieComplete ()
	{
		maxVelocity = 1500.0f;
		isControlLocked = false;
	}

	public void Initialize (int itemId, bool isMainPlayer = true) 
	{
		
		
		base.Deploy();
		this.isMainPlayer = isMainPlayer;
		
		isMoving = false;

		SetTargetedMoveValues();
		// Calculate the Cut-Off areas.
		float padding = 10.0f;
			
		team = Team.Friendly;
		ItemShip item = ItemPattern.ShipList[itemId];
		weapon.CopyDataFrom(item.weapon);
		weapon.Deploy(this);
		
		stats = new Stats();
		ProfileController.CalibrateStats();
		stats.Add(ProfileController.Singleton.finalStats);
		if (ProfileController.Singleton.isFightingRival) stats.life *= 30.0f;
		stats.Calibrate();




		boosterEmitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		boosterEmitter.CopyFromModel(EffectController.Singleton.shipBooster);
		boosterEmitter.SetColor(item.boosterColorStart, item.boosterColorEnd);
		boosterEmitter.SetObjectLink(boosterObject);
		boosterEmitter.Deploy();
		
		boosterEmitter2 = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		boosterEmitter2.CopyFromModel(EffectController.Singleton.shipBoosterStay);
		boosterEmitter2.SetColor(item.boosterColorStart, item.boosterColorEnd);
		boosterEmitter2.SetObjectLink(boosterObject);
		boosterEmitter2.SetIsContained(true);
		boosterEmitter2.Deploy();





		AddTimer(Run);
		if (NetworkController.Singleton.isMultiplayer && isMainPlayer) AddTimer(RunMulti, 0.15f);
		
		animator.Play(item.shipClip);
		

		
		hasPowerUp = false;
		
		// TEMP
		
		if (reactor == null)
		{
			reactor = gameObject.AddComponent<Reactor>();
		}

		reactors.Clear();
		
		reactors.Add(Collectable.Type.Green, gameObject.AddComponent<Reactor>());
		reactors.Add(Collectable.Type.Blue, gameObject.AddComponent<Reactor>());
		reactors.Add(Collectable.Type.Yellow, gameObject.AddComponent<Reactor>());
		reactors.Add(Collectable.Type.Pink, gameObject.AddComponent<Reactor>());
		
		reactors[Collectable.Type.Green].Deploy(this, Collectable.Type.Green);
		reactors[Collectable.Type.Yellow].Deploy(this, Collectable.Type.Yellow);
		reactors[Collectable.Type.Pink].Deploy(this, Collectable.Type.Pink);
		reactors[Collectable.Type.Blue].Deploy(this, Collectable.Type.Blue);
		
		
		modules.Clear();
		
		// Add The Modules based on Equipment.
		
		foreach (ItemBasic equipItem in ProfileController.Singleton.equipList)
		{
			if (equipItem.module != null) {
				// Module Exists
				AddModule(equipItem.moduleId, equipItem.EffectiveLevel, equipItem.FinalModulePower);
			}
		}

		if (item.moduleId != 0) {
			AddModule(item.moduleId, 1.0f, 1.0f);
		}
		
		transform.position = new Vector3(0.0f, -120.0f, 0.0f);
		x = transform.position.x;
		y = transform.position.y;
		// Debug.Log (stats.lifePoints);

//		GainReactor(Collectable.Type.Pink);
	}
	
	public void CollectGem ()
	{

		modules.OnCollectGem();
		
		CombatPopup combatPopup = KJActivePool.GetNewOf<CombatPopup>("Popup");
		CombatPopup.Type popupType = CombatPopup.Type.Blue;

		int gemGain = 1;
		if (ProfileController.GemDouble) gemGain = 2;
		ProfileController.GainRoundGem(gemGain);
		combatPopup.Deploy(transform.position + Random.insideUnitSphere * 25.0f, "+" + gemGain, popupType);
	}

	public void CollectCargo ()
	{
		ProfileController.hasCollectedCargo = true;
		
		CombatPopup combatPopup = KJActivePool.GetNewOf<CombatPopup>("Popup");
		CombatPopup.Type popupType = CombatPopup.Type.Pink;
		combatPopup.Deploy(transform.position + Random.insideUnitSphere * 25.0f, "GOT CARGO!", popupType);
	}
	
	public void FireReactor (int id = -1)
	{
		if (id == -1 && IsAlive) {
			
			if (hasPowerUp)
			{
				
				if (NetworkController.Singleton.isMultiplayer)
				{
					NetworkController.Singleton.SendReactor(Reactor.getIdForType(currentReactorType));	
				}

				hasPowerUp = false;
				reactor.Execute();	
				modules.OnReactorUse();

			}	
			
		} else {
			
			currentReactorType = Reactor.getTypeForId(id);
			reactor = reactors[currentReactorType];
			reactor.Execute();
		}
		
		
	}
	
	public override void GainReactor (Collectable.Type collectableType)
	{
		if (hasPowerUp == false) {
			
			// Switch the icon and slide it in as well.
			if (!ProfileController.Singleton.tutorialStatus[2] && !GameController.Singleton.hasGameEnded) KJTime.Add(ShowReactorTutorial, 0.5f, 1);
			UIController.ShowReactorButton(collectableType);
			
		} else {
			
			// Switch the Icon but do not slide it in.
			UIController.ShowReactorButton(collectableType, false);
		}
		
		currentReactorType = collectableType;
		reactor = reactors[currentReactorType];
		modules.OnCollectPowerUp();
		
		hasPowerUp = true;
	}

	void ShowReactorTutorial ()
	{
		TutorialData.Deploy(2);
	}
	
	public override void TakeHit (float damage = 25.0f, Entity.DamageType damageType = Entity.DamageType.Normal, bool isCrit = false, bool ignoreArmor = false)
	{
		sprite.color = new Color(1.2f, 0.6f, 0.2f);
		
		AddTimer (ChangeToNormalColor);

		if (buffAcid.IsActive) damage += buffAcid.power;
		TakeDamage(damage, damageType, isCrit, ignoreArmor);
	}

	public override void TakeDamage (float damage, Entity.DamageType damageType = Entity.DamageType.Normal, bool isCriticalDamage = false, bool instantDeath = false, bool ignoreArmor = false)
	{
		base.TakeDamage(damage, damageType, isCriticalDamage, instantDeath, ignoreArmor);

		if (isMainPlayer) {
			
			GameController.Singleton.UpdateLifeBar();
			if (damageType != Entity.DamageType.Burn) {
				GameController.Singleton.BigShakeScreen();
			}
			
		}
		Achievement.noTakeDamage = false;
	}
	
	protected override void RunPeriodic ()
	{
		
		base.RunPeriodic();
		
		if (isMainPlayer) {
			
			GameController.Singleton.UpdateLifeBar();
			
		}
	}
	
	void ChangeToNormalColor ()	
	{
		sprite.color = Color.Lerp(sprite.color, currentColor, 0.15f);
		
		if (sprite.color.r < 0.01f) { 
			sprite.color = currentColor;
			RemoveTimerOrFlash (ChangeToNormalColor); 
		}
	}
	
	public void ExternalDie ()
	{
		Explode();
	}

	public override void StopWeapons ()
	{
		if (IsActualPlayer)
		{
			InputController.Singleton.StopShooting();
		}
	}

	bool IsActualPlayer {
		get {
			return GameController.PlayerUnit == this;
		}
	}
	
	protected override void Explode ()
	{
		if (isMainPlayer) {
			NetworkController.Singleton.SendDie();
			KJTime.Add (GameController.OnDie, 2.0f, 1);
			UIController.HideReactorButton();
		}

		weapon.StopShooting();
		base.Explode();	
	}
	
	enum WeaponSlot {
		Left, Right
	}
	
	WeaponSlot currentWeaponSlot;
	float offsetAmount = 8.0f;
	
	public override Vector3 GetWeaponOffset ()
	{
		Vector3 newVector = transform.position;
		float useOffsetX = 0.0f;
		if (currentWeaponSlot == WeaponSlot.Left)
		{
			currentWeaponSlot = WeaponSlot.Right;
			useOffsetX = offsetAmount;
		} else {
			currentWeaponSlot = WeaponSlot.Left;
			useOffsetX = - offsetAmount;
		}
		
		return new Vector3(useOffsetX + newVector.x, newVector.y, newVector.z);
		
	}


	// AI FUNCTIONS

	public void DeployAsRival ()
	{
//
//		// this.isSocial = true;
//		
//		RivalData rival = ProfileController.Rival;
//		
//		// Set the stats.
//		stats.life = rival.stats.life * 30.0f;
//		stats.damage = rival.stats.damage * 0.70f;
//		stats.armor = rival.stats.armor * 0.65f;
//		
//		maxVelocity = maxSpeed = 850.0f;
//		
//		// Set the ship graphic.
//		ItemShip item = ItemPattern.ShipList[rival.shipId];
//		animator.Play(item.shipClip);
//		if (item.moduleId != 0) {
//			AddModule(item.moduleId, 1.0f, 1.0f);
//		}
//		
//		boosterEmitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
//		boosterEmitter.CopyFromModel(EffectController.Singleton.shipBooster);
//		boosterEmitter.SetColor(item.boosterColorStart, item.boosterColorEnd);
//		boosterEmitter.SetObjectLink(boosterObject);
//		boosterEmitter.SetDirection(0);
//		boosterEmitter.Deploy();
//		
//		boosterEmitter2 = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
//		boosterEmitter2.CopyFromModel(EffectController.Singleton.shipBoosterStay);
//		boosterEmitter2.SetColor(item.boosterColorStart, item.boosterColorEnd);
//		boosterEmitter2.SetObjectLink(boosterObject);
//		boosterEmitter2.SetIsContained(true);
//		boosterEmitter2.Deploy();
//		
//		// EnemyPattern newEnemyPattern = new EnemyPattern();
//		Deploy(new Vector2(0.0f, 300.0f), WeaponPattern.Standard);
//		
//		// Set the modules.
//		foreach (ModuleData moduleData in rival.moduleDataList)
//		{
//			AddModule(moduleData.moduleId, moduleData.moduleLevel, moduleData.modulePower);
//		}
//		
//		// UIController.UpdateBossBar(LifePercent, stats.lifePoints.ToString("F0"));
	
	}

	public void InitializeAsAlly () 
	{

		RivalData rival = ProfileController.Rival;

		base.Deploy();
		this.isMainPlayer = false;
		this.isAlly = true;
		isMoving = false;

		maxAcceleration = 20000.0f;
		maxVelocity = 1000.0f;
		SetTargetedMoveValues();
		 
		// Calculate the Cut-Off areas.
		float padding = 10.0f;
		team = Team.Friendly;
		ItemShip item = ItemPattern.ShipList[rival.shipId];
		weapon.CopyDataFrom(item.weapon);
		weapon.Deploy(this);
		
		stats = new Stats();

		float totalStats = rival.stats.life + rival.stats.damage + rival.stats.armor;

		float lifeDist = rival.stats.life / totalStats;
		float damageDist = rival.stats.damage / totalStats;
		float armorDist = rival.stats.armor / totalStats;

		float minNGV = Stats.NGVForLevel(ProfileController.SectorsUnlocked - 5);
		float maxNGV = Stats.NGVForLevel(ProfileController.SectorsUnlocked + 5);

		totalStats = KJMath.Cap (totalStats, minNGV, maxNGV);

		// Now we need to convert the stats to a scaled level.

		stats.life = lifeDist * totalStats * 3.5f;
		stats.damage = damageDist * totalStats * 0.7f;
		stats.armor = armorDist * totalStats;
		stats.Calibrate();

		// Debug.Log ("Ally Unit: " + stats.lifePoints + " LIFE. " + stats.damage + " DAMAGE. " + stats.armor + " ARMOR."); 
		
		AddTimer(Run);

		// Set the ship graphic.
		//ItemShip item = ItemPattern.ShipList[rival.shipId];
		animator.Play(item.shipClip);

		boosterEmitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		boosterEmitter.CopyFromModel(EffectController.Singleton.shipBooster);
		boosterEmitter.SetColor(item.boosterColorStart, item.boosterColorEnd);
		boosterEmitter.SetObjectLink(boosterObject);
		boosterEmitter.Deploy();
		
		boosterEmitter2 = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		boosterEmitter2.CopyFromModel(EffectController.Singleton.shipBoosterStay);
		boosterEmitter2.SetColor(item.boosterColorStart, item.boosterColorEnd);
		boosterEmitter2.SetObjectLink(boosterObject);
		boosterEmitter2.SetIsContained(true);
		boosterEmitter2.Deploy();
		
		hasPowerUp = false;
		
		if (reactor == null)
		{
			reactor = gameObject.AddComponent<Reactor>();
		}
		
		modules.Clear();
		
		// Add The Modules based on Equipment.

		foreach (ModuleData moduleData in rival.moduleDataList)
		{
			AddModule(moduleData.moduleId, moduleData.moduleLevel, moduleData.modulePower);
		}

		if (item.moduleId != 0) {
			
			AddModule(item.moduleId, 1.0f, 1.0f);
		}
		
		transform.position = new Vector3(0.0f, -420.0f, 0.0f);
		x = transform.position.x;
		y = transform.position.y;

	}

	Vector3 aiOffsetPosition, aiFinalPosition;

	public void StartAI ()
	{
		aiOffsetPosition = Vector3.zero;
		aiFinalPosition = Vector3.zero;

		StepAI();
		KJTime.Add(StepAI, 1.0f);
	}

	void SetAIPosition ()
	{
		aiFinalPosition = GameController.PlayerUnit.transform.position + aiOffsetPosition;
		SetTargetPoint( aiFinalPosition );
	}

	bool isAIShooting = true;

	void StepAI ()
	{
		if (isAIShooting) {
			isAIShooting = false;
			StopShooting();
		} else {
			isAIShooting = true;
			StartShooting();
		}


		float minDistance = 40.0f, maxDistance = 80.0f;
		float angle = Random.value * KJMath.CIRCLE;
		float distance = Random.Range(minDistance, maxDistance);

		aiOffsetPosition = new Vector3(distance * Mathf.Sin (angle), distance * Mathf.Cos (angle), 0.0f);
	}
}

