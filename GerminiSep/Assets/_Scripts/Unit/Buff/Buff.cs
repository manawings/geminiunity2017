using UnityEngine;
using System.Collections;

public class Buff
{
	public enum Type {
		None,
		Stun,
		Burn,
		Acid,
		Shield,
		Void,
		EliteShield
	}
	
	// Category
	public bool IsActive { get; private set; }
	Type buffType = Type.None;
	
	// Metrics
	float time = 0.0f; // Remaining Time Active in Seconds
	public float power { get; private set; } // Current Power of Buff
	
	// References
	Unit unit;
	
	public void Deploy (Buff.Type buffType, Unit unit)
	{
		time = 0.0f;
		power = 0.0f;
		this.buffType = buffType;
		IsActive = false;
		this.unit = unit;
	}
	
	public void Run (float deltaTime)
	{
		if (IsActive) {
			time -= deltaTime;
			
			if (time < 0)
			{
				Dispel();
			}
		}	
	}
	
	public void Apply (float power, float time)
	{
		if (!IsActive)
		{
			IsActive = true;
			OnApply();
		}

		if (unit != null) {
			if (buffType == Type.Stun) {
				if (unit.isElite) time *= 0.50f;
			}
		}

		if (time > this.time) this.time = time;
		if (power > this.power) this.power = power;
	}
	
	public void Dispel ()
	{
		// Buff is forced to be dispelled.
		if (IsActive) {
			
			IsActive = false;
			time = 0.0f;
			OnRemove();
			
		}
	}
	
	public void Terminate ()
	{
		time = 0.0f;
		power = 0.0f;
		this.buffType = Buff.Type.None;
		IsActive = false;
		unit = null;
	}
	
	void OnApply ()
	{
		// Visual Effects when buff is applied for the first time.
		if (unit != null) {
			unit.CalibrateBuffGraphics();
			if (buffType == Type.Stun) unit.StopWeapons();
		}
	}
	
	void OnRemove ()
	{
		// Dispel visual effect when buff is removed.	
		if (unit != null) {
			unit.CalibrateBuffGraphics();
			if (buffType == Type.Stun && unit != GameController.PlayerUnit) unit.ResumeWeapons();
		}
	}	
	
	// Visuals
	public static Color ColorForType (Buff.Type buffType)
	{
		switch (buffType)
		{
		
		case Buff.Type.None:
			return Color.black;
			break;
			
		case Buff.Type.Stun:
			return new Color(0.0f, 0.25f, 0.5f, 1.0f);
			break;
			
		case Buff.Type.Burn:
			return new Color(0.5f, 0.15f, 0.0f, 1.0f);
			break;
			
		case Buff.Type.Acid:
			return new Color(0.05f, 0.3f, 0.0f, 1.0f);
			break;
			
		case Buff.Type.Shield:
			return new Color(0.0f, 0.15f, 0.25f, 1.0f);
			break;

		case Buff.Type.EliteShield:
			return new Color(0.0f, 0.15f, 0.25f, 1.0f);
			break;
			
		case Buff.Type.Void:
			return new Color(0.18f, 0.0f, 0.25f, 1.0f);
			break;
			
		}
		
		return Color.black;
	}
	
	public static KJEmitter EmitterForType (Buff.Type buffType)
	{
		switch (buffType)
		{
		
		case Buff.Type.None:
			return null;
			break;
			
		case Buff.Type.Stun:
			return EffectController.Singleton.buffStun;
			break;
			
		case Buff.Type.Burn:
			return EffectController.Singleton.buffBurn;
			break;
			
		case Buff.Type.Acid:
			return EffectController.Singleton.buffAcid;
			break;
			
		case Buff.Type.Shield:
			return EffectController.Singleton.shieldTrail;
			break;

		case Buff.Type.EliteShield:
			return EffectController.Singleton.bigShieldTrail;
			break;
			
		case Buff.Type.Void:
			return EffectController.Singleton.buffVoid;
			break;
			
		}
		
		return null;
	}
}

