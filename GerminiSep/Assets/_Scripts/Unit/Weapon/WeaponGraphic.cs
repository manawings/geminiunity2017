using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponGraphic
{
	
	/** Class to keep track of data used to render a particular Weapon.
	  * All Weapons will have a reference to WeaponGraphic object. */
	
	public enum Image
	{

		StandardBlue,
		StandardPink,
		StandardYellow,
		StandardRed,
		StandardGreen,

		MissileBlue,
		MissilePink,
		MissileYellow,
		MissileRed,
		MissileGreen,

		StarBlue,
		StarPink,
		StarGreen,
		StarYellow,
		StarRed,

		RoundBlue,
		RoundPink,
		RoundYellow,
		RoundRed,
		RoundGreen,

		SpeedBlue,
		SpeedPink,
		SpeedYellow,
		SpeedRed,
		SpeedGreen,

		SonarBlue,
		SonarPink,
		SonarYellow,
		SonarRed,
		SonarGreen,

		ElectroBlue,
		ElectroPink,
		ElectroYellow,
		ElectroRed,
		ElectroGreen,

		SwirlBlue,
		SwirlPink,
		SwirlYellow,
		SwirlRed,
		SwirlGreen,

		BoomerBlue,
		BoomerPink,
		BoomerYellow,
		BoomerRed,
		BoomerGreen,

		BlackHoleBomb
	}

	private Image _image;
	public Image image
	{
		get { return _image; }
		set {
			_image = value;

			if ( _image == Image.StarBlue || _image == Image.StarYellow || _image == Image.StarGreen || _image == Image.StarPink || _image == Image.StarRed || _image == Image.BlackHoleBomb) {
				isSpinning = true;
			}

			if ( _image == Image.SwirlBlue || _image == Image.SwirlGreen || _image == Image.SwirlPink || _image == Image.SwirlRed || _image == Image.SwirlYellow ) {
				isSpinning = true;
			}

			if ( _image == Image.BoomerBlue || _image == Image.BoomerGreen || _image == Image.BoomerPink || _image == Image.BoomerRed || _image == Image.BoomerYellow ) {
				isSpinning = true;
			}
		}
	}
	public Color startColor = Color.white, endColor = Color.white;
	public Color startColorExplosion = Color.white, endColorExplosion = Color.white;
	public bool canExplode = false;
	public bool hasEmitter = false;
	public bool isSpinning = false;
	
	public KJEmitter emitterModel = null;

	public string GetNameOfSoundEffect {
		get {
			string imageName = image.ToString();

			if (imageName.Contains("Standard")) {
				return "Shot";
			}

			if (imageName.Contains("Electro")) {
				return "Electro";
			}

			if (imageName.Contains("Missile")) {
				return "Missile";
			}

			if (imageName.Contains("BlackHoleBomb")) {
				return "BH_Fire";
			}

			if (imageName.Contains("Star")) {
				return "Scatter";
			}

			if (imageName.Contains("Round")) {
				return "Scatter";
			}

			if (imageName.Contains("Sonar")) {
				return "Sonar";
			}

			if (imageName.Contains("Speed")) {
				return "Laser";
			}

			return "Shot";
		}

	}
	
	public static string GetNameOfClip (Image image)
	{
		switch (image)
		{


		case Image.StandardBlue:
			return "b_StandardBlue";
			break;
			
		case Image.StandardYellow:
			return "b_StandardYellow";
			break;
			
		case Image.StandardRed:
			return "b_StandardRed";
			break;
			
		case Image.StandardPink:
			return "b_StandardPink";
			break;
			
		case Image.StandardGreen:
			return "b_StandardGreen";
			break;



		case Image.MissileBlue:
			return "b_MissileBlue";
			break;


		case Image.MissileYellow:
			return "b_MissileYellow";
			break;

		case Image.MissileRed:
			return "b_MissileRed";
			break;

		case Image.MissilePink:
			return "b_MissilePink";
			break;

		case Image.MissileGreen:
			return "b_MissileGreen";
			break;



		case Image.StarBlue:
			return "b_StarBlue";
			break;

		case Image.StarYellow:
			return "b_StarYellow";
			break;

		case Image.StarRed:
			return "b_StarRed";
			break;

		case Image.StarPink:
			return "b_StarPink";
			break;

		case Image.StarGreen:
			return "b_StarGreen";
			break;



		case Image.RoundBlue:
			return "b_RoundBlue";
			break;

		case Image.RoundYellow:
			return "b_RoundYellow";
			break;

		case Image.RoundRed:
			return "b_RoundRed";
			break;

		case Image.RoundPink:
			return "b_RoundPink";
			break;

		case Image.RoundGreen:
			return "b_RoundGreen";
			break;



		case Image.SpeedBlue:
			return "b_SpeedBlue";
			break;
			
		case Image.SpeedYellow:
			return "b_SpeedYellow";
			break;
			
		case Image.SpeedRed:
			return "b_SpeedRed";
			break;
			
		case Image.SpeedPink:
			return "b_SpeedPink";
			break;
			
		case Image.SpeedGreen:
			return "b_SpeedGreen";
			break;



		case Image.SonarBlue:
			return "b_SonarBlue";
			break;
			
		case Image.SonarYellow:
			return "b_SonarYellow";
			break;
			
		case Image.SonarRed:
			return "b_SonarRed";
			break;
			
		case Image.SonarPink:
			return "b_SonarPink";
			break;
			
		case Image.SonarGreen:
			return "b_SonarGreen";
			break;



		case Image.ElectroBlue:
			return "b_ElectroBlue";
			break;
			
		case Image.ElectroYellow:
			return "b_ElectroYellow";
			break;
			
		case Image.ElectroRed:
			return "b_ElectroRed";
			break;
			
		case Image.ElectroPink:
			return "b_ElectroPink";
			break;
			
		case Image.ElectroGreen:
			return "b_ElectroGreen";
			break;



		case Image.SwirlBlue:
			return "b_SwirlBlue";
			break;
			
		case Image.SwirlYellow:
			return "b_SwirlYellow";
			break;
			
		case Image.SwirlRed:
			return "b_SwirlRed";
			break;
			
		case Image.SwirlPink:
			return "b_SwirlPink";
			break;
			
		case Image.SwirlGreen:
			return "b_SwirlGreen";
			break;



		case Image.BoomerBlue:
			return "b_BoomerBlue";
			break;
			
		case Image.BoomerYellow:
			return "b_BoomerYellow";
			break;
			
		case Image.BoomerRed:
			return "b_BoomerRed";
			break;
			
		case Image.BoomerPink:
			return "b_BoomerPink";
			break;
			
		case Image.BoomerGreen:
			return "b_BoomerGreen";
			break;




		case Image.BlackHoleBomb:
			return "b_BlackHoleBomb";
			break;


		}
		
		return "b_RoundYellow";
	}
}

