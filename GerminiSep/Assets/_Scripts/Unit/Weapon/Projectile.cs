using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Projectile : KJSpriteBehavior
{
	
	/** Basic Physical Properties */
	
	float x = 0.0f, y = 0.0f;
	float speed = 0.0f, terminalSpeed = 700.0f, savedTerminalSpeed = 0.0f, accelerationFactor = 0.15f;
	float direction = 0, velocityX = 0.0f, velocityY = 0.0f;
	bool spin = false;
	bool ignoreArmor = false;
	
	// Advanced Physical Properties
	float procTime = 0.0f;
	int procCount = 0; // Phase of the proc.
	
	float hitCounter = 0.0f;
	const float hitCounterMax = 0.1f; // Time between checking each hit.
	
	
	/** Homing Missile Control */
	
	float homingTimeOut = 0.0f;
	const float HomingTimeoutMax = 4.0f;
	const int HomingDisengageSq = 4200; // 40 sq.
	const int HomingDisengageSqRival = 16000; // 40 sq.
	
	/** Ejection Properties */
	
	float ejectSpeed = 0.0f, ejectDirection = 0.0f;
	
	/** Damage and Reference */
	
	Weapon weapon;
	float damage = 0.0f;
	float extraModDamage = 0.0f;
	Unit.Team team;
	
	float time = 0.0f;
	
	float ModdedDamge {
		get {
			
			if (weapon.unit != null)
			{
				return weapon.unit.modules.OnDealDamage(damage) + extraModDamage;
			}
			
			return damage + extraModDamage;
		}
	}
	
	/** Behavior Properties */
	bool isHoming = false, isEjecting = false, isPiercing = false, isChainer = false, isBoomerang = false, isSwirl = false, isEngage = false;
	int chainCount = 3;	
	
	/** Buff */
	
	Buff.Type buffType = Buff.Type.None;
	int buffProc = 0;
	bool buffIsFactor = true;
	float buffPowerFactor = -1.0f;
	float buffTime = -1.0f;
	
	/** Visuals */
	private static Dictionary<WeaponGraphic.Image, tk2dSpriteAnimationClip> clipDictionary;
	tk2dSpriteAnimator _spriteAnimator;
	tk2dSpriteAnimator spriteAnimator {
		get {
			if (_spriteAnimator == null)
			{
				_spriteAnimator = GetComponent<tk2dSpriteAnimator>();	
			}
			
			return _spriteAnimator;
		}
	}
	
	/*
	private var startColor:int, endColor:int;
	private var startColorExplosion:int, endColorExplosion:int;
	private var isCritical:Boolean = false; */
	
	/** Hit Radius and Expansion */
	const int hitRadius = 20;
	const int hitRadiusElite = 50;
	
	int hitRadiusSq = 0; // 20 * 20
	int hitRadiusSqElite = 0; // 50 * 50
	
	// private var hitRadiusExpand:Number = 0;
	// private var scaleExpand:Number = 0;
	
	/** Chain Effect: Remember which Targets have been hit! */
	Entity currentTarget;
	GameObject previousTarget;
	
	/** Convienience Variables */
	
	Vector3 differenceVector = new Vector3();
	Vector3 directionVector = new Vector3();
	float angleVelocity;
	float proxyMoveDistance;
	float speedDifference;
	float absSpeedDifference;
	
	// Addition Projectile Options
	
	KJEmitter emitter;
	WeaponGraphic weaponGraphic;
	
	private static int unitOutLimitXMin = 0, unitOutLimitXMax = 0, unitOutLimitYMin = 0, unitOutLimitYMax = 0;
	public static void SetBulletLimits ()
	{
		tk2dCamera camera = Camera.main.GetComponent<tk2dCamera>();
		unitOutLimitXMin = (int) (camera.ScreenExtents.x * 2.5f);
		unitOutLimitXMax = (int) (camera.ScreenExtents.xMax * 2.5f);
		unitOutLimitYMin = (int) (camera.ScreenExtents.y * 1.3f);
		unitOutLimitYMax = (int) (camera.ScreenExtents.yMax * 1.65f);
	}
	
	bool platformMove = false;
	
	public void Deploy (Weapon weapon, WeaponGraphic weaponGraphic, Vector3 startPosition, float direction, float speed, float terminalSpeed, float accelerationFactor, Unit.Team team, float time, bool isPiercing)
	{
		procTime = 0.0f;
		procCount = 0;
		
		previousTarget = null;
		buffType = weapon.buffType;
		buffProc = weapon.buffProcChance;
		buffPowerFactor = weapon.buffPowerFactor;
		buffTime = weapon.buffTime;
		
		if (WaveController.type == WaveController.Type.Platform)
			platformMove = true;
		else
			platformMove = false;
		
		transform.position = startPosition;
		x = transform.position.x;
		y = transform.position.y;
		RotationByRadians = direction;
		
		this.weapon = weapon;
		this.direction = direction;
		this.isPiercing = isPiercing;
		this.speed = speed;
		this.terminalSpeed = terminalSpeed;
		this.savedTerminalSpeed = terminalSpeed;
		this.accelerationFactor = accelerationFactor;
		this.time = time;
		this.weaponGraphic = weaponGraphic;
		
		damage = weapon.AdjustedDamage;
		extraModDamage = 0.0f;
		
		/** Direction */
		directionVector.x = Mathf.Sin(direction);
		directionVector.y = Mathf.Cos(direction);
		
		//		hitRadiusSq = hitRadius * hitRadius;
		
		AddTimer (Run);
		this.team = team;
		
		SetImage(weaponGraphic.image);
		
		if (weaponGraphic.hasEmitter)
		{
			
			emitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
			emitter.CopyFromModel(weaponGraphic.emitterModel);
			emitter.SetObjectLink(gameObject);
			emitter.SetColor(weaponGraphic.startColor, weaponGraphic.endColor);
			emitter.Deploy();
			
		}
		
		spin = weaponGraphic.isSpinning;
		
		if (team == Entity.Team.Enemy) {
			currentTarget = GameController.PlayerUnit;
		} else {
			currentTarget = null;
		}
	}
	
	protected override void OnDeactivate ()
	{
		if (emitter != null) {
			emitter.Terminate();
			emitter = null;
		}
		
		spriteAnimator.Stop();
		base.OnDeactivate();
	}
	
	public void SetHitRadius (bool isDouble = false) {
		if (isDouble) {
			hitRadiusSq = (hitRadius * 2) * (hitRadius * 2);
			hitRadiusSqElite = (hitRadiusElite * 2) * (hitRadiusElite * 2);
		} else {
			hitRadiusSq = hitRadius * hitRadius;
			hitRadiusSqElite = hitRadiusElite * hitRadiusElite;
		}
	}
	
	public void SetHoming (bool isHoming)
	{
		this.isHoming = isHoming;
		homingTimeOut = HomingTimeoutMax;
	}
	
	public void SetChainer (int chainAmount)
	{
		if (chainAmount == 0) {
			isChainer = false;
		} else {
			isChainer = true;
			chainCount = chainAmount;
		}
	}

	public void SetEngage (bool isEngage) {
		this.isEngage = isEngage;
	}

	public void SetBoomerang (bool isBoomerang) {
		this.isBoomerang = isBoomerang;
	}
	
	public void SetSwirl (bool isSwirl) {
		this.isSwirl = isSwirl;
	}
	
	public void SetProcTime (float procTime) {
		// Time until the intended procs.
		this.procTime = procTime;
		
//		if (isSwirl || isBoomerang)
//			sprite.scale = Vector3.one * 2.0f;
//		else
//			sprite.scale = Vector3.one;
	}
	
	public void EjectWithSpeedAndDirection (float speed, float direction)
	{
		ejectSpeed = speed;
		ejectDirection = direction;
	}
	
	void ProcEffect () {
		
		// The Proc Timer has run out.
		procTime = 0.0f;
		procCount ++;
		
		if (isBoomerang) {
			
			if (procCount == 1) {
				// First Proc - Deccelerate to Zero.
				procTime = 0.5f;
				terminalSpeed = 0.0f;
			}
			
			if (procCount == 2) {
				// Reverse the Speed to the other side.
				terminalSpeed = -savedTerminalSpeed;
			}
		}
		
		if (isSwirl) {
			terminalSpeed = 0.0f;
		}
	}
		
	void Run ()
	{
		//		alpha += 10;
		
		directionVector.x = Mathf.Sin(direction);
		directionVector.y = Mathf.Cos(direction);
		
		RunHoming();
		RunEjection();
		//RunBoomerang();
		
		/** Accelerate */
		if (speed != terminalSpeed)
		{
			speedDifference = terminalSpeed - speed;
			absSpeedDifference = speedDifference < 0 ? -speedDifference : speedDifference;
			if (absSpeedDifference < 10.0f)
			{
				speed = terminalSpeed;	

				if (terminalSpeed == 0.0f && !isSwirl && !isBoomerang) {
					OnHit();
					Deactivate();
				}
			} else {
				speed += speedDifference * accelerationFactor;	
			}
			
			if (!isHoming)
			{
				velocityX = directionVector.x * speed;
				velocityY = directionVector.y * speed;
			}
			
		}
		
		if (platformMove) y -= 40.0f * DeltaTime;
		
		x += velocityX * Time.deltaTime;
		y += velocityY * Time.deltaTime;

		if (isSwirl) y -= 30.0f * Time.deltaTime;
		
		
		transform.position = new Vector3(x, y, 0.0f);
		
		if (spin) {
			RotationByRadians += 0.25f;
		} else {
			RotationByRadians = direction;
		}

		if ( y > unitOutLimitYMax || y < unitOutLimitYMin || x > unitOutLimitXMax || x < unitOutLimitXMin)
		{
			if (!isBoomerang) {
				gameObject.GetComponent<KJActiveObject>().Deactivate();
			}
		} else {
			if (isSwirl) {
				if (hitCounter <= 0) {
					hitCounter = hitCounterMax;
					CheckForHit ();
				} else {
					hitCounter -= DeltaTime;
				}
			} else {
				CheckForHit ();
			}
		}
		
		if (emitter != null) emitter.SetDirection(direction);
		
		if (procTime != 0) {
			procTime -= DeltaTime;
			if (procTime <= 0)
				ProcEffect();
		}
		
		if (time != 0.0f)
		{
			time -= DeltaTime;
			if (time <= 0.0f)
			{
				//Debug.Log ("Timer Ran Out");
				OnHit();	
				Deactivate();
			}
		}
	}
	
	float alpha = 30;
				
	void RunHoming ()
	{
		// Run the Homing Script
		if (!isHoming) return;
		
		if (homingTimeOut > 0) {
			
			homingTimeOut -= DeltaTime;
			
		} else {
			
			// Missile has run out of time.
			if (!isEngage) {
				Disengage();
				return;
			}
		}
		
		Entity targetUnit = GetNearestTarget();
		
		if (targetUnit != null)
		{
			
			differenceVector.x = targetUnit.x - x;
			differenceVector.y = targetUnit.y - y;
			differenceVector.Normalize();
			
			Vector3 cross = Vector3.Cross(directionVector, differenceVector);
			float smallestAngle = Mathf.Acos(Vector3.Dot(directionVector, differenceVector));
			
			angleVelocity = smallestAngle * 0.15f;
			if (System.Single.IsNaN(smallestAngle)) {
				smallestAngle = 0.0f;
			}
			
			float speedFactor = ((1 - smallestAngle * KJMath.INVERSE_PI) * 0.85f) + 0.15f;
			float oldDirection = direction;
			
			if (cross.z > 0)
			{
				direction -= angleVelocity;
			} else {
				direction += angleVelocity;
			}
			
			if (System.Single.IsNaN(direction)) direction = oldDirection;
			
			if (emitter) emitter.SetDirection(direction);
			
			float distanceSquared = ((targetUnit.x - x) * (targetUnit.x - x)) + ((targetUnit.y - y) * (targetUnit.y - y));
			proxyMoveDistance = speed * speedFactor;
			if (System.Single.IsNaN(distanceSquared)) distanceSquared = 6400.0f;
			
			if (distanceSquared < 6400 && distanceSquared > 0)
			{
				float distanceToTarget =   Mathf.Sqrt(distanceSquared);
				
				if (System.Single.IsNaN(distanceToTarget)) distanceToTarget = 0.0f;
				float speedFactorDistance = distanceToTarget / 80.0f;
				proxyMoveDistance *= (0.15f + (speedFactorDistance * 0.85f));
				
			}
			velocityX = directionVector.x * proxyMoveDistance;
			velocityY = directionVector.y * proxyMoveDistance;
			
		} else {
			velocityX = directionVector.x * speed;
			velocityY = directionVector.y * speed;	
		}
	}
	
	void RunEjection ()
	{
		x += Mathf.Sin (ejectDirection) * ejectSpeed * Time.deltaTime;
		y += Mathf.Cos (ejectDirection) * ejectSpeed * Time.deltaTime;
		ejectSpeed = KJMath.ConvergeToZero(ejectSpeed, 0.9f, 10.0f);
	}
	
	void CheckForHit ()
	{
		if (team == Entity.Team.Friendly)
		{
			if (CheckForHitWithEnemy()) return;	
			
			// if (CheckForHitCargo()) return;	
			if (CheckForHitWithBoss()) return;
			
		} else {
			if (CheckForHitWithPlayer()) return;
			CheckForHitWithAlly();
		}
	}
	
	void OnHit ()
	{
		if (weaponGraphic.canExplode)
		{
			EffectController.SmallExplosionAt(transform.position, weaponGraphic.startColorExplosion, weaponGraphic.endColorExplosion);	
			KJCanary.PlaySoundEffect("MissileHit", transform.position, true);
			
		} else {
			KJActivePool.GetNewOf<Boom>("Boom").Deploy(transform.position, weaponGraphic.startColor);	
			EffectController.HitBlastAt(transform.position, weaponGraphic.startColor, direction);
			KJCanary.PlaySoundEffect("Hit", transform.position, true);
			// EffectController.HexShieldAt(transform.position);
		}
		
		
		if (weapon.isBlackHole)
		{
			BlackHole.CreateAndDeploy(transform.position, damage, weapon.BHradius, weapon.BHtime);
		}
		
	}
	
	void CheckForHitWithAlly ()
	{
		if (GameController.AllyUnit == null) return;
		if (GameController.AllyUnit.IsAlive) {
			
			float distanceToTarget = KJMath.DistanceSquared2d(transform.position, GameController.AllyUnit.transform.position);
			
			if (distanceToTarget < HomingDisengageSq)
			{
				Disengage();
			}
			
			if (distanceToTarget < hitRadiusSq)
			{
				
				HitUnit(GameController.AllyUnit);
				if (!isSwirl) Deactivate();
			}
		}
	}
	
	void Disengage ()
	{
		// Release the Lock on the unit.
		isHoming = false;
	}
	
	bool CheckForHitWithPlayer ()
	{
		if (GameController.PlayerUnit.IsAlive) {
			
			float distanceToTarget = KJMath.DistanceSquared2d(transform.position, GameController.PlayerUnit.transform.position);
			
			
			if (distanceToTarget < HomingDisengageSq)
			{
				if (!isEngage) {
					Disengage();
				}
			}
			
			
			if (distanceToTarget < hitRadiusSq)
			{
				HitUnit(GameController.PlayerUnit);
				if (!isSwirl) Deactivate();
				return true;
			}
		}
		
		return false;
	}
	
	bool CheckForHitWithBoss ()
	{
		if (GameController.CurrentSectorBoss != null )
		{
			if (GameController.CurrentSectorBoss.CheckForHit(this, 300, damage))
			{ 
				OnHit();
				Deactivate();
				return true;
			}
		}
		
		return false;
	}

	public void DealDamageToSoftSpot (SoftSpot softSpot) {

		softSpot.TakeDamage(ModdedDamge);

	}
	
	
	void HitUnit (Unit unit)
	{
		
		unit.modules.OnHitFromProjectile(this);
		
		float originalDamage = damage;
		float moddedDamage = ModdedDamge;
		
		bool isCritical = (moddedDamage - originalDamage > 0.35f * originalDamage) ? true : false;
		
		Entity.DamageType damageType = Entity.DamageType.Normal;
		if (weaponGraphic.canExplode) damageType = Entity.DamageType.Missile;
		
		unit.TakeHit(moddedDamage, damageType, isCritical, ignoreArmor);
		
		ApplyBuff(unit);
		
		OnHit();
	}
	
	public void SetIgnoreArmor (bool ignoreArmor = false) {
		this.ignoreArmor = ignoreArmor;
	}
	
	void ApplyBuff (Unit unit)
	{
		
		if (buffType == Buff.Type.None) return;
		
		if (KJDice.Roll(buffProc)) {
			
			float buffPower;
			
			if (buffPowerFactor == -1.0f) buffPowerFactor = 0.0f;
			if (buffTime == -1.0f) buffTime = 3.0f;
			
			if (buffIsFactor) {
				buffPower = damage * buffPowerFactor;
			} else {
				buffPower = buffPowerFactor;
			}
			
			switch (buffType)//#####
			{
				
			case Buff.Type.Stun:
				
				unit.ApplyStun(buffPower, buffTime);
				break;
				
			case Buff.Type.Burn:
				
				unit.ApplyBurn(buffPower, buffTime);
				break;
				
			case Buff.Type.Acid:
				
				unit.ApplyAcid(buffPower, buffTime);
				
				break;
				
			case Buff.Type.Shield:
				
				unit.ApplyShield(buffPower, buffTime);
				
				break; 
				
			}
		}
	}
	
	bool CheckForHitWithEnemy ()
	{
		float distanceToTarget;
		float useHitRadius;
		
		for (int i = EnemyUnit.allActive.Count - 1 ; i > -1 ; i -- )
		{
			Unit enemyUnit = EnemyUnit.allActive[i];
			
			if (enemyUnit.gameObject == previousTarget) continue;
			
			distanceToTarget = KJMath.DistanceSquared2d(transform.position, enemyUnit.transform.position);
			
			if (isHoming && enemyUnit.isRival && distanceToTarget < HomingDisengageSqRival)
			{
				Disengage();
			}
			
			useHitRadius = (!enemyUnit.isElite || enemyUnit.isRival) ? hitRadiusSq : hitRadiusSqElite;
			
			if (distanceToTarget < useHitRadius)
			{
				
				previousTarget = enemyUnit.gameObject;
				
				HitUnit(enemyUnit);
				
				if (isChainer)
				{
					isHoming = true;
					if (chainCount == 0)
					{
						Deactivate();
					} else {
						speed *= 0.5f;
						chainCount --;
					}
				} else {
					if (!isPiercing)
					{
						Deactivate();
					}
				}
				
				return true;
			}
		}
		return false;
	}
	
	Entity GetNearestTarget ()
	{
		
		if (currentTarget != null && currentTarget.IsAlive && (previousTarget != currentTarget.gameObject)) return currentTarget;
		
		
		Entity nearestTarget = null;
		float closestDistance = float.MaxValue;
		
		if (team == Unit.Team.Friendly)
		{
			
			if (GameController.CurrentSectorBoss != null) {
				
				List<SoftSpot> softSpots = GameController.CurrentSectorBoss.VulnerableSoftSpots;
				for (int i = 0 ; i < softSpots.Count ; i ++ )	
				{
					SoftSpot softSpot = softSpots[i];
					if (softSpot.gameObject == previousTarget) continue;
					float distanceToTarget = KJMath.DistanceSquared2d(softSpot.gameObject, gameObject);
					
					if (distanceToTarget < closestDistance) {
						closestDistance = distanceToTarget;
						nearestTarget = softSpot;
					}
				} 
				
			} else {
				
				for (int i = 0 ; i < EnemyUnit.allActive.Count ; i ++ )	
				{
					Unit enemyUnit = EnemyUnit.allActive[i];
					if (enemyUnit.gameObject == previousTarget) continue;
					float distanceToTarget = KJMath.DistanceSquared2d(enemyUnit.gameObject, gameObject);
					
					if (distanceToTarget < closestDistance) {
						closestDistance = distanceToTarget;
						nearestTarget = enemyUnit;	
					}
				} 
			}
		}
		
		if (team == Unit.Team.Enemy)
		{
			// TODO: Add in multiple unit tracking for multiplayer
			if (GameController.PlayerUnit.IsAlive) {
				
				nearestTarget = GameController.PlayerUnit;
				
			} else {
				Disengage ();	
			}
		}
		
		currentTarget = nearestTarget;
		return nearestTarget;
	}
	
	void SetImage (WeaponGraphic.Image image)
	{
		spriteAnimator.Play(GetClipForImage(image));
	}
	
	tk2dSpriteAnimationClip GetClipForImage (WeaponGraphic.Image image)
	{
		if (clipDictionary == null)
		{
			clipDictionary = new Dictionary<WeaponGraphic.Image, tk2dSpriteAnimationClip>();	
		}
		
		if (!clipDictionary.ContainsKey(image))
		{
			clipDictionary.Add(image, spriteAnimator.GetClipByName(WeaponGraphic.GetNameOfClip(image)));
		}
		
		return clipDictionary[image];
	}
	
	public void MAddDamage (float damage)
	{
		this.extraModDamage += damage;
	}
	
	public void MAddBuff (Buff.Type buffType, float powerFactor = -1.0f, float time = -1.0f, bool isFactor = true)
	{
		this.buffType = buffType;
		buffProc = 100;
		buffPowerFactor = powerFactor;
		buffIsFactor = isFactor;
		buffTime = time;
	}
	
	public void MAddPiercing ()
	{
		isPiercing = true;
	}
	
	public void MAddLaserTrail ()
	{
		
		if (emitter != null) {
			emitter.Terminate();
			emitter = null;
		}
		
		emitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		emitter.CopyFromModel(EffectController.Singleton.lineTrail);
		emitter.SetObjectLink(gameObject);
		emitter.SetColor(weaponGraphic.startColor, weaponGraphic.endColor);
		emitter.Deploy();
	}
	
	public void MAddSpeed (float pixelPerSecond = 0.0f)
	{
		speed += pixelPerSecond;
		terminalSpeed += pixelPerSecond;
	}
}
		
