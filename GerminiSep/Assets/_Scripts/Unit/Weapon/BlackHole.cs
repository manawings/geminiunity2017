using UnityEngine;
using System.Collections;

public class BlackHole : KJSpriteBehavior
{

	public static BlackHole CreateAndDeploy (Vector3 position, float damage, int radius = 70, float time = 5.0f)
	{
		GameObject gameObject = (GameObject) Instantiate(Resources.Load("BlackHole"));
		BlackHole blackHole = gameObject.AddComponent<BlackHole>();
		blackHole.Deploy(position, damage, radius, time);
		return blackHole;
	}
	
	KJEmitter emitter;
	
	int hitRadiusSq;
	Vector3 position;
	bool readyToTick = true;
	float damage;

	float currentScale = 0.0f;
	bool scalingIn = true, isScaling = false;
	
	public void Deploy (Vector3 position, float damage = 0.0f, int radius = 70, float time = 5.0f)
	{
		this.position = position;
		position.z = 10;
		transform.localPosition = position;
		this.damage = damage * 0.25f;
		if( damage < 1) damage = 1;
		
		emitter = KJActivePool.GetNew("KJEmitter").GetComponent<KJEmitter>();
		emitter.CopyFromModel(EffectController.Singleton.blackHole);
		emitter.Deploy(position);
		
		int totalRadius = radius + 30;
		hitRadiusSq = totalRadius * totalRadius;

		AddTimer (Run);
		AddTimer (PeriodRun, 0.25f);
		AddTimer (EndHole, time, 1);

		KJCanary.PlaySoundEffect("BH_Start");

		flashColor = new Color(0.15f, 0.0f, 0.15f, 1.0f);
		Flash (5);

		currentScale = 0.0f;
		scalingIn = true;
		isScaling = true;
	}

	void EndHole ()
	{

		EndBHSound();

		KJCanary.PlaySoundEffect("BH_End");
		emitter.Terminate();

		scalingIn = false;
		isScaling = true;
	}
	
	void PeriodRun ()
	{
		readyToTick = true;
	}

	AudioSource audioSource;

	void StartBHSound ()
	{
		audioSource = KJCanary.PlaySoundEffect("BH_Loop");
	}

	void EndBHSound ()
	{
		if (audioSource != null) {
			KJCanary.FadeOut(audioSource);
			audioSource = null;
		}
	}
	
	void Run ()
	{
		CheckForHitWithEnemy();
		CheckForHitWithPlayer();
		if (readyToTick) readyToTick = false;

		if (isScaling) {

			float scaleDiff;//  = 1.0f - currentScale

			if (scalingIn) {

				scaleDiff = 1.0f - currentScale;

				if (scaleDiff > 0.02f) {
					currentScale += scaleDiff * 0.07f;

				} else {

					currentScale = 1.0f;
					isScaling = false;

					StartBHSound();
				}

			} else {


				if (currentScale > 0.02f) {
					currentScale -= currentScale * 0.10f;
					
				} else {
					
					currentScale = 0.0f;
					isScaling = false;
					Deactivate();
				}
	
			}

			transform.localScale = Vector3.one * currentScale;
		}
	}
	
	void EffectUnit (Unit unit)
	{
		unit.BlackHolePull(position);
		unit.ApplyVoid(0.1f);
		if (readyToTick) unit.TakeDamage(damage);
	}

	void CheckForHitWithPlayer ()
	{
		if (!GameController.PlayerUnit.IsAlive) return; 

		Unit unit = GameController.PlayerUnit;
		float distanceToTarget = KJMath.DistanceSquared2d(position, unit.transform.position);
			
		if (distanceToTarget < hitRadiusSq)
		{
				
			EffectUnit(unit);
		}
	}
	
	void CheckForHitWithEnemy ()
	{
		for (int i = 0 ; i < EnemyUnit.allActive.Count ; i ++ )
		{
			Unit enemyUnit = EnemyUnit.allActive[i];
			
			float distanceToTarget = KJMath.DistanceSquared2d(position, enemyUnit.transform.position);

			if (distanceToTarget < hitRadiusSq)
			{

				EffectUnit(enemyUnit);
			}
		}
	}
	
	protected override void OnDeactivate ()
	{

		if (audioSource != null) {
			KJCanary.FadeOut(audioSource);
			audioSource = null;
		}


		RemoveAllTimers();
		Destroy(gameObject);
	}

}

