using UnityEngine;
using System.Collections;

public class Reactor : KJBehavior
{
	
	Weapon weapon;
	Collectable.Type type;
	
	public void Deploy (Unit unit, Collectable.Type type)
	{
		
		this.type = type;
		weapon = gameObject.AddComponent<Weapon>();
		
		switch (type)
		{
			
			case Collectable.Type.Green:
			weapon.CopyDataFrom(WeaponPattern.ReactorGreen);
			break;
			
			case Collectable.Type.Yellow:
			weapon.CopyDataFrom(WeaponPattern.ReactorYellow);
			break;
			
			case Collectable.Type.Pink:
			weapon.CopyDataFrom(WeaponPattern.ReactorPink);
			break;
			
			case Collectable.Type.Blue:
			weapon.CopyDataFrom(WeaponPattern.ReactorBlue);
			break;
			
		}
		
		weapon.Deploy(unit);
	}
	
	public static int getIdForType (Collectable.Type type)
	{
		switch (type)
		{
			
			case Collectable.Type.Green:
			return 0;
			break;
			
			case Collectable.Type.Yellow:
			return 1;
			break;
			
			case Collectable.Type.Pink:
			return 2;
			break;
			
			case Collectable.Type.Blue:
			return 3;
			break;
			
		}
		
		return 0;
	}
	
	public static Collectable.Type getTypeForId (int id)
	{
		switch (id)
		{
			
			case 0:
			return Collectable.Type.Green;
			
			case 1:
			return Collectable.Type.Yellow;
			
			case 2:
			return Collectable.Type.Pink;
			
			case 3:
			return Collectable.Type.Blue;
			
		}
		
		return Collectable.Type.Green;
	}
	
	public void Execute ()
	{
		KJCanary.PlaySoundEffect("ReactorUse", transform.position);
		weapon.Fire();
	}
}

