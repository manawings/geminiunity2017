using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Beam : KJBeam
{
	
	Entity originEntity;
	Weapon weapon;
	float beamTime;

	//Buff
	Buff.Type buffType = Buff.Type.None;
	int buffProc = 0;
	bool buffIsFactor = true;
	float buffPowerFactor = -1.0f;
	float buffTime = -1.0f;
	float damage = 0.0f;
	
	private const int BeamLength = 800;
	private const float DefaultBeamCooldown = 0.05f;
	
	private static float _BeamDamageFactor = 0.0f;
	private static float BeamDamageFactor {
		get {
			if (_BeamDamageFactor == 0.0f) {
				_BeamDamageFactor = 1.0f / (1.0f / DefaultBeamCooldown);
			}
			return _BeamDamageFactor * 0.75f;//#####
		}
	}
	
	int beamDamage = 0;
	
	Dictionary<Entity, float> targetCooldownDictionary = new Dictionary<Entity, float>();
	List<Entity> targetList = new List<Entity>();


	
	public void Deploy (Entity originEntity, Weapon weapon, float beamTime, bool isPlayerBeam = false)
	{
		audioSource = null;
		KJCanary.PlaySoundEffect("PreBeam", originEntity.transform.position);
		buffType = weapon.buffType;
		buffProc = weapon.buffProcChance;
		buffPowerFactor = weapon.buffPowerFactor;
		buffTime = weapon.buffTime;
		damage = weapon.AdjustedDamage;

		targetCooldownDictionary.Clear();
		targetList.Clear();
		
		this.originEntity = originEntity;
		this.weapon = weapon;
		this.beamTime = beamTime;
		this.isPlayerBeam = isPlayerBeam;

		innerBeamSprite.color = weapon.weaponGraphic.startColor;
		outerBeamSprite.color = weapon.weaponGraphic.endColor;
		//Color fadeColor = outerBeamSprite.color;
		//fadeColor.a = 0.0f;

		trailEmitter.SetColor(
			weapon.weaponGraphic.startColor,
			weapon.weaponGraphic.endColor);
		chargeEmitter.SetColor(
			weapon.weaponGraphic.startColor,
			weapon.weaponGraphic.endColor);
		electroEmitter.SetColor(
			weapon.weaponGraphic.startColor,
			weapon.weaponGraphic.endColor);
		
		originEntity.beam = this;
	
		// Calibrate the Beam Damage
		float beamTotalDamage = weapon.AdjustedDamage;
		beamDamage = (int) (beamTotalDamage * BeamDamageFactor);
		if (beamDamage < 1) beamDamage = 1;
		
		SetBeamPosition();
		StartBeam();
		
	}
	
	protected override void OnActivate ()
	{
		
	}
	
	protected override void OnDeactivate ()
	{
		if (audioSource != null) KJCanary.FadeOut(audioSource);
		targetList.Clear();
		targetCooldownDictionary.Clear();
		if (originEntity != null) originEntity.beam = null;
		base.OnDeactivate();
	}

	AudioSource audioSource;
	
	protected override void EngageBeam ()
	{
		audioSource = KJCanary.PlaySoundEffect("Beam", originEntity.transform.position);

		Run ();

		AddTimer (Run);
		AddTimer (EndBeam, beamTime, 1);

		if (originEntity == GameController.PlayerUnit)
			GameController.Singleton.MegaShakeScreen();

		base.EngageBeam();
	}
	
	protected override void DisengageBeam ()
	{

		if (audioSource != null) {
			KJCanary.FadeOut(audioSource);
			audioSource = null;
		}

		KJCanary.PlaySoundEffect("BeamEnd", originEntity.transform.position);
		RemoveTimerOrFlash (Run);
		base.DisengageBeam();
	}
	
	void Run ()
	{
		LowerCooldowns();
		CheckForHit();
	}
	
	void LowerCooldowns ()
	{
		foreach(Entity entity in targetList)
		{
		    if (targetCooldownDictionary[entity] > 0.0f) targetCooldownDictionary[entity] -= DeltaTime;
		}
	}
	
	void CheckForHit ()
	{
		if (weapon.team == Entity.Team.Friendly)
		{
			// Friendly Beam

			if (GameController.CurrentSectorBoss != null) {
				
				List<SoftSpot> softSpots = GameController.CurrentSectorBoss.VulnerableSoftSpots;
				for (int i = 0 ; i < softSpots.Count ; i ++ )	
				{
					SoftSpot softSpot = softSpots[i];
					ExecuteHit(softSpot);
				} 
				
			}

			for (int i = 0 ; i < EnemyUnit.allActive.Count ; i ++ )	
			{

				Unit enemyUnit = EnemyUnit.allActive[i];
				ExecuteHit(enemyUnit);
					
			}
			
		} else if (weapon.team == Entity.Team.Enemy) {
			
			// Enemy Beam	
			if (GameController.PlayerUnit.IsAlive) {
				if (ExecuteHit(GameController.PlayerUnit))
				{
					// TEMP
					GameController.Singleton.UpdateLifeBar();	
				}
			}

			if (GameController.AllyUnit !=null && GameController.AllyUnit.IsAlive) {

				ExecuteHit(GameController.AllyUnit);
				
			}
		}
	}

	void ApplyBuff (Unit unit)
	{
		if (buffType == Buff.Type.None) return;
		
		if (KJDice.Roll(buffProc)) {
			
			float buffPower;
			
			if (buffPowerFactor == -1.0f) buffPowerFactor = 0.0f;
			if (buffTime == -1.0f) buffTime = 3.0f;
			
			if (buffIsFactor) {
				buffPower = damage * buffPowerFactor;
			} else {
				buffPower = buffPowerFactor;
			}
			
			switch (buffType)//#####
			{
				
			case Buff.Type.Stun:
				
				unit.ApplyStun(buffPower, buffTime);
				break;
				
			case Buff.Type.Burn:
				
				unit.ApplyBurn(buffPower, buffTime);
				break;
				
			case Buff.Type.Acid:
				
				unit.ApplyAcid(buffPower, buffTime);
				
				break;
				
			case Buff.Type.Shield:
				
				unit.ApplyShield(buffPower, buffTime);
				
				break; 
				
			}
		}
	}

	bool ExecuteHit (Entity targetEntity)
	{
		bool hasHitTarget = false;
		Vector2 closePoint = KJMath.GetClosestPointToSegment2D ( beamStartPoint, beamTargetPoint, targetEntity.transform.position );				
		if (KJMath.DistanceSquared2d(closePoint, targetEntity.transform.position) < 500.0f) {
	
			
			// Check if this particular unit is on cooldown.

			bool canShoot = true;
			if (targetCooldownDictionary.ContainsKey(targetEntity))
			{
				if (targetCooldownDictionary[targetEntity] > 0.0f) canShoot = false;
			} else {
				targetList.Add(targetEntity);
				targetCooldownDictionary.Add(targetEntity, 0.0f);
			}
			if (canShoot) {
				targetEntity.TakeDamage(beamDamage, Entity.DamageType.Beam, false, false, true);
				hasHitTarget = true;
				targetCooldownDictionary[targetEntity] = DefaultBeamCooldown;
			}

			if (targetEntity is Unit) {
				Unit unit = (Unit) targetEntity;
				ApplyBuff(unit);
			}
		}

		return hasHitTarget;
	}

	public override void SetBeamPosition ()
	{
		if (originEntity == null) { 
			Deactivate();
			return;
		}

		beamStartPoint = originEntity.transform.position;
		beamTargetPoint = GameController.PlayerUnit.transform.localPosition + Vector3.up;
		
		beamTargetPoint.x = beamStartPoint.x + Mathf.Sin(weapon.direction) * BeamLength;
		beamTargetPoint.y = beamStartPoint.y + Mathf.Cos(weapon.direction) * BeamLength;

		base.SetBeamPosition();
	}
}

