using UnityEngine;
using System.Collections;

public class Weapon : KJBehavior {
	
	
	public int minLevelBossUse = 0 ;
	public int maxLevelBossUse = 999;
	public bool isBeam = false;
	
	[HideInInspector]
	public float direction = 0.0f;
	
	// Reference to unit
	Entity _entity;
	public Entity entity {
		get {
			if (_entity == null) _entity = GetComponent<Entity>();
			return _entity;
		}	
		
		set {
			_entity = value;
		}
	}
	
	public Unit unit { get; private set; }
	public float Cooldown {
		get {
			return cooldown;
		}
		set {
			cooldown = value;
		}
	}
	
	// Stats
	
	public float damage = 1.0f;
	public float staticDamage = 0.0f;
	
	// Shooting Properties
	
	private float currentCooldown = 0.0f;
	private float cooldown = 1.0f;
	private float startSpeed = 600.0f;
	private float endSpeed = 600.0f;
	private float accelerationFactor = 0.2f;
	private float procTimeMin = 0.0f, procTimeMax = 0.0f;
	private bool isShooting = false;
	private bool isPiercing = false;
	private bool ignoreArmor = false;
	
	// Visual
	
	public WeaponGraphic weaponGraphic;
	private bool isUsingOffset = false;
	
	// Scattering
	
	private int projectilesPerBurst = 1;
	private float scatterFactor = 0.0f;
	private float scatterRange = KJMath.CIRCLE * 0.3f;
	
	// Rapid Shots
	
	private int numberOfShots = 1;
	private float rapidFireGap = 0.10f;
	private int rapidFireShotsLeft;
	private float missRange = 0.0f;
	private float spinDirectionOffset = 0.0f; // Caluclated on runtime.
	private float spinDirectionFactor = 0.0f; // Caluclated on runtime.
	private float currentSpinDirection = 0.0f; // Caluclated on runtime.
	private float spinDirectionRange = 1.5f;
	
	// Homing + Ejection
	
	private bool isHoming = false;
	private bool isEjection = false;
	private float ejectionFactor = 0.0f;
	private float ejectSpeed = 900.0f;
	private float ejectRange;
	private float ejectOffset;
	
	private bool isEngage = false;
	
	// Chainer
	
	private int chainAmount = 0;
	
	//Beam
	
	private float beamTime;

	//Boomerange
	private bool isBoomerang = false;
	private bool isSwirl = false;
	private bool isDoubleHitRadius = false;

	// Buff Effects
	
	public Buff.Type buffType = Buff.Type.None;
	public int buffProcChance = 100;
	public float buffPowerFactor = 0.5f;
	public float buffTime = -1.0f;
	
	// Hit Sound
	public string hitSoundClip = string.Empty;
	
	// Black Hole
	
	public bool isBlackHole = false;
	public int BHradius = 70;
	public float BHtime = 3.0f;
	float projectileTime = 0.0f; // How long it lasts before auto-detonate.
	
	public Entity.Team team {
		get { return entity.team; }	
	}
	
	// Module
	
	public bool isModule = false;
	
	void Awake ()	
	{
		weaponGraphic = new WeaponGraphic();
		ejectRange = KJMath.CIRCLE * 0.35f;
		ejectOffset = (KJMath.CIRCLE - ejectRange) * 0.5f;
	}
	
	public float AdjustedDamage {
		get {
			return damage * entity.stats.damage + staticDamage;	
		}
	}
	
	public void CopyDataFrom (Weapon model)
	{
		damage = model.damage;
		staticDamage = model.staticDamage;
		ejectSpeed = model.ejectSpeed;
		
		isBeam = model.isBeam;
		isUsingOffset = model.isUsingOffset;
		beamTime = model.beamTime;
		
		cooldown = model.cooldown;
		startSpeed = model.startSpeed;
		endSpeed = model.endSpeed;
		accelerationFactor = model.accelerationFactor;
		isPiercing = model.isPiercing;
		ignoreArmor = model.ignoreArmor;
		
		projectilesPerBurst = model.projectilesPerBurst;
		scatterFactor = model.scatterFactor;
		scatterRange = model.scatterRange;
		
		numberOfShots = model.numberOfShots;
		rapidFireGap = model.rapidFireGap;
		rapidFireShotsLeft = model.rapidFireShotsLeft;
		missRange = model.missRange;
		
		isHoming = model.isHoming;
		isEngage = model.isEngage;
		isEjection = model.isEjection;
		ejectionFactor = model.ejectionFactor;
		ejectSpeed = model.ejectSpeed;
		ejectRange = model.ejectRange;
		ejectOffset = (KJMath.CIRCLE - ejectRange) * 0.5f;
		
		chainAmount = model.chainAmount;
		
		isBlackHole = model.isBlackHole;
		BHtime = model.BHtime;
		BHradius = model.BHradius;
		projectileTime = model.projectileTime;
		
		spinDirectionRange = model.spinDirectionRange;
		spinDirectionOffset = model.spinDirectionOffset;
		spinDirectionFactor = model.spinDirectionFactor;
		
		isBoomerang = model.isBoomerang;
		isSwirl = model.isSwirl;
		procTimeMin = model.procTimeMin;
		procTimeMax = model.procTimeMax;
		isDoubleHitRadius = model.isDoubleHitRadius;
		
		
		// Buffs
		
		buffType = model.buffType;
		buffProcChance = model.buffProcChance;
		buffPowerFactor = model.buffPowerFactor;
		buffTime = model.buffTime;
		
		weaponGraphic = model.weaponGraphic;
		hitSoundClip = model.hitSoundClip;
		if (hitSoundClip == string.Empty) hitSoundClip = weaponGraphic.GetNameOfSoundEffect;
		
	}
	
	public void SetIgnoreArmor ()
	{
		this.ignoreArmor = true;
	}
	
	public void SetDamage (float damage, float staticDamage = 0.0f)
	{
		this.damage = damage;
		this.staticDamage = staticDamage;
	}
	
	public void SetStandardVars (float cooldown, float startSpeed, float endSpeed, float accelerationFactor, bool isUsingOffset = false)
	{
		this.cooldown = cooldown;
		this.startSpeed = startSpeed;
		this.endSpeed = endSpeed;
		this.accelerationFactor = accelerationFactor;
		this.isUsingOffset = isUsingOffset;
	}
	
	public void SetRapidVars (int numberOfShots, float rapidFireGap, float missRange, float directionRange = 0.0f)
	{
		this.numberOfShots = numberOfShots;
		this.rapidFireGap = rapidFireGap;
		this.missRange = missRange;
		this.spinDirectionRange = directionRange;
		this.spinDirectionOffset = - directionRange * 0.5f;
		this.spinDirectionFactor = directionRange / numberOfShots;
	}
	
	public void SetScatterVars (int projectilesPerBurst, float scatterRange) 
	{
		this.projectilesPerBurst = projectilesPerBurst;
		this.scatterRange = scatterRange;
	}
	
	public void SetHomingVars ()
	{
		isHoming = true;
	}
	
	public void SetDoubleHitRadius ()
	{
		isDoubleHitRadius = true;
	}
	
	public void SetEjectionVars (float speed = 900.0f)
	{
		ejectSpeed = speed;
		isEjection = true;
	}
	
	public void SetBeamVars (float beamTime = 1.0f)
	{
		this.beamTime = beamTime;
		isBeam = true;
	}
	
	public void SetGraphicVars (WeaponGraphic.Image image, Color startColor, Color endColor, KJEmitter emitter = null)
	{
		if (weaponGraphic == null) weaponGraphic = new WeaponGraphic();
		weaponGraphic.image = image;
		weaponGraphic.startColor = startColor;
		weaponGraphic.endColor = endColor;
		weaponGraphic.hasEmitter = emitter == null ? false : true;
		weaponGraphic.emitterModel = emitter;
	}
	
	public void SetExplosionVars (Color startColor, Color endColor)
	{
		if (weaponGraphic == null) weaponGraphic = new WeaponGraphic();
		weaponGraphic.startColorExplosion = startColor;
		weaponGraphic.endColorExplosion = endColor;
		weaponGraphic.canExplode = true;
	}
	
	public void SetBuff (Buff.Type buffType, int procChance = 100, float buffPowerFactor = 0.75f, float buffTime = 1.25f)
	{
		if (buffType == Buff.Type.Burn) { 
			buffPowerFactor = 0.65f;
			buffTime = 1.6f;
			//			SetIgnoreArmor();
		}
		
		if (buffType == Buff.Type.Acid) { 
			buffTime = 4f;
			//			SetIgnoreArmor();
		}
		this.buffType = buffType;
		this.buffProcChance = procChance;
		this.buffPowerFactor = buffPowerFactor;
		this.buffTime = buffTime;
	}
	
	public void SetChainerVars (int chainAmount)
	{
		this.chainAmount = chainAmount;
	}
	
	public void SetBlackHole (int radius = 70, float time = 3.5f)
	{
		BHtime = time;
		BHradius = radius;
		isBlackHole = true;	
	}
	
	public void SetTime (float time)
	{
		projectileTime = time;	
	}
	
	public void SetPiercing ()
	{
		isPiercing = true;	
	}

	public void SetEngage (float time)
	{
		isEngage = true;
		projectileTime = time;
	}

	public void SetBoomerang (float procTime = 1.0f, float procRange = 0.3f) 
	{
		this.procTimeMin = procTime - procRange;
		this.procTimeMax = procTime + procRange;
		isBoomerang = true;
	}
	
	public void SetSwirl (float procTime = 1.0f, float procRange = 0.3f) 
	{
		this.procTimeMin = procTime - procRange;
		this.procTimeMax = procTime + procRange;
		isSwirl = true;
	}
	
	public void AdjustForBossFactor ()
	{
		
		
		float finalIncreaseFactor = 1.0f + SectorBoss.BossModValue;
		float finalDecreaseFactor = 1.0f - SectorBoss.BossModValue;
		
		rapidFireGap *= finalDecreaseFactor;
		if (!isBeam) cooldown *= finalDecreaseFactor;
		
		if (numberOfShots > 1) numberOfShots = Mathf.FloorToInt(numberOfShots * (1.0f + SectorBoss.BossModValue * 1.5f));
		
		float weaponSpeedFactor = 1.0f + SectorBoss.BossModValue * 0.4f;
		startSpeed *= weaponSpeedFactor;
		endSpeed *= weaponSpeedFactor;
		
	}
	
	void Run ()
	{
		if (currentCooldown > 0.0f)
		{
			currentCooldown -= Time.deltaTime;	
		} else {
			currentCooldown = 0.0f;	
		}
		
		if (currentCooldown == 0.0f)
		{
			if (isShooting)
			{
				if (entity.IsAbleToFire) {
					Fire ();
					currentCooldown = cooldown;
				}
			}
		}
	}
	
	public void Deploy (Unit unit = null)
	{
		this.unit = unit;
		AddTimer(Run);
	}
	
	public void Terminate ()
	{
		StopShooting();
		RemoveAllTimers();
	}
	
	public void StartShooting ()
	{
		if (!isModule) isShooting = true;
	}
	
	public void StopShooting ()
	{
		isShooting = false;
		RemoveTimerOrFlash(ExecuteRapid);
		RemoveTimerOrFlash(OnFinishBurst);
	}
	
	
	public void Fire ()	
	{
		
		if (entity.team == Entity.Team.Enemy) {
			if (unit != null){
				unit.StartShootingTicker();
				unit.StopShootingTicker();
			}
		}
		
		
		if (isBeam)
		{
			if (entity.team == Entity.Team.Enemy) 
			{
				if (unit != null ) unit.beamDir = 0;
				entity.SetBeamLock();
				AddTimer(SpawnBeam, 0.15f, 1);
				return;
			}
			SpawnBeam();
			
			
		} else {
			
			if (numberOfShots > 1)
			{
				StartRapidBurst();
			} else {
				ExecuteBurst(projectilesPerBurst);
			}	
		}
	}
	
	public void StartRapidBurst ()	
	{
		rapidFireShotsLeft = numberOfShots;
		currentSpinDirection = spinDirectionOffset;
		AddTimer (ExecuteRapid, rapidFireGap, rapidFireShotsLeft);
		AddTimer (OnFinishBurst, rapidFireGap * rapidFireShotsLeft, 1);
	}
	
	public void ExecuteRapid ()
	{
		if (entity.IsAbleToFireBasic) {
			currentSpinDirection += spinDirectionFactor;
			ExecuteBurst (projectilesPerBurst);
			rapidFireShotsLeft --;
		}
	}
	
	public void ExecuteBurst (int burstValue = 1, bool runAsExternal = false)
	{	
		
		KJCanary.PlaySoundEffect(hitSoundClip, entity.transform.position, true);
		
		if (isEjection)
		{
			if (burstValue > 1) {
				ejectionFactor = ejectRange / (burstValue - 1);	
			} 
			//			else {
			//				ejectionFactor = ejectRange * Random.value;
			//			}
		}
		
		if (scatterRange != 0.0f)
		{
			if (burstValue > 1) {
				if (scatterRange == KJMath.CIRCLE)
					scatterFactor = scatterRange / (burstValue);
				else 
					scatterFactor = scatterRange / (burstValue - 1);
			}
		}
		
		while (burstValue > 0)
		{
			SpawnProjectile((int) burstValue);
			burstValue --;
		}
	}
	
	void SpawnBeam ()
	{
		entity.isBeamLock = false;
		if (entity.beam == null)
		{		
			/** Beam cannot be null before firing a new Beam. */
			GameObject gameObject = KJActivePool.GetNew("Beam");
			Beam beam = gameObject.GetComponent<Beam>();
			bool isPlayerBeam = false;
			if (entity == GameController.PlayerUnit) isPlayerBeam = true;
			beam.Deploy(entity, this, beamTime, isPlayerBeam);
		}
	}
	
	
	void SpawnProjectile (int shotNumberInSequence)
	{
		
		/** Step 1: Spawn the projectile. */
		
		Projectile projectile = KJActivePool.GetNewOf<Projectile>("Projectile");
		
		if (isUsingOffset)
		{
			projectile.transform.position = entity.GetWeaponOffset();
		} else {
			projectile.transform.position = transform.position;
		}
		
		/** Step 2: Set the direction. */
		
		float projectileDirection = direction;
		
		if (scatterFactor != 0.0f)
		{
			projectileDirection += ( (shotNumberInSequence - 1) * scatterFactor)  - scatterRange * 0.5f;
		}
		
		if (missRange != 0.0f) projectileDirection += Random.Range(-missRange, missRange);
		if (currentSpinDirection != 0.0f) projectileDirection += currentSpinDirection;
		
		projectile.Deploy(this, weaponGraphic, projectile.transform.position, projectileDirection, startSpeed, endSpeed, accelerationFactor, entity.team, projectileTime, isPiercing);
		projectile.SetHoming(isHoming);
		projectile.SetChainer(chainAmount);
		projectile.SetEngage(isEngage);
		projectile.SetBoomerang(isBoomerang);
		projectile.SetSwirl(isSwirl);
		projectile.SetIgnoreArmor(ignoreArmor);
		projectile.SetHitRadius(isDoubleHitRadius);
		
		if (isBoomerang || isSwirl) 
			projectile.SetProcTime(Random.Range(procTimeMin, procTimeMax));
		
		if (unit != null && this == unit.weapon) {
			// Only Fire if from primary weapon.
			unit.modules.OnFireProjectile(projectile);
		}
		
		if (isEjection)
		{
			float ejectDirection;
			if (projectilesPerBurst == 1) {
				ejectDirection = (direction + ejectOffset) + ejectRange * Random.value;
			} else {
				ejectDirection = (direction + ejectOffset) + (ejectionFactor * (shotNumberInSequence - 1));
				
			}
			projectile.EjectWithSpeedAndDirection(ejectSpeed, ejectDirection);
		}
		
	}
	
	void OnFinishBurst ()	
	{
		
	}
	
	//	public void SetUnit (Unit unit = null)
	//	{
	//		this.unit = unit;
	//	}
}
