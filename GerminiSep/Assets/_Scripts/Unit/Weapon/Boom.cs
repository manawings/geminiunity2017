﻿using UnityEngine;
using System.Collections;

public class Boom : KJSpriteBehavior {

	
	public void Deploy (Vector3 position, Color color, float scale = 0.4f)
	{
		transform.position = position;
		FlatScale = scale;
		sprite.color = color;
		animator.PlayFromFrame(0);
		AddTimer(Deactivate, 0.26f, 1);
	}
	
	
}
