using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponPattern
{
	
	private static GameObject holderObject;

	//Ship
	public static Weapon StandardYellow { get; private set; }
	public static Weapon StandardGreen { get; private set; }
	public static Weapon StandardBlue { get; private set; }
	public static Weapon StandardRed { get; private set; }
	public static Weapon StandardPink { get; private set; }

	public static Weapon R_StandardYellow { get; private set; }
	public static Weapon R_StandardGreen { get; private set; }
	public static Weapon R_StandardBlue { get; private set; }
	public static Weapon R_StandardRed { get; private set; }
	public static Weapon R_StandardPink { get; private set; }

	public static Weapon StandardSonar_1 { get; private set; }
	public static Weapon StandardSonar_2 { get; private set; }
	public static Weapon StandardSonar_3 { get; private set; }
	public static Weapon StandardRound { get; private set; }
	public static Weapon StandardSweep { get; private set; }
	public static Weapon StandardShock { get; private set; }
	public static Weapon StandardConah { get; private set; }
	public static Weapon StandardRocket { get; private set; }
	public static Weapon StandardZarall { get; private set; }

	public static Weapon StandardEnergy { get; private set; }
	public static Weapon CorsairEnergy { get; private set; }
	public static Weapon SakuraEnergy { get; private set; }
	public static Weapon EidolonEnergy { get; private set; }
	
	public static Weapon Ship_missile_1 { get; private set; }
	public static Weapon Ship_scatter_1 { get; private set; }
	public static Weapon Ship_scatter_2 { get; private set; }
	public static Weapon Ship_scatter_3 { get; private set; }
	public static Weapon Ship_scatter_4 { get; private set; }
	public static Weapon Ship_missile_green { get; private set; }
	public static Weapon Ship_missile_4 { get; private set; }
	public static Weapon Ship_missile_5 { get; private set; }
	public static Weapon Ship_missile_red { get; private set; }
	public static Weapon Ship_shock_1 { get; private set; }
	public static Weapon Ship_shock_2 { get; private set; }
	public static Weapon Ship_shock_3 { get; private set; }
	public static Weapon Ship_shock_4 { get; private set; }
	public static Weapon Ship_shock_5 { get; private set; }
	public static Weapon Ship_laser_1 { get; private set; }
	public static Weapon Ship_laser_2 { get; private set; }
	public static Weapon Ship_laser_3 { get; private set; }
	public static Weapon Ship_twin_chain_1 { get; private set; }
	public static Weapon Ship_twin_chain_2 { get; private set; }
	public static Weapon p_crystal { get; private set; }
	public static Weapon Ship_missile_blue_2 { get; private set; }
	public static Weapon p_void { get; private set; }
	public static Weapon p_gold { get; private set; }
	public static Weapon Ship_sonar_1 { get; private set; }
	public static Weapon Ship_sonar_2 { get; private set; }
	public static Weapon Ship_sonar_3 { get; private set; }
	public static Weapon Ship_chain_Green { get; private set; }
	public static Weapon Ship_chain_Orange { get; private set; }
	public static Weapon Ship_chain_Red { get; private set; }
	public static Weapon Ship_chain_Twin_Red { get; private set; }
	public static Weapon Ship_chain_Twin_Pink { get; private set; }
	public static Weapon Ship_Oriash { get; private set; }
	public static Weapon Ship_Zarall { get; private set; }
	public static Weapon Ship_Conah_3 { get; private set; }
	public static Weapon Ship_Conah_4 { get; private set; }
	
	public static Weapon ReactorGreen { get; private set; }
	public static Weapon ReactorBlue { get; private set; }
	public static Weapon ReactorYellow { get; private set; }
	public static Weapon ReactorPink { get; private set; }

	// Mixed

	public static Weapon E_Slow { get; private set; }
	public static Weapon E_Slow2 { get; private set; }
	public static Weapon E_Support { get; private set; }
	public static Weapon E_Sweep { get; private set; }
	public static Weapon E_Spread { get; private set; }
	public static Weapon E_Scatter { get; private set; }
	public static Weapon E_Beam { get; private set; }
	public static Weapon E_Homing { get; private set; }
	public static Weapon E_FastHoming { get; private set; }
	public static Weapon E_BlackHole { get; private set; }
	public static Weapon E_Chainer { get; private set; }

	public static Weapon E_BlueStun { get; private set; }
	public static Weapon E_Rapid { get; private set; }
	public static Weapon E_StrikerMissiles { get; private set; }

	public static Weapon E_LaserGreen { get; private set; }
	public static Weapon E_LaserBlue { get; private set; }
	public static Weapon E_LaserRed { get; private set; }
	public static Weapon E_LaserPink { get; private set; }
	public static Weapon E_LaserCirclePink { get; private set; }
	public static Weapon E_LaserCircleGreen { get; private set; }

	//New Weapon
	public static Weapon E_LaserCircleBlue { get; private set; }
	public static Weapon E_LaserCircleRed { get; private set; }
	public static Weapon E_SweepBlue { get; private set; }
	public static Weapon E_SpreadBlue { get; private set; }
	public static Weapon E_ChainerBlue2 { get; private set; }
	public static Weapon E_ChainerRed { get; private set; }
	public static Weapon E_ChainerPurple { get; private set; }
	public static Weapon E_ChainerRed2 { get; private set; }
	public static Weapon E_ChainerPurple2 { get; private set; }
//	public static Weapon E_DirectBeamPurple { get; private set; }
//	public static Weapon E_DirectBeamBlue { get; private set; }
//	public static Weapon E_DirectBeamRed { get; private set; }
//	public static Weapon E_DirectBeamYellow { get; private set; }
	public static Weapon E_StrikerMissilesBlue { get; private set; }
	public static Weapon E_StrikerMissilesRed { get; private set; }
	public static Weapon E_StrikerMissilesPink { get; private set; }
	public static Weapon E_HomingRed { get; private set; }
	public static Weapon E_HomingPurple { get; private set; }
	public static Weapon E_HomingBlue { get; private set; }
	public static Weapon E_Homing2 { get; private set; }
	public static Weapon E_HomingRed2 { get; private set; }
	public static Weapon E_HomingPurple2 { get; private set; }
	public static Weapon E_HomingBlue2 { get; private set; }
	public static Weapon E_SonarRed { get; private set; }
	public static Weapon E_SonarBlue { get; private set; }
	public static Weapon E_SonarPink { get; private set; }
	public static Weapon E_SonarGreen { get; private set; }
	public static Weapon E_DoubleSonarGreen { get; private set; }
	public static Weapon E_DoubleSonarBlue { get; private set; }
	public static Weapon E_DoubleSonarRed { get; private set; }
	public static Weapon E_DoubleSonarPink { get; private set; }

	//Boomerang
	public static Weapon E_BoomerangYellow { get; private set; }
	public static Weapon E_BoomerangGreen { get; private set; }
	public static Weapon E_BoomerangRed { get; private set; }
	public static Weapon E_BoomerangBlue { get; private set; }
	public static Weapon E_BoomerangPink { get; private set; }

	public static Weapon E_BoomerangYellow2 { get; private set; }
	public static Weapon E_BoomerangGreen2 { get; private set; }
	public static Weapon E_BoomerangRed2 { get; private set; }
	public static Weapon E_BoomerangBlue2 { get; private set; }
	public static Weapon E_BoomerangPink2 { get; private set; }

	public static Weapon E_SwirlYellow { get; private set; }
	public static Weapon E_SwirlGreen { get; private set; }
	public static Weapon E_SwirlRed { get; private set; }
	public static Weapon E_SwirlBlue { get; private set; }
	public static Weapon E_SwirlPink { get; private set; }

	public static Weapon E_SwirlYellow2 { get; private set; }
	public static Weapon E_SwirlGreen2 { get; private set; }
	public static Weapon E_SwirlRed2 { get; private set; }
	public static Weapon E_SwirlBlue2 { get; private set; }
	public static Weapon E_SwirlPink2 { get; private set; }

	public static Weapon E_Engage_Y { get; private set; }
	public static Weapon E_Engage_R { get; private set; }
	public static Weapon E_Engage_G { get; private set; }
	public static Weapon E_Engage_B { get; private set; }
	public static Weapon E_Engage_P { get; private set; }

	public static Weapon B_Engage_Y { get; private set; }
	public static Weapon B_Engage_R { get; private set; }
	public static Weapon B_Engage_G { get; private set; }
	public static Weapon B_Engage_B { get; private set; }
	public static Weapon B_Engage_P { get; private set; }

	public static Weapon B_Engage_Y2 { get; private set; }
	public static Weapon B_Engage_R2 { get; private set; }
	public static Weapon B_Engage_G2 { get; private set; }
	public static Weapon B_Engage_B2 { get; private set; }
	public static Weapon B_Engage_P2 { get; private set; }


	//extreme
	public static Weapon E_ScatterSlowGreen { get; private set; }
	public static Weapon E_ScatterSlowBlue { get; private set; }
	public static Weapon E_ScatterSlowRed { get; private set; }
	public static Weapon E_ScatterSlowPink { get; private set; }
	public static Weapon E_SupremeMissilePink { get; private set; }
	public static Weapon E_SupremeMissileRed { get; private set; }

//	public static Weapon E_ShockWaveRed { get; private set; }
//	public static Weapon E_ShockWaveBlue { get; private set; }
//	public static Weapon E_ShockWavePink { get; private set; }
//	public static Weapon E_GatheringYellow { get; private set; }
//	public static Weapon E_GatheringRed { get; private set; }
//	public static Weapon E_GatheringBlue { get; private set; }
//	public static Weapon E_GatheringGreen { get; private set; }

	//Elite
	public static Weapon E_EliteRapid_Easy { get; private set; }
	public static Weapon E_EliteMissile_Easy { get; private set; }
	public static Weapon E_EliteGreenScatter_Easy { get; private set; }
	public static Weapon E_EliteGreenHoming_Easy { get; private set; }
	public static Weapon E_EliteWave_Easy { get; private set; }
	public static Weapon E_EliteSweep_Easy { get; private set; }
	public static Weapon E_EliteGreenLaser_Easy { get; private set; }

	public static Weapon E_EliteRapid { get; private set; }
	public static Weapon E_EliteRapidHard { get; private set; }
	public static Weapon E_EliteRapid2 { get; private set; }
	public static Weapon E_EliteBeam { get; private set; }
	public static Weapon E_EliteMissile { get; private set; }
	public static Weapon E_EliteMissile2 { get; private set; }
	public static Weapon E_EliteGreenScatter { get; private set; }
	public static Weapon E_EliteRedScatter { get; private set; }
	public static Weapon E_EliteBlueScatter { get; private set; }
	public static Weapon E_ElitePinkScatter { get; private set; }
	public static Weapon E_EliteSweep { get; private set; }
	public static Weapon E_EliteSweepHard { get; private set; }
	public static Weapon E_EliteBlueHoming { get; private set; }
	public static Weapon E_EliteRedHoming { get; private set; }
	public static Weapon E_ElitePinkHoming { get; private set; }
	public static Weapon E_EliteGreenHoming { get; private set; }
	public static Weapon E_EliteBlueHoming2 { get; private set; }
	public static Weapon E_EliteRedHoming2 { get; private set; }
	public static Weapon E_EliteGreenHoming2 { get; private set; }
	public static Weapon E_ElitePinkHoming2 { get; private set; }
	public static Weapon E_EliteGreenLaser { get; private set; }
	public static Weapon E_EliteRedLaser { get; private set; }
	public static Weapon E_EliteBlueLaser { get; private set; }
	public static Weapon E_ElitePinkLaser { get; private set; }
	public static Weapon E_EliteWaveGreen { get; private set; }
	public static Weapon E_EliteWaveBlue { get; private set; }
	public static Weapon E_EliteWaveRed { get; private set; }
	public static Weapon E_EliteWavePink { get; private set; }
	public static Weapon E_EliteWaveGreen2 { get; private set; }
	public static Weapon E_EliteWaveBlue2 { get; private set; }
	public static Weapon E_EliteWaveRed2 { get; private set; }
	public static Weapon E_EliteWavePink2 { get; private set; }
	public static Weapon E_EliteRapidHell { get; private set; }

	public static Weapon E_EliteBlueStriker { get; private set; }
	public static Weapon E_EliteRedStriker { get; private set; }


	public static Weapon E_EliteGreenCircle { get; private set; }
	public static Weapon E_EliteBlueCircle { get; private set; }
	public static Weapon E_EliteRedCircle { get; private set; }
	public static Weapon E_ElitePinkCircle { get; private set; }




	// Experimental Weapons
	public static Weapon X_Boomerang { get; private set; }
	public static Weapon X_Swirl { get; private set; }



	public static Weapon PrimaryWeaponElite () {
		List<Weapon> weaponList = new List<Weapon>();
		weaponList.Add(Elite_GatheringRed);
		weaponList.Add(Elite_GatheringBlue);
		weaponList.Add(Elite_GatheringGreen);
		weaponList.Add(Elite_RapidHardBlue);
		weaponList.Add(Elite_RapidHardGreen);
		weaponList.Add(Elite_RapidHardRed);
		weaponList.Add(Elite_SweepHardBlue);
		weaponList.Add(Elite_SweepHardGreen);
		weaponList.Add(Elite_SweepHardRed);
		weaponList.Add(Elite_WaveBlue);
		weaponList.Add(Elite_WaveRed);
		weaponList.Add(Elite_WaveGreen);
		return KJDice.RandomMemberOf<Weapon>(weaponList);
	}

	public static Weapon PrimaryWeaponRival () {
		List<Weapon> weaponList = new List<Weapon>();
		weaponList.Add(R_StandardYellow);
		weaponList.Add(R_StandardRed);
		weaponList.Add(R_StandardGreen);
		weaponList.Add(R_StandardBlue);
		weaponList.Add(R_StandardPink);
		return KJDice.RandomMemberOf<Weapon>(weaponList);
	}

	public static Weapon secondaryWeaponElite () {
		List<Weapon> weaponList = new List<Weapon>();
		weaponList.Add(Elite_BlueHoming);
		weaponList.Add(Elite_RedHoming);
		weaponList.Add(Elite_PinkHoming);
		weaponList.Add(Elite_GreenHoming);
		weaponList.Add(Elite_BlueHoming2);
		weaponList.Add(Elite_RedHoming2);
		weaponList.Add(Elite_GreenHoming2);
		weaponList.Add(Elite_PinkHoming2);
		weaponList.Add(Elite_Missile2_2Red);
		weaponList.Add(Elite_Missile2_2Blue);
		weaponList.Add(Elite_Missile2_2Yellow);
		weaponList.Add(Elite_Missile2_2Pink);
		weaponList.Add(Elite_Missile3Red);
		weaponList.Add(Elite_Missile3Blue);
		weaponList.Add(Elite_Missile3Yellow);
		weaponList.Add(Elite_Missile3Pink);
		weaponList.Add(Elite_BeamBlue);
		weaponList.Add(Elite_BeamRed);
		weaponList.Add(Elite_BeamPink);
		weaponList.Add(Elite_BeamOrange);
		weaponList.Add(Elite_BeamGreen);
		weaponList.Add(Elite_MissileSetRed);
		weaponList.Add(Elite_MissileSetBlue);
		weaponList.Add(Elite_MissileSetYellow);
		weaponList.Add(Elite_MissileSetPink);
		return KJDice.RandomMemberOf<Weapon>(weaponList);
	}

	public static Weapon secondaryWeaponRival () {
		List<Weapon> weaponList = new List<Weapon>();
		weaponList.Add(Rival_BlueHoming);
		weaponList.Add(Rival_RedHoming);
		weaponList.Add(Rival_PinkHoming);
		weaponList.Add(Rival_GreenHoming);
		weaponList.Add(Rival_BlueHoming2);
		weaponList.Add(Rival_RedHoming2);
		weaponList.Add(Rival_GreenHoming2);
		weaponList.Add(Rival_PinkHoming2);
		weaponList.Add(Rival_Missile2_2Red);
		weaponList.Add(Rival_Missile2_2Blue);
		weaponList.Add(Rival_Missile2_2Yellow);
		weaponList.Add(Rival_Missile2_2Pink);
		weaponList.Add(Rival_Missile3Red);
		weaponList.Add(Rival_Missile3Blue);
		weaponList.Add(Rival_Missile3Yellow);
		weaponList.Add(Rival_Missile3Pink);
		weaponList.Add(Rival_BeamBlue);
		weaponList.Add(Rival_BeamRed);
		weaponList.Add(Rival_BeamPink);
		weaponList.Add(Rival_BeamOrange);
		weaponList.Add(Rival_BeamGreen);
		weaponList.Add(Rival_MissileSetRed);
		weaponList.Add(Rival_MissileSetBlue);
		weaponList.Add(Rival_MissileSetYellow);
		weaponList.Add(Rival_MissileSetPink);
		return KJDice.RandomMemberOf<Weapon>(weaponList);
	}

	//Rival Weapon
	public static Weapon Rival_BlueHoming { get; private set; }
	public static Weapon Rival_RedHoming { get; private set; }
	public static Weapon Rival_PinkHoming { get; private set; }
	public static Weapon Rival_GreenHoming { get; private set; }
	public static Weapon Rival_BlueHoming2 { get; private set; }
	public static Weapon Rival_RedHoming2 { get; private set; }
	public static Weapon Rival_GreenHoming2 { get; private set; }
	public static Weapon Rival_PinkHoming2 { get; private set; }
	public static Weapon Rival_Missile2_2Red { get; private set; }
	public static Weapon Rival_Missile2_2Blue { get; private set; }
	public static Weapon Rival_Missile2_2Yellow { get; private set; }
	public static Weapon Rival_Missile2_2Pink { get; private set; }
	public static Weapon Rival_Missile3Red { get; private set; }
	public static Weapon Rival_Missile3Blue { get; private set; }
	public static Weapon Rival_Missile3Yellow { get; private set; }
	public static Weapon Rival_Missile3Pink { get; private set; }
	public static Weapon Rival_BeamBlue { get; private set; }
	public static Weapon Rival_BeamRed { get; private set; }
	public static Weapon Rival_BeamPink { get; private set; }
	public static Weapon Rival_BeamOrange { get; private set; }
	public static Weapon Rival_BeamGreen { get; private set; }
	public static Weapon Rival_MissileSetRed { get; private set; }
	public static Weapon Rival_MissileSetBlue { get; private set; }
	public static Weapon Rival_MissileSetYellow { get; private set; }
	public static Weapon Rival_MissileSetPink { get; private set; }

	//Elite Boss
	public static Weapon Elite_GatheringRed { get; private set; }
	public static Weapon Elite_GatheringBlue { get; private set; }
	public static Weapon Elite_GatheringGreen { get; private set; }
	public static Weapon Elite_RapidHardBlue { get; private set; }
	public static Weapon Elite_RapidHardGreen { get; private set; }
	public static Weapon Elite_RapidHardRed { get; private set; }
	public static Weapon Elite_SweepHardBlue { get; private set; }
	public static Weapon Elite_SweepHardGreen { get; private set; }
	public static Weapon Elite_SweepHardRed { get; private set; }
	public static Weapon Elite_WaveBlue { get; private set; }
	public static Weapon Elite_WaveRed { get; private set; }
	public static Weapon Elite_WaveGreen { get; private set; }

	public static Weapon Elite_BlueHoming { get; private set; }
	public static Weapon Elite_RedHoming { get; private set; }
	public static Weapon Elite_PinkHoming { get; private set; }
	public static Weapon Elite_GreenHoming { get; private set; }
	public static Weapon Elite_BlueHoming2 { get; private set; }
	public static Weapon Elite_RedHoming2 { get; private set; }
	public static Weapon Elite_GreenHoming2 { get; private set; }
	public static Weapon Elite_PinkHoming2 { get; private set; }
	public static Weapon Elite_Missile2_2Red { get; private set; }
	public static Weapon Elite_Missile2_2Blue { get; private set; }
	public static Weapon Elite_Missile2_2Yellow { get; private set; }
	public static Weapon Elite_Missile2_2Pink { get; private set; }
	public static Weapon Elite_Missile3Red { get; private set; }
	public static Weapon Elite_Missile3Blue { get; private set; }
	public static Weapon Elite_Missile3Yellow { get; private set; }
	public static Weapon Elite_Missile3Pink { get; private set; }
	public static Weapon Elite_BeamBlue { get; private set; }
	public static Weapon Elite_BeamRed { get; private set; }
	public static Weapon Elite_BeamPink { get; private set; }
	public static Weapon Elite_BeamOrange { get; private set; }
	public static Weapon Elite_BeamGreen { get; private set; }
	public static Weapon Elite_MissileSetRed { get; private set; }
	public static Weapon Elite_MissileSetBlue { get; private set; }
	public static Weapon Elite_MissileSetYellow { get; private set; }
	public static Weapon Elite_MissileSetPink { get; private set; }

	// Balanced Only
	

	// Bullet Hell Only

	public static Weapon E_BH_Scatter { get; private set; }
	public static Weapon E_BH_Slow { get; private set; }
	public static Weapon E_BH_Rapid { get; private set; }

	// AoE Only

	// BeamHell Only
	public static Weapon E_BeamHell { get; private set; }
	public static Weapon E_BeamStun { get; private set; }
	public static Weapon E_BeamBurn { get; private set; }

	//Boss
	public static Weapon B_Homing { get; private set; }
	public static Weapon B_Scatter { get; private set; }
	public static Weapon B_Chainer { get; private set; }
	public static Weapon B_ShockWave { get; private set; }
	public static Weapon B_Red_ElectricBall {get ; private set;}
	public static Weapon B_Pink_Electricball {get ;private set;}
	public static Weapon B_Blue_Electricball {get ; private set;}
	public static Weapon B_Green_Electricball {get ;private set;}

	public static Weapon B_SpreadStun { get; private set; }
	public static Weapon B_AutoFire { get; private set; }
	public static Weapon B_SlowShockWave { get; private set; }
	public static Weapon B_SlowShockWaveRed { get; private set; }

	public static Weapon B_GreenLaserScatter { get; private set; }
	public static Weapon B_GreenScatter { get; private set; }
	public static Weapon B_PurpleScatter { get; private set; }
	public static Weapon B_RedScatter { get; private set; }
	public static Weapon B_BlueChain { get; private set; }
	public static Weapon B_BlueMissile { get; private set; }
	public static Weapon B_BlueScatter { get; private set; }
	public static Weapon B_PurpleMissile { get; private set; }

	public static Weapon B_BlackHole { get; private set; }
	public static Weapon B_SuperMissile { get; private set; }
	public static Weapon B_SuperMissile2 { get; private set; }
	public static Weapon B_GatheringRed { get; private set; }
	public static Weapon B_GatheringBlue { get; private set; }
	public static Weapon B_GatheringGreen { get; private set; }

	public static Weapon B_ScatterChain4Blue { get; private set; }
	public static Weapon B_ScatterChain4Red { get; private set; }
	public static Weapon B_SonarRed { get; private set; }
	public static Weapon B_SonarBlue { get; private set; }
	public static Weapon B_FastHoming { get; private set; }

	public static Weapon B_RunScatterRed { get; private set; }
	public static Weapon B_RunScatterPink { get; private set; }

	public static Weapon BT_TurretPlasma { get; private set; }
	public static Weapon BT_TurretSlowStun { get; private set; }
	public static Weapon BT_TurretVolleyStun { get; private set; }
	public static Weapon BT_TurretScatterShot { get; private set; }

	public static Weapon BT_SonarRed { get; private set; }
	public static Weapon BT_SonarBlue { get; private set; }
	public static Weapon BT_ScatterChain4Red { get; private set; }
	public static Weapon BT_ScatterChain4Blue { get; private set; }
	public static Weapon BT_HommingRed { get; private set; }
	public static Weapon BT_HommingBlue { get; private set; }
	public static Weapon BT_BeamBlue { get; private set; }
	public static Weapon BT_BeamRed { get; private set; }
	public static Weapon BT_ScatterLaserYellow { get; private set; }
	public static Weapon BT_ScatterLaserGreen { get; private set; }
	public static Weapon BT_HomingSonarGreen { get; private set; }
	public static Weapon BT_HomingSonarYellow { get; private set; }

	public static Weapon Orbita_SpreadBurn { get; private set; }
	public static Weapon Orbita_LaserBurn { get; private set; }
	public static Weapon Orbita_SpreadLaserBurn { get; private set; }
	public static Weapon Orbita_HomingBurn { get; private set; }
	public static Weapon Orbita_RedElectricball2 { get; private set; }
	public static Weapon Orbita_RedElectricball {get ;private set;}
	public static Weapon Orbita_RedSonarSlow {get ;private set;}
	public static Weapon Orbita_RedSonarFast {get ;private set;}

	public static Weapon Arbiter_GreenSupport {get ;private set;}
	public static Weapon Arbiter_RoundSlow {get ;private set;}
	public static Weapon Arbiter_StunSupport {get ;private set;}
	public static Weapon Arbiter_ScatterPink {get ;private set;}
	public static Weapon Arbiter_ScatterBlue {get ;private set;}
	public static Weapon Arbiter_RapidSlow {get ;private set;}
	public static Weapon Arbiter_RapidFast {get ;private set;}

	public static Weapon B_RedScatterLaser {get ;private set;}
	public static Weapon B_BlueScatterLaser {get ;private set;}
	public static Weapon B_PinkScatterLaser {get ;private set;}
	public static Weapon B_GreenScatterLaser {get ;private set;}

	public static Weapon B_DestoryerRedScatterLaser {get ;private set;}
	public static Weapon B_DestoryerBlueScatterLaser {get ;private set;}
	public static Weapon B_DestoryerPinkScatterLaser {get ;private set;}
	public static Weapon B_DestoryerGreenScatterLaser {get ;private set;}

	public static Weapon B_GreenLaser {get ;private set;}
	public static Weapon B_BlueLaser {get ;private set;}
	public static Weapon B_RedLaser {get ;private set;}
	public static Weapon B_PinkLaser {get ;private set;}

	public static Weapon B_DestoryerBlueMissile {get ;private set;}
	
	public static Weapon M_CounterScatter { get; private set; }
	public static Weapon M_CounterMissile_4 { get; private set; }
	public static Weapon M_GreenLaserScatter { get; private set; }
	public static Weapon M_RedLaserScatter { get; private set; }
	public static Weapon M_BlueLaserScatter { get; private set; }
	public static Weapon M_PurpleLaserScatter { get; private set; }
	public static Weapon M_GreenLaser { get; private set; }
	public static Weapon M_RedLaser { get; private set; }
	public static Weapon M_BlueLaser { get; private set; }
	public static Weapon M_PurpleLaser { get; private set; }
	public static Weapon M_GreenMissile { get; private set; }
	public static Weapon M_BlueMissile { get; private set; }
	public static Weapon M_RedMissile { get; private set; }
	public static Weapon M_PurpleMissile { get; private set; }
	public static Weapon M_GreenChain { get; private set; }
	public static Weapon M_BlueChain { get; private set; }
	public static Weapon M_RedChain { get; private set; }
	public static Weapon M_PurpleChain { get; private set; }
	public static Weapon M_GreenScatter { get; private set; }
	public static Weapon M_BlueScatter { get; private set; }
	public static Weapon M_RedScatter { get; private set; }
	public static Weapon M_PurpleScatter { get; private set; }
	public static Weapon M_BlackHole { get; private set; }
	public static Weapon M_UltimateMissile { get; private set; }
	public static Weapon M_UltimateScatter { get; private set; }
	public static Weapon M_UltimateChain { get; private set; }
	public static Weapon M_UltimateLaser { get; private set; }
	public static Weapon M_SonarGreen { get; private set; }
	public static Weapon M_SonarBlue { get; private set; }
	public static Weapon M_SonarRed { get; private set; }
	public static Weapon M_SonarPink { get; private set; }
	public static Weapon M_UltimateSonar { get; private set; }

	public static Weapon Dreadnought_Core1 { get; private set; }
	public static Weapon Dreadnought_Core2 { get; private set; }
	public static Weapon Dreadnought_Core3 { get; private set; }
	public static Weapon Dreadnought_Core4 { get; private set; }
	public static Weapon Dreadnought_Core5 { get; private set; }
	public static Weapon Dreadnought_Core6 { get; private set; }
	public static Weapon Dreadnought_Core7 { get; private set; }
	public static Weapon Dreadnought_Core8 { get; private set; }
	public static Weapon Dreadnought_Core9 { get; private set; }
	public static Weapon Dreadnought_Core10 { get; private set; }

	public static Weapon Dreadnought_Core11 { get; private set; }
	public static Weapon Dreadnought_Core12 { get; private set; }
	public static Weapon Dreadnought_Core13 { get; private set; }
	public static Weapon Dreadnought_Core14 { get; private set; }
	public static Weapon Dreadnought_Core15 { get; private set; }
	public static Weapon Dreadnought_Core16 { get; private set; }
	public static Weapon Dreadnought_Core17 { get; private set; }
	public static Weapon Dreadnought_Core18 { get; private set; }
	public static Weapon Dreadnought_Core19 { get; private set; }
	public static Weapon Dreadnought_Core20 { get; private set; }

	public static Weapon Dreadnought_Core21 { get; private set; }
	public static Weapon Dreadnought_Core22 { get; private set; }
	public static Weapon Dreadnought_Core23 { get; private set; }
	public static Weapon Dreadnought_Core24 { get; private set; }
	public static Weapon Dreadnought_Core25 { get; private set; }
	public static Weapon Dreadnought_Core26 { get; private set; }
	public static Weapon Dreadnought_Core27 { get; private set; }
	public static Weapon Dreadnought_Core28 { get; private set; }
	public static Weapon Dreadnought_Core29 { get; private set; }
	public static Weapon Dreadnought_Core30 { get; private set; }

	public static Weapon Dreadnought_Core31 { get; private set; }
	public static Weapon Dreadnought_Core32 { get; private set; }
	public static Weapon Dreadnought_Core33 { get; private set; }
	public static Weapon Dreadnought_Core34 { get; private set; }
	public static Weapon Dreadnought_Core35 { get; private set; }
	public static Weapon Dreadnought_Core36 { get; private set; }
	public static Weapon Dreadnought_Core37 { get; private set; }
	public static Weapon Dreadnought_Core38 { get; private set; }
	public static Weapon Dreadnought_Core39 { get; private set; }
	public static Weapon Dreadnought_Core40 { get; private set; }

	public static Weapon Dreadnought_Turret1 { get; private set; }
	public static Weapon Dreadnought_Turret2 { get; private set; }
	public static Weapon Dreadnought_Turret3 { get; private set; }
	public static Weapon Dreadnought_Turret4 { get; private set; }
	public static Weapon Dreadnought_Turret5 { get; private set; }
	public static Weapon Dreadnought_Turret6 { get; private set; }
	public static Weapon Dreadnought_Turret7 { get; private set; }
	public static Weapon Dreadnought_Turret8 { get; private set; }
	public static Weapon Dreadnought_Turret9 { get; private set; }
	public static Weapon Dreadnought_Turret10 { get; private set; }
	public static Weapon Dreadnought_Turret11 { get; private set; }
	public static Weapon Dreadnought_Turret12 { get; private set; }
	public static Weapon Dreadnought_Turret13 { get; private set; }
	public static Weapon Dreadnought_Turret14 { get; private set; }
	public static Weapon Dreadnought_Turret15 { get; private set; }
	public static Weapon Dreadnought_Turret16 { get; private set; }
	public static Weapon Dreadnought_Turret17 { get; private set; }
	public static Weapon Dreadnought_Turret18 { get; private set; }
	public static Weapon Dreadnought_Turret19 { get; private set; }
	public static Weapon Dreadnought_Turret20 { get; private set; }

	public static Weapon Dreadnought_Turret21 { get; private set; }
	public static Weapon Dreadnought_Turret22 { get; private set; }
	public static Weapon Dreadnought_Turret23 { get; private set; }
	public static Weapon Dreadnought_Turret24 { get; private set; }
	public static Weapon Dreadnought_Turret25 { get; private set; }
	public static Weapon Dreadnought_Turret26 { get; private set; }
	public static Weapon Dreadnought_Turret27 { get; private set; }
	public static Weapon Dreadnought_Turret28 { get; private set; }
	public static Weapon Dreadnought_Turret29 { get; private set; }
	public static Weapon Dreadnought_Turret30 { get; private set; }
	public static Weapon Dreadnought_Turret31 { get; private set; }
	public static Weapon Dreadnought_Turret32 { get; private set; }
	public static Weapon Dreadnought_Turret33 { get; private set; }
	public static Weapon Dreadnought_Turret34 { get; private set; }
	public static Weapon Dreadnought_Turret35 { get; private set; }
	public static Weapon Dreadnought_Turret36 { get; private set; }
	public static Weapon Dreadnought_Turret37 { get; private set; }
	public static Weapon Dreadnought_Turret38 { get; private set; }
	public static Weapon Dreadnought_Turret39 { get; private set; }
	public static Weapon Dreadnought_Turret40 { get; private set; }

	public static Weapon Dreadnought_Turret41 { get; private set; }
	public static Weapon Dreadnought_Turret42 { get; private set; }
	public static Weapon Dreadnought_Turret43 { get; private set; }
	public static Weapon Dreadnought_Turret44 { get; private set; }
	public static Weapon Dreadnought_Turret45 { get; private set; }
	public static Weapon Dreadnought_Turret46 { get; private set; }
	public static Weapon Dreadnought_Turret47 { get; private set; }
	public static Weapon Dreadnought_Turret48 { get; private set; }
	public static Weapon Dreadnought_Turret49 { get; private set; }
	public static Weapon Dreadnought_Turret50 { get; private set; }
	public static Weapon Dreadnought_Turret51 { get; private set; }
	public static Weapon Dreadnought_Turret52 { get; private set; }
	public static Weapon Dreadnought_Turret53 { get; private set; }
	public static Weapon Dreadnought_Turret54 { get; private set; }
	public static Weapon Dreadnought_Turret55 { get; private set; }
	public static Weapon Dreadnought_Turret56 { get; private set; }
	public static Weapon Dreadnought_Turret57 { get; private set; }
	public static Weapon Dreadnought_Turret58 { get; private set; }
	public static Weapon Dreadnought_Turret59 { get; private set; }
	public static Weapon Dreadnought_Turret60 { get; private set; }

	public static Weapon Dreadnought_Turret61 { get; private set; }
	public static Weapon Dreadnought_Turret62 { get; private set; }
	public static Weapon Dreadnought_Turret63 { get; private set; }
	public static Weapon Dreadnought_Turret64 { get; private set; }
	public static Weapon Dreadnought_Turret65 { get; private set; }
	public static Weapon Dreadnought_Turret66 { get; private set; }
	public static Weapon Dreadnought_Turret67 { get; private set; }
	public static Weapon Dreadnought_Turret68 { get; private set; }
	public static Weapon Dreadnought_Turret69 { get; private set; }
	public static Weapon Dreadnought_Turret70 { get; private set; }
	public static Weapon Dreadnought_Turret71 { get; private set; }
	public static Weapon Dreadnought_Turret72 { get; private set; }
	public static Weapon Dreadnought_Turret73 { get; private set; }
	public static Weapon Dreadnought_Turret74 { get; private set; }
	public static Weapon Dreadnought_Turret75 { get; private set; }
	public static Weapon Dreadnought_Turret76 { get; private set; }
	public static Weapon Dreadnought_Turret77 { get; private set; }
	public static Weapon Dreadnought_Turret78 { get; private set; }
	public static Weapon Dreadnought_Turret79 { get; private set; }
	public static Weapon Dreadnought_Turret80 { get; private set; }

	public static Weapon Dreadnought_Turret_Swirl1 { get; private set; }
	public static Weapon Dreadnought_Turret_Swirl2 { get; private set; }
	public static Weapon Dreadnought_Turret_Swirl3 { get; private set; }
	public static Weapon Dreadnought_Turret_Swirl4 { get; private set; }
	public static Weapon Dreadnought_Turret_Swirl5 { get; private set; }
	public static Weapon Dreadnought_Turret_Swirl6 { get; private set; }
	public static Weapon Dreadnought_Turret_Swirl7 { get; private set; }
	public static Weapon Dreadnought_Turret_Swirl8 { get; private set; }
	public static Weapon Dreadnought_Turret_Swirl9 { get; private set; }
	public static Weapon Dreadnought_Turret_Swirl10 { get; private set; }

	public static Weapon Dreadnought_Turret_Boomerang1 { get; private set; }
	public static Weapon Dreadnought_Turret_Boomerang2 { get; private set; }
	public static Weapon Dreadnought_Turret_Boomerang3 { get; private set; }
	public static Weapon Dreadnought_Turret_Boomerang4 { get; private set; }
	public static Weapon Dreadnought_Turret_Boomerang5 { get; private set; }
	public static Weapon Dreadnought_Turret_Boomerang6 { get; private set; }
	public static Weapon Dreadnought_Turret_Boomerang7 { get; private set; }
	public static Weapon Dreadnought_Turret_Boomerang8 { get; private set; }
	public static Weapon Dreadnought_Turret_Boomerang9 { get; private set; }
	public static Weapon Dreadnought_Turret_Boomerang10 { get; private set; }

	static bool hasBeenInit = false;
	
	public static void Initialize ()
	{
		if (holderObject != null) return;

		Color yellowStart = new Color (1.0f, 0.4f, 0.1f, 1.0f);
		Color yellowEnd = new Color (1.0f, 0.1f, 0.0f, 1.0f);

		Color redStart = new Color (1.0f, 0.3f, 0.2f, 1.0f);
		Color redEnd = new Color (1.0f, 0.0f, 0.0f, 1.0f);

		Color blueStart = new Color (0.0f, 0.3f, 1.0f, 1.0f);
		Color blueEnd = new Color (0.0f, 0.0f, 1.0f, 1.0f);

		Color greenStart = new Color (0.4f, 1.0f, 0.1f, 1.0f);
		Color greenEnd = new Color (0.0f, 0.8f, 0.2f, 1.0f);

		Color pinkStart = new Color (1.0f, 0.4f, 0.9f, 1.0f);
		Color pinkEnd = new Color (0.3f, 0.0f, 0.7f, 1.0f);

		holderObject = new GameObject("Weapon Pool");
		Object.DontDestroyOnLoad(holderObject);
		
		EffectController.Singleton.CheckAndCreate();

		//Ship
		StandardYellow = holderObject.AddComponent<Weapon>();
		StandardYellow.SetStandardVars(0.07f, 800.0f, 600.0f, 0.15f, true);
		StandardYellow.SetGraphicVars(WeaponGraphic.Image.StandardYellow, yellowStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));

		StandardGreen = holderObject.AddComponent<Weapon>();
		StandardGreen.SetStandardVars(0.025f, 900.0f, 700.0f, 0.15f, true);
		StandardGreen.SetDamage(0.4f);
		StandardGreen.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));

		StandardRed = holderObject.AddComponent<Weapon>();
		StandardRed.SetStandardVars(0.10f, 750.0f, 550.0f, 0.15f, true);
		StandardRed.SetDamage(1.25f);
		StandardRed.SetGraphicVars(WeaponGraphic.Image.StandardRed, redStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));

		StandardBlue = holderObject.AddComponent<Weapon>();
		StandardBlue.SetStandardVars(0.06f, 850.0f, 650.0f, 0.15f, true);
		StandardBlue.SetDamage(0.85f);
		StandardBlue.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));

		StandardPink = holderObject.AddComponent<Weapon>();
		StandardPink.SetStandardVars(0.07f, 800.0f, 600.0f, 0.15f, true);
		StandardPink.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		StandardPink.SetDamage(1.1f);

		StandardEnergy = holderObject.AddComponent<Weapon>();
		StandardEnergy.SetStandardVars(0.10f, 850.0f, 650.0f, 0.15f, true);
		StandardEnergy.SetDamage(1.35f);
		StandardEnergy.SetGraphicVars(WeaponGraphic.Image.StandardYellow, yellowStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));

		EidolonEnergy = holderObject.AddComponent<Weapon>();
		EidolonEnergy.SetStandardVars(0.10f, 850.0f, 650.0f, 0.15f, true);
		EidolonEnergy.SetDamage(1.35f);
		EidolonEnergy.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));

		SakuraEnergy = holderObject.AddComponent<Weapon>();
		SakuraEnergy.SetStandardVars(0.03f, 900.0f, 700.0f, 0.15f, true);
		SakuraEnergy.SetDamage(0.6f);
		SakuraEnergy.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));

		StandardRound = holderObject.AddComponent<Weapon>();
		StandardRound.SetStandardVars(0.2f, 601.0f, 600.0f, 0.2f, true);
		StandardRound.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, new Color(1.0f, 0.7f, 0.0f, 0.7f));
		StandardRound.SetDamage(1.75f);

		StandardShock = holderObject.AddComponent<Weapon>();
		StandardShock.SetStandardVars(0.3f, 601.0f, 600.0f, 0.2f, false);
		StandardShock.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(0.2f, 0.0f, 0.9f, 0.7f), EffectController.Singleton.waveTrail);
		StandardShock.SetHomingVars();
		StandardShock.SetDamage(1.0f);

		StandardSonar_1 = holderObject.AddComponent<Weapon>();
		StandardSonar_1.SetStandardVars(0.5f, 350.0f, 550.0f, 0.2f, false);
		StandardSonar_1.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.4f, 0.7f, 0.0f), EffectController.Singleton.lightSonarTrail);
		StandardSonar_1.SetPiercing();
		StandardSonar_1.SetBuff(Buff.Type.Stun);
		StandardSonar_1.SetDamage(6.0f);

		StandardSonar_2 = holderObject.AddComponent<Weapon>();
		StandardSonar_2.SetStandardVars(0.28f, 350.0f, 550.0f, 0.2f, false);
		StandardSonar_2.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, yellowStart, new Color(1.0f, 0.4f, 0.7f, 0.0f), EffectController.Singleton.lightSonarTrail);
		StandardSonar_2.SetPiercing();
		StandardSonar_2.SetBuff(Buff.Type.Stun, 45, 1, 0.5f);
		StandardSonar_2.SetDamage(1.0f);

		StandardSonar_3 = holderObject.AddComponent<Weapon>();
		StandardSonar_3.SetStandardVars(0.6f, 350.0f, 550.0f, 0.2f, false);
		StandardSonar_3.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color(1.0f, 0.4f, 0.7f, 0.0f), EffectController.Singleton.lightSonarTrail);
		StandardSonar_3.SetPiercing();
		StandardSonar_3.SetDamage(10.0f);

		StandardSweep = holderObject.AddComponent<Weapon>();
		StandardSweep.SetStandardVars(0.07f, 750.0f, 650.0f, 0.2f);
		StandardSweep.SetDamage(1.0f);
		StandardSweep.SetGraphicVars(WeaponGraphic.Image.RoundGreen, greenStart, Color.white);
		StandardSweep.SetRapidVars(1, 0.01f, 0.35f, 0.5f);

		StandardConah = holderObject.AddComponent<Weapon>();
		StandardConah.SetStandardVars(0.07f, 701.0f, 700.0f, 0.2f, true);
		StandardConah.SetDamage(1.5f);
		StandardConah.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, Color.white);

		StandardRocket = holderObject.AddComponent<Weapon>();
		StandardRocket.SetStandardVars(0.21f, 501.0f, 500.0f, 0.2f);
		StandardRocket.SetDamage(5f);
		StandardRocket.SetGraphicVars(WeaponGraphic.Image.MissileYellow, yellowStart, redStart);

		StandardZarall = holderObject.AddComponent<Weapon>();
		StandardZarall.SetStandardVars(0.3f, 701.0f, 700.0f, 0.2f, false);
		StandardZarall.SetDamage(2.5f);
		StandardZarall.SetScatterVars(3, 0.3f);
		StandardZarall.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, Color.white);

		//Standard.SetBuff(Buff.Type.Acid, 100, 1.0f, 5.0f);
		// Standard.SetBuff(Buff.Type.Stun, 100);
		//Standard.SetTime(0.75f);
		//Standard.SetBlackHole();
		// Standard.SetPiercing();

		Ship_missile_1 = holderObject.AddComponent<Weapon>();
		Ship_missile_1.SetStandardVars(1.0f, 0.0f, 1000.0f, 0.10f, false);
		Ship_missile_1.SetGraphicVars(WeaponGraphic.Image.MissileYellow, yellowStart, new Color(1.0f, 0.5f, 0.0f, 0.65f), EffectController.Singleton.missileTrail);
		Ship_missile_1.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Ship_missile_1.SetHomingVars();
		Ship_missile_1.SetDamage(1.0f);

		Ship_scatter_1 = holderObject.AddComponent<Weapon>();
		Ship_scatter_1.SetStandardVars(2.5f, 501.0f, 300.0f, 0.2f);
		Ship_scatter_1.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(1.0f, 0.6f, 0.1f, 1.0f));
		Ship_scatter_1.SetDamage(1.0f);
		Ship_scatter_1.SetScatterVars(10, KJMath.CIRCLE * 0.25f);
		Ship_scatter_1.SetRapidVars(1, 0.25f, 0.0f);
		Ship_scatter_1.SetBuff(Buff.Type.Acid);
		Ship_scatter_1.SetDamage(1.0f);

		Ship_scatter_2 = holderObject.AddComponent<Weapon>();
		Ship_scatter_2.SetStandardVars(2.5f, 501.0f, 300.0f, 0.2f);
		Ship_scatter_2.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(1.0f, 0.6f, 0.1f, 1.0f));
		Ship_scatter_2.SetDamage(1.0f);
		Ship_scatter_2.SetScatterVars(10, KJMath.CIRCLE * 0.25f);
		Ship_scatter_2.SetRapidVars(1, 0.25f, 0.0f);
		Ship_scatter_2.SetBuff(Buff.Type.Acid);
		Ship_scatter_2.SetDamage(1.0f);

		Ship_scatter_3 = holderObject.AddComponent<Weapon>();
		Ship_scatter_3.SetStandardVars(2.5f, 501.0f, 300.0f, 0.2f);
		Ship_scatter_3.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(1.0f, 0.6f, 0.1f, 1.0f));
		Ship_scatter_3.SetDamage(1.0f);
		Ship_scatter_3.SetScatterVars(7, KJMath.CIRCLE * 0.25f);
		Ship_scatter_3.SetRapidVars(2, 0.25f, 0.0f);
		Ship_scatter_3.SetBuff(Buff.Type.Acid);
		Ship_scatter_3.SetDamage(1.0f);

		Ship_scatter_4 = holderObject.AddComponent<Weapon>();
		Ship_scatter_4.SetStandardVars(2.5f, 501.0f, 300.0f, 0.2f);
		Ship_scatter_4.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(1.0f, 0.6f, 0.1f, 1.0f));
		Ship_scatter_4.SetDamage(1.0f);
		Ship_scatter_4.SetScatterVars(8, KJMath.CIRCLE * 0.25f);
		Ship_scatter_4.SetRapidVars(2, 0.25f, 0.0f);
		Ship_scatter_4.SetBuff(Buff.Type.Acid);
		Ship_scatter_4.SetDamage(1.0f);

		Ship_missile_green = holderObject.AddComponent<Weapon>();
		Ship_missile_green.SetStandardVars(1.0f, 0.0f, 1000.0f, 0.10f, false);
		Ship_missile_green.SetGraphicVars(WeaponGraphic.Image.MissileGreen, greenStart, new Color(0.0f, 0.8f, 0.2f, 1.00f), EffectController.Singleton.missileTrail);
		Ship_missile_green.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Ship_missile_green.SetHomingVars();
		Ship_missile_green.SetDamage(1.0f);

		Ship_missile_4 = holderObject.AddComponent<Weapon>();
		Ship_missile_4.SetStandardVars(1.0f, 0.0f, 1000.0f, 0.10f, false);
		Ship_missile_4.SetGraphicVars(WeaponGraphic.Image.MissileYellow, yellowStart, new Color(1.0f, 0.45f, 0.05f, 1.00f), EffectController.Singleton.missileTrail);
		Ship_missile_4.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Ship_missile_4.SetHomingVars();
		Ship_missile_4.SetDamage(1.0f);

		Ship_missile_5 = holderObject.AddComponent<Weapon>();
		Ship_missile_5.SetStandardVars(1.0f, 0.0f, 1000.0f, 0.10f, false);
		Ship_missile_5.SetGraphicVars(WeaponGraphic.Image.MissilePink, pinkStart, new Color(0.0f, 0.7f, 0.1f, 1.00f), EffectController.Singleton.missileTrail);
		Ship_missile_5.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Ship_missile_5.SetHomingVars();
		Ship_missile_5.SetDamage(1.0f);

		Ship_missile_red = holderObject.AddComponent<Weapon>();
		Ship_missile_red.SetStandardVars(1.0f, 0.0f, 1000.0f, 0.10f, false);
		Ship_missile_red.SetGraphicVars(WeaponGraphic.Image.MissileGreen, greenStart, new Color(1.0f, 0.2f, 0.2f, 1.00f), EffectController.Singleton.missileTrail);
		Ship_missile_red.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Ship_missile_red.SetHomingVars();
		Ship_missile_red.SetBuff(Buff.Type.Burn);
		Ship_missile_red.SetDamage(1.0f);

		Ship_chain_Green = holderObject.AddComponent<Weapon>();
		Ship_chain_Green.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, false);
		Ship_chain_Green.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.lightShockTrail);
		Ship_chain_Green.SetDamage(1.0f);
		Ship_chain_Green.SetChainerVars(2);
		Ship_chain_Green.SetHomingVars();

		Ship_chain_Red = holderObject.AddComponent<Weapon>();
		Ship_chain_Red.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, false);
		Ship_chain_Red.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color(1.0f, 0.0f, 0.3f, 1.0f), EffectController.Singleton.lightShockTrail);
		Ship_chain_Red.SetDamage(1.0f);
		Ship_chain_Red.SetChainerVars(2);
		Ship_chain_Red.SetHomingVars();

		Ship_chain_Orange = holderObject.AddComponent<Weapon>();
		Ship_chain_Orange.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, false);
		Ship_chain_Orange.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, yellowStart, new Color(1.0f, 0.5f, 0.1f, 1.0f), EffectController.Singleton.lightShockTrail);
		Ship_chain_Orange.SetDamage(1.0f);
		Ship_chain_Orange.SetChainerVars(2);
		Ship_chain_Orange.SetHomingVars();

		Ship_twin_chain_1 = holderObject.AddComponent<Weapon>();
		Ship_twin_chain_1.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, true);
		Ship_twin_chain_1.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color(1.0f, 0.2f, 0.0f, 0.65f), EffectController.Singleton.lightShockTrail);
		Ship_twin_chain_1.SetDamage(1.0f);
		Ship_twin_chain_1.SetEjectionVars();
		Ship_twin_chain_1.SetRapidVars(1, 0.25f, 0.0f);
		Ship_twin_chain_1.SetScatterVars(2, 0.0f);
		Ship_twin_chain_1.SetChainerVars(2);
		Ship_twin_chain_1.SetHomingVars();

		Ship_twin_chain_2 = holderObject.AddComponent<Weapon>();
		Ship_twin_chain_2.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, true);
		Ship_twin_chain_2.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color(0.6f, 0.2f, 0.9f, 0.65f), EffectController.Singleton.lightShockTrail);
		Ship_twin_chain_2.SetDamage(1.0f);
		Ship_twin_chain_2.SetEjectionVars();
		Ship_twin_chain_2.SetRapidVars(1, 0.25f, 0.0f);
		Ship_twin_chain_2.SetScatterVars(2, 0.0f);
		Ship_twin_chain_2.SetChainerVars(2);
		Ship_twin_chain_2.SetHomingVars();
		
		Ship_shock_1 = holderObject.AddComponent<Weapon>();
		Ship_shock_1.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, false);
		Ship_shock_1.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.2f, 0.7f, 0.2f, 1.00f), EffectController.Singleton.lightShockTrail);
		Ship_shock_1.SetDamage(1.0f);
		Ship_shock_1.SetChainerVars(2);
		Ship_shock_1.SetHomingVars();

		Ship_shock_2 = holderObject.AddComponent<Weapon>();
		Ship_shock_2.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, false);
		Ship_shock_2.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, yellowStart, new Color(1.0f, 0.5f, 0.0f, 1.00f), EffectController.Singleton.lightShockTrail);
		Ship_shock_2.SetDamage(1.0f);
		Ship_shock_2.SetChainerVars(2);
		Ship_shock_2.SetHomingVars();

		Ship_shock_3 = holderObject.AddComponent<Weapon>();
		Ship_shock_3.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, false);
		Ship_shock_3.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color(1.0f, 0.2f, 0.0f, 1.00f), EffectController.Singleton.lightShockTrail);
		Ship_shock_3.SetDamage(1.0f);
		Ship_shock_3.SetChainerVars(2);
		Ship_shock_3.SetHomingVars();

		Ship_shock_4 = holderObject.AddComponent<Weapon>();
		Ship_shock_4.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, false);
		Ship_shock_4.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.25f, 0.7f, 1.0f, 1.0f), EffectController.Singleton.lightShockTrail);
		Ship_shock_4.SetDamage(1.0f);
		Ship_shock_4.SetChainerVars(2);
		Ship_shock_4.SetHomingVars();

		Ship_shock_5 = holderObject.AddComponent<Weapon>();
		Ship_shock_5.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, false);
		Ship_shock_5.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color(1.0f, 0.2f, 1.0f, 1.00f), EffectController.Singleton.lightShockTrail);
		Ship_shock_5.SetDamage(1.0f);
		Ship_shock_5.SetChainerVars(2);
		Ship_shock_5.SetHomingVars();

		Ship_sonar_1 = holderObject.AddComponent<Weapon>();
		Ship_sonar_1.SetStandardVars(0.07f, 350.0f, 550.0f, 0.2f, false);
		Ship_sonar_1.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(1.0f, 0.2f, 1.0f, 0.10f), EffectController.Singleton.sonarTrail);
		Ship_sonar_1.SetPiercing();
		Ship_sonar_1.SetDamage(1.0f);

		Ship_sonar_2 = holderObject.AddComponent<Weapon>();
		Ship_sonar_2.SetStandardVars(0.07f, 350.0f, 550.0f, 0.2f, false);
		Ship_sonar_2.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(1.0f, 0.2f, 1.0f, 0.10f), EffectController.Singleton.sonarTrail);
		Ship_sonar_2.SetPiercing();
		Ship_sonar_2.SetDamage(1.0f);

		Ship_sonar_3 = holderObject.AddComponent<Weapon>();
		Ship_sonar_3.SetStandardVars(0.07f, 350.0f, 550.0f, 0.2f, false);
		Ship_sonar_3.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(1.0f, 0.2f, 1.0f, 0.10f), EffectController.Singleton.sonarTrail);
		Ship_sonar_3.SetPiercing();
		Ship_sonar_3.SetDamage(1.0f);
		
		Ship_missile_blue_2 = holderObject.AddComponent<Weapon>();
		Ship_missile_blue_2.SetStandardVars(0.07f, 0.0f, 800.0f, 0.2f, true);
		Ship_missile_blue_2.SetGraphicVars(WeaponGraphic.Image.MissileBlue, blueStart, new Color(0.0f, 0.2f, 1.0f, 1.00f), EffectController.Singleton.missileTrail);
		Ship_missile_blue_2.SetDamage(1.0f);
		Ship_missile_blue_2.SetEjectionVars();
		Ship_missile_blue_2.SetRapidVars(1, 0.25f, 0.0f);
		Ship_missile_blue_2.SetScatterVars(2, 0.0f);
		Ship_missile_blue_2.SetBuff(Buff.Type.Stun, 100, 1.0f, 1.0f);
		Ship_missile_blue_2.SetHomingVars();

		Ship_laser_1 = holderObject.AddComponent<Weapon>();
		Ship_laser_1.SetStandardVars(2.5f, 1500.0f, 900.0f, 0.2f);
		Ship_laser_1.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(0.7f, 0.0f, 0.7f, 0.8f), EffectController.Singleton.lineTrail2);
		Ship_laser_1.SetDamage(1.0f);
		Ship_laser_1.SetPiercing();
		//Ship_laser_1.SetScatterVars(8, KJMath.CIRCLE * 0.25f);
		Ship_laser_1.SetRapidVars(2, 0.50f, 0.0f);

		Ship_laser_2 = holderObject.AddComponent<Weapon>();
		Ship_laser_2.SetStandardVars(2.5f, 1500.0f, 900.0f, 0.2f);
		Ship_laser_2.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(0.7f, 0.0f, 0.7f, 0.8f), EffectController.Singleton.lineTrail2);
		Ship_laser_2.SetDamage(1.0f);
		//Ship_laser_2.SetScatterVars(8, KJMath.CIRCLE * 0.25f);
		Ship_laser_2.SetRapidVars(2, 0.50f, 0.0f);

		Ship_laser_3 = holderObject.AddComponent<Weapon>();
		Ship_laser_3.SetStandardVars(2.5f, 1500.0f, 900.0f, 0.2f);
		Ship_laser_3.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(0.0f, 0.0f, 0.7f, 0.8f), EffectController.Singleton.lineTrail2);
		Ship_laser_3.SetDamage(1.0f);
		//Ship_laser_3.SetScatterVars(8, KJMath.CIRCLE * 0.25f);
		Ship_laser_3.SetRapidVars(2, 0.50f, 0.0f);

		Ship_Oriash = holderObject.AddComponent<Weapon>();
		Ship_Oriash.SetStandardVars(1.5f, 0.0f, 800.0f, 0.10f, false);
		Ship_Oriash.SetGraphicVars(WeaponGraphic.Image.MissilePink, pinkStart, pinkEnd, EffectController.Singleton.missileTrail);
		Ship_Oriash.SetHomingVars();
		Ship_Oriash.SetEjectionVars();
		Ship_Oriash.SetExplosionVars(pinkStart, pinkEnd);
		Ship_Oriash.SetScatterVars(2, 0.0f);

		Ship_Zarall = holderObject.AddComponent<Weapon>();
		Ship_Zarall.SetStandardVars(0.07f, 1200.0f, 900.0f, 0.2f, false);
		Ship_Zarall.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, pinkEnd, EffectController.Singleton.lightShockTrail);
		Ship_Zarall.SetDamage(1.0f);
		Ship_Zarall.SetEjectionVars();
		Ship_Zarall.SetScatterVars(2, 0.0f);
		Ship_Zarall.SetHomingVars();

		Ship_Conah_3 = holderObject.AddComponent<Weapon>();
		Ship_Conah_3.SetStandardVars(1.5f, 0.0f, 800.0f, 0.10f, false);
		Ship_Conah_3.SetGraphicVars(WeaponGraphic.Image.MissilePink, pinkStart, pinkEnd, EffectController.Singleton.missileTrail);
		Ship_Conah_3.SetHomingVars();
		Ship_Conah_3.SetEjectionVars();
		Ship_Conah_3.SetExplosionVars(pinkStart, pinkEnd);
		Ship_Conah_3.SetScatterVars(2, 0.0f);

		Ship_Conah_4 = holderObject.AddComponent<Weapon>();
		Ship_Conah_4.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		Ship_Conah_4.SetGraphicVars(WeaponGraphic.Image.SpeedPink, new Color(1.0f, 0.3f, 0.7f, 1.0f), new Color(0.2f, 0.1f, 0.7f, 1.0f), EffectController.Singleton.lineTrail2);
		Ship_Conah_4.SetRapidVars(5, 0.12f, 0.0f, 1.6f);
		Ship_Conah_4.SetDamage(1.0f);
		Ship_Conah_4.SetPiercing();

		p_crystal = holderObject.AddComponent<Weapon>();
		p_crystal.SetStandardVars(0.14f, 701.0f, 700.0f, 0.2f, true);
		p_crystal.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.2f, 0.2f, 0.9f, 0.7f), EffectController.Singleton.lineTrail);
		p_crystal.SetDamage(2.0f);
		p_crystal.SetPiercing();

		p_void = holderObject.AddComponent<Weapon>();
		p_void.SetStandardVars(0.10f, 601.0f, 600.0f, 0.2f, true);
		p_void.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(0.0f, 0.2f, 0.9f, 1.0f), EffectController.Singleton.waveTrail);
		p_void.SetDamage(2.0f);

		p_gold = holderObject.AddComponent<Weapon>();
		p_gold.SetStandardVars(0.12f, 601.0f, 600.0f, 0.2f, true);
		p_gold.SetGraphicVars(WeaponGraphic.Image.StandardYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 0.0f), EffectController.Singleton.laserTrail);
		p_gold.SetDamage(1.7f);
		p_gold.SetPiercing();

		//Reactor
		ReactorGreen = holderObject.AddComponent<Weapon>();
		ReactorGreen.SetStandardVars(1.5f, 0.0f, 800.0f, 0.10f, false);
		ReactorGreen.SetGraphicVars(WeaponGraphic.Image.MissileGreen, new Color(0.5f, 0.9f, 0.2f, 1.0f), new Color(0.2f, 0.6f, 0.7f, 1.0f), EffectController.Singleton.missileTrail);
		ReactorGreen.SetHomingVars();
		ReactorGreen.SetEjectionVars();
		ReactorGreen.SetRapidVars(4, 0.25f, 0.0f);
		ReactorGreen.SetExplosionVars(new Color(0.5f, 0.9f, 0.2f, 1.0f), new Color(0.2f, 0.6f, 0.7f, 1.0f));
		ReactorGreen.SetScatterVars(2, 0.0f);
		ReactorGreen.SetDamage(2.5f);
		
		ReactorPink = holderObject.AddComponent<Weapon>();
		ReactorPink.SetStandardVars(6.0f, 400.0f, 200.0f, 0.2f);
		ReactorPink.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(255, 218, 252, 255), new Color32(188, 0, 255, 255));
		ReactorPink.SetDamage(50.0f);
		ReactorPink.SetBeamVars(2.2f);
		
		ReactorYellow = holderObject.AddComponent<Weapon>();
		ReactorYellow.SetStandardVars(3.0f, 800.0f, 200.0f, 0.1f);
		ReactorYellow.SetGraphicVars(WeaponGraphic.Image.StarYellow, new Color(1.0f, 0.2f, 0.2f), Color.white);
		ReactorYellow.SetScatterVars(8, KJMath.CIRCLE);
		ReactorYellow.SetRapidVars(6, 0.15f, 0.0f, KJMath.CIRCLE);
		ReactorYellow.SetBuff(Buff.Type.Burn, 100, 2.0f);
		
		ReactorBlue = holderObject.AddComponent<Weapon>();
		ReactorBlue.SetStandardVars(3.0f, 900.0f, 600.0f, 0.2f);
		ReactorBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(0.2f, 0.7f, 1.0f), new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		ReactorBlue.SetChainerVars(5);
		ReactorBlue.SetHomingVars();
		ReactorBlue.SetDamage(3.5f);
		ReactorBlue.SetBuff(Buff.Type.Stun, 100);

		//Boss Weapon

		B_AutoFire = holderObject.AddComponent<Weapon>();
		B_AutoFire.SetStandardVars(5.0f, 700.0f, 500.0f, 0.05f, true);
		B_AutoFire.SetDamage(0.4f);
		B_AutoFire.SetGraphicVars(WeaponGraphic.Image.StandardRed, redStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		B_AutoFire.SetRapidVars(10, 0.08f, 0.05f, 0);

		B_SpreadStun = holderObject.AddComponent<Weapon>();
		B_SpreadStun.SetStandardVars(3.6f, 400.0f, 200.0f, 0.2f);
		B_SpreadStun.SetDamage(1.2f);
		B_SpreadStun.SetGraphicVars(WeaponGraphic.Image.StarBlue, blueStart, Color.white);
		B_SpreadStun.SetScatterVars(8, KJMath.CIRCLE);
		B_SpreadStun.SetRapidVars(6, 0.30f, 0, KJMath.CIRCLE);
		B_SpreadStun.SetBuff(Buff.Type.Stun);

		B_SlowShockWave = holderObject.AddComponent<Weapon>();
		B_SlowShockWave.SetStandardVars(3.2f, 0.0f, 400.0f, 0.015f);
		B_SlowShockWave.SetDamage(1.5f);
		B_SlowShockWave.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(0.5f, 0.0f, 1.0f, 0.0f), EffectController.Singleton.lightSonarTrail);
		B_SlowShockWave.SetScatterVars(3, 1.2f);
		B_SlowShockWave.SetRapidVars(4, 0.4f, 0.3f);

		B_SlowShockWaveRed = holderObject.AddComponent<Weapon>();
		B_SlowShockWaveRed.SetStandardVars(3.2f, 0.0f, 400.0f, 0.015f);
		B_SlowShockWaveRed.SetDamage(0.325f);
		B_SlowShockWaveRed.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(0.9f, 0.0f, 0.0f, 0.0f), EffectController.Singleton.lightSonarTrail);
		B_SlowShockWaveRed.SetScatterVars(3, 1.2f);
		B_SlowShockWaveRed.SetRapidVars(4, 0.4f, 0.3f);
		B_SlowShockWaveRed.SetBuff(Buff.Type.Burn);

		B_Scatter = holderObject.AddComponent<Weapon>();
		B_Scatter.SetStandardVars(3.0f, 400.0f, 200.0f, 0.2f);
		B_Scatter.SetDamage(1.0f);
		B_Scatter.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, Color.white);
		B_Scatter.SetScatterVars(7, KJMath.CIRCLE);
		B_Scatter.SetRapidVars(4, 0.25f, 0, 0);

		B_RunScatterRed = holderObject.AddComponent<Weapon>();
		B_RunScatterRed.SetStandardVars(5f, 0.0f, 600.0f, 0.015f);
		B_RunScatterRed.SetDamage(0.325f);
		B_RunScatterRed.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(0.9f, 0.0f, 0.0f, 0.0f), EffectController.Singleton.lightSonarTrail);
		B_RunScatterRed.SetScatterVars(3, KJMath.CIRCLE * 0.45f);
		B_RunScatterRed.SetRapidVars(8, 0.3f, 0.5f);
		B_RunScatterRed.SetBuff(Buff.Type.Burn);

		B_RunScatterPink = holderObject.AddComponent<Weapon>();
		B_RunScatterPink.SetStandardVars(5f, 0.0f, 600.0f, 0.015f);
		B_RunScatterPink.SetDamage(1.2f);
		B_RunScatterPink.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(0.9f, 0.0f, 1.0f, 0.0f), EffectController.Singleton.lightSonarTrail);
		B_RunScatterPink.SetScatterVars(3, KJMath.CIRCLE * 0.45f);
		B_RunScatterPink.SetRapidVars(8, 0.3f, 0.5f);

		B_Homing = holderObject.AddComponent<Weapon>();
		B_Homing.SetStandardVars(4.0f, 0.0f, 500.0f, 0.03f, false);
		B_Homing.SetGraphicVars(WeaponGraphic.Image.MissileYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		B_Homing.SetHomingVars();
		B_Homing.SetDamage(1.0f);
		B_Homing.SetEjectionVars();
		B_Homing.SetRapidVars(3, 0.25f, 1.8f, 0);

		B_Chainer = holderObject.AddComponent<Weapon>();
		B_Chainer.SetStandardVars(3.0f, 700.0f, 350.0f, 0.2f);
		B_Chainer.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		B_Chainer.SetChainerVars(3);
		B_Chainer.SetHomingVars();
		B_Chainer.SetDamage(0.8f);
		B_Chainer.SetRapidVars(3, 0.3f, 1.8f, 0);
		B_Chainer.SetBuff(Buff.Type.Stun, 100);

		B_ShockWave = holderObject.AddComponent<Weapon>();
		B_ShockWave.SetStandardVars(2.0f, 700.0f, 250.0f, 0.2f);
		B_ShockWave.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.3f, 0.3f, 1.0f, 0.0f), EffectController.Singleton.lightWaveTrail);
		B_ShockWave.SetScatterVars(32, KJMath.CIRCLE * 0.15f);
		B_ShockWave.SetRapidVars(1, 0.15f, 0.0f);
		B_ShockWave.SetDamage(0.20f);
		B_ShockWave.SetBuff(Buff.Type.Stun, 100, 1f, 0.25f);

		B_Red_ElectricBall = holderObject.AddComponent<Weapon>();
		B_Red_ElectricBall.SetStandardVars(3f,350.0f,80.0f,0.2f);
		B_Red_ElectricBall.SetDamage(1.75f);
		B_Red_ElectricBall.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, Color.white);
		B_Red_ElectricBall.SetRapidVars(12,0.08f,0.5f,1.5f);

		B_Pink_Electricball = holderObject.AddComponent<Weapon>();
		B_Pink_Electricball.SetStandardVars(3f,400.0f,200.0f,0.2f);
		B_Pink_Electricball.SetDamage(2.5f);
		B_Pink_Electricball.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, Color.white);
		B_Pink_Electricball.SetRapidVars(8,0.08f,0.8f,1.5f);

		B_Blue_Electricball = holderObject.AddComponent<Weapon>();
		B_Blue_Electricball.SetStandardVars(3f,350.0f,200.0f,0.2f);
		B_Blue_Electricball.SetDamage(1.75f);
		B_Blue_Electricball.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, Color.white);
		B_Blue_Electricball.SetRapidVars(7,0.08f,0.5f,1.5f);
		B_Blue_Electricball.SetBuff(Buff.Type.Stun,100,1f,0.5f);

		B_Green_Electricball = holderObject.AddComponent<Weapon>();
		B_Green_Electricball.SetStandardVars(3f,240.0f,100.0f,0.2f);
		B_Green_Electricball.SetDamage(1.5f);
		B_Green_Electricball.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, Color.white);
		B_Green_Electricball.SetRapidVars(8,0.08f,0.5f,1.5f);

		B_GreenLaserScatter = holderObject.AddComponent<Weapon>();
		B_GreenLaserScatter.SetStandardVars(2.0f, 0.0f, 600.0f, 0.05f);
		B_GreenLaserScatter.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.lineTrail);
		B_GreenLaserScatter.SetRapidVars(3, 0.6f, 0.0f, 1.5f);
		B_GreenLaserScatter.SetDamage(0.3f);
		B_GreenLaserScatter.SetScatterVars(3, 1.0f);
		B_GreenLaserScatter.SetBuff(Buff.Type.Acid);
		B_GreenLaserScatter.SetPiercing();
		
		B_GreenScatter = holderObject.AddComponent<Weapon>();
		B_GreenScatter.SetStandardVars(3.0f, 600.0f, 280.0f, 0.2f);
		B_GreenScatter.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, Color.white);
		B_GreenScatter.SetScatterVars(14, KJMath.CIRCLE * 0.45f);
		B_GreenScatter.SetRapidVars(2, 0.3f, 0.0f);
		B_GreenScatter.SetDamage(1.0f);
		
		B_PurpleScatter = holderObject.AddComponent<Weapon>();
		B_PurpleScatter.SetStandardVars(3.4f, 525.0f, 225.0f, 0.2f);
		B_PurpleScatter.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, Color.white);
		B_PurpleScatter.SetScatterVars(10, KJMath.CIRCLE * 0.45f);
		B_PurpleScatter.SetRapidVars(3, 0.3f, 0.0f);
		B_PurpleScatter.SetDamage(1.0f);
		
		B_RedScatter = holderObject.AddComponent<Weapon>();
		B_RedScatter.SetStandardVars(3.5f, 550.0f, 250.0f, 0.2f);
		B_RedScatter.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, Color.white);
		B_RedScatter.SetScatterVars(14, KJMath.CIRCLE * 0.6f);
		B_RedScatter.SetRapidVars(3, 0.5f, 0.0f);
		B_RedScatter.SetDamage(0.275f);
		B_RedScatter.SetBuff(Buff.Type.Burn);
		
		B_BlueChain = holderObject.AddComponent<Weapon>();
		B_BlueChain.SetStandardVars(2.75f, 625.0f, 500.0f, 0.2f);
		B_BlueChain.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.shockTrail);
		B_BlueChain.SetChainerVars(2);
		B_BlueChain.SetHomingVars();
		B_BlueChain.SetDamage(1.0f);
		B_BlueChain.SetBuff(Buff.Type.Stun);
		
		B_BlueMissile = holderObject.AddComponent<Weapon>(); 
		B_BlueMissile.SetStandardVars(3f, 250.0f, 545.0f, 0.50f);
		B_BlueMissile.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		B_BlueMissile.SetHomingVars();
		B_BlueMissile.SetDamage(0.5f);
		B_BlueMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		B_BlueMissile.SetBuff(Buff.Type.Stun, 100 , 1 , 2.5f);

		B_DestoryerBlueMissile = holderObject.AddComponent<Weapon>(); 
		B_DestoryerBlueMissile.SetStandardVars(3f, 250.0f, 425.0f, 0.50f);
		B_DestoryerBlueMissile.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		B_DestoryerBlueMissile.SetHomingVars();
		B_DestoryerBlueMissile.SetDamage(0.5f);
		B_DestoryerBlueMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		B_DestoryerBlueMissile.SetBuff(Buff.Type.Stun, 100 , 1 , 2.5f);
		
		B_BlueScatter = holderObject.AddComponent<Weapon>();
		B_BlueScatter.SetStandardVars(2.75f, 575.0f, 275.0f, 0.2f);
		B_BlueScatter.SetGraphicVars(WeaponGraphic.Image.SpeedBlue, blueStart, Color.white);
		B_BlueScatter.SetScatterVars(16, KJMath.CIRCLE * 0.45f);
		B_BlueScatter.SetRapidVars(1, 0.15f, 0.0f);
		B_BlueScatter.SetDamage(1.0f);
		B_BlueScatter.SetBuff(Buff.Type.Stun);
		
		B_PurpleMissile = holderObject.AddComponent<Weapon>(); 
		B_PurpleMissile.SetStandardVars(0.8f, 0.0f, 450.0f, 0.50f);
		B_PurpleMissile.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(1.0f, 0.3f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		B_PurpleMissile.SetHomingVars();
		B_PurpleMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		
		B_BlackHole = holderObject.AddComponent<Weapon>();
		B_BlackHole.SetStandardVars(1.0f, 300.0f, 200.0f, 0.2f);
		B_BlackHole.SetGraphicVars(WeaponGraphic.Image.BlackHoleBomb, new Color(1.0f, 0.5f, 1.0f, 1.0f), new Color(0.1f, 0.3f, 1.0f, 0.0f), EffectController.Singleton.sonarTrail);
		B_BlackHole.SetTime(1.20f);
		B_BlackHole.SetBlackHole();
		B_BlackHole.SetHomingVars();

		B_SuperMissile = holderObject.AddComponent<Weapon>(); 
		B_SuperMissile.SetStandardVars(4.3f, -2500.0f, 350.0f, 0.50f);
		B_SuperMissile.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.3f, 1.0f), new Color(1.0f, 0.7f, 0.3f, 1.0f), EffectController.Singleton.missileTrail);
		B_SuperMissile.SetHomingVars();
		B_SuperMissile.SetDamage(0.75f);
		B_SuperMissile.SetRapidVars(10, 0.1f, 0.0f, KJMath.CIRCLE / 2);
		B_SuperMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		
		B_SuperMissile2 = holderObject.AddComponent<Weapon>(); 
		B_SuperMissile2.SetStandardVars(4.3f, -2500.0f, 350.0f, 0.50f);
		B_SuperMissile2.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.3f, 1.0f), new Color(1.0f, 0.7f, 0.3f, 1.0f), EffectController.Singleton.missileTrail);
		B_SuperMissile2.SetHomingVars();
		B_SuperMissile2.SetEjectionVars();
		B_SuperMissile2.SetDamage(0.75f);
		B_SuperMissile2.SetRapidVars(3, 0.25f, 0.0f);
		B_SuperMissile2.SetScatterVars(6, KJMath.CIRCLE * 0.45f);
		B_SuperMissile2.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));

		B_GatheringRed = holderObject.AddComponent<Weapon>(); 
		B_GatheringRed.SetStandardVars(4.3f, 401.0f, 400.0f, 0.50f);
		B_GatheringRed.SetGraphicVars(WeaponGraphic.Image.StandardRed, redStart, new Color(1.0f, 0.7f, 0.3f, 1.0f));
		B_GatheringRed.SetDamage(0.325f);
		B_GatheringRed.SetBuff(Buff.Type.Burn);
		B_GatheringRed.SetRapidVars(20, 0.35f, 0.0f);

		B_GatheringGreen = holderObject.AddComponent<Weapon>(); 
		B_GatheringGreen.SetStandardVars(4.3f, 401.0f, 400.0f, 0.50f);
		B_GatheringGreen.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(1.0f, 0.7f, 0.3f, 1.0f));
		B_GatheringGreen.SetDamage(0.5f);
		B_GatheringGreen.SetBuff(Buff.Type.Acid, 100, 1.0f, 4.0f);
		B_GatheringGreen.SetRapidVars(20, 0.35f, 0.0f);

		B_GatheringBlue = holderObject.AddComponent<Weapon>(); 
		B_GatheringBlue.SetStandardVars(4.3f, 401.0f, 400.0f, 0.50f);
		B_GatheringBlue.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(1.0f, 0.7f, 0.3f, 1.0f));
		B_GatheringBlue.SetDamage(0.3f);
		B_GatheringBlue.SetBuff(Buff.Type.Stun, 100, 1.0f, 1.0f);
		B_GatheringBlue.SetRapidVars(20, 0.35f, 0.0f);

		B_ScatterChain4Blue = holderObject.AddComponent<Weapon>();
		B_ScatterChain4Blue.SetStandardVars(5.0f, 0.0f, 450.0f, 0.05f, false);
		B_ScatterChain4Blue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		B_ScatterChain4Blue.SetBuff(Buff.Type.Stun);
		B_ScatterChain4Blue.SetScatterVars(3 , 0.7f);
		B_ScatterChain4Blue.SetRapidVars(3, 0.50f, 0.0f, 0.00f);
		B_ScatterChain4Blue.SetDamage(1.0f);

		B_ScatterChain4Red = holderObject.AddComponent<Weapon>();
		B_ScatterChain4Red.SetStandardVars(5.0f, 0.0f, 450.0f, 0.05f, false);
		B_ScatterChain4Red.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color (0.9f, 0.0f, 0.2f, 1.0f), EffectController.Singleton.shockTrail);
		B_ScatterChain4Red.SetBuff(Buff.Type.Burn);
		B_ScatterChain4Red.SetScatterVars(3 , 0.7f);
		B_ScatterChain4Red.SetRapidVars(3, 0.50f, 0.0f, 0.00f);
		B_ScatterChain4Red.SetDamage(0.325f);

		B_SonarRed = holderObject.AddComponent<Weapon>();
		B_SonarRed.SetStandardVars(5.0f, 650.0f, 150.0f, 0.05f, false);
		B_SonarRed.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color(0.6f, 0.0f, 0.0f, 0.0f), EffectController.Singleton.sonarTrail);
		B_SonarRed.SetPiercing();
		B_SonarRed.SetBuff(Buff.Type.Burn);
		B_SonarRed.SetScatterVars(2 , 0.7f);
		B_SonarRed.SetRapidVars(4, 0.15f, 0.0f, 1.9f);
		B_SonarRed.SetDamage(0.325f);
		
		B_SonarBlue = holderObject.AddComponent<Weapon>();
		B_SonarBlue.SetStandardVars(5.0f, 650.0f, 150.0f, 0.05f, false);
		B_SonarBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.0f, 0.8f, 0.0f), EffectController.Singleton.sonarTrail);
		B_SonarBlue.SetPiercing();
		B_SonarBlue.SetBuff(Buff.Type.Stun);
		B_SonarBlue.SetScatterVars(2 , 0.7f);
		B_SonarBlue.SetRapidVars(4, 0.15f, 0.0f, 1.9f);
		B_SonarBlue.SetDamage(0.8f);

		B_FastHoming = holderObject.AddComponent<Weapon>();
		B_FastHoming.SetStandardVars(2.0f, -1000.0f, 500.0f, 0.05f, false);
		B_FastHoming.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		B_FastHoming.SetHomingVars();
		B_FastHoming.SetScatterVars(2 , 0.7f);
		//B_FastHoming.SetRapidVars(2, 0.5f, 0.0f, 1.9f);
		B_FastHoming.SetDamage(1.0f);
		B_FastHoming.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		// Boss TITAN Weapons

		BT_TurretPlasma = holderObject.AddComponent<Weapon>();
		BT_TurretPlasma.SetStandardVars(0.8f, 400.0f, 150.0f, 0.05f, true);
		BT_TurretPlasma.SetDamage(1.0f);
		BT_TurretPlasma.SetGraphicVars(WeaponGraphic.Image.RoundPink, pinkStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));

		BT_TurretSlowStun = holderObject.AddComponent<Weapon>();
		BT_TurretSlowStun.SetStandardVars(0.8f, 400.0f, 150.0f, 0.05f, true);
		BT_TurretSlowStun.SetDamage(0.5f);
		BT_TurretSlowStun.SetGraphicVars(WeaponGraphic.Image.RoundBlue, blueStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		BT_TurretSlowStun.SetBuff(Buff.Type.Stun);

		BT_TurretVolleyStun = holderObject.AddComponent<Weapon>();
		BT_TurretVolleyStun.SetStandardVars(10.0f, 600.0f, 275.0f, 0.05f, true);
		BT_TurretVolleyStun.SetDamage(0.4f);
		BT_TurretVolleyStun.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		BT_TurretVolleyStun.SetBuff(Buff.Type.Stun);
		BT_TurretVolleyStun.SetRapidVars(8, 0.12f, 0.07f, 0);

		BT_TurretScatterShot = holderObject.AddComponent<Weapon>();
		BT_TurretScatterShot.SetStandardVars(8.0f, 100.0f, 650.0f, 0.03f, true);
		BT_TurretScatterShot.SetDamage (0.8f);
		BT_TurretScatterShot.SetIgnoreArmor();
		BT_TurretScatterShot.SetGraphicVars(WeaponGraphic.Image.SpeedYellow, yellowStart, new Color(0.6f, 0.15f, 0.0f, 1.0f), EffectController.Singleton.laserTrail);
//		BT_TurretScatterShot.SetTime(0.5f);
		BT_TurretScatterShot.SetScatterVars(3, KJMath.CIRCLE);
		BT_TurretScatterShot.SetRapidVars(10, 0.12f, 0.0f, 1.8f);

		BT_SonarRed = holderObject.AddComponent<Weapon>();
		BT_SonarRed.SetStandardVars(5.0f, 650.0f, 150.0f, 0.05f, false);
		BT_SonarRed.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color(0.6f, 0.0f, 0.0f, 0.0f), EffectController.Singleton.sonarTrail);
		BT_SonarRed.SetPiercing();
		BT_SonarRed.SetBuff(Buff.Type.Burn);
		BT_SonarRed.SetScatterVars(2 , 0.7f);
		BT_SonarRed.SetRapidVars(4, 0.15f, 0.0f, 1.9f);
		BT_SonarRed.SetDamage(0.325f);

		BT_SonarBlue = holderObject.AddComponent<Weapon>();
		BT_SonarBlue.SetStandardVars(5.0f, 650.0f, 150.0f, 0.05f, false);
		BT_SonarBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.0f, 0.8f, 0.0f), EffectController.Singleton.sonarTrail);
		BT_SonarBlue.SetPiercing();
		BT_SonarBlue.SetBuff(Buff.Type.Stun);
		BT_SonarBlue.SetScatterVars(2 , 0.7f);
		BT_SonarBlue.SetRapidVars(4, 0.15f, 0.0f, 1.9f);
		BT_SonarBlue.SetDamage(0.8f);

		BT_ScatterChain4Blue = holderObject.AddComponent<Weapon>();
		BT_ScatterChain4Blue.SetStandardVars(5.0f, 400.0f, 275.0f, 0.05f, false);
		BT_ScatterChain4Blue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		BT_ScatterChain4Blue.SetBuff(Buff.Type.Stun);
		BT_ScatterChain4Blue.SetScatterVars(3 , 0.7f);
		BT_ScatterChain4Blue.SetRapidVars(3, 0.50f, 0.0f, 0.00f);
		BT_ScatterChain4Blue.SetDamage(1.0f);

		BT_ScatterChain4Red = holderObject.AddComponent<Weapon>();
		BT_ScatterChain4Red.SetStandardVars(5.0f, 400.0f, 275.0f, 0.05f, false);
		BT_ScatterChain4Red.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color (0.9f, 0.0f, 0.2f, 1.0f), EffectController.Singleton.shockTrail);
		BT_ScatterChain4Red.SetBuff(Buff.Type.Burn);
		BT_ScatterChain4Red.SetScatterVars(3 , 0.7f);
		BT_ScatterChain4Red.SetRapidVars(3, 0.50f, 0.0f, 0.00f);
		BT_ScatterChain4Red.SetDamage(0.325f);

		BT_HommingRed = holderObject.AddComponent<Weapon>();
		BT_HommingRed.SetStandardVars(6.0f, 0.0f, 325.0f, 0.03f, false);
		BT_HommingRed.SetGraphicVars(WeaponGraphic.Image.MissileRed, redStart, new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		BT_HommingRed.SetHomingVars();
		BT_HommingRed.SetDamage(1.0f);
		BT_HommingRed.SetEjectionVars();
		BT_HommingRed.SetRapidVars(3, 0.3f, 1.8f, 0);

		BT_HommingBlue = holderObject.AddComponent<Weapon>();
		BT_HommingBlue.SetStandardVars(6.0f, 0.0f, 325.0f, 0.03f, false);
		BT_HommingBlue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, blueStart, new Color(0.0f, 0.0f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		BT_HommingBlue.SetHomingVars();
		BT_HommingBlue.SetDamage(1.0f);
		BT_HommingBlue.SetEjectionVars();
		BT_HommingBlue.SetRapidVars(3, 0.3f, 1.8f, 0);

		BT_BeamBlue = holderObject.AddComponent<Weapon>();
		BT_BeamBlue.SetStandardVars(6.5f, 400.0f, 200.0f, 0.2f);
		BT_BeamBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(150, 216, 255, 255), new Color32(0, 76, 255, 255));
		BT_BeamBlue.SetDamage(6.0f);
		BT_BeamBlue.SetBuff(Buff.Type.Stun);
		BT_BeamBlue.SetBeamVars(2.5f);

		BT_BeamRed = holderObject.AddComponent<Weapon>();
		BT_BeamRed.SetStandardVars(6.5f, 400.0f, 200.0f, 0.2f);
		BT_BeamRed.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(255, 193, 193, 255), new Color32(255, 13, 13, 255));
		BT_BeamRed.SetDamage(0.6f);
		BT_BeamRed.SetBuff(Buff.Type.Burn);
		BT_BeamRed.SetBeamVars(2.5f);

		BT_ScatterLaserYellow = holderObject.AddComponent<Weapon>();
		BT_ScatterLaserYellow.SetStandardVars(6.0f, 351.0f, 350.0f, 0.03f, true);
		BT_ScatterLaserYellow.SetDamage (0.8f);
		BT_ScatterLaserYellow.SetGraphicVars(WeaponGraphic.Image.StandardYellow, yellowStart, new Color(0.6f, 0.15f, 0.0f, 1.0f), EffectController.Singleton.laserTrail);
		BT_ScatterLaserYellow.SetScatterVars(4, KJMath.CIRCLE);
		BT_ScatterLaserYellow.SetRapidVars(7, 0.150f, 0.0f, -1.8f);

		BT_ScatterLaserGreen = holderObject.AddComponent<Weapon>();
		BT_ScatterLaserGreen.SetStandardVars(6.0f, 100.0f, 350.0f, 0.03f, true);
		BT_ScatterLaserGreen.SetDamage (0.8f);
		BT_ScatterLaserGreen.SetGraphicVars(WeaponGraphic.Image.StandardGreen, new Color(0.0f, 0.8f, 0.2f, 1.0f), new Color(0.1f, 0.8f, 0.2f, 1.0f), EffectController.Singleton.laserTrail);
		BT_ScatterLaserGreen.SetScatterVars(4, KJMath.CIRCLE);
		BT_ScatterLaserGreen.SetRapidVars(7, 0.150f, 0.0f, -1.8f);

		BT_HomingSonarGreen = holderObject.AddComponent<Weapon>();
		BT_HomingSonarGreen.SetStandardVars(5.0f, 0.0f, 300.0f, 0.05f, false);
		BT_HomingSonarGreen.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, new Color(0.2f, 0.8f, 0.2f, 1.0f), new Color(0.0f, 0.8f, 0.0f, 0.0f), EffectController.Singleton.sonarTrail);
		BT_HomingSonarGreen.SetBuff(Buff.Type.Acid);
		BT_HomingSonarGreen.SetHomingVars();
		BT_HomingSonarGreen.SetRapidVars(4, 0.4f, 0.0f, 0.0f);
		BT_HomingSonarGreen.SetDamage(1.0f);

		BT_HomingSonarYellow = holderObject.AddComponent<Weapon>();
		BT_HomingSonarYellow.SetStandardVars(5.0f, 0.0f, 300.0f, 0.05f, false);
		BT_HomingSonarYellow.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, new Color(1.0f, 0.7f, 0.2f, 1.0f), new Color(0.6f, 0.15f, 0.0f, 0.0f), EffectController.Singleton.sonarTrail);
		BT_HomingSonarYellow.SetBuff(Buff.Type.Acid);
		BT_HomingSonarYellow.SetHomingVars();
		BT_HomingSonarYellow.SetRapidVars(4, 0.4f, 0.0f, 0.0f);
		BT_HomingSonarYellow.SetDamage(1.0f);
	
		Orbita_SpreadBurn = holderObject.AddComponent<Weapon>();
		Orbita_SpreadBurn.SetStandardVars(3.6f, 0.0f, 300.0f, 0.2f);
		Orbita_SpreadBurn.SetDamage(0.325f);
		Orbita_SpreadBurn.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		Orbita_SpreadBurn.SetScatterVars(10, KJMath.CIRCLE);
		Orbita_SpreadBurn.SetRapidVars(4, 0.3f, 0, KJMath.CIRCLE);
		Orbita_SpreadBurn.SetBuff(Buff.Type.Burn);

		Orbita_SpreadLaserBurn = holderObject.AddComponent<Weapon>();
		Orbita_SpreadLaserBurn.SetStandardVars(3.6f, 451.0f, 450.0f, 0.2f);
		Orbita_SpreadLaserBurn.SetDamage(0.325f);
		Orbita_SpreadLaserBurn.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, new Color(0.9f, 0.0f, 0.2f), EffectController.Singleton.lineTrail);
		Orbita_SpreadLaserBurn.SetScatterVars(4, KJMath.CIRCLE * 0.45f);
		Orbita_SpreadLaserBurn.SetRapidVars(5, 0.15f, 0, KJMath.CIRCLE);
		Orbita_SpreadLaserBurn.SetBuff(Buff.Type.Burn);

		Orbita_LaserBurn = holderObject.AddComponent<Weapon>();
		Orbita_LaserBurn.SetStandardVars(3.6f, 451.0f, 450.0f, 0.2f);
		Orbita_LaserBurn.SetDamage(0.325f);
		Orbita_LaserBurn.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, new Color(0.9f, 0.0f, 0.2f), EffectController.Singleton.lineTrail);
		Orbita_LaserBurn.SetScatterVars(2, KJMath.CIRCLE * 0.15f);
		Orbita_LaserBurn.SetRapidVars(16, 0.10f, 0, KJMath.CIRCLE);
		Orbita_LaserBurn.SetBuff(Buff.Type.Burn);

		Orbita_HomingBurn = holderObject.AddComponent<Weapon>();
		Orbita_HomingBurn.SetStandardVars(6.0f, 10.0f, 400.0f, 1.5f, false);
		Orbita_HomingBurn.SetGraphicVars(WeaponGraphic.Image.ElectroRed, new Color(1.0f, 0.2f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 0.1f), EffectController.Singleton.sonarTrail);
		Orbita_HomingBurn.SetDamage(0.325f);
		Orbita_HomingBurn.SetScatterVars(2, -KJMath.CIRCLE);
		Orbita_HomingBurn.SetRapidVars(4, 0.50f, -0.7f);
		Orbita_HomingBurn.SetHomingVars();
		Orbita_HomingBurn.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Orbita_HomingBurn.SetBuff(Buff.Type.Burn);

		Orbita_RedElectricball2 = holderObject.AddComponent<Weapon>();
		Orbita_RedElectricball2.SetStandardVars(6.0f, 25.0f, 200.0f, 1.5f, false);
		Orbita_RedElectricball2.SetGraphicVars(WeaponGraphic.Image.ElectroRed, new Color(1.0f, 0.2f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Orbita_RedElectricball2.SetDamage(0.325f);
		Orbita_RedElectricball2.SetScatterVars(10, KJMath.CIRCLE * 0.5f);
		Orbita_RedElectricball2.SetRapidVars(3, 0.7f, 0f, 0.5f);
		Orbita_RedElectricball2.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Orbita_RedElectricball2.SetBuff(Buff.Type.Burn);

		Orbita_RedElectricball = holderObject.AddComponent<Weapon>();
		Orbita_RedElectricball.SetStandardVars(3f,240.0f,100.0f,0.2f);
		Orbita_RedElectricball.SetDamage(0.325f);
		Orbita_RedElectricball.SetGraphicVars(WeaponGraphic.Image.ElectroRed, greenStart, Color.white);
		Orbita_RedElectricball.SetRapidVars(12,0.08f, 0.8f, 1.5f);
		Orbita_RedElectricball.SetBuff(Buff.Type.Burn);

		Orbita_RedSonarSlow = holderObject.AddComponent<Weapon>();
		Orbita_RedSonarSlow.SetStandardVars(3.2f, 500.0f, 100.0f, 1f);
		Orbita_RedSonarSlow.SetDamage(0.325f);
		Orbita_RedSonarSlow.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.0f), EffectController.Singleton.lightSonarTrail);
		Orbita_RedSonarSlow.SetScatterVars(3, 1.2f);
		Orbita_RedSonarSlow.SetRapidVars(4, 0.4f, 0.5f);
		Orbita_RedSonarSlow.SetBuff(Buff.Type.Burn);
		
		Orbita_RedSonarFast = holderObject.AddComponent<Weapon>();
		Orbita_RedSonarFast.SetStandardVars(3.2f, 0.0f, 400.0f, 0.015f);
		Orbita_RedSonarFast.SetDamage(0.325f);
		Orbita_RedSonarFast.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.0f), EffectController.Singleton.lightSonarTrail);
		Orbita_RedSonarFast.SetScatterVars(3, 1.2f);
		Orbita_RedSonarFast.SetRapidVars(4, 0.4f, 0.5f);
		Orbita_RedSonarFast.SetBuff(Buff.Type.Burn);

//		public static Weapon Arbiter_RedLaser {get ;private set;}
//		public static Weapon Arbiter_BlueLaser {get ;private set;}
//		public static Weapon Arbiter_PinkLaser {get ;private set;}
//		public static Weapon Arbiter_GreenLaser {get ;private set;}

		Arbiter_GreenSupport = holderObject.AddComponent<Weapon>();
		Arbiter_GreenSupport.SetStandardVars(1.25f, 225.0f, 175.0f, 0.2f);
		Arbiter_GreenSupport.SetGraphicVars(WeaponGraphic.Image.RoundGreen, greenStart, new Color(1.0f, 0.0f, 0.0f, 0.0f));
		Arbiter_GreenSupport.SetDamage(0.75f);
		Arbiter_GreenSupport.SetRapidVars(10, 0.08f, 0.5f, 1.5f);

		Arbiter_RoundSlow = holderObject.AddComponent<Weapon>();
		Arbiter_RoundSlow.SetStandardVars(1.25f, 300.0f, 100.0f, 0.2f);
		Arbiter_RoundSlow.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 0.0f));
		Arbiter_RoundSlow.SetDamage(0.8f);
		Arbiter_RoundSlow.SetRapidVars(9, 0.12f, 0.5f, 1.5f);

		Arbiter_StunSupport = holderObject.AddComponent<Weapon>();
		Arbiter_StunSupport.SetStandardVars(1.25f, 175.0f, 125.0f, 0.2f);
		Arbiter_StunSupport.SetGraphicVars(WeaponGraphic.Image.RoundBlue, blueStart, new Color(1.0f, 0.0f, 0.0f, 0.0f));
		Arbiter_StunSupport.SetDamage(0.5f);
		Arbiter_StunSupport.SetBuff(Buff.Type.Stun);
		Arbiter_StunSupport.SetRapidVars(7, 0.12f, 0.5f, 1.0f);

		Arbiter_ScatterPink = holderObject.AddComponent<Weapon>();
		Arbiter_ScatterPink.SetStandardVars(1f, 350.0f, 175.0f, 0.2f);
		Arbiter_ScatterPink.SetDamage(1.25f);
		Arbiter_ScatterPink.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, Color.white);
		Arbiter_ScatterPink.SetScatterVars(10, KJMath.CIRCLE);
		Arbiter_ScatterPink.SetRapidVars(3, 0.6f, 0, 2.5f);

		Arbiter_ScatterBlue = holderObject.AddComponent<Weapon>();
		Arbiter_ScatterBlue.SetStandardVars(1f, 350.0f, 175.0f, 0.2f);
		Arbiter_ScatterBlue.SetDamage(0.5f);
		Arbiter_ScatterBlue.SetGraphicVars(WeaponGraphic.Image.SpeedBlue, blueStart, Color.white);
		Arbiter_ScatterBlue.SetScatterVars(10, KJMath.CIRCLE);
		Arbiter_ScatterBlue.SetRapidVars(3, 0.6f, 0, 2.5f);
		Arbiter_StunSupport.SetBuff(Buff.Type.Stun);

		Arbiter_RapidSlow = holderObject.AddComponent<Weapon>();
		Arbiter_RapidSlow.SetStandardVars(3.2f, 500.0f, 100.0f, 1f);
		Arbiter_RapidSlow.SetDamage(0.8f);
		Arbiter_RapidSlow.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.0f), EffectController.Singleton.lightSonarTrail);
		Arbiter_RapidSlow.SetScatterVars(3, 1.2f);
		Arbiter_RapidSlow.SetRapidVars(4, 0.4f, 0.5f);
		Arbiter_RapidSlow.SetBuff(Buff.Type.Acid,100,1,6.0f);

		Arbiter_RapidFast = holderObject.AddComponent<Weapon>();
		Arbiter_RapidFast.SetStandardVars(3.2f, 0.0f, 400.0f, 0.015f);
		Arbiter_RapidFast.SetDamage(0.8f);
		Arbiter_RapidFast.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.0f), EffectController.Singleton.lightSonarTrail);
		Arbiter_RapidFast.SetScatterVars(3, 1.2f);
		Arbiter_RapidFast.SetRapidVars(4, 0.4f, 0.5f);
		Arbiter_RapidFast.SetBuff(Buff.Type.Stun,100,1,3.5f);

		B_RedScatterLaser = holderObject.AddComponent<Weapon>();
		B_RedScatterLaser.SetStandardVars(3.0f, 351.0f, 350.0f, 0.03f, true);
		B_RedScatterLaser.SetDamage (0.3f);
		B_RedScatterLaser.SetGraphicVars(WeaponGraphic.Image.StandardRed, redStart, new Color(1.0f, 0.3f, 0.2f, 1.0f), EffectController.Singleton.laserTrail);
		B_RedScatterLaser.SetScatterVars(4, KJMath.CIRCLE);
		B_RedScatterLaser.SetRapidVars(7, 0.150f, 0.0f, -1.8f);
		B_RedScatterLaser.SetBuff(Buff.Type.Burn);

		B_BlueScatterLaser = holderObject.AddComponent<Weapon>();
		B_BlueScatterLaser.SetStandardVars(3.0f, 351.0f, 350.0f, 0.03f, true);
		B_BlueScatterLaser.SetDamage (0.8f);
		B_BlueScatterLaser.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 1.0f), EffectController.Singleton.laserTrail);
		B_BlueScatterLaser.SetScatterVars(4, KJMath.CIRCLE);
		B_BlueScatterLaser.SetRapidVars(7, 0.150f, 0.0f, -1.8f);
		B_BlueScatterLaser.SetBuff(Buff.Type.Stun,100,1,3.0f);

		B_GreenScatterLaser = holderObject.AddComponent<Weapon>();
		B_GreenScatterLaser.SetStandardVars(3.0f, 351.0f, 350.0f, 0.03f, true);
		B_GreenScatterLaser.SetDamage (0.7f);
		B_GreenScatterLaser.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 1.0f), EffectController.Singleton.laserTrail);
		B_GreenScatterLaser.SetScatterVars(4, KJMath.CIRCLE);
		B_GreenScatterLaser.SetRapidVars(7, 0.150f, 0.0f, -1.8f);
		B_GreenScatterLaser.SetBuff(Buff.Type.Acid,100,1,6.0f);

		B_PinkScatterLaser = holderObject.AddComponent<Weapon>();
		B_PinkScatterLaser.SetStandardVars(3.0f, 301.0f, 300.0f, 0.03f, true);
		B_PinkScatterLaser.SetDamage (1.0f);
		B_PinkScatterLaser.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(1.0f, 0.4f, 0.9f, 1.0f), EffectController.Singleton.laserTrail);
		B_PinkScatterLaser.SetScatterVars(4, KJMath.CIRCLE);
		B_PinkScatterLaser.SetRapidVars(7, 0.150f, 0.0f, -1.8f);

		B_DestoryerRedScatterLaser = holderObject.AddComponent<Weapon>();
		B_DestoryerRedScatterLaser.SetStandardVars(3.0f, 351.0f, 350.0f, 0.03f, true);
		B_DestoryerRedScatterLaser.SetDamage (0.3f);
		B_DestoryerRedScatterLaser.SetGraphicVars(WeaponGraphic.Image.StandardRed, redStart, new Color(1.0f, 0.3f, 0.2f, 1.0f), EffectController.Singleton.laserTrail);
		B_DestoryerRedScatterLaser.SetScatterVars(4, KJMath.CIRCLE);
		B_DestoryerRedScatterLaser.SetRapidVars(6, 0.17f, 0.0f, -1.8f);
		B_DestoryerRedScatterLaser.SetBuff(Buff.Type.Burn);
		
		B_DestoryerBlueScatterLaser = holderObject.AddComponent<Weapon>();
		B_DestoryerBlueScatterLaser.SetStandardVars(3.0f, 351.0f, 350.0f, 0.03f, true);
		B_DestoryerBlueScatterLaser.SetDamage (0.8f);
		B_DestoryerBlueScatterLaser.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 1.0f), EffectController.Singleton.laserTrail);
		B_DestoryerBlueScatterLaser.SetScatterVars(4, KJMath.CIRCLE);
		B_DestoryerBlueScatterLaser.SetRapidVars(6, 0.17f, 0.0f, -1.8f);
		B_DestoryerBlueScatterLaser.SetBuff(Buff.Type.Stun,100,1,3.0f);
		
		B_DestoryerGreenScatterLaser = holderObject.AddComponent<Weapon>();
		B_DestoryerGreenScatterLaser.SetStandardVars(3.0f, 351.0f, 350.0f, 0.03f, true);
		B_DestoryerGreenScatterLaser.SetDamage (0.7f);
		B_DestoryerGreenScatterLaser.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 1.0f), EffectController.Singleton.laserTrail);
		B_DestoryerGreenScatterLaser.SetScatterVars(4, KJMath.CIRCLE);
		B_DestoryerGreenScatterLaser.SetRapidVars(6, 0.17f, 0.0f, -1.8f);
		B_DestoryerGreenScatterLaser.SetBuff(Buff.Type.Acid,100,1,6.0f);
		
		B_DestoryerPinkScatterLaser = holderObject.AddComponent<Weapon>();
		B_DestoryerPinkScatterLaser.SetStandardVars(3.0f, 301.0f, 300.0f, 0.03f, true);
		B_DestoryerPinkScatterLaser.SetDamage (1.0f);
		B_DestoryerPinkScatterLaser.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(1.0f, 0.4f, 0.9f, 1.0f), EffectController.Singleton.laserTrail);
		B_DestoryerPinkScatterLaser.SetScatterVars(4, KJMath.CIRCLE);
		B_DestoryerPinkScatterLaser.SetRapidVars(6, 0.17f, 0.0f, -1.8f);

		B_GreenLaser = holderObject.AddComponent<Weapon>();
		B_GreenLaser.SetStandardVars(3.0f, 375.0f, 376.0f, 0.05f);
		B_GreenLaser.SetGraphicVars(WeaponGraphic.Image.StandardGreen, new Color(0.2f, 0.8f, 0.2f, 1.0f), new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.laserTrail);
		B_GreenLaser.SetDamage(0.7f);
		B_GreenLaser.SetScatterVars(3, KJMath.CIRCLE * 0.35f);
		B_GreenLaser.SetRapidVars(8, 0.10f, 0.45f);
		B_GreenLaser.SetBuff(Buff.Type.Acid, 100, 1.0f, 6.0f);

		B_RedLaser = holderObject.AddComponent<Weapon>();
		B_RedLaser.SetStandardVars(3.0f, 375.0f, 376.0f, 0.05f);
		B_RedLaser.SetGraphicVars(WeaponGraphic.Image.StandardRed, redStart, new Color(1.0f, 0.3f, 0.2f, 1.0f), EffectController.Singleton.laserTrail);
		B_RedLaser.SetDamage(0.3f);
		B_RedLaser.SetScatterVars(3, KJMath.CIRCLE * 0.35f);
		B_RedLaser.SetRapidVars(8, 0.10f, 0.45f);
		B_RedLaser.SetBuff(Buff.Type.Burn);

		B_BlueLaser = holderObject.AddComponent<Weapon>();
		B_BlueLaser.SetStandardVars(3.0f, 375.0f, 376.0f, 0.05f);
		B_BlueLaser.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 1.0f), EffectController.Singleton.laserTrail);
		B_BlueLaser.SetDamage(0.8f);
		B_BlueLaser.SetScatterVars(3, KJMath.CIRCLE * 0.35f);
		B_BlueLaser.SetRapidVars(8, 0.10f, 0.45f);
		B_BlueLaser.SetBuff(Buff.Type.Stun,100,1,3.0f);

		B_PinkLaser = holderObject.AddComponent<Weapon>();
		B_PinkLaser.SetStandardVars(3.0f, 375.0f, 376.0f, 0.05f);
		B_PinkLaser.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(1.0f, 0.4f, 0.9f, 1.0f), EffectController.Singleton.laserTrail);
		B_PinkLaser.SetDamage(2.0f);
		B_PinkLaser.SetScatterVars(3, KJMath.CIRCLE * 0.35f);
		B_PinkLaser.SetRapidVars(7, 0.10f, 0.45f);

		Dreadnought_Core1 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core1.SetStandardVars(4.0f, 400.0f, 250.0f, 0.2f);
		Dreadnought_Core1.SetDamage(1.0f);
		Dreadnought_Core1.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, Color.white);
		Dreadnought_Core1.SetScatterVars(8, KJMath.CIRCLE * 0.45f);
		Dreadnought_Core1.SetRapidVars(2, 0.5f, 0, 0);

		Dreadnought_Core2 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core2.SetStandardVars(3.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Core2.SetDamage(0.25f);
		Dreadnought_Core2.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		Dreadnought_Core2.SetRapidVars(14, 0.12f, 0.9f);
		Dreadnought_Core2.SetBuff(Buff.Type.Burn);

		Dreadnought_Core3 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core3.SetStandardVars(3.5f, 600.0f, 200.0f, 0.2f);
		Dreadnought_Core3.SetDamage(1f);
		Dreadnought_Core3.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, Color.white);
		Dreadnought_Core3.SetRapidVars(14, 0.09f, 0.7f);
		Dreadnought_Core3.SetBuff(Buff.Type.Stun);

		Dreadnought_Core4 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core4.SetStandardVars(3f,251.0f,250.0f,0.2f);
		Dreadnought_Core4.SetDamage(1f);
		Dreadnought_Core4.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, Color.white);
		Dreadnought_Core4.SetRapidVars(14,0.08f,0.5f,1.5f);

		Dreadnought_Core5 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core5.SetStandardVars(3.5f, 550.0f, 250.0f, 0.2f);
		Dreadnought_Core5.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, Color.white);
		Dreadnought_Core5.SetScatterVars(12, KJMath.CIRCLE * 0.6f);
		Dreadnought_Core5.SetRapidVars(2, 0.5f, 0.0f);
		Dreadnought_Core5.SetDamage(0.25f);
		Dreadnought_Core5.SetBuff(Buff.Type.Burn);

		Dreadnought_Core6 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core6.SetStandardVars(4.25f, 401.0f, 450.0f, 0.05f, true);
		Dreadnought_Core6.SetDamage(0.65f);
		Dreadnought_Core6.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		Dreadnought_Core6.SetBuff(Buff.Type.Stun);
		Dreadnought_Core6.SetScatterVars(3, 0.8f);
		Dreadnought_Core6.SetRapidVars(3, 0.5f, 0.00f, 0);

		Dreadnought_Core7 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core7.SetStandardVars(3f, 0.0f, 1000.0f, 0.01f, true);
		Dreadnought_Core7.SetDamage(1f);
		Dreadnought_Core7.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.0f), EffectController.Singleton.sonarTrail);
		Dreadnought_Core7.SetHomingVars();
		Dreadnought_Core7.SetRapidVars(2, 0.25f ,0);
		Dreadnought_Core7.SetBuff(Buff.Type.Stun);

		Dreadnought_Core8 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core8.SetStandardVars(4.0f, 400.0f, 600.0f, 0.03f, true);
		Dreadnought_Core8.SetDamage (1f);
		Dreadnought_Core8.SetIgnoreArmor();
		Dreadnought_Core8.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 1.0f), EffectController.Singleton.laserTrail);
		Dreadnought_Core8.SetScatterVars(2, KJMath.CIRCLE * 0.22f);
		Dreadnought_Core8.SetRapidVars(4, 0.3f, 0.0f, 1.8f);

		Dreadnought_Core9 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core9.SetStandardVars(4.5f, 369.0f, 370.0f, 0.05f, true);
		Dreadnought_Core9.SetDamage(1f);
		Dreadnought_Core9.SetScatterVars(5, KJMath.CIRCLE * 0.5f);
		Dreadnought_Core9.SetRapidVars(2, 0.25f, 0, 0.0f);
		Dreadnought_Core9.SetGraphicVars(WeaponGraphic.Image.RoundGreen, pinkStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));

		Dreadnought_Core10 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core10.SetStandardVars(4.25f, 500.0f, 150.0f, 1f);
		Dreadnought_Core10.SetDamage(1f);
		Dreadnought_Core10.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.0f));
		Dreadnought_Core10.SetScatterVars(4, 3f);
		Dreadnought_Core10.SetTime(3f);
		Dreadnought_Core10.SetRapidVars(6, 0.4f, 1.5f);

		Dreadnought_Core11 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core11.SetStandardVars(4.0f, 400.0f, 250.0f, 0.2f);
		Dreadnought_Core11.SetDamage(1.25f);
		Dreadnought_Core11.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, Color.white);
		Dreadnought_Core11.SetScatterVars(8, KJMath.CIRCLE * 0.45f);
		Dreadnought_Core11.SetRapidVars(2, 0.5f, 0, 0);
		
		Dreadnought_Core12 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core12.SetStandardVars(3.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Core12.SetDamage(0.25f);
		Dreadnought_Core12.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		Dreadnought_Core12.SetRapidVars(14, 0.12f, 0.9f);
		Dreadnought_Core12.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Core13 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core13.SetStandardVars(3.5f, 600.0f, 200.0f, 0.2f);
		Dreadnought_Core13.SetDamage(1f);
		Dreadnought_Core13.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, Color.white);
		Dreadnought_Core13.SetRapidVars(14, 0.09f, 0.7f);
		Dreadnought_Core13.SetBuff(Buff.Type.Stun);
		
		Dreadnought_Core14 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core14.SetStandardVars(3f,251.0f,250.0f,0.2f);
		Dreadnought_Core14.SetDamage(1f);
		Dreadnought_Core14.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, Color.white);
		Dreadnought_Core14.SetRapidVars(14,0.08f,0.5f,1.5f);
		
		Dreadnought_Core15 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core15.SetStandardVars(3.5f, 550.0f, 250.0f, 0.2f);
		Dreadnought_Core15.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, Color.white);
		Dreadnought_Core15.SetScatterVars(12, KJMath.CIRCLE * 0.6f);
		Dreadnought_Core15.SetRapidVars(2, 0.5f, 0.0f);
		Dreadnought_Core15.SetDamage(0.20f);
		Dreadnought_Core15.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Core16 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core16.SetStandardVars(4.25f, 401.0f, 450.0f, 0.05f, true);
		Dreadnought_Core16.SetDamage(0.65f);
		Dreadnought_Core16.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		Dreadnought_Core16.SetBuff(Buff.Type.Stun);
		Dreadnought_Core16.SetScatterVars(3, 0.8f);
		Dreadnought_Core16.SetRapidVars(3, 0.5f, 0.00f, 0);
		
		Dreadnought_Core17 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core17.SetStandardVars(3f, 0.0f, 1000.0f, 0.01f, true);
		Dreadnought_Core17.SetDamage(1f);
		Dreadnought_Core17.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.0f), EffectController.Singleton.sonarTrail);
		Dreadnought_Core17.SetHomingVars();
		Dreadnought_Core17.SetRapidVars(2, 0.25f ,0);
		Dreadnought_Core17.SetBuff(Buff.Type.Stun);
		
		Dreadnought_Core18 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core18.SetStandardVars(4.0f, 400.0f, 600.0f, 0.03f, true);
		Dreadnought_Core18.SetDamage (1f);
		Dreadnought_Core18.SetIgnoreArmor();
		Dreadnought_Core18.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 1.0f), EffectController.Singleton.laserTrail);
		Dreadnought_Core18.SetScatterVars(2, KJMath.CIRCLE * 0.22f);
		Dreadnought_Core18.SetRapidVars(4, 0.3f, 0.0f, 1.8f);
		
		Dreadnought_Core19 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core19.SetStandardVars(4.5f, 369.0f, 370.0f, 0.05f, true);
		Dreadnought_Core19.SetDamage(0.80f);
		Dreadnought_Core19.SetScatterVars(5, KJMath.CIRCLE * 0.5f);
		Dreadnought_Core19.SetRapidVars(2, 0.25f, 0, 0.0f);
		Dreadnought_Core19.SetGraphicVars(WeaponGraphic.Image.RoundGreen, pinkStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		
		Dreadnought_Core20 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core20.SetStandardVars(4.25f, 500.0f, 150.0f, 1f);
		Dreadnought_Core20.SetDamage(0.8f);
		Dreadnought_Core20.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.0f));
		Dreadnought_Core20.SetScatterVars(4, 3f);
		Dreadnought_Core20.SetTime(3f);
		Dreadnought_Core20.SetRapidVars(6, 0.4f, 1.5f);

		Dreadnought_Core21 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core21.SetStandardVars(3.5f, 400.0f, 200.0f, 0.2f);
		Dreadnought_Core21.SetDamage(0.275f);
		Dreadnought_Core21.SetBuff(Buff.Type.Burn);
		Dreadnought_Core21.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, Color.white);
		Dreadnought_Core21.SetScatterVars(6, KJMath.CIRCLE * 0.45f);
		Dreadnought_Core21.SetRapidVars(2, 0.5f, 0, 0);
		
		Dreadnought_Core22 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core22.SetStandardVars(3f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Core22.SetDamage(0.275f);
		Dreadnought_Core22.SetBuff(Buff.Type.Burn);
		Dreadnought_Core22.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, Color.white);
		Dreadnought_Core22.SetRapidVars(8, 0.12f, 0.9f);
		
		Dreadnought_Core23 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core23.SetStandardVars(3f, 600.0f, 175.0f, 0.2f);
		Dreadnought_Core23.SetDamage(0.275f);
		Dreadnought_Core23.SetBuff(Buff.Type.Burn);
		Dreadnought_Core23.SetGraphicVars(WeaponGraphic.Image.RoundRed, redStart, Color.white);
		Dreadnought_Core23.SetRapidVars(9, 0.09f, 0.9f);
		
		Dreadnought_Core24 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core24.SetStandardVars(2.75f,251.0f,200.0f,0.2f);
		Dreadnought_Core24.SetDamage(1.0f);
		Dreadnought_Core24.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, Color.white);
		Dreadnought_Core24.SetRapidVars(10, 0.08f ,0.5f, 1.5f);
		
		Dreadnought_Core25 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core25.SetStandardVars(3f, 550.0f, 250.0f, 0.2f);
		Dreadnought_Core25.SetGraphicVars(WeaponGraphic.Image.StarYellow, yellowStart, Color.white);
		Dreadnought_Core25.SetScatterVars(10, KJMath.CIRCLE * 0.6f);
		Dreadnought_Core25.SetRapidVars(2, 0.5f, 0.0f);
		Dreadnought_Core25.SetDamage(1.00f);
		
		Dreadnought_Core26 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core26.SetStandardVars(3.75f, 251.0f, 250.0f, 0.05f, true);
		Dreadnought_Core26.SetDamage(1.00f);
		Dreadnought_Core26.SetGraphicVars(WeaponGraphic.Image.SonarYellow, yellowStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		Dreadnought_Core26.SetScatterVars(5, 1.2f);
		Dreadnought_Core26.SetRapidVars(2, 0.5f, 0.00f, 0);
		
		Dreadnought_Core27 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core27.SetStandardVars(3f, 0.0f, 500.0f, 0.01f, true);
		Dreadnought_Core27.SetDamage(1.00f);
		Dreadnought_Core27.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, yellowStart, new Color(1.0f, 0.4f, 0.1f, 0.0f), EffectController.Singleton.sonarTrail);
		Dreadnought_Core27.SetHomingVars();
		Dreadnought_Core27.SetRapidVars(2, 0.25f ,0);
		
		Dreadnought_Core28 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core28.SetStandardVars(3.5f, 100.0f, 575.0f, 0.03f, true);
		Dreadnought_Core28.SetDamage (0.85f);
		Dreadnought_Core28.SetGraphicVars(WeaponGraphic.Image.RoundBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.0f), EffectController.Singleton.laserTrail);
		Dreadnought_Core28.SetScatterVars(2, KJMath.CIRCLE * 0.22f);
		Dreadnought_Core28.SetBuff(Buff.Type.Stun);
		Dreadnought_Core28.SetRapidVars(5, 0.15f, 0.0f, 1.8f);
		
		Dreadnought_Core29 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core29.SetStandardVars(3.75f, 375.0f, 376.0f, 0.05f, true);
		Dreadnought_Core29.SetDamage(1.00f);
		Dreadnought_Core29.SetScatterVars(2, KJMath.CIRCLE * 0.15f);
		Dreadnought_Core29.SetRapidVars(5, 0.25f, 0, 0.5f);
		Dreadnought_Core29.SetBuff(Buff.Type.Stun);
		Dreadnought_Core29.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.0f));
		
		Dreadnought_Core30 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core30.SetStandardVars(3.0f, 500.0f, 150.0f, 1f);
		Dreadnought_Core30.SetDamage(1.0f);
		Dreadnought_Core30.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.0f));
		Dreadnought_Core30.SetScatterVars(3, 3f);
		Dreadnought_Core30.SetTime(3f);
		Dreadnought_Core30.SetBuff(Buff.Type.Stun);
		Dreadnought_Core30.SetRapidVars(5, 0.4f, 1.2f);

		Dreadnought_Core31 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core31.SetStandardVars(3.5f, 400.0f, 200.0f, 0.2f);
		Dreadnought_Core31.SetDamage(0.275f);
		Dreadnought_Core31.SetBuff(Buff.Type.Burn);
		Dreadnought_Core31.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, Color.white);
		Dreadnought_Core31.SetScatterVars(6, KJMath.CIRCLE * 0.45f);
		Dreadnought_Core31.SetRapidVars(2, 0.5f, 0, 0);
		
		Dreadnought_Core32 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core32.SetStandardVars(3f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Core32.SetDamage(0.275f);
		Dreadnought_Core32.SetBuff(Buff.Type.Burn);
		Dreadnought_Core32.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, Color.white);
		Dreadnought_Core32.SetRapidVars(8, 0.12f, 0.9f);
		
		Dreadnought_Core33 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core33.SetStandardVars(3f, 600.0f, 175.0f, 0.2f);
		Dreadnought_Core33.SetDamage(0.275f);
		Dreadnought_Core33.SetBuff(Buff.Type.Burn);
		Dreadnought_Core33.SetGraphicVars(WeaponGraphic.Image.RoundRed, redStart, Color.white);
		Dreadnought_Core33.SetRapidVars(9, 0.09f, 0.9f);
		
		Dreadnought_Core34 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core34.SetStandardVars(2.75f,251.0f,200.0f,0.2f);
		Dreadnought_Core34.SetDamage(1.0f);
		Dreadnought_Core34.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, Color.white);
		Dreadnought_Core34.SetRapidVars(10, 0.08f ,0.5f, 1.5f);
		
		Dreadnought_Core35 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core35.SetStandardVars(3f, 550.0f, 250.0f, 0.2f);
		Dreadnought_Core35.SetGraphicVars(WeaponGraphic.Image.StarYellow, yellowStart, Color.white);
		Dreadnought_Core35.SetScatterVars(10, KJMath.CIRCLE * 0.6f);
		Dreadnought_Core35.SetRapidVars(2, 0.5f, 0.0f);
		Dreadnought_Core35.SetDamage(1.00f);
		
		Dreadnought_Core36 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core36.SetStandardVars(3.75f, 251.0f, 250.0f, 0.05f, true);
		Dreadnought_Core36.SetDamage(1.00f);
		Dreadnought_Core36.SetGraphicVars(WeaponGraphic.Image.SonarYellow, yellowStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		Dreadnought_Core36.SetScatterVars(5, 1.2f);
		Dreadnought_Core36.SetRapidVars(2, 0.5f, 0.00f, 0);
		
		Dreadnought_Core37 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core37.SetStandardVars(3f, 0.0f, 500.0f, 0.01f, true);
		Dreadnought_Core37.SetDamage(1.00f);
		Dreadnought_Core37.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, yellowStart, new Color(1.0f, 0.4f, 0.1f, 0.0f), EffectController.Singleton.sonarTrail);
		Dreadnought_Core37.SetHomingVars();
		Dreadnought_Core37.SetRapidVars(2, 0.25f ,0);
		
		Dreadnought_Core38 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core38.SetStandardVars(3.5f, 100.0f, 575.0f, 0.03f, true);
		Dreadnought_Core38.SetDamage (0.85f);
		Dreadnought_Core38.SetGraphicVars(WeaponGraphic.Image.RoundBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.0f), EffectController.Singleton.laserTrail);
		Dreadnought_Core38.SetScatterVars(2, KJMath.CIRCLE * 0.22f);
		Dreadnought_Core38.SetBuff(Buff.Type.Stun);
		Dreadnought_Core38.SetRapidVars(5, 0.15f, 0.0f, 1.8f);
		
		Dreadnought_Core39 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core39.SetStandardVars(3.75f, 375.0f, 376.0f, 0.05f, true);
		Dreadnought_Core39.SetDamage(1.00f);
		Dreadnought_Core39.SetScatterVars(2, KJMath.CIRCLE * 0.15f);
		Dreadnought_Core39.SetRapidVars(5, 0.25f, 0, 0.5f);
		Dreadnought_Core39.SetBuff(Buff.Type.Stun);
		Dreadnought_Core39.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.0f));
		
		Dreadnought_Core40 = holderObject.AddComponent<Weapon>();
		Dreadnought_Core40.SetStandardVars(3.0f, 500.0f, 150.0f, 1f);
		Dreadnought_Core40.SetDamage(1.0f);
		Dreadnought_Core40.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.0f));
		Dreadnought_Core40.SetScatterVars(3, 3f);
		Dreadnought_Core40.SetTime(3f);
		Dreadnought_Core40.SetBuff(Buff.Type.Stun);
		Dreadnought_Core40.SetRapidVars(5, 0.4f, 1.2f);

		Dreadnought_Turret1 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret1.SetStandardVars(2.75f, 400.0f, 300.0f, 0.2f);
		Dreadnought_Turret1.SetDamage(0.275f);
		Dreadnought_Turret1.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, Color.white);
		Dreadnought_Turret1.SetScatterVars(6, KJMath.CIRCLE * 0.25f);
		Dreadnought_Turret1.SetRapidVars(2, 0.25f, 0, 0);
		Dreadnought_Turret1.SetBuff(Buff.Type.Burn);

		Dreadnought_Turret2 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret2.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret2.SetDamage(0.7f);
		Dreadnought_Turret2.SetGraphicVars(WeaponGraphic.Image.BoomerGreen, greenStart, Color.white);
		Dreadnought_Turret2.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret2.SetTime(4.0f);
		Dreadnought_Turret2.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret2.SetDoubleHitRadius();

		Dreadnought_Turret3 = holderObject.AddComponent<Weapon>(); 
		Dreadnought_Turret3.SetStandardVars(2f, 250.0f, 600.0f, 0.50f);
		Dreadnought_Turret3.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret3.SetHomingVars();
		Dreadnought_Turret3.SetDamage(0.5f);
		Dreadnought_Turret3.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		Dreadnought_Turret3.SetBuff(Buff.Type.Stun, 100 , 1 , 2.5f);

		Dreadnought_Turret4 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret4.SetStandardVars(3f,350.0f,200.0f,0.2f);
		Dreadnought_Turret4.SetDamage(1.0f);
		Dreadnought_Turret4.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, Color.white);
		Dreadnought_Turret4.SetRapidVars(10,0.08f,0.5f,1.5f);
		Dreadnought_Turret4.SetBuff(Buff.Type.Stun,100,1f,0.5f);

		Dreadnought_Turret5 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret5.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Turret5.SetDamage(0.75f);
		Dreadnought_Turret5.SetGraphicVars(WeaponGraphic.Image.StarYellow, yellowStart, Color.white);
		Dreadnought_Turret5.SetRapidVars(9, 0.10f, 0.0f, 1.8f);

		Dreadnought_Turret6 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret6.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Turret6.SetDamage(0.2f);
		Dreadnought_Turret6.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		Dreadnought_Turret6.SetRapidVars(9, 0.10f, 0.0f, 1.8f);
		Dreadnought_Turret6.SetBuff(Buff.Type.Burn);

		Dreadnought_Turret7 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret7.SetStandardVars(3.5f, 0.0f, 700.0f, 0.03f, false);
		Dreadnought_Turret7.SetGraphicVars(WeaponGraphic.Image.MissileBlue, blueStart, blueStart, EffectController.Singleton.missileTrail);
		Dreadnought_Turret7.SetDamage(0.6f);
		Dreadnought_Turret7.SetRapidVars(5, 0.15f, 0.6f);
		Dreadnought_Turret7.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Dreadnought_Turret7.SetBuff(Buff.Type.Stun);

		Dreadnought_Turret8 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret8.SetStandardVars(4.0f, 0.0f, 700.0f, 0.005f, false);
		Dreadnought_Turret8.SetGraphicVars(WeaponGraphic.Image.MissileYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret8.SetHomingVars();
		Dreadnought_Turret8.SetDamage(1.0f);
		Dreadnought_Turret8.SetEjectionVars();
		Dreadnought_Turret8.SetRapidVars(2, 0.35f, 1.8f, 0);

		Dreadnought_Turret9 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret9.SetStandardVars(3.0f, 400.0f, 150.0f, 0.15f);
		Dreadnought_Turret9.SetDamage(1.35f);
		Dreadnought_Turret9.SetGraphicVars(WeaponGraphic.Image.StarPink, pinkStart, Color.white);
		Dreadnought_Turret9.SetRapidVars(9, 0.25f, 0.0f, 0.0f);
		Dreadnought_Turret9.SetScatterVars(6, KJMath.CIRCLE * 0.35f);

		Dreadnought_Turret10 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret10.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret10.SetDamage(0.2f);
		Dreadnought_Turret10.SetGraphicVars(WeaponGraphic.Image.BoomerRed, redStart, Color.white);
		Dreadnought_Turret10.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret10.SetTime(4.0f);
		Dreadnought_Turret10.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret10.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret10.SetDoubleHitRadius();

		Dreadnought_Turret11 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret11.SetStandardVars(3f, 400.0f, 100.0f, 0.7f);
		Dreadnought_Turret11.SetDamage(1.5f);
		Dreadnought_Turret11.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, Color.white);
		Dreadnought_Turret11.SetRapidVars(11, 0.075f, 0.0f, 2.55f);

		Dreadnought_Turret12 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret12.SetStandardVars(5f, 400.0f, 200.0f, 0.2f);
		Dreadnought_Turret12.SetDamage(7.0f);
		Dreadnought_Turret12.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(206, 255, 215, 255), new Color32(0, 255, 86, 255));
		Dreadnought_Turret12.SetBeamVars(1.4f);

		Dreadnought_Turret13 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret13.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret13.SetDamage(0.7f);
		Dreadnought_Turret13.SetGraphicVars(WeaponGraphic.Image.BoomerYellow, yellowStart, Color.white);
		Dreadnought_Turret13.SetTime(4.0f);
		Dreadnought_Turret13.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret13.SetRapidVars(4, 0.2f, 0.0f, 2.0f);
		Dreadnought_Turret13.SetDoubleHitRadius();

		Dreadnought_Turret14 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret14.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret14.SetDamage(0.7f);
		Dreadnought_Turret14.SetGraphicVars(WeaponGraphic.Image.BoomerGreen, greenStart, Color.white);
		Dreadnought_Turret14.SetTime(4.0f);
		Dreadnought_Turret14.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret14.SetRapidVars(4, 0.2f, 0.0f, -2.0f);
		Dreadnought_Turret14.SetDoubleHitRadius();

		Dreadnought_Turret15 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret15.SetStandardVars(3.0f, 100.0f, 150.0f, 0.03f);
		Dreadnought_Turret15.SetDamage(0.2f);
		Dreadnought_Turret15.SetGraphicVars(WeaponGraphic.Image.BoomerRed, redStart, Color.white);
		Dreadnought_Turret15.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret15.SetTime(7.0f);
		Dreadnought_Turret15.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret15.SetBoomerang(2.6f, 0.0f);
		Dreadnought_Turret15.SetDoubleHitRadius();

		Dreadnought_Turret16 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret16.SetStandardVars(2.0f, 0.0f, 600.0f, 0.05f);
		Dreadnought_Turret16.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.lineTrail);
		Dreadnought_Turret16.SetRapidVars(3, 0.2f, 0.0f, 1.5f);
		Dreadnought_Turret16.SetDamage(0.3f);
		Dreadnought_Turret16.SetScatterVars(3, 1.0f);
		Dreadnought_Turret16.SetBuff(Buff.Type.Acid);
		Dreadnought_Turret16.SetPiercing();

		Dreadnought_Turret17 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret17.SetStandardVars(3.0f, 525.0f, 225.0f, 0.2f);
		Dreadnought_Turret17.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, Color.white);
		Dreadnought_Turret17.SetScatterVars(10, KJMath.CIRCLE * 0.40f);
		Dreadnought_Turret17.SetRapidVars(2, 0.3f, 0.0f);
		Dreadnought_Turret17.SetDamage(1.0f);

		Dreadnought_Turret18 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret18.SetStandardVars(2.0f, 0.0f, 600.0f, 0.05f);
		Dreadnought_Turret18.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, redStart, EffectController.Singleton.lineTrail);
		Dreadnought_Turret18.SetRapidVars(3, 0.2f, 0.0f, 1.5f);
		Dreadnought_Turret18.SetDamage(0.15f);
		Dreadnought_Turret18.SetScatterVars(3, 1.0f);
		Dreadnought_Turret18.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret18.SetPiercing();

		Dreadnought_Turret19 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret19.SetStandardVars(2.0f, 400.0f, 100.0f, 0.02f);
		Dreadnought_Turret19.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.0f), EffectController.Singleton.lightWaveTrail);
		Dreadnought_Turret19.SetScatterVars(24, KJMath.CIRCLE * 0.08f);
		Dreadnought_Turret19.SetRapidVars(1, 0.15f, 0.0f);
		Dreadnought_Turret19.SetDamage(0.10f);
		Dreadnought_Turret19.SetBuff(Buff.Type.Acid);

		Dreadnought_Turret20 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret20.SetStandardVars(2.0f, 400.0f, 100.0f, 0.02f);
		Dreadnought_Turret20.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.0f), EffectController.Singleton.lightWaveTrail);
		Dreadnought_Turret20.SetScatterVars(24, KJMath.CIRCLE * 0.08f);
		Dreadnought_Turret20.SetRapidVars(1, 0.15f, 0.0f);
		Dreadnought_Turret20.SetDamage(0.10f);
		Dreadnought_Turret20.SetBuff(Buff.Type.Burn);

		Dreadnought_Turret21 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret21.SetStandardVars(2.75f, 400.0f, 300.0f, 0.2f);
		Dreadnought_Turret21.SetDamage(0.2f);
		Dreadnought_Turret21.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, Color.white);
		Dreadnought_Turret21.SetScatterVars(6, KJMath.CIRCLE * 0.25f);
		Dreadnought_Turret21.SetRapidVars(2, 0.25f, 0, 0);
		Dreadnought_Turret21.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Turret22 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret22.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret22.SetDamage(0.7f);
		Dreadnought_Turret22.SetGraphicVars(WeaponGraphic.Image.BoomerGreen, greenStart, Color.white);
		Dreadnought_Turret22.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret22.SetTime(4.0f);
		Dreadnought_Turret22.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret22.SetDoubleHitRadius();
		
		Dreadnought_Turret23 = holderObject.AddComponent<Weapon>(); 
		Dreadnought_Turret23.SetStandardVars(2f, 250.0f, 600.0f, 0.50f);
		Dreadnought_Turret23.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret23.SetHomingVars();
		Dreadnought_Turret23.SetDamage(0.5f);
		Dreadnought_Turret23.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		Dreadnought_Turret23.SetBuff(Buff.Type.Stun, 100 , 1 , 2.5f);
		
		Dreadnought_Turret24 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret24.SetStandardVars(3f,350.0f,200.0f,0.2f);
		Dreadnought_Turret24.SetDamage(1.0f);
		Dreadnought_Turret24.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, Color.white);
		Dreadnought_Turret24.SetRapidVars(10,0.08f,0.5f,1.5f);
		Dreadnought_Turret24.SetBuff(Buff.Type.Stun,100,1f,0.5f);
		
		Dreadnought_Turret25 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret25.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Turret25.SetDamage(0.75f);
		Dreadnought_Turret25.SetGraphicVars(WeaponGraphic.Image.StarYellow, yellowStart, Color.white);
		Dreadnought_Turret25.SetRapidVars(9, 0.10f, 0.0f, 1.8f);
		
		Dreadnought_Turret26 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret26.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Turret26.SetDamage(0.2f);
		Dreadnought_Turret26.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		Dreadnought_Turret26.SetRapidVars(9, 0.10f, 0.0f, 1.8f);
		Dreadnought_Turret26.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Turret27 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret27.SetStandardVars(3.5f, 0.0f, 700.0f, 0.03f, false);
		Dreadnought_Turret27.SetGraphicVars(WeaponGraphic.Image.MissileBlue, blueStart, blueStart, EffectController.Singleton.missileTrail);
		Dreadnought_Turret27.SetDamage(0.6f);
		Dreadnought_Turret27.SetRapidVars(5, 0.15f, 0.6f);
		Dreadnought_Turret27.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Dreadnought_Turret27.SetBuff(Buff.Type.Stun);
		
		Dreadnought_Turret28 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret28.SetStandardVars(4.0f, 0.0f, 700.0f, 0.005f, false);
		Dreadnought_Turret28.SetGraphicVars(WeaponGraphic.Image.MissileYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret28.SetHomingVars();
		Dreadnought_Turret28.SetDamage(1.0f);
		Dreadnought_Turret28.SetEjectionVars();
		Dreadnought_Turret28.SetRapidVars(2, 0.35f, 1.8f, 0);
		
		Dreadnought_Turret29 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret29.SetStandardVars(3.0f, 400.0f, 150.0f, 0.15f);
		Dreadnought_Turret29.SetDamage(1.35f);
		Dreadnought_Turret29.SetGraphicVars(WeaponGraphic.Image.StarPink, pinkStart, Color.white);
		Dreadnought_Turret29.SetRapidVars(9, 0.25f, 0.0f, 0.0f);
		Dreadnought_Turret29.SetScatterVars(6, KJMath.CIRCLE * 0.35f);
		
		Dreadnought_Turret30 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret30.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret30.SetDamage(0.2f);
		Dreadnought_Turret30.SetGraphicVars(WeaponGraphic.Image.BoomerRed, redStart, Color.white);
		Dreadnought_Turret30.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret30.SetTime(4.0f);
		Dreadnought_Turret30.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret30.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret30.SetDoubleHitRadius();
		
		Dreadnought_Turret31 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret31.SetStandardVars(3f, 400.0f, 100.0f, 0.7f);
		Dreadnought_Turret31.SetDamage(1.5f);
		Dreadnought_Turret31.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, Color.white);
		Dreadnought_Turret31.SetRapidVars(11, 0.075f, 0.0f, 2.55f);
		
		Dreadnought_Turret32 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret32.SetStandardVars(5f, 400.0f, 200.0f, 0.2f);
		Dreadnought_Turret32.SetDamage(7.0f);
		Dreadnought_Turret32.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(206, 255, 215, 255), new Color32(0, 255, 86, 255));
		Dreadnought_Turret32.SetBeamVars(1.4f);
		
		Dreadnought_Turret33 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret33.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret33.SetDamage(0.7f);
		Dreadnought_Turret33.SetGraphicVars(WeaponGraphic.Image.BoomerYellow, yellowStart, Color.white);
		Dreadnought_Turret33.SetTime(4.0f);
		Dreadnought_Turret33.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret33.SetRapidVars(4, 0.2f, 0.0f, 2.0f);
		Dreadnought_Turret33.SetDoubleHitRadius();
		
		Dreadnought_Turret34 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret34.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret34.SetDamage(0.7f);
		Dreadnought_Turret34.SetGraphicVars(WeaponGraphic.Image.BoomerGreen, greenStart, Color.white);
		Dreadnought_Turret34.SetTime(4.0f);
		Dreadnought_Turret34.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret34.SetRapidVars(4, 0.2f, 0.0f, -2.0f);
		Dreadnought_Turret34.SetDoubleHitRadius();
		
		Dreadnought_Turret35 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret35.SetStandardVars(3.0f, 100.0f, 150.0f, 0.03f);
		Dreadnought_Turret35.SetDamage(0.2f);
		Dreadnought_Turret35.SetGraphicVars(WeaponGraphic.Image.BoomerRed, redStart, Color.white);
		Dreadnought_Turret35.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret35.SetTime(7.0f);
		Dreadnought_Turret35.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret35.SetBoomerang(2.6f, 0.0f);
		Dreadnought_Turret35.SetDoubleHitRadius();
		
		Dreadnought_Turret36 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret36.SetStandardVars(2.0f, 0.0f, 600.0f, 0.05f);
		Dreadnought_Turret36.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.lineTrail);
		Dreadnought_Turret36.SetRapidVars(3, 0.2f, 0.0f, 1.5f);
		Dreadnought_Turret36.SetDamage(0.3f);
		Dreadnought_Turret36.SetScatterVars(3, 1.0f);
		Dreadnought_Turret36.SetBuff(Buff.Type.Acid);
		Dreadnought_Turret36.SetPiercing();
		
		Dreadnought_Turret37 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret37.SetStandardVars(3.0f, 525.0f, 225.0f, 0.2f);
		Dreadnought_Turret37.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, Color.white);
		Dreadnought_Turret37.SetScatterVars(10, KJMath.CIRCLE * 0.40f);
		Dreadnought_Turret37.SetRapidVars(2, 0.3f, 0.0f);
		Dreadnought_Turret37.SetDamage(1.0f);
		
		Dreadnought_Turret38 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret38.SetStandardVars(2.0f, 0.0f, 600.0f, 0.05f);
		Dreadnought_Turret38.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, redStart, EffectController.Singleton.lineTrail);
		Dreadnought_Turret38.SetRapidVars(3, 0.2f, 0.0f, 1.5f);
		Dreadnought_Turret38.SetDamage(0.15f);
		Dreadnought_Turret38.SetScatterVars(3, 1.0f);
		Dreadnought_Turret38.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret38.SetPiercing();
		
		Dreadnought_Turret39 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret39.SetStandardVars(2.0f, 400.0f, 100.0f, 0.02f);
		Dreadnought_Turret39.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.0f), EffectController.Singleton.lightWaveTrail);
		Dreadnought_Turret39.SetScatterVars(24, KJMath.CIRCLE * 0.08f);
		Dreadnought_Turret39.SetRapidVars(1, 0.15f, 0.0f);
		Dreadnought_Turret39.SetDamage(0.10f);
		Dreadnought_Turret39.SetBuff(Buff.Type.Acid);
		
		Dreadnought_Turret40 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret40.SetStandardVars(2.0f, 400.0f, 100.0f, 0.02f);
		Dreadnought_Turret40.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.0f), EffectController.Singleton.lightWaveTrail);
		Dreadnought_Turret40.SetScatterVars(24, KJMath.CIRCLE * 0.08f);
		Dreadnought_Turret40.SetRapidVars(1, 0.15f, 0.0f);
		Dreadnought_Turret40.SetDamage(0.10f);
		Dreadnought_Turret40.SetBuff(Buff.Type.Burn);

		Dreadnought_Turret41 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret41.SetStandardVars(3.0f, 400.0f, 300.0f, 0.2f);
		Dreadnought_Turret41.SetDamage(0.2f);
		Dreadnought_Turret41.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, Color.white);
		Dreadnought_Turret41.SetScatterVars(2, KJMath.CIRCLE * 0.25f);
		Dreadnought_Turret41.SetRapidVars(5, 0.25f, 0, 1.5f);
		Dreadnought_Turret41.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Turret42 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret42.SetStandardVars(2.75f, 250.0f, 100.0f, 0.2f);
		Dreadnought_Turret42.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.engageTrail);
		Dreadnought_Turret42.SetHomingVars();
		Dreadnought_Turret42.SetDamage(0.5f);
		Dreadnought_Turret42.SetEngage(10);
		Dreadnought_Turret42.SetBuff(Buff.Type.Stun);
		Dreadnought_Turret42.SetExplosionVars(blueStart, new Color(0.0f, 0.2f, 0.6f, 0.1f));
		
		Dreadnought_Turret43 = holderObject.AddComponent<Weapon>(); 
		Dreadnought_Turret43.SetStandardVars(2f, 250.0f, 400.0f, 0.50f);
		Dreadnought_Turret43.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret43.SetHomingVars();
		Dreadnought_Turret43.SetDamage(0.5f);
		Dreadnought_Turret43.SetRapidVars(3, 0.5f, 0, 0);
		Dreadnought_Turret43.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		Dreadnought_Turret43.SetBuff(Buff.Type.Stun, 100 , 1 , 2.5f);
		
		Dreadnought_Turret44 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret44.SetStandardVars(3f,350.0f,225.0f,0.2f);
		Dreadnought_Turret44.SetDamage(1.0f);
		Dreadnought_Turret44.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, Color.white);
		Dreadnought_Turret44.SetRapidVars(14,0.08f,0.25f,1.5f);
		Dreadnought_Turret44.SetBuff(Buff.Type.Stun,100,1f,0.5f);
		
		Dreadnought_Turret45 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret45.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Turret45.SetDamage(1.5f);
		Dreadnought_Turret45.SetGraphicVars(WeaponGraphic.Image.StarPink, yellowStart, Color.white);
		Dreadnought_Turret45.SetScatterVars(3, KJMath.CIRCLE * 0.5f);
		Dreadnought_Turret45.SetRapidVars(12, 0.16f, 0.0f, 2.5f);
		
		Dreadnought_Turret46 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret46.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Turret46.SetDamage(0.2f);
		Dreadnought_Turret46.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		Dreadnought_Turret46.SetScatterVars(3, KJMath.CIRCLE * 0.5f);
		Dreadnought_Turret46.SetRapidVars(12, 0.12f, 0.0f, 2.2f);
		Dreadnought_Turret46.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Turret47 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret47.SetStandardVars(3.5f, 0.0f, 625.0f, 0.03f, false);
		Dreadnought_Turret47.SetGraphicVars(WeaponGraphic.Image.MissileBlue, blueStart, blueStart, EffectController.Singleton.missileTrail);
		Dreadnought_Turret47.SetDamage(0.6f);
		Dreadnought_Turret47.SetRapidVars(6, 0.100f, 0.6f);
		Dreadnought_Turret47.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Dreadnought_Turret47.SetBuff(Buff.Type.Stun);
		
		Dreadnought_Turret48 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret48.SetStandardVars(4.0f, 0.0f, 700.0f, 0.005f, false);
		Dreadnought_Turret48.SetGraphicVars(WeaponGraphic.Image.MissileYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret48.SetDamage(1.0f);
		Dreadnought_Turret48.SetEjectionVars();
		Dreadnought_Turret48.SetRapidVars(10, 0.275f, 0.6f, 0);
		
		Dreadnought_Turret49 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret49.SetStandardVars(3.0f, 400.0f, 150.0f, 0.15f);
		Dreadnought_Turret49.SetDamage(1.35f);
		Dreadnought_Turret49.SetGraphicVars(WeaponGraphic.Image.StarPink, pinkStart, Color.white);
		Dreadnought_Turret49.SetRapidVars(5, 0.225f, 0.0f, 0.0f);
		Dreadnought_Turret49.SetScatterVars(5, KJMath.CIRCLE * 0.35f);
		
		Dreadnought_Turret50 = holderObject.AddComponent<Weapon>(); 
		Dreadnought_Turret50.SetStandardVars(2f, 0.0f, 400.0f, 0.50f);
		Dreadnought_Turret50.SetGraphicVars(WeaponGraphic.Image.MissileRed, redStart, redStart, EffectController.Singleton.missileTrail);
		Dreadnought_Turret50.SetRapidVars(2, 0.20f, 0.0f, 0.0f);
		Dreadnought_Turret50.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret50.SetDamage(0.175f);
		Dreadnought_Turret50.SetHomingVars();
		Dreadnought_Turret50.SetExplosionVars(redStart, redStart);
		
		Dreadnought_Turret51 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret51.SetStandardVars(3f, 400.0f, 100.0f, 0.7f);
		Dreadnought_Turret51.SetDamage(1.4f);
		Dreadnought_Turret51.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, pinkStart, Color.white);
		Dreadnought_Turret51.SetRapidVars(9, 0.075f, 0.0f, 2f);
		
		Dreadnought_Turret52 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret52.SetStandardVars(5f, 400.0f, 200.0f, 0.2f);
		Dreadnought_Turret52.SetDamage(7.0f);
		Dreadnought_Turret52.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(206, 255, 215, 255), new Color32(0, 255, 86, 255));
		Dreadnought_Turret52.SetBeamVars(1.2f);
		
		Dreadnought_Turret53 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret53.SetStandardVars(2.0f, -800.0f, 750.0f, 0.025f, false);
		Dreadnought_Turret53.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret53.SetHomingVars();
		Dreadnought_Turret53.SetScatterVars(3, 6f);
		Dreadnought_Turret53.SetDamage(1.0f);
		Dreadnought_Turret53.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Dreadnought_Turret54 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret54.SetStandardVars(3.0f, -800.0f, 500.0f, 0.05f, false);
		Dreadnought_Turret54.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret54.SetDamage(0.175f);
		Dreadnought_Turret54.SetRapidVars(3, 0.175f, 0.5f);
		Dreadnought_Turret54.SetHomingVars();
		Dreadnought_Turret54.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Dreadnought_Turret54.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Turret55 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret55.SetStandardVars(3.5f, 0.0f, 650.0f, 0.03f, false);
		Dreadnought_Turret55.SetGraphicVars(WeaponGraphic.Image.MissilePink, pinkStart, pinkStart, EffectController.Singleton.missileTrail);
		Dreadnought_Turret55.SetDamage(1.0f);
		Dreadnought_Turret55.SetRapidVars(5, 0.15f, 0.6f);
		Dreadnought_Turret55.SetExplosionVars(pinkStart, pinkStart);
		
		Dreadnought_Turret56 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret56.SetStandardVars(2.0f, 0.0f, 500.0f, 0.05f);
		Dreadnought_Turret56.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.lineTrail);
		Dreadnought_Turret56.SetRapidVars(4, 0.175f, 0.0f, 1.5f);
		Dreadnought_Turret56.SetDamage(0.3f);
		Dreadnought_Turret56.SetScatterVars(2, 1.0f);
		Dreadnought_Turret56.SetBuff(Buff.Type.Acid);
		Dreadnought_Turret56.SetPiercing();
		
		Dreadnought_Turret57 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret57.SetStandardVars(3.0f, 525.0f, 225.0f, 0.2f);
		Dreadnought_Turret57.SetGraphicVars(WeaponGraphic.Image.RoundGreen, pinkStart, Color.white);
		Dreadnought_Turret57.SetScatterVars(10, KJMath.CIRCLE * 0.45f);
		Dreadnought_Turret57.SetRapidVars(2, 0.3f, 0.0f);
		Dreadnought_Turret57.SetDamage(1.0f);
		
		Dreadnought_Turret58 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret58.SetStandardVars(2.0f, 0.0f, 500.0f, 0.05f);
		Dreadnought_Turret58.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, pinkStart, EffectController.Singleton.lineTrail);
		Dreadnought_Turret58.SetRapidVars(4, 0.2f, 0.0f, 1.5f);
		Dreadnought_Turret58.SetDamage(1.35f);
		Dreadnought_Turret58.SetScatterVars(2, 1.0f);
		Dreadnought_Turret58.SetPiercing();
		
		Dreadnought_Turret59 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret59.SetStandardVars(2.5f, 400.0f, 50.0f, 0.02f);
		Dreadnought_Turret59.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.0f), EffectController.Singleton.lightWaveTrail);
		Dreadnought_Turret59.SetScatterVars(22, KJMath.CIRCLE * 0.12f);
		Dreadnought_Turret59.SetDamage(0.20f);
		Dreadnought_Turret59.SetBuff(Buff.Type.Acid);
		
		Dreadnought_Turret60 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret60.SetStandardVars(2.5f, 400.0f, 50.0f, 0.02f);
		Dreadnought_Turret60.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.0f), EffectController.Singleton.lightWaveTrail);
		Dreadnought_Turret60.SetScatterVars(22, KJMath.CIRCLE * 0.12f);
		Dreadnought_Turret60.SetDamage(0.20f);
		Dreadnought_Turret60.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Turret61 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret61.SetStandardVars(3.0f, 400.0f, 300.0f, 0.2f);
		Dreadnought_Turret61.SetDamage(0.3f);
		Dreadnought_Turret61.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, Color.white);
		Dreadnought_Turret61.SetScatterVars(2, KJMath.CIRCLE * 0.25f);
		Dreadnought_Turret61.SetRapidVars(6, 0.25f, 0, 0);
		Dreadnought_Turret61.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Turret62 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret62.SetStandardVars(2.75f, 250.0f, 100.0f, 0.2f);
		Dreadnought_Turret62.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.engageTrail);
		Dreadnought_Turret62.SetHomingVars();
		Dreadnought_Turret62.SetDamage(1f);
		Dreadnought_Turret62.SetEngage(12);
		Dreadnought_Turret62.SetBuff(Buff.Type.Stun);
		Dreadnought_Turret62.SetExplosionVars(blueStart, new Color(0.0f, 0.2f, 0.6f, 0.1f));
		
		Dreadnought_Turret63 = holderObject.AddComponent<Weapon>(); 
		Dreadnought_Turret63.SetStandardVars(2f, 250.0f, 400.0f, 0.50f);
		Dreadnought_Turret63.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret63.SetHomingVars();
		Dreadnought_Turret63.SetDamage(1f);
		Dreadnought_Turret63.SetRapidVars(4, 0.5f, 0, 0);
		Dreadnought_Turret63.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		Dreadnought_Turret63.SetBuff(Buff.Type.Stun, 100 , 1 , 2.5f);
		
		Dreadnought_Turret64 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret64.SetStandardVars(3f,350.0f,225.0f,0.2f);
		Dreadnought_Turret64.SetDamage(1.0f);
		Dreadnought_Turret64.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, Color.white);
		Dreadnought_Turret64.SetRapidVars(16,0.08f,0.25f,1.5f);
		Dreadnought_Turret64.SetBuff(Buff.Type.Stun,100,1f,0.5f);
		
		Dreadnought_Turret65 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret65.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Turret65.SetDamage(1.5f);
		Dreadnought_Turret65.SetGraphicVars(WeaponGraphic.Image.StarPink, yellowStart, Color.white);
		Dreadnought_Turret65.SetScatterVars(4, KJMath.CIRCLE * 0.5f);
		Dreadnought_Turret65.SetRapidVars(14, 0.16f, 0.0f, 2.5f);
		
		Dreadnought_Turret66 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret66.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		Dreadnought_Turret66.SetDamage(0.3f);
		Dreadnought_Turret66.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		Dreadnought_Turret66.SetScatterVars(4, KJMath.CIRCLE * 0.5f);
		Dreadnought_Turret66.SetRapidVars(14, 0.12f, 0.0f, 2.2f);
		Dreadnought_Turret66.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Turret67 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret67.SetStandardVars(3.5f, 0.0f, 625.0f, 0.03f, false);
		Dreadnought_Turret67.SetGraphicVars(WeaponGraphic.Image.MissileBlue, blueStart, blueStart, EffectController.Singleton.missileTrail);
		Dreadnought_Turret67.SetDamage(1f);
		Dreadnought_Turret67.SetRapidVars(8, 0.100f, 0.6f);
		Dreadnought_Turret67.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Dreadnought_Turret67.SetBuff(Buff.Type.Stun);
		
		Dreadnought_Turret68 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret68.SetStandardVars(4.0f, 0.0f, 700.0f, 0.005f, false);
		Dreadnought_Turret68.SetGraphicVars(WeaponGraphic.Image.MissileYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret68.SetDamage(1.0f);
		Dreadnought_Turret68.SetEjectionVars();
		Dreadnought_Turret68.SetRapidVars(12, 0.275f, 0.6f, 0);
		
		Dreadnought_Turret69 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret69.SetStandardVars(3.0f, 400.0f, 150.0f, 0.15f);
		Dreadnought_Turret69.SetDamage(1.5f);
		Dreadnought_Turret69.SetGraphicVars(WeaponGraphic.Image.StarPink, pinkStart, Color.white);
		Dreadnought_Turret69.SetRapidVars(5, 0.225f, 0.0f, 0.0f);
		Dreadnought_Turret69.SetScatterVars(7, KJMath.CIRCLE * 0.35f);
		
		Dreadnought_Turret70 = holderObject.AddComponent<Weapon>(); 
		Dreadnought_Turret70.SetStandardVars(2f, 0.0f, 400.0f, 0.50f);
		Dreadnought_Turret70.SetGraphicVars(WeaponGraphic.Image.MissileRed, redStart, redStart, EffectController.Singleton.missileTrail);
		Dreadnought_Turret70.SetRapidVars(4, 0.20f, 0.0f, 0.0f);
		Dreadnought_Turret70.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret70.SetDamage(0.3f);
		Dreadnought_Turret70.SetHomingVars();
		Dreadnought_Turret70.SetExplosionVars(redStart, redStart);
		
		Dreadnought_Turret71 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret71.SetStandardVars(3f, 400.0f, 100.0f, 0.7f);
		Dreadnought_Turret71.SetDamage(1.4f);
		Dreadnought_Turret71.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, pinkStart, Color.white);
		Dreadnought_Turret71.SetRapidVars(12, 0.075f, 0.0f, 2.2f);
		
		Dreadnought_Turret72 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret72.SetStandardVars(5f, 400.0f, 200.0f, 0.2f);
		Dreadnought_Turret72.SetDamage(7.0f);
		Dreadnought_Turret72.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(206, 255, 215, 255), new Color32(0, 255, 86, 255));
		Dreadnought_Turret72.SetBeamVars(1.2f);
		
		Dreadnought_Turret73 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret73.SetStandardVars(2.0f, -800.0f, 750.0f, 0.025f, false);
		Dreadnought_Turret73.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret73.SetHomingVars();
		Dreadnought_Turret73.SetScatterVars(5, 6f);
		Dreadnought_Turret73.SetDamage(1.0f);
		Dreadnought_Turret73.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Dreadnought_Turret74 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret74.SetStandardVars(3.0f, -800.0f, 500.0f, 0.05f, false);
		Dreadnought_Turret74.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Dreadnought_Turret74.SetDamage(0.3f);
		Dreadnought_Turret74.SetRapidVars(4, 0.175f, 0.5f);
		Dreadnought_Turret74.SetHomingVars();
		Dreadnought_Turret74.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		Dreadnought_Turret74.SetBuff(Buff.Type.Burn);
		
		Dreadnought_Turret75 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret75.SetStandardVars(3.5f, 0.0f, 650.0f, 0.03f, false);
		Dreadnought_Turret75.SetGraphicVars(WeaponGraphic.Image.MissilePink, pinkStart, pinkStart, EffectController.Singleton.missileTrail);
		Dreadnought_Turret75.SetDamage(1.0f);
		Dreadnought_Turret75.SetRapidVars(6, 0.15f, 0.6f);
		Dreadnought_Turret75.SetExplosionVars(pinkStart, pinkStart);
		
		Dreadnought_Turret76 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret76.SetStandardVars(2.0f, 0.0f, 500.0f, 0.05f);
		Dreadnought_Turret76.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.lineTrail);
		Dreadnought_Turret76.SetRapidVars(5, 0.175f, 0.0f, 1.5f);
		Dreadnought_Turret76.SetDamage(0.8f);
		Dreadnought_Turret76.SetScatterVars(3, 1.0f);
		Dreadnought_Turret76.SetBuff(Buff.Type.Acid);
		Dreadnought_Turret76.SetPiercing();
		
		Dreadnought_Turret77 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret77.SetStandardVars(3.0f, 525.0f, 225.0f, 0.2f);
		Dreadnought_Turret77.SetGraphicVars(WeaponGraphic.Image.RoundGreen, pinkStart, Color.white);
		Dreadnought_Turret77.SetScatterVars(10, KJMath.CIRCLE * 0.45f);
		Dreadnought_Turret77.SetRapidVars(3, 0.3f, 0.0f);
		Dreadnought_Turret77.SetDamage(1.0f);
		
		Dreadnought_Turret78 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret78.SetStandardVars(2.0f, 0.0f, 500.0f, 0.05f);
		Dreadnought_Turret78.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, pinkStart, EffectController.Singleton.lineTrail);
		Dreadnought_Turret78.SetRapidVars(4, 0.2f, 0.0f, 1.5f);
		Dreadnought_Turret78.SetDamage(1.35f);
		Dreadnought_Turret78.SetScatterVars(3, 1.0f);
		Dreadnought_Turret78.SetPiercing();
		
		Dreadnought_Turret79 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret79.SetStandardVars(2.5f, 400.0f, 50.0f, 0.02f);
		Dreadnought_Turret79.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.0f), EffectController.Singleton.lightWaveTrail);
		Dreadnought_Turret79.SetScatterVars(22, KJMath.CIRCLE * 0.15f);
		Dreadnought_Turret79.SetDamage(0.5f);
		Dreadnought_Turret79.SetBuff(Buff.Type.Acid);
		
		Dreadnought_Turret80 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret80.SetStandardVars(2.5f, 400.0f, 50.0f, 0.02f);
		Dreadnought_Turret80.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.0f), EffectController.Singleton.lightWaveTrail);
		Dreadnought_Turret80.SetScatterVars(22, KJMath.CIRCLE * 0.15f);
		Dreadnought_Turret80.SetDamage(0.3f);
		Dreadnought_Turret80.SetBuff(Buff.Type.Burn);

		Dreadnought_Turret_Boomerang1 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang1.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret_Boomerang1.SetDamage(0.4f);
		Dreadnought_Turret_Boomerang1.SetGraphicVars(WeaponGraphic.Image.BoomerGreen, greenStart, Color.white);
		Dreadnought_Turret_Boomerang1.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang1.SetTime(4.0f);
		Dreadnought_Turret_Boomerang1.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret_Boomerang1.SetDoubleHitRadius();
		Dreadnought_Turret_Boomerang1.SetBuff(Buff.Type.Acid);

		Dreadnought_Turret_Boomerang2 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang2.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret_Boomerang2.SetDamage(0.6f);
		Dreadnought_Turret_Boomerang2.SetGraphicVars(WeaponGraphic.Image.BoomerBlue, blueStart, Color.white);
		Dreadnought_Turret_Boomerang2.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang2.SetTime(4.0f);
		Dreadnought_Turret_Boomerang2.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret_Boomerang2.SetDoubleHitRadius();
		Dreadnought_Turret_Boomerang2.SetBuff(Buff.Type.Stun);

		Dreadnought_Turret_Boomerang3 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang3.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret_Boomerang3.SetDamage(0.2f);
		Dreadnought_Turret_Boomerang3.SetGraphicVars(WeaponGraphic.Image.BoomerRed, redStart, Color.white);
		Dreadnought_Turret_Boomerang3.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang3.SetTime(4.0f);
		Dreadnought_Turret_Boomerang3.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret_Boomerang3.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret_Boomerang3.SetDoubleHitRadius();

		Dreadnought_Turret_Boomerang4 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang4.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret_Boomerang4.SetDamage(0.8f);
		Dreadnought_Turret_Boomerang4.SetGraphicVars(WeaponGraphic.Image.BoomerYellow, yellowStart, Color.white);
		Dreadnought_Turret_Boomerang4.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang4.SetTime(4.0f);
		Dreadnought_Turret_Boomerang4.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret_Boomerang4.SetDoubleHitRadius();

		Dreadnought_Turret_Boomerang5 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang5.SetStandardVars(3.0f, 0.0f, 300.0f, 0.03f);
		Dreadnought_Turret_Boomerang5.SetDamage(1.2f);
		Dreadnought_Turret_Boomerang5.SetGraphicVars(WeaponGraphic.Image.BoomerPink, pinkStart, Color.white);
		Dreadnought_Turret_Boomerang5.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang5.SetTime(4.0f);
		Dreadnought_Turret_Boomerang5.SetBoomerang(1.6f, 0.0f);
		Dreadnought_Turret_Boomerang5.SetDoubleHitRadius();

		Dreadnought_Turret_Boomerang6 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang6.SetStandardVars(3.0f, 100.0f, 150.0f, 0.03f);
		Dreadnought_Turret_Boomerang6.SetDamage(0.4f);
		Dreadnought_Turret_Boomerang6.SetGraphicVars(WeaponGraphic.Image.BoomerGreen, greenStart, Color.white);
		Dreadnought_Turret_Boomerang6.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang6.SetTime(7.0f);
		Dreadnought_Turret_Boomerang6.SetBuff(Buff.Type.Acid);
		Dreadnought_Turret_Boomerang6.SetBoomerang(2.6f, 0.0f);
		Dreadnought_Turret_Boomerang6.SetDoubleHitRadius();

		Dreadnought_Turret_Boomerang7 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang7.SetStandardVars(3.0f, 100.0f, 150.0f, 0.03f);
		Dreadnought_Turret_Boomerang7.SetDamage(0.3f);
		Dreadnought_Turret_Boomerang7.SetGraphicVars(WeaponGraphic.Image.BoomerRed, redStart, Color.white);
		Dreadnought_Turret_Boomerang7.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang7.SetTime(7.0f);
		Dreadnought_Turret_Boomerang7.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret_Boomerang7.SetBoomerang(2.6f, 0.0f);
		Dreadnought_Turret_Boomerang7.SetDoubleHitRadius();

		Dreadnought_Turret_Boomerang8 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang8.SetStandardVars(3.0f, 100.0f, 150.0f, 0.03f);
		Dreadnought_Turret_Boomerang8.SetDamage(0.6f);
		Dreadnought_Turret_Boomerang8.SetGraphicVars(WeaponGraphic.Image.BoomerBlue, blueStart, Color.white);
		Dreadnought_Turret_Boomerang8.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang8.SetTime(7.0f);
		Dreadnought_Turret_Boomerang8.SetBuff(Buff.Type.Stun);
		Dreadnought_Turret_Boomerang8.SetBoomerang(2.6f, 0.0f);
		Dreadnought_Turret_Boomerang8.SetDoubleHitRadius();

		Dreadnought_Turret_Boomerang9 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang9.SetStandardVars(3.0f, 100.0f, 150.0f, 0.03f);
		Dreadnought_Turret_Boomerang9.SetDamage(0.8f);
		Dreadnought_Turret_Boomerang9.SetGraphicVars(WeaponGraphic.Image.BoomerYellow, yellowStart, Color.white);
		Dreadnought_Turret_Boomerang9.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang9.SetTime(7.0f);
		Dreadnought_Turret_Boomerang9.SetBoomerang(2.6f, 0.0f);
		Dreadnought_Turret_Boomerang9.SetDoubleHitRadius();

		Dreadnought_Turret_Boomerang10 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Boomerang10.SetStandardVars(3.0f, 100.0f, 150.0f, 0.03f);
		Dreadnought_Turret_Boomerang10.SetDamage(1.2f);
		Dreadnought_Turret_Boomerang10.SetGraphicVars(WeaponGraphic.Image.BoomerPink, pinkStart, Color.white);
		Dreadnought_Turret_Boomerang10.SetScatterVars(4, KJMath.CIRCLE * 0.2f);
		Dreadnought_Turret_Boomerang10.SetTime(7.0f);
		Dreadnought_Turret_Boomerang10.SetBoomerang(2.6f, 0.0f);
		Dreadnought_Turret_Boomerang10.SetDoubleHitRadius();

		Dreadnought_Turret_Swirl1 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl1.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl1.SetDamage(0.65f);
		Dreadnought_Turret_Swirl1.SetGraphicVars(WeaponGraphic.Image.SwirlYellow, yellowStart, Color.white);
		Dreadnought_Turret_Swirl1.SetRapidVars(3, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl1.SetTime(7f);
		Dreadnought_Turret_Swirl1.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl1.SetPiercing();
		Dreadnought_Turret_Swirl1.SetDoubleHitRadius();
		
		Dreadnought_Turret_Swirl2 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl2.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl2.SetDamage(0.2f);
		Dreadnought_Turret_Swirl2.SetGraphicVars(WeaponGraphic.Image.SwirlRed, redStart, Color.white);
		Dreadnought_Turret_Swirl2.SetRapidVars(3, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl2.SetTime(7f);
		Dreadnought_Turret_Swirl2.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl2.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret_Swirl2.SetPiercing();
		Dreadnought_Turret_Swirl2.SetDoubleHitRadius();
		
		Dreadnought_Turret_Swirl3 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl3.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl3.SetDamage(0.325f);
		Dreadnought_Turret_Swirl3.SetGraphicVars(WeaponGraphic.Image.SwirlGreen, greenStart, Color.white);
		Dreadnought_Turret_Swirl3.SetRapidVars(3, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl3.SetTime(7f);
		Dreadnought_Turret_Swirl3.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl3.SetBuff(Buff.Type.Acid);
		Dreadnought_Turret_Swirl3.SetPiercing();
		Dreadnought_Turret_Swirl3.SetDoubleHitRadius();
		
		Dreadnought_Turret_Swirl4 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl4.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl4.SetDamage(0.5f);
		Dreadnought_Turret_Swirl4.SetGraphicVars(WeaponGraphic.Image.SwirlBlue , blueStart, Color.white);
		Dreadnought_Turret_Swirl4.SetRapidVars(3, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl4.SetTime(7f);
		Dreadnought_Turret_Swirl4.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl4.SetBuff(Buff.Type.Stun);
		Dreadnought_Turret_Swirl4.SetPiercing();
		Dreadnought_Turret_Swirl4.SetDoubleHitRadius();
		
		Dreadnought_Turret_Swirl5 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl5.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl5.SetDamage(0.8f);
		Dreadnought_Turret_Swirl5.SetGraphicVars(WeaponGraphic.Image.SwirlPink, pinkStart, Color.white);
		Dreadnought_Turret_Swirl5.SetRapidVars(3, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl5.SetTime(7f);
		Dreadnought_Turret_Swirl5.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl5.SetPiercing();
		Dreadnought_Turret_Swirl5.SetDoubleHitRadius();

		Dreadnought_Turret_Swirl6 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl6.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl6.SetDamage(0.65f);
		Dreadnought_Turret_Swirl6.SetGraphicVars(WeaponGraphic.Image.SwirlYellow, yellowStart, Color.white);
		Dreadnought_Turret_Swirl6.SetRapidVars(5, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl6.SetTime(6.5f);
		Dreadnought_Turret_Swirl6.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl6.SetPiercing();
		Dreadnought_Turret_Swirl6.SetDoubleHitRadius();
		
		Dreadnought_Turret_Swirl7 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl7.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl7.SetDamage(0.2f);
		Dreadnought_Turret_Swirl7.SetGraphicVars(WeaponGraphic.Image.SwirlRed, redStart, Color.white);
		Dreadnought_Turret_Swirl7.SetRapidVars(5, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl7.SetTime(6.5f);
		Dreadnought_Turret_Swirl7.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl7.SetBuff(Buff.Type.Burn);
		Dreadnought_Turret_Swirl7.SetPiercing();
		Dreadnought_Turret_Swirl7.SetDoubleHitRadius();
		
		Dreadnought_Turret_Swirl8 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl8.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl8.SetDamage(0.325f);
		Dreadnought_Turret_Swirl8.SetGraphicVars(WeaponGraphic.Image.SwirlGreen, greenStart, Color.white);
		Dreadnought_Turret_Swirl8.SetRapidVars(5, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl8.SetTime(6.5f);
		Dreadnought_Turret_Swirl8.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl8.SetBuff(Buff.Type.Acid);
		Dreadnought_Turret_Swirl8.SetPiercing();
		Dreadnought_Turret_Swirl8.SetDoubleHitRadius();
		
		Dreadnought_Turret_Swirl9 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl9.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl9.SetDamage(0.5f);
		Dreadnought_Turret_Swirl9.SetGraphicVars(WeaponGraphic.Image.SwirlBlue , blueStart, Color.white);
		Dreadnought_Turret_Swirl9.SetRapidVars(5, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl9.SetTime(6.5f);
		Dreadnought_Turret_Swirl9.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl9.SetBuff(Buff.Type.Stun);
		Dreadnought_Turret_Swirl9.SetPiercing();
		Dreadnought_Turret_Swirl9.SetDoubleHitRadius();
		
		Dreadnought_Turret_Swirl10 = holderObject.AddComponent<Weapon>();
		Dreadnought_Turret_Swirl10.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		Dreadnought_Turret_Swirl10.SetDamage(0.8f);
		Dreadnought_Turret_Swirl10.SetGraphicVars(WeaponGraphic.Image.SwirlPink, pinkStart, Color.white);
		Dreadnought_Turret_Swirl10.SetRapidVars(5, 0.25f, 0.9f, 0.35f);
		Dreadnought_Turret_Swirl10.SetTime(6.5f);
		Dreadnought_Turret_Swirl10.SetSwirl(0.4f, 0.12f);
		Dreadnought_Turret_Swirl10.SetPiercing();
		Dreadnought_Turret_Swirl10.SetDoubleHitRadius();

		//Elite Weapon

		E_EliteRapid_Easy = holderObject.AddComponent<Weapon>();
		E_EliteRapid_Easy.SetStandardVars(3.5f, 500.0f, 200.0f, 0.2f);
		E_EliteRapid_Easy.SetDamage(0.7f);
		E_EliteRapid_Easy.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		E_EliteRapid_Easy.SetRapidVars(7, 0.10f, 0.7f);

		E_EliteRapid = holderObject.AddComponent<Weapon>();
		E_EliteRapid.SetStandardVars(3.5f, 500.0f, 200.0f, 0.2f);
		E_EliteRapid.SetDamage(0.7f);
		E_EliteRapid.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		E_EliteRapid.SetRapidVars(9, 0.12f, 0.9f);

		E_EliteRapid2 = holderObject.AddComponent<Weapon>();
		E_EliteRapid2.SetStandardVars(5.0f, 400.0f, 150.0f, 0.15f);
		E_EliteRapid2.SetDamage(1.35f);
		E_EliteRapid2.SetGraphicVars(WeaponGraphic.Image.StarPink, pinkStart, Color.white);
		E_EliteRapid2.SetRapidVars(4, 0.35f, 0.0f, 3.0f);
		E_EliteRapid2.SetScatterVars(6, KJMath.CIRCLE);

		E_EliteRapidHard = holderObject.AddComponent<Weapon>();
		E_EliteRapidHard.SetStandardVars(3.5f, 500.0f, 250.0f, 0.2f);
		E_EliteRapidHard.SetDamage(1.25f);
		E_EliteRapidHard.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, Color.white);
		E_EliteRapidHard.SetRapidVars(12, 0.10f, 0.75f);

		E_EliteBeam = holderObject.AddComponent<Weapon>();
		E_EliteBeam.SetStandardVars(7f, 400.0f, 200.0f, 0.2f);
		E_EliteBeam.SetDamage(4.0f);
		E_EliteBeam.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(150, 216, 255, 255), new Color32(0, 76, 255, 255));
		E_EliteBeam.SetBeamVars(1.35f);

		E_EliteGreenScatter_Easy = holderObject.AddComponent<Weapon>();
		E_EliteGreenScatter_Easy.SetStandardVars(5.0f, 300.0f, 200.0f, 0.2f);
		E_EliteGreenScatter_Easy.SetDamage(0.25f);
		E_EliteGreenScatter_Easy.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, Color.white);
		E_EliteGreenScatter_Easy.SetScatterVars(6, KJMath.CIRCLE);
		E_EliteGreenScatter_Easy.SetRapidVars(3, 0.35f, 0, 2.5f);
		E_EliteGreenScatter_Easy.SetBuff(Buff.Type.Acid, 100, 1.25f, 7.5f);

		E_EliteGreenScatter = holderObject.AddComponent<Weapon>();
		E_EliteGreenScatter.SetStandardVars(5.0f, 300.0f, 200.0f, 0.2f);
		E_EliteGreenScatter.SetDamage(0.25f);
		E_EliteGreenScatter.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, Color.white);
		E_EliteGreenScatter.SetScatterVars(7, KJMath.CIRCLE);
		E_EliteGreenScatter.SetRapidVars(4, 0.35f, 0, 2.5f);
		E_EliteGreenScatter.SetBuff(Buff.Type.Acid, 100, 1.25f, 7.5f);

		E_EliteRedScatter = holderObject.AddComponent<Weapon>();
		E_EliteRedScatter.SetStandardVars(5.0f, 300.0f, 200.0f, 0.2f);
		E_EliteRedScatter.SetDamage(0.35f);
		E_EliteRedScatter.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, Color.white);
		E_EliteRedScatter.SetScatterVars(7, KJMath.CIRCLE);
		E_EliteRedScatter.SetRapidVars(4, 0.35f, 0, 2.5f);
		E_EliteRedScatter.SetBuff(Buff.Type.Burn, 100, 0.65f);

		E_EliteBlueScatter = holderObject.AddComponent<Weapon>();
		E_EliteBlueScatter.SetStandardVars(5.0f, 300.0f, 200.0f, 0.2f);
		E_EliteBlueScatter.SetDamage(0.50f);
		E_EliteBlueScatter.SetGraphicVars(WeaponGraphic.Image.SpeedBlue, blueStart, Color.white);
		E_EliteBlueScatter.SetScatterVars(7, KJMath.CIRCLE);
		E_EliteBlueScatter.SetRapidVars(4, 0.35f, 0, 2.5f);
		E_EliteBlueScatter.SetBuff(Buff.Type.Stun, 100, 1.25f, 1.5f);

		E_ElitePinkScatter = holderObject.AddComponent<Weapon>();
		E_ElitePinkScatter.SetStandardVars(5.0f, 300.0f, 150.0f, 0.2f);
		E_ElitePinkScatter.SetDamage(1.25f);
		E_ElitePinkScatter.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, Color.white);
		E_ElitePinkScatter.SetScatterVars(7, KJMath.CIRCLE);
		E_ElitePinkScatter.SetRapidVars(4, 0.5f, 0, 2.5f);

		E_EliteMissile_Easy = holderObject.AddComponent<Weapon>();
		E_EliteMissile_Easy.SetStandardVars(4.5f, 50.0f, 300.0f, 0.50f, false);
		E_EliteMissile_Easy.SetGraphicVars(WeaponGraphic.Image.MissileGreen, new Color(0.2f, 0.9f, 0.3f, 1.0f), new Color(0.0f, 0.9f, 0.2f, 1.0f), EffectController.Singleton.missileTrail);
		E_EliteMissile_Easy.SetHomingVars();
		E_EliteMissile_Easy.SetDamage(0.8f);
		E_EliteMissile_Easy.SetScatterVars(1, 0.0f);
		E_EliteMissile_Easy.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteMissile_Easy.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		E_EliteMissile = holderObject.AddComponent<Weapon>();
		E_EliteMissile.SetStandardVars(4.5f, 100.0f, 350.0f, 0.50f, false);
		E_EliteMissile.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		E_EliteMissile.SetHomingVars();
		E_EliteMissile.SetDamage(1.5f);
		E_EliteMissile.SetScatterVars(1, 0.0f);
		E_EliteMissile.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		E_EliteMissile2 = holderObject.AddComponent<Weapon>();
		E_EliteMissile2.SetStandardVars(5.0f, 50.0f, 325.0f, 0.50f, false);
		E_EliteMissile2.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_EliteMissile2.SetHomingVars();
		E_EliteMissile2.SetEjectionVars ();
		E_EliteMissile2.SetDamage(1.0f);
		E_EliteMissile2.name = "E_EliteMissile2";
		E_EliteMissile2.SetScatterVars(2, 0.0f);
		E_EliteMissile2.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteMissile2.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		E_EliteBlueStriker = holderObject.AddComponent<Weapon>();
		E_EliteBlueStriker.SetStandardVars(6.0f, 0.0f, 525.0f, 0.008f, false);
		E_EliteBlueStriker.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 1.0f, 0.7f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_EliteBlueStriker.SetDamage(0.8f);
		E_EliteBlueStriker.SetRapidVars(4, 0.5f, 0.5f);
		E_EliteBlueStriker.SetHomingVars();
		E_EliteBlueStriker.SetEjectionVars();
		E_EliteBlueStriker.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		E_EliteBlueStriker.SetBuff(Buff.Type.Stun);
		
		E_EliteRedStriker = holderObject.AddComponent<Weapon>();
		E_EliteRedStriker.SetStandardVars(6.0f, 0.0f, 525.0f, 0.008f, false);
		E_EliteRedStriker.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_EliteRedStriker.SetDamage(0.35f);
		E_EliteRedStriker.SetRapidVars(4, 0.5f, 0.5f);
		E_EliteRedStriker.SetHomingVars();
		E_EliteRedStriker.SetEjectionVars();
		E_EliteRedStriker.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		E_EliteRedStriker.SetBuff(Buff.Type.Burn);

		E_EliteGreenHoming = holderObject.AddComponent<Weapon>();
		E_EliteGreenHoming.SetStandardVars(4.0f, 1500.0f, 300.0f, 1.2f);
		E_EliteGreenHoming.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, new Color(0.2f, 0.8f, 0.4f, 1.0f), new Color (0.2f, 1.0f, 0.6f), EffectController.Singleton.shockTrail);
		E_EliteGreenHoming.SetChainerVars(1);
		E_EliteGreenHoming.SetHomingVars();
		E_EliteGreenHoming.SetDamage(0.50f);
		E_EliteGreenHoming.SetScatterVars(2, 0.0f);
		E_EliteGreenHoming.SetEjectionVars();
		E_EliteGreenHoming.SetBuff(Buff.Type.Acid, 100, 1.25f, 7.5f);

		E_EliteRedHoming = holderObject.AddComponent<Weapon>();
		E_EliteRedHoming.SetStandardVars(4.0f, 1500.0f, 300.0f, 1.2f);
		E_EliteRedHoming.SetGraphicVars(WeaponGraphic.Image.ElectroRed, new Color(1.0f, 0.2f, 0.3f, 1.0f), new Color (1.0f, 0.2f, 0.0f, 1.0f), EffectController.Singleton.shockTrail);
		E_EliteRedHoming.SetChainerVars(1);
		E_EliteRedHoming.SetHomingVars();
		E_EliteRedHoming.SetDamage(0.35f);
		E_EliteRedHoming.SetScatterVars(2, 0.0f);
		E_EliteRedHoming.SetEjectionVars();
		E_EliteRedHoming.SetBuff(Buff.Type.Burn);

		E_EliteBlueHoming = holderObject.AddComponent<Weapon>();
		E_EliteBlueHoming.SetStandardVars(2.5f, 0.0f, 425.0f, 1.2f);
		E_EliteBlueHoming.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(0.2f, 0.7f, 1.0f, 1.0f), new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		E_EliteBlueHoming.SetChainerVars(1);
		E_EliteBlueHoming.SetHomingVars();
		E_EliteBlueHoming.SetDamage(0.5f);
		E_EliteBlueHoming.SetScatterVars(2, 0.0f);
		E_EliteBlueHoming.SetEjectionVars();
		E_EliteBlueHoming.SetBuff(Buff.Type.Stun, 100);

		E_ElitePinkHoming = holderObject.AddComponent<Weapon>();
		E_ElitePinkHoming.SetStandardVars(4.0f, 1500.0f, 300.0f, 1.2f);
		E_ElitePinkHoming.SetGraphicVars(WeaponGraphic.Image.ElectroPink, new Color(0.7f, 0.2f, 0.7f, 1.0f), new Color (0.7f, 0.2f, 0.7f, 1.0f), EffectController.Singleton.shockTrail);
		E_ElitePinkHoming.SetChainerVars(1);
		E_ElitePinkHoming.SetHomingVars();
		E_ElitePinkHoming.SetScatterVars(2, 0.0f);
		E_ElitePinkHoming.SetEjectionVars();
		E_ElitePinkHoming.SetDamage(1.5f);

		E_EliteGreenHoming2 = holderObject.AddComponent<Weapon>();
		E_EliteGreenHoming2.SetStandardVars(4.0f, 1500.0f, 300.0f, 1.2f);
		E_EliteGreenHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, new Color(0.2f, 0.8f, 0.4f, 1.0f), new Color (0.2f, 1.0f, 0.6f, 1.0f), EffectController.Singleton.shockTrail);
		E_EliteGreenHoming2.SetChainerVars(1);
		E_EliteGreenHoming2.SetHomingVars();
		E_EliteGreenHoming2.SetDamage(0.50f);
		E_EliteGreenHoming2.SetScatterVars(2, 0.0f);
		E_EliteGreenHoming2.SetEjectionVars();
		E_EliteGreenHoming2.SetBuff(Buff.Type.Acid, 100, 1.25f, 7.5f);
		
		E_EliteRedHoming2 = holderObject.AddComponent<Weapon>();
		E_EliteRedHoming2.SetStandardVars(4.0f, 1500.0f, 300.0f, 1.2f);
		E_EliteRedHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroRed, new Color(1.0f, 0.2f, 0.3f, 1.0f), new Color (1.0f, 0.2f, 0.0f, 1.0f), EffectController.Singleton.shockTrail);
		E_EliteRedHoming2.SetChainerVars(1);
		E_EliteRedHoming2.SetHomingVars();
		E_EliteRedHoming2.SetDamage(0.35f);
		E_EliteRedHoming2.SetScatterVars(2, 0.0f);
		E_EliteRedHoming2.SetEjectionVars();
		E_EliteRedHoming2.SetBuff(Buff.Type.Burn);
		
		E_EliteBlueHoming2 = holderObject.AddComponent<Weapon>();
		E_EliteBlueHoming2.SetStandardVars(2.25f, 0.0f, 475.0f, 1.2f);
		E_EliteBlueHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(0.2f, 0.7f, 1.0f, 1.0f), new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		E_EliteBlueHoming2.SetChainerVars(1);
		E_EliteBlueHoming2.SetHomingVars();
		E_EliteBlueHoming2.SetDamage(0.25f);
		E_EliteBlueHoming2.SetScatterVars(2, 0.0f);
		E_EliteBlueHoming2.SetEjectionVars();
		E_EliteBlueHoming2.SetBuff(Buff.Type.Stun, 100);
		
		E_ElitePinkHoming2 = holderObject.AddComponent<Weapon>();
		E_ElitePinkHoming2.SetStandardVars(4.0f, 1500.0f, 300.0f, 1.2f);
		E_ElitePinkHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroPink, new Color(0.7f, 0.2f, 0.7f, 1.0f), new Color (0.7f, 0.2f, 0.7f, 1.0f), EffectController.Singleton.shockTrail);
		E_ElitePinkHoming2.SetChainerVars(1);
		E_ElitePinkHoming2.SetHomingVars();
		E_ElitePinkHoming2.SetScatterVars(2, 0.0f);
		E_ElitePinkHoming2.SetEjectionVars();
		E_ElitePinkHoming2.SetDamage(1.5f);

		E_EliteSweep_Easy = holderObject.AddComponent<Weapon>();
		E_EliteSweep_Easy.SetStandardVars(2.5f, 400.0f, 175.0f, 0.2f);
		E_EliteSweep_Easy.SetDamage(0.6f);
		E_EliteSweep_Easy.SetGraphicVars(WeaponGraphic.Image.StarGreen, greenStart, Color.white);
		E_EliteSweep_Easy.SetRapidVars(6, 0.10f, 0.0f, 1.8f);

		E_EliteSweep = holderObject.AddComponent<Weapon>();
		E_EliteSweep.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		E_EliteSweep.SetDamage(0.75f);
		E_EliteSweep.SetGraphicVars(WeaponGraphic.Image.StarGreen, greenStart, Color.white);
		E_EliteSweep.SetRapidVars(7, 0.10f, 0.0f, 1.8f);

		E_EliteSweepHard = holderObject.AddComponent<Weapon>();
		E_EliteSweepHard.SetStandardVars(2.5f, 600.0f, 350.0f, 0.2f);
		E_EliteSweepHard.SetDamage(1.0f);
		E_EliteSweepHard.SetGraphicVars(WeaponGraphic.Image.StarBlue, blueStart, Color.white);
		E_EliteSweepHard.SetRapidVars(7, 0.10f, 0.0f, 1.8f);

		E_EliteGreenLaser_Easy = holderObject.AddComponent<Weapon>();
		E_EliteGreenLaser_Easy.SetStandardVars(5.0f, 501.0f, 500.0f, 0.2f);
		E_EliteGreenLaser_Easy.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.2f, 1.0f, 0.6f), EffectController.Singleton.lineTrail);
		//E_EliteLaser.SetScatterVars(5, KJMath.CIRCLE * 0.15f);
		E_EliteGreenLaser_Easy.SetRapidVars(4, 0.45f, 0.0f, 1.5f);
		E_EliteGreenLaser_Easy.SetDamage(0.25f);
		E_EliteGreenLaser_Easy.SetPiercing();
		E_EliteGreenLaser_Easy.SetBuff(Buff.Type.Acid, 100, 1.25f, 7.5f);

		E_EliteGreenLaser = holderObject.AddComponent<Weapon>();
		E_EliteGreenLaser.SetStandardVars(5.0f, 601.0f, 600.0f, 0.2f);
		E_EliteGreenLaser.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.2f, 1.0f, 0.6f), EffectController.Singleton.lineTrail);
		//E_EliteLaser.SetScatterVars(5, KJMath.CIRCLE * 0.15f);
		E_EliteGreenLaser.SetRapidVars(5, 0.45f, 0.0f, 1.5f);
		E_EliteGreenLaser.SetDamage(0.25f);
		E_EliteGreenLaser.SetPiercing();
		E_EliteGreenLaser.SetBuff(Buff.Type.Acid, 100, 1.25f, 7.5f);

		E_EliteRedLaser = holderObject.AddComponent<Weapon>();
		E_EliteRedLaser.SetStandardVars(5.0f, 601.0f, 600.0f, 0.2f);
		E_EliteRedLaser.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, new Color(0.9f, 0.0f, 0.2f), EffectController.Singleton.lineTrail);
		E_EliteRedLaser.SetRapidVars(5, 0.45f, 0.0f, 1.5f);
		E_EliteRedLaser.SetDamage(0.35f);
		E_EliteRedLaser.SetPiercing();
		E_EliteRedLaser.SetBuff(Buff.Type.Burn);

		E_EliteBlueLaser = holderObject.AddComponent<Weapon>();
		E_EliteBlueLaser.SetStandardVars(5.0f, 601.0f, 600.0f, 0.2f);
		E_EliteBlueLaser.SetGraphicVars(WeaponGraphic.Image.SpeedBlue, blueStart, new Color(0.0f, 0.0f, 0.8f, 1.0f), EffectController.Singleton.lineTrail);
		E_EliteBlueLaser.SetRapidVars(5, 0.45f, 0.0f, 1.5f);
		E_EliteBlueLaser.SetDamage(0.5f);
		E_EliteBlueLaser.SetPiercing();
		E_EliteBlueLaser.SetBuff(Buff.Type.Stun, 100, 1.25f, 3f);

		E_ElitePinkLaser = holderObject.AddComponent<Weapon>();
		E_ElitePinkLaser.SetStandardVars(5.0f, 601.0f, 600.0f, 0.2f);
		E_ElitePinkLaser.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, new Color(0.7f, 0.0f, 0.7f, 0.8f), EffectController.Singleton.lineTrail);
		E_ElitePinkLaser.SetRapidVars(5, 0.45f, 0.0f, 1.5f);
		E_ElitePinkLaser.SetDamage(1.5f);
		E_ElitePinkLaser.SetPiercing();

		E_EliteWave_Easy = holderObject.AddComponent<Weapon>();
		E_EliteWave_Easy.SetStandardVars(5.0f, 0.0f, 300.0f, 0.1f);
		E_EliteWave_Easy.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.2f, 1.0f, 0.2f), EffectController.Singleton.waveTrail);
		E_EliteWave_Easy.SetScatterVars(3, KJMath.CIRCLE * 0.15f);
		E_EliteWave_Easy.SetRapidVars(1, 0.5f, 0.0f);
		E_EliteWave_Easy.SetDamage(0.30f);

		E_EliteWaveGreen = holderObject.AddComponent<Weapon>();
		E_EliteWaveGreen.SetStandardVars(5.0f, 0.0f, 400.0f, 0.1f);
		E_EliteWaveGreen.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.2f, 1.0f, 0.2f), EffectController.Singleton.waveTrail);
		E_EliteWaveGreen.SetScatterVars(4, KJMath.CIRCLE * 0.15f);
		E_EliteWaveGreen.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteWaveGreen.SetDamage(0.50f);
		E_EliteWaveGreen.SetBuff(Buff.Type.Acid, 100, 1f, 2.0f);

		E_EliteWaveBlue = holderObject.AddComponent<Weapon>();
		E_EliteWaveBlue.SetStandardVars(5.0f, 0.0f, 400.0f, 0.1f);
		E_EliteWaveBlue.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.1f, 0.2f, 0.8f), EffectController.Singleton.waveTrail);
		E_EliteWaveBlue.SetScatterVars(4, KJMath.CIRCLE * 0.15f);
		E_EliteWaveBlue.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteWaveBlue.SetDamage(0.50f);
		E_EliteWaveBlue.SetBuff(Buff.Type.Stun, 100, 1f, 2.0f);

		E_EliteWaveRed = holderObject.AddComponent<Weapon>();
		E_EliteWaveRed.SetStandardVars(5.0f, 0.0f, 400.0f, 0.1f);
		E_EliteWaveRed.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.00f, 0.0f, 0.2f), EffectController.Singleton.waveTrail);
		E_EliteWaveRed.SetScatterVars(4, KJMath.CIRCLE * 0.15f);
		E_EliteWaveRed.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteWaveRed.SetDamage(0.35f);
		E_EliteWaveRed.SetBuff(Buff.Type.Burn);

		E_EliteWavePink = holderObject.AddComponent<Weapon>();
		E_EliteWavePink.SetStandardVars(5.0f, 0.0f, 400.0f, 0.1f);
		E_EliteWavePink.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(0.7f, 0.3f, 0.7f, 1.0f), EffectController.Singleton.waveTrail);
		E_EliteWavePink.SetScatterVars(4, KJMath.CIRCLE * 0.15f);
		E_EliteWavePink.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteWavePink.SetDamage(1.00f);

		E_EliteWaveGreen2 = holderObject.AddComponent<Weapon>();
		E_EliteWaveGreen2.SetStandardVars(4.5f, 0.0f, 500.0f, 0.1f);
		E_EliteWaveGreen2.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.2f, 1.0f, 0.2f), EffectController.Singleton.waveTrail);
		E_EliteWaveGreen2.SetScatterVars(5, KJMath.CIRCLE * 0.15f);
		E_EliteWaveGreen2.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteWaveGreen2.SetDamage(0.50f);
		E_EliteWaveGreen2.SetBuff(Buff.Type.Acid, 100, 1f, 2.0f);
		
		E_EliteWaveBlue2 = holderObject.AddComponent<Weapon>();
		E_EliteWaveBlue2.SetStandardVars(4.5f, 0.0f, 500.0f, 0.1f);
		E_EliteWaveBlue2.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.1f, 0.2f, 0.8f), EffectController.Singleton.waveTrail);
		E_EliteWaveBlue2.SetScatterVars(5, KJMath.CIRCLE * 0.15f);
		E_EliteWaveBlue2.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteWaveBlue2.SetDamage(0.50f);
		E_EliteWaveBlue2.SetBuff(Buff.Type.Stun, 100, 1f, 2.0f);
		
		E_EliteWaveRed2 = holderObject.AddComponent<Weapon>();
		E_EliteWaveRed2.SetStandardVars(4.5f, 0.0f, 500.0f, 0.1f);
		E_EliteWaveRed2.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.00f, 0.0f, 0.2f), EffectController.Singleton.waveTrail);
		E_EliteWaveRed2.SetScatterVars(5, KJMath.CIRCLE * 0.15f);
		E_EliteWaveRed2.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteWaveRed2.SetDamage(0.35f);
		E_EliteWaveRed2.SetBuff(Buff.Type.Burn);
		
		E_EliteWavePink2 = holderObject.AddComponent<Weapon>();
		E_EliteWavePink2.SetStandardVars(4.5f, 0.0f, 500.0f, 0.1f);
		E_EliteWavePink2.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(0.7f, 0.3f, 0.7f, 1.0f), EffectController.Singleton.waveTrail);
		E_EliteWavePink2.SetScatterVars(5, KJMath.CIRCLE * 0.15f);
		E_EliteWavePink2.SetRapidVars(2, 0.5f, 0.0f);
		E_EliteWavePink2.SetDamage(1.00f);

		E_EliteRapidHell = holderObject.AddComponent<Weapon>();
		E_EliteRapidHell.SetStandardVars(4f, 500.0f, 300.0f, 0.2f);
		E_EliteRapidHell.SetDamage(1.50f);
		E_EliteRapidHell.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, Color.white);
		E_EliteRapidHell.SetRapidVars(9, 0.15f, 0.7f);

		E_EliteGreenCircle = holderObject.AddComponent<Weapon>();
		E_EliteGreenCircle.SetStandardVars(4.0f, 300.0f, 301.0f, 0.05f, false);
		E_EliteGreenCircle.SetGraphicVars(WeaponGraphic.Image.RoundGreen, greenStart, new Color(0.1f, 0.5f, 0.1f, 1.0f), EffectController.Singleton.waveTrail);
		E_EliteGreenCircle.SetDamage(1.2f);
		E_EliteGreenCircle.SetRapidVars(54, 0.065f, 0.0f, KJMath.CIRCLE * 2);

		E_EliteBlueCircle = holderObject.AddComponent<Weapon>();
		E_EliteBlueCircle.SetStandardVars(4.0f, 300.0f, 301.0f, 0.05f, false);
		E_EliteBlueCircle.SetGraphicVars(WeaponGraphic.Image.RoundBlue, blueStart, new Color(0.0f, 0.0f, 0.8f, 1.0f), EffectController.Singleton.waveTrail);
		E_EliteBlueCircle.SetDamage(0.5f);
		E_EliteBlueCircle.SetRapidVars(54, 0.065f, 0.0f, KJMath.CIRCLE * 2);
		E_EliteBlueCircle.SetBuff(Buff.Type.Stun, 100, 0.5f, 2.0f);

		E_EliteRedCircle = holderObject.AddComponent<Weapon>();
		E_EliteRedCircle.SetStandardVars(4.0f, 300.0f, 301.0f, 0.05f, false);
		E_EliteRedCircle.SetGraphicVars(WeaponGraphic.Image.RoundRed, redStart, new Color(1.0f, 0.2f, 0.2f, 1.0f), EffectController.Singleton.waveTrail);
		E_EliteRedCircle.SetDamage(0.35f);
		E_EliteRedCircle.SetRapidVars(54, 0.065f, 0.0f, KJMath.CIRCLE * 2);
		E_EliteRedCircle.SetBuff(Buff.Type.Burn);

		E_ElitePinkCircle = holderObject.AddComponent<Weapon>();
		E_ElitePinkCircle.SetStandardVars(4.0f, 300.0f, 301.0f, 0.05f, false);
		E_ElitePinkCircle.SetGraphicVars(WeaponGraphic.Image.RoundPink, pinkStart, new Color(0.7f, 0.3f, 0.7f, 1.0f), EffectController.Singleton.waveTrail);
		E_ElitePinkCircle.SetDamage(1.5f);
		E_ElitePinkCircle.SetRapidVars(54, 0.065f, 0.0f, KJMath.CIRCLE * 2);

		//Elite Boss
		Elite_GatheringRed = holderObject.AddComponent<Weapon>(); 
		Elite_GatheringRed.SetStandardVars(3f, 301.0f, 300.0f, 0.50f);
		Elite_GatheringRed.SetGraphicVars(WeaponGraphic.Image.StandardRed, redStart, new Color(1.0f, 0.7f, 0.3f, 1.0f));
		Elite_GatheringRed.SetDamage(0.4f);
		Elite_GatheringRed.SetScatterVars(3, KJMath.CIRCLE * 0.15f);
		Elite_GatheringRed.SetRapidVars(6, 0.25f, 0.0f);
		
		Elite_GatheringGreen = holderObject.AddComponent<Weapon>(); 
		Elite_GatheringGreen.SetStandardVars(3f, 301.0f, 300.0f, 0.50f);
		Elite_GatheringGreen.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(1.0f, 0.7f, 0.3f, 1.0f));
		Elite_GatheringGreen.SetDamage(0.4f);
		Elite_GatheringGreen.SetScatterVars(3, KJMath.CIRCLE * 0.15f);
		Elite_GatheringGreen.SetRapidVars(6, 0.25f, 0.0f);
		
		Elite_GatheringBlue = holderObject.AddComponent<Weapon>(); 
		Elite_GatheringBlue.SetStandardVars(3f, 301.0f, 300.0f, 0.50f);
		Elite_GatheringBlue.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(1.0f, 0.7f, 0.3f, 1.0f));
		Elite_GatheringBlue.SetDamage(0.4f);
		Elite_GatheringBlue.SetScatterVars(3, KJMath.CIRCLE * 0.15f);
		Elite_GatheringBlue.SetRapidVars(6, 0.25f, 0.0f);

		Elite_RapidHardBlue = holderObject.AddComponent<Weapon>();
		Elite_RapidHardBlue.SetStandardVars(2.5f, 500.0f, 250.0f, 0.2f);
		Elite_RapidHardBlue.SetDamage(0.6f);
		Elite_RapidHardBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, Color.white);
		Elite_RapidHardBlue.SetRapidVars(14, 0.10f, 0.75f);
		Elite_RapidHardBlue.SetBuff(Buff.Type.Stun);

		Elite_RapidHardGreen = holderObject.AddComponent<Weapon>();
		Elite_RapidHardGreen.SetStandardVars(2.5f, 500.0f, 250.0f, 0.2f);
		Elite_RapidHardGreen.SetDamage(0.50f);
		Elite_RapidHardGreen.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, Color.white);
		Elite_RapidHardGreen.SetRapidVars(14, 0.10f, 0.75f);
		Elite_RapidHardGreen.SetBuff(Buff.Type.Acid, 100, 1.0f, 4.0f);

		Elite_RapidHardRed = holderObject.AddComponent<Weapon>();
		Elite_RapidHardRed.SetStandardVars(2.5f, 500.0f, 250.0f, 0.2f);
		Elite_RapidHardRed.SetDamage(0.35f);
		Elite_RapidHardRed.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, Color.white);
		Elite_RapidHardRed.SetRapidVars(14, 0.10f, 0.75f);
		Elite_RapidHardRed.SetBuff(Buff.Type.Burn);

		Elite_SweepHardBlue = holderObject.AddComponent<Weapon>();
		Elite_SweepHardBlue.SetStandardVars(1.6f, 600.0f, 350.0f, 0.2f);
		Elite_SweepHardBlue.SetDamage(0.6f);
		Elite_SweepHardBlue.SetGraphicVars(WeaponGraphic.Image.StarBlue, blueStart, Color.white);
		Elite_SweepHardBlue.SetRapidVars(10, 0.10f, 0.0f, 2.4f);
		Elite_SweepHardBlue.SetBuff(Buff.Type.Stun);

		Elite_SweepHardGreen = holderObject.AddComponent<Weapon>();
		Elite_SweepHardGreen.SetStandardVars(1.6f, 600.0f, 350.0f, 0.2f);
		Elite_SweepHardGreen.SetDamage(0.5f);
		Elite_SweepHardGreen.SetGraphicVars(WeaponGraphic.Image.StarGreen, greenStart, Color.white);
		Elite_SweepHardGreen.SetRapidVars(10, 0.10f, 0.0f, 2.4f);
		Elite_SweepHardGreen.SetBuff(Buff.Type.Acid, 100, 1.0f, 4.0f);

		Elite_SweepHardRed = holderObject.AddComponent<Weapon>();
		Elite_SweepHardRed.SetStandardVars(1.6f, 600.0f, 350.0f, 0.2f);
		Elite_SweepHardRed.SetDamage(0.35f);
		Elite_SweepHardRed.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		Elite_SweepHardRed.SetRapidVars(10, 0.10f, 0.0f, 2.4f);
		Elite_SweepHardRed.SetBuff(Buff.Type.Burn);

		Elite_WaveBlue = holderObject.AddComponent<Weapon>();
		Elite_WaveBlue.SetStandardVars(1.75f, 0.0f, 400.0f, 0.1f);
		Elite_WaveBlue.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.1f, 0.2f, 0.8f), EffectController.Singleton.waveTrail);
		Elite_WaveBlue.SetScatterVars(6, KJMath.CIRCLE * 0.25f);
		Elite_WaveBlue.SetRapidVars(2, 0.5f, 0.0f);
		Elite_WaveBlue.SetDamage(0.50f);
		Elite_WaveBlue.SetBuff(Buff.Type.Stun);

		Elite_WaveGreen = holderObject.AddComponent<Weapon>();
		Elite_WaveGreen.SetStandardVars(1.75f, 0.0f, 400.0f, 0.1f);
		Elite_WaveGreen.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.2f, 1.0f, 0.2f), EffectController.Singleton.waveTrail);
		Elite_WaveGreen.SetScatterVars(6, KJMath.CIRCLE * 0.25f);
		Elite_WaveGreen.SetRapidVars(2, 0.5f, 0.0f);
		Elite_WaveGreen.SetDamage(0.50f);
		Elite_WaveGreen.SetBuff(Buff.Type.Acid, 100, 1.0f, 4.0f);

		Elite_WaveRed = holderObject.AddComponent<Weapon>();
		Elite_WaveRed.SetStandardVars(1.75f, 0.0f, 400.0f, 0.1f);
		Elite_WaveRed.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.00f, 0.0f, 0.2f), EffectController.Singleton.waveTrail);
		Elite_WaveRed.SetScatterVars(6, KJMath.CIRCLE * 0.25f);
		Elite_WaveRed.SetRapidVars(2, 0.5f, 0.0f);
		Elite_WaveRed.SetDamage(0.35f);
		Elite_WaveRed.SetBuff(Buff.Type.Burn);

		Elite_GreenHoming = holderObject.AddComponent<Weapon>();
		Elite_GreenHoming.SetStandardVars(3.0f, 1500.0f, 450.0f, 1.2f);
		Elite_GreenHoming.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.shockTrail);
		Elite_GreenHoming.SetChainerVars(1);
		Elite_GreenHoming.SetHomingVars();
		Elite_GreenHoming.SetScatterVars(3, 0.0f);
		Elite_GreenHoming.SetEjectionVars();
		Elite_GreenHoming.SetDamage(0.75f);
		Elite_GreenHoming.SetBuff(Buff.Type.Acid, 100, 1 , 5.0f);
		
		Elite_BlueHoming = holderObject.AddComponent<Weapon>();
		Elite_BlueHoming.SetStandardVars(3.0f, 1500.0f, 450.0f, 1.2f);
		Elite_BlueHoming.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		Elite_BlueHoming.SetChainerVars(1);
		Elite_BlueHoming.SetHomingVars();
		Elite_BlueHoming.SetScatterVars(3, 0.0f);
		Elite_BlueHoming.SetEjectionVars();
		Elite_BlueHoming.SetDamage(1.0f);
		Elite_BlueHoming.SetBuff(Buff.Type.Stun, 100, 1 , 2.0f);

		Elite_RedHoming = holderObject.AddComponent<Weapon>();
		Elite_RedHoming.SetStandardVars(3.0f, 1500.0f, 450.0f, 1.2f);
		Elite_RedHoming.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color (1.0f, 0.2f, 0.0f), EffectController.Singleton.shockTrail);
		Elite_RedHoming.SetChainerVars(1);
		Elite_RedHoming.SetHomingVars();
		Elite_RedHoming.SetScatterVars(3, 0.0f);
		Elite_RedHoming.SetEjectionVars();
		Elite_RedHoming.SetDamage(0.25f);
		Elite_RedHoming.SetBuff(Buff.Type.Burn);
		
		Elite_PinkHoming = holderObject.AddComponent<Weapon>();
		Elite_PinkHoming.SetStandardVars(3.0f, 1500.0f, 450.0f, 1.2f);
		Elite_PinkHoming.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color (0.7f, 0.2f, 0.7f, 1.0f), EffectController.Singleton.shockTrail);
		Elite_PinkHoming.SetChainerVars(1);
		Elite_PinkHoming.SetHomingVars();
		Elite_PinkHoming.SetScatterVars(3, 0.0f);
		Elite_PinkHoming.SetEjectionVars();
		Elite_PinkHoming.SetDamage(1.35f);

		Elite_GreenHoming2 = holderObject.AddComponent<Weapon>();
		Elite_GreenHoming2.SetStandardVars(3.75f, 1500.0f, 525.0f, 1.2f);
		Elite_GreenHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.shockTrail);
		Elite_GreenHoming2.SetChainerVars(1);
		Elite_GreenHoming2.SetHomingVars();
		Elite_GreenHoming2.SetScatterVars(2, 0.0f);
		Elite_GreenHoming2.SetEjectionVars();
		Elite_GreenHoming2.SetDamage(0.75f);
		Elite_GreenHoming2.SetRapidVars(2, 0.75f, 0.0f);
		Elite_GreenHoming2.SetBuff(Buff.Type.Acid, 100, 1 , 5.0f);

		Elite_RedHoming2 = holderObject.AddComponent<Weapon>();
		Elite_RedHoming2.SetStandardVars(3.75f, 1500.0f, 525.0f, 1.2f);
		Elite_RedHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color (1.0f, 0.2f, 0.0f), EffectController.Singleton.shockTrail);
		Elite_RedHoming2.SetChainerVars(1);
		Elite_RedHoming2.SetHomingVars();
		Elite_RedHoming2.SetScatterVars(2, 0.0f);
		Elite_RedHoming2.SetEjectionVars();
		Elite_RedHoming2.SetDamage(0.25f);
		Elite_RedHoming2.SetRapidVars(2, 0.75f, 0.0f);
		Elite_RedHoming2.SetBuff(Buff.Type.Burn);

		Elite_BlueHoming2 = holderObject.AddComponent<Weapon>();
		Elite_BlueHoming2.SetStandardVars(3.75f, 1500.0f, 525.0f, 1.2f);
		Elite_BlueHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		Elite_BlueHoming2.SetChainerVars(1);
		Elite_BlueHoming2.SetHomingVars();
		Elite_BlueHoming2.SetScatterVars(2, 0.0f);
		Elite_BlueHoming2.SetEjectionVars();
		Elite_BlueHoming2.SetDamage(1.0f);
		Elite_BlueHoming2.SetRapidVars(2, 0.75f, 0.0f);
		Elite_BlueHoming2.SetBuff(Buff.Type.Stun, 100, 1 , 2.0f);

		Elite_PinkHoming2 = holderObject.AddComponent<Weapon>();
		Elite_PinkHoming2.SetStandardVars(3.75f, 1500.0f, 525.0f, 1.2f);
		Elite_PinkHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color (0.7f, 0.2f, 0.7f, 1.0f), EffectController.Singleton.shockTrail);
		Elite_PinkHoming2.SetChainerVars(1);
		Elite_PinkHoming2.SetHomingVars();
		Elite_PinkHoming2.SetScatterVars(2, 0.0f);
		Elite_PinkHoming2.SetEjectionVars();
		Elite_PinkHoming2.SetDamage(1.35f);
		Elite_PinkHoming2.SetRapidVars(2, 0.75f, 0.0f);

		Elite_Missile2_2Yellow = holderObject.AddComponent<Weapon>();
		Elite_Missile2_2Yellow.SetStandardVars(3.75f, 50.0f, 350.0f, 0.50f, false);
		Elite_Missile2_2Yellow.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_Missile2_2Yellow.SetHomingVars();
		Elite_Missile2_2Yellow.SetEjectionVars ();
		Elite_Missile2_2Yellow.SetDamage(1.25f);
		Elite_Missile2_2Yellow.SetScatterVars(2, 0.0f);
		Elite_Missile2_2Yellow.SetRapidVars(2, 0.5f, 0.0f);
		Elite_Missile2_2Yellow.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Elite_Missile2_2Blue = holderObject.AddComponent<Weapon>();
		Elite_Missile2_2Blue.SetStandardVars(4.25f, 100.0f, 400.0f, 0.50f, false);
		Elite_Missile2_2Blue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_Missile2_2Blue.SetHomingVars();
		Elite_Missile2_2Blue.SetEjectionVars ();
		Elite_Missile2_2Blue.SetDamage(1.1f);
		Elite_Missile2_2Blue.SetScatterVars(2, 0.0f);
		Elite_Missile2_2Blue.SetRapidVars(2, 0.6f, 0.0f);
		Elite_Missile2_2Blue.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Elite_Missile2_2Red = holderObject.AddComponent<Weapon>();
		Elite_Missile2_2Red.SetStandardVars(3.6f, 75.0f, 375.0f, 0.50f, false);
		Elite_Missile2_2Red.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_Missile2_2Red.SetHomingVars();
		Elite_Missile2_2Red.SetEjectionVars ();
		Elite_Missile2_2Red.SetDamage(1.0f);
		Elite_Missile2_2Red.SetScatterVars(2, 0.0f);
		Elite_Missile2_2Red.SetRapidVars(2, 0.75f, 0.0f);
		Elite_Missile2_2Red.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Elite_Missile2_2Pink = holderObject.AddComponent<Weapon>();
		Elite_Missile2_2Pink.SetStandardVars(3.3f, 75.0f, 375.0f, 0.50f, false);
		Elite_Missile2_2Pink.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_Missile2_2Pink.SetHomingVars();
		Elite_Missile2_2Pink.SetEjectionVars ();
		Elite_Missile2_2Pink.SetDamage(1.0f);
		Elite_Missile2_2Pink.SetScatterVars(2, 0.0f);
		Elite_Missile2_2Pink.SetRapidVars(2, 0.75f, 0.0f);
		Elite_Missile2_2Pink.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Elite_Missile3Blue = holderObject.AddComponent<Weapon>();
		Elite_Missile3Blue.SetStandardVars(2.5f, 75.0f, 425.0f, 0.50f, false);
		Elite_Missile3Blue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_Missile3Blue.SetHomingVars();
		Elite_Missile3Blue.SetEjectionVars ();
		Elite_Missile3Blue.SetDamage(1.3f);
		Elite_Missile3Blue.SetScatterVars(3, 0.0f);
		Elite_Missile3Blue.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Elite_Missile3Yellow = holderObject.AddComponent<Weapon>();
		Elite_Missile3Yellow.SetStandardVars(2.75f, 75.0f, 425.0f, 0.50f, false);
		Elite_Missile3Yellow.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_Missile3Yellow.SetHomingVars();
		Elite_Missile3Yellow.SetEjectionVars ();
		Elite_Missile3Yellow.SetDamage(1.4f);
		Elite_Missile3Yellow.SetScatterVars(3, 0.0f);
		Elite_Missile3Yellow.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Elite_Missile3Red = holderObject.AddComponent<Weapon>();
		Elite_Missile3Red.SetStandardVars(3.25f, 75.0f, 425.0f, 0.50f, false);
		Elite_Missile3Red.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_Missile3Red.SetHomingVars();
		Elite_Missile3Red.SetEjectionVars ();
		Elite_Missile3Red.SetDamage(1.5f);
		Elite_Missile3Red.SetScatterVars(3, 0.0f);
		Elite_Missile3Red.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Elite_Missile3Pink = holderObject.AddComponent<Weapon>();
		Elite_Missile3Pink.SetStandardVars(3.25f, 75.0f, 425.0f, 0.50f, false);
		Elite_Missile3Pink.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_Missile3Pink.SetHomingVars();
		Elite_Missile3Pink.SetEjectionVars ();
		Elite_Missile3Pink.SetDamage(1.6f);
		Elite_Missile3Pink.SetScatterVars(3, 0.0f);
		Elite_Missile3Pink.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Elite_MissileSetYellow = holderObject.AddComponent<Weapon>();
		Elite_MissileSetYellow.SetStandardVars(4.0f, 75.0f, 375.0f, 0.2f, false);
		Elite_MissileSetYellow.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_MissileSetYellow.SetHomingVars();
		Elite_MissileSetYellow.SetDamage(1.25f);
		Elite_MissileSetYellow.SetRapidVars(4, 0.20f, 0.0f);
		Elite_MissileSetYellow.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Elite_MissileSetRed = holderObject.AddComponent<Weapon>();
		Elite_MissileSetRed.SetStandardVars(3.75f, 75.0f, 375.0f, 0.2f, false);
		Elite_MissileSetRed.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_MissileSetRed.SetHomingVars();
		Elite_MissileSetRed.SetDamage(1.35f);
		Elite_MissileSetRed.SetRapidVars(4, 0.20f, 0.0f);
		Elite_MissileSetRed.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Elite_MissileSetBlue = holderObject.AddComponent<Weapon>();
		Elite_MissileSetBlue.SetStandardVars(3.75f, 75.0f, 375.0f, 0.2f, false);
		Elite_MissileSetBlue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_MissileSetBlue.SetHomingVars();
		Elite_MissileSetBlue.SetDamage(1.25f);
		Elite_MissileSetBlue.SetRapidVars(4, 0.20f, 0.0f);
		Elite_MissileSetBlue.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Elite_MissileSetPink = holderObject.AddComponent<Weapon>();
		Elite_MissileSetPink.SetStandardVars(4.0f, 75.0f, 375.0f, 0.2f, false);
		Elite_MissileSetPink.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		Elite_MissileSetPink.SetHomingVars();
		Elite_MissileSetPink.SetDamage(1.5f);
		Elite_MissileSetPink.SetRapidVars(4, 0.20f, 0.0f);
		Elite_MissileSetPink.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		Rival_BlueHoming = holderObject.AddComponent<Weapon>();
		Rival_BlueHoming.SetStandardVars(5.0f, 600.0f, 300.0f, 1.2f);
		Rival_BlueHoming.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		Rival_BlueHoming.SetChainerVars(1);
		Rival_BlueHoming.SetHomingVars();
		Rival_BlueHoming.SetScatterVars(3, 0.0f);
		Rival_BlueHoming.SetEjectionVars();
		Rival_BlueHoming.SetDamage(1.0f);
		Rival_BlueHoming.SetBuff(Buff.Type.Stun, 100, 1 , 2.0f);
		
		Rival_RedHoming = holderObject.AddComponent<Weapon>();
		Rival_RedHoming.SetStandardVars(7.0f, 600.0f, 300.0f, 1.2f);
		Rival_RedHoming.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color (1.0f, 0.2f, 0.0f), EffectController.Singleton.shockTrail);
		Rival_RedHoming.SetChainerVars(1);
		Rival_RedHoming.SetHomingVars();
		Rival_RedHoming.SetScatterVars(3, 0.0f);
		Rival_RedHoming.SetEjectionVars();
		Rival_RedHoming.SetDamage(0.25f);
		Rival_RedHoming.SetBuff(Buff.Type.Burn);
		
		Rival_PinkHoming = holderObject.AddComponent<Weapon>();
		Rival_PinkHoming.SetStandardVars(7.0f, 600.0f, 300.0f, 1.2f);
		Rival_PinkHoming.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color (0.7f, 0.2f, 0.7f, 1.0f), EffectController.Singleton.shockTrail);
		Rival_PinkHoming.SetChainerVars(1);
		Rival_PinkHoming.SetHomingVars();
		Rival_PinkHoming.SetScatterVars(3, 0.0f);
		Rival_PinkHoming.SetEjectionVars();
		Rival_PinkHoming.SetDamage(1.35f);
		
		Rival_GreenHoming2 = holderObject.AddComponent<Weapon>();
		Rival_GreenHoming2.SetStandardVars(7.0f, 600.0f, 300.0f, 1.2f);
		Rival_GreenHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.shockTrail);
		Rival_GreenHoming2.SetChainerVars(1);
		Rival_GreenHoming2.SetHomingVars();
		Rival_GreenHoming2.SetScatterVars(2, 0.0f);
		Rival_GreenHoming2.SetEjectionVars();
		Rival_GreenHoming2.SetDamage(0.75f);
		Rival_GreenHoming2.SetRapidVars(2, 0.75f, 0.0f);
		Rival_GreenHoming2.SetBuff(Buff.Type.Acid, 100, 1 , 5.0f);
		
		Rival_RedHoming2 = holderObject.AddComponent<Weapon>();
		Rival_RedHoming2.SetStandardVars(7.0f, 600.0f, 300.0f, 1.2f);
		Rival_RedHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color (1.0f, 0.2f, 0.0f), EffectController.Singleton.shockTrail);
		Rival_RedHoming2.SetChainerVars(1);
		Rival_RedHoming2.SetHomingVars();
		Rival_RedHoming2.SetScatterVars(2, 0.0f);
		Rival_RedHoming2.SetEjectionVars();
		Rival_RedHoming2.SetDamage(0.25f);
		Rival_RedHoming2.SetRapidVars(2, 0.75f, 0.0f);
		Rival_RedHoming2.SetBuff(Buff.Type.Burn);
		
		Rival_BlueHoming2 = holderObject.AddComponent<Weapon>();
		Rival_BlueHoming2.SetStandardVars(7.0f, 600.0f, 300.0f, 1.2f);
		Rival_BlueHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		Rival_BlueHoming2.SetChainerVars(1);
		Rival_BlueHoming2.SetHomingVars();
		Rival_BlueHoming2.SetScatterVars(2, 0.0f);
		Rival_BlueHoming2.SetEjectionVars();
		Rival_BlueHoming2.SetDamage(1.0f);
		Rival_BlueHoming2.SetRapidVars(2, 0.75f, 0.0f);
		Rival_BlueHoming2.SetBuff(Buff.Type.Stun, 100, 1 , 2.0f);
		
		Rival_PinkHoming2 = holderObject.AddComponent<Weapon>();
		Rival_PinkHoming2.SetStandardVars(7.0f, 600.0f, 300.0f, 1.2f);
		Rival_PinkHoming2.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color (0.7f, 0.2f, 0.7f, 1.0f), EffectController.Singleton.shockTrail);
		Rival_PinkHoming2.SetChainerVars(1);
		Rival_PinkHoming2.SetHomingVars();
		Rival_PinkHoming2.SetScatterVars(2, 0.0f);
		Rival_PinkHoming2.SetEjectionVars();
		Rival_PinkHoming2.SetDamage(1.35f);
		Rival_PinkHoming2.SetRapidVars(2, 0.75f, 0.0f);
		
		Rival_Missile2_2Yellow = holderObject.AddComponent<Weapon>();
		Rival_Missile2_2Yellow.SetStandardVars(7f, 50.0f, 300.0f, 0.50f, false);
		Rival_Missile2_2Yellow.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_Missile2_2Yellow.SetHomingVars();
		Rival_Missile2_2Yellow.SetEjectionVars ();
		Rival_Missile2_2Yellow.SetDamage(1.25f);
		Rival_Missile2_2Yellow.SetScatterVars(2, 0.0f);
		Rival_Missile2_2Yellow.SetRapidVars(2, 0.5f, 0.0f);
		Rival_Missile2_2Yellow.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_Missile2_2Blue = holderObject.AddComponent<Weapon>();
		Rival_Missile2_2Blue.SetStandardVars(7f, 100.0f, 300.0f, 0.50f, false);
		Rival_Missile2_2Blue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_Missile2_2Blue.SetHomingVars();
		Rival_Missile2_2Blue.SetEjectionVars ();
		Rival_Missile2_2Blue.SetDamage(1.1f);
		Rival_Missile2_2Blue.SetScatterVars(2, 0.0f);
		Rival_Missile2_2Blue.SetRapidVars(2, 0.6f, 0.0f);
		Rival_Missile2_2Blue.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_Missile2_2Red = holderObject.AddComponent<Weapon>();
		Rival_Missile2_2Red.SetStandardVars(7f, 75.0f, 300.0f, 0.50f, false);
		Rival_Missile2_2Red.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_Missile2_2Red.SetHomingVars();
		Rival_Missile2_2Red.SetEjectionVars ();
		Rival_Missile2_2Red.SetDamage(1.0f);
		Rival_Missile2_2Red.SetScatterVars(2, 0.0f);
		Rival_Missile2_2Red.SetRapidVars(2, 0.75f, 0.0f);
		Rival_Missile2_2Red.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_Missile2_2Pink = holderObject.AddComponent<Weapon>();
		Rival_Missile2_2Pink.SetStandardVars(7f, 75.0f, 300.0f, 0.50f, false);
		Rival_Missile2_2Pink.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_Missile2_2Pink.SetHomingVars();
		Rival_Missile2_2Pink.SetEjectionVars ();
		Rival_Missile2_2Pink.SetDamage(1.0f);
		Rival_Missile2_2Pink.SetScatterVars(2, 0.0f);
		Rival_Missile2_2Pink.SetRapidVars(2, 0.75f, 0.0f);
		Rival_Missile2_2Pink.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_Missile3Blue = holderObject.AddComponent<Weapon>();
		Rival_Missile3Blue.SetStandardVars(7f, 75.0f, 300.0f, 0.50f, false);
		Rival_Missile3Blue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_Missile3Blue.SetHomingVars();
		Rival_Missile3Blue.SetEjectionVars ();
		Rival_Missile3Blue.SetDamage(1.3f);
		Rival_Missile3Blue.SetScatterVars(3, 0.0f);
		Rival_Missile3Blue.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_Missile3Yellow = holderObject.AddComponent<Weapon>();
		Rival_Missile3Yellow.SetStandardVars(7f, 75.0f, 300.0f, 0.50f, false);
		Rival_Missile3Yellow.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_Missile3Yellow.SetHomingVars();
		Rival_Missile3Yellow.SetEjectionVars ();
		Rival_Missile3Yellow.SetDamage(1.4f);
		Rival_Missile3Yellow.SetScatterVars(3, 0.0f);
		Rival_Missile3Yellow.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_Missile3Red = holderObject.AddComponent<Weapon>();
		Rival_Missile3Red.SetStandardVars(7f, 75.0f, 300.0f, 0.50f, false);
		Rival_Missile3Red.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_Missile3Red.SetHomingVars();
		Rival_Missile3Red.SetEjectionVars ();
		Rival_Missile3Red.SetDamage(1.5f);
		Rival_Missile3Red.SetScatterVars(3, 0.0f);
		Rival_Missile3Red.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_Missile3Pink = holderObject.AddComponent<Weapon>();
		Rival_Missile3Pink.SetStandardVars(7f, 75.0f, 300.0f, 0.50f, false);
		Rival_Missile3Pink.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_Missile3Pink.SetHomingVars();
		Rival_Missile3Pink.SetEjectionVars ();
		Rival_Missile3Pink.SetDamage(1.6f);
		Rival_Missile3Pink.SetScatterVars(3, 0.0f);
		Rival_Missile3Pink.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_MissileSetYellow = holderObject.AddComponent<Weapon>();
		Rival_MissileSetYellow.SetStandardVars(7f, 75.0f, 300.0f, 0.2f, false);
		Rival_MissileSetYellow.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_MissileSetYellow.SetHomingVars();
		Rival_MissileSetYellow.SetDamage(1.25f);
		Rival_MissileSetYellow.SetRapidVars(4, 0.20f, 0.0f);
		Rival_MissileSetYellow.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_MissileSetRed = holderObject.AddComponent<Weapon>();
		Rival_MissileSetRed.SetStandardVars(7f, 75.0f, 300.0f, 0.2f, false);
		Rival_MissileSetRed.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_MissileSetRed.SetHomingVars();
		Rival_MissileSetRed.SetDamage(1.35f);
		Rival_MissileSetRed.SetRapidVars(4, 0.20f, 0.0f);
		Rival_MissileSetRed.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_MissileSetBlue = holderObject.AddComponent<Weapon>();
		Rival_MissileSetBlue.SetStandardVars(7f, 75.0f, 300.0f, 0.2f, false);
		Rival_MissileSetBlue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_MissileSetBlue.SetHomingVars();
		Rival_MissileSetBlue.SetDamage(1.25f);
		Rival_MissileSetBlue.SetRapidVars(4, 0.20f, 0.0f);
		Rival_MissileSetBlue.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
		Rival_MissileSetPink = holderObject.AddComponent<Weapon>();
		Rival_MissileSetPink.SetStandardVars(7.0f, 75.0f, 300.0f, 0.2f, false);
		Rival_MissileSetPink.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		Rival_MissileSetPink.SetHomingVars();
		Rival_MissileSetPink.SetDamage(1.5f);
		Rival_MissileSetPink.SetRapidVars(4, 0.20f, 0.0f);
		Rival_MissileSetPink.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));


		Elite_BeamBlue = holderObject.AddComponent<Weapon>();
		Elite_BeamBlue.SetStandardVars(7f, 400.0f, 200.0f, 0.2f);
		Elite_BeamBlue.SetDamage(6.0f);
		Elite_BeamBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(150, 216, 255, 255), new Color32(0, 76, 255, 255));
		Elite_BeamBlue.SetBeamVars(1.75f);

		Elite_BeamPink = holderObject.AddComponent<Weapon>();
		Elite_BeamPink.SetStandardVars(5f, 400.0f, 200.0f, 0.2f);
		Elite_BeamPink.SetDamage(8.0f);
		Elite_BeamPink.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(255, 218, 252, 255), new Color32(188, 0, 255, 255));
		Elite_BeamPink.SetBeamVars(1.75f);

		Elite_BeamRed = holderObject.AddComponent<Weapon>();
		Elite_BeamRed.SetStandardVars(5f, 400.0f, 200.0f, 0.2f);
		Elite_BeamRed.SetDamage(8.0f);
		Elite_BeamRed.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(255, 193, 193, 255), new Color32(255, 13, 13, 255));
		Elite_BeamRed.SetBeamVars(1.75f);
		
		Elite_BeamOrange = holderObject.AddComponent<Weapon>();
		Elite_BeamOrange.SetStandardVars(5f, 400.0f, 200.0f, 0.2f);
		Elite_BeamOrange.SetDamage(8.0f);
		Elite_BeamOrange.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(255, 227, 206, 255), new Color32(255, 86, 0, 255));
		Elite_BeamOrange.SetBeamVars(1.75f);
		
		Elite_BeamGreen = holderObject.AddComponent<Weapon>();
		Elite_BeamGreen.SetStandardVars(5f, 400.0f, 200.0f, 0.2f);
		Elite_BeamGreen.SetDamage(8.0f);
		Elite_BeamGreen.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(206, 255, 215, 255), new Color32(0, 255, 86, 255));
		Elite_BeamGreen.SetBeamVars(1.50f);

		//Enemy Weapon

		E_BlueStun = holderObject.AddComponent<Weapon>();
		E_BlueStun.SetStandardVars(3.5f, 700.0f, 200.0f, 0.2f);
		E_BlueStun.SetDamage(0.80f);
		E_BlueStun.SetGraphicVars(WeaponGraphic.Image.RoundBlue, blueStart, Color.white);
		E_BlueStun.SetRapidVars(18, 0.09f, 0.7f);
		E_BlueStun.SetBuff(Buff.Type.Stun);

		E_Rapid = holderObject.AddComponent<Weapon>();
		E_Rapid.SetStandardVars(3.5f, 500.0f, 200.0f, 0.2f);
		E_Rapid.SetDamage(0.7f);
		E_Rapid.SetGraphicVars(WeaponGraphic.Image.StarGreen, greenStart, Color.white);
		E_Rapid.SetRapidVars(6, 0.10f, 0.7f);

		E_Slow = holderObject.AddComponent<Weapon>();
		E_Slow.SetStandardVars(1.0f, 300.0f, 150.0f, 0.2f);
		E_Slow.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 0.0f));
		E_Slow.SetRapidVars(3, 0.25f, 0.5f);

		E_Slow2 = holderObject.AddComponent<Weapon>();
		E_Slow2.SetStandardVars(2.5f, 300.0f, 150.0f, 0.2f);
		E_Slow2.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 0.0f));
		E_Slow2.SetRapidVars(3, 0.25f, 0.5f);

		E_BH_Slow = holderObject.AddComponent<Weapon>();
		E_BH_Slow.SetStandardVars(1.0f, 300.0f, 150.0f, 0.2f);
		E_BH_Slow.SetDamage(2.0f);
		E_BH_Slow.SetGraphicVars(WeaponGraphic.Image.RoundPink, pinkStart, new Color(1.0f, 0.0f, 0.0f, 0.0f));
		E_BH_Slow.SetRapidVars(3, 0.3f, 0.5f);

		E_Support = holderObject.AddComponent<Weapon>();
		E_Support.SetStandardVars(2.0f, 250.0f, 150.0f, 0.2f);
		E_Support.SetGraphicVars(WeaponGraphic.Image.RoundRed, redStart, new Color(1.0f, 0.0f, 0.0f, 0.0f));
		E_Support.SetRapidVars(2, 0.25f, 0.5f);

		E_Sweep = holderObject.AddComponent<Weapon>();
		E_Sweep.SetStandardVars(2.5f, 500.0f, 200.0f, 0.2f);
		E_Sweep.SetDamage(0.75f);
		E_Sweep.SetGraphicVars(WeaponGraphic.Image.StarGreen, greenStart, Color.white);
		E_Sweep.SetRapidVars(9, 0.10f, 0.0f, 1.8f);

		E_SweepBlue = holderObject.AddComponent<Weapon>();
		E_SweepBlue.SetStandardVars(2.5f, 300.0f, 175.0f, 0.2f);
		E_SweepBlue.SetDamage(0.5f);
		E_SweepBlue.SetGraphicVars(WeaponGraphic.Image.StarBlue, blueStart, Color.white);
		E_SweepBlue.SetRapidVars(7, 0.10f, 0.0f, 1.8f);
		E_SweepBlue.SetBuff(Buff.Type.Stun);

		E_Spread = holderObject.AddComponent<Weapon>();
		E_Spread.SetStandardVars(2.5f, 400.0f, 200.0f, 0.2f);
		E_Spread.SetDamage(1.0f);
		E_Spread.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, Color.white);
		E_Spread.SetScatterVars(6, KJMath.CIRCLE * 0.35f);

		E_SpreadBlue = holderObject.AddComponent<Weapon>();
		E_SpreadBlue.SetStandardVars(2.5f, 300.0f, 175.0f, 0.2f);
		E_SpreadBlue.SetDamage(0.5f);
		E_SpreadBlue.SetGraphicVars(WeaponGraphic.Image.SpeedBlue, blueStart, Color.white);
		E_SpreadBlue.SetScatterVars(5, KJMath.CIRCLE * 0.35f);
		E_SpreadBlue.SetBuff(Buff.Type.Stun);

		E_Scatter = holderObject.AddComponent<Weapon>();
		E_Scatter.SetStandardVars(2.5f, 400.0f, 200.0f, 0.2f);
		E_Scatter.SetDamage(1.0f);
		E_Scatter.SetGraphicVars(WeaponGraphic.Image.SpeedYellow, yellowStart, Color.white);
		E_Scatter.SetScatterVars(8, KJMath.CIRCLE);

		E_BH_Scatter = holderObject.AddComponent<Weapon>();
		E_BH_Scatter.SetStandardVars(5.0f, 400.0f, 200.0f, 0.2f);
		E_BH_Scatter.SetDamage(2.0f);
		E_BH_Scatter.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, Color.white);
		E_BH_Scatter.SetScatterVars(8, KJMath.CIRCLE);
		E_BH_Scatter.SetRapidVars(3, 0.5f, 0, 2.5f);

		E_BH_Rapid = holderObject.AddComponent<Weapon>();
		E_BH_Rapid.SetStandardVars(7.0f, 400.0f, 200.0f, 0.2f);
		E_BH_Rapid.SetDamage(2.0f);
		E_BH_Rapid.SetGraphicVars(WeaponGraphic.Image.StarPink, pinkStart, Color.white);
		// E_BH_Rapid.SetScatterVars(3, 1.5f);
		E_BH_Rapid.SetRapidVars(8, 0.20f, 0.8f);
		
		E_Beam = holderObject.AddComponent<Weapon>();
		E_Beam.SetStandardVars(3.0f, 400.0f, 200.0f, 0.2f);
		E_Beam.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(1.0f, 0.8f, 0.1f, 1.0f), new Color(1.0f, 0.2f, 0.0f, 1.0f));
		E_Beam.SetDamage(7.0f);
		E_Beam.SetBeamVars(1.35f);
		
		E_BeamHell = holderObject.AddComponent<Weapon>();
		E_BeamHell.SetStandardVars(6.0f, 400.0f, 200.0f, 0.2f);
		E_BeamHell.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(1.0f, 0.8f, 1.0f, 1.0f), new Color(0.75f, 0.2f, 1.0f, 1.0f));
		E_BeamHell.SetDamage(7.0f);
		E_BeamHell.SetBeamVars(3.0f);
		
		E_BeamStun = holderObject.AddComponent<Weapon>();
		E_BeamStun.SetStandardVars(6.0f, 400.0f, 200.0f, 0.2f);
		E_BeamStun.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(150, 173, 255, 255), new Color32(0, 44, 255, 255));
		E_BeamStun.SetDamage(7.0f);
		E_BeamStun.SetBeamVars(3.0f);
		E_BeamStun.SetBuff(Buff.Type.Stun, 100, 1.0f, 2.0f);
		
		E_BeamBurn = holderObject.AddComponent<Weapon>();
		E_BeamBurn.SetStandardVars(6.0f, 400.0f, 200.0f, 0.2f);
		E_BeamBurn.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(1.0f, 0.7f, 0.7f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		E_BeamBurn.SetDamage(1.0f);
		E_BeamBurn.SetBeamVars(2.2f);
		E_BeamBurn.SetBuff(Buff.Type.Burn);

		E_BlackHole = holderObject.AddComponent<Weapon>();
		E_BlackHole.SetStandardVars(99.0f, 325.0f, 200.0f, 0.2f);
		E_BlackHole.SetGraphicVars(WeaponGraphic.Image.BlackHoleBomb, new Color(1.0f, 0.5f, 1.0f, 1.0f), new Color(0.1f, 0.3f, 1.0f, 0.0f), EffectController.Singleton.sonarTrail);
		E_BlackHole.SetDamage(3.0f);
		E_BlackHole.SetTime(1.50f);
		E_BlackHole.SetHomingVars();
		E_BlackHole.SetBlackHole(70, 3.5f);

		E_Chainer = holderObject.AddComponent<Weapon>();
		E_Chainer.SetStandardVars(3.0f, 800.0f, 375.0f, 0.2f);
		E_Chainer.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		E_Chainer.SetChainerVars(3);
		E_Chainer.SetHomingVars();
		E_Chainer.SetDamage(1.2f);
		E_Chainer.SetBuff(Buff.Type.Stun, 100);

		E_Homing = holderObject.AddComponent<Weapon>();
		E_Homing.SetStandardVars(4.0f, 0.0f, 300.0f, 0.05f, false);
		E_Homing.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_Homing.SetHomingVars();
		E_Homing.SetDamage(1.5f);
//		E_Homing.SetRapidVars(5, 0.15f, 0, 0);
		E_Homing.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		E_Homing2 = holderObject.AddComponent<Weapon>();
		E_Homing2.SetStandardVars(4.0f, 0.0f, 300.0f, 0.05f, false);
		E_Homing2.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_Homing2.SetHomingVars();
		E_Homing2.SetDamage(1.5f);
		E_Homing2.SetEjectionVars();
		E_Homing2.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
	    E_Homing2.SetScatterVars(2, 0.0f);

		E_HomingRed = holderObject.AddComponent<Weapon>();
		E_HomingRed.SetStandardVars(4.0f, 0.0f, 300.0f, 0.05f, false);
		E_HomingRed.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.missileTrail);
		E_HomingRed.SetHomingVars();
		E_HomingRed.SetDamage(0.45f);
		E_HomingRed.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		E_HomingRed.SetBuff(Buff.Type.Burn);

		E_HomingPurple = holderObject.AddComponent<Weapon>();
		E_HomingPurple.SetStandardVars(4.0f, 0.0f, 300.0f, 0.05f, false);
		E_HomingPurple.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		E_HomingPurple.SetHomingVars();
		E_HomingPurple.SetDamage(1.5f);
		E_HomingPurple.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		E_HomingBlue = holderObject.AddComponent<Weapon>();
		E_HomingBlue.SetStandardVars(4.0f, 0.0f, 300.0f, 0.05f, false);
		E_HomingBlue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_HomingBlue.SetHomingVars();
		E_HomingBlue.SetDamage(1.5f);
		E_HomingBlue.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		E_HomingBlue.SetBuff(Buff.Type.Stun);

		E_HomingRed2 = holderObject.AddComponent<Weapon>();
		E_HomingRed2.SetStandardVars(4.0f, 0.0f, 300.0f, 0.05f, false);
		E_HomingRed2.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.missileTrail);
		E_HomingRed2.SetHomingVars();
		E_HomingRed2.SetDamage(0.45f);
		E_HomingRed2.SetEjectionVars();
		E_HomingRed2.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		E_HomingRed2.SetScatterVars(2, 0.0f);
		E_HomingRed2.SetBuff(Buff.Type.Burn);

		E_HomingPurple2 = holderObject.AddComponent<Weapon>();
		E_HomingPurple2.SetStandardVars(4.0f, 0.0f, 300.0f, 0.05f, false);
		E_HomingPurple2.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.7f, 0.1f, 0.9f, 1.0f), new Color(0.7f, 0.1f, 0.9f, 1.0f), EffectController.Singleton.missileTrail);
		E_HomingPurple2.SetHomingVars();
		E_HomingPurple2.SetDamage(1.5f);
		E_HomingPurple2.SetEjectionVars();
		E_HomingPurple2.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		E_HomingPurple2.SetScatterVars(2, 0.0f);

		E_HomingBlue2 = holderObject.AddComponent<Weapon>();
		E_HomingBlue2.SetStandardVars(4.0f, 0.0f, 300.0f, 0.05f, false);
		E_HomingBlue2.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_HomingBlue2.SetHomingVars();
		E_HomingBlue2.SetDamage(1.5f);
		E_HomingBlue2.SetEjectionVars();
		E_HomingBlue2.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		E_HomingBlue2.SetScatterVars(2, 0.0f);
		E_HomingBlue2.SetBuff(Buff.Type.Stun);

		E_StrikerMissiles = holderObject.AddComponent<Weapon>();
		E_StrikerMissiles.SetStandardVars(3.5f, 0.0f, 800.0f, 0.03f, false);
		E_StrikerMissiles.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_StrikerMissiles.SetDamage(1.0f);
		E_StrikerMissiles.SetRapidVars(5, 0.15f, 0.6f);
		E_StrikerMissiles.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		E_StrikerMissilesBlue = holderObject.AddComponent<Weapon>();
		E_StrikerMissilesBlue.SetStandardVars(3.5f, 0.0f, 800.0f, 0.03f, false);
		E_StrikerMissilesBlue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_StrikerMissilesBlue.SetDamage(1.0f);
		E_StrikerMissilesBlue.SetRapidVars(5, 0.15f, 0.6f);
		E_StrikerMissilesBlue.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		E_StrikerMissilesBlue.SetBuff(Buff.Type.Stun);

		E_StrikerMissilesRed = holderObject.AddComponent<Weapon>();
		E_StrikerMissilesRed.SetStandardVars(3.5f, 0.0f, 800.0f, 0.03f, false);
		E_StrikerMissilesRed.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.5f, 0.2f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_StrikerMissilesRed.SetDamage(0.45f);
		E_StrikerMissilesRed.SetRapidVars(5, 0.15f, 0.6f);
		E_StrikerMissilesRed.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));
		E_StrikerMissilesRed.SetBuff(Buff.Type.Burn);

		E_StrikerMissilesPink = holderObject.AddComponent<Weapon>();
		E_StrikerMissilesPink.SetStandardVars(3.5f, 0.0f, 800.0f, 0.03f, false);
		E_StrikerMissilesPink.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(1.0f, 0.2f, 0.7f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_StrikerMissilesPink.SetDamage(1.0f);
		E_StrikerMissilesPink.SetRapidVars(5, 0.15f, 0.6f);
		E_StrikerMissilesPink.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		E_FastHoming = holderObject.AddComponent<Weapon>();
		E_FastHoming.SetStandardVars(2.0f, 200.0f, 325.0f, 0.05f, false);
		E_FastHoming.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.missileTrail);
		E_FastHoming.SetHomingVars();
		E_FastHoming.SetDamage(1.0f);
		E_FastHoming.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f));

		E_LaserGreen = holderObject.AddComponent<Weapon>();
		E_LaserGreen.SetStandardVars(3.0f, 500.0f, 501.0f, 0.05f, false);
		E_LaserGreen.SetGraphicVars(WeaponGraphic.Image.StandardGreen, new Color(0.2f, 0.8f, 0.2f, 1.0f), new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.laserTrail);
		E_LaserGreen.SetDamage(1.0f);
		E_LaserGreen.SetRapidVars(4, 0.15f, 0.75f);
		E_LaserGreen.SetBuff(Buff.Type.Acid, 100, 1.0f, 5.0f);
		E_LaserGreen.SetPiercing();

		E_LaserBlue = holderObject.AddComponent<Weapon>();
		E_LaserBlue.SetStandardVars(3.0f, 500.0f, 501.0f, 0.05f, false);
		E_LaserBlue.SetGraphicVars(WeaponGraphic.Image.StandardBlue, new Color(0.0f, 0.0f, 0.8f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f), EffectController.Singleton.laserTrail);
		E_LaserBlue.SetDamage(1.0f);
		E_LaserBlue.SetRapidVars(4, 0.15f, 0.75f);
		E_LaserBlue.SetBuff(Buff.Type.Stun);
		E_LaserBlue.SetPiercing();

		E_LaserRed = holderObject.AddComponent<Weapon>();
		E_LaserRed.SetStandardVars(3.0f, 500.0f, 501.0f, 0.05f, false);
		E_LaserRed.SetGraphicVars(WeaponGraphic.Image.StandardRed, new Color(0.8f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.laserTrail);
		E_LaserRed.SetDamage(0.45f);
		E_LaserRed.SetRapidVars(4, 0.15f, 0.75f);
		E_LaserRed.SetBuff(Buff.Type.Burn);
		E_LaserRed.SetPiercing();

		E_LaserPink = holderObject.AddComponent<Weapon>();
		E_LaserPink.SetStandardVars(3.0f, 500.0f, 501.0f, 0.05f, false);
		E_LaserPink.SetGraphicVars(WeaponGraphic.Image.StandardPink, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(0.7f, 0.3f, 0.7f, 1.0f), EffectController.Singleton.laserTrail);
		E_LaserPink.SetDamage(1.5f);
		E_LaserPink.SetRapidVars(4, 0.15f, 0.75f);
		E_LaserPink.SetPiercing();

		E_LaserCircleGreen = holderObject.AddComponent<Weapon>();
		E_LaserCircleGreen.SetStandardVars(5.0f, 271.0f, 270.0f, 0.05f, false);
		E_LaserCircleGreen.SetGraphicVars(WeaponGraphic.Image.StandardGreen, new Color(0.0f, 0.8f, 0.2f, 1.0f), new Color(0.1f, 0.8f, 0.1f, 1.0f));
		E_LaserCircleGreen.SetDamage(1.0f);
		E_LaserCircleGreen.SetRapidVars(64, 0.065f, 0.0f, KJMath.CIRCLE * 3 );

		E_LaserCirclePink = holderObject.AddComponent<Weapon>();
		E_LaserCirclePink.SetStandardVars(5.0f, 301.0f, 300.0f, 0.05f, false);
		E_LaserCirclePink.SetGraphicVars(WeaponGraphic.Image.StandardPink, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(0.7f, 0.3f, 0.7f, 1.0f));
		E_LaserCirclePink.SetDamage(1.0f);
		E_LaserCirclePink.SetRapidVars(64, 0.065f, 0.0f, KJMath.CIRCLE * 3 );

		E_LaserCircleBlue = holderObject.AddComponent<Weapon>();
		E_LaserCircleBlue.SetStandardVars(5.0f, 301.0f, 300.0f, 0.05f, false);
		E_LaserCircleBlue.SetGraphicVars(WeaponGraphic.Image.StandardBlue, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(0.7f, 0.3f, 0.7f, 1.0f));
		E_LaserCircleBlue.SetDamage(1.0f);
		E_LaserCircleBlue.SetRapidVars(64, 0.065f, 0.0f, KJMath.CIRCLE * 3 );
		E_LaserCircleBlue.SetBuff(Buff.Type.Stun);

		E_LaserCircleRed = holderObject.AddComponent<Weapon>();
		E_LaserCircleRed.SetStandardVars(5.0f, 271.0f, 270.0f, 0.05f, false);
		E_LaserCircleRed.SetGraphicVars(WeaponGraphic.Image.StandardRed, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(0.7f, 0.3f, 0.7f, 1.0f));
		E_LaserCircleRed.SetDamage(0.45f);
		E_LaserCircleRed.SetRapidVars(64, 0.065f, 0.0f, KJMath.CIRCLE * 3 );
		E_LaserCircleRed.SetBuff(Buff.Type.Burn);


		E_ChainerBlue2 = holderObject.AddComponent<Weapon>();
		E_ChainerBlue2.SetStandardVars(3.0f, 700.0f, 320.0f, 0.2f);
		E_ChainerBlue2.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(0.2f, 0.7f, 1.0f), new Color (0.3f, 0.0f, 0.5f, 1.0f), EffectController.Singleton.shockTrail);
		E_ChainerBlue2.SetChainerVars(2);
		E_ChainerBlue2.SetHomingVars();
		E_ChainerBlue2.SetEjectionVars();
		E_ChainerBlue2.SetScatterVars(2, 0.0f);
		E_ChainerBlue2.SetDamage(0.85f);
		E_ChainerBlue2.SetBuff(Buff.Type.Stun, 100);

		E_ChainerRed = holderObject.AddComponent<Weapon>();
		E_ChainerRed.SetStandardVars(3.0f, 700.0f, 300.0f, 0.2f);
		E_ChainerRed.SetGraphicVars(WeaponGraphic.Image.ElectroRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.shockTrail);
		E_ChainerRed.SetChainerVars(3);
		E_ChainerRed.SetHomingVars();
		E_ChainerRed.SetDamage(0.45f);
		E_ChainerRed.SetBuff(Buff.Type.Burn);

		E_ChainerPurple = holderObject.AddComponent<Weapon>();
		E_ChainerPurple.SetStandardVars(3.0f, 700.0f, 300.0f, 0.2f);
		E_ChainerPurple.SetGraphicVars(WeaponGraphic.Image.ElectroPink, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(1.0f, 0.3f, 1.0f, 1.0f), EffectController.Singleton.shockTrail);
		E_ChainerPurple.SetChainerVars(3);
		E_ChainerPurple.SetHomingVars();
		E_ChainerPurple.SetDamage(1.0f);

		E_ChainerRed2 = holderObject.AddComponent<Weapon>();
		E_ChainerRed2.SetStandardVars(3.0f, 700.0f, 275.0f, 0.2f);
		E_ChainerRed2.SetGraphicVars(WeaponGraphic.Image.ElectroRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.shockTrail);
		E_ChainerRed2.SetChainerVars(2);
		E_ChainerRed2.SetHomingVars();
		E_ChainerRed2.SetEjectionVars();
		E_ChainerRed2.SetScatterVars(2, 0.0f);
		E_ChainerRed2.SetDamage(0.45f);
		E_ChainerRed2.SetBuff(Buff.Type.Burn);

		E_ChainerPurple2 = holderObject.AddComponent<Weapon>();
		E_ChainerPurple2.SetStandardVars(3.0f, 700.0f, 275.0f, 0.2f);
		E_ChainerPurple2.SetGraphicVars(WeaponGraphic.Image.ElectroPink, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(1.0f, 0.3f, 1.0f, 1.0f), EffectController.Singleton.shockTrail);
		E_ChainerPurple2.SetChainerVars(2);
		E_ChainerPurple2.SetHomingVars();
		E_ChainerPurple2.SetEjectionVars();
		E_ChainerPurple2.SetScatterVars(2, 0.0f);
		E_ChainerPurple2.SetDamage(1.0f);

		E_SonarRed = holderObject.AddComponent<Weapon>();
		E_SonarRed.SetStandardVars(1.5f, 0.0f, 1500.0f, 0.005f);
		E_SonarRed.SetGraphicVars(WeaponGraphic.Image.ElectroRed, new Color(1.0f, 0.2f, 0.0f, 1.0f), new Color(1.0f, 0.2f, 0.0f, 0.1f), EffectController.Singleton.sonarTrail);
		E_SonarRed.SetPiercing();
		E_SonarRed.SetHomingVars();
		E_SonarRed.SetBuff(Buff.Type.Burn);
		E_SonarRed.SetDamage(0.45f);

		E_SonarBlue = holderObject.AddComponent<Weapon>();
		E_SonarBlue.SetStandardVars(1.5f, 0.0f, 1500.0f, 0.005f);
		E_SonarBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(0.0f, 0.2f, 1.0f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 0.1f), EffectController.Singleton.sonarTrail);
		E_SonarBlue.SetPiercing();
		E_SonarBlue.SetHomingVars();
		E_SonarBlue.SetBuff(Buff.Type.Stun);
		E_SonarBlue.SetDamage(1.0f);

		E_SonarPink = holderObject.AddComponent<Weapon>();
		E_SonarPink.SetStandardVars(1.5f, 0.0f, 1500.0f, 0.005f);
		E_SonarPink.SetGraphicVars(WeaponGraphic.Image.ElectroPink, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(1.0f, 0.3f, 1.0f, 0.1f), EffectController.Singleton.sonarTrail);
		E_SonarPink.SetPiercing();
		E_SonarPink.SetHomingVars();
		E_SonarPink.SetDamage(1.0f);

		E_SonarGreen = holderObject.AddComponent<Weapon>();
		E_SonarGreen.SetStandardVars(1.5f, 0.0f, 1500.0f, 0.005f);
		E_SonarGreen.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.1f), EffectController.Singleton.sonarTrail);
		E_SonarGreen.SetPiercing();
		E_SonarGreen.SetHomingVars();
		E_SonarGreen.SetBuff(Buff.Type.Acid);
		E_SonarGreen.SetDamage(0.5f);

		E_DoubleSonarGreen = holderObject.AddComponent<Weapon>();
		E_DoubleSonarGreen.SetStandardVars(2f, 0.0f, 1200.0f, 0.01f, false);
		E_DoubleSonarGreen.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.1f), EffectController.Singleton.sonarTrail);
		E_DoubleSonarGreen.SetPiercing();
		E_DoubleSonarGreen.SetHomingVars();
		E_DoubleSonarGreen.SetRapidVars(2, 0.35f, 0, 0);
		E_DoubleSonarGreen.SetBuff(Buff.Type.Acid);
		E_DoubleSonarGreen.SetDamage(0.35f);

		E_DoubleSonarBlue = holderObject.AddComponent<Weapon>();
		E_DoubleSonarBlue.SetStandardVars(2f, 0.0f, 1200.0f, 0.01f, false);
		E_DoubleSonarBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(0.0f, 0.2f, 1.0f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 0.1f), EffectController.Singleton.sonarTrail);
		E_DoubleSonarBlue.SetPiercing();
		E_DoubleSonarBlue.SetHomingVars();
		E_DoubleSonarBlue.SetRapidVars(2, 0.35f, 0, 0);
		E_DoubleSonarBlue.SetBuff(Buff.Type.Stun);
		E_DoubleSonarBlue.SetDamage(0.35f);

		E_DoubleSonarRed = holderObject.AddComponent<Weapon>();
		E_DoubleSonarRed.SetStandardVars(2f, 0.0f, 1200.0f, 0.01f, false);
		E_DoubleSonarRed.SetGraphicVars(WeaponGraphic.Image.ElectroRed, new Color(1.0f, 0.2f, 0.0f, 1.0f), new Color(1.0f, 0.2f, 0.0f, 0.1f), EffectController.Singleton.sonarTrail);
		E_DoubleSonarRed.SetPiercing();
		E_DoubleSonarRed.SetHomingVars();
		E_DoubleSonarRed.SetRapidVars(2, 0.35f, 0, 0);
		E_DoubleSonarRed.SetBuff(Buff.Type.Burn);
		E_DoubleSonarRed.SetDamage(0.25f);

		E_DoubleSonarPink = holderObject.AddComponent<Weapon>();
		E_DoubleSonarPink.SetStandardVars(2f, 0.0f, 1200.0f, 0.01f, false);
		E_DoubleSonarPink.SetGraphicVars(WeaponGraphic.Image.ElectroPink, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(1.0f, 0.3f, 1.0f, 0.1f), EffectController.Singleton.sonarTrail);
		E_DoubleSonarPink.SetPiercing();
		E_DoubleSonarPink.SetHomingVars();
		E_DoubleSonarPink.SetRapidVars(2, 0.35f, 0, 0);
		E_DoubleSonarPink.SetDamage(1.35f);

		E_ScatterSlowGreen = holderObject.AddComponent<Weapon>();
		E_ScatterSlowGreen.SetStandardVars(4f, 100.0f, 50.0f, 0.01f, false);
		E_ScatterSlowGreen.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, greenStart);
		E_ScatterSlowGreen.SetRapidVars(8, 0.05f, 0, 2.6f);
		E_ScatterSlowGreen.SetBuff(Buff.Type.Acid);
		E_ScatterSlowGreen.SetTime(5);
		E_ScatterSlowGreen.SetDamage(1.0f);

		E_ScatterSlowBlue = holderObject.AddComponent<Weapon>();
		E_ScatterSlowBlue.SetStandardVars(4f, 100.0f, 50.0f, 0.01f, false);
		E_ScatterSlowBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, blueStart);
		E_ScatterSlowBlue.SetRapidVars(8, 0.05f, 0, 2.6f);
		E_ScatterSlowBlue.SetBuff(Buff.Type.Stun);
		E_ScatterSlowBlue.SetTime(5);
		E_ScatterSlowBlue.SetDamage(1.0f);

		E_ScatterSlowRed = holderObject.AddComponent<Weapon>();
		E_ScatterSlowRed.SetStandardVars(4f, 100.0f, 50.0f, 0.01f, false);
		E_ScatterSlowRed.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, redStart);
		E_ScatterSlowRed.SetRapidVars(8, 0.05f, 0, 2.6f);
		E_ScatterSlowRed.SetBuff(Buff.Type.Burn);
		E_ScatterSlowRed.SetTime(5);
		E_ScatterSlowRed.SetDamage(1.0f);

		E_ScatterSlowPink = holderObject.AddComponent<Weapon>();
		E_ScatterSlowPink.SetStandardVars(4f, 100.0f, 50.0f, 0.01f, false);
		E_ScatterSlowPink.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, pinkStart);
		E_ScatterSlowPink.SetRapidVars(8, 0.05f, 0, 2.6f);
		E_ScatterSlowPink.SetDamage(2.0f);

		E_BoomerangYellow = holderObject.AddComponent<Weapon>();
		E_BoomerangYellow.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangYellow.SetDamage(0.8f);
		E_BoomerangYellow.SetGraphicVars(WeaponGraphic.Image.BoomerYellow, yellowStart, Color.white);
		E_BoomerangYellow.SetTime(6.0f);
		E_BoomerangYellow.SetPiercing();
		E_BoomerangYellow.SetBoomerang(1.5f, 0.4f);
		E_BoomerangYellow.SetDoubleHitRadius();
		
		E_BoomerangGreen = holderObject.AddComponent<Weapon>();
		E_BoomerangGreen.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangGreen.SetDamage(0.8f);
		E_BoomerangGreen.SetGraphicVars(WeaponGraphic.Image.BoomerGreen, greenStart, Color.white);
		E_BoomerangGreen.SetTime(6.0f);
		E_BoomerangGreen.SetBuff(Buff.Type.Acid);
		E_BoomerangGreen.SetPiercing();
		E_BoomerangGreen.SetBoomerang(1.5f, 0.4f);
		E_BoomerangGreen.SetDoubleHitRadius();
		
		E_BoomerangBlue = holderObject.AddComponent<Weapon>();
		E_BoomerangBlue.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangBlue.SetDamage(0.8f);
		E_BoomerangBlue.SetGraphicVars(WeaponGraphic.Image.BoomerBlue, blueStart, Color.white);
		E_BoomerangBlue.SetTime(6.0f);
		E_BoomerangBlue.SetBuff(Buff.Type.Stun);
		E_BoomerangBlue.SetPiercing();
		E_BoomerangBlue.SetBoomerang(1.5f, 0.4f);
		E_BoomerangBlue.SetDoubleHitRadius();
		
		E_BoomerangRed = holderObject.AddComponent<Weapon>();
		E_BoomerangRed.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangRed.SetDamage(0.2f);
		E_BoomerangRed.SetGraphicVars(WeaponGraphic.Image.BoomerRed, redStart, Color.white);
		E_BoomerangRed.SetTime(6.0f);
		E_BoomerangRed.SetBuff(Buff.Type.Burn);
		E_BoomerangRed.SetPiercing();
		E_BoomerangRed.SetBoomerang(1.5f, 0.4f);
		E_BoomerangRed.SetDoubleHitRadius();
		
		E_BoomerangPink = holderObject.AddComponent<Weapon>();
		E_BoomerangPink.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangPink.SetDamage(1.0f);
		E_BoomerangPink.SetGraphicVars(WeaponGraphic.Image.BoomerPink, pinkStart, Color.white);
		E_BoomerangPink.SetTime(6.0f);
		E_BoomerangPink.SetIgnoreArmor();
		E_BoomerangPink.SetBoomerang(1.5f, 0.4f);
		E_BoomerangPink.SetDoubleHitRadius();
		
		E_BoomerangYellow2 = holderObject.AddComponent<Weapon>();
		E_BoomerangYellow2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangYellow2.SetDamage(0.8f);
		E_BoomerangYellow2.SetGraphicVars(WeaponGraphic.Image.BoomerYellow, yellowStart, Color.white);
		E_BoomerangYellow2.SetRapidVars(2 , 1, 0.5f, 0);
		E_BoomerangYellow2.SetTime(6.0f);
		E_BoomerangYellow2.SetPiercing();
		E_BoomerangYellow2.SetBoomerang(1.5f, 0.4f);
		E_BoomerangYellow2.SetDoubleHitRadius();
		
		E_BoomerangGreen2 = holderObject.AddComponent<Weapon>();
		E_BoomerangGreen2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangGreen2.SetDamage(0.8f);
		E_BoomerangGreen2.SetGraphicVars(WeaponGraphic.Image.BoomerGreen, greenStart, Color.white);
		E_BoomerangGreen2.SetRapidVars(2 , 1, 0.5f, 0);
		E_BoomerangGreen2.SetTime(6.0f);
		E_BoomerangGreen2.SetScatterVars(2, KJMath.CIRCLE * 0.2f);
		E_BoomerangGreen2.SetBuff(Buff.Type.Acid);
		E_BoomerangGreen2.SetPiercing();
		E_BoomerangGreen2.SetBoomerang(1.5f, 0.4f);
		E_BoomerangGreen2.SetDoubleHitRadius();
		
		E_BoomerangBlue2 = holderObject.AddComponent<Weapon>();
		E_BoomerangBlue2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangBlue2.SetDamage(0.8f);
		E_BoomerangBlue2.SetGraphicVars(WeaponGraphic.Image.BoomerBlue, blueStart, Color.white);
		E_BoomerangBlue2.SetRapidVars(2 , 1, 0.5f, 0);
		E_BoomerangBlue2.SetTime(6.0f);
		E_BoomerangBlue2.SetScatterVars(2, KJMath.CIRCLE * 0.2f);
		E_BoomerangBlue2.SetBuff(Buff.Type.Stun);
		E_BoomerangBlue2.SetPiercing();
		E_BoomerangBlue2.SetBoomerang(1.5f, 0.4f);
		E_BoomerangBlue2.SetDoubleHitRadius();
		
		E_BoomerangRed2 = holderObject.AddComponent<Weapon>();
		E_BoomerangRed2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangRed2.SetDamage(0.2f);
		E_BoomerangRed2.SetGraphicVars(WeaponGraphic.Image.BoomerRed, redStart, Color.white);
		E_BoomerangRed2.SetRapidVars(2 , 1, 0.5f, 0);
		E_BoomerangRed2.SetTime(6.0f);
		E_BoomerangRed2.SetBuff(Buff.Type.Burn);
		E_BoomerangRed2.SetPiercing();
		E_BoomerangRed2.SetBoomerang(1.5f, 0.4f);
		E_BoomerangRed2.SetDoubleHitRadius();
		
		E_BoomerangPink2 = holderObject.AddComponent<Weapon>();
		E_BoomerangPink2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_BoomerangPink2.SetDamage(1.0f);
		E_BoomerangPink2.SetGraphicVars(WeaponGraphic.Image.BoomerPink, pinkStart, Color.white);
		E_BoomerangPink2.SetRapidVars(2 , 1, 0.5f, 0);
		E_BoomerangPink2.SetTime(6.0f);
		E_BoomerangPink2.SetIgnoreArmor();
		E_BoomerangPink2.SetBoomerang(1.5f, 0.4f);
		E_BoomerangPink2.SetDoubleHitRadius();
		
		E_SwirlYellow = holderObject.AddComponent<Weapon>();
		E_SwirlYellow.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlYellow.SetDamage(0.8f);
		E_SwirlYellow.SetGraphicVars(WeaponGraphic.Image.SwirlYellow, yellowStart, Color.white);
		E_SwirlYellow.SetTime(6.0f);
		E_SwirlYellow.SetPiercing();
		E_SwirlYellow.SetSwirl(0.5f, 0.4f);
		E_SwirlYellow.SetDoubleHitRadius();
		
		E_SwirlGreen = holderObject.AddComponent<Weapon>();
		E_SwirlGreen.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlGreen.SetDamage(0.8f);
		E_SwirlGreen.SetGraphicVars(WeaponGraphic.Image.SwirlGreen, greenStart, Color.white);
		E_SwirlGreen.SetTime(6.0f);
		E_SwirlGreen.SetBuff(Buff.Type.Acid);
		E_SwirlGreen.SetPiercing();
		E_SwirlGreen.SetSwirl(0.5f, 0.4f);
		E_SwirlGreen.SetDoubleHitRadius();
		
		E_SwirlBlue = holderObject.AddComponent<Weapon>();
		E_SwirlBlue.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlBlue.SetDamage(0.8f);
		E_SwirlBlue.SetGraphicVars(WeaponGraphic.Image.SwirlBlue, blueStart, Color.white);
		E_SwirlBlue.SetTime(6.0f);
		E_SwirlBlue.SetBuff(Buff.Type.Stun);
		E_SwirlBlue.SetPiercing();
		E_SwirlBlue.SetSwirl(0.5f, 0.4f);
		E_SwirlBlue.SetDoubleHitRadius();
		
		E_SwirlRed = holderObject.AddComponent<Weapon>();
		E_SwirlRed.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlRed.SetDamage(0.2f);
		E_SwirlRed.SetGraphicVars(WeaponGraphic.Image.SwirlRed, redStart, Color.white);
		E_SwirlRed.SetTime(6.0f);
		E_SwirlRed.SetBuff(Buff.Type.Burn);
		E_SwirlRed.SetPiercing();
		E_SwirlRed.SetSwirl(0.5f, 0.4f);
		E_SwirlRed.SetDoubleHitRadius();
		
		E_SwirlPink = holderObject.AddComponent<Weapon>();
		E_SwirlPink.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlPink.SetDamage(1.0f);
		E_SwirlPink.SetGraphicVars(WeaponGraphic.Image.SwirlPink, pinkStart, Color.white);
		E_SwirlPink.SetTime(6.0f);
		E_SwirlPink.SetIgnoreArmor();
		E_SwirlPink.SetSwirl(0.5f, 0.4f);
		E_SwirlPink.SetDoubleHitRadius();
		
		E_SwirlYellow2 = holderObject.AddComponent<Weapon>();
		E_SwirlYellow2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlYellow2.SetDamage(0.8f);
		E_SwirlYellow2.SetGraphicVars(WeaponGraphic.Image.SwirlYellow, yellowStart, Color.white);
		E_SwirlYellow2.SetTime(6.0f);
		E_SwirlYellow2.SetRapidVars(2 , 1, 1.5f, 0);
		E_SwirlYellow2.SetPiercing();
		E_SwirlYellow2.SetSwirl(0.5f, 0.4f);
		E_SwirlYellow2.SetDoubleHitRadius();
		
		E_SwirlGreen2 = holderObject.AddComponent<Weapon>();
		E_SwirlGreen2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlGreen2.SetDamage(0.8f);
		E_SwirlGreen2.SetGraphicVars(WeaponGraphic.Image.SwirlGreen, greenStart, Color.white);
		E_SwirlGreen2.SetTime(6.0f);
		E_SwirlGreen2.SetRapidVars(2 , 1, 1.5f, 0);
		E_SwirlGreen2.SetBuff(Buff.Type.Acid);
		E_SwirlGreen2.SetPiercing();
		E_SwirlGreen2.SetSwirl(0.5f, 0.4f);
		E_SwirlGreen2.SetDoubleHitRadius();
		
		E_SwirlBlue2 = holderObject.AddComponent<Weapon>();
		E_SwirlBlue2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlBlue2.SetDamage(0.8f);
		E_SwirlBlue2.SetGraphicVars(WeaponGraphic.Image.SwirlBlue, blueStart, Color.white);
		E_SwirlBlue2.SetTime(6.0f);
		E_SwirlBlue2.SetRapidVars(2 , 1, 1.5f, 0);
		E_SwirlBlue2.SetBuff(Buff.Type.Stun);
		E_SwirlBlue2.SetPiercing();
		E_SwirlBlue2.SetSwirl(0.5f, 0.4f);
		E_SwirlBlue2.SetDoubleHitRadius();
		
		E_SwirlRed2 = holderObject.AddComponent<Weapon>();
		E_SwirlRed2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlRed2.SetDamage(0.2f);
		E_SwirlRed2.SetGraphicVars(WeaponGraphic.Image.SwirlRed, redStart, Color.white);
		E_SwirlRed2.SetTime(6.0f);
		E_SwirlRed2.SetRapidVars(2 , 1, 1.5f, 0);
		E_SwirlRed2.SetBuff(Buff.Type.Burn);
		E_SwirlRed2.SetPiercing();
		E_SwirlRed2.SetSwirl(0.5f, 0.4f);
		E_SwirlRed2.SetDoubleHitRadius();
		
		E_SwirlPink2 = holderObject.AddComponent<Weapon>();
		E_SwirlPink2.SetStandardVars(4.5f, 0.0f, 300.0f, 0.03f);
		E_SwirlPink2.SetDamage(1.0f);
		E_SwirlPink2.SetGraphicVars(WeaponGraphic.Image.SwirlPink, pinkStart, Color.white);
		E_SwirlPink2.SetTime(6.0f);
		E_SwirlPink2.SetRapidVars(2 , 1, 1.5f, 0);
		E_SwirlPink2.SetIgnoreArmor();
		E_SwirlPink2.SetSwirl(0.5f, 0.4f);
		E_SwirlPink2.SetDoubleHitRadius();

		E_Engage_Y = holderObject.AddComponent<Weapon>();
		E_Engage_Y.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		E_Engage_Y.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, yellowStart, new Color(1.0f, 0.4f, 0.1f, 0.8f), EffectController.Singleton.engageTrail);
		E_Engage_Y.SetHomingVars();
		E_Engage_Y.SetDamage(1.0f);
		E_Engage_Y.SetEngage(6);

		E_Engage_G = holderObject.AddComponent<Weapon>();
		E_Engage_G.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		E_Engage_G.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.8f), EffectController.Singleton.engageTrail);
		E_Engage_G.SetHomingVars();
		E_Engage_G.SetDamage(0.75f);
		E_Engage_G.SetEngage(6);
		E_Engage_G.SetBuff(Buff.Type.Acid);

		E_Engage_B = holderObject.AddComponent<Weapon>();
		E_Engage_B.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		E_Engage_B.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.3f, 1f, 0.8f), EffectController.Singleton.engageTrail);
		E_Engage_B.SetHomingVars();
		E_Engage_B.SetDamage(0.8f);
		E_Engage_B.SetEngage(6);
		E_Engage_B.SetBuff(Buff.Type.Stun);

		E_Engage_R = holderObject.AddComponent<Weapon>();
		E_Engage_R.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		E_Engage_R.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.8f), EffectController.Singleton.engageTrail);
		E_Engage_R.SetHomingVars();
		E_Engage_R.SetDamage(0.2f);
		E_Engage_R.SetEngage(6);
		E_Engage_R.SetBuff(Buff.Type.Burn);

		E_Engage_P = holderObject.AddComponent<Weapon>();
		E_Engage_P.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		E_Engage_P.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color(1.0f, 0.4f, 0.9f, 0.8f), EffectController.Singleton.engageTrail);
		E_Engage_P.SetHomingVars();
		E_Engage_P.SetDamage(1.4f);
		E_Engage_P.SetEngage(6);

		B_Engage_Y = holderObject.AddComponent<Weapon>();
		B_Engage_Y.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_Y.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, yellowStart, new Color(1.0f, 0.4f, 0.1f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_Y.SetHomingVars();
		B_Engage_Y.SetDamage(1.0f);
		B_Engage_Y.SetEngage(6);
		
		B_Engage_G = holderObject.AddComponent<Weapon>();
		B_Engage_G.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_G.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_G.SetHomingVars();
		B_Engage_G.SetDamage(0.75f);
		B_Engage_G.SetEngage(6);
		B_Engage_G.SetBuff(Buff.Type.Acid);
		
		B_Engage_B = holderObject.AddComponent<Weapon>();
		B_Engage_B.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_B.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.3f, 1f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_B.SetHomingVars();
		B_Engage_B.SetDamage(0.8f);
		B_Engage_B.SetEngage(6);
		B_Engage_B.SetBuff(Buff.Type.Stun);
		
		B_Engage_R = holderObject.AddComponent<Weapon>();
		B_Engage_R.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_R.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_R.SetHomingVars();
		B_Engage_R.SetDamage(0.2f);
		B_Engage_R.SetEngage(6);
		B_Engage_R.SetBuff(Buff.Type.Burn);
		
		B_Engage_P = holderObject.AddComponent<Weapon>();
		B_Engage_P.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_P.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color(1.0f, 0.4f, 0.9f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_P.SetHomingVars();
		B_Engage_P.SetDamage(1.4f);
		B_Engage_P.SetEngage(6);

		B_Engage_Y2 = holderObject.AddComponent<Weapon>();
		B_Engage_Y2.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_Y2.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, yellowStart, new Color(1.0f, 0.4f, 0.1f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_Y2.SetHomingVars();
		B_Engage_Y2.SetDamage(1.0f);
		B_Engage_Y2.SetEngage(11);
		
		B_Engage_G2 = holderObject.AddComponent<Weapon>();
		B_Engage_G2.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_G2.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.4f, 1.0f, 0.1f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_G2.SetHomingVars();
		B_Engage_G2.SetDamage(0.75f);
		B_Engage_G2.SetEngage(11);
		B_Engage_G2.SetBuff(Buff.Type.Acid);
		
		B_Engage_B2 = holderObject.AddComponent<Weapon>();
		B_Engage_B2.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_B2.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.3f, 1f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_B2.SetHomingVars();
		B_Engage_B2.SetDamage(0.8f);
		B_Engage_B2.SetEngage(11);
		B_Engage_B2.SetBuff(Buff.Type.Stun);
		
		B_Engage_R2 = holderObject.AddComponent<Weapon>();
		B_Engage_R2.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_R2.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_R2.SetHomingVars();
		B_Engage_R2.SetDamage(0.2f);
		B_Engage_R2.SetEngage(11);
		B_Engage_R2.SetBuff(Buff.Type.Burn);
		
		B_Engage_P2 = holderObject.AddComponent<Weapon>();
		B_Engage_P2.SetStandardVars(4f, 250.0f, 135.0f, 0.2f);
		B_Engage_P2.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color(1.0f, 0.4f, 0.9f, 0.8f), EffectController.Singleton.engageTrail);
		B_Engage_P2.SetHomingVars();
		B_Engage_P2.SetDamage(1.4f);
		B_Engage_P2.SetEngage(11);

		//Module
		
		M_CounterScatter = holderObject.AddComponent<Weapon>();
		M_CounterScatter.SetStandardVars(2.5f, 2400.0f, 0.0f, 0.2f);
		M_CounterScatter.SetDamage(0.0f);
		M_CounterScatter.SetGraphicVars(WeaponGraphic.Image.RoundRed, new Color(1.0f, 0.3f, 0.2f), Color.red, EffectController.Singleton.missileTrail);
		M_CounterScatter.SetScatterVars(5, KJMath.CIRCLE * 0.35f);
		
		M_CounterMissile_4 = holderObject.AddComponent<Weapon>(); 
		M_CounterMissile_4.SetStandardVars(2.0f, 0.0f, 400.0f, 0.50f);
		M_CounterMissile_4.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(0.75f, 0.0f, 1.0f, 1.0f), new Color(0.75f, 0.0f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		M_CounterMissile_4.SetHomingVars();
		M_CounterMissile_4.SetEjectionVars();
		M_CounterMissile_4.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		M_CounterMissile_4.SetDamage(1.25f);
		M_CounterMissile_4.SetScatterVars(4, 0.0f);
		//M_CounterMissile_4.SetRapidVars(2, 0.25f, 0.0f);


		M_GreenLaser = holderObject.AddComponent<Weapon>();
		M_GreenLaser.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		M_GreenLaser.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, new Color(0.25f, 1.0f, 0.1f, 1.0f), new Color(0.0f, 0.2f, 0.8f, 1.0f), EffectController.Singleton.lineTrail2);
		M_GreenLaser.SetRapidVars(3, 0.25f, 0.10f);
		M_GreenLaser.SetDamage(1.0f);
		M_GreenLaser.SetBuff(Buff.Type.Acid);
		M_GreenLaser.SetPiercing();

		M_RedLaser = holderObject.AddComponent<Weapon>();
		M_RedLaser.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		M_RedLaser.SetGraphicVars(WeaponGraphic.Image.SpeedRed, new Color(1.0f, 0.25f, 0.1f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.lineTrail2);
		M_RedLaser.SetRapidVars(3, 0.25f, 0.1f);
		M_RedLaser.SetDamage(1.0f);
		M_RedLaser.SetBuff(Buff.Type.Burn);
		M_RedLaser.SetPiercing();

		M_BlueLaser = holderObject.AddComponent<Weapon>();
		M_BlueLaser.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		M_BlueLaser.SetGraphicVars(WeaponGraphic.Image.SpeedBlue, new Color(0.1f, 0.3f, 0.7f, 1.0f), new Color(0.2f, 0.1f, 0.7f, 1.0f), EffectController.Singleton.lineTrail2);
		M_BlueLaser.SetRapidVars(3, 0.25f, 0.1f);
		M_BlueLaser.SetDamage(1.0f);
		M_BlueLaser.SetBuff(Buff.Type.Stun);
		M_BlueLaser.SetPiercing();

		M_PurpleLaser = holderObject.AddComponent<Weapon>();
		M_PurpleLaser.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		M_PurpleLaser.SetGraphicVars(WeaponGraphic.Image.SpeedPink, new Color(1.0f, 0.3f, 0.7f, 1.0f), new Color(0.2f, 0.1f, 0.7f, 1.0f), EffectController.Singleton.lineTrail2);
		M_PurpleLaser.SetRapidVars(3, 0.25f, 0.1f);
		M_PurpleLaser.SetDamage(1.0f);
		M_PurpleLaser.SetPiercing();

		M_GreenLaserScatter = holderObject.AddComponent<Weapon>();
		M_GreenLaserScatter.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		M_GreenLaserScatter.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, new Color(0.25f, 1.0f, 0.1f, 1.0f), new Color(0.0f, 0.2f, 0.8f, 1.0f), EffectController.Singleton.lineTrail2);
		M_GreenLaserScatter.SetRapidVars(5, 0.12f, 0.0f, 1.2f);
		M_GreenLaserScatter.SetDamage(1.0f);
//		M_GreenLaserScatter.SetScatterVars(3, 1.0f);
		M_GreenLaserScatter.SetBuff(Buff.Type.Acid);
		M_GreenLaserScatter.SetPiercing();

		M_RedLaserScatter = holderObject.AddComponent<Weapon>();
		M_RedLaserScatter.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		M_RedLaserScatter.SetGraphicVars(WeaponGraphic.Image.SpeedRed, new Color(1.0f, 0.25f, 0.1f, 1.0f), new Color(1.0f, 0.0f, 0.0f, 1.0f), EffectController.Singleton.lineTrail2);
		M_RedLaserScatter.SetRapidVars(5, 0.12f, 0.0f, 1.2f);
		M_RedLaserScatter.SetDamage(1.0f);;
//		M_RedLaserScatter.SetScatterVars(3, 1.0f);
		M_RedLaserScatter.SetBuff(Buff.Type.Burn);
		M_RedLaserScatter.SetPiercing();

		M_BlueLaserScatter = holderObject.AddComponent<Weapon>();
		M_BlueLaserScatter.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		M_BlueLaserScatter.SetGraphicVars(WeaponGraphic.Image.SpeedBlue, new Color(0.1f, 0.3f, 0.7f, 1.0f), new Color(0.2f, 0.1f, 0.7f, 1.0f), EffectController.Singleton.lineTrail2);
		M_BlueLaserScatter.SetRapidVars(5, 0.12f, 0.0f, 1.2f);
		M_BlueLaserScatter.SetDamage(1.0f);
//		M_BlueLaserScatter.SetScatterVars(3, 1.0f);
		M_BlueLaserScatter.SetBuff(Buff.Type.Stun);
		M_BlueLaserScatter.SetPiercing();

		M_PurpleLaserScatter = holderObject.AddComponent<Weapon>();
		M_PurpleLaserScatter.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		M_PurpleLaserScatter.SetGraphicVars(WeaponGraphic.Image.SpeedPink, new Color(1.0f, 0.3f, 0.7f, 1.0f), new Color(0.2f, 0.1f, 0.7f, 1.0f), EffectController.Singleton.lineTrail2);
		M_PurpleLaserScatter.SetRapidVars(5, 0.12f, 0.0f, 1.2f);
		M_PurpleLaserScatter.SetDamage(1.0f);
//		M_PurpleLaserScatter.SetScatterVars(3, 1.0f);
		M_PurpleLaserScatter.SetPiercing();
		
		M_GreenMissile = holderObject.AddComponent<Weapon>(); 
		M_GreenMissile.SetStandardVars(1.5f, 0.0f, 500.0f, 0.50f);
		M_GreenMissile.SetGraphicVars(WeaponGraphic.Image.MissileGreen, new Color(0.2f, 0.8f, 0.2f, 1.0f), new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.missileTrail);
		M_GreenMissile.SetHomingVars();
		M_GreenMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		
		M_BlueMissile = holderObject.AddComponent<Weapon>(); 
		M_BlueMissile.SetStandardVars(1.3f, 0.0f, 600.0f, 0.50f);
		M_BlueMissile.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		M_BlueMissile.SetHomingVars();
		M_BlueMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		M_BlueMissile.SetBuff(Buff.Type.Stun);

		M_RedMissile = holderObject.AddComponent<Weapon>(); 
		M_RedMissile.SetStandardVars(1.1f, 0.0f, 650.0f, 0.50f);
		M_RedMissile.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.missileTrail);
		M_RedMissile.SetHomingVars();
		M_RedMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		M_RedMissile.SetBuff(Buff.Type.Burn);
		
		M_PurpleMissile = holderObject.AddComponent<Weapon>(); 
		M_PurpleMissile.SetStandardVars(0.8f, 0.0f, 700.0f, 0.50f);
		M_PurpleMissile.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(1.0f, 0.3f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		M_PurpleMissile.SetHomingVars();
		M_PurpleMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));


		M_GreenChain = holderObject.AddComponent<Weapon>();
		M_GreenChain.SetStandardVars(3.0f, 800.0f, 500.0f, 0.2f);
		M_GreenChain.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, new Color(0.2f, 0.8f, 0.2f, 1.0f), new Color(0.4f, 1.0f, 0.4f, 1.0f), EffectController.Singleton.lightShockTrail);
		M_GreenChain.SetChainerVars(2);
		M_GreenChain.SetHomingVars();
		M_GreenChain.SetDamage(1.5f);

		M_BlueChain = holderObject.AddComponent<Weapon>();
		M_BlueChain.SetStandardVars(2.75f, 800.0f, 500.0f, 0.2f);
		M_BlueChain.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(0.2f, 0.4f, 0.9f, 1.0f), new Color(0.0f, 0.2f, 1.0f, 1.0f), EffectController.Singleton.lightShockTrail);
		M_BlueChain.SetChainerVars(2);
		M_BlueChain.SetHomingVars();
		M_BlueChain.SetDamage(1.5f);
		M_BlueChain.SetBuff(Buff.Type.Stun);

		M_RedChain = holderObject.AddComponent<Weapon>();
		M_RedChain.SetStandardVars(2.50f, 800.0f, 500.0f, 0.2f);
		M_RedChain.SetGraphicVars(WeaponGraphic.Image.ElectroRed, new Color(1.0f, 0.0f, 0.0f, 1.0f), new Color(1.0f, 0.3f, 0.3f, 1.0f), EffectController.Singleton.lightShockTrail);
		M_RedChain.SetChainerVars(2);
		M_RedChain.SetHomingVars();
		M_RedChain.SetDamage(1.5f);
		M_RedChain.SetBuff(Buff.Type.Burn);

		M_PurpleChain = holderObject.AddComponent<Weapon>();
		M_PurpleChain.SetStandardVars(2.0f, 800.0f, 500.0f, 0.2f);
		M_PurpleChain.SetGraphicVars(WeaponGraphic.Image.ElectroPink, new Color(0.7f, 0.3f, 0.7f, 1.0f), new Color(1.0f, 0.3f, 1.0f, 1.0f), EffectController.Singleton.lightShockTrail);
		M_PurpleChain.SetChainerVars(2);
		M_PurpleChain.SetHomingVars();
		M_PurpleChain.SetDamage(1.5f);

		M_GreenScatter = holderObject.AddComponent<Weapon>();
		M_GreenScatter.SetStandardVars(3.0f, 501.0f, 500.0f, 0.2f);
		M_GreenScatter.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, Color.white);
		M_GreenScatter.SetScatterVars(6, KJMath.CIRCLE * 0.125f);
//		M_GreenScatter.SetRapidVars(1, 0.15f, 0.0f);
		M_GreenScatter.SetDamage(1.0f);

		M_BlueScatter = holderObject.AddComponent<Weapon>();
		M_BlueScatter.SetStandardVars(2.75f, 575.0f, 275.0f, 0.2f);
		M_BlueScatter.SetGraphicVars(WeaponGraphic.Image.SpeedBlue, blueStart, Color.white);
		M_BlueScatter.SetScatterVars(16, KJMath.CIRCLE * 0.45f);
		M_BlueScatter.SetRapidVars(1, 0.15f, 0.0f);
		M_BlueScatter.SetDamage(1.0f);
		M_BlueScatter.SetBuff(Buff.Type.Stun);

		M_RedScatter = holderObject.AddComponent<Weapon>();
		M_RedScatter.SetStandardVars(2.5f, 550.0f, 350.0f, 0.2f);
		M_RedScatter.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, Color.white);
		M_RedScatter.SetScatterVars(6, KJMath.CIRCLE * 0.125f);
		M_RedScatter.SetRapidVars(1, 0.15f, 0.0f);
		M_RedScatter.SetDamage(0.5f);
		M_RedScatter.SetBuff(Buff.Type.Burn);

		M_PurpleScatter = holderObject.AddComponent<Weapon>();
		M_PurpleScatter.SetStandardVars(2.25f, 471.0f, 470.0f, 0.2f);
		M_PurpleScatter.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, Color.white);
		M_PurpleScatter.SetScatterVars(6, KJMath.CIRCLE * 0.125f);
		M_PurpleScatter.SetRapidVars(1, 0.15f, 0.0f);
		M_PurpleScatter.SetDamage(1.0f);

		M_BlackHole = holderObject.AddComponent<Weapon>();
		M_BlackHole.SetStandardVars(1.0f, 300.0f, 200.0f, 0.2f);
		M_BlackHole.SetGraphicVars(WeaponGraphic.Image.BlackHoleBomb, new Color(1.0f, 0.5f, 1.0f, 1.0f), new Color(0.1f, 0.3f, 1.0f, 0.0f), EffectController.Singleton.sonarTrail);
		M_BlackHole.SetTime(1.20f);
		M_BlackHole.SetBlackHole();

		M_UltimateMissile = holderObject.AddComponent<Weapon>(); 
		M_UltimateMissile.SetStandardVars(2.0f, 0.0f, 650.0f, 0.50f);
		M_UltimateMissile.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(1.0f, 0.2f, 0.7f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f), EffectController.Singleton.missileTrail);
		M_UltimateMissile.SetHomingVars();
		M_UltimateMissile.SetExplosionVars(new Color(1.0f, 0.7f, 0.0f, 0.5f), new Color(1.0f, 0.0f, 0.0f, 0.15f));
		M_UltimateMissile.SetEjectionVars();
		M_UltimateMissile.SetScatterVars(3, 0.0f);
		M_UltimateMissile.SetRapidVars(1, 0.15f, 0.0f);

		M_UltimateScatter = holderObject.AddComponent<Weapon>();
		M_UltimateScatter.SetStandardVars(3.0f, 501.0f, 500.0f, 0.2f);
		M_UltimateScatter.SetGraphicVars(WeaponGraphic.Image.SpeedPink, new Color(1.0f, 0.2f, 0.2f), Color.white);
		M_UltimateScatter.SetScatterVars(6, KJMath.CIRCLE * 0.125f);
		M_UltimateScatter.SetRapidVars(2, 0.15f, 0.0f);

		M_UltimateChain = holderObject.AddComponent<Weapon>();
		M_UltimateChain.SetStandardVars(2.50f, 600.0f, 400.0f, 0.2f);
		M_UltimateChain.SetGraphicVars(WeaponGraphic.Image.ElectroPink, new Color(1.0f, 0.2f, 0.7f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f), EffectController.Singleton.shockTrail);
		M_UltimateChain.SetChainerVars(2);
		M_UltimateChain.SetHomingVars();
		M_UltimateChain.SetScatterVars(2, KJMath.CIRCLE * 0.45f);
		M_UltimateChain.SetRapidVars(1, 0.15f, 0.0f);

		M_UltimateLaser = holderObject.AddComponent<Weapon>();
		M_UltimateLaser.SetStandardVars(2.0f, 850.0f, 700.0f, 0.2f);
		M_UltimateLaser.SetGraphicVars(WeaponGraphic.Image.SpeedPink, new Color(1.0f, 0.2f, 0.7f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f), EffectController.Singleton.lineTrail2);
		M_UltimateLaser.SetRapidVars(5, 0.12f, 0.0f, 1.2f);
		//M_UltimateLaser.SetEjectionVars();
//		M_UltimateLaser.SetScatterVars(3, KJMath.CIRCLE * 0.15f);
		M_UltimateLaser.SetDamage(1.0f);
		M_UltimateLaser.SetPiercing();

		M_SonarGreen = holderObject.AddComponent<Weapon>();
		M_SonarGreen.SetStandardVars(2.00f, 250.0f, 350.0f, 0.2f, false);
		M_SonarGreen.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.4f, 1.0f, 1.0f, 0.10f), EffectController.Singleton.lightSonarTrail);
		M_SonarGreen.SetRapidVars(4, 0.1f, 0.0f, 0.9f);
		M_SonarGreen.SetPiercing();
		M_SonarGreen.SetDamage(1.0f);

		M_SonarBlue = holderObject.AddComponent<Weapon>();
		M_SonarBlue.SetStandardVars(2.00f, 300.0f, 400.0f, 0.2f, false);
		M_SonarBlue.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.0f, 0.3f, 1.0f, 0.10f), EffectController.Singleton.lightSonarTrail);
		M_SonarBlue.SetRapidVars(4, 0.1f, 0.0f, 0.9f);
		M_SonarBlue.SetPiercing();
		M_SonarBlue.SetBuff(Buff.Type.Stun);
		M_SonarBlue.SetDamage(1.0f);

		M_SonarRed = holderObject.AddComponent<Weapon>();
		M_SonarRed.SetStandardVars(2.00f, 250.0f, 350.0f, 0.2f, false);
		M_SonarRed.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(1.0f, 0.3f, 0.2f, 0.10f), EffectController.Singleton.lightSonarTrail);
		M_SonarRed.SetRapidVars(4, 0.1f, 0.0f, 0.9f);
		M_SonarRed.SetPiercing();
		M_SonarRed.SetBuff(Buff.Type.Burn);
		M_SonarRed.SetDamage(1.0f);

		M_SonarPink = holderObject.AddComponent<Weapon>();
		M_SonarPink.SetStandardVars(2.00f, 300.0f, 400.0f, 0.2f, false);
		M_SonarPink.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(1.0f, 0.4f, 0.9f, 0.10f), EffectController.Singleton.lightSonarTrail);
		M_SonarPink.SetRapidVars(4, 0.1f, 0.0f, 0.9f);
		M_SonarPink.SetPiercing();
		M_SonarPink.SetDamage(1.0f);

		M_UltimateSonar = holderObject.AddComponent<Weapon>();
		M_UltimateSonar.SetStandardVars(2.00f, 300.0f, 500.0f, 0.2f, false);
		M_UltimateSonar.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(1.0f, 0.4f, 0.9f, 0.10f), EffectController.Singleton.lightSonarTrail);
		M_UltimateSonar.SetRapidVars(5, 0.1f, 0.0f, 0.9f);
		M_UltimateSonar.SetPiercing();
		M_UltimateSonar.SetDamage(1.0f);


		//Weapon Platform
		P_BeamPink = holderObject.AddComponent<Weapon>();
		P_BeamPink.SetStandardVars(1.0f, 400.0f, 200.0f, 0.2f);
		P_BeamPink.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color(1.0f, 0.8f, 1.0f, 1.0f), new Color(0.75f, 0.2f, 1.0f, 1.0f));
		P_BeamPink.SetDamage(7.0f);
		P_BeamPink.SetBeamVars(4.0f);

		P_BeamOrange = holderObject.AddComponent<Weapon>();
		P_BeamOrange.SetStandardVars(1.0f, 400.0f, 200.0f, 0.2f);
		P_BeamOrange.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(255, 227, 206, 255), new Color32(255, 86, 0, 255));
		P_BeamOrange.SetDamage(7.0f);
		P_BeamOrange.SetBeamVars(3.25f);

		P_BeamRed = holderObject.AddComponent<Weapon>();
		P_BeamRed.SetStandardVars(1.0f, 400.0f, 200.0f, 0.2f);
		P_BeamRed.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(255, 193, 193, 255), new Color32(255, 13, 13, 255));
		P_BeamRed.SetDamage(7.0f);
		P_BeamRed.SetBeamVars(2.5f);

		P_BeamGreen = holderObject.AddComponent<Weapon>();
		P_BeamGreen.SetStandardVars(1.0f, 400.0f, 200.0f, 0.2f);
		P_BeamGreen.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(206, 255, 215, 255), new Color32(0, 255, 86, 255));
		P_BeamGreen.SetDamage(7.0f);
		P_BeamGreen.SetBeamVars(2.0f);

		P_BeamBlue = holderObject.AddComponent<Weapon>();
		P_BeamBlue.SetStandardVars(1.0f, 400.0f, 200.0f, 0.2f);
		P_BeamBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, new Color32(150, 216, 255, 255), new Color32(0, 76, 255, 255));
		P_BeamBlue.SetDamage(7.0f);
		P_BeamBlue.SetBeamVars(1.5f);

		P_ElectroGreen = holderObject.AddComponent<Weapon>();
		P_ElectroGreen.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_ElectroGreen.SetGraphicVars(WeaponGraphic.Image.ElectroGreen, greenStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_ElectroGreen.SetDamage(1.0f);
		
		P_ElectroYellow = holderObject.AddComponent<Weapon>();
		P_ElectroYellow.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_ElectroYellow.SetGraphicVars(WeaponGraphic.Image.ElectroYellow, yellowStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_ElectroYellow.SetDamage(1.0f);
		
		P_ElectroPink = holderObject.AddComponent<Weapon>();
		P_ElectroPink.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_ElectroPink.SetGraphicVars(WeaponGraphic.Image.ElectroPink, pinkStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_ElectroPink.SetDamage(1.0f);
		
		P_ElectroRed = holderObject.AddComponent<Weapon>();
		P_ElectroRed.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_ElectroRed.SetGraphicVars(WeaponGraphic.Image.ElectroRed, redStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_ElectroRed.SetDamage(1.0f);
		
		P_ElectroBlue = holderObject.AddComponent<Weapon>();
		P_ElectroBlue.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_ElectroBlue.SetGraphicVars(WeaponGraphic.Image.ElectroBlue, blueStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_ElectroBlue.SetDamage(1.0f);
		
		P_MissileGreen = holderObject.AddComponent<Weapon>();
		P_MissileGreen.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_MissileGreen.SetGraphicVars(WeaponGraphic.Image.MissileGreen, new Color(0.0f, 0.0f, 0.8f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_MissileGreen.SetDamage(1.0f);
		
		P_MissileYellow = holderObject.AddComponent<Weapon>();
		P_MissileYellow.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_MissileYellow.SetGraphicVars(WeaponGraphic.Image.MissileYellow, new Color(0.0f, 0.0f, 0.8f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_MissileYellow.SetDamage(1.0f);
		
		P_MissilePink = holderObject.AddComponent<Weapon>();
		P_MissilePink.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_MissilePink.SetGraphicVars(WeaponGraphic.Image.MissilePink, new Color(0.0f, 0.0f, 0.8f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_MissilePink.SetDamage(1.0f);
		
		P_MissileRed = holderObject.AddComponent<Weapon>();
		P_MissileRed.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_MissileRed.SetGraphicVars(WeaponGraphic.Image.MissileRed, new Color(0.0f, 0.0f, 0.8f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_MissileRed.SetDamage(1.0f);
		
		P_MissileBlue = holderObject.AddComponent<Weapon>();
		P_MissileBlue.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_MissileBlue.SetGraphicVars(WeaponGraphic.Image.MissileBlue, new Color(0.0f, 0.0f, 0.8f, 1.0f), new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_MissileBlue.SetDamage(1.0f);
		
		P_RoundGreen = holderObject.AddComponent<Weapon>();
		P_RoundGreen.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_RoundGreen.SetGraphicVars(WeaponGraphic.Image.RoundGreen, greenStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_RoundGreen.SetDamage(1.0f);
		
		P_RoundYellow = holderObject.AddComponent<Weapon>();
		P_RoundYellow.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_RoundYellow.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_RoundYellow.SetDamage(1.0f);
		
		P_RoundPink = holderObject.AddComponent<Weapon>();
		P_RoundPink.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_RoundPink.SetGraphicVars(WeaponGraphic.Image.RoundPink, pinkStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_RoundPink.SetDamage(1.0f);
		
		P_RoundRed = holderObject.AddComponent<Weapon>();
		P_RoundRed.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_RoundRed.SetGraphicVars(WeaponGraphic.Image.RoundRed, redStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_RoundRed.SetDamage(1.0f);
		
		P_RoundBlue = holderObject.AddComponent<Weapon>();
		P_RoundBlue.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_RoundBlue.SetGraphicVars(WeaponGraphic.Image.RoundBlue, blueStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_RoundBlue.SetDamage(1.0f);
		
		P_SonarGreen = holderObject.AddComponent<Weapon>();
		P_SonarGreen.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SonarGreen.SetGraphicVars(WeaponGraphic.Image.SonarGreen, greenStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SonarGreen.SetDamage(1.0f);
		
		P_SonarYellow = holderObject.AddComponent<Weapon>();
		P_SonarYellow.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SonarYellow.SetGraphicVars(WeaponGraphic.Image.SonarYellow, yellowStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SonarYellow.SetDamage(1.0f);
		
		P_SonarPink = holderObject.AddComponent<Weapon>();
		P_SonarPink.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SonarPink.SetGraphicVars(WeaponGraphic.Image.SonarPink, pinkStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SonarPink.SetDamage(1.0f);
		
		P_SonarRed = holderObject.AddComponent<Weapon>();
		P_SonarRed.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SonarRed.SetGraphicVars(WeaponGraphic.Image.SonarRed, redStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SonarRed.SetDamage(1.0f);
		
		P_SonarBlue = holderObject.AddComponent<Weapon>();
		P_SonarBlue.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SonarBlue.SetGraphicVars(WeaponGraphic.Image.SonarBlue, blueStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SonarBlue.SetDamage(1.0f);
		
		P_SpeedGreen = holderObject.AddComponent<Weapon>();
		P_SpeedGreen.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SpeedGreen.SetGraphicVars(WeaponGraphic.Image.SpeedGreen, greenStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SpeedGreen.SetDamage(1.0f);
		
		P_SpeedYellow = holderObject.AddComponent<Weapon>();
		P_SpeedYellow.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SpeedYellow.SetGraphicVars(WeaponGraphic.Image.SpeedYellow, yellowStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SpeedYellow.SetDamage(1.0f);
		
		P_SpeedPink = holderObject.AddComponent<Weapon>();
		P_SpeedPink.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SpeedPink.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SpeedPink.SetDamage(1.0f);
		
		P_SpeedRed = holderObject.AddComponent<Weapon>();
		P_SpeedRed.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SpeedRed.SetGraphicVars(WeaponGraphic.Image.SpeedRed, redStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SpeedRed.SetDamage(1.0f);
		
		P_SpeedBlue = holderObject.AddComponent<Weapon>();
		P_SpeedBlue.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_SpeedBlue.SetGraphicVars(WeaponGraphic.Image.SpeedBlue, blueStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_SpeedBlue.SetDamage(1.0f);
		
		P_StandarGreen = holderObject.AddComponent<Weapon>();
		P_StandarGreen.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StandarGreen.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StandarGreen.SetDamage(1.0f);
		
		P_StandarYellow = holderObject.AddComponent<Weapon>();
		P_StandarYellow.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StandarYellow.SetGraphicVars(WeaponGraphic.Image.StandardYellow, yellowStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StandarYellow.SetDamage(1.0f);
		
		P_StandarPink = holderObject.AddComponent<Weapon>();
		P_StandarPink.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StandarPink.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StandarPink.SetDamage(1.0f);
		
		P_StandarRed = holderObject.AddComponent<Weapon>();
		P_StandarRed.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StandarRed.SetGraphicVars(WeaponGraphic.Image.StandardRed, redStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StandarRed.SetDamage(1.0f);
		
		P_StandarBlue = holderObject.AddComponent<Weapon>();
		P_StandarBlue.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StandarBlue.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StandarBlue.SetDamage(1.0f);
		
		P_StarGreen = holderObject.AddComponent<Weapon>();
		P_StarGreen.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StarGreen.SetGraphicVars(WeaponGraphic.Image.StarGreen, greenStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarGreen.SetDamage(1.0f);
		
		P_StarYellow = holderObject.AddComponent<Weapon>();
		P_StarYellow.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StarYellow.SetGraphicVars(WeaponGraphic.Image.StarYellow, yellowStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarYellow.SetDamage(1.0f);
		
		P_StarPink = holderObject.AddComponent<Weapon>();
		P_StarPink.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StarPink.SetGraphicVars(WeaponGraphic.Image.StarPink, pinkStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarPink.SetDamage(1.0f);
		
		P_StarRed = holderObject.AddComponent<Weapon>();
		P_StarRed.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StarRed.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarRed.SetDamage(1.0f);
		
		P_StarBlue = holderObject.AddComponent<Weapon>();
		P_StarBlue.SetStandardVars(0.25f, 201.0f, 200.0f, 0.2f);
		P_StarBlue.SetGraphicVars(WeaponGraphic.Image.StarBlue, blueStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarBlue.SetDamage(1.0f);

		P_StarGreenS = holderObject.AddComponent<Weapon>();
		P_StarGreenS.SetStandardVars(0.75f, 51.0f, 50.0f, 0.2f);
		P_StarGreenS.SetGraphicVars(WeaponGraphic.Image.StarGreen, greenStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarGreenS.SetDamage(1.0f);
		
		P_StarYellowS = holderObject.AddComponent<Weapon>();
		P_StarYellowS.SetStandardVars(0.75f, 51.0f, 50.0f, 0.2f);
		P_StarYellowS.SetGraphicVars(WeaponGraphic.Image.StarYellow, yellowStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarYellowS.SetDamage(1.0f);
		
		P_StarPinkS = holderObject.AddComponent<Weapon>();
		P_StarPinkS.SetStandardVars(0.75f, 51.0f, 50.0f, 0.2f);
		P_StarPinkS.SetGraphicVars(WeaponGraphic.Image.StarPink, pinkStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarPinkS.SetDamage(1.0f);
		
		P_StarRedS = holderObject.AddComponent<Weapon>();
		P_StarRedS.SetStandardVars(0.75f, 51.0f, 50.0f, 0.2f);
		P_StarRedS.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarRedS.SetDamage(1.0f);
		
		P_StarBlueS = holderObject.AddComponent<Weapon>();
		P_StarBlueS.SetStandardVars(0.75f, 51.0f, 50.0f, 0.2f);
		P_StarBlueS.SetGraphicVars(WeaponGraphic.Image.StarBlue, blueStart, new Color(0.0f, 0.0f, 1.0f, 1.0f));
		P_StarBlueS.SetDamage(1.0f);

		P_LaserPink = holderObject.AddComponent<Weapon>();
		P_LaserPink.SetStandardVars(0.25f, 850.0f, 700.0f, 0.2f);
		P_LaserPink.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, new Color(0.2f, 0.1f, 0.7f, 1.0f), EffectController.Singleton.lineTrail2);
		P_LaserPink.SetRapidVars(5, 0.12f, 0.0f, 0.0f);
		P_LaserPink.SetDamage(1.0f);
		
		P_LaserR = holderObject.AddComponent<Weapon>();
		P_LaserR.SetStandardVars(3.0f, 850.0f, 700.0f, 0.2f);
		P_LaserR.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, new Color(0.0f, 0.0f, 1.0f, 1.0f), EffectController.Singleton.lineTrail2);
		P_LaserR.SetRapidVars(5, 0.12f, 0.0f, 0.0f);
		P_LaserR.SetDamage(1.0f);

		P_ScatterShot = holderObject.AddComponent<Weapon>();
		P_ScatterShot.SetStandardVars(4.0f, 100.0f, 400.0f, 0.03f, true);
		P_ScatterShot.SetDamage (1.0f);
		P_ScatterShot.SetIgnoreArmor();
		P_ScatterShot.SetGraphicVars(WeaponGraphic.Image.SpeedYellow, yellowStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		P_ScatterShot.SetScatterVars(3, KJMath.CIRCLE);
		P_ScatterShot.SetRapidVars(20, 0.12f, 0.0f, 1.8f);

		B_G_RapidRed = holderObject.AddComponent<Weapon>();
		B_G_RapidRed.SetStandardVars(3.5f, 500.0f, 200.0f, 0.2f);
		B_G_RapidRed.SetDamage(1.0f);
		B_G_RapidRed.SetGraphicVars(WeaponGraphic.Image.StarRed, redStart, Color.white);
		B_G_RapidRed.SetRapidVars(16, 0.06f, 0.65f, KJMath.CIRCLE * 0.65f);

		B_BlueStun = holderObject.AddComponent<Weapon>();
		B_BlueStun.SetStandardVars(3.5f, 600.0f, 200.0f, 0.2f);
		B_BlueStun.SetDamage(0.75f);
		B_BlueStun.SetGraphicVars(WeaponGraphic.Image.RoundBlue, blueStart, Color.white);
		B_BlueStun.SetRapidVars(12, 0.09f, 0.7f);
		B_BlueStun.SetBuff(Buff.Type.Stun);

		B_C_Support = holderObject.AddComponent<Weapon>();
		B_C_Support.SetStandardVars(1.0f, 250.0f, 150.0f, 0.2f);
		B_C_Support.SetGraphicVars(WeaponGraphic.Image.RoundRed, redStart, new Color(1.0f, 0.0f, 0.0f, 0.0f));
		B_C_Support.SetRapidVars(8, 0.075f, 0.5f, 1.5f);

		B_C_Slow = holderObject.AddComponent<Weapon>();
		B_C_Slow.SetStandardVars(1.25f, 300.0f, 125.0f, 0.2f);
		B_C_Slow.SetGraphicVars(WeaponGraphic.Image.RoundYellow, yellowStart, new Color(1.0f, 0.0f, 0.0f, 0.0f));
		B_C_Slow.SetRapidVars(8, 0.12f, 0.5f, 1.5f);

		B_ScatterPink = holderObject.AddComponent<Weapon>();
		B_ScatterPink.SetStandardVars(4.0f, 400.0f, 250.0f, 0.2f);
		B_ScatterPink.SetDamage(1.2f);
		B_ScatterPink.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, Color.white);
		B_ScatterPink.SetScatterVars(14, KJMath.CIRCLE);
		B_ScatterPink.SetRapidVars(3, 0.5f, 0, 2.5f);

		B_C_ScatterPink = holderObject.AddComponent<Weapon>();
		B_C_ScatterPink.SetStandardVars(1f, 350.0f, 200.0f, 0.2f);
		B_C_ScatterPink.SetDamage(1.2f);
		B_C_ScatterPink.SetGraphicVars(WeaponGraphic.Image.SpeedPink, pinkStart, Color.white);
		B_C_ScatterPink.SetScatterVars(10, KJMath.CIRCLE);
		B_C_ScatterPink.SetRapidVars(3, 0.5f, 0, 2.5f);

		B_RapidGreen = holderObject.AddComponent<Weapon>();
		B_RapidGreen.SetStandardVars(2.0f, 500.0f, 200.0f, 0.2f);
		B_RapidGreen.SetDamage(0.7f);
		B_RapidGreen.SetGraphicVars(WeaponGraphic.Image.StarGreen, greenStart, Color.white);
		B_RapidGreen.SetRapidVars(20, 0.25f, 0.7f);

		X_Boomerang = holderObject.AddComponent<Weapon>();
		X_Boomerang.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		X_Boomerang.SetDamage(0.7f);
		X_Boomerang.SetGraphicVars(WeaponGraphic.Image.SwirlBlue, greenStart, Color.white);
//		X_Boomerang.SetRapidVars(20, 0.25f, 0.7f);
		X_Boomerang.SetScatterVars(6, KJMath.CIRCLE);
		X_Boomerang.SetTime(2.5f);
		X_Boomerang.SetBoomerang(0.7f, 0.0f);
		X_Boomerang.SetDoubleHitRadius();

		X_Swirl = holderObject.AddComponent<Weapon>();
		X_Swirl.SetStandardVars(3.0f, 0.0f, 600.0f, 0.03f);
		X_Swirl.SetDamage(0.7f);
		X_Swirl.SetGraphicVars(WeaponGraphic.Image.SwirlBlue, greenStart, Color.white);
		X_Swirl.SetRapidVars(5, 0.25f, 1.2f);
		X_Swirl.SetTime(10.0f);
		X_Swirl.SetSwirl(0.5f, 0.25f);
		X_Swirl.SetPiercing();
		X_Swirl.SetDoubleHitRadius();

		//Rival
		R_StandardYellow = holderObject.AddComponent<Weapon>();
		R_StandardYellow.SetDamage(0.5f);
		R_StandardYellow.SetIgnoreArmor();
		R_StandardYellow.SetStandardVars(3f, 800.0f, 600.0f, 0.15f, true);
		R_StandardYellow.SetGraphicVars(WeaponGraphic.Image.StandardYellow, yellowStart, new Color(0.6f, 0.15f, 0.0f, 1.0f));
		R_StandardYellow.SetRapidVars(12, 0.07f, 0, 0);

		R_StandardBlue = holderObject.AddComponent<Weapon>();
		R_StandardBlue.SetDamage(0.5f);
		R_StandardBlue.SetIgnoreArmor();
		R_StandardBlue.SetStandardVars(3f, 800.0f, 600.0f, 0.15f, true);
		R_StandardBlue.SetGraphicVars(WeaponGraphic.Image.StandardBlue, blueStart, blueStart);
		R_StandardBlue.SetRapidVars(12, 0.07f, 0, 0);

		R_StandardGreen = holderObject.AddComponent<Weapon>();
		R_StandardGreen.SetDamage(0.5f);
		R_StandardGreen.SetIgnoreArmor();
		R_StandardGreen.SetStandardVars(3f, 800.0f, 600.0f, 0.15f, true);
		R_StandardGreen.SetGraphicVars(WeaponGraphic.Image.StandardGreen, greenStart, greenStart);
		R_StandardGreen.SetRapidVars(12, 0.07f, 0, 0);

		R_StandardRed = holderObject.AddComponent<Weapon>();
		R_StandardRed.SetDamage(0.5f);
		R_StandardRed.SetIgnoreArmor();
		R_StandardRed.SetStandardVars(3f, 800.0f, 600.0f, 0.15f, true);
		R_StandardRed.SetGraphicVars(WeaponGraphic.Image.StandardRed, redStart, redStart);
		R_StandardRed.SetRapidVars(12, 0.07f, 0, 0);

		R_StandardPink = holderObject.AddComponent<Weapon>();
		R_StandardPink.SetDamage(0.5f);
		R_StandardPink.SetIgnoreArmor();
		R_StandardPink.SetStandardVars(3f, 800.0f, 600.0f, 0.15f, true);
		R_StandardPink.SetGraphicVars(WeaponGraphic.Image.StandardPink, pinkStart, pinkStart);
		R_StandardPink.SetRapidVars(12, 0.07f, 0, 0);

	}

	//New Boss Weapon
	public static Weapon B_G_RapidRed { get; private set; }
	public static Weapon B_BlueStun { get; private set; }
	public static Weapon B_C_Support { get; private set; }
	public static Weapon B_C_Slow { get; private set; }
	public static Weapon B_ScatterPink { get; private set; }
	public static Weapon B_C_ScatterPink { get; private set; }
	public static Weapon B_RapidGreen { get; private set; }

	//Platform
	public static Weapon P_ScatterShot { get; private set; }

	public static Weapon P_BeamPink { get; private set; }
	public static Weapon P_BeamOrange { get; private set; }
	public static Weapon P_BeamRed { get; private set; }
	public static Weapon P_BeamGreen { get; private set; }
	public static Weapon P_BeamBlue { get; private set; }
	public static Weapon P_ElectroGreen { get; private set; }
	public static Weapon P_ElectroYellow { get; private set; }
	public static Weapon P_ElectroPink { get; private set; }
	public static Weapon P_ElectroRed { get; private set; }
	public static Weapon P_ElectroBlue { get; private set; }
	public static Weapon P_MissileGreen { get; private set; }
	public static Weapon P_MissileYellow { get; private set; }
	public static Weapon P_MissilePink { get; private set; }
	public static Weapon P_MissileRed { get; private set; }
	public static Weapon P_MissileBlue { get; private set; }
	public static Weapon P_RoundGreen { get; private set; }
	public static Weapon P_RoundYellow { get; private set; }
	public static Weapon P_RoundPink { get; private set; }
	public static Weapon P_RoundRed { get; private set; }
	public static Weapon P_RoundBlue { get; private set; }
	public static Weapon P_SonarGreen { get; private set; }
	public static Weapon P_SonarYellow { get; private set; }
	public static Weapon P_SonarPink { get; private set; }
	public static Weapon P_SonarRed { get; private set; }
	public static Weapon P_SonarBlue { get; private set; }
	public static Weapon P_SpeedGreen { get; private set; }
	public static Weapon P_SpeedYellow { get; private set; }
	public static Weapon P_SpeedPink { get; private set; }
	public static Weapon P_SpeedRed { get; private set; }
	public static Weapon P_SpeedBlue { get; private set; }
	public static Weapon P_StandarGreen { get; private set; }
	public static Weapon P_StandarYellow { get; private set; }
	public static Weapon P_StandarPink { get; private set; }
	public static Weapon P_StandarRed { get; private set; }
	public static Weapon P_StandarBlue { get; private set; }
	public static Weapon P_StarGreen { get; private set; }
	public static Weapon P_StarYellow { get; private set; }
	public static Weapon P_StarPink { get; private set; }
	public static Weapon P_StarRed { get; private set; }
	public static Weapon P_StarBlue { get; private set; }

	public static Weapon P_StarGreenS { get; private set; }
	public static Weapon P_StarYellowS { get; private set; }
	public static Weapon P_StarPinkS { get; private set; }
	public static Weapon P_StarRedS { get; private set; }
	public static Weapon P_StarBlueS { get; private set; }


	public static Weapon P_LaserPink { get; private set; }
	public static Weapon P_LaserR { get; private set; }
}
