﻿using UnityEngine;
using System.Collections;
#if UNITY_IPHONE
using UnityEngine.SocialPlatforms.GameCenter;
#endif

public class GeminiGameCenter : MonoBehaviour {

	public  GeminiGameCenter ()
	{
		#if UNITY_IPHONE
		Social.localUser.Authenticate( success => {
			if (success)
			{
				Authenticate();
			}
			else
			{
				Debug.Log("Unable to connect to GameCenter");
				//StandardPopup.Deploy("Unable to connect to GameCenter","Unable to connect to GameCenter","OK",KJPopUp.Close);
			}
		} );
		#endif

	}


	static GeminiGameCenter _Singleton;
	public static GeminiGameCenter Singleton {
		get {

			if (_Singleton == null) {
				_Singleton = new GeminiGameCenter ();

			}

			return _Singleton;
		}

		set {
			_Singleton = value;
		}
	}


	void Authenticate ()
	{
		#if UNITY_IPHONE

		GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
		Debug.Log("SUCCESS  to authenticate");
		//		Social.LoadAchievements (achievements => {
		//		Debug.Log ("number of a is "  + achievements.Length);
		//});
		#endif
	}

	public void ReportAchievement(string id,float progress) {
		#if UNITY_IPHONE

		Social.ReportProgress( id, progress, (result) => {
			Debug.Log ( result ? "Reported achievement" : "Failed to report achievement" + id);
			GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);

		} );
		#endif

	}

	public static void ShowGamcenterAchievementUI()
	{
		#if UNITY_IPHONE

		Social.ShowAchievementsUI();
		#endif
	}

	public void Reset ()
	{
	}
}
