﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Achievement {

	public static int enumCount;
	public static List<bool> AchievementStatus = new List<bool>();

	public static bool noTakeDamage = true;

	public static void Initialize ()
	{
		enumCount = System.Enum.GetNames(typeof(AchievementType)).Length;

		ResetAchievements();
	}

	public static void ResetAchievements () {

		AchievementStatus = new List<bool>();
		for (int i = 0 ; i < enumCount ; i ++ )
		{
			AchievementStatus.Add (false);
		}

	}

	public static void AchievementComplete (AchievementType achievementType) {
		//Debug.Log("AchievementComplete # " + achievementType + " index = " + (int)achievementType );
		int achIndex = (int) achievementType;
		if (!AchievementStatus[achIndex]) {
			AchievementStatus[achIndex] = true;
			AchievementManager.PostAchievement(achievementType.ToString());
		}
	}

	public enum AchievementType {

		A0Collect1000Gems,
		A1BountyMission, //Boss
		A2SecretMission, // Elite
		A3ReachSector5,
		A4ReachSector10,
		A5ReacSector15,
		A6ReachSector20,

		A7FindRareItem,
		A8FindSecretItem,
		A9UnlockShip,
		A10UpgradeItem,

		A11ClearPlatform,
		A12ClearRock,
		A13ClearMines

	}
	
}
