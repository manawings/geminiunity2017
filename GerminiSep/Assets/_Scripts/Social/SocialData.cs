using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SocialData {

	public enum DataType
	{
		Int,
		String,
		Float
	}

	public string key;
	public int intValue;
	public string stringValue;
	public float floatValue;
	public DataType dataType;

	public SocialData (string key, string value)
	{
		this.key = key;
		stringValue = value;
		dataType = DataType.String;
	}

	public SocialData (string key, int value)
	{
		this.key = key;
		intValue = value;
		dataType = DataType.Int;
	}

	public SocialData (string key, float value)
	{
		this.key = key;
		floatValue = value;
		dataType = DataType.Float;
	}

}
