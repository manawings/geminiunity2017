﻿using UnityEngine;
using System.Collections;
using CodeStage.AntiCheat.ObscuredTypes;

public class FriendItem {

	public enum FriendStatus
	{
		None,
		Friend,
		Requested,
		Pending,
		FriendReward,
		GoldReward
	}

	// Reward Stats
	public ObscuredInt rewardAmount = 0;
	public int rewardPoints = 0;
	public int shipId = 0;

	public string name, displayName, realName;
	public string id;
	public string profilePicURL;
	public FriendStatus status = FriendStatus.None;
	public Texture fbTexture;


}
