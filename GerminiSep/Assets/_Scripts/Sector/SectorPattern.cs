﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SectorPattern {

	public string name;
	public int id = 0, overWrite = 1, bossOveride = 0;

	// Background Info

	// Story

	public int story_SectorEnter = -1, story_SectorMid = -1, story_SectorBoss = -1, story_PreBoss = -1, story_PreVictory = -1, story_FirstSector = -1;


	public static SectorPattern sector; // MilkyWay;
	public static List< SectorPattern > sectorList; // MilkyWay;

	private static bool hasBeenInit = false;

	public string SectorBGSet {
		get {

			return BGGraphics.BgSetNames[id % 2];

		}
	}

	public int SectorBGStartId {
		get {

			return Random.Range(1, 4);
			
		}
	}

	public Vector2 SectorMoonPosition {
		get {

			Vector2 moonPosition = Vector2.one;
			moonPosition.x = Random.Range(-80, 80);
			moonPosition.y = Random.Range(-150, 150);

			return moonPosition;
			
		}
	}

	public float SectorMoonRotation {
		get {
			return Random.Range(0, 360);
			
		}
	}

	public int Seed {
		get {
			return (id * 15) + overWrite;
		}
	}

	public BGGraphics SectorGraphic
	{
		get {
			Random.seed = Seed;
			BGGraphics returnGraphic = KJMath.RandomMemberOf<BGGraphics>(BGGraphics.list);
			return returnGraphic;

		}
	}

	public static void Initialize ()	
	{

		//		sector.story_SectorEnter = 11;
		//		sector.story_FirstSector = 12;
		//		sector.story_SectorMid = 13;
		//		sector.story_SectorBoss = 14;
		//		sector.story_PreBoss = 15;
		//		sector.story_PreVictory = 16;


		if (hasBeenInit) return;
		hasBeenInit = true;

		sectorList = new List<SectorPattern>();




		// 1
		CreateSector("Awakening", 1, 1);
		sector.story_SectorMid = 51;
		sector.story_PreBoss = 52;




		// 2
		CreateSector("Above the Fold", 2, 2);
		sector.story_SectorEnter = 55;
		sector.story_SectorMid = 56;
		sector.story_SectorBoss = 57;
		sector.story_PreBoss = 58;
		sector.story_PreVictory = 59;




		// 3
		CreateSector("Anniversary", 12, 3);
		sector.story_SectorEnter = 60;
		sector.story_FirstSector = 61;
		sector.story_SectorMid = 62;
		sector.story_SectorBoss = 63;
		sector.story_PreVictory = 64;



		// 4
		CreateSector("Retribution", 8, 4);
		sector.story_SectorEnter = 65;
		sector.story_FirstSector = 66;
		sector.story_SectorMid = 67;
		sector.story_PreVictory = 68;


		// 5
		CreateSector("POETS", 5, 5);
		sector.story_SectorEnter = 69;
		sector.story_SectorMid = 70;
		sector.story_SectorBoss = 71;
		sector.story_PreVictory = 72;


		// 6
		CreateSector("SPIDER HUNT", 5, 6);
		sector.story_SectorEnter = 73;
		sector.story_SectorMid = 74;
		sector.story_SectorBoss = 75;
		sector.story_PreVictory = 76;


		// 7
		CreateSector("REPRISE");
		sector.story_SectorEnter = 77;
		sector.story_SectorMid = 78;
		sector.story_SectorBoss = 79;
		sector.story_PreBoss = 80;
		sector.story_PreVictory = 81;

		// 8
		CreateSector("Drill");
		sector.story_SectorEnter = 82;
		sector.story_SectorBoss = 83;

		// 9
		CreateSector("DESTRUCTIVE FEEDBACK");
		sector.story_SectorEnter = 84;
		sector.story_SectorMid = 85;
		sector.story_SectorBoss = 86;
		sector.story_PreBoss = 87;
		sector.story_PreVictory = 88;


		// 10
		CreateSector("THE LAST SYLLABLE");
		sector.story_SectorEnter = 89;
		sector.story_SectorMid = 90;
		sector.story_SectorBoss = 91;
		sector.story_PreBoss = 92;
		sector.story_PreVictory = 93;


		// 11
		CreateSector("Ripples");
		sector.story_SectorEnter = 94;
		sector.story_SectorMid = 95;
		sector.story_SectorBoss = 96;
		sector.story_PreVictory = 97;


		// 12
		CreateSector("Letters");
		sector.story_SectorEnter = 98;
		sector.story_FirstSector = 99;
		sector.story_SectorMid = 100;
		sector.story_SectorBoss = 101;
		sector.story_PreVictory = 102;


		// 13
		CreateSector("Twins");
		sector.story_SectorEnter = 103;
		sector.story_SectorMid = 104;
		sector.story_SectorBoss = 105;
		sector.story_PreVictory = 106;


		// 14
		CreateSector("Investigation");
		sector.story_SectorEnter = 107;
		sector.story_SectorMid = 108;
		sector.story_SectorBoss = 109;
		sector.story_PreVictory = 110;


		// 15
		CreateSector("Mortal Hero", 1);
		sector.story_SectorEnter = 111;
		sector.story_SectorMid = 112;
		sector.story_SectorBoss = 113;
		sector.story_PreVictory = 114;


		// 16
		CreateSector("The Wrong Books");
		sector.story_SectorEnter = 115;
		sector.story_FirstSector = 116;
		sector.story_SectorMid = 117;
		sector.story_SectorBoss = 118;
		sector.story_PreVictory = 119;


		// 17
		CreateSector("AUTOBIOGRAPHY", 2);
		sector.story_SectorEnter = 120;
		sector.story_FirstSector = 121;
		sector.story_SectorMid = 122;
		sector.story_SectorBoss = 123;
		sector.story_PreBoss = 124;
		sector.story_PreVictory = 125;


		// 18
		CreateSector("ONE OF THOSE DAYS");
		sector.story_SectorEnter = 126;
		sector.story_FirstSector = 127;
		sector.story_SectorMid = 128;
		sector.story_SectorBoss = 129;


		// 19
		CreateSector("HANGMAN'S NOOSE");
		sector.story_SectorEnter = 130;
		sector.story_SectorMid = 131;
		sector.story_PreVictory = 132;


		// 20
		CreateSector("THE FINAL VERDICT");
		sector.story_SectorEnter = 133;
		sector.story_SectorMid = 134;
		sector.story_SectorBoss = 135;
		sector.story_PreBoss = 136;
		sector.story_PreVictory = 137;

		// 21
		CreateSector("REFLECTIONS", 5); 
		// NO STORY

		// 22
		CreateSector("PAROLE", 1);
		sector.story_SectorEnter = 138;
		sector.story_SectorMid = 139;

		// 23
		CreateSector("SCAN");
		sector.story_SectorEnter = 139;
		sector.story_SectorMid = 140;

		// 24
		CreateSector("ACTORS");
		sector.story_SectorEnter = 141;
		sector.story_SectorMid = 142;
		sector.story_SectorBoss = 143;
		sector.story_PreVictory = 144;

		// 25
		CreateSector("BUSINESS", 5);
		// NO STORY

		// 26
		CreateSector("BIRTHDAY", 9);
		sector.story_SectorEnter = 145;
		sector.story_SectorMid = 146;
		sector.story_SectorBoss = 147;
		sector.story_PreVictory = 148;

		// 27
		CreateSector("PERPLEXION");
		sector.story_SectorEnter = 149;
		sector.story_FirstSector = 150;
		sector.story_SectorMid = 151;
		sector.story_SectorBoss = 152;
		sector.story_PreVictory = 153;


		// 28
		CreateSector("SUBSISTENCE");
		sector.story_SectorEnter = 154;


		// 29
		CreateSector("TEMPORAL");
		// NO STORY

		// 30
		CreateSector("THINK OF ME");
		sector.story_SectorEnter = 155;

		// 31
		CreateSector("LINGERING");
		// NO STORY


		// 32
		CreateSector("ROMANS");
		sector.story_SectorEnter = 156;
		sector.story_SectorMid = 157;
		sector.story_PreVictory = 158;


		// 33
		CreateSector("GUILLOTINE");
		sector.story_SectorEnter = 159;
		sector.story_SectorMid = 160;
		sector.story_SectorBoss = 161;
		sector.story_PreVictory = 162;


		// 34
		CreateSector("THE DEAL");
		sector.story_SectorEnter = 163;
		sector.story_SectorMid = 164;
		sector.story_PreVictory = 165;


		// 35
		CreateSector("ABSENCE");
		sector.story_SectorEnter = 166;
		sector.story_SectorMid = 167;


		// 36
		CreateSector("LIMITED");
		sector.story_SectorEnter = 168;
		sector.story_SectorMid = 169;
		sector.story_SectorBoss = 170;
		sector.story_PreVictory = 171;


		// 37
		CreateSector("BETRAYAL");
		sector.story_SectorEnter = 172;
		sector.story_FirstSector = 173;
		sector.story_SectorMid = 174;
		sector.story_SectorBoss = 175;
		sector.story_PreVictory = 176;


		// 38
		CreateSector("CONTEMPLATION");
		sector.story_SectorEnter = 177;


		// 39
		CreateSector("MEMORIES");
		sector.story_SectorEnter = 178;


		// 40
		CreateSector("DECISION");
		sector.story_SectorEnter = 179;


		// 41
		CreateSector("NOT BY BLOOD");
		sector.story_SectorEnter = 180;
		sector.story_FirstSector = 181;
		sector.story_SectorMid = 182;
		sector.story_SectorBoss = 183;
		sector.story_PreBoss = 184;
		sector.story_PreVictory = 185;


		// 42
		CreateSector("NO HARD FEELINGS");
		sector.story_SectorEnter = 186;
		sector.story_FirstSector = 187;
		sector.story_SectorMid = 188;
		sector.story_SectorBoss = 189;
		sector.story_PreVictory = 190;


		// 43
		CreateSector("DIVIDE");
		sector.story_SectorEnter = 191;
		sector.story_SectorMid = 192;
		sector.story_SectorBoss = 193;
		sector.story_PreVictory = 194;

		// 44
		CreateSector("JAILBREAK");
		sector.story_SectorEnter = 195;
		sector.story_FirstSector = 196;
		sector.story_SectorMid = 197;
		sector.story_SectorBoss = 198;
		sector.story_PreBoss = 199;
		sector.story_PreVictory = 200;

		// 45
		CreateSector("FAILURE");
		sector.story_SectorEnter = 201;
		sector.story_SectorMid = 202;
		sector.story_PreVictory = 203;


		// 46
		CreateSector("MAYBE IF");
		sector.story_SectorEnter = 204;
		sector.story_SectorMid = 205;
		sector.story_PreVictory = 206;


		// 47
		CreateSector("ANTICIPATION");
		sector.story_SectorEnter = 207;
		sector.story_SectorMid = 208;
		sector.story_SectorBoss = 209;
		sector.story_PreVictory = 210;

		// 48
		CreateSector("PROUD");
		sector.story_SectorEnter = 211;
		sector.story_SectorMid = 212;
		sector.story_SectorBoss = 213;


		// 49
		CreateSector("SUBMISSION");
		sector.story_SectorEnter = 214;
		sector.story_SectorMid = 215;
		sector.story_SectorBoss = 216;


		// 50
		CreateSector("NO MORE GAMES");
		sector.story_SectorEnter = 217;
		sector.story_SectorMid = 218;
		sector.story_SectorBoss = 219;
		sector.story_PreBoss = 220;
		sector.story_PreVictory = 221;

		// 51
		CreateSector("MYTHS");
		sector.story_SectorEnter = 222;
		sector.story_SectorMid = 223;
		sector.story_SectorBoss = 224;
		sector.story_PreBoss = 225;
		sector.story_PreVictory = 226;

		// 52
		CreateSector("SPECTRE");
		sector.story_SectorEnter = 227;
		sector.story_SectorMid = 228;
		sector.story_SectorBoss = 229;
		sector.story_PreVictory = 230;

		// 53
		CreateSector("CHARIOT");
		sector.story_SectorEnter = 231;
		sector.story_SectorMid = 232;
		sector.story_SectorBoss = 233;
		sector.story_PreVictory = 234;

		// 54
		CreateSector("RUBICON");
		sector.story_SectorEnter = 235;
		sector.story_SectorMid = 236;
		sector.story_SectorBoss = 237;
		sector.story_PreVictory = 238;

		// 55
		CreateSector("THE JUSTICE QUARTET");
		sector.story_SectorEnter = 239;
		sector.story_SectorMid = 240;
		sector.story_SectorBoss = 241;
		sector.story_PreVictory = 242;


		// 56
		CreateSector("SCRAPBOOK");
		sector.story_SectorEnter = 243;
		sector.story_SectorMid = 244;
		sector.story_SectorBoss = 245;
		sector.story_PreVictory = 246;

		// 57
		CreateSector("BAD MEMORIES");
		sector.story_SectorEnter = 247;
		sector.story_SectorMid = 248;
		sector.story_SectorBoss = 249;

		// 58
		CreateSector("NEW INTEL");
		sector.story_SectorEnter = 250;
		sector.story_SectorMid = 251;
		sector.story_SectorBoss = 252;

		// 59
		CreateSector("INFAMOUS");
		sector.story_SectorEnter = 253;
		sector.story_SectorMid = 254;
		sector.story_PreBoss = 255;
		sector.story_PreVictory = 256;

		// 60
		CreateSector("PETRA COLONY");
		sector.story_SectorEnter = 257;
		sector.story_SectorMid = 258;
		sector.story_PreBoss = 259;
		sector.story_PreVictory = 260;

		// 61
		CreateSector("LIKE THE PHOENIX");
		sector.story_SectorEnter = 261;
		sector.story_SectorMid = 262;
		sector.story_SectorBoss = 263;
		sector.story_PreVictory = 264;

		// 62
		CreateSector("BRISTAINE");
		sector.story_SectorEnter = 265;
		sector.story_SectorMid = 266;
		sector.story_PreBoss = 267;
		sector.story_PreVictory = 268;


		// 63
		CreateSector("PRIVATE SECURITY");
		sector.story_SectorEnter = 269;
		sector.story_SectorMid = 270;
		sector.story_PreBoss = 271;
		sector.story_PreVictory = 272;

		// 64
		CreateSector("ELEDAN 5");
		sector.story_SectorEnter = 273;
		sector.story_SectorMid = 274;
		sector.story_SectorBoss = 275;
		sector.story_PreBoss = 276;
		sector.story_PreVictory = 277;

		// 65
		CreateSector("HISTORY LESSON");
		sector.story_SectorEnter = 278;
		sector.story_SectorMid = 279;
		sector.story_SectorBoss = 280;
		sector.story_PreVictory = 281;


		// 66
		CreateSector("SEIGMEID");
		sector.story_SectorEnter = 282;
		sector.story_SectorMid = 283;
		sector.story_SectorBoss = 284;
		sector.story_PreVictory = 285;

		// 67
		CreateSector("LOCK BLADES");
		sector.story_SectorEnter = 286;
		sector.story_SectorMid = 287;
		sector.story_PreBoss = 288;
		sector.story_PreVictory = 289;

		// 68
		CreateSector("NEUMANN");
		sector.story_SectorEnter = 290;
		sector.story_SectorMid = 291;
		sector.story_SectorBoss = 292;
		sector.story_PreVictory = 293;

		// 69
		CreateSector("SOON");
		sector.story_SectorEnter = 294;
		sector.story_SectorMid = 295;
		sector.story_PreBoss = 296;
		sector.story_PreVictory = 297;

		// 70
		CreateSector("RETROSPECT");
		sector.story_SectorEnter = 298;
		sector.story_FirstSector = 299;
		sector.story_SectorMid = 300;
		sector.story_SectorBoss = 301;
		sector.story_PreVictory = 302;


		// 71
		CreateSector("AUTOPILOT");
		sector.story_SectorEnter = 303;
		sector.story_SectorMid = 304;
		sector.story_SectorBoss = 305;
		sector.story_PreVictory = 306;

		// 72
		CreateSector("AUXILIARY");

		// 73
		CreateSector("PUBLIC ADDRESS");
		sector.story_SectorEnter = 307;
		sector.story_SectorMid = 308;
		sector.story_SectorBoss = 309;
		sector.story_PreVictory = 310;


		// 74
		CreateSector("PREPARATION");
		sector.story_SectorEnter = 311;
		sector.story_SectorMid = 312;
		sector.story_SectorBoss = 313;
		sector.story_PreVictory = 314;

		// 75
		CreateSector("DIGGING");
		sector.story_SectorEnter = 315;
		sector.story_SectorMid = 316;
		sector.story_SectorBoss = 317;
		sector.story_PreVictory = 318;

		// 76
		CreateSector("ADMIT IT");
		sector.story_SectorEnter = 319;
		sector.story_SectorMid = 320;
		sector.story_SectorBoss = 321;
		sector.story_PreVictory = 322;

		// 77
		CreateSector("RELOAD");
		sector.story_SectorEnter = 323;
		sector.story_SectorMid = 324;
		sector.story_SectorBoss = 325;
		sector.story_PreVictory = 326;
		sector.story_PreVictory = 327;

		// 78
		CreateSector("MISFORTUNE");
		sector.story_SectorEnter = 328;
		sector.story_SectorMid = 329;
		sector.story_SectorBoss = 330;
		sector.story_PreVictory = 331;

		// 79
		CreateSector("FORGIVENESS");
		sector.story_SectorEnter = 332;
		sector.story_SectorMid = 333;
		sector.story_SectorBoss = 334;
		sector.story_PreVictory = 335;


		// 80
		CreateSector("FUEL FOR THE FIRE");
		sector.story_SectorEnter = 336;
		sector.story_SectorMid = 337;
		sector.story_SectorBoss = 338;
		sector.story_PreVictory = 339;


		// 81
		CreateSector("THERAPY");
		sector.story_SectorEnter = 340;
		sector.story_SectorMid = 341;
		sector.story_SectorBoss = 342;
		sector.story_PreVictory = 343;


		// 82
		CreateSector("CONSCIENCE");
		sector.story_SectorEnter = 344;
		sector.story_SectorMid = 345;
		sector.story_SectorBoss = 346;
		sector.story_PreVictory = 347;


		// 83
		CreateSector("EXHAUST");
		sector.story_SectorEnter = 348;
		sector.story_SectorMid = 349;
		sector.story_SectorBoss = 350;
		sector.story_PreVictory = 351;


		// 84
		CreateSector("GUARDIAN");
		sector.story_SectorEnter = 352;
		sector.story_SectorMid = 353;
		sector.story_SectorBoss = 354;
		sector.story_PreVictory = 355;


		// 85
		CreateSector("REALITY");
		sector.story_SectorEnter = 356;
		sector.story_SectorMid = 357;
		sector.story_SectorBoss = 358;
		sector.story_PreVictory = 359;


		// 86
		CreateSector("REPRISE");
		sector.story_SectorEnter = 360;
		sector.story_SectorMid = 361;
		sector.story_PreVictory = 362;


		// 87
		CreateSector("DRONES");
		sector.story_SectorEnter = 363;
		sector.story_SectorMid = 364;

		// 88
		CreateSector("RESTLESS");

		// 89
		CreateSector("SATELLITE");
		sector.story_SectorBoss = 365;
		sector.story_PreVictory = 366;


		// 90
		CreateSector("TWILIGHT");
		sector.story_SectorEnter = 367;
		sector.story_SectorMid = 368;
		sector.story_PreBoss = 369;
		sector.story_PreVictory = 370;

		// 91
		CreateSector("AVALON");
		sector.story_SectorEnter = 371;
		sector.story_SectorMid = 372;
		sector.story_SectorBoss = 373;
		sector.story_PreVictory = 374;

		// 92
		CreateSector("MAGPIE");
		sector.story_SectorEnter = 375;
		sector.story_SectorMid = 376;
		sector.story_SectorBoss = 377;
		sector.story_PreVictory = 378;


		// 93
		CreateSector("SMOKE SCREEN");
		sector.story_SectorEnter = 379;
		sector.story_SectorMid = 380;
		sector.story_SectorBoss = 381;
		sector.story_PreVictory = 382;

		// 94
		CreateSector("TRANSCENDENCE");
		sector.story_SectorEnter = 383;
		sector.story_SectorMid = 384;
		sector.story_SectorBoss = 385;
		sector.story_PreVictory = 386;

		// 95
		CreateSector("COMPREHENSION");
		sector.story_SectorEnter = 387;
		sector.story_SectorMid = 388;
		sector.story_SectorBoss = 389;
		sector.story_PreVictory = 390;

		// 96
		CreateSector("HOURGLASS");
		sector.story_SectorEnter = 391;
		sector.story_SectorMid = 392;
		sector.story_SectorBoss = 393;
		sector.story_PreVictory = 394;

		// 97
		CreateSector("PROMISE");
		sector.story_SectorEnter = 395;
		sector.story_SectorMid = 396;
		sector.story_SectorBoss = 397;
		sector.story_PreVictory = 398;

		// 98
		CreateSector("ENDURANCE");

		// 99
		CreateSector("RECORDING");
		sector.story_SectorEnter = 399;
		sector.story_SectorMid = 400;
		sector.story_SectorBoss = 401;
		sector.story_PreVictory = 402;

		// 100
		CreateSector("DOWNFALL");
		sector.story_SectorEnter = 403;
		sector.story_SectorMid = 404;
		sector.story_PreBoss = 405;
		sector.story_PreVictory = 406;





//		sector.story_SectorEnter = 0;
//		sector.story_FirstSector = 0;
//		sector.story_SectorMid = 0;
//		sector.story_SectorBoss = 0;
//		sector.story_PreBoss = 0;
//		sector.story_PreVictory = 0;

		while (sectorList.Count < 120) {
			CreateSector("Randsec");
		}



	}

	private static void CreateSector (string name = "SECTOR NAME", int overrideSeed = 0, int bossOveride = 0)
	{
		sector = new SectorPattern();
		sector.id = sectorList.Count;

		sector.name = name;
		sector.overWrite = overrideSeed;
		sector.bossOveride = bossOveride;
		sectorList.Add(sector);

	}

	public static SectorPattern At (int index) {
		return sectorList[index];
	}

	void DeployStory (int storyId)
	{
		if (storyId!= -1) Story.DeployStory(storyId);
	}


	public void DeployEnterStory ()
	{
		if (ProfileController.previewMode) return;
		DeployStory(story_SectorEnter);
	}

	public void DeployFirstSectorStory ()
	{
		if (ProfileController.previewMode) return;
		DeployStory(story_FirstSector);
	}

	public void DeployMidSectorStory ()
	{
		if (ProfileController.previewMode) return;
		DeployStory(story_SectorMid);
	}

	public void DeployPreBossStory ()
	{
		if (ProfileController.previewMode) return;
		DeployStory(story_PreBoss);
	}

	public void DeployPreVictoryStory ()
	{
		if (ProfileController.previewMode) return;
		DeployStory(story_PreVictory);
	}

	public void DeployBossSectorStory ()
	{
		if (ProfileController.previewMode) return;
		DeployStory(story_SectorBoss);
	}


}
