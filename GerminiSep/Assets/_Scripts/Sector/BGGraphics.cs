using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BGGraphics {

	static bool hasBeenInit = false;

	public static List<BGGraphics> list;

	public Color skyColorTop, skyColorBottom;
	public Color moonColorTop, moonColorBottom;
	public Color glowColor;
	public Color midMainColor;
	public Color particleColorStart;
	public Color particleColorEnd;

	public static string[] BgSetNames = {"Set1Back", "Set2Back"};

	public static void Initialize ()
	{
		if (hasBeenInit) return;
		hasBeenInit = true;

		list = new List<BGGraphics>();

		// Mono Green
		Create (new Color (0.00f, 1.00f, 0.17f, 1.00f ),new Color (0.00f, 0.45f, 0.54f, 1.00f ),new Color (0.25f, 0.78f, 0.25f, 1.00f ),new Color (0.22f, 0.38f, 0.29f, 1.00f ),new Color (0.53f, 1.00f, 0.71f, 0.42f ),new Color (0.23f, 0.46f, 0.34f, 1.00f ),new Color (0.04f, 1.00f, 0.60f, 1.00f ),new Color (0.04f, 0.43f, 0.10f, 1.00f ));

		// Mono Blue
		Create (new Color (0.00f, 1.00f, 0.79f, 1.00f ),new Color (0.36f, 0.07f, 0.49f, 1.00f ),new Color (0.35f, 1.00f, 0.97f, 1.00f ),new Color (0.29f, 0.41f, 0.63f, 1.00f ),new Color (0.00f, 0.54f, 1.00f, 0.42f ),new Color (0.13f, 0.33f, 0.52f, 1.00f ),new Color (0.04f, 0.60f, 1.00f, 1.00f ),new Color (0.11f, 0.60f, 0.42f, 1.00f ));

		// Mono Red
		Create (new Color (1.00f, 0.00f, 0.19f, 1.00f ),new Color (0.14f, 0.00f, 0.16f, 1.00f ),new Color (0.68f, 0.16f, 0.16f, 1.00f ),new Color (0.43f, 0.18f, 0.18f, 1.00f ),new Color (1.00f, 0.20f, 0.58f, 0.42f ),new Color (0.54f, 0.27f, 0.27f, 1.00f ),new Color (1.00f, 0.04f, 0.04f, 1.00f ),new Color (0.60f, 0.11f, 0.59f, 1.00f ));

		// Mono Yellow
		Create (new Color (0.96f, 0.62f, 0.00f, 1.00f ),new Color (0.39f, 0.14f, 0.09f, 1.00f ),new Color (0.93f, 0.59f, 0.00f, 1.00f ),new Color (1.00f, 0.23f, 0.00f, 1.00f ),new Color (1.00f, 0.68f, 0.20f, 0.42f ),new Color (0.57f, 0.34f, 0.17f, 1.00f ),new Color (1.00f, 0.77f, 0.04f, 1.00f ),new Color (1.00f, 0.59f, 0.27f, 1.00f ));

		// RollDown: Pink + s.Blue
		Create (new Color (1.00f, 0.40f, 0.40f, 1.00f ),new Color (0.19f, 0.55f, 0.74f, 1.00f ),new Color (1.00f, 0.43f, 0.43f, 1.00f ),new Color (0.98f, 0.41f, 0.68f, 1.00f ),new Color (1.00f, 0.48f, 0.14f, 0.42f ),new Color (0.45f, 0.27f, 0.49f, 1.00f ),new Color (0.27f, 0.70f, 1.00f, 1.00f ),new Color (0.78f, 0.27f, 1.00f, 1.00f ));
	
		// RollDown: Green + s.Yellow
		Create (new Color (0.28f, 1.00f, 0.64f, 1.00f ),new Color (1.00f, 0.60f, 0.24f, 1.00f ),new Color (0.82f, 0.63f, 0.40f, 1.00f ),new Color (0.18f, 0.33f, 0.25f, 1.00f ),new Color (1.00f, 0.88f, 0.40f, 0.42f ),new Color (0.24f, 0.35f, 0.33f, 1.00f ),new Color (1.00f, 0.61f, 0.04f, 1.00f ),new Color (0.00f, 0.51f, 0.49f, 1.00f ));

		// RollDown: Blue + s.Yellow
		Create (new Color (0.18f, 0.66f, 0.89f, 1.00f ),new Color (0.96f, 0.62f, 0.31f, 1.00f ),new Color (1.00f, 0.76f, 0.41f, 1.00f ),new Color (0.81f, 0.38f, 0.31f, 1.00f ),new Color (1.00f, 0.60f, 0.00f, 0.42f ),new Color (0.23f, 0.36f, 0.45f, 1.00f ),new Color (0.04f, 0.68f, 1.00f, 1.00f ),new Color (0.00f, 0.17f, 0.51f, 1.00f ));
	
//		// RollDown: Red + s.Green
//		Create (new Color (0.21f, 0.72f, 0.26f, 1.00f ),new Color (1.00f, 0.51f, 0.48f, 1.00f ),new Color (0.20f, 0.70f, 0.24f, 1.00f ),new Color (0.24f, 0.58f, 0.40f, 1.00f ),new Color (0.40f, 1.00f, 0.68f, 0.42f ),new Color (0.44f, 0.25f, 0.21f, 1.00f ),new Color (0.04f, 0.68f, 1.00f, 1.00f ),new Color (0.00f, 0.17f, 0.51f, 1.00f ));

		// Dawn
		Create (new Color (0.00f, 0.73f, 0.83f, 1.00f ),new Color (1.00f, 0.51f, 0.48f, 1.00f ),new Color (0.57f, 0.27f, 0.42f, 1.00f ),new Color (0.40f, 0.49f, 0.50f, 1.00f ),new Color (1.00f, 0.51f, 0.07f, 0.42f ),new Color (0.40f, 0.29f, 0.42f, 1.00f ),new Color (0.04f, 0.68f, 1.00f, 1.00f ),new Color (0.00f, 0.17f, 0.51f, 1.00f ));

		// Simic
		Create (new Color (0.00f, 1.00f, 0.88f, 1.00f ),new Color (0.31f, 0.87f, 0.32f, 1.00f ),new Color (0.32f, 0.66f, 1.00f, 1.00f ),new Color (0.37f, 0.71f, 0.30f, 1.00f ),new Color (0.46f, 0.82f, 1.00f, 0.42f ),new Color (0.24f, 0.40f, 0.48f, 1.00f ),new Color (0.04f, 0.68f, 1.00f, 1.00f ),new Color (0.00f, 0.17f, 0.51f, 1.00f ));
	
		// Sakura1
		Create (new Color (0.29f, 1.00f, 0.74f, 1.00f ),new Color (1.00f, 0.50f, 0.28f, 1.00f ),new Color (0.86f, 0.85f, 0.86f, 1.00f ),new Color (0.81f, 0.40f, 0.22f, 1.00f ),new Color (0.74f, 0.20f, 0.35f, 0.42f ),new Color (0.47f, 0.38f, 0.31f, 1.00f ),new Color (1.00f, 0.57f, 0.99f, 1.00f ),new Color (1.00f, 0.13f, 0.22f, 1.00f ));
	
		// Sakura2
		Create (new Color (0.28f, 0.59f, 0.74f, 1.00f ),new Color (0.31f, 1.00f, 0.63f, 1.00f ),new Color (0.91f, 0.65f, 1.00f, 1.00f ),new Color (0.56f, 0.71f, 0.55f, 1.00f ),new Color (0.31f, 0.31f, 1.00f, 0.42f ),new Color (0.27f, 0.40f, 0.49f, 1.00f ),new Color (0.16f, 0.83f, 1.00f, 1.00f ),new Color (0.13f, 0.28f, 1.00f, 1.00f ));

		// ???
		Create (new Color (0.39f, 0.93f, 0.34f, 1.00f ),new Color (0.33f, 0.59f, 0.69f, 1.00f ),new Color (1.00f, 0.66f, 0.22f, 1.00f ),new Color (0.50f, 0.71f, 0.76f, 1.00f ),new Color (1.00f, 0.32f, 0.70f, 0.42f ),new Color (0.22f, 0.39f, 0.45f, 1.00f ),new Color (0.16f, 0.83f, 1.00f, 1.00f ),new Color (0.13f, 0.28f, 1.00f, 1.00f ));

		// ??
		Create (new Color (0.79f, 0.79f, 0.79f, 1.00f ),new Color (0.13f, 0.75f, 0.37f, 1.00f ),new Color (0.87f, 0.66f, 0.50f, 1.00f ),new Color (0.39f, 0.85f, 0.56f, 1.00f ),new Color (1.00f, 0.70f, 0.15f, 0.42f ),new Color (0.30f, 0.31f, 0.42f, 1.00f ),new Color (0.16f, 0.83f, 1.00f, 1.00f ),new Color (0.13f, 0.28f, 1.00f, 1.00f ));

		// Cool Blue
		Create (new Color (0.03f, 0.34f, 0.52f, 1.00f ),new Color (0.22f, 1.00f, 0.65f, 1.00f ),new Color (0.26f, 1.00f, 0.85f, 1.00f ),new Color (0.36f, 0.50f, 0.60f, 1.00f ),new Color (0.00f, 0.30f, 1.00f, 0.42f ),new Color (0.16f, 0.31f, 0.28f, 1.00f ),new Color (0.16f, 0.83f, 1.00f, 1.00f ),new Color (0.13f, 0.28f, 1.00f, 1.00f ));


		// Luna Aqua
		Create (new Color (0.00f, 0.55f, 0.65f, 1.00f ),new Color (1.00f, 0.68f, 0.93f, 1.00f ),new Color (1.00f, 0.92f, 0.49f, 1.00f ),new Color (0.43f, 0.75f, 0.81f, 1.00f ),new Color (0.00f, 0.30f, 1.00f, 0.42f ),new Color (0.23f, 0.37f, 0.56f, 1.00f ),new Color (0.16f, 0.83f, 1.00f, 1.00f ),new Color (0.13f, 0.28f, 1.00f, 1.00f ));

		// Viper
		Create (new Color (0.27f, 0.90f, 0.25f, 1.00f ),new Color (0.60f, 1.00f, 0.93f, 1.00f ),new Color (0.84f, 1.00f, 0.52f, 1.00f ),new Color (0.09f, 1.00f, 0.77f, 1.00f ),new Color (0.00f, 0.53f, 0.14f, 0.42f ),new Color (0.10f, 0.32f, 0.26f, 1.00f ),new Color (0.16f, 0.83f, 1.00f, 1.00f ),new Color (0.13f, 0.28f, 1.00f, 1.00f ));

		// Candy
		Create (new Color (0.89f, 0.01f, 0.01f, 1.00f ),new Color (0.48f, 0.14f, 0.74f, 1.00f ),new Color (1.00f, 0.58f, 0.97f, 1.00f ),new Color (0.53f, 0.34f, 0.51f, 1.00f ),new Color (1.00f, 0.13f, 0.37f, 0.42f ),new Color (0.26f, 0.19f, 0.32f, 1.00f ),new Color (0.16f, 0.83f, 1.00f, 1.00f ),new Color (0.13f, 0.28f, 1.00f, 1.00f ));

		// Spider
		Create (new Color (0.01f, 0.89f, 0.42f, 1.00f ),new Color (0.82f, 0.34f, 0.66f, 1.00f ),new Color (1.00f, 0.99f, 0.58f, 1.00f ),new Color (0.53f, 0.34f, 0.51f, 1.00f ),new Color (1.00f, 0.13f, 0.47f, 0.42f ),new Color (0.14f, 0.25f, 0.31f, 1.00f ),new Color (0.16f, 0.83f, 1.00f, 1.00f ),new Color (0.13f, 0.28f, 1.00f, 1.00f ));

		// Energy
		Create (new Color (1.00f, 0.35f, 0.60f, 1.00f ),new Color (1.00f, 0.60f, 0.00f, 1.00f ),new Color (1.00f, 0.45f, 0.21f, 1.00f ),new Color (1.00f, 0.24f, 0.24f, 1.00f ),new Color (1.00f, 0.78f, 0.20f, 0.42f ),new Color (0.29f, 0.15f, 0.13f, 1.00f ),new Color (1.00f, 0.63f, 0.16f, 1.00f ),new Color (1.00f, 0.22f, 0.13f, 1.00f ));

		// Calm
	
	}

	static void Create (Color skyColorTop, Color skyColorBottom, Color moonColorTop, Color moonColorBottom, Color glowColor,  Color midMainColor, Color particleColor, Color particleColorEnd)
	{

		BGGraphics graphics = new BGGraphics();
		list.Add (graphics);

		graphics.skyColorTop = skyColorTop;
		graphics.skyColorBottom = skyColorBottom;
		graphics.moonColorTop = moonColorTop;
		graphics.moonColorBottom = moonColorBottom;
		graphics.glowColor = glowColor;
		graphics.midMainColor = midMainColor;
		graphics.particleColorStart = particleColor;
		graphics.particleColorEnd = particleColorEnd;

	}
}
